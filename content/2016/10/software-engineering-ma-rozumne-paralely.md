+++
title = "Software Engineering, má rozumné paralely?"
date = 2016-10-04T09:59:00Z
updated = 2018-02-22T13:06:44Z
description = """Rád hledám paralely k softwarovému inženýrství. Trošku
    jsem se nad tím zamyslel a snad z toho vznikne krátký seriál."""
tags = ["sw-engineering"]
aliases = [
    "/2016/10/software-engineering-ma-rozumne-paralely.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2016/10/Rock-of-Cashel.png"
    link="/2016/10/Rock-of-Cashel.png" width="200" >}}

Konstantou [Software Engineeringu](//en.wikipedia.org/wiki/Software_engineering) (SE) je jeho dynamika. Snad právě proto hledáme paralely a podobenství v jiných oblastech lidské činnosti --- abychom lépe porozuměli jeho zákonitostem, uchopili jeho abstraktní nehmotnost a někdy ;-)  i jen našli pevnou půdu pod nohoma.

Nejčastějšími doménami, kam se sahá pro inspiraci a popis jsou: "klasická" architektura a stavebnictví (stavíme sofware), strojní výroba (vyrábíme software) a automobilový průmysl (procesní a automatizovaná výroba od designu po prodej). Tyto paralely jsou hodnotné, protože umožňují [průmět](//cs.wikipedia.org/wiki/Pr%C5%AFm%C4%9Bt), [projekci](//en.wikipedia.org/wiki/Graphical_projection) něčeho obecně známého do něčeho, o čem někdy tušíme pouze [fuzzy](//en.wikipedia.org/wiki/Fuzzy_logic) obrysy.

## Auta, domy, nebo továrny?

Tak jak jsou tyto paralely přínosné, tak jsou zároveň zavádějící. Což není nijak překvapivé --- architektura má za sebou tisíce let vývoje. První automobil vznikl někdy v 19. století. Pokud bychom tedy chtěli srovnávat softwarový vývoj s automobilovým průmyslem, bylo by adekvátnější, kdybychom to dělali s fází, kdy ještě nebyl _průmyslem_. Tedy v dobách, kdy byly zhruba jasné požadavky, ale každý je implementoval po svém.

{{< figure src="/2016/10/Benz.jpg" caption="Automobil Benz z r. 1885 (zdroj [Wikepedia])" width="600" >}}

Tehdy ještě nebyla žádná standardizace. Postupně vznikala unifikace, normy, automatizace, globalizace. Tyhle tendence lze také vysledovat i v oblasti SE. Ale pořád to každý dělá tak nějak po svém.

Pokud máme k dispozici standardizované komponenty (třeba cihly), lze z nich skládat komplexní řešení. Ale nejen to --- dá se také přesně spočítat (s rozumnou odchylkou) koncová cena, v návrhu se dají zohlednit jejich fyzické vlastnosti (např. stabilita cílového řešení) atd. Tohle všecno v SE chybí.

## Marnost, samá marnost

Lidé z byznysu už dlouhá léta sní o tom, jak se bude software skládat z jednotlivých "krabiček" (= komponent), ale kromě marketérů, kteří se snaží taková řešení prodat, nejspíš nikdo nevěří tomu, že by to fungovalo.

Z mojí zkušenosti se nejdál v tomto směru dostaly nástroje a systémy z oblasti [SOA](//en.wikipedia.org/wiki/Service-oriented_architecture) (a nijak překvapivě, hlavně ty komerční). Do určíté úrovně (abstrakce, či návrhu) lze řešení vytvářet "taháním kostiček a šipek". Ovšem od této hranice níž nastává, námi vývojáři běžně zažívaná, realita --- ty "kostičky" je potřeba naimplementovat a to už jsme na poli klasického softwarového vývoje.

{{< figure src="/2016/10/Oracle-SOA-Suite-composite-app.png" caption="Kompozitní aplikace v Oracle SOA Suite" >}}

Samozřejmě, hodně by zde pomohla [znovupoužitelnost](//en.wikipedia.org/wiki/Reusability). Ale ruku na srdce --- už jste někdy viděli, že by [reusabilita](//en.wikipedia.org/wiki/Reusability) fungovala na úrovni komponent? Já teda moc ne. Obvykle bývá problém v [governance](//en.wikipedia.org/wiki/SOA_governance) --- dozvíte se, že to není pořeba, není to ve scopu projektu, verzování je zbytečná práce navíc, řešení (zpětné) kompatibility je moc náročné atd. Takže řekněme, že... znovupoužitelnost je hezký, ale teoretický koncept. Zatím. Oni se to lidi časem naučí. Tak jako se naučili stavět domy a auta.

Jiným problémem SE je, že se nezabývá jednou (byť třeba rozsáhlou) doménou, jako třeba stavebnictví, ale zasahuje v podstatě do jakékoliv oblasti. V takovém prostředí se pak velmi těžko extrahují nějaké principy, o které se lze opřít v příštích projektech. Proč asi agilní vývoj neslibuje nic konkrétního, ale spíš něco ve stylu "bude to kvalitní, budeme to neustále zlepšovat, ale nevíme přesně, co to nakonec bude" :-)

[Vanitas vanitatum](//www.biblegateway.com/passage/?search=Eccl.%201:2;12:8;&amp;version=VULGATE;). Byť se někteří z nás věnují SE celý svůj produktivní život a pokud zůstanou [zdravými programátory](/2013/08/zdravy-programator/), tak snad i zbytek života; tak z hlediska pomíjivosti je SE stále ještě v dětských letech (ne-li v plenkách). Třeba se za sto let naši potomci a následovníci ohlédnou zpět a budou nás litovat, v jakých krutých a nestabilních podmínkách jsme budovali svá díla. A po dalších staletích, až vznikne nové vědecké odvětví --- softwarová archeologie --- budou vzdálené generace stát v úžasu nad tím, co jsme dokázali vytvořit _holýma rukama_. (Anebo taky ne.)

## Má to smysl?

Pokud se kruhem vrátím zpátky na začátek --- má smysl hledat pro SE nějaké paralely? Myslím si, že ano. Pomůžou s porozumněním komplexních řešení a problémů, na které lidská mysl není designovaná. Ale je potřeba to brát s rezervou... software zkrátka nemá čtyři kola.

## Související externí články

* [Stavebnictví versus softwarové inženýrství](//blog.zvestov.cz/item/142)
