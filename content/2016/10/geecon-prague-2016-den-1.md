+++
title = "GeeCON Prague 2016, den 1"
date = 2016-10-31T22:47:00Z
updated = 2016-10-31T22:50:56Z
description = """Navštívil jsem po čase vývojářskou konferenci. Jaký byl první
    den pražského GeeCONu?"""
tags = ["konference", "java"]
aliases = [
    "/2016/10/geecon-prague-2016-den-1.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2016/11/GeeCon.png" >}}

Letos jsem to nevychytal co se týče školení --- prostě jsem to úplně vypustil --- a tak jsem si řekl, že se aspoň trochu zahojím na nějaké konferenci. A protože jsem už pár let po očku sledoval [GeeCON](//geecon.cz/), resp. jeho pražskou odnož, padla volba na něj.

A vybalím to na vás hned na začátku: Je to dobrá konference, stojí za to, na ni jít. Určitě mají (kluci polský) ještě co zlepšovat. Ale celkově je to přínosný --- ať chcete držet prst na tepu doby (= bleeding edge), mít všeobecný přehled, co se v doméně děje, anebo najít inspiraci --- to vše tady najdete v rozumně vyvážené symbióze.

## Keynote

Keynote byly dvě --- první sponzorská, do počtu, naštěstí jen 20minutovka a druhá, která se pro mne stala vrcholem dne.

### Next Gen R&D: craft vs. chop-shop

Keynote Ondřeje Krajíčka ([@OndrejKrajicek](//twitter.com/ondrejkrajicek)) ze sponzorujícího Y Softu byla krátká a... neinspirativní. Úplně jsem nepochopil souvislost mezi názvem a obsahem přednášky --- možná v obsahu bylo něco o [R&D](//en.wikipedia.org/wiki/Research_and_development), ale asi mi to uniklo. V podstatě šlo o to, že jako vývojáři jsme zároveň vědci, umělci i řemeslníci (scientists, artists/artisans, craftsmen) a že učednictví, jako způsob výuky, je s námi spousty (stovky) let.

### Become a Polyglot by learning Java!

Keynote Jaroslava Tulacha ([@JaroslavTulach](//twitter.com/jaroslavtulach)) nejlépe shrnuje následující tweet:

{{< tweet 789022632842846213 >}}

Nadšení bylo na místě, takhle má prostě vypadat správná key note --- nemusíte se nutně posadit na zadek, ale nějaký wov! moment by tam měl být. A to se tady podařilo.

{{< figure class="floatright" src="/2016/11/Oracle-Labs.jpg" >}}

Během 50 minut, představil Jaroslav dvě věci: [Graal VM](//github.com/graalvm) a [Truffle](//github.com/graalvm/truffle). Graal je nová virtuální mašina, která se integruje s HotSpotem (prý stačí jen na classpath "upustit" Graal JAR), která umožňuje běh různých jazyků --- JVM i ne-JVM (resp. ne jejich JVM, ale nativní implementaci), třeba JavaScript a Ruby.

Toho se týkala i všechna tři dema. V prvním šlo o zavolání Ruby metody z JavaScriptu (nebo naopak?). Druhé demo ukazovalo jakýsi cross-debugging: prostě debugguju, debugguju, jsem v Ruby, ještě chvilku debugguju a jsem v JavaScriptu. A vidím šechno, proměnný atd. Cool, hustý, hustokrutý. Třetí demo jsem myšlenkově trochu vypustil, ale myslím že šlo o [Node.js](//nodejs.org/en/).

Jestli jsem to správně pochopil, tak Truffle je nástroj na psaní vlastních jazyků, který pak můžou běžet v Graalu a rovnou mají dobrou performance. Co dobrou, jsou rychlý jak sviňa. To byla taky jedna z věcí, kterou Jaroslav předváděl/říkal --- že Graal VM má lepší performance, než HotSpot (v případě Javy), nabo nativní implementace Ruby. Ukazoval to na prográmku, který implementoval [Eratosthenovo síto](//en.wikipedia.org/wiki/Sieve_of_Eratosthenes).

Věc, která mě zaujala, když Jaroslav zmiňoval problematiku JVM a dynamických jazyků: vývojáři zaměřený na aplikace (např. NetBeans) preferují staticky typované jazyky. Naopak vývojáři zaměřený na data (např. astronomové) preferují dynamické jazyky. Na tom by něco být mohlo (i když pro mě to neplatí --- vzdycky jsem dělal aplikace, a začínal jsem na dynamických jazycích, takže (ne)existenci typů neřeším).

## Čtvrtek

Celkově hodnoceno, první den GeeCONu byl slabší, než ten následující --- vyjma Graal/Truffle key note a přednášky o graph DB, šlo o průměrné přednášky. Ne nezajímavé a určitě ne ztráta času, ale přeci jen... slabší.

### Who's Afraid of Graphs?

Přednáška Davida Ostrovskyho ([@DavidOstrovsky](//twitter.com/DavidOstrovsky)) o grafových databázích pro mne byla toho dne nejzajímavější a nejpřínosnější. David je určitě velký fanoušek filmu [Spaceballs](//www.youtube.com/watch?v=CtPzInmj8JU), jímž vydatně špikoval demo, a filmů [Mela Brookse](//www.csfd.cz/tvurce/366-mel-brooks/). A taky se podílí na vývoji [Couchbase](//www.couchbase.com/), což je dokumentová NoSQL databáze. Nicméně tentokrát hovořil o "konkurenci".

Zajímavá byla hned úvodní myšlenka --- NoSQL databáze jsou s námi cca 10 let. Relační DB zhruba 40 let. Ale grafy jsou s náma minimálně 300 let. Mimo jiné zmiňoval matematický problém [Seven Bridges of Königsberg](//en.wikipedia.org/wiki/Seven_Bridges_of_K%C3%B6nigsberg), kterým se zabýval [Euler](//en.wikipedia.org/wiki/Leonhard_Euler) v 18. století.

David zmiňoval use casy pro využítí grafových databází a zároveň uvedl a hned zavrhl nejčastější případ --- sociální sítě. K tomu hned za chvíli. Dalšími příklady užití jsou authorizace a herní ekonomiky (game economies). A obecně případy, kdy jde použít "statické dotazy".

Proč nepoužívat grafové databáze pro sociální sítě? Protože neškálují. Doslova _"graphs don't scale"_. Procházet celý graf pro každý dotaz? To asi Facebook nedělá. Co s tím? David doporučoval dvě věci: Za prvé [sharding](//en.wikipedia.org/wiki/Shard_(database_architecture)). Sice to pořád neškáluje, ale je to lepší než nic. A za druhé, rozdělit data --- mít grafová data v grafové databázi (např. [Neo4j](//neo4j.com/)) a zbývající data v relační databázi (např. [MySQL)](//www.mysql.com/).

{{< figure class="floatright" src="/2016/10/Neo4j.png" >}}

Velkou část přednášky věnoval David praktickým ukázkám několika grafových databází. Začal tou nejpopulárnější, [Neo4j](//neo4j.com/) a jejím dotazovacím jazykem [Cypher](//neo4j.com/developer/cypher-query-language/). Cypher má specifickou notaci, kdy používá [ASCII art](//en.wikipedia.org/wiki/ASCII_art) pro reprezentaci patternů. Pro koho je Cypher moc exotický, může zkusit [OrientDB](//orientdb.com/), která používá SQL-like dotazování.

Problematiku dotazování řeší také další nástroj: [Gremlin](//github.com/tinkerpop/gremlin/wiki), jazyk pro traversální dotazování grafů. Gremlin není svázaný s konkrétní databázovou implementací, ale pomocí [blueprintů](//github.com/tinkerpop/blueprints/wiki) se může dotazovat nad libovolnou grafovou databází. Blueprint je analogie JDBC pro grafové databáze.

Poslední ukázka se týkala [Elasticsearch](//www.elastic.co/products/elasticsearch), kdy David ukazoval live Twitter analýzu (Hillary vs. Trump). Bylo to na mě hodně rychlé a efektní. Co mě zaujalo, možnost vytvářet virtuální grafy on-the-fly, podle momentální potřeby, tedy bez toho, aby dané vazby v datech existovaly.

### Ten Productivity Tips for Java EE and Spring Developers

U přednášky Simona Maplea ([@sjmaple](//twitter.com/sjmaple)) ze [ZeroTurnaround](//zeroturnaround.com/) nepřekvapilo, že na závěr zpropagoval dva jejich nástroje, nicméně nebylo to nijak násilné a do konceptu to zapadalo.

Ohledně produktivity zmiňoval tři oblasti: komunikaci, správu tasků a nástroje. Komunikaci věnoval Simon minimum prostoru. V podstatě řekl, že komunikace je klíčová, mnohem náročnější, pokud je vzdálená (remote) a dobrým nástrojem pro distribuovanou komunikaci je [Slack](//slack.com/). Myslím si, že v reálném životě má největší přínos pro produktivitu právě komunikace, ale chápu, že se to na vývojářské konferenci špatně prezentuje.

Doporučení ohledně správy tasků je přímočaré:

* Optimizing current tasks
* Removing non-essential tasks
* Retaining positive (process, activities, tools etc.)

{{< figure class="floatright" src="/2016/10/JBoss-Forge.png" width="200" >}}

Nejvíce prostoru věnoval Simon nástrojům a z nich nejvíce [JBoss Forge](//forge.jboss.org/). Tenhle nástroj mě zatím zcela míjel, takže jestli jsem to správně pochopil, jde o JBossí odpověď na [scafolding](//en.wikipedia.org/wiki/Scaffold_(programming)). No, řekl bych, že v dnešní době celkem passé. Co mě trochu zarazilo --- vzhledem k zaměření přednášky --- že Simon v demu preferoval [Maven](//maven.apache.org/) před [Gradlem](//gradle.org/). Buď si tu ukázku ulehčil, nebo tak trochu ztrácí na kredibilitě.

Další nástroje zahrnovaly [Arquillian](//arquillian.org/) (JUnit-like testování v kontejneru), [TomEE](//tomee.apache.org/) (Java EE extension/profil pro Tomcat), [Spring Boot](//projects.spring.io/spring-boot/) ([RAD](//en.wikipedia.org/wiki/Rapid_application_development)(?) ve Springu) a konečně v úvodu zmiňovaný [JRebel](//zeroturnaround.com/software/jrebel/) a [XRebel](//zeroturnaround.com/software/xrebel/). Výčet sice hezký, ale asi nic, co by seniorního vývojáře překvapilo. Osobně mě to neinspirovalo si ani jeden nástroj vyzkoušet.

Shrnutí Simonovy přednášky znělo:

* Care about your time
* Don't lose sight of your goal
* Explore productivity tools

### Apache Cassandra: building a production app on an eventually-consistent DB

[Oliver Lockwood](//www.linkedin.com/in/oliverlockwood) se na úvod zeptal, kdo sleduje fotbal. A kdo sleduje [Premier League](//www.premierleague.com/). Oliver pracuje pro televizi [Sky](//www.sky.com/), která je držitelem licence na vysílání Premier League. Jak to spolu souvisí? Sky používá pro persistenci jejich Online Video platformy databázi [Apache Cassandra](//cassandra.apache.org/).

{{< figure class="floatright"
    src="/2016/10/Cassandra.png" >}}

Cassandra je NoSQL distribuovaná databáze pro správu velkých dat. Z úvodu si pamatuju, že jeden node (náhodně vybraný loadbalancerem) je pro daný příkaz řídící pro zbytek clusteru. Pro databázi se definuje _replication factor_, tj. kolik kopií záznamu chci mít na jednotlivých nodech. A že při synchronizaci vyhrává poslední zápis (last write wins).

K čemu není Cassandra dobrá? Pokud potřebujete komplexní dotazy, nebo něco jako RDBMS transakce. A co může být na Cassandře problematické? Synchronizace času mezi jednotlivými nody. Co s tím? Buď používat synchronizační timestamp na straně klienta, nebo se "časově citlivým" (time-sensitive) dotazům vyhnout.

Poslední část přednášky byla o rozvoji DB schématu. Ve Sky si na to napsali vlastní nástoj [cqlmigrate](//github.com/sky-uk/cqlmigrate), dostupný na GitHubu. Nástroj napomáhá automatizaci, může provádět přírůstkové změny na jednotlivých nodech.

### Machine Learning by Example

Pro mne nejslabší přednáška tohoto dne. Téma prezentované Michałem Matłokou ([@mmatloka](//twitter.com/mmatloka)) bylo sice zajímavý, ale asi nešťastně zpracovaný. Předně, věnoval příliš mnoho času úvodu do tématiky, což nejspíš bylo zbytečný --- každý, kdo měl aspoň trochu podobný předmět na škole (u nás se to jmenovalo "Expertní systémy") se asi trochu nudil.

{{< figure class="floatright" src="/2016/10/Spark.png" width="200" >}}

Takže use casy --- rozpoznání hlasu, detekce obličeje, analýza podvodů (fraud analysis), self-driving cars. Typy učení --- supervised, unsupervised, semo-supervised a reinforcement learning. A citát z Alana Turinga.

A pak, bez nějakého přechodu, skok rovnou do dema, kdy jsem nepochopil, co se děje. Ukázka byla založená na použití nástrojů [Apache Spark](//spark.apache.org/) a [Zeppelin](//zeppelin.apache.org/), ale neptejte se mě, jak spolu souvisí. Demo bylo dlouhé a já jsem se v prvních 5 minutách ztratil, takže jsem zbytek přednášky protweetoval.

### Definition of Done: Working Software in Production

Poslední čtvrteční prezentace (kterou jsem absolvoval) od Thomase Sundberga ([@thomassundberg](//twitter.com/thomassundberg)), byla přesně ten typ přednášky, který nesnáším:

1. Vyberete si podle anotace,
1. Přenáška začne
1. Cca v půlce si říkáte, o čem to vlastně je,
1. Podíváte se znovu (stále během přednášky) do anotace, abyste si připomněli, na co jste to šli
1. a cca 5 minut po přednášce vám dojde, jak spolu souvisí titul a obsah.
1. Jenže tu není žádný aha! moment, protože nic tak nového jste se nedozvěděli.

A přitom to vlastně tak špatná přednáška nebyla. Takže o co šlo? [Continuous Delivery](//en.wikipedia.org/wiki/Continuous_delivery). A co že je tím [DoD](//www.agilealliance.org/glossary/definition-of-done/)? Váš software v produkci. A teď, jak na to:

* Převezměte kontrolu nad buildem --- musí být triviální spustit build lokálně jedním příkazem.
* Všechno verzujte --- tady se to trochu rozjíždělo s tím, že dávali na produkci SNAPSHOTY (už se nepamatuju proč).
* Automatizujte testování.
* Procvičujte (practice) --- prvně v testu, pak na produkci.
* Perfect is not a place, it is a direction.
* A hlavně: Začněte s málem a vydržte. Každé zlepšení se počítá.

No a pak vám s tím, samozřejmě, pomůžou nástroje. V Thomasově případě to byly: [Git](//git-scm.com/), [Jenkins](//jenkins.io/), [Puppet](//puppet.com/), [Gradle](//gradle.org/), [RPM](//www.rpm.org/) a [JFrog](//www.jfrog.com/).

## Pátek

Vzhledem k tomu, že jsem se slušně rozepsal, rozlomil jsem článek ve dví. Mé dojmy z pátečního GeeCONu si můžete přečíst na odkazu [GeeCON Prague 2016, den 2](/2016/11/geecon-prague-2016-den-2). ~~(strpeníčko... dejte mi trochu času to napsat ;-)~~

## Mind Map

{{< figure src="/2016/10/GeeCON-Prague-2016.png"
    link="/2016/10/GeeCON-Prague-2016.png"
    caption="GeeCON Prague 2016 --- Key Note, organizace a témata" >}}

{{< figure src="/2016/10/GeeCON-Prague-2016-Thursday.png"
    link="/2016/10/GeeCON-Prague-2016-Thursday.png"
    caption="GeeCON Prague 2016, den 1." >}}

## Související články

* [Můj pohled na Agile Prague 2014](/2014/09/muj-pohled-na-agile-prague-2014/)
