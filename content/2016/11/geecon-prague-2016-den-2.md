+++
title = "GeeCON Prague 2016, den 2"
date = 2016-11-30T23:28:00Z
updated = 2016-11-30T23:28:04Z
description = """Druhý den pražské vývojářské (Java) konference
    GeeCON. Dobré přednášky, špatné kafe. ☕"""
tags = ["konference", "java"]
aliases = [
    "/2016/11/geecon-prague-2016-den-2.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2016/11/GeeCon.png" >}}

Zúčastnil jsem se dvoudenní Java vývojářské konference [GeeCON Prague](//geecon.cz/). Možná se mýlím, protože nemám potřebný rozhled a informace, ale GeeCON mi přijde jako momentálně nejlepší Java konference v Praze --- má mezinároní spíkry (všechny přednášky v angličtině), slušné renomé a odpovídající podporu sponzorů.

Pokud máte tip na jinou Java/vývojářskou konferenci (obdobného rozsahu), dejte mi vědět v komentářích, ať vím, kam vyrazit příští rok.

## Pátek

O prvním dni konference jsem se rozepsal v článku [GeeCON Prague 2016, den 1](/2016/10/geecon-prague-2016-den-1/). Dnes budu pokračovat druhým, pátečním dnem, který se mi celkově líbil víc. Což může být subjektivní --- mě udělaly radost dvě výborné přednášky o [Clojure](//clojure.org/), z nichž jedna pro mne byla vrcholem celého GeeCONu.

A kromě Clojure zde byla také výborná přednáška o Microservices a pokračování tématu Graal/Truffle.

### How to Bake Reactive Behavior into Your Java EE Application

Přednáška Ondreje Mihályi ([@OMihalyi](//twitter.com/OMihalyi)) o [reaktivním programování](//en.wikipedia.org/wiki/Reactive_programming) (RP) mne moc neoslovila, i když nebyla špatná. Přiznám se, poslední dva tři roky jsem nesledoval aktuální trendy, takže jsem možná vedle, ale tyhle rective záležitosti mi přijdou jako momentální hype.

V úvodu přednášky mi chybělo nějaké "zorientování se" pro ty z nás, co nejsme v reactive doma. Uvítal bych nějaké diagramy, který daný design osvětlí. A naopak bych oželel "programování ve slidech" (powerpoint programming).

{{< figure class="floatright" src="/2016/11/Payara.jpg" >}}

Jinak měl Ondrej rozumný přístup --- používat RP tam, kde to má smysl a pomocí refactoringu ho aplikovat na stávající design. Toho se týkalo také demo: na počátku byla aplikace (tuším nějaké vyhledávání letů) napsaná v JSF a JAX-RS, v níž se sekvenčně, ve vláčku, volali tři webové služby.

Během refactoringu došlo na zapojení [WebSocketů](//en.wikipedia.org/wiki/WebSocket) a Event Busu v [Payara Micro](//www.payara.fish/payara_micro). S tou Payarou jsem to moc nepobral --- zrovna tady bych ocenil nějaký komponent diagram. Odnáším si z toho, že Payara tam byla hlavně kvůli [CDI Event Busu](//docs.oracle.com/javaee/7/tutorial/cdi-adv005.htm#GKHIC). Pokud to tak bylo, tak v realitě by stálo za to tam mít něco event-minimalistic, případně použít služby aplikačního serveru (pokud je poskytuje).

### One VM to Rule Them All

Přednáška Jakuba Podlešáka ([@japod](//twitter.com/japod)) a [Jana Štoly](//www.linkedin.com/in/jan-%C5%A1tola-5950422) pokračovala v tématu čtvrteční key note --- [Graal VM](//github.com/graalvm) a [Truffle](//github.com/graalvm/truffle). Pánové to měli sladěný nejen tematicky, ale i outfitem, zkrátka bylo hned jasný, kdo dělá v Oracle Labs:

{{< tweet 788786994168139776 >}}

Obsahově byla přednáška zajímavá, ale pro mne ne moc záživná --- přece jenom, kompilery a podobná havěť byly pro mne na škole jen nutné zlo. Takže byť stromy jsou velmi zajímavá algoritmická struktura, tak vytváření a manipulace [AST](//en.wikipedia.org/wiki/Abstract_syntax_tree) velká zabava není.

{{< figure class="floatright" src="/2016/11/Oracle-Labs.jpg" >}}

Kluci se v prezentaci střídali, což trochu rozbilo plynulost a soudržnost. Začali prezentací [Graalu](//github.com/graalvm) a tím, jak rychle v něm běží [jazyk R](//www.r-project.org/). Zkrátka R jako Rychle :-) Pak bylo něco o symbióze R a JavaScriptu (import funkce z jednoho jazyka do druhého).

A pak přišlo to AST... teda _Truffle_. Přiznám se, že po těch pár týdnech se mi toho už dost vykouřilo z hlavy a to i když se dívám do mind mapy (viz níže), kterou jsem si psal. Takže v krátkosti, viděli jsme jak napomoci parsování pomocí anotace [@Specializaction](//cesquivias.github.io/blog/2014/12/02/writing-a-language-in-truffle-part-2-using-truffle-and-graal/#specialized-read-methods) a parametru [guards](//cesquivias.github.io/blog/2014/12/02/writing-a-language-in-truffle-part-2-using-truffle-and-graal/#quotenode). Pak tam bylo taky něco o optimalizaci a partial evaluation. No, a víc už vám k tomu neřeknu.

### Thirty Months of Microservices. Stairway to Heaven or Highway to Hell?

Jednoznačně největší show celého GeeCONu. Holanďan Sander Hoogendoorn ([@aahoogendoorn](//twitter.com/aahoogendoorn)) to umí pořádně rozjet! Už jenom to, že jako cizinec je schopný do své prezentace naprat několik (smysluplných) slidů z [A je to!](//cs.wikipedia.org/wiki/Pat_a_Mat)

{{< tweet 789397734901952512 >}}

Sander začal obligátním _"monoliths are bad"_, aby pak pokračoval bojovými zkušenosti z implementace microservis. V jedné větě by se jeho příspěvek dal shrnout: _"microservices are not easy"_.

Sanderovi zkušenosti se  dají shrnout do následujících bodů:

* Microservices require evolutionary architecture.
* Start with guiding principles --- implement business process, avoid transactions, components own their own data/storage.
* RESTfulness is not easy --- [Postel's Law](//en.wikipedia.org/wiki/Robustness_principle), have conventions (e.g. URI).
* Testing --- everything, fail quickly, automation, contract testing ([JBehave](//jbehave.org/)).
* [DevOps](//en.wikipedia.org/wiki/DevOps) is not easy --- [Infractructure as Code](//en.wikipedia.org/wiki/Infrastructure_as_Code).

Hlavní poselství přednášky? Tak samozřejmě rozumné _"microservices are not for everyone"_. Rozumné proto, že cca poslední 3 roky se s microservices roztrhl pytel --- prostě klasický [hype](//en.wiktionary.org/wiki/hype) (naštěstí už to opadá) --- a opět to není žádná [silver bullet](//en.wikipedia.org/wiki/No_Silver_Bullet).

### TDD: That's not What We Meant

{{< figure class="floatright" src="/2016/11/Growing-Object-Oriented-SW.jpg"
    link="/2016/11/Growing-Object-Oriented-SW.jpg" width="250" >}}

Zklamáním pro mne bylo vystoupení Steva Freemana ([@sf105](//twitter.com/sf105)). Steva mám zapsaného jako spoluautora frameworku [jMock](//www.jmock.org/), bohužel, dnes už mrtvého, který jsem měl hodně rád (verzi 2). A také jako spoluautora oceňované knihy [Growing Object-Oriented Software Guided by Tests](//www.growing-object-oriented-software.com/). Takže jsem čekal hodně.

Steve byl jeden z mála nativních anglických speakerů (je z Londýna) a jak to tak někdy bývá, nebylo mu vůbec rozumět. Mám na akcenty docela ucho, ale řeknu vám, zlatý irský, nebo skotský akcent :-)

Zklamaný jsem byl hlavně proto, že Stevova přednáška byla nudná, unylá. A přitom tam bylo pár skrytých rubínů a smaragdů. Třeba o profláknuté slavné knize Kenta Becka [Test Driven Development](//www.amazon.com/Test-Driven-Development-Kent-Beck/dp/0321146530):

> _"It's worth re-reading. Nothing much there for the 1st read. But, so much there... after couple of years!"_

Steve hodně kritizoval praktiku, kdy se testy dělají jen na oko --- _"Look, we have tests!"_. Nazýval to _"Test-Driven Theatre"_.

Nejvíc se mi líbil tenhle slide:

{{< tweet 789510188319506432 >}}

### Doing Data Science with Clojure: the Ugly, the Sad, the Joyful

Přednáška Simona Belaka ([@sbelak](//twitter.com/sbelak)) pro mne byla příjemným překvapením. Prezentaci jsem si vybral hlavně proto, že mám pro [Clojure](//clojure.org/) slabost (i když ho už pár let zanedbávám).

{{< figure class="floatright" src="/img/logos/Clojure.png" width="200" >}}

Simon mluvil o Clojure hlavně ve vztahu k [Data Science](//en.wikipedia.org/wiki/Data_science). Proč je Clojure v této doméně ideální jazyk? V Data Science jsou různé oblasti, požadavky a většina používaných jazyků je řeší jenom částečně.

Infrastruktura se dá dobře řešit v Pythonu, nebo ve Scale. Modelování zase v [R](//www.r-project.org/), nebo (opět) v Pythonu. Pak tu máme čištění a přípravu dat. A nakonec --- a zde Clojure exceluje --- manipulace se strukturou. A Clojure pokrývá všechny tyto aspekty. No a samozřejmostí (u všech jmenovaných jazyků), je [Live Programming](//en.wikipedia.org/wiki/Interactive_programming), které poskytuje [REPL](//en.wikipedia.org/wiki/Read%E2%80%93eval%E2%80%93print_loop).

Dalším silným argumentem pro použití Clojure v Data Science je horká novinka: [clojure.spec](//clojure.org/about/spec). Upřímně, neřeknu vám, co to je --- budu si to muset pořádně nastudovat. Podle Simona se to dá použít na "dotazovatelný popis dat" (queryable data descriptions) a ve spojení s [Apache Kafkou](//kafka.apache.org/) (distributed streaming platform) je to husto-krutý. Opět nevím, o čem píšu, je to na mne moc raketová věda. Teda datová.

{{< tweet 789438991590363136 >}}

Na závěr byly dvě zajímavé otázky z publika. Zaprvé, jak je na tom Clojure s unit testy. Odpověď --- Clojure není TDD-oriented. Což mimochodem říkal už tvůrce jazyka [Rich Hickey](//twitter.com/richhickey), který daleko radši debugguje. Mnohem vhodnější je [spec](//softwareengineering.stackexchange.com/questions/323509/diferences-between-test-vs-spec).

Druhá otázka se týkala toho, jak se naučit Clojure. Odpověď mi přišla brilantní --- na Clojure se dá dívat jako na funkce v Excelu. Takže v prvním kroku si zkusit problém "naimplementovat", či ukázat v Excelu a pak totéž jako funkci v Clojure. Jednoduché, geniální.

### Clojure, clojure.spec and Beyond

Pozor, famfára, tum tu du dú!! A cena za nejlepší přednášku jde za Carin Meier ([@gigasquid](//twitter.com/gigasquid)). Za mne jednoznačně a bez diskuze. Moc pěkně provedená, moc pěkně přednesená a zatraceně zajímavá.

Carin je autorkou knihy [Living Clojure](//shop.oreilly.com/product/0636920034292.do), dle svých slov píše v Clojure na denní bázi a Clojure v jejím podání je ten nejvíc cool a sexy jazyk, na který letos narazíte... víc cool &amp; sexy už je jen [clojure.spec](//clojure.org/about/spec). Ehm, použil jsem právě v jedné větě 4x slovo Clojure?!?

{{< tweet 789460691828797440 >}}

No, dost chvály. O čem že byla ta přednáška? Carin si vzala za základ dětskou knížku Doktora Seusse [One Fish Two Fish Red Fish Blue Fish](//en.wikipedia.org/wiki/One_Fish_Two_Fish_Red_Fish_Blue_Fish) a pomocí clojure.spec se jala čarovat. Programy jako stvoření, která se geneticky vyvíjejí.

{{< figure class="floatright" src="/2016/11/One-fish-two-fish.jpg" >}}

Kdo někdy trochu přičichnul k Lispu a podobné havěti, ví, že [code is data and data is code](//en.wikipedia.org/wiki/Homoiconicity). To není nic nového, ale clojure.spec to posouvá na úplně jinou úroveň --- můžete si generovat specifikace, které vám generují data, která mohou generovat další specifikace, přitom se modifikují a generují...

A Carin to krásně umí podat. Část její přednášky si můžete přečíst na jejím blogu: [One Fish Spec Fish](//gigasquidsoftware.com/blog/2016/05/29/one-fish-spec-fish/).

Asi jste si všimli, že v popisu téhle přednášky dost bruslím. Je to tak. I když se podíváte do mind mapy níže, uvidíte, že jsem si toho moc nezapsal. Ze dvou důvodů. Už jsem říkal, že to byla nejlepší přednáška? Bylo by prostě škoda si u toho dělat zápisky. A pak, bylo tam toho tolik, že je to výživné ještě na měsíce studia. O teď mě omluvte --- jdu generovat testovací data, která se rýmují. (Mimochodem, všimli jste si, že _two_ se rýmuje s _blue_?)

### New Java Literals for the Brave and True

Na přednášku Lukáše Křečana ([@lukas_krecan](//twitter.com/lukas_krecan)) jsem dorazil pozdě, vlastně až cca v druhé půlce, protože jsem se v předsálí zakecal s Carin Meier o [clojure.spec](//clojure.org/about/spec).

Takže jestli jsem to ze závěru pochopil, šlo o to, mít v Javě literály podobně jako ve Scale a Lukáš k tomu zneužil [Javovské lambdy](//docs.oracle.com/javase/tutorial/java/javaOO/lambdaexpressions.html).

Budu teď trochu ošklivý, nebo upřímný --- šel jsem se na přednášku podívat, abych viděl Lukáše na živo. Protože --- ruku na srdce --- Scala je mi dost ukradená.

### Dirty Hacks with Java Reflection

Závěrečná key note Heinze Kabutze ([@heinzkabutz](//twitter.com/heinzkabutz)) mě minula. Možná to byla únava, po dvou dnech intenzivního vstřebávání informací, možná přeplněná mozková kapacita, anebo, po vrcholné přednášce Carin Meyer, už nic nemohlo být zajímavé. Zkoušel jsem to, ale po 10 minutách jsem sjížděl Twitter a pak jsem se, ještě před půlkou, sbalil a šel domů.

To málo, co jsem v úvodu zachytil, byly klíčový slova _Reflection_, _Java 9_, _Atomic_ a _Concurrent_.

## Organizace

Tak, to byly přednášky. Nejen ty ale tvoří konferenci. Co organizace? Byl to můj první GeeCON a tak nemám s čím srovnávat. Jasně, srovnávat třeba s [Google Developer Day](//en.wikipedia.org/wiki/Google_Developer_Day) by nebylo fér.

V první řadě musím vyzdvihnout lidi z GeeCON, že se do toho vůbec pouští. Na téhle fan bázi, kdy za váma nestojí velká firma, to musí být velmi náročné, postavené na spoustě entuziasmu. I proto jsem tolerantnější.

Organizaci přednášek se v podstatě nedá nic vytknout. Technika fungovala, časový rozvrh klapal a to je hlavní. Android aplikace s programem konference fungovala uspokojivě. Dostal jsem zadarmo jednu el. knížku od O'Reilly.

Pak už to byly více méně drobnosti --- třeba hostesky by mohly znát heslo na WiFi, anebo tak nezmatkovat (a lépe informovat) s konferenčním tričkem.

Jednu velkou výhradu bych ale měl... catering. Koláčky ke svačině a džusy dobrý. Obědy docela slabota --- dalo se to, ale hodně lidí nedojídalo. Ale kafe? To byl průšvih! Java konference a mizerný kafe?!? Do zákulisí samozřejmě nevidím. Co ale příště domluvit sponzoring s [Doubleshotem](//www.doubleshot.cz/)?

{{< tweet 789089815933444096 >}}

## Shrnutí

GeeCON je opravdu dobrá Java konference. Přednášky jsou zajímavé a aktuální. Rozsah je tak akorát a ambice odpovídají Praze. Pokud během roku nenarazím na něco zajímavějšího, rád se na GeeCON podívám znovu.

## Mind Map

{{< figure src="/2016/11/GeeCON-Prague-2016.png"
    link="/2016/11/GeeCON-Prague-2016.png"
    caption="GeeCON Prague 2016 --- Key Note, organizace a témata" >}}

{{< figure src="/2016/11/GeeCON-Prague-2016-Friday.png"
    link="/2016/11/GeeCON-Prague-2016-Friday.png"
    caption="GeeCON Prague 2016, den 2." >}}

## Související články

* [GeeCON Prague 2016, den 1](/2016/10/geecon-prague-2016-den-1/)
* [Můj pohled na Agile Prague 2014](/2014/09/muj-pohled-na-agile-prague-2014/)
