+++
title = "Team Geek, team leader se srdcem"
date = 2013-07-17T23:58:00Z
updated = 2017-03-10T21:09:12Z
description = """Chlapci z Googlu (bývalí) napsali knihu o teamleadingu.
    Je dobrá - strukturovaná a myslí na lidi.Patterns & antipatterns
    included."""
tags = ["teamleading", "knihy"]
aliases = [
    "/2013/07/team-geek-team-leader-se-srdcem.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2013/07/Team-Geek.jpg"
    link="/2013/07/Team-Geek.jpg" width="250" >}}

Pryč jsou doby, kdy geekové a hackeři kutili něco v jeskyních a ven vycházeli jen když potřebovali vyklidit navršené krabice od pizzy. Dnešní geekové se myjou, češou, umí si sami objednat v restauraci a... pracují v týmu.

## Team Geek

Jsou různé způsoby, jak vést lidi a jeden z nich je založený na důvěře. Je to způsob, který mi vyhovuje a který praktikuju poslední tři roky, co se potýkám s rolí team leadera. Je to způsob, který vykrystalizoval spolu s hnutím [Open source](//en.wikipedia.org/wiki/Open_source) a poslední dobou, podobně jako agilní metodiky, si úspěšně prohryzává cestu do srdcí otrokářských korporací.

Necháte si občas poradit od zkušenějších? Pokud jste, nebo máte aspiraci být team leaderem, možná byste ocenili nahlédnutí do kuchyně pánů [Briana Fitzpatrika](//www.red-bean.com/fitz/) a [Bena Collins-Sussmana](//www.red-bean.com/sussman/), kteří o tomto tématu napsali knihu [Team Geek](//www.amazon.com/Team-Geek-ebook/dp/B008EKF87S) s podtitulem _A Software Developer's Guide to Working Well with Others_. A je to dobrá kniha. Není se co divit --- oba patří mezi zakládající členy projektu [Subversion](//subversion.apache.org/), později pracovali v Googlu a posledních pět let se věnují přednášení o sociálních vztazích mezi vývojáři. Pár jejich přednášek je [k vidění na YouTube](//www.youtube.com/playlist?list=PL1C8C35BCFF6C8A38).

## Tři pilíře srdce

Tři hlavní principy, na kterých staví poselství knihy, jsou označeny anglickou zkratkou HRT (vyslovuj jako _heart_) --- Humility, Respect, Trust. Čili pokora, respekt a důvěra. Kolem těchto základů pak postupně nabalují jednotlivé komunikační sféry. Začínají u nejdůležitější postavy, team leadera, aby se postupně přes tým, spolupracovníky a organizaci dostali až k uživatelům.

{{< figure src="/2013/07/HRT.png" caption="HRT --- Humility, Respect, Trust (zdroj [Team Geek](//www.amazon.com/Team-Geek-ebook/dp/B008EKF87S))" >}}

## Šest kapitol kolaborativní nirvany

Kniha je rozdělena do šesti kapitol, které částečně kopírují narůstání kontextu, ve kterém se každý tým pohybuje a částečně řeší praktické problémy, se kterými se team (leader) může setkat:

### 1. The Myth of the Genius Programmer

Myslíte si, že [Linus Torvalds](//en.wikipedia.org/wiki/Linus_Torvalds) napsal Linux úplně sám? Samozřejmě, že ne. Ale z určité vzdálenosti to tak může vypadat. Souvisí to s lidskou potřebou mít hrdiny (nebo mýty?). A hrdinové přece nedělají chyby.

Proč je v knize tahle kapitola? Protože druhou stranou touhy po dokonalosti je strach ukázat selhání, který vyvěrá z nedostatku sebe-důvěry a hlavně  --- důvěry v rámci určitého prostředí. V takovém kontextu se těžko buduje fungující tým.

### 2. Building an Awsome Team Culture

Jednoduchá rovnice: úžasný tým má skvělou kulturu. Jak ji ale vybudovat? A hlavně udržet? Kromě "komunikačních vzorců úspěšných kultur" :-)  je zajímavé si přečíst, jak formování kultury napomáhají věci jako [issue tracking](//en.wikipedia.org/wiki/Issue_tracking_system), nebo [code review](//en.wikipedia.org/wiki/Code_review) pro každý komit. Plus spousta dalších.

### 3. Every Boat Needs a Captain

Kniha propaguje typ leadera, který slouží svému týmu, tzv. _servant leader_. Jak takový člověk vypadá, je popsáno pomocí leadership antipatternů a patternů.

### 4. Dealing with Poisonous People

Pokud má tým silnou kulturu, může se stát, že se mu "jedovatí" lidé buď úplně vyhnou, nebo se aspoň přizpůsobí natolik, aby byli snesitelní (a kulturu nekazili). Pokud nemáme to štěstí, musíme to nějak řešit (jinak sklouzneme do jednoho z antipatternů z minulé kapitoly). Někdy to přes všechnu snahu "prostě nejde". Nic naplat, jak ví každý zahradník, shnilé ovoce je potřeba vyhodit. Čím dřív, tím líp.

Tohle je jedna ze silných kapitol, která pomůže s řešením těchto nepříjemných --- a nelehkých --- záležitostí. Důležitá je také proto, že připomíná jednu podstatnou věc --- team leadeři se často rekrutují ze seniorních vývojářů, kteří se mnohdy zaměřují na technickou stránku věcí a naopak hodně (až zcela) ignoruj lidskou stránku (členů) týmu.

### 5. The Art of Organizational Manipulation

Manipulace, to zní ošklivě, že jo? Někdo tomu říká politika, někdo sociální inženýrství, autoři tomu říkají organizační manipulace. Váš tým nežije ve vzduchoprázdnu a ať chcete nebo nechcete, jako team leadeři se budete potýkat s prostředím okolo vás. A mimochodem, jedním z vašich úkolů je váš tým odstínit od toho šílenství, které vládne "tam venku".

### 6. Users Are People, Too

To je další věc, kterou vývojáři neradi slyší. Že existují taky nějací uživatelé (nejčastěji zmiňovaní s despektem), kteří používají "náš" software. Nedejbože, abychom s nima museli komunikovat. Pokud se ale na věc podíváme rozumně, může nám to otevřít nové perspektivy na design našeho projektu. Google by mohl vyprávět.

## Závěrečná myšlenka

Četl jsem už několik knih o team leadingu/managementu. Většinou to dopadlo tak, že jsem si cizí zkušeností potvrdil, k čemu jsem došel už předtím intuitivně. Ale pokaždé to bylo inspirativní a umožnilo mi to posunout se o kus dál. Tahle kniha není výjimkou. A pokud zvažujete svoji první knihu o team leadingu, tak tohle určitě nebude špatná volba.

## Související články

* [Management za zavřenými dveřmi](/2012/09/management-za-zavrenymi-dvermi/)
* [(Ne)funkční tým](/2011/11/nefunkcni-tym/)
* [Manažerem humorně a kousavě](/2011/07/manazerem-humorne-a-kousave/)
