+++
title = "Gradle tutorial: tasky"
date = 2013-07-08T21:38:00Z
updated = 2017-06-17T11:53:41Z
description = """V tomto díle Gradle tutoriálu se podíváme, jak se pracuje s tasky."""
tags = ["gradle", "build"]
aliases = [
    "/2013/07/gradle-tutorial-tasky.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2013/08/Gradle.png" >}}

Vítejte u prvního dílu tutorialu o automatizačním nástroji [Gradle](//www.gradle.org/). Filozoficko-marketingovou masáž jsme si odbyli v [minulém článku](/2013/05/gradle-moderni-nastroj-na-automatizaci/), takže je čas si vyhrnout rukávy: let's get our hands dirty!

## 3 informace na úvod

Asi nejdůležitější informací je: co by mělo být přínosem tohoto tutorialu? Gradle má velmi pěknou dokumentaci ([User Guide](//www.gradle.org/docs/current/userguide/userguide.html), [DSL Reference](//www.gradle.org/docs/current/dsl/), [Javadoc](//www.gradle.org/docs/current/javadoc/)/[Groovydoc](//www.gradle.org/docs/current/groovydoc/)), tak proč psát ještě tutorial? Důvody jsou dva. Jednak dokumentace ne vždy pomůže při řešení praktických věcí --- dotazů je plný [Stack Overflow](//stackoverflow.com/questions/tagged/gradle), a jednak některé věci nejsou úplně ve stavu, v jakém bych je chtěl používat. Jako příklad můžu uvést [Jetty](//www.eclipse.org/jetty/): Gradle obsahuje out-of-the-box [Jetty plugin](//www.gradle.org/docs/current/userguide/jetty_plugin.html), který ale používá Jetty ve verzi 6. Což je verze, která je _deprecated_. Pokud tedy chci používat nějakou _stable_ verzi (7-9), musím to udělat jinak. Tutorial by tedy měl řešit praktické věci v aktuálních verzích použitých nástrojů.

Druhou informací je _scope_ tutorialu. Ten by měl odpovídat [mojí přednášce](//www.slideshare.net/VtKotaka/gradle-20028605) na SlideShare, plus něco navíc. Základní zaměření odpovídá mým potřebám, tedy na co Gradle používám já. Pokud byste si rádi přečetli o nějakých dalších tématech, dejte mi vědět v komentářích --- rád tutorial přizpůsobím vašim potřebám. Tutorial by měl zahrnovat hlavně Java vývoj: tj. Java, unit testy, web, metriky kódu, multi projekty, kontinuální integraci, integraci s Antem. Předstupněm závěru by byl jednoduchý, ale reálný projekt --- webová služba na JAX-WS a úplné finále by obstarala [case study](//en.wikipedia.org/wiki/Case_study) (opět reálný projekt).

{{< figure class="floatright" src="/2013/07/Bitbucket.png" width="200" >}}

Poslední informace je praktická. Ke každému dílu budou k dispozici zdrojové kódy build skriptů, které budu postupně publikovat na [Bitbucketu](//bitbucket.org/sw-samuraj/gradle-tutorial). Zdrojové kódy v repozitáři budou obohacené o komentáře, takže budou (doufám) použitelné i samostatně.

Bitbucket je [Mercurial](//mercurial.selenic.com/) (a Git) hosting, který funguje obdobně jako známější [GitHub](//github.com/). Že jsem zvolil Mercurial a ne třeba populárnější Git je, dejme tomu, srdcová záležitost. (A pak, toho Gitu už je všude trochu moc ;-)

## Instalace a verze Gradlu

Instalace Gradlu je jednoduchá ve stylu stáhnout-rozbalit-přidat _bin_ do PATH-spustit. Prerekvizitou je JDK 1.5+. Pro přesný postup vás odkážu na stránky dokumentace: [Installing Gradle](//www.gradle.org/installation). Funkčnost instalace ověříme příkazem `gradle -v`.

{{< figure src="/2013/07/Gradle-version.png"
    caption="Verze Gradlu" >}}

V rámci tutorialu budu používat verzi 1.7 z [nočního buildu](//www.gradle.org/nightly). Předpokládám, že tutorial bude zdrojem informací i v budoucnu, ne jenom v čase publikování, tak si chci podržet iluzi, že takto vydrží být aktuální alespoň o něco déle. Může se sice stát, že některé funkčnosti, které popisuji se mohou ve stabilních releasech chovat trochu jinak (např. výstupy na konzoli), nicméně většina příkladů byla původně vyvinuta pro (stable) verzi 1.5 a ve verzi z nočního buildu funguje bez úprav, takže nepředpokládám problémy :-)

Pokud byste u příkladů narazili na nekompatibilitu mezi verzemi, dejte mi, prosím, vědět v komentářích.

## Projekty a tasky

Dva základní koncepty, na kterých jsou Gradle build skripty postaveny jsou [projekt](//www.gradle.org/docs/current/dsl/org.gradle.api.Project.html) a [task](//www.gradle.org/docs/current/dsl/org.gradle.api.Task.html). Koncept _projektu_ by nám měl být familiární, např. z IDE. Zjednodušeně, _projekt_ představuje nějaký komponent, který potřebujeme sestavit: JAR/WAR/EAR/ZIP archiv apod. _Projekt_ je prezentován souborem `build.gradle` v root adresáři projektu.

{{< figure src="/2013/07/Gradle-project.png"
    caption="Definice Gradle projektu (projekt _00_HelloWorld_)" >}}

_Projekt_ definuje související množinu _tasků_. _Task_ je atomická jednotka práce, kterou můžeme spustit v rámci buildu. Každý task může obsahovat množinu [akcí](//www.gradle.org/docs/current/javadoc/org/gradle/api/Action.html). Akce už je nějaká konrétní věc, kterou chceme provést: výpis na konzoli, přesun souboru, kompilace, spuštění jiného _tasku_ atd.

## Hello, world!

Nevím, jak vy, ale já když se učím něco nového (jazyk, framework, nástroj), tak si vždycky rád napíšu [Hello world](//en.wikipedia.org/wiki/Hello_world_program). Jak vypadá Hello world v Gradlu?

```groovy
task hello << {
    println 'Hello, Gradle!'
}
```

{{< figure src="/2013/07/Gradle-Hello-world.png"
    caption="Výstup tasku `hello`" >}}

Výstup je možná trochu ukecaný --- kromě výpisu našeho textu je tam label daného tasku (`:hello`) a informace o úspěšnosti vykonání tasku. Pokud chceme kompaktnější výstup, můžeme spustit task s přepínačem `-q`. Gradle pak vypisuje (ve skutečnosti loguje) pouze to, co jsme poslali na standardní výstup (`println`) a zprávy se severitou `error`.

```bash
gradle hello -q
```

Zrojový kód na Bitbucketu: [00_HelloWorld/build.gradle](//bitbucket.org/sw-samuraj/gradle-tutorial/src/7e25dd4c1f52/00_HelloWorld/build.gradle?at=published)

## Jaké tasky jsou k dispozici?

Nejčastějším uživatelem Gradlu bude vývojář. Ale může se taky stát, že ho bude spouštět tester, nebo analytik, nebo... (nedejbože ;-) projekťák, či slečna asistentka. Pamatujete? Automatizace.

Tyto ostatní role se asi nebudou chtít vrtat v kódu build skriptu, aby zjistily, co že to má dělat apod. Jako dělaný speciálně pro ně je příkaz `gradle tasks`, který vypíše seznam tasků, které jsou k dispozici.

{{< figure src="/2013/07/Gradle-tasks.png" caption="Výpis tasků" >}}

V závislosti na verzi Gradlu se výpis může lišit. Pokud nevidíte nějaký task, který byste vidět měli, zkuste příkaz spustit s přepínačem `--all`:

```bash
gradle tasks --all
```

## Gradle GUI

Graficky orientovaní jedinci možná ocení práci s grafickou verzí Gradlu, která se spouští příkazem `gradle --gui`. Zde je také vidět přehled tasků. Ty se navíc dají rovnou spouštět, je vidět jejich výpis na konzoli atd.

{{< figure src="/2013/07/Gradle-gui.png" caption="Gradle GUI" >}}

## Syntaxe tasku

Nejčastěji se budeme setkávat s definicí tasku, která byla uvedena v příkladu _Hello world_, čili:

```groovy
task myList << {
    println 'middle point'
}
```

Na této syntaxi je nejzajímavější operátor `&lt;&lt;`, který je aliasem pro metodu [doLast](//www.gradle.org/docs/current/dsl/org.gradle.api.Task.html#org.gradle.api.Task:doLast(groovy.lang.Closure)). Stejný výsledek dostaneme zápisem:

```groovy
task myList {
    doLast {
        println 'middle point'
    }
}
```

Když máme metodu `doLast`, tak nepřekvapí, že máme i obdobnou [doFirst](//www.gradle.org/docs/current/dsl/org.gradle.api.Task.html#org.gradle.api.Task:doFirst(groovy.lang.Closure)). K čemu tyto metody slouží? Umožňují nám "dekorovat" již definovaný task --- metoda `doFirst` přidává nové akce na začátek seznamu akcí a metoda `doLast` dělá totéž na konci seznamu akcí daného tasku. Každý task tedy můžeme postupně rovíjet a obohacovat jeho chování. V jednoduchém build skriptu to asi nebude potřeba, ale v hierarchii skriptů (multi project) už to může být zajímavé.

```groovy
task myList << {
    println 'middle point'
}

myList {
    doLast {
        println 'last point'
    }
}

myList {
    doFirst {
        println 'first point'
    }
}
```

{{< figure src="/2013/07/Gradle-doFirstLast.png" caption="Výstup \"dekorovaného\" tasku `myList`" >}}

Zdrojový kód na Bitbucketu: [01_Tasks/build.gradle](//bitbucket.org/sw-samuraj/gradle-tutorial/src/7e25dd4c1f52/01_Tasks/build.gradle?at=published)

## Zdrojové kódy

Zdrojové kódy k dnešnímu dílu jsou k dispozici na [Bitbucketu](//bitbucket.org/sw-samuraj/gradle-tutorial). Můžete si je tam buď [probrouzdat](//bitbucket.org/sw-samuraj/gradle-tutorial/src/50fe576f418e78c0f351cbdb8efb7216121016da/?at=published), stáhnout jako [ZIP archiv](//bitbucket.org/sw-samuraj/gradle-tutorial/get/published.zip), anebo --- pokud jste cool hakeři jako já :-) --- naklonovat [Mercurialem](//mercurial.selenic.com/):

```bash
hg clone ssh://hg@bitbucket.org/sw-samuraj/gradle-tutorial
```

## Co nás čeká příště?

Protože tasky jsou pro Gradle stěžejním konceptem, budu se jim věnovat i v příštím díle. Tři hlavní okruhy by měly být:

* závislosti mezi tasky,
* dynamické generování tasků --- task rules
* a "běžné" problémy, se kterými se můžeme při definici tasků setkat.

_Tento článek byl původně publikován na serveru [Zdroják](//www.zdrojak.cz/)._

## Související články

* [Gradle, moderní nástroj na automatizaci](/2013/05/gradle-moderni-nastroj-na-automatizaci/)

## Související externí články

* [Build Script Basics](//www.gradle.org/docs/current/userguide/tutorial_using_tasks.html) (Gradle User Guide)
* [More about Tasks](//www.gradle.org/docs/current/userguide/more_about_tasks.html) (Gradle User Guide)
* [Task](//www.gradle.org/docs/current/dsl/org.gradle.api.Task.html) (Gradle DSL Reference)
