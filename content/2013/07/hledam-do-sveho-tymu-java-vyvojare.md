+++
title = "Hledám do svého týmu Java vývojáře"
date = 2013-07-22T23:07:00Z
updated = 2017-08-31T11:40:05Z
description = """Rozhodl jsem se "jít tomu štěstíčku trochu naproti" a
    publikovat zde pracovní inzerát. Proč? Protože hledám lidi k sobě
    do týmu. Bude to lepší, než čekat, koho mi najde HR oddělení, nebo
    pošle nějaká agentura. Zkrátka, vytvářím si vlastní příležitost."""
tags = ["teamleading", "interview", "java"]
aliases = [
    "/2013/07/hledam-do-sveho-tymu-java-vyvojare.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/img/logos/Java.png" >}}

[UPDATE] _Ačkoliv by to mělo být zřejmé z data článku (2013), pro jistotu to v úvodu zdůrazním: **tento inzerát již není platný**. Nechávám jej zde z historických a studijních 😉 důvodů._ [/UPDATE]

Dnešní post bude jiný, než jste na tomto blogu zvyklí --- rozhodl jsem se "jít tomu štěstíčku trochu naproti" a publikovat zde pracovní inzerát. Proč? Protože _hledám lidi k sobě do týmu_. A myslím, že pokud to někoho osloví, bude to lepší, než čekat, koho mi najde HR oddělení, nebo pošle nějaká agentura. Zkrátka, vytvářím si vlastní příležitost.

Tenhle post se bude lišit ještě v jednom ohledu. Když píšu, snažím se vždy maximálně odstínit od svého zaměstnavatele, nebo projektu, kde právě pracuji. Dnes budu naopak velmi konkrétní, nebo chete-li upřímný (v rámci možností :-)

## The Company

Pracuji ve společnosti [Gemalto](//www.gemalto.com/). Už z webu můžete poznat, že jde o korporaci. Ano, je to tak --- celosvětově máme 11.000+ zaměstnanců, head quarter sídlí ve Francii. Gemalto se zabývá bezpečností a jednotlivé divize pokrývají oblasti finančních služeb, telco, identity a goverment.

V Praze [na Brumlovce](//www.google.cz/search?q=%C5%BDeletavsk%C3%A1+1448%2F7.+Praha+4%2C+140+00) má Gemalto vývojové centrum, zvící cca 300 zaměstnanců, které reflektuje uvedené divize a které má na starosti dodávání SW části našich řešení. Já jsem jedním z koleček v divizi governance, která má na starosti klienty z oblasti [EMEA](//en.wikipedia.org/wiki/Europe,_the_Middle_East_and_Africa) --- našimi zákazníky jsou vlády z oblasti Evropy, Blízkého východu a Afriky (můžu vás uklidnit, pro českou vládu nic neděláme :-)

{{< figure class="floatright" src="/2013/07/logo-gemalto.png" width="200"
    link="/2013/07/logo-gemalto.png" >}}

Naše vývojové oddělení má kolem 45 lidí (vývojáři a testeři) a chceme se rozrůst o cca 10 dalších vývojářů (= můj tým). Řekl bych, že nabírání lidí je v našem případě pozitivním signálem --- práce je dost a ještě nějaký čas bude. Mmch. pražská pobočka Gemalta během tzv. "krize" neustále rostla, takže to může naznačit jistou perspektivnost našeho segementu.

Gemalto je společností mezinárodní nejenom strukturou, ale i obsahem --- v našem oddělení je cca 2/3 Čechů a Slováků, zbytek tvoří cizinci posbíraní různě po Evropě. Asi nepřekvapí, že nejsilnější "menšinou" jsou Francouzi. Nicméně oficiálním komunikačním jazykem je _angličtina_.

## Projects

To co dodáváme (teď už mluvím o našem oddělení) našim zákazníkům --- vládám --- jsou elektronické dokumenty: pasy, ID karty (občanky), řidičáky, povolení k pobytu (resident permit), volební průkazy (doména Afriky) apod.

Dodávkou může být komplexní řešení, které umožní dané vládě si kompletně produkovat daný typ dokumentu, nebo jenom jeho část. Záleží projekt od projektu. Pokud bych měl načrtnout dodávané řešení pomocí tří kostiček, vypadalo by takto:

{{< figure src="/2013/07/Coesys-3-cubes-design.png" >}}

Kostičky _Enrolment_ a _Quality Center_ nás nemusí zajímat, ;-)  protože jsou napsaný v .NETu. Jen pro představu, _Enrolemnt _je o "narolování" dat, které budou umístěny na výsledném dokumentu. Může jít o napojení na nějaký národní registr, nebo např. fyzické snímání otisků prstů. _Quality Center_ je, hm, kontrola kvality. Takže třeba kontrola, že to, co je vytištěno na dokumentu je také zapsáno na čipu.

Nenechte se zmást, že na Javu zbyla jenom kostička _Issuance_. Javisté tvoří 2/3-3/4 našeho týmu, takže se jedná o docela rozsáhlé řešení, které se většinou skládá z několika aplikací. A co kostička _Issuance_ řeší? Má na starosti věci jako validaci a preprocesing dat, různé kryptografické záležitosti (třeba [PKI](//en.wikipedia.org/wiki/Public-key_infrastructure)) a hlavně správu a samotnou produkci dokumentů.

Jak jsou projekty veliké? Jejich délka se pohybuje od 6 měsíců do 2 let a podílí se na nich od 3 do 20 lidí (všech rolí). To jsou nějaké mezní hodnoty, takže většina projektů se pohybuje někde mezi nimi a má "rozumnou" velikost.

Podstatná informace je, jak jsou projekty řízeny. Vzhledem k tomu, že jsme korporace a pracujeme pro vlády, je to jasné --- je to čistokrevný vodopád. Interně si ale bereme z agilních metodik to použitelné, takže například děláme code a design review, máme kontinuální integraci apod. Míra se projekt od projektu liší. Osobně si myslím, že je to lepší přístup, než jsem zažil za poslední tři roky v bankingu --- lhaní si do vlastní kapsy, že "to děláme agilně", aby se pak projekty běžně zpožďovaly o rok a víc (true stories). To se nám nestává.

Zatím nezaručenou informací a příslibem do budoucna je, že bych chtěl pokračovat [v používání Kanbanu](//www.sw-samuraj.cz/2013/01/kanban-zpravy-z-fronty.html). Tohle zatím nemůžu slíbit, ale budu se snažit, abychom to opravdu používali a nebylo to jen [plácnutí do vody](//blog.krecan.net/2012/06/11/korporatni-hry-2-vyzadej-ticket-placnuti-do-vody/). Kanban má oproti třeba Scrumu tu výhodu, že není v protikladu k vodopádu a do korporátního prostředí velmi dobře zapadne.

Poslední důležitá informace --- naši vývojáři cestujou. Služební cesty jsou krátkodobé, většinou je to tak kolem dvou týdnů na začátku projektu (sbírání požadavků, workshopy apod.) a obdobně na konci projektu (instalace, integrace, školení apod.). Takže pokud se chcete podívat do zahraničí, kam byste nejspíš na dovolenou nejeli (třeba Saudská Arábie, nebo do Irska), tak u nás máte možnost. Samozřejmě, že respektujeme vaše preference, takže pokud třeba máte malé děti, vždy se to dá individuálně nastavit.

## Technologies

Konečně se dostávám k tomu, co je na tomhle článku asi nejzajímavější. Jaké technologie tady používáme? Náš hlavní technologický stack je postavený nad Java EE. Pokud je to podstatné, uvádím v následujícím přehledu verzi komponenty, ta v závorce je pro starší projekty, ta před závorkou pro nové projekty.

{{< figure class="floatright" src="/2013/07/JBoss7.png" >}}

* [JBoss AS 7](//www.jboss.org/jbossas) (5) jako aplikační server,
* [Vaadin](//vaadin.com/home) jako frontend,
* EJB 3.1 (3.0) a CDI pro business logiku,
* JPA 2.0 (1.0)/Hibernate pro perzistenci,
* JMS/[HornetQ](//www.jboss.org/hornetq) pro messaging,
* JAX-WS pro SOAP webové služby.

Kromě těchto "mainstream" technologií se u nás můžete setkat minoritně s [Grails](//grails.org/) a ještě minoritněji se [Springem](//www.springsource.org/). A pak s celou kupou dalších, většinu open source, věcí, které jsou specifické per projekt. Z hlavy mě napadá třeba [JasperReports](//community.jaspersoft.com/project/jasperreports-library), nebo [Dozer](//dozer.sourceforge.net/).

Buildujeme _Mavenem_ (rád bych zkusil prosadit upgrade na [Gradle](//www.gradle.org/)). Na verzování zdrojáků používáme [Mercurial](//mercurial.selenic.com/), takže pokud chcete zkusit něco stejně skvělého, jako je Git, ale [uživatelsky kompaktnější](//geekandpoke.typepad.com/geekandpoke/2012/07/simply-explained-2.html) ;-)  tak to bude příjemná zkušenost. Na kontinuální integraci používáme [Jenkins](//jenkins-ci.org/) a na metriky [Sonar(Quebe)](//www.sonarqube.org/). Issue tracking [Redmine](//www.redmine.org/).

Java IDE je na vás, běžná je tady svatá trojice [E](//eclipse.org/)/[I](//www.jetbrains.com/idea/)/[N](//netbeans.org/). Kupodivu, dost lidí tady dělá v NetBeans (asi to sem dotáhli přebehlíci ze Sunu ;-)  Pokud se ukáže, že to s námi myslíte vážně, rádi vám koupíme Ideu. Největší borci samozřejmě dělají ve [Vimu](//www.vim.org/) (no dobře, to je vtip... ve Vimu dělám jenom já).

## Pros


Tak v první řadě, budete dělat s nejlepším team leaderem široko daleko. Jsem skromný a myslím to vážně.

Doména ve které Gemalto podniká je hodně zajímavá. Pokud vás nudí telco nebo banking, zkuste securitu! Když se tak vyprofilujete, může být kryptografie vaším dením chlebem. A i  pokud ne, tak security záležitosti tady budete potkávat daleko častěji než v jakékoliv jiné doméně.

O krátkodobém cestování už jsem mluvil. Co je opravdu zajímavé, Gemalto poskytuje tzv. stáže --- můžete na dva tři roky vycestovat do zahraničí a pracovat v jiné pobočce někde ve světě (tahle možnost přesahuje výše zmíněnou oblast EMEA). Gemalto vám při tom pomůže s relokací.

Jako (skutečný, ne pouze softwarový) polyglot bych ze standardních benefitů vypíchnul výuku jazyka _v pracovní době_ --- angličtina nebo francouzština.

Věc, která se obtížně popisuje, ale já ji považuji za velmi cennou. V našem oddělení panuje důvěra a respekt a pracují tady fajn lidé. Vůbec, lidi, které jsem potkal na přijímacím pohovoru (kolega team leader a můj šéf) jsou jedním z důvodů, proč jsem se rozhodl pro práci tady. A během další spolupráce jsem si to jen potvrdil.

## Cons

Co vám mám povídat? Je to korporace. Máme tady procesy, komunikační šumy a některý věci jsou zkrátka na dlouho.

Také některé věci/procesy ještě nejsou nastavený, nebo úplně ideální. Do značné míry je to dáno rychlostí, jakou pražská pobočka v uplynulých letech vyrostla --- dělaly se projekty a na procesy/metodiky/guidelines nebyl čas. Jde o věci jako třeba _jednotný_ issue tracking. Nebo větší míra automatizace. Jsou to věci, které bych rád prosadil pomocí týmové kultury.

## Who?

Momentálně hledáme seniornější vývojáře. Pokud jste absolvent, musím vás zklamat. Pokud jste junior, "máte jiskru v oku" a jste opravdu dobrý (= přesvědčíte mě na pohovoru), máte šanci.

Důležitá informace je, že bereme pouze zaměstnance. Kontraktory nám firemní politika zapovídá.

## How much?

Otázka peněz je delikátní. Tak pojďme do toho. Jsme názoru, že každý by měl znát svou cenu. Bavíme se o rolích Java vývojáře a technical leadera. Takže pokud nepřešvihnete naše stropy pro tyto role, tak dostanete, co si řeknete. A říct si můžete, vzhledem k vaší senioritě, standardní plat Java vývojáře v Praze. Na rovinu říkám, že vás nebudeme přeplácet. Ale nebudeme vás ani vykořisťovat :-)  A jinak, mám velmi přesnou představu, co byste v rámci vaší seniority měli umět.

## Interview

Přijímací pohovor má tři kola. Prvně bude _phone screen_, cca 30 min. Následující technické kolo budete dělat se mnou a bude probíhat formou, kterou [jsem už popisoval](//www.sw-samuraj.cz/2012/11/jak-delam-java-pohovor.html). Změnily se tam dvě věci (časem o tom napíšu) --- jednak už nedávám hlavolam a pak, část _podíváme se na můj kód_ je mnohem "praktičtější" ;-)

Poslední kolo je pohovor s mým šéfem a s někým s HR.

## Conclusion

Nabízím vám slušnou práci, za slušné peníze. U mne v týmu. Není to práce pro každého --- pro někoho je show-stopper, že to je enterprise, pro někoho absence Springu, pro někoho...

Ale pokud jste s výše uvedeným OK a vzájemně si sedneme (vy mně jako vývojář a my vám jako firma), myslím, že by nás to mělo společně hodně bavit.

Pokud to chcete zkusit, můžete začít tím, že pošlete na můj firemní email svoje CV v angličtině: [vit.kotacka@gemalto.com](mailto:vit.kotacka@gemalto.com)

Pokud by vás něco zajímalo, zeptejte se v komentářích, nebo mi napište na [Twitter](//twitter.com/sw_samuraj), či [LinkedIn](//cz.linkedin.com/in/vitkotacka/).

## Official Ad

Pokud přesto všechno, co jsem napsal, chcete mrknout na oficiální inzerát, nebo okouknout takové ty standardní firemní benefity:

* [Java Developer at Gemalto](//www.java.cz/article/sw-developer-java-j2ee-ics-dc-7)
* [Technical Leader at Gemalto](//www.java.cz/article/sw-technical-leader-developer-java-j2ee-ics-dc-6)
