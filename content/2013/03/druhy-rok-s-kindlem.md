+++
title = "Druhý rok s Kindlem"
date = 2013-03-03T23:07:00Z
updated = 2013-08-17T22:45:38Z
description = """Tak jsem strávil druhý rok s Kindlem. Jaké odborné
    knihy mojí čtečkou protekly? Bylo jich méně, než loni, ale to
    nevadí - důležité je nepřestávat číst."""
tags = ["knihy"]
aliases = [
    "/2013/03/druhy-rok-s-kindlem.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2014/03/Kindle-Touch.jpg" >}}

Tak už jsou tomu dva roky, co jsem si koupil [Kindle](//www.amazon.com/Kindle-Ereader-ebook-reader/dp/B007HCCNJU). A [tak jako loni](/2012/03/rok-s-kindlem/), i dnes si udělám malou inventuru, co vše mi z odborné četby prosvištělo čtečkou. Už pouhým okem je vidět, že jsem toho přečetl sotva polovinu oproti loňsku. Můžou za to tři faktory:

* Narodilo se nám druhé dítě. Jak jsem četl někde na (českém) internetu: _Jedno dítě, žádný dítě. Dvě děti, moc dětí_. Je to pravda.
* Cca na polovinu se mi zkrátila doba cesty do práce. A odborné věci čtu převážně v MHD.
* Koupil jsem si PlayStation a jako fanoušek série [The Elder Scrolls](//en.wikipedia.org/wiki/The_Elder_Scrolls) jsem nemohl aktuální počin [Skyrim](//www.elderscrolls.com/skyrim/) nechat ležet ladem.

Očekávám, že příští rok budu mít na čtení zase více času, protože 1) s dětmi už jsme se časově jakž takž sžili, 2) od května mi cesta do práce bude trvat o dost dýl a konečně 3) Skyrim jsem už prochodil skrz naskrz, takže se budu moci věnovat produktivnějšímu odpočinku.

## Kindle, Kindle, Kindle...

Co bych tak řekl o Kindlu samotném? Je to fantastická čtečka a já, jako nelítostný čtenář, se jí ani po dvou letech nedokážu nabažit. Je tak skvělá, že já, Linuxák, jsem se rád a dobrovolně upsal Amazonímu [DRM](//en.wikipedia.org/wiki/Digital_rights_management). Nicméně kupuju i DRM-free knihy, zejména z produkce [Pragmatic Programmer](//pragprog.com/) a [Manning](//manning.com/).

Když už jsme u kupování --- _všechny_ knížky kupuju, nebo si nechám koupit od zaměstnavatele. Čas od času se mě totiž někdo, jako vyhlášeného čtenáře, ptá, kde se dají sehnat knížky zadarmo. Odborné legálně téměř nikde. Peníze, které utratím za odborné knížky, beru jako investici do svého vzdělání. A můžu potvrdit, že se to bohatě vrátí.

No a na jakém stroji náš hrdina své dechberoucí, ekvilibristické, čtenářské kousky předvádí? Začínal jsem s modelem _Kindle 3_ (dnes _Keyboard_), který se ale chvilku po skončení (americké roční) záruky odebral do křemíkového nebe --- nepřežil cestu v batůžky mé dcery. Obratem jsem zakoupil aktuální novinku _Kindle Touch_ (už se neprodává), který mi zatím věrně slouží.

Po cca 3/4 roku s Touchem nechápu, jak jsem mohl být nadšený z nedotykového Kindlu. Nicméně pokud pominu uživatelsky ne úplně povedený _Kindle 4_ (dárek pro manželku), musím říct, že už leta jsem se nesetkal s něčím tak ergonomickým, jako je (libovolný) Kindle. Všechna čest inženýrům ze Seattlu.

Tak a teď konečně ten seznam:

## SOA

{{< figure src="/2013/03/Kindle-2-SOA-books.png"
    link="/2013/03/Kindle-2-SOA-books.png" >}}

* **[SOA Patterns](//www.manning.com/rotem/)** --- kniha se SOA sice koliduje, ale jde převážně o vzory z oblasti distribuovaných systémů. Ale jelikož u [Manningu](//manning.com/) málokdy vyjde slabý počin, stojí i tato kniha za přečtení.
* **[SOA Governance in Action](//www.manning.com/dirksen/)** --- další počin od Manningu a tentokrát trefa do černého. Výborný úvod do tématu [SOA Governance](//en.wikipedia.org/wiki/SOA_governance). Kniha uvádí do kontextu a nabízí okamžitě implementovatelné postupy governance, jako je verzování, lifecycle služeb, security, [BAM](//en.wikipedia.org/wiki/Business_activity_monitoring) atd.
* **[Oracle SOA Suite 11g Handbook](//www.amazon.com/Oracle-Suite-Handbook-Press-ebook/dp/B0043239Y0)** --- 800stránková bichle na téma "co všechno jste chtěli vědět o SOA Suite, báli jste se zeptat a na školení vám to neřekli". Knihu rozhodně doporučuji jako kvalitní zdroj k danému tématu.
* **[Oracle SOA Infrastructure Implementation Certification Handbook](//www.packtpub.com/oracle-soa-infrastructure-implementation-certification-handbook/book)** --- certifikační guide. Celkem slušný úvod do SOA Suite pro někoho, kdo vůbec neví o co jde. Plus sada testovacích otázek a odpovědí ke každému tématu/technologii.

## Kanban & Management

{{< figure src="/2013/03/Kindle-2-Kanban-books.png"
    link="/2013/03/Kindle-2-Kanban-books.png" >}}

* **[Kanban](//www.amazon.com/Kanban-ebook/dp/B0057H2M70)** --- ultimátní kniha o [Kanbanu](//en.wikipedia.org/wiki/Kanban_(development)). Pokud si chcete Kanban nastudovat opravdu do hloubky, tohle je ten pravý opus.
* **[Kanban for Skeptics](//leanpub.com/kanbanforskeptics)** --- pokud si chcete přečíst knihu o Kanbanu  a utratit za ni 0-5 $, tak tahle není špatná.
* **[Lean from the Trenches: Managing Large-Scale Projects with Kanban](//pragprog.com/book/hklean/lean-from-the-trenches)** --- skvělá kniha o implementaci Kanbanu a jeho propojení se Scrumem. Nedoporučuji ji jen já, ale třeba taky [Mary Poppendieck](//www.poppendieck.com/reference.htm).
* **[Behind Closed Doors: Secrets of Great Management](//pragprog.com/book/rdbcd/behind-closed-doors)** --- pokud na vás spadnou manažerské povinnosti a moc o tom nevíte (nebo to děláte špatně ;-) tak tohle není špatný počin.
* **[Technical Blogging](//pragprog.com/book/actb/technical-blogging)** --- kniha, která vás provede vším, co obnáší blogování na světové úrovni. A ještě na tom vyděláte (i když ne třeba peníze). Zajímavé jsou kapitoly o sociálních sítích.

Pokud bych měl uvedený seznam nějak zhodnotit, tak v podstatě všechny knihy ze seznamu byly _project-driven_, takže hlavně SOA a Kanban. Z výše uvedených časových důvodů jsem se nedostal k mnoha dalším tématům, která mne prostě zajímají, ale na projektu je aktuálně nevyužiju; což doufám, že se do budoucna zlepší.

A co vy? Jak jste na tom se čtením? Máte nějaké dobré tipy? Budu rád, když se o ně podělíte v komentářích.

### Související články

* [Rok s Kindlem](/2012/03/rok-s-kindlem/)
* [SOA Patterns, kniha](/2012/12/soa-patterns-kniha/)
* [Technologické blogování](/2012/11/technologicke-blogovani/)
* [Management za zavřenými dveřmi](/2012/09/management-za-zavrenymi-dvermi/)
* [Lean ze zákopů](/2012/08/lean-ze-zakopu/)
* [Kanban, ultimátní kniha](/2012/08/kanban-ultimatni-kniha/)
