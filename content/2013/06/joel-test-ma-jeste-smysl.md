+++
title = "Joel test, má ještě smysl?"
date = 2013-06-29T21:59:00Z
updated = 2013-06-29T21:59:56Z
description = """Jako vývojáři jste se možná už někde setkali s Joelovým testem.
    Když jsem na něj cca před osmi lety narazil, bylo to pro mne jako zjevení.
    A pracoval jsem ve společnosti, jejíž skóre v tomto testu bylo... ehm, nula."""
tags = ["knihy", "sw-engineering"]
aliases = [
    "/2013/06/joel-test-ma-jeste-smysl.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure src="/2013/06/Joel-test.png" class="floatleft" >}}

Jste vývojáři? Pak už jste se možná někde setkali s [Joelovým testem](//www.joelonsoftware.com/articles/fog0000000043.html). Možná jste to zaslechli někde na internetu, možná se vás na to ptali na pohovoru (nebo vy jich) a možná jste to dokonce sami ve firmě zaváděli.

Joelův test je skvělý. Když jsem na něj cca před osmi lety narazil, bylo to pro mne jako zjevení. Měl jsem tehdy tři, čtyři roky zkušeností s PHP a začal jsem pronikat do enterprise Javy. A pracoval jsem ve společnosti, jejíž skóre v tomto testu bylo... ehm, nula.

## The Joel Test

1. Do you use source control?
1. Can you make a build in one step?
1. Do you make daily builds?
1. Do you have a bug database?
1. Do you fix bugs before writing new code?
1. Do you have an up-to-date schedule?
1. Do you have a spec?
1. Do programmers have quiet working conditions?
1. Do you use the best tools money can buy?
1. Do you have testers?
1. Do new candidates write code during their interview?
1. Do you do hallway usability testing?

## Expozice

Jak říkám, bylo to pro mne zjevení. A výzva. Ze své pozice jsem toho nemohl _formálně_ mnoho ovlivnit, nicméně se mi podařilo to skóre zvednout na 3. První, co jsem udělal, nechal jsem vytvořit virtuální server, rozchodil na něm Subversion repozitory, všechny své projekty jsem tam naimportoval a začal SVN denně používat. Hned jsem se cítil o moc dospělejší :-)

OK, takže máme Joelův test --- stačí si to odškrtat, co ještě nemáme, tak zavedeme a hned je z nás světová firma. Asi tušíte, že mezi odškrtnutím položky a každodenní praxí může být nebetyčný rozdíl. Já bych ale chtěl poukázat ještě na jednu věc.

Joelův test je minimálně 13 let starý --- Joel ho publikoval v roce 2000. Nevím, jak to připadá vám, ale já bych řekl, že za tu dobu se svět docela změnil. Já jsem se změnil. Joel se změnil (už nemá tolik vlasů, co míval). Ten test by se taky měl změnit.

Dnes se běžně setkávám s tím, že společnosti mají v testu kolem deseti bodů. Dvanáctku jsem nikdy nepotkal. A i kdyby --- už jste v softvarovém vývoji viděli něco, co by nepotřebovalo zlepšit? Většinou toho bývá docela dost.

Nemám ambice, napsat novou verzi Joelova testu. Spíš bych se nad ním chtěl zamyslet. Bude to takový rozpracovaný materiál --- rozvrtám to a třeba se to někam vyvine.

## Co mi na testu vadí

{{< figure class="floatright"
    src="//4.bp.blogspot.com/-8mVXYCLVSzo/Uc82ixvyW5I/AAAAAAAAD9U/9Nqj2MIkoO0/s320/Joel.jpg"
    link="//4.bp.blogspot.com/-8mVXYCLVSzo/Uc82ixvyW5I/AAAAAAAAD9U/9Nqj2MIkoO0/s380/Joel.jpg"
>}}

[Joel Spolsky](//www.joelonsoftware.com/AboutMe.html) je určitě autorita, které stojí za to naslouchat. Ale není nutné se vším souhlasit. Podobné je to i s jeho testem --- ten prostě vyjadřuje (tehdejší) Joelovy preference (jak to dělat), priority (co dělat) a potřeby (ne všechny body jsou možné, nebo vhodné v každé doméně). Je docela možné, že Joel žije ve světě, který je vaší realitě velmi vzdálen: [5 Reasons Why Joel's Business (Probably) Isn't Like Yours](//listeningtoreason.blogspot.cz/2006/09/5-reasons-why-joels-business-probably.html).

Tím chci jen říct, že ten test je potřeba brát s rezervou. A pokud už se s tím někde veřejně chlubím, třeba firma, měl by to být jen střípek do mozaiky. Když mi někdo řekne, že mají v testu 12 bodů, tak mě nezajímá, _že to mají_, ale _jak to dělají_ a hlavně _kam to chtějí dál rozvíjet_. Uvedu příklad.

_Do you use source control?_ No, znáte někoho, kdo nepoužívá v softwarovém vývoji [VCS](//en.wikipedia.org/wiki/Revision_control)? Je možné, že někdo takový existuje, ale mě to přijde podobný "standard", jako když má dnes každý mobilní telefon. Co mě bude bude u VCS zajímat je, co mají navíc --- mají nějaké metodiky/guidelines (pro branchování, mergování, tagování, komentování atd.), nějaké propojení na další nástroje? Chápu, že se někdo cítí jako šampion, že používají _Git_. Ale že by mi z toho spadla čelist? Asi ne :-/

## Kde zůstal agilní vývoj?

Joel svůj test publikoval v létě 2000. Půl roku poté byl publikován jiný dokument, který měl nesrovnatelně větší vliv na celou následující dekádu. A je platný podnes --- [Agile Manifesto](//agilemanifesto.org/). Věřím :-) že všichni čtenáři ví, co tím myslím. A jen pro jistotu --- agilní vývoj existoval dávno před manifestem (např. _Scrum Methodology_ byla publikována v roce 1995).

Zdá se mi to, nebo v Joelově testu mnoho z agilních technik není? Při troše dobré vůle se některé body dají interpretovat agilně. Třeba _Do you fix bugs before writing new code?_ Ale když si přečtete Joelovu argumentaci, tak zjistíte, že se opírá o zkušenosti a manažerská rozhodnutí v _Microsoftu_ při psaní _Wordu_.

Očekával bych, že něco z agilního vývoje by se mělo do testu promítnout. A když už ne v době jeho vzniku, tak alespoň později, kdy už byly jednotlivé techniky široce adoptovány.

## Je to jediný test k dispozici?

{{< tweet 337584162196316160 >}}

Joel má štěstí, že je slavný a tak jeho test zná (a uctívá ;-) hodně lidí. Ale jsou i jiné testy. Například když jsme o tomhle tématu disputovali na Twitteru s [Banterem](//blog.zvestov.cz/), tak mě odkázal na [The Rands Test](//www.randsinrepose.com/archives/2011/10/11/the_rands_test.html).

[Randse mám rád](/2011/07/manazerem-humorne-a-kousave/) a jeho test má taky něco do sebe. I když je o něčem jiném. Ale pro SW engineera je stejně podstatný, jako ten Joelův.

{{< figure class="floatright"
    src="//1.bp.blogspot.com/-5lI2bEyvIDE/Uc7-QbgaPaI/AAAAAAAAD9E/1D1ucSsHb3s/s200/leadinglean.jpg"
    link="//1.bp.blogspot.com/-5lI2bEyvIDE/Uc7-QbgaPaI/AAAAAAAAD9E/1D1ucSsHb3s/s500/leadinglean.jpg"
>}}

Jiným příkladem obdobného testu je ten uvedný v knize [Leading Lean Sotfware Development](//www.amazon.com/Leading-Lean-Software-Development-ebook/dp/B002Y1U7VU). Mary Poppendieck v něm radí, ať si jednotlivé "disciplíny" obodujete od 0 do 5. 0 znamená, že jste o ní nikdy neslyšeli a 5, že o ní můžete přednášet. Pokud máte z nějaké disciplíny 3 a méně, měli byste se zaměřit na to, jak se v ní zlepšit.

1. Coding standards (for code clarity)
1. Design/code reviews
1. Configuration/version management
1. One-click build (private and public)
1. Continuous integration
1. Automated unit tests
1. Automated acceptance tests
1. _Stop_ if the tests don't pass
1. System testing with each iteration
1. Stress testing (application- and system-level)
1. Automated release/install packaging
1. Escaped defect analysis and feedback

Mary má svůj test zasazený do daleko širšího a hlubšího kontextu --- test se nachází v kapitole _Technical Excellence_, což je jedna ze složek [Lean Developmentu](//en.wikipedia.org/wiki/Lean_software_development).

Kdyby člověk hledal, určitě najde nějaké další testy. Uvedl jsem jen ty, na které jsem narazil, aniž bych je nějak hledal. Pokud znáte nějaké zajímavé exempláře, budu rád, když je zmíníte v komentářích.

## Joel test, má ještě smysl?

Má, samozřejmě, že má. Jenom je potřeba si uvědomit, že dobrý výsledek v něm není známkou nějaké výjimečnosti. Z mého pohledu je to (v dnešní době) jen checklist, že firma/projekt dosahuje v našem oboru _běžného standardu_.

Joelův test také může být inspirací, pokud si chci podobný test vytvořit, s přihlédnutím ke svým potřebám (technologická/business doména, používané/zamýšlené nástroje a metodiky atd.). Určitě je to dobrý základ, na kterém se dá stavět.

Mám-li zhodnotit Joelův přínos historii :-) tak spíš než za tento test, jsem mu vděčný, že založil [Stack Overflow](//stackoverflow.com/). Jeho test je výrazným počinem v historii našeho oboru. Ale jak říká _Roy Batty_:

> _"All those moments will be lost in time, like tears in rain._

{{< youtube _JjJzMBGUwo >}}

## Související externí články

* [The Joel Test: 12 Steps to Better Code](//www.joelonsoftware.com/2000/08/09/the-joel-test-12-steps-to-better-code/)
  (originál Joel Spolsky)
* [Joel Test 2.0](//blog.zvestov.cz/software%20development/2015/10/22/joel-test-2.0.html)
  (Banter bloguje)
