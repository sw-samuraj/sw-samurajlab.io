+++
title = "Měl by mít vývojář portfolio?"
date = 2013-06-08T22:55:00Z
updated = 2013-06-08T23:05:59Z
description = """Pokud přijdete jako vývojář na pohovor a přinesete ukázku
    své práce, buďte si jistí, že jste vystoupili z řady - téměř nikdo to
    totiž nedělá."""
tags = ["interview"]
aliases = [
    "/2013/06/mel-by-mit-vyvojar-portfolio.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
thumbnail = "2013/06/Portfolio.png"
+++

{{< figure class="floatleft" src="/2013/06/Portfolio.png" >}}

Jednou z činností, které vás, jako seniorního vývojáře, mohou potkat, je dělání pohovorů s kanditáty, kteří by chtěli ve vaší firmě pracovat. Řekl bych, že je to docela zodpovědná (a zajímavá) činnost. Přece jenom, vybíráte své potencionální kolegy.

Přesto bývá (dost často) tento úkol pojat nekoncepčně a nahodile. Pohovor dělá, kdo má zrovna čas, zkouší se věci, které s prací přímo nesouvisí, v rámci firmy to každý dělá "trochu" jinak... Tak jako v jiných oblastech, i zde je známkou kvality konzistence.

Před časem jsem psal, [jak dělám java pohovor](/2012/11/jak-delam-java-pohovor/) já, kde jsem popsal postup, který používám cca poslední dva roky. (Teď se změnou zaměstnání jsem v něm provedl dvě úpravy, o kterých napíšu, až si to trochu "sedne".) Jedním z bodů, který po vývojářích vždycky chci, je, aby mi před pohovorem poslali ukázku svého (java) kódu _dle vlastního výběru_. Pojďmě se na to podívat trochu zevrubněji.

## Proč chci ukázku kódu?

Kromě úvodní informační části pojímám interview jako diskuzi. Je to diskuze mezi mnou --- team leaderem, či architektem --- a potencionálním kolegou. Své názory umím hájit, ale nejsem autoritativní --- myslím, že nejlepší řešení vyrůstají z kvalitní diskuze.

Kód, který mi kandidát pošle (a který pak spolu probíráme), je pro mne podkladem pro takovou diskuzi. V kódu si pomocí [TODO](//en.wikipedia.org/wiki/Comment_(computer_programming)#Tags) označím místa, na která se chci zeptat. Jsou to místa, která mě nějak zaujmou --- nejsou mi třeba jasný, jsou inspirativní, anebo je považuji za chybná. Nad tím pak diskutujeme.

Jak říkám, nejsem autoritativní, nemám dopředu připravený jedno řešení, které chci slyšet; takže pokud je kandidát schopen _své_ řešení obhájit, je to skvělé. V pořádku je, i pokud přizná chybu, že něco není úplně OK, dneska by to udělal jinak apod. Špatným znamením je, pokud k tomu kandidát nemá co říct.

Je to podobný, jako bychom _už_ byli spolu na projektu a diskutovali refactoring nějakého staršího kódu. Nebo dělali [code review](//en.wikipedia.org/wiki/Code_review). Takže věc, kterou --- pokud se staneme kolegy --- budeme v budoucnu běžně dělat.

Dobré je, že nediskutujem nějaké moje řešení. Je to kandidátův přínos k diskuzi --- on zná kontext kódu, on může poukázat na pěkný design, zajímavou implementaci, efektivní algoritmus. A nebo aspoň dobře odvedenou práci. Pamatujete? Je to kód _dle vlastního výběru_. Čili kandidát volí prostor k diskuzi. Protože nejde jen o tu diskuzi --- také se ptám: _Proč jste se rozhodl mi ukázat **právě tento kód**?_

Ta otázka je důležitá. Občas lze narazit na kandidáty, kteří prostě "obcházejí pohovory". A stejně tak, jako nevěnují pozornost dané firmě (prostě jen hledají _jakoukoliv_ práci), nevěnují pozornost ani kódu, kterým se prezentují. Přitom je to úžasná možnost představit se v dobrém/nejlepším světle a vystoupit z řady (jiných kandidátů).

## Kde vzít kód?

Někdy se setkám s tím, že daný vývojář řekne něco ve smyslu: "Ale já žádný kód na ukázku nemám. A to co momentálně píšu na projektu, nemůžu/nesmím dát k dispozici třetí straně." To zdánlivě vypadá jako show-stopper. Ale není.

Já samozřejmě nechci, aby někdo porušoval [NDA](//en.wikipedia.org/wiki/Non-disclosure_agreement). To je neetické. Ale i v takové situaci se dají věci řešit. _Když se chce_. V první řadě, je dobré si zjistit, co je předmětem NDA. Moje osobní zkušenost je, že to vývojáři většinou nevědí. Zkrátka "něco" podepsali. V takovém případě je dobré se zeptat někoho, kdo to ví.

{{< figure src="/2013/06/NDA.png"
    caption="Source [Noise to Signal Cartoon](//www.robcottingham.ca/cartoon/archive/can-you-keep-a-secret/)" >}}

Já když jsem chtěl ve své diplomové práci použít část práce, kterou jsem dělal pro tehdejšího zákazníka, tak jsem se jednoduše zeptal firemního právníka. Ten se podíval do příslušné smlouvy a řekl: "jo, můžeš to použít, akorát uveď zdroj. A pro jistotu, udělej tyhle anonymizační kroky: ..."

Čímž je řečena jedna možnost, jak to řešit. Ať už jste podepsali cokoliv, nikdo vás nemůže zbavit autorství vašeho kódu. A pokud kód "dostatečně" zanonymizujete, neměl by nikdo nic namítat. Otázkou je, jak určit, kolik to je "dostatečně". Nevíte-li, máte "tvrdé" smlouvy, máte pochybnosti --- konzultujte.

Další možností je napsat nový kód. _Cože?! Mám napsat nový kód? Práce navíc?!_ Jo, přesně tak. Chcete tu práci. Nebo ne? Já prostě chci vidět na pohovoru váš kód. Pokud nemůžete využít předešlou, nebo následující možnost, je to také jedna z cest. Já vám umožňuji prezentovat kód _dle vašeho výběru_. Tak napiště něco, co vás baví. A nemusí toho být moc. Nějaký návrhový vzor, vychytaný algoritmus, vyřešený problém, co vás před časem zaujal? To je ono! Myslím, že byste si to mohli užít. Pokud ne, asi jste se minuli povoláním. V tom případě... nemám zájem.

Nejjednodušší samozřejmě je, pokud nějaký kód, který můžete ukázat, máte k dispozici. Možná ho máte na [GitHubu](//github.com/), [Bitbucketu](//bitbucket.org/), nebo doma v kredenci. Třeba máte archiv minulých projektů, nebo schované nějaké [prototypy](//en.wikipedia.org/wiki/Software_prototyping). Každopádně máte něco, co se dá rovnou vzít a ukázat. Za takový kód se asi nebude stydět --- nechali jste si ho přece z nějakého důvodu, ne? A tím se dostáváme k jádru tohoto článku.

## Měl by mít vývojář portfolio?

Pokud přijdete (jako vývojář) někam na pohovor a přinesete s sebou ukázku své práce, buďte si jistí, že jste minimálně vystoupili z řady ostatních uchazečů. Téměř nikdo to totiž nedělá. A pokud vaše úkázky za něco stojí (nebo jsou dokonce excelentní ;-)  je dost pravděpodobné, že jste ostatní kandidáty zastínili. To ještě neznamená, že to máte v kapse. Ale výrazně jste zvýšili svoji šanci na úspěch.

Řeknu vám svůj příběh. Když jsem se ucházel o práci  (java vývojáře) u svého minulého zaměstnavatele, byla to jediná firma, kterou jsem oslovil --- líbila se mi a vybral jsem si ji jako místo, kde bych chtěl pracovat. Na pohovor jsem si (sám od sebe) přinesl _ukázky_ svého _kódu_, _screenshoty aplikací_ (tehdy jsem dělal hodně frontendů) a taky _generovanou dokumentaci_ ke svým aplikacím. A krátce jsem svou práci u pohovoru představil. Nevím, jaký to mělo vliv na mé přijetí. Faktem je, že jsem dostal vyšší nástupní plat, než jsem si sám řekl.

Když jsem se ucházel o svou současnou práci, nepřinesl jsem si na pohor nic. (Ha!) Důvodem je, že jsem nehledal práci vývojáře. Resp. rád bych si trochu zakódoval (tak z 1/3), ale hlavně mě zajímala práce team leadera (vývojářů). A taky architekta. Takže místo ukázky kódu jsem dal na vrchol svého CV odkaz na svůj blog (kde o těchto věcech píšu) a u pohovoru jsem se snažil tyto témata prezentovat a čerpat z nich. Myslím, že to fungovalo :-)

Takže jaká je odpověď na titulní otázku? Je subjektivní a není jednoznačná. Ale já se kloním k názoru, že vývojář by portfolio mít měl. Za každého hovoří jeho práce. A ta není někde v CV nebo mezi slovy na pohovoru. Je v textovém souboru někde na disku. Je tam, nebo ne? ;-)

## Související články

* [Jak dělám java pohovor](/2012/11/jak-delam-java-pohovor/)
* [Jak dělají java pohovor jinde](/2013/02/jak-delaji-java-pohovor-jinde/)
* [Geek, který zapadne](/2013/04/geek-ktery-zapadne/)

## Související externí články

* [Lester and Rothman: Do You Need a Portfolio?](//pragprog.com/magazines/2013-05/lester-and-rothman)
