+++
title = "Gradle tutorial: tasky (pokračování)"
date = 2013-08-29T22:29:00Z
updated = 2017-06-17T11:53:41Z
description = """V minulém díle Gradle tutorialu jsme si vystřihli rafinované
    Hello world a řekli jsme si něco o základním stavebním prvku každého
    build skriptu — Task, to je to, oč tu běží. Dnesk se na tasky podíváme
    podrobněji."""
tags = ["gradle", "build"]
aliases = [
    "/2013/08/gradle-tutorial-tasky-pokracovani.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2013/08/Gradle.png" >}}

[V minulém díle](/2013/07/gradle-tutorial-tasky/) Gradle tutorialu jsme si vystřihli rafinované _Hello world_ a řekli jsme si něco o základním stavebním prvku každého build skriptu --- [Task](//www.gradle.org/docs/current/dsl/org.gradle.api.Task.html), to je to, oč tu běží. A ruku na srdce, milý čtenáři, jistě jsi zvědavý, kam nás naše povídání zavede. Protože to zajímavé teprve přijde.

## Závislosti mezi tasky

Tasky téměř nikdy nestojí samostatně. To není nic moc překvapivého. Známe to z [Antu](//ant.apache.org/), známe to z [Mavenu](//maven.apache.org/), bylo by divný, kdyby to Gradle dělal jinak. Když si například vezmeme klasický [Maven build lifecycle](//maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html), tak vypadá nějak takhle:

1. zpracuj resources,
1. zkompiluj třídy,
1. zpracuj test-resources,
1. zkompiluj třídy testů,
1. spusť testy,
1. vytvoř archiv,
1. atd.

Tyto jednotlivé úkony (v Mavenu nazývané goals) na sebe logicky navazují a jsou na sobě _implicitně_ závislé. Implicitně znamená, že nemusím závislost jednotlivých tasků (goals) _explicitně_ uvádět --- Maven "už to nějak ví", jak si ty tasky má seřadit. Magie? Ne, "někdo" mu to řekl.

Podobné je to i v Gradlu. Některé tasky už mají závislosti definované, většinou je to v rámci nějakého pluginu. Třeba u Java pluginu (o kterém si budeme povídat v příště) vytváří závislosti cyklus, který je velmi podobný tomu Mavenímu. Velkou výhodou Gradlu je, že závislosti mezi tasky se definují velmi jednoduše a navíc nad nimi máme programovou kontrolu.

Závislost tasku se dá definovat buď pomocí [property](//www.gradle.org/docs/current/dsl/org.gradle.api.Task.html#org.gradle.api.Task:dependsOn) nebo [metody](//www.gradle.org/docs/current/dsl/org.gradle.api.Task.html#org.gradle.api.Task:dependsOn(java.lang.Object...)) `dependsOn`. Způsob zápisu se může podobat tomu, který známe z Antu, čili přímo při definici tasku:

```gradle
task first << { task ->
    println "  Working on the ${task.name} task"
}

task second(dependsOn: first) << { task ->
    println "  Working on the ${task.name} task"
}
```

Anebo můžeme využít nespoutanosti Groovy a závislost definovat v podstatě na libovolném místě:

```gradle
task third << { task ->
    println "  Working on the ${task.name} task"
}

third.dependsOn second
```

Vzhledem k tomu, že zde máme závislost tasků `third` -&gt; `second` -&gt; `first`, výstup by při spuštění tasku `third` měl vypadat následovně:

{{< figure src="/2013/08/Gradle-dependencies.png" caption="Závislost tasků" >}}

Zajímovou možností je definovat závislosti tasku dynamicky. Vstupem do metody `dependsOn` totiž může být [closure](//groovy.codehaus.org/Closures), jedinou podmínkou je, aby vracela objekt typu [Task](//www.gradle.org/docs/current/dsl/org.gradle.api.Task.html), nebo kolekci Tasků.

```gradle
task fourth << { task ->
    println "  Working on the ${task.name} task"
}

fourth.dependsOn {
    tasks.findAll { task -> task.group.equals('sequence') }
}
```

V uvedeném výpisu je task `fourth` závislý na všech tascích projektu, které jsou seskupeny pomocí vlastnosti [group](//www.gradle.org/docs/current/dsl/org.gradle.api.Task.html#org.gradle.api.Task:group).

Zdrojový kód na Bitbucketu: [02_TaskDependencies/build.gradle](//bitbucket.org/sw-samuraj/gradle-tutorial/src/228d1e85f4b0693b432fec18a1473ebf393d1198/02_TaskDependencies/build.gradle?at=published)

## Přeskočení tasku

{{< figure class="floatright" src="/2013/08/Skippy.jpg"
    link="/2013/08/Skippy.jpg" width="250" >}}

Ať už jste agilní vývojáři, kteří milují dynamické prostředí, nebo jste naopak vyznavači petrifikovaného zadání, jedno je jisté --- chtěná, či nechtěná, stejně přijde nějaká _změna_. A tak, jakkoliv je náš pracovní lifecycle vymazlený, bude do něj potřeba zasáhnout. Někdy třeba jen dočasně.

Dynamická práce s tasky může být dost komplexní a vydala by na samostatný článek. My se v rámci tutorialu podíváme na část, která je k dispozici out-of-the-box a sice na způsoby, jak zajistit "nevykonání" naplánovoného tasku.

(Při psaní příkladů pro tuto část jsem měl rozvernou náladu, takže v této sekci bude naším průvodcem jeden z hrdinů mého dětství, klokan [Skippy](//www.youtube.com/watch?v=yFHtc_bDXWA) :-)

První ze způsobů, jak přeskočit task, je využití metody [onlyIf](//www.gradle.org/docs/current/dsl/org.gradle.api.Task.html#org.gradle.api.Task:onlyIf(groovy.lang.Closure)), pro kterou můžeme definovat predikát. Akce v tasku (a tím pádem i task samotný) jsou vykonány pouze tehdy, pokud je predikát vyhodnocen jako `true`.

```gradle
task skippy << {
    println 'I\'m happy, so I jump!'
}

skippy.onlyIf { project.isHappy == 'true' }
```

V tomto příkladu je task `skippy` vykonán pouze tehdy, pokud je projektová property `isHappy` nastavena na `true`. Pokud takovou property v build skriptu nemáme definovanou, nevadí --- přidáme si ji z příkazové řádky pomocí přepínače [-P](//www.gradle.org/docs/current/userguide/gradle_command_line.html), např. `-PisHappy=true`.

{{< figure class="floatright" src="/2013/08/Gradle-skip-task-onlyIf.png"
    caption="Přeskočení tasku pomocí metody onlyIf" >}}

Další možnost, jak nevykonat task, nebo jeho část, je vyhodit výjimku [StopExecutionException](//www.gradle.org/docs/current/javadoc/org/gradle/api/tasks/StopExecutionException.html). To způsobí, že je zastaveno vykonávání aktuálního tasku a je spuštěno vykonání toho následujícího. Tato výjimka přeruší pouze daný task, build pokračuje dál.

```gradle
task exceptionalSkippy << {
    if (project.isHappy == 'false') {
        throw new StopExecutionException()
    }

    println 'I\'m happy, so I jump!'
}
```

Jak je vidět na následujícím výstupu, task `exceptionalSkippy` nebyl přeškočen (jako v předešlém příkladu), ale byl normálně spuštěn a pak byl někde "uvnitř" přerušen.

{{< figure src="/2013/08/Gradle-skip-task-exception.png" caption="Přerušení tasku pomocí StopExecutionException" >}}

Třetí možností, jak rozhodnout o vykonání tasku je použití property [enabled](//www.gradle.org/docs/current/dsl/org.gradle.api.Task.html#org.gradle.api.Task:enabled) --- task tak můžeme vypnout, nebo zapnout.

V následujícím příkladu máme dva tasky. Task `disabledSkippy` (který nám řekne, jestli Skippy bude skákat) závisí na tasku `skippyMood` (který nám oznámí Skippyho náladu). Task `skippyMood` má možnost vypnout task `disabledSkippy`.

```gradle
task skippyMood << {
    if (project.isHappy == 'false') {
        disabledSkippy.enabled = false

        println 'I\'m not happy :-('
    } else {
        println 'I\'m happy :-)'
    }
}

task disabledSkippy << {
    println 'I\'m happy, so I jump!'
}

disabledSkippy.dependsOn skippyMood
```

{{< figure src="/2013/08/Gradle-skip-task-enabled.png"
    caption="Přeskočení tasku pomocí property enabled" >}}

Zdrojový kód na Bitbucketu: [03_SkipTask/build.gradle](//bitbucket.org/sw-samuraj/gradle-tutorial/src/228d1e85f4b0693b432fec18a1473ebf393d1198/03_SkipTask/build.gradle?at=published)

## Task rules, dynamické generování tasků

Už jsem párkrát naznačoval cosi o dynamičnosti Gradle, pojďmě se tedy podívat na něco konkrétního. Co kdybychom potřebovali větší množství tasků, které dělají v podstatě to samé, akorát se to "to" liší nějakými parametry. Můžeme takovou potřebu řešit pomocí principů objektového programování, třeba. kompozicí. To může být docela otrava, hlavně správa a přetestovávání takových tasků.

Co si takhle potřebné tasky nechat dynamicky vygenerovat? Právě od toho jsou tady [task rules](//www.gradle.org/docs/current/userguide/more_about_tasks.html#N10F58).

```gradle
def environments = [
    'DEV': ['url': 'http://dev:8080'],
    'SIT': ['url': 'http://sit:9090'],
    'UAT': ['url': 'http://uat:7001']]

tasks.addRule('Pattern: deploy<ENV>') { taskName ->
    if (taskName.startsWith('deploy')) {
        task(taskName) << {
            def env = taskName - 'deploy'

            if (environments.containsKey(env)) {
                def url = environments[env].url

                println "Deploying to ${env} environment, url: ${url}"
            } else {
                println "Unsupported environment: ${env}"
            }
        }
    }
}
```

{{< figure src="/2013/08/Gradle-task-rules.png" caption="Příklad spuštění jednotlivých task rules" >}}

Vzhledem ke své povaze jsou task rules uvedeny v samostatné sekci při výpisu tasků pomocí příkazu `gradle tasks`:

{{< figure src="http://2.bp.blogspot.com/-ahFKCwHBdXk/UfUFFGo5o4I/AAAAAAAAEPk/HBnRR51lQ-Q/s1600/Gradle+task+rules+list.png" caption="Výpis pravidel (rules) příkazem `gradle tasks`" >}}

Zdrojový kód na Bitbucketu: [04_TaskRules/build.gradle](//bitbucket.org/sw-samuraj/gradle-tutorial/src/228d1e85f4b0693b432fec18a1473ebf393d1198/04_TaskRules/build.gradle?at=published)

## Asi v tom bude nějakej háček

Jedním z nejčastějších problémů u tasků, se kterým se můžeme zpočátku setkávat, je záměna _konfigurace_ a _definice_ tasku. Definici tasku už známe --- používali jsme ji celou dobu. V rámci definice přidáváme do tasku jednotlivé [akce](//www.gradle.org/docs/current/javadoc/org/gradle/api/Action.html), které jsou pak vykonány během exekutivní fáze. Jen pro připomenutí, akce můžeme do tasku přidat metodami [doFirst](//www.gradle.org/docs/current/dsl/org.gradle.api.Task.html#org.gradle.api.Task:doFirst(groovy.lang.Closure)) a [doLast](//www.gradle.org/docs/current/dsl/org.gradle.api.Task.html#org.gradle.api.Task:doLast(groovy.lang.Closure)) a daleko nejčastěji se používá operátor `&lt;&lt;` který je aliasem pro metodu `doLast`.

Konfigurace tasku slouží... ehm, ke konfiguraci tasku. Podstatné je, že konfigurace je vykonána v jiné fázi buildu. A proběhne pokaždé, i když zrovna daný task nespouštíme. Task lze nakonfigurovat několika způsoby, ten "problematický" má stejnou syntaxi jako definice tasku, pouze je bez `&lt;&lt;` operátoru.

```gradle
task foo {
    // some task configuration here
}
```

V následujícím výpisu task `pitfall` prvně "nakonfigurujeme" a následně do něj přidáme akci.

```gradle
task pitfall { task ->
    println "Printed from task ${task.name} configuration."
}

pitfall << { task ->
    println "Printed from task ${task.name} execution."
}
```

Když se podíváme na spuštění tasku `pitfall`, všimněte si, kdy proběhl "konfigurační" výpis. Ještě před začátkem spuštění tasku (tedy před vykonáním obsažených akcí).

{{< figure src="http://4.bp.blogspot.com/-iln2Loh2Rx4/UfWLayuZ1BI/AAAAAAAAEP8/QFg7z4eglfU/s1600/Gradle+task+pitfall.png" caption="Vizualizace konfigurační a exekuční fáze tasku" >}}

Druhým častým problémem s tasky bývá následující chyba:

```gradle
Cannot add task ':exists' as a task with that name already exists.
```

Což znamená, že se snažíme definovat task, který už je definován někde jinde. Problém bude buď v chybné syntaxi, anebo v praxi daleko častěji, pokud pracujeme s projektem, kde používáme plugin, který task stejného jména již obsahuje. Obdobou téhož je, když si do projektu natáhneme antovské tasky (jeden z budoucích dílů tutorialu) a opět, dané jméno tasku už je použito.

Pokud řešíme druhý případ (duplicitní název/definice tasku), můžeme task explicitně předefinovat. Nemusím zdůrazňovat, že bychom měli vědět co a proč to děláme. Pokud máme buildovací skripty plně v rukou, bude tento problém indikovat chybu v designu buildu. Ovšem v případě, že se potýkáme s nějakým [legacy](//en.wikipedia.org/wiki/Legacy_system) buildem (staré Ant build soubory), může být redefinice tasku nutností.

Task lze předefinovat pomocí property `overwrite`:

```gradle
task exists << {
    println 'primary task definition'
}

task exists(overwrite: true) << {
    println 'overwritten task definition'
}
```

{{< figure src="/2013/08/Gradle-task-overwrite.png"
    caption="Redefinice tasku pomocí property `overwrite`" >}}

Zdrojový kód na Bitbucketu: [05_TaskPitfalls/build.gradle](//bitbucket.org/sw-samuraj/gradle-tutorial/src/082daa53ea81a91beaecdc8e3a6bbf5354929da8/05_TaskPitfalls/build.gradle?at=published)

## Zdrojové kódy

Zdrojové kódy k dnešnímu dílu jsou k dispozici na [Bitbucketu](//bitbucket.org/sw-samuraj/gradle-tutorial). Můžete si je tam buď [probrouzdat](//bitbucket.org/sw-samuraj/gradle-tutorial/src/228d1e85f4b0?at=published), stáhnout jako [ZIP archiv](//bitbucket.org/sw-samuraj/gradle-tutorial/get/published.zip), anebo --- pokud jste cool hakeři jako já :-) --- naklonovat [Mercurialem](//mercurial.selenic.com/):

```bash
hg clone ssh://hg@bitbucket.org/sw-samuraj/gradle-tutorial
```

## Co nás čeká příště

O tascích by se dalo povídat ještě dlouho. Ale protože jsem příznivcem hesla [better is enemy of done](//en.wikipedia.org/wiki/Perfect_is_the_enemy_of_good), tasky dnešním dílem opouštíme a příště se podíváme, jak Gradle řeší Java vývoj.

_Tento článek byl původně publikován na serveru [Zdroják](//www.zdrojak.cz/)._

## Související články

* [Gradle, moderní nástroj na automatizaci](/2013/05/gradle-moderni-nastroj-na-automatizaci/)
* [Gradle tutorial: tasky](/2013/07/gradle-tutorial-tasky/)

## Související externí články

* [Build Script Basics](//www.gradle.org/docs/current/userguide/tutorial_using_tasks.html) (Gradle User Guide)
* [More about Tasks](//www.gradle.org/docs/current/userguide/more_about_tasks.html) (Gradle User Guide)
* [Task](//www.gradle.org/docs/current/dsl/org.gradle.api.Task.html) (Gradle DSL Reference)
