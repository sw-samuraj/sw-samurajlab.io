+++
title = "Kontrakt místo pohovoru, je to reálné?"
date = 2013-08-04T23:18:00Z
updated = 2013-08-05T08:08:25Z
description = """Poslední dobou se nám nějak rozmohlo zadávání projektů
    namísto (anebo hlavní gró) přijímacích pohovorů. SoftWare Samuraj
    si nemyslí, že je to dobrý nápad. Ale může se mýlit."""
tags = ["teamleading", "interview"]
aliases = [
    "/2013/08/kontrakt-misto-pohovoru-je-to-realne.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2013/08/Stag-beetle.jpg"
    link="/2013/08/Stag-beetle.jpg" width="300" >}}

Největším pracovním tématem posledního čtvrt roku je pro mne vytváření týmu. Potřebuji sestavit skupinu 10 vývojářů. Zatím je přijata cca třetina lidí a ta težší část mě teprve čeká --- vytvořit týmovou kulturu a tým zapracovat a rozpohybovat.

Posleního čtvrt roku žiju pohovory. Dělám technická interview, dosti podobná tomu, co jsem před časem popsal v článku [Jak dělám Java pohovor](/2012/11/jak-delam-java-pohovor/). Je to pro mne živé a přitažlivé téma --- kromě toho, že je to (téměř) každodení zkušenost, tak sleduju různé články na internetu. Většinou v angličtině. K dnešnímu postu mě ale inspiroval článek, který je... ve slovenštině.

Riki Fridrich navrhuje v článku [Kontrak namiesto pohovoru](//content.fczbkk.com/kontrakt-namiesto-pohovoru/) alternativu ke klasickému výběrovému řízení --- místo pohovoru si kandidát střihne maličký projekt. Riki nepřichází s ničím novým --- před rokem o tom psali [Jeff Atwood](//www.codinghorror.com/blog/2012/03/how-to-hire-a-programmer.html), [Jon Evans](//techcrunch.com/2012/05/12/bloodsport/), letos [Nelly Yusupova](//www.entrepreneur.com/article/225932) a [Tommy Morgan](//wellbredgrapefruit.com/blog/2013/06/01/how-we-hire-developers-at-treehouse/). Takže to je myšlenka, která rezonuje. Minimálně na internetu.

## V čem je problém?

Všemi výše uvednémi články prosvítají dvě základní myšlenky:

1. Pracovní pohovory jsou neefektivní.
1. Nejlepším způsobem, jak si (z několika úhlů) otestovat nového vývojáře, je zadat mu malý, ale reálný projekt.

Riki jde u druhého bodu ještě dál, když říká, že _"to je budoucnost najímání seniorních ajťáků"_.

## Jsou pracovní pohovory neefektivní?

Pracovní pohovory jsou tak neefektivní, jak neefektivní si je každý udělá. A to platí pro obě strany. Říct, že pracovní pohovor je neefektivní, je stejné, jako říct, že neefektivní je třeba [Scrum](//en.wikipedia.org/wiki/Scrum_(development)). Nebo psaní [unit testů](//en.wikipedia.org/wiki/Unit_testing). Nebo [kontinuální integrace](//en.wikipedia.org/wiki/Continuous_integration). Mám pokračovat?

Všechny právě zmíněné "činnosti" jsou nástroje, které nám pomáhají v naší práci. Je přece na nás, jestli je budeme používat efektivně (a správně). A je velmi důležité si uvědomit, že všechny tyto nástroje fungují v určitém _kontextu_ (moje oblíbené slovo :-)

Myslíte si, že např. softwarový projekt bude úspěšný jen proto, že "nasadím" nějaký z těchto nástrojů? Já nevím jak vy, ale já už jsem viděl dost špatných implementací Scrumu (mmch. je zajímavý, že zainteresovaní vývojáři to většinou vidí, jako úspěch, i když projekt šel do kytek).

Podobné je to i s pracovním pohovorem. Není to žádná [stříbrná kulka](//en.wikipedia.org/wiki/No_Silver_Bullet). Pohovor, to je jen taková "kvalifikace do závodu". Co v člověku opravdu je, se ukáže teprve během _zkušební_ doby. Jak říká Rands, až teprve po [90 dnech](//www.randsinrepose.com/archives/2007/01/03/ninety_days.html) budou obě strany vědět, na čem jsou. Neexistuje žádná zkratka.

Takže, ať už děláte takový skvělý pohovor jako já ;-) nebo jedete v obvyklé korporátní nudě, pořád je to jen jeden stupínek na dlouhé cestě. Pohovor bude tak efektivní, jak efektivní si ho uděláte --- ať už jste kandidát, nebo zaměstnavatel.

## Projekt místo pohovoru?

Je to hezká myšlenka. Místo příkladu do šuflíku --- opravdový projekt. Místo programování na tabuli/papír --- programování v oblíbeném [IDE](//en.wikipedia.org/wiki/Integrated_development_environment). Místo akademických algoritmů --- reálný problém. Místo stresující nudy --- práce, která baví. OK, myšlenka je to inspirativní. Co ale musíme udělat, aby byla proveditelná?

**Nastavení prostředí**. Aby kandidát mohl začít na něčem vyšívat, musí si nachystat nástroje. Jak dlouho mu to může trvat? Třeba je firma tak vyspělá, že nejenom že má build (nebo dokonce release) [na jeden klik](/2013/06/joel-test-ma-jeste-smysl/). Dokonce má na jeden klik i nastavení prostředí. Takový ty věci, jako vývojový (aplikační/webový) server, potřebné pluginy do IDE (ve správných verzích), všechny závislosti pro build, deploy a běh aplikace atd.

Řekněme, že tohle všechno je na jeden klik a když to bude hodně komplexní, tak to bude trvat maximálně 30 minut (berme, že konektivita pro stahování artefaktů je v dostatečné kapacitě). Build je taky na jeden klik a opět, když to bude hodně komplexní, tak dejme tomu 10 minut. Takže včetně uvaření kafe, za hodinu je kandidát připraven vyvíjet.

Mno. Zrovna jsem tenhle týden dostal od kandidáta na pohovoru otázku, jak dlouho u nás trvá příprava prostředí pro vývoj. A upřímně jsem odpověděl, že když jsem si rozcházel projekt, na kterém teď vypomáhám s nějakými [change requesty](//en.wikipedia.org/wiki/Change_request), trvalo mi to tři dny. Ten projekt je specifický a má to svoje objektivní důvody. Ale je to realita, která se čas od času přihodí.

Obecně, moje zkušenost (v oblasti enterprise Javy) je, že rozchození projektu trvá řádově [man days](//en.wikipedia.org/wiki/Man-hour). Nevím, možná je to extrémní situace. Když jsem před 10 lety programoval v [PHP](//php.net/), tak jsem si: 1) zkopíroval z disku zdrojáky, 2) hodil je na [Apache](//httpd.apache.org/) (mod_php) a 3) a začal bastlit ve [Vimu](//www.vim.org/), tak to mohlo trvat tak 20 minut. (Teď trochu kecám --- tehdy jsem bušil ve [Slackwaru](//www.slackware.com/) a celý [LAMP](//en.wikipedia.org/wiki/LAMP_(software_bundle)) jsem si kompiloval ze zdrojáků. Takže i s přípravou serveru to trvalo o dost dýl. Ale dejme tomu, že to zanedbáme.)

Máte zkušenosti z jiných domén? Jak dlouho trvá nastavit prostředí v Ruby? V Pythonu? V Erlangu? V Androidu? Myslím reálné business projekty.

**Sdílení artefaktů.** Kandidát bude potřebovat s firmou sdílet nějaké artefakty. V první řadě, bude potřebovat někde získat zdrojový kód. A zpátky komitovat svoje změny. Co nějaká dokumentace, Wiki? Nějaké konfigurační artefakty, které nejsou přímo součástí aplikace?

Pokud jste na [GitHubu](//github.com/) (nebo jiné, [obdobné](//bitbucket.org/) hostingové službě), máte vyhráno. Nejste na GitHubu? A safra! Kdo bude s vaším kandidátem řešit problémy s konektivitou, nastavením práv apod.? Nejde o to, že by to bylo složité (zatím to vždycky fungovalo ;-) a kandidát by si s tím neuměl poradit. Problém je, že to "žere" čas, který má kandidát na projekt.

**Sdílení informací.** Kolik artefaktů, vyjma aplikace samotné, vám na projektu vzniká? Jste agilní a máte jich minimum? Znalosti a kontext se sdílí v týmu hlavně orálně (stand-upy, statusy atd.)? Nebo máte naopak hodně formálních artefaktů? Věci jako specifikaci, analytický model, guide lines atd.

A teď. Kolik času kandidátovy zabere, aby vstřebal alespoň nutné minimum, které mu umožní na projektu začít?

**Znalost technologií.** Třeba firma dělá v něčem křišťálově čistém --- víš, nevíš. Abych pravdu řekl, nic takového mě nenapadá. Děláte v JavaScriptu? Kolik frameworků znáte opravdu dobře? Děláte v PHP? Totéž. Pokud bych vzal v potaz doménu Java frontend frameworků, tak pravděpodobnost, že kandidát zná, či má zkušenosti právě s tím vaším, je tak 10 %. OK, pokud dělal s ([nějakým klonem](//en.wikipedia.org/wiki/JavaServer_Faces#Ajax-enabled_components_and_frameworks)) [JSF](//en.wikipedia.org/wiki/JavaServer_Faces), tak se to může blížit i 50 %.

Moje frontendová zkušenost za cca 8 let v Javě? Co projekt, to jiný framework. Pokaždé se to učíte znova a znova. Když to vezmu chronologicky: [Servlety](//en.wikipedia.org/wiki/Java_Servlet), [HTMLB](//help.sap.com/saphelp_nwce72/helpdata/en/4a/4ce98c76201cd4e10000000a421937/content.htm) (zná to někdo?), [Wicket](//wicket.apache.org/), [RichFaces](//www.jboss.org/richfaces), [Vaadin](//vaadin.com/home).

U backendových technologií to není tak divoký, ale stejně. Celkem běžně potkávám na pohovorech lidi, kteří třeba 5 let dělali v [JDBC](//en.wikipedia.org/wiki/Java_Database_Connectivity) a ani nezavadili o [JPA](//en.wikipedia.org/wiki/Java_Persistence_API), nebo něco podobného.

Takže jaká je pravděpodobnost, že kandidát bude technologicky sedět na potřeby firemního produktu? Pokud to nebude 80-90 %, co s tím? Jak dlouho se bude nové technologie učit? Nebo si oživovat ty staré (třeba přes dvě, tři nekompatibilní verze)? Jaký to bude mít vztah k našemu projektu?</div>

## Je to reálné?

V předešlé sekci jsem načrtnul některé z problémů (jistě ne jediných), které by bylo potřeba vyřešit, aby byl "přijímací" projekt proveditelný. Všechno jsou to technické záležitosti, v podstatě jenom prerekvizity. Už jenom u těchto předpokladů mám vážné obavy, že by se jejich splnění mohlo vejít do pár hodin, maximálně jednoho dne. Ale dejme tomu, že by to šlo.

Další sadou výzev bude nastavení firmy a její kultura. Kandidát by ideálně měl dostat za svůj projekt zaplaceno. Kde se ty peníze ve firmě vezmou? Některé společnosti fungují striktně nákladově --- vezmou se ty peníze z budgetu projektu? Nebo z nějaké režijní činnosti? Kdo za to bude odpovědný? Jakým způsobem se budou ty náklady vyúčtovávat? V maličkým startupu nad tím můžeme mávnout rukou. Ve větší firmě to bude show-stopper.

Také nemůžu nezmínit věc, která mi přijde velmi kritická --- součinost firmy s kandidátem. Zdá se mi to, nebo se furt nikdo nepoučil, že přidáním lidí na projekt se vývoj zpomalí ([Brook's law](//en.wikipedia.org/wiki/Brooks's_law))? Pokud přijímáme jednoho kandidáta, (časové) náklady na součinost se v běžné práci ztratí. Pokud ale potřebujeme najmout tým lidí --- a neříkám, že najednou --- znamená to zasekat se v podpoře kandidátů na půl roku dopředu. Na full time. To si nedovedu představit. A to mám se zaškolováním lidí na projektech, řekl bych, celkem bohaté zkušenosti.

A pak nám zbývá poslední, esenciální ingredience. Kandidát. Chápu, že když chce někdo dělat pro firmu svých snů, udělá pro to opravdu hodně. Doplňte si dle svých preferencí Google, Twitter, ThoughtWorks, GitHub, Stack Overflow, Apache, Canonical... já nevím, co ještě. Takže když vám taková firma zadá vstupní projekt, rádi ho uděláte a ještě to budete považovat za čest.

Jenže, kolik takových firem je? Jedno promile? Co ty ostatní, kde pracuje 90 % těch vývojářů, co nejsou špičkový? Ruku na srdce, kolik z vás by šlo do týdenního projektu (byť placeného) s nejistým výsledkem?

Aby kandidáti do něčeho takového šli, museli by změnit svůj [mindset](//en.wikipedia.org/wiki/Mindset). Nedávno jsem v článku [Měl by mít vývojář portfolio?](/2013/06/mel-by-mit-vyvojar-portfolio/) psal o tom, že na pohovorech chci vidět kandidátův kód. Nemám srovnání, ale odhaduju, že jsem v tomhle spíš výjimka. Je to vidět i na přístupu kandidátů --- někteří jsou tím dost zaskočeni. Někteří dají do placu kód, který není zrovna oslnivý. A zlomek jich tento požadavek prostě ignoruje.

Takže můj pocit je, že pro majoritu vývojářů by psaní vstupního projektu bylo příliš velkým myšlenkovým veletočem.

## Závěr

Myslím, že myšlenka dát vývojáři místo pohovoru projekt, je zajímavá. Ale realizovatelná tak v jednom procentu případů. Velmi specifických případů. Asi půjde o nějaký startup, kde to nebude problém procesně a technicky. V případě kandidáta půjde asi o excelentního bušiče kódu, který zrovna šťastně odpovídá technologickému nastavení firmy. A práce bude pro kandidáta tak zajímavá, že mu to za ten projekt bude stát. To je dost výjimečná konstelace.

## Související články

* [Jak dělám Java pohovor](/2012/11/jak-delam-java-pohovor/)
* [Měl by mít vývojář portfolio?](/2013/06/mel-by-mit-vyvojar-portfolio/)
* [Jak dělají Java pohovor jinde](/2013/02/jak-delaji-java-pohovor-jinde/)

## Související externí články

* [How to Hire a Programmer](//www.codinghorror.com/blog/2012/03/how-to-hire-a-programmer.html)
* [No Shortcuts, No Mercy: The Bloodsport of Recruitment](//techcrunch.com/2012/05/12/bloodsport/)
* [5 Tips for Hiring a Great Web Developer](//www.entrepreneur.com/article/225932)
* [How We Hire Developer at Treehouse](//wellbredgrapefruit.com/blog/2013/06/01/how-we-hire-developers-at-treehouse/)
* [Hiring Developers: You're Doing It Wrong](//devinterviews.pen.io/)
* [On Hiring Good Engineers](//www.josefassad.com/on_hiring_good_engineers)
