+++
title = "Zdravý programátor"
date = 2013-08-17T22:53:00Z
updated = 2013-08-17T22:53:05Z
description = """Pryč jsou doby, kdy stačilo, aby byl programátor
    pragmatický. Dnes musí být ještě navíc zdravý. Programátoři
    totiž často vedou dosti nezdravý způsob života. Recenze knihy,
    která radí ke svému (programátorskému) zdraví přistupovat
    agilně."""
tags = ["knihy", "sw-engineering"]
aliases = [
    "/2013/08/zdravy-programator.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2013/08/Healthy-Programmer.png"
    link="/2013/08/Healthy-Programmer.png" width="300" >}}

Pokud čtete alespoň polovinu odborných knih, [toho co já](/2013/03/druhy-rok-s-kindlem/), asi vám nebude neznámé nakladatelství [The Pragmatic Programmer](//pragprog.com/). Pravda, schopnost (spíš posedlost) přečíst kvanta knih jsem získal v minulém století, tak možná už to není tak in.

Takže, i pokud knížky nečtete, ale jste vývojář, který se nebojí v hospodě třísknout do stolu a zvolat na celý lokál: [já jsem programátor, kdo je víc?](//ekonomika.idnes.cz/hornik-v-cssr-drina-a-nadstandard-d4r-/ekonomika.aspx?c=A090622_112923_ekonomika_vem), měla by vám něco říkat jména [Andy Hunt](//andy.pragprog.com/) a [Dave Thomas](//pragdave.pragprog.com/). Pořád nic? Dobře, dávám vám poslední šanci: [The Pragmatic Programmer](//pragprog.com/the-pragmatic-programmer). Bible softwarového inženýrství. Ano, napsali ji ti dva pánové. A ano, oni založili i to vydavatelství.

Proč se o tom tak vykecávám? Protože tohle parádní vydavatelství si zaslouží trochu promotion. Četl jsem od nich už šest knih a všechny byly skvělé, pokaždé 5 hvězdiček. Takové kvalitě se, s výjimkou [Manningu](//www.manning.com/), žádné další nakladatelství ani nepřibližuje. Takže, pokud váháte, jestli od nich něco koupit, anebo jste je doposud vůbec neznali --- vřele doporučuju.

## Memento mori

Pryč jsou doby, kdy stačilo, aby byl programátor pragmatický. Dnes musí být ještě navíc zdravý. Že by diktát [New Age](//en.wikipedia.org/wiki/New_Age)? Ne, jenom realita.

Programátoři (podobně jako rockeři či jazzmeni) vedou ve velké většině dosti nezdravý způsob života. Dokud je člověk mladý, tak to nějak jde. Ale jak začne oťukávat třicítku, tak mu tak nějak dojde, že není nesmrtelný a začnou se ozývat různé bolístky. A tady by měl každý zůstat ostražitý...

Jinak můžete dopadnout, jako já před pár lety --- svoji programátorskou vášeň (10-12 hodin u počítače, včetně víkendů) jsem krutě zaplatil --- vyhřeznutou plotýnkou. Plotýnka nabořená v míše je kritická věc (můžete třeba ochrnout) a zcela spolehlivě vás odradí od práce s počítačem --- nejen, že nemůžete sedět, nemůžete ani ležet, ani stát. Všechno hrozně bolí.

No, vzhledem k tomu, že teď čtete tenhle článek, tak to asi dobře dopadlo. Vysekal jsem se z toho --- s pomocí dobré víly a hlavně vlastním úsilím, jsem slezl chirugovi z lopaty a dnes žiju běžný život jako "předtím". Opět vášnivě programuji (i když rozumně --- snažím se dodržovat XP [Eight-Hour Burn](//c2.com/cgi/wiki?EightHourBurn)), své děti vyhodím bez problémů metr nad hlavu, běhám, plavu atd. Všechny tyto věci mi doktoři zakazovali. (A pak jim věřte. ;-)

## The Healthy Programmer

Kniha [The Healthy Programmer](//pragprog.com/book/jkthp/the-healthy-programmer) od [Joea Kutnera](//twitter.com/codefinger) sice nenabízí tak srdceryvný příběh, jaký jste právě dočetli :-)  Nabízí ale něco jiného --- jak se takovým problémům vyhnout. Knihu dobře vystihuje její podtitul: _Get Fit, Feel Better, and Keep Coding_. Nejdůležitější je to poslední --- _Keep Coding_ --- protože jestli chcete programovat (nebo dělat cokoliv jiného na počítači) ještě za 10 let, budete s tím muset něco udělat. Čas, biologie a mechanika jsou neúprosné a pracují proti vám.

{{< figure src="/2013/08/Healthy-Programmer-goal.png"
    caption="Zdroj [The Healthy Programmer](//pragprog.com/book/jkthp/the-healthy-programmer)" >}}

Jádro knihy vám neprozradí nic nového, prostě: jezte zdravě, cvičte, dávejte si pozor (a pečujte) na oči, záda a zápěstí, buďte aktivní atd. Co je ale na knize unikátní, je způsob, jak jsou tyto věci vysvětlovány a jak byste k nim měli přistupovat.

Kniha totiž radí přistupovat ke svému zdraví agilně. Pro daný aspekt si udělat test a pak iterativně refaktorovat danou kvalitu, až test projde. Dostane se i na [Pomodoro](//www.pomodorotechnique.com/) a [kaizen](//en.wikipedia.org/wiki/Kaizen). Všechny změny jsou malé a postupné. Smyslem není jen vyřešit váš aktuální problém (nadváha, bolesti zad či hlavy, únava apod.) a upozornit na (závažná) rizika, která se s vývojářským životním stylem pojí, ale hlavně najít systém, který pro vás bude udržitelný po zbytek vašeho (programátorského) života. A taky vás bude bavit.

Všechny věci, které autor doporučuje jsou podepřeny vědeckými studiemi. Pokud někde korelace není prokazatelná, tak na to upozorní. Kniha obsahuje obrovskou sumu referencí, na vědecké články a další literaturu. Abych pravdu řekl, tolik odkazů jsem v publikaci ještě neviděl.

Zakončil bych prvním citátem, který jsem si podtrhnul. Myslím, že knihu velmi přesně charakterizuje.

> "Agile processes are characterized by an iterative and incremental approach to development, which allows us to adapt to changing requirements. The method you use to stay healthy shouldn't be any different."
