+++
title = "Geek, který zapadne"
date = 2013-04-04T20:18:00Z
updated = 2017-11-04T15:45:56Z
description = """Jak se správně rozhodnout po úspěšném pracovním pohovoru?
    Překlad článku Being the Geek Who Fits od Andyho Lestera."""
tags = ["interview"]
aliases = [
    "/2013/04/geek-ktery-zapadne.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
thumbnail = "2013/04/Interview.jpg"
+++

{{< figure src="/2013/04/Interview.jpg"
    caption="Image courtesy of Ambro / [FreeDigitalPhotos.net](//freedigitalphotos.net/)" >}}

_Tento článek je překladem originálu
[Being the Geek Who Fits](//pragprog.com/magazines/2013-03/being-the-geek-who-fits),
který byl publikován v březnovém čísle časopisu
[PragPub Magazine](//pragprog.com/magazines/2013-03). Článek byl poskytnut
s laskavým svolením Andyho Lestera._

_This article is a translation of the original
[Being the Geek Who Fits](//pragprog.com/magazines/2013-03/being-the-geek-who-fits),
which has been published in the March issue of the
[PragPub Magazine](//pragprog.com/magazines/2013-03).
The article has been provided with a kind permission of Andy Lester._

Absolvovali jste několik kol pohovorů na tuhle zajímavě vypadající práci a právě jste dostali nabídku. Peníze jsou slušné, takže řeknete "ano", správně? No, možná ne.

Řekněme, že jdete na schůzku naslepo a věci se vyvíjejí dobře. Jste spolu na večeři a ona se vás ptá na všechno možné, aby zjistila, jaká jste osobnost a jaké máte plány do budoucna. Prostě takové ty věci. Pak spolu jdete na další schůzku, ale tentokrát si s sebou přivede svého bratra a rodiče. Všichni čtyři vás začnou bombardovat otázkami o vašem životě. Potom, po několika hodinách, vám řekne: "Rozhodla jsem se, že jsi pro mě ten správný partner. Pojďme se vzít." Řekli byste právě v tuto chvíli ano? Byli byste blázni, ne?

Zkuste se zamyslet, jak často lidé přijímají pracovní nabídku na základě toho, že je někdo pár hodin griloval a pak jim bylo řečeno, že byli shledáni adekvátními.

Akceptování nabídky od zaměstnavatele je začátek dlouhého vztahu. Je to vztah, kdy strávíte víc hodin v kanceláři, než trávíte bdělí se svým partnerem. Měli byste se ujistit, že to je pro vás ta pravá práce.

Dělání vaší práce, to je pouze polovina vašeho života v zaměstnání. Stejně tak, jako vás šéf hodnotí podle vaší práce a spolupráce s ostatními, byste i vy měli hodnotit společnost podle vaší práce a toho, jak společnost funguje vůči vám. Potřebujete posoudit firemní kulturu.

Firemní kultura není o lidech. Ale lidi jsou její součástí. Pokud je například někdo, koho jste potkali u pohovoru pitomec, tak tam nejspíš nebudete chtít pracovat. Nicméně, to nemusí být dobré kritérium, protože lidé přicházejí a odcházejí a ten pitomec už může být příští týden v jiném zaměstnání. Kultura je konzistentnější. V tomto případě není problémem ten pitomec, ale firemní kultura, která pitomce toleruje.

Většina vašeho porozumění firemní kultuře bude pocházet z rozhovorů během přijímacího řízení. Pamatujte, že interview není výslech, kde jsou kandidátovi kladeny otázky a jinak by měl zůstat zticha. Je to konverzace mezi potenciálními kolegy, kteří diskutují business potřeby organizace a jak by kandidát mohl tyto potřeby naplnit a také, jak by kandidát do organizace zapadl. Oba, kandidát i organizace si musí vzájemně sednout, jinak je tento vtah odsouzen k zániku.

Nedělejte si úsudek o kultuře na začátku přijímacího procesu. Detailní otázky byste si měli schovat, až dostanete nabídku ke spolupráci. Většina vašeho času a energie během přijímacího procesu by měla být zaměřena na získání nabídky. Pokud selžete v získání nabídky, tak vůbec nezáleží na tom, jaká je firemní kultura.

Jakmile ale máte nabídku, rozložení sil se mění ve váš prospěch a nyní je správný čas kulturu detailně prozkoumat. Ptejte se, potkejte se s lidmi a snažte se co nejvíc pozorovat.

## Ptejte se

Tady je několik otázek, na které byste, jak budete postupně poznávat společnost, asi rádi znali odpovědi. Uvědomte si, že nejlepší cesta, jak získat odpovědi, nemusí být přímá otázka. Tazatel nezjišťuje, jestli jste pracovitý otázkou "Jste pracovitý?", protože odpověď bude samozřejmě "Ano". Budou ho zajímat příklady, které ilustrují vaši pracovní morálku. Obdobně, když budete chtít vědět, jak společnost řeší krize, zeptejte se, kdy naposled skončil nějaký projekt špatně. Můžete se na tuto otázku zeptat více lidí, ne pouze přijímajícího manažera.

V těchto otázkách používám slovo _skupina_ ve smyslu samostatná pracovní skupina v rámci oddělení, ale tyto otázky lze aplikovat také na úrovni oddělení nebo společnosti. Taky si uvědomte, že žádná z těchto otázek by neměla implikovat, že jeden způsob práce je lepší než druhý. Například úzce provázaný tým není lepší nebo horší než skupina nezávislých pracovníků. Pouze _vy_ rozhodujete, jestli je to kultura, jejíž chcete být součástí.

## Pracovní otázky

První věc ke zvážení, vzhledem ke kultuře, jsou otázky o tom, jak věci fungují, o firemních hodnotách, o tom, jaký je každodenní život.

* Jak úzce provázaná je skupina? Kolik práce se dělá týmově a kolik individuálně?
* Jaké jsou pracovní hodiny? Jak často je potřeba pracovat mimo tyto běžné pracovní hodiny? Je tento extra čas považován za součást práce, nebo je na něj nahlíženo jako na dodatečné úsilí?
* Co skupina oceňuje na ostatních? Co váš nový šéf oceňuje na skupině, nebo oddělení? Čeho si společnost cení na svých zaměstnancích?
* Jsou nepodařené projekty součástí života? Nebo jsou důvodem k vyhazovu?
* Jak tým/oddělení/společnost řeší krize?
* Jak vypadá vnitrofiremní komunikace? Jsou zde striktně vymezené informační kanály, nebo jsou zaměstnanci podněcováni, aby mluvili s ostatními odděleními napřímo?
* Je skupina otevřená změnám a novým technologiím? Nebo preferují stabilitu známého prostředí?

## Vztahové otázky

Další část je ještě mlhavější, přemýšlení o tom, jak si lidi rozumí a jaké mají vztahy. (Nenechte se splést, máte vztah s každou osobou, se kterou přijdete do styku a to dokonce i když si myslíte, že jste jenom kodér s očima přilepenýma na obrazovku. Dokonce i přímé vyhýbání se vztahům s ostatními je v podstatě vztahem.)

* Jaké jsou vztahy ve skupině? Mají se lidé navzájem rádi, nebo jsou vztahy striktně pracovní? Jsou lidé přáteli i mimo skupinu, nebo jsou pracovní a domácí vztahy oddělené?
* Je ve skupině dynamika, která přesahuje práci, kterou je potřeba udělat? Chodí skupina společně na obědy? Jsou zde pravidelné páteční hospody? Víkendové výlety na lezeckou stěnu? Budete divný, když nepůjdete lézt s nimi?
* Jaká je v oddělení fluktuace? Jak často se tým obměňuje? Přijdete do zavedeného týmu, který je už roky spolu?
* Jsou ve skupině nějaké vůdčí osobnosti, ať už formální, či neformální? Nebo jsou si všichni rovni?
* Kritizují členové týmu navzájem svoji práci? Nebo dělá každý věci po svém?

## Život společnosti

Důležité je také porozumět kultuře společnosti jako celku a jak do ní skupina, k níž se připojíte, zapadá.

* Jak bude hodnocena vaše výkonnost? Je vaše hodnocení založeno pouze na vaší výkonnosti, nebo zahrnuje i vyhodnocení celého oddělení, nebo celé společnosti?
* Jsou zaměstnanci odměňováni i jinak než finančně? Jak? Pizza party? Cadillac Eldorado? Sada steakových nožů? Jsou zde bonusy, od čeho se odvíjejí? Od výkonu vašeho týmu? Celé společnosti?
* Jak je tým přijímán zbytkem oddělení? Jak je oddělení přijímáno zbytkem společnosti?

Opět, tyto otázky nejsou seznamem, který si odškrtáte na interview. Jsou výchozím bodem pro zvažování otázek, během vašeho poznávání společnosti, pro kterou byste pracovali.

## Další cesty, jak pozorovat kulturu

Jsou i další, méně přímé způsoby, jak získat přehled o firemní kultuře a jsou občas informačně daleko přínosnější. Jeden z takových způsobů je nahlédnout do manuálu zaměstnanců. Požádejte o to přijímajícího manažera. Dejte si čas si ho pročíst. Je to tlustý šanon plný specifických regulací, jako třeba kde v kóji můžete mít pověšený obrázky? Nebo druhý extrém, kdy vůbec nemají stanovenou politiku docházky a říkají pouze "dokud máš hotovou práci, je všechno v pořádku"?

Mají kulturu rozpoznávání? Já mám emailovou složku nazvanou "Pochvalné zmínky", kde shromažďuji všechno, co hezkého, o mně nebo mém týmu, řekl někdo ze společnosti. Spousta těchto zmínek je od vyššího managementu, kde děkují mé skupině za úspěšný projekt. Má přijímající manažer podobný archiv? Zeptejte se, jestli můžete vidět nějaké aktuální ukázky.

Požádejte o návštěvu kanceláří během pracovního dne. Co vidíte? Jsou lidé spokojení? Mají hlavy zabrané do práce, nebo probírají věci jako tým? Je tu spousta kójí, nebo mají kanceláře? Jsou tam veřejná místa pro spolupráci? Je to sterilní, upnuté prostředí, nebo po sobě lidé střílejí z dětských pistolek?

Nedívejte se pouze na skupinu, kde budete pracovat. Navštivte třeba účtárnu, nebo zákaznický servis. Vypadají, že jsou tam šťastní?

A konečně, porozhlídněte se na internetu. Začněte s firemní stránkou a blogem. Například i jen zběžné přečtení GitHub blogu dává jasně najevo, že hodně kultury se točí kolem společenského pití. Možná najdete blog současných nebo bývalých zaměstnanců, kde může být nějaké vodítko. Hledejte na LinkedIn profily těch, kdo ve společnosti pracovali. Možná zjistíte, že lidé, kteří tam pracovali, odchází do jednoho roku.

A poslední poznámka: bez ohledu na to, co jste zjistili o kultuře a jak skvěle daná práce vypadá, nikdy neakceptujte nabídku okamžitě. Počkejte minimálně 24 hodin, abyste se ujistili, že je to to nejlepší pro vás. Jste emocionálně vypjatí, nastartovaní a to není dobrý rámec pro finální rozhodnutí. Lepší je trochu poodejít a podívat se na situaci z nadhledu. A je to o to zásadnější, pokud máte v životě ještě něco dalšího, co je podstatné.

Když dostanete nabídku, řekněte "Mockrát děkuji, jsem tímto výsledkem velmi potěšen. Samozřejmě, nyní potřebuji trochu času, abych nabídku zvážil. Jaký čas se vám zítra hodí, abychom si promluvili?" Nebojte se, že by svoji nabídku stáhli. Každá rozumná společnost to pochopí. Pokud by vás tlačili do okamžitého rozhodnutí, je to varovné znamení.

_Andy Lester vyvíjí software přes dvacet let, jak v business oblasti, tak na webu v rámci open source komunity. Léta strávená proséváním životopisů, pohovorováním nepřipravených kandidátů a také vlastní nerozumná kariérní rozhodnutí, ho dohnaly k napsání [jeho netradiční knihy](//www.pragprog.com/refer/pragpub45/book/algh/land-the-tech-job-you-love) o nových způsobech hledání zaměstnání. Andy je aktivním členem open source komunity a žije v oblasti Chicaga. Bloguje na [petdance.com](//petdance.com/) a je k zastižení na emailu <a href="mailto:andy@petdance.com">andy@petdance.com</a>._

_Andy Lester has developed software for more than twenty years in the business world and on the Web in the open source community. Years of sifting through résumés, interviewing unprepared candidates, and even some unwise career choices of his own spurred him to write [his nontraditional book](//www.pragprog.com/refer/pragpub45/book/algh/land-the-tech-job-you-love) on the new guidelines for tech job hunting. Andy is an active member of the open source community, and lives in the Chicago area. He blogs at [petdance.com](//petdance.com/), and can be reached by email at <a href="mailto:andy@petdance.com">andy@petdance.com</a>._

## Související odkazy

* [Jak dělám java pohovor](/2012/11/jak-delam-java-pohovor/)
* [Jak dělají java pohor jinde](/2013/02/jak-delaji-java-pohovor-jinde/)

## Související externí odkazy

* [Otázky u pohovoru](//blog.zvestov.cz/item/114) (Banter bloguje)
* [Jak si (ne)zruinovat životopis](//www.dagblog.cz/2012/10/jak-si-nezruinovat-zivotopis.html) (Dagblog)

Pokud jste na svém blogu (ne firemním) publikovali článek se souvisejícím
tématem (pracovní pohovory, CV apod.), napište mi ~~do komentářů~~
[na Twitter](//twitter.com/sw_samuraj), rád vás odkážu.
