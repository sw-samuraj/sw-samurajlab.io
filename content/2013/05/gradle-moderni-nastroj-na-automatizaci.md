+++
title = "Gradle, moderní nástroj na automatizaci"
date = 2013-05-08T23:21:00Z
updated = 2017-06-17T11:53:41Z
description = """Gradle je nástroj na automatizaci. Potřebujete udělat build,
    mít Continuous Integration, zprovoznit deployment, generovat dokumentaci,
    připravit release, dojít nakoupit a vyvenčit psa? Gradle je to pravé pro vás!"""
tags = ["gradle", "build"]
aliases = [
    "/2013/05/gradle-moderni-nastroj-na-automatizaci.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2013/08/Gradle.png" >}}

[Gradle](//www.gradle.org/) je nástroj na automatizaci. Potřebujete udělat build, mít [Continuous Integration](//en.wikipedia.org/wiki/Continuous_integration), zprovoznit deployment, generovat dokumentaci, připravit release, dojít nakoupit a vyvenčit psa? Gradle je to pravé pro vás! Gradle je něco jako Ferrari, Land Rover a Mini Cooper v jednom. A funguje to.

## Gradle? Co to je?

Gradle je nástroj na automatizaci. Čehokoliv. Zmáčknete tlačítko a vypadne banán :-)  Nejčastější využití asi bude na build nějakého projektu. Ale není potřeba se svazovat jen touto představou --- jakmile potřebuju něco zautomatizovat, můžu na to použít Gradle (třeba na generování a odesílání měsíčních výkazů).

Gradle ve skutečnosti není automatizační nástroj --- je to _jazyk_ pro automatizaci. Je to [Domain Specific Language](//en.wikipedia.org/wiki/Domain-specific_language) (DSL), který nám umožňuje popsat to, co chceme zautomatizovat. Nepřekvapí, že Gradle je založený na [Groovy](//groovy.codehaus.org/) (jestliže byl Groovy v něčem úspěšný (oproti svým konkurentům z oblasti [dynamických JVM jazyků](//en.wikipedia.org/wiki/List_of_JVM_languages#High-profile_languages)), tak je to právě jeho využití ve sféře DSL).

Výchozí těžiště Gradlu leží v buildování. V tomto směru na něj lze nahlížet jako na nástroj další generace v linii Ant -&gt; Maven -&gt; Gradle, přičemž ze svých předchůdců si bere (a kombinuje) to nejlepší: z Antu jeho sílu a flexibilitu, z Mavenu [konvence](//maven.apache.org/maven-conventions.html), [dependency management](//maven.apache.org/guides/introduction/introduction-to-dependency-mechanism.html) a [pluginy](//maven.apache.org/plugins/).

Jádro Gradlu samo o sobě toho moc neumí --- zkuste si po instalaci pustit příkaz `gradle tasks`. Distribuce Gradlu ale obsahuje sadu [standardních pluginů](//www.gradle.org/docs/current/userguide/standard_plugins.html), takže out-of-the-box je k dispozici podpora pro Javu, Groovy, Scalu, Web, OSGi aj. Další funkčnosti jsou k mání přes [komunitní pluginy](//wiki.gradle.org/display/GRADLE/Plugins).

Tak jako spousta skvělých věcí v životě i Gradle je zadarmo --- je k dispozici s licencí [Apache License, Version 2.0](//www.gradle.org/license).

## Je opravdu tak dobrý?

{{< figure class="floatright" src="/2013/05/Gradle-references.png" >}}

Pokud jste se s Gradlem ještě nestkali, možná jste na pochybách, jestli má smysl se jím vůbec zabývat. Vždyť přece buildujeme Mavenem/Antem, tak na co další další nástroj? Třeba vás přesvědčí, že mezi uživatele Gradlu patří např. Spring, Hibernate, Grails, Software AG, nebo LinkedIn.

Dalším důvodem, proč u Gradle zpozornět je, že byl již podruhé uveden v prestižním(?) přehledu ThoughtWorks [Technology Radar](//www.thoughtworks.com/insights). V tom aktuálním z [října 2012](//thoughtworks.fileburst.com/assets/technology-radar-october-2012.pdf) je Gradle umístěn v sekci _Trial_, což znamená _"Worth pursuing. It is important to understand how to build up this capability. Enterprises should try this technology on a project that can handle the risk."_

Konkrétně o Gradlu a buildovacích nástrojích je zde napsáno:

> "Two things have caused fatigue with XML-based build tools  like Ant and Maven: too many angry pointy braces and the  coarseness of plug-in architectures. While syntax issues can  be dealt with through generation, plug-in architectures severely  limit the ability for build tools to grow gracefully as projects  become more complex. We have come to feel that plug-ins are  the wrong level of abstraction, and prefer language-based tools  like **Gradle** and Rake instead, because they offer finer-grained  abstractions and more flexibility long term."

**[update 7. 6. 2013]** Aktuálně vyšel nový ThoughtWorks [Technology Radar](//thoughtworks.fileburst.com/assets/technology-radar-may-2013.pdf) (květen 2013), kde se Gradle posunul do sekce _Adopt_, čili: _"We feel strongly that the industry should be adopting these items. We use them when appropriate on our projects."_ **[/update]**

## My Way

O Gradlu jsem se dozvěděl v roce 2009, kdy jsem byl na [přednášce CZJUGu](//java.cz/article/26493) v tehdejším Sunu (R.I.P.). Měl jsem to štěstí, že přednášejícím byl [Hans Dockter](//www.gradleware.com/team) --- zakladatel a leader Gradlu (Hansova původní prezentace je v [archivu CZJUGu](//java.cz/dwn/1003/26898_czjug_2009.pdf)).

Gradle se mi tehdy zalíbil. Vyzkoušel jsem ho a použil na pár vlastních drobných Java a Groovy projektech. Gradle byl tehdy ve verzi [0.8](//www.gradle.org/docs/0.8/userguide/userguide.html) a když se podíváte na [historii verzí](//en.wikipedia.org/wiki/Gradle#Version_History), je vidět, že to byla velmi čerstvá záležitost.

Mezitím urazil Gradle dlouhou cestu: 7. května vyšla verze 1.6 a osobně si myslím, že Gradle je dostatečně zralá technologie pro nasazení do enterprise prostředí. Nejsou to plané řeči --- na posledním projektu jsem zvažoval, na čem postavit release management a po zralé úvaze, kdy ve hře byl kromě Gradlu také Maven, Ant, čisté Groovy a shell skripty, jsem zvolil právě Gradle jako řešení, které nejvíce vyhovovalo dané situaci a bylo dostatečně robustní, srozumitelné a rozšiřitelné, aby fungovalo i po mém odchodu z projektu.

Právě při přípravě release managementu tohoto projektu jsem se musel do Gradlu důkladně ponořit, protože jsem musel vyřešit některé ne úplně triviální záležitosti. Abych nabyté vědomosti nějak zúročil, uspořádal jsem rozlučkovou přednášku o Gradlu ve svém bývalém zaměstnání. Přednáška je k dispozici na SlideShare:

{{< figure src="/2013/05/Gradle-SlideShare.png"
    link="//www.slideshare.net/slideshow/embed_code/key/8d1p7b71dqS0EL" >}}

## Tutorial

No a abych nabyté vědomosti ještě více zúročil :-)  rozhodl jsem se napsat o Gradlu tutorial, který budu na svém blogu postupně publikovat. Jednak bych si formát tutorialu rád vyzkoušel, ale hlavně bych se chtěl o své zkušenosti podělit. Právě sdílení informací je jedna z věcí, které mne (jako seniorního vývojáře) baví.

Pokud se již nemůžete dočkat první lekce, můžete si čekání ukrátit [instalací](//www.gradle.org/installation) Gradlu. V tutorialu budu používat nejaktuálnější verzi 1.7, která je dostupná v [nočních buildech](//www.gradle.org/nightly), ale můžete použít i stabilní verzi.

{{< figure src="/2013/07/Gradle-version.png" >}}

Pokud máte nějaké téma, které byste rádi v tutorialu viděli, napište mi a já se ho pokusím někam zařadit. No a samozřejmě budu rád za každý komentář.

**[update 7. 6. 2013]** Domluvil jsem se se serverem [Zdroják](//www.zdrojak.cz/), že budu (přednostně) tutorial publikovat i tam (seriál [Gradle --- moderní nástroj na automatizaci](//www.zdrojak.cz/serialy/gradle-moderni-nastroj-na-automatizaci/)). Vzhledem k "publikační mašinerii" tak budou jednotlivé díly vycházet s většími prodlevami, než jsem původně počítal. Ale co to je z pohledu věčnosti? :-) **[/update]**

## Související články

* [ThoughtWorks Radar, zajímavé technologie](/2011/08/thoughtworks-radar-zajimave-technologie/)
