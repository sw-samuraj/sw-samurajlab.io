+++
title = "Cesta samuraje, rok druhý"
date = 2013-05-04T15:54:00Z
updated = 2013-05-04T21:44:24Z
description = """Blog SoftWare Samuraj má druhé narozeniny. Tradiční retrospektiva."""
tags = ["sw-samuraj"]
aliases = [
    "/2013/05/cesta-samuraje-rok-druhy.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/img/samurai.gif" >}}

Tak je to tady, _SoftWare Samuraj_ má druhé narozeniny. A protože je hezké mít nějakou tradici, tak si teď jednu založím. Takže, roční rekapitulace. (Mimochodem, přiznanou inspirací mi v tomto byl [N-tý rok Myšlenek Otce Fura](//www.google.cz/webhp?ie=UTF-8#hl=en&amp;q=rok+myšlenek+site:novoj.net).)

Takový souhrn lidé většinou píší k novému roku. Přiznám se, já nemám rád novoroční předsevzetí. Ale rekapitulace je trochu něco jako _lesson learned_, takže věc jistě užitečná. Navíc, kolikrát si člověk (pro samou práci) ani neuvědomí, jakou cestu už urazil.

## Jak to bylo

Když jsem se ohlížel zpět [před rokem](/2012/04/cesta-samuraje-rok-prvni/), neměl jsem ještě moc jasno, o čem bych chtěl psát, jak blog zaměřit. Taky jsem psal jen příležitostně. Mezitím jsem začal psát pravidelně. Poslední dobou jsem se ustálil na rytmu tři články za měsíc. Což mi časově vychází a doufám, že to vyhovuje i vám, mým čtenářům. Pozitivní je, že témat je stále dost, vyskakujou jako houby po dešti --- mám jich běžně v zásobě kolem deseti a pořád neubejvaj. Dokonce musím čas od času některé i zahodit.

Celkové skóre za minulý rok je 31 publikovaných článků. Silnými tématy byly jednak technické věci, zastoupené nejčastěji prolínající se trojicí [SOA](/tags/soa/), [integrace](/tags/integrace/), [messaging](/tags/messaging/) a jednak věci personální --- téma [pohovorů](/tags/interview/). Technická témata byla daná hlavně projektově, kde jsem vytěžil několik článků o [Oracle SOA Suite](/tags/soa%20suite/), [WLST](/tags/wlst/) a [Kanbanu](/tags/kanban/). Personální témata byla v mém pracovním (nejenom projektovém) životě také často přítomna --- dělal jsem desítky pohovorů na několika různých úrovních, plus inspirativní zkušenosti z [team leadingu](/tags/teamleading/). No a tradičně živné téma pro mne byly samozřejmě (odborné) [knihy](/tags/knihy/).

Co cítím jako resty, zbylo mi několik témat z oblasti [SOA governance](/tags/governance/), SOA designu a [Perforce](/tags/perforce/) (P4). Možná se ještě k něčemu z toho vrátím (no, P4 asi ne).

## Wind of Change

Jak to tak v životě chodí, jednou za čas přichází změna. Poslední projekt pro mne byl velmi intenzivním zážitkem. V tom pozitivním smyslu to byly technologie a částečně lidský faktor. V tom negativním to bylo projektové řízení. Ten negativní aspekt byl tak působivý, že se tento projekt pro mne stal katalyzátorem odchodu --- a to nejen z projektu, ale také z firmy, kde jsem pracoval 4,5 roku.

O tom projektu určitě něco napíšu, dost možná, že to bude i série článků (tak vydatný zdroj inspirace to byl ;-) Ale nechám tomu nějaký čas --- když člověk píše o negativních věcech, měl by od toho mít odstup. A taky si k tomu chci načíst nějakou literaturu.

## Vizionářské okénko

[intermezzo] Víte, kdo byl (podle mne) _nejlepší_ "počítačový" vizionář? Steve Jobs. Jsem linuxák a Steva jsem nikdy nežral. Ale to co dokázal je úctyhodné (hezky to sepsal třeba [Petr Staníček](//pixy.cz/pixynergia/2011/10/07/jaky-byl-vlastne-prinos-steva-jobse)).

A víte, kdo je (pro mne) _nejhorší_ "počítačový" vizionář? Bill Gates. (OK, jsem linuxák... ale třeba Win 7, nebo MS SQL Server používám rád.) Chudák, tomu chlápkovi snad nikdy žádná předpověď nevyšla. A za ten jeho špílec _"640K ought to be enough for anybody."_ by mu měli postavit pomník. (No, prej to možná neřek. Ale stejně je to dobrá hláška. A někdo ji říct musel.) [/intermezzo]

Já se samozřejmě s uvedenými pány nebudu srovnávat. Jen si trochu zavěštím, o čem byste si mohli přečíst v příštím roce. Některé věci budou přímo spjaté s mojí novou prací, takže bych čekal něco o _security_ (zatím nemůžu být ~~víc specifický~~ konkrétnější), něco o verzovacím systému [Mercurial](//mercurial.selenic.com/) a možná i nějaký [JBoss 7](//www.jboss.org/jbossas). A opět team leading.

Mezi témata spjatá s prací volněji (anebo vůbec) patří nástroj na automatizaci [Gradle](//www.gradle.org/), o kterém bych chtěl napsat tutorial a také bych se chtěl vrátit k [Hadoopu](//hadoop.apache.org/) (což se mi pořád ne a ne podařit). Tak uvidíme. O čem byste si rádi přečetli vy?

## Související články

* [Cesta samuraje, rok první](/2012/04/cesta-samuraje-rok-prvni/)
* [Druhý rok s Kindlem](/2013/03/druhy-rok-s-kindlem.html)
