+++
title = "Jak dělají Java pohovor jinde"
date = 2013-02-25T13:53:00Z
updated = 2017-03-10T21:09:12Z
description = """Prošel jsem několik pracovních pohovorů a sepsalsi
    svoje zkušenosti. Bylo to zajímavé srovnání s tím, jak dělám technický
    pohovor já."""
tags = ["teamleading", "interview", "java"]
aliases = [
    "/2013/02/jak-delaji-java-pohovor-jinde.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
thumbnail = "2013/02/Interview.jpg"
+++

{{< figure class="floatleft" src="/2013/02/Interview.jpg"
    link="/2013/02/Interview.jpg" width="400" >}}

Nedávno jsem psal o tom, [jak dělám Java pohovor](/2012/11/jak-delam-java-pohovor/) já. Bylo to napsáno z pohledu toho, kdo zná kontext, ví, jak by měl vhodný kandidát vypadat a také mmj. určuje pravidla, jak bude pohovor probíhat.

Aktuálně mám několik čerstvých zkušeností z opačné strany interview, tak bych se chtěl podělit o své dojmy. Na (Java) pohovoru jsem nebyl již několik let, v podstatě od "krize", kdy jsem si testoval stav trhu :-) a musím říct, že to byla zajímavá profesní zkušenost.

Budu diskrétní, takže nebudu prozrazovat konkrétní zadání a ani nebudu jmenovat jednotlivé firmy. Pro lepší představu pouze uvedu, že ve všech případech šlo o nadnárodní korporace, kde je běžným komunikačním jazykem angličtina. Také se budu věnovat pouze technickým kolům pohovorů, tzn. že různé HR a psychologické srandičky si odpustím.

No, a protože moje paměť je děravá a výběrová, zaměřím se pouze na signifikantní rysy daného pohovoru. Taky vynechám takové ty běžné věci, jako je probírání projektů nad CV.

Co bych předeslal, že měly všechny pohovory společné:

* nikde po mně nechtěli vidět můj kód,
* nikde po mně dopředu nechtěli nějakou specifickou přípravu
* a nikde po mně (naštěstí) nechtěli psaní testů.

## Algoritmy na white boardu

Tohle byla taková algoritmová klasika --- měl jsem (za předpokladu, že neexistují [Java Collections](//docs.oracle.com/javase/tutorial/collections/index.html)) naimplementovat [spojitý seznam](//en.wikipedia.org/wiki/Linked_list). S povděkem jsem si vzpomněl na školní předmět _Algoritmy a datové struktury_ a i po těch cca šesti letech vydoloval celkem slušnou implementaci.

Implementace probíhala na [whiteboardu](//en.wikipedia.org/wiki/Whiteboard), doplňovaná diskuzí. Debata byla uvolněná a nebazírovalo se na termínech. Výhodou whiteboardu je, že kromě kódu se dají kreslit různé pomocné schématka apod., nad kterými se dá dále diskutovat. A je to takové agilní :-)

Závěr: tohle je celkem příjemná metoda pohovoru, která ale staví na více méně fixních a teoretických znalostech. Navíc tam není moc prostoru pro invenci. Schválně, kolikrát už jste (v Javě) implementovali nějaký _základní_ algoritmus a kdy jste naposledy vymysleli nějakou novou metodu [sortování](//en.wikipedia.org/wiki/Sorting_algorithm)? Nicméně, pokud člověk nepřijde nabiflovaný algoritmy, dá se touto metodou sledovat, jak uvažuje a dobírá se řešení.

## Checklist technologií

* _"Jak dlouho máte zkušenost s J2EE?"_
* "8 let."
* _"Jak dlouho máte zkušenost s JSF?"_
* "Rok a půl."
* _"Musíme to zaokrouhlit na celý roky."_
* "OK, tak rok."

Na začátku tohohle interview byl seznam asi patnácti technologií, kdy jsem měl říct, jak dlouhou s nima mám zkušenost. Zkušenost musela být vyjádřená v celých rocích. Pak jsme jednotlivé technologie procházeli pomocí kontrolních otázek.

Např. u Java Core to byl vztah metod [equals](//docs.oracle.com/javase/7/docs/api/java/lang/Object.html#equals(java.lang.Object)) a [hashCode](//docs.oracle.com/javase/7/docs/api/java/lang/Object.html#hashCode()). U JPA tři způsoby dotazování ([JPQL](//docs.oracle.com/javaee/6/tutorial/doc/bnbtg.html), [Criteria API](//docs.oracle.com/javaee/6/tutorial/doc/gjitv.html) a [native SQL](//docs.oracle.com/javaee/6/api/javax/persistence/EntityManager.html#createNativeQuery(java.lang.String))) apod. Platné byly pouze zkušenosti z projektů. Např. to, že mám [Flex certifikaci](/2012/03/flex-certifikace/) a navrhoval jsem architekturu, kde byl Flex jako frontend, nic neznamenalo (člověk by řekl, že aspoň jednooký mezi slepými králem).

Závěr: pro mne jednoznačné nejhorší zkušenost --- člověk je jenom souhrnem aktuálních znalostí, vůbec se nepočítá s nějakým potenciálem, schopností řešit problémy, vymýšlet řešení apod. Navíc ten checklist mi přijde hrozně zavádějící: když jsem [Spring Security](//static.springsource.org/spring-security/site/) použil na dvou projektech v délce cca 3 let, ale čisté práce na něm bylo maximálně 3 týdny (konfigurace plus nějakej ten custom handler) --- jak dlouhou zkušenost mám s touto technologií?

## Algoritmy a rozhraní

Na tomto pohovoru jsem dostal dva příklady. První se týkal "jistého" vyhledávacího algoritmu, implementovaného pomocí dvou zanořených cyklů (mmch. jediný kód, byť na papíře, který jsem na pohovorech viděl). Měl jsem určit, jestli algoritmus pracuje _vždy_ správně, jaká je jeho [složitost](//en.wikipedia.org/wiki/Big_O_notation#Orders_of_common_functions), jak by se dal optimalizovat, jaká bude jeho složitost po optimalizaci atd.

Druhý příklad se týkal návrhu rozhraní a ev. jeho implementace. Zadání bylo (schválně) velmi volné a strohé, aby podnítilo přemýšlení a diskuzi. Požadavek byl pro mne lehce cizokrajný a příslušná (zevrubná) diskuze mne celkem obohatila (i když výsledné řešení bylo celkem konformní).

Závěr: tohle interview se mi profesně líbilo nejvíc --- šlo nejvíc do hloubky, testovalo jak znalost algoritmů, tak přemýšlení nad nejednoznačným problémem. Čili otestovalo jak základní znalosti, tak myšlenkový potenciál.

## Moje otázky

Na všech pohovorech jsem se hodně ptal a strávili jsme mýma otázkama poměrně dost času. Měl jsem vždycky připravenou stejnou sadu otázek:

* Jaká je struktura společnosti?
* Jak velké je oddělení, kde bych pracoval?
* Jaký je budoucí kolekiv?
* Kdo bude můj šéf a uvidím ho na některém kole pohovoru?
* Jak vypadá moje budoucí pracoviště?
* Jakým způsobem jsou vedeny projekty?
* Jaké se používají metodiky?
* (obligátní) Jaké se používají technologie?
* Jaké podpůrné technologie se používají (Continuous Integration, Issue Tracking, Version Control)?
* Funguje nějaký osobní rozvoj (školení atd.)?
* Jak budou vypadat další kola pohovoru?

Mmch. před časem psal o otázkách při pohovoru [Banter](//blog.zvestov.cz/item/114).

## Závěr

Moje dojmy z pohovorů jsou samozřejmě subjektivní a zpětná vazba je většinou minimální --- člověk se většinou dozví, jestli byl celkově úspěšný nebo ne, ale nedozví se podrobnosti. Proto se můžu ve svém náhledu mýlit. Protože nastavení pohovoru a jeho vyhodnocení je v rukou tazatele.

## Související články

* [Jak dělám Java pohovor](/2012/11/jak-delam-java-pohovor/)
