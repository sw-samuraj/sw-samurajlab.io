+++
title = "Kanban, zprávy z fronty"
date = 2013-01-23T10:37:00Z
updated = 2017-03-10T21:09:12Z
description = """SoftWare Samuraj se pokusil o svoji první implementaci Kanbanu
    na projektu - stylem pokus omyl a nedopadlo to úplně špatně. Zkušenost je
    pozitivní a příště bych to zkusil znova."""
tags = ["teamleading", "kanban", "agile", "sw-engineering"]
aliases = [
    "/2013/01/kanban-zpravy-z-fronty.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2013/01/Kanban-logo.png" >}}

V létě jsem psal o tom, jak na mne nečekaně spadla implementace [Kanbanu](//en.wikipedia.org/wiki/Kanban_(development)) (post [Kanban z čistého nebe](/2012/07/kanban-z-cisteho-nebe/)). Turbolence na projektu mě mezitím zavály jinam, nicméně aby moje investice nepřišla nazmar, udělám si jakési _lesson learned_.

## Co je to Kanban?

Protože tenhle článek je hlavně o implementaci Kanbanu, uvedu pouze krátkou definici pro neznalé. Odkazy pro další informace o Kanbanu najdete v článku [Kanban z čistého nebe](/2012/07/kanban-z-cisteho-nebe/).

Takže. Kanban je _metoda_ pro limitování rozpracované práce (work-in-progress), která vychází z  [Lean developmentu](//en.wikipedia.org/wiki/Lean_software_development) a je definovaná pěti základními principy:

1. Vizualizuj workflow.
1. Limituj rozdělanou práci.
1. Měr a spravuj flow.
1. Udělej procesní politiky explicitní.
1. Používej modely pro rozpoznání příležitosti ke zlepšení.

## Obecně o projektu

Úkolem daného projektu bylo dodat sadu webových služeb (cca 60 business služeb). Daná technologie je z hlediska Kanbanu nepodstatná. Služby by měly splňovat základní SOA pradigmata --- být autonomní, volně propojené ([loose coupled](//en.wikipedia.org/wiki/Loose_coupling)), s velkou granularitou ([coarse-grained](//en.wikipedia.org/wiki/Granularity)). Implementované služby byly zasazeny do klasické [middleware](//en.wikipedia.org/wiki/Middleware) architektury, která vypadala velmi zhruba takto:

{{< figure src="/2013/01/MiddleWare.png" >}}

Vývoj služeb byl trekován v JIRA: co služba, to ([GreenHopper](//www.atlassian.com/software/greenhopper/overview)) user story. Z hlediska projektu (Kanbanu) byly všechny dodávané služby unifikované --- jejich dodávka se sestávala vždy ze stejných kroků, což Kanbanu (ale stejně tak třeba i Scrumu) vyhovuje. Jednotlivé kroky byly v JIRA vedeny jako technické subtasky dané user story.

1. **Kontrakt.** Definice WSDL a příslušných XSD schemat.
1. **Mock.** Jednoduchá implementace služby bez orchestrace, business logiky a volání backendů, tak aby frontendové komponenty měly vůči čemu vyvíjet.
1. **Implementace.** Plná implementace služby včetně orchestrace, volání backendů, číselníkových překladů atd.
1. **Unit testy.** Unit testy validací, orchestrace, business logiky, fault handlingu apod.
1. **Dokumentace.** Jednak sebepopisná dokumentace kontraktu, jednak model orchestrace služby v [CASE](//en.wikipedia.org/wiki/Computer-aided_software_engineering) nástroji.
1. **Continuous Integration.** Build, deployment, unit testing a undeployment v [CI](//en.wikipedia.org/wiki/Continuous_integration) nástroji.

Dodávka služby končila nasazením služby na SIT (System Integration Test) prostředí.

## Něco málo o týmu

Na projektu jsme začínali dva. Pak to bez varování vyskočilo až na neúnosných 17 lidí, aby se to po nějakém čase ustálilo na rozumných 10 vývojářích.

Tým byl tvořen z 90 % seniorními vývojáři, z nichž všichni se s Kanbanem setkávali poprvé. Lidé v týmu byli přátelsky a pozitivně naladěni a naštěstí se nenašel žádný kverulant, který by se metodiku snažil sestřelit.

## Implementace 5 principů Kanbanu

Následujících pět sekcí je gró tohoto článku, aneb jak jsme se vypořádali s implementací jednotlivých principů Kanbanu.

### 1) Visualize Workflow

{{< figure class="floatright" src="/2013/01/GreenHopper-logo.png" >}}

Vizualizuj workflow. Workflow jsme měli ve dvou provedeních, ve vztahu master-slave. "Pravda" byla v issue tracking nástroji [JIRA](//www.atlassian.com/software/jira/overview), kde jsme používali agilní plugin [GreenHopper](//www.atlassian.com/software/greenhopper/overview) (jeho [Kanban část](//www.atlassian.com/software/greenhopper/overview/kanban)). JIRA byla mým hlavním nástrojem pro řízení týmu.

{{< figure src="/2013/01/GreenHopper-board.png"
    link="/2013/01/GreenHopper-board.png"
    caption="Elektronický JIRA Rapid Board" >}}

JIRA má pro klasický _Kanban Board_ vlastní název --- _Rapid Board_. Vstupem do něj je běžný JIRA filtr, v obrázku výše jsou vyfiltrované pouze technické subtasky.

Obrazem elektronického workflow v JIRA byl fyzický Kanban Board, vytvořený z [whiteboardu](//en.wikipedia.org/wiki/Whiteboard) a lepicích lístečků. Každý člen týmu měl za úkol synchronizovat své úkoly v JIŘe s fyzickou tabulí.

{{< figure src="/2013/01/Kanban-board.jpg"
    link="/2013/01/Kanban-board.jpg"
    caption="Fyzický Kanban board" >}}

Podobně jako v JIŘe byly vyfiltrovány pouze technické subtasky, tak i na fyzické tabuli byly jenom subtaskové lístečky. Subtasky z jedné user story se v sekci _Done_ lepily na sebe a když byla user story kompletní, nahradily se lístečky lístkem jiné barvy, který reprezentoval danou user story.

### 2) Limit Work-in-Progress

{{< figure class="floatleft" src="/2013/01/GreenHopper-WIP.png" >}}

Limituj rozdělanou práci. Nastavení správného počtu úkolů, které mohou být v jednu chvíli rozpracovaný, je jedním z nejtěžších úkolů v Kanbanu. Hodně tady může napomoct [teorie front](//en.wikipedia.org/wiki/Queueing_theory) a samozřejmě empirické zkoušení, testování a měření.

V Kanbanu se někdy nastavuje pouze horní limit, já jsme nastavil horní i spodní a sice spodní limit byl počet vývojářů (velikost týmu mínus teamleader) a horní limit na dvojnásobek vývojářů. Každý vývojář tak musel mít přiřazený minimálně jeden úkol, ale maximálně mohl mít rozpracovaný pouze dva tasky.

{{< figure src="/2013/01/WIP.jpg" >}}

### 3) Measure and Manage Flow

Měr a spravuj flow. Flow se v Kanbanu typicky zobrazuje pomocí [diagramu Cumulative Flow](//en.wikipedia.org/wiki/Cumulative_flow_diagram), což dokáže i JIRA. Na obrázku níže představují barvy jednotlivé sekce ve flow: oranžová --- _To Do_, modrošedá --- _In Progress_, zelená --- _Done_.

Diagram má dva důležité rozměry. Horizontální délka dané sekce (nebo sekcí) říká, jak dlouho byl průměrně úkol v odpovídajícím stavu. Pokud vezmu například oranžovou a modrošedou barvu (= _To Do_ + _In Progress_), říká mi tím diagram, jak (průměrně) dlouhý čas uběhl od zadání tasku do JIRy po jeho implementaci. Pokud bych vzal pouze úkoly ve stavu _In Progress_, dostávám velocitu tasku --- jak dlouho trvala implementace úkolu.

Vertikální délka dané sekce říká, kolik bylo úkolů v dané sekci v určitý čas. V případě tasků v _In Progress_ by to mělo odpovídat limitům WIP.

Ideálním stavem pro Kanban je, pokud je "tloušťka" tasků v _In Progress_ (šedomodrá) více méně konstantní a rovnoměrně rostoucí. Alarmujícím příznakem je zvětšující se sekce _To Do_, což znamená, že se nám fronta začíná zahlcovat.

{{< figure src="/2013/01/GreenHopper-cumulative-flow.png"
    link="/2013/01/GreenHopper-cumulative-flow.png"
    caption="Diagram Cumulative Flow v JIRA" >}}

Tolik teorie. V praxi tady vidím (u sebe) dluh, protože jsme nedokázali měřit velocitu týmu. JIRA sice umí vytvořit diagram kumulativního flow, ale neumožňuje s těmi daty nijak pracovat, tj. nejsem schopen spočítat, kolik tasků je tým schopný udělat např. za týden (velocita týmu) a jak průměrně dlouho trvá implementace úkolu. (Kdybych se mýlil a věděli byste, jak tyto údaje z JIRy dostat, dejte mi, prosím, vědět v komentářích.)

Inkriminované údaje by se daly například spočítat v Excelu, ale ten se mi nechtělo ručně vést, takže jsem na jejich zjištění rezignoval a pouze vizuálně kontroloval diagram, jestli nedochází k nějakým extrémním výkyvům.

Co se týká správy flow, průběžně jsem ho měnil podle zpětné vazby od týmu a také podle vlastní úvahy, jak jsem nad ním přemýšlel a pracoval s ním. Jako příklad můžu uvést nové stavy _Blocked_, _In Test_, nebo _To Review_.

**[update 13. 2. 2013]** V komentáři jsem dostal odkaz na výborný obrázek, kde je Cumulative Flow diagram krásně vysvětlený:

{{< figure src="http://4.bp.blogspot.com/-qGKEMYa5CvQ/URtdEDdxorI/AAAAAAAADZE/x3yYCuDO1NY/s1600/Kanban+Cumulative+Flow.jpg"
    caption="Jak rozumět diagramu Cumulative Flow (zdroj [Paul Klipp](//paulklipp.com/images/cfd.jpg))" >}}

**[/update]**

### 4) Make Process Policies Explicit

Udělej procesní politiky explicitní. Procesní politiky jsme měli v podstatě pouze dvě, jednu z nich nepsanou. Ta definovala jak a kdo je zodpovědný za jednotlivé tasky v průběhu flow. Ačkoliv vznikl její model (viz další bod), nebyl do týmu v daném čase publikován.

Kromě toho existoval na týmové wiki dokument s [Definition of Done](//www.scrumalliance.org/articles/105-what-is-definition-of-done-dod) (DoD) pro jednotlivé technické subtasky. Tento dokument byl na wiki z toho důvodu, že jak už jsem psal výše, jednotlivé user story byly unifikované, takže nebylo potřeba uvádět DoD pro každý subtask do JIRy, ale dal se použít jeden centrální dokument vůči kterému šlo úkoly validovat.

### 5) Use Models to Recognize Improvement Opportunities

Používej modely pro rozpoznání příležitostí ke zlepšení. Tenhle ten princip jsem dosti dlouho ignoroval, protože jsem nevěděl jak ho uchopit a z počátku mi přišly daleko důležitější první tři principy Kanbanu. Nicméně, jeden model jsem nakonec vystřihnul. Jeden obrázek je za tisíc slov, takže jak fungovalo "samoobslužné" zpracování tasků:

{{< figure src="/2013/01/JIRA-flow.png"
    link="/2013/01/JIRA-flow.png"
    caption="Model flow" >}}

Barvy v obrázku odpovídají jednotlivým stavům v diagramu cumulative flow (To Do, In Progress, Done). Pokud přišel požadavek na novou službu, team leader jej rozpadl na jednotlivé subtasky (kontrakt, mock atd.) a zadal je do JIRy, kde je zprioritizoval.

Vývojáři si potom sami brali prioritní tasky a implementovali je. Hotové úkoly, ve stavu _Done_, pak reviewoval team leader.

Zlepšení flow, které mne díky tomuto jednoduchému modelu napadlo (ale už jsem neměl čas je zrealizovat) je přesunutí _Review_ na roli vývojáře. Podobně, jako si vývojáři sami brali nové tasky, brali by si sami i tasky na review.

Jak se totiž v praxi ukázalo, _Review_ bylo úzkým hrdlem flow. Jeden člověk (team leader) bude těžko zvládat dělat review a ještě držet agendu 10-15 lidí v týmu. Což je logické, byť to nemusí být na první pohled zřejmé.

## Pravidelné týmové aktivity

Samotný Kanban neřeší další týmové náležitosti, ale pro lepší dokreslení řízení projektu je tady taky uvedu.

### Denní stand-up

Ráno jsme dělali klasický [stand-up meeting](//en.wikipedia.org/wiki/Stand-up_meeting). Nepoužívali jsme Scrumovské: "co jsem dělal včera a co budu dělat dneska?" Protože Kanban je zaměřený na flow a odstraňování jeho překážek, zněla otázka:

_Co budu dneska dělat a co mi v tom brání?_

### Týdenní code review

Jednou týdně, v pátek, probíhalo [code review](//en.wikipedia.org/wiki/Code_review), kdy se procházely kontrakty, implementace služeb, unit testy, zkrátka věci definované v Definition of Done. Review probíhalo (s celým týmem) check-outem z verzovacího systému, procházení kódu v IDE a jeho promítání na stěnu, tak aby to viděl celý tým.

### Týdenní design review

Jednou týdně, ve středu, probíhalo design review, kdy se procházely návrhy orchestrací služeb, jejich implementací apod. Review probíhalo (s celým týmem) u [whiteboardu](//en.wikipedia.org/wiki/Whiteboard), kde se ručně kreslilo zadání a diskutoval design.

## Shrnutí

Jak už jsem psal v úvodu, Kanban byl pro mne novinka a z počátku jsem k němu přistupoval opatrně. Přece jenom, pár zmršených implementací Scrumu už jsem viděl a Kanban je něco ještě daleko novějšího.

Nicméně, snažil jsem se k tomu přistoupit zodpovědně a myslím, že se nám Kanban podařilo naimplementovat rozumně, korektně a funkčně.

Pokud bych to měl nějak celkově shrnout, myslím, že Kanban je životaschopný způsob, jak řídit vývojářský tým. Je vhodný nejenom pro supportní typy tasků (jak se obvykle traduje), ale i pro vývoj nových funkčností a systémů. Ve velmi dobré symbióze funguje Kanban s vývojem middlewarových služeb, protože požadavky na nové a úpravu stávajících služeb se jaksi přirozeně frontují.

Pokud bych tedy měl někdy v budoucnu volnou ruku ve výběru metodiky, o Kanbanu bych velmi vážně uvažoval.

## Související články:

* [Kanban z čistého nebe](/2012/07/kanban-z-cisteho-nebe/)
* [Kanban, ultimátní kniha](/2012/08/kanban-ultimatni-kniha/)
* [Lean ze zákopů](/2012/08/lean-ze-zakopu/)
