+++
title = "yum, pár šikovných příkazů"
date = 2018-05-05T12:32:00Z
updated = 2018-05-05T12:32:32Z
tags = ["linux"]
draft = true
blogimport = true 
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

<div dir="ltr" style="text-align: left;" trbidi="on"><div class="separator" style="clear: both; text-align: center;"><a href="https://3.bp.blogspot.com/-zB4T0g1qvxg/Wu10nSL-NZI/AAAAAAAAeiM/MB0IYi372kk1nqkbHLRJlxIKkNqHHYRxgCLcBGAs/s1600/yum.png" imageanchor="1" style="clear: left; float: left; margin-bottom: 1em; margin-right: 1em;"><img border="0" data-original-height="70" data-original-width="185" src="https://3.bp.blogspot.com/-zB4T0g1qvxg/Wu10nSL-NZI/AAAAAAAAeiM/MB0IYi372kk1nqkbHLRJlxIKkNqHHYRxgCLcBGAs/s1600/yum.png" /></a></div>Nevím, čím to je, ale nikdy jsem nepřišel na chuť linuxům založených, či odklonovaných z <i>Red Hatu</i>. Nebo přesněji, linuxům využívajících balíčkovací systém RPM.<br /><br />Teď, v nové práci, jsou RPM artefakty buď finální, nebo parciální <a href="https://en.wikipedia.org/wiki/Deliverable">deliverables</a>. Máme <a href="http://yum.baseurl.org/wiki/RepoCreate">yum repositoy</a>, kam nahráváme sestavené RPM artefakty, a kterou dále používáme pro tvorbu dalších, downstream artefaktů, třeba <i>Docker</i> (a jiných) imagí.<br /><br />Jelikož se teď podílím na designu nového <a href="https://en.wikipedia.org/wiki/Continuous_delivery">Continuous Delivery</a> (via <a href="https://docs.gitlab.com/ee/ci/pipelines.html">GitLab Pipelines</a>), tak jsem se do používání <span style="font-family: &quot;Courier New&quot;, Courier, monospace;">yum</span> trochu víc namočil. Zatím mi chybí zkušenosti a tak si sepisuju takový "pamatováček". Budu rád, když se v komentářích podělíte o své <span style="font-family: &quot;Courier New&quot;, Courier, monospace;">yum</span> vychytávky.<br /><br /><h2 style="text-align: left;">Repository definition</h2>Pokud si potřebujete přidat novou repository...<br /><br /><h2 style="text-align: left;">Externí odkazy</h2><ul style="text-align: left;"><li><a href="http://yum.baseurl.org/wiki/Guides">yum guides</a></li><li><a href="https://access.redhat.com/articles/yum-cheat-sheet">yum cheat sheet</a></li></ul><br /></div>
