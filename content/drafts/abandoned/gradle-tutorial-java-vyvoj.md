+++
title = "Gradle tutorial: Java vývoj"
date = 2013-07-30T11:42:00Z
updated = 2017-06-17T11:53:41Z
tags = ["gradle", "tutorial", "build", "java"]
draft = true
blogimport = true 
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

<div dir="ltr" style="text-align: left;" trbidi="on"><div class="separator" style="clear: both; text-align: center;"><a href="http://1.bp.blogspot.com/-Du-gtt00uQY/UYo4-8Ey6uI/AAAAAAAADkw/8kDHeYSYGAQ/s1600/Gradle.png" imageanchor="1" style="clear: left; float: left; margin-bottom: 1em; margin-right: 1em;"><img border="0" height="60" src="http://1.bp.blogspot.com/-Du-gtt00uQY/UYo4-8Ey6uI/AAAAAAAADkw/8kDHeYSYGAQ/s200/Gradle.png" width="200" /></a></div>qwer<br /><br /><h3 style="text-align: left;">Zdrojové kódy</h3><div>Zdrojové kódy k dnešnímu dílu jsou k dispozici na <a href="https://bitbucket.org/sw-samuraj/gradle-tutorial">Bitbucketu</a>. Můžete si je tam buď <a href="https://bitbucket.org/sw-samuraj/gradle-tutorial/src/228d1e85f4b0?at=published">probrouzdat</a>, stáhnout jako <a href="https://bitbucket.org/sw-samuraj/gradle-tutorial/get/published.zip">ZIP archiv</a>, anebo - pokud jste cool hakeři jako já :-) - naklonovat <a href="http://mercurial.selenic.com/">Mercurialem</a>:</div><pre class="clj">hg clone ssh://hg@bitbucket.org/sw-samuraj/gradle-tutorial<br /></pre><br /><h3 style="text-align: left;">Co nás čeká příště</h3>qwer<br /><br /><i>Tento článek byl původně publikován na serveru <a href="http://www.zdrojak.cz/">Zdroják</a>.</i><br /><br /><h3 style="text-align: left;">Související články</h3><ul style="text-align: left;"><li><a href="http://www.sw-samuraj.cz/2013/05/gradle-moderni-nastroj-na-automatizaci.html">Gradle, moderní nástroj na automatizaci</a></li><li><a href="http://www.sw-samuraj.cz/2013/07/gradle-tutorial-tasky.html">Gradle tutorial: tasky</a></li><li><a href="http://www.sw-samuraj.cz/2013/07/gradle-tutorial-tasky-pokracovani.html">Gradle tutorial: tasky (pokračování)</a></li></ul><br /><h3 style="text-align: left;">Související externí články</h3><ul style="text-align: left;"><li><a href="http://www.gradle.org/docs/current/userguide/tutorial_using_tasks.html">Build Script Basics</a> (Gradle User Guide)</li><li><a href="http://www.gradle.org/docs/current/userguide/more_about_tasks.html">More about Tasks</a> (Gradle User Guide)</li><li><a href="http://www.gradle.org/docs/current/dsl/org.gradle.api.Task.html">Task</a> (Gradle DSL Reference)</li></ul></div>
