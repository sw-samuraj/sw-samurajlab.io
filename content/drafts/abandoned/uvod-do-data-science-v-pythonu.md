+++
title = "Úvod do Data Science v Pythonu"
date = 2016-11-27T22:06:00Z
updated = 2016-12-02T22:14:09Z
tags = ["python"]
draft = true
blogimport = true 
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

<div dir="ltr" style="text-align: left;" trbidi="on"><div class="separator" style="clear: both; text-align: center;"><a href="https://3.bp.blogspot.com/-vCy1mevBGug/WDtE9W1Tz2I/AAAAAAAAXUQ/qTnelY5o3Ac82TKBbcG54_jvR3tsFfCywCLcB/s1600/Coursera.svg.png" imageanchor="1" style="clear: left; float: left; margin-bottom: 1em; margin-right: 1em;"><img border="0" height="31" src="https://3.bp.blogspot.com/-vCy1mevBGug/WDtE9W1Tz2I/AAAAAAAAXUQ/qTnelY5o3Ac82TKBbcG54_jvR3tsFfCywCLcB/s200/Coursera.svg.png" width="200" /></a></div>Procházím teď v práci určitou obsahovou změnou - sám pro sebe si předefinovávám svoji roli. A tak minimálně ze dvou důvodů jsem na čas skončil u Pythonu a online kurzu.<br /><br />Začnu tím kurzem. Jsem tradiční čtenář - do práce mi to metrem a tramvají trvá cca 40 minut a většinou ten čas trávím čtením knih. Dost často odborných. A chtěl jsem změnu.<br /><br />Poslouchání podcastů mě nikdy moc nebavilo a chtěl jsem něco, co by mě někam posouvalo v učení. <a href="https://www.coursera.org/">Coursera</a> je dobrá volba - mají 4 roky zkušeností s <a href="https://en.wikipedia.org/wiki/Massive_open_online_course">MOOC</a>, dělají to dobře, mají Android aplikaci kde se dají videa stáhnout offline... co víc si přát?<br /><br /><div class="separator" style="clear: both; text-align: center;"></div><div class="separator" style="clear: both; text-align: center;"><a href="https://2.bp.blogspot.com/-n2pNY6XST8c/WDtKbP3rdgI/AAAAAAAAXUg/3IcdjNTJ7UQijGsyl5144xrvy37u4ycVgCLcB/s1600/python-logo.png" imageanchor="1" style="clear: right; float: right; margin-bottom: 1em; margin-left: 1em;"><img border="0" src="https://2.bp.blogspot.com/-n2pNY6XST8c/WDtKbP3rdgI/AAAAAAAAXUg/3IcdjNTJ7UQijGsyl5144xrvy37u4ycVgCLcB/s1600/python-logo.png" /></a></div>A Python? Python je taková moje stará láska.<br /><br /><br /></div>
