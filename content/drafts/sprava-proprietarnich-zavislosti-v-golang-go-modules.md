+++
title = "Správa proprietárních závislostí v Golang: go modules"
date = 2018-08-28T19:35:00Z
updated = 2018-08-28T19:35:47Z
tags = ["golang", "version-control", "git"]
draft = true
blogimport = true 
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

<div dir="ltr" style="text-align: left;" trbidi="on"><br /></div>
