+++
title = "Jak by měly vypadat integrační testy"
date = 2018-07-21T13:54:00Z
updated = 2018-07-21T13:54:12Z
tags = ["testing", "sw-engineering"]
draft = true
blogimport = true 
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

<div dir="ltr" style="text-align: left;" trbidi="on"><div class="separator" style="clear: both; text-align: center;"><a href="https://1.bp.blogspot.com/-uJjhTBxbNro/W1MWz1cn_2I/AAAAAAAAgKo/ir_vLB0x37UytIbmAEaZ7BJyxjyo8skUgCLcBGAs/s1600/mother-board-581597_640.jpg" imageanchor="1" style="clear: left; float: left; margin-bottom: 1em; margin-right: 1em;"><img border="0" data-original-height="480" data-original-width="640" height="150" src="https://1.bp.blogspot.com/-uJjhTBxbNro/W1MWz1cn_2I/AAAAAAAAgKo/ir_vLB0x37UytIbmAEaZ7BJyxjyo8skUgCLcBGAs/s200/mother-board-581597_640.jpg" width="200" /></a></div>Píšeme teď v práci nový produkt, založený na cloudovém řešení, a ačkoli jsme zatím pořád ještě ve fázi prototypování, máme ho částečně pokrytý integračními testy.<br /><br />Ohledně designu integračních testů jsme měli pár diskuzí a ještě nás brzy čeká retrospektiva. A právě kvůli retrospektivě bych si rád sepsal pár bodů, protože upřímně, se stávajícím řešením nejsem, z pohledu softwarového inženýra, spokojen.<br /><br /><br /><br /><i>Použité obrázky mají licenci <a href="https://creativecommons.org/publicdomain/zero/1.0/deed.en">Creative Commons CC0</a> a jsou poskytnuty stránkou <a href="https://pixabay.com/">Pixaby</a>.</i><br /></div>
