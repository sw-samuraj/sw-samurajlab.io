+++
title = "Clojure concurrency: Atoms"
date = 2019-12-27T14:39:44+01:00
updated = 2017-03-28T21:27:46Z
description = """TBD"""
tags = ["clojure", "concurrency"]
blogimport = true 
draft = true
[author]
	name = "Vít Kotačka"
	uri = "https://www.blogger.com/profile/12147991495120816879"
+++

{{< figure class="floatleft" src="/drafts/Clojure-atom.png" >}}

[Atomy](//clojure.org/reference/atoms) jsou nejjednodušším způsobem, jak v Clojure pracovat s neměnitelnými daty. Aby nám suchá, strohá fyzika trošku ubíhala, pojďme si to zpestřit příběhem. Třeba o uhlíku.

## Související články

* [Clojure concurrency: Vars](/2017/03/clojure-concurrency-vars/)
* TBD: Clojure concurrency: Refs
* TBD: Clojure concurrency: Agents
