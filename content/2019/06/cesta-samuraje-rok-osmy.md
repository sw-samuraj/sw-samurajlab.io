+++
title = "Cesta samuraje, rok osmý"
date = 2019-12-13T16:30:44+01:00
description = """SoftWare Samuraj dosáhl satori. Je spokojený a tak nepíše.
    Je tak spokojený, že se mu dokonce ani nechtělo napsat tenhle článek.
    Je spokojený se svou prácí, je spokojený se svou rodinou, je spokojený
    se svým životem. Už vám řekl, že je spokojený?"""
tags = ["sw-samuraj"]
hero_image = "hero.jpg"
+++

{{< figure class="floatleft" src="/img/samurai.gif" >}}

_Disclaimer: Tenhle blogpost jsem napsal už před půl rokem, v červnu. Jenom se mi to
"nějak" doposud nepodařilo publikovat. Ale stihnul jsem to ještě letos, takže se to
počítá._ 🤭

------------

_SoftWare Samuraj_ dosáhl [satori](//en.wikipedia.org/wiki/Satori). Je spokojený a tak nepíše.
Je tak spokojený, že se mu dokonce ani nechtělo napsat tenhle článek. Je spokojený se svou
prácí, je spokojený se svou rodinou, je spokojený se svým životem. Už jsem vám řekl, že je
spokojený?

Tak tedy, aby bylo dokonale jasné, že tenhle článek se ponese jen a pouze
v optimistickém duchu: 🦄🐰🧸☮️✌️☯️

Protože, konec konců, proč se neradovat, když má ~~člověk~~ blog narozeniny!

## Bylo, (v práci), nebylo

{{< figure class="floatleft" src="/2019/06/Gopher-1.jpg"
           link="/2019/06/Gopher-1.jpg" width="100" >}}
{{< figure class="floatleft" src="/2019/06/Gopher-2.jpg"
           link="/2019/06/Gopher-2.jpg" width="100" >}}
{{< figure class="floatleft" src="/2019/06/Gopher-3.jpg"
           link="/2019/06/Gopher-3.jpg" width="100" >}}
{{< figure class="floatleft" src="/2019/06/Gopher-4.jpg"
           link="/2019/06/Gopher-4.jpg" width="100" >}}
{{< figure class="floatleft" src="/2019/06/Gopher-5.jpg"
           link="/2019/06/Gopher-5.jpg" width="157" >}}
{{< figure class="floatleft" src="/2019/06/Gopher-6.jpg"
           link="/2019/06/Gopher-6.jpg" width="100" >}}

Jak už jsem psal v úvodu - ani se mi nechce psát o technologiích. Takže o nich psát nebudu. 🤐

## Nečtu (knihy o programování), jsem neviditelný (na sociálních sítích)

Když jsem zhruba před rokem a půl nastupoval do nové práce, byl to výsledek déle se vyvíjejících
okolností. Jsou to hlavně věci, kterými jsem si musel projít niterně. Navenek jsou ale vidět dvě
podstatné změny.

Za posledních 15-20 let jsem pracovně urazil pořádný kus cesty. Stěžejním důvodem, že jsem nešlapal
na místě, bylo čtení odborných knih. Začínal jsem u těch papírových, které jsem si objednával
z _Amazonu_. Bylo to ještě v dobách, kdy se neplatilo z Ameriky clo a bylo to v dobách, kdy
_Amazon_ prodával hlavně knížky --- žádný cloud ještě neexistoval.

Pak jsem si v roce 2010 koupil svůj první _Kindle_ ([3.generace](https://en.wikipedia.org/wiki/Amazon_Kindle#Third_generation))
a začal jsem odborné knížky číst jenom elektronicky. To bylo (a je výhodné) --- problémem
počítačových knih je (až na pár výjimek), že rychle zastarávají. A mě je líto knihy vyhazovat
a v knihovně mám knih tolik, že je nemám kam dávat. Takže _Kindle_ super! 👍

No a letos... jsem tenhle typ knih přestal číst. Úplně. Po tom kvantu, co jsem přečetl už mi to
přestalo dávat smysl. Vzdělávám se jinými způsoby. Čtu pořád hodně, ale už jen beletrii
a non-ficiton.

Nevím, jestli s tím souvisí i druhá změna, která má každopádně kořeny v mém rodičovství:
opustil jsem sociální sítě. Ve smyslu minimální aktivity. Když má člověk (po určité "odvykačce")
odstup, zjistí, jak málo mu sociální sítě reálně přinášejí. Abych byl pravdomluvný, jedné
sociální síti se pořád ještě věnuji --- [Goodreads](https://www.goodreads.com/), protože...
knížky! 📖🤓📚

## Už zase skáču (a běhám) přes kaluže

{{< figure src="/2019/06/Copenhagen-marathon.jpg"
           link="/2019/06/Copenhagen-marathon.jpg" >}}

Před časem jsem se párkrát na Twitteru pochválil, jak mi zase jde běhání. A taky si
trochu pofoukal bolístku ~~hojícího se~~ zahojeného (lehkého) pracovního vyhoření.
Pravdou je, že než jsem nastoupil do nové práce, zažíval jsem největší krizi své
běžecké kariéry. Vlastně jsem na rok přestal sportovat.

Proto jsem hodně spokojený, že jsem začal opět pravidelně běhat a během uplynulého roku
se vrátil na svou původní výkonnost. Bylo to 1.000 mil tvrdé dřiny. A mám z toho dobrý
pocit.

Po dvou a půl letech jsem znovu běžel maraton --- největší radost mi udělalo zlepšení
osobního času o 10 minut. 💪 A další dva maratony mě letos ještě čekají. Takže,
báječná sezóna!

## Svět je jedna velká permutace

{{< figure src="/2019/06/Rubiks-puzzles.jpg"
           link="/2019/06/Rubiks-puzzles-solved.jpg" >}}

Člověk by se pořád měl učit něco nového. Jak v práci, tak v soukromí. Jedna z výhod
mít děti 🤭 je, že se s nimi můžete vrátit k věcem, které jste dělali dřív, anebo
se naučit něco úplně nového.

Jedna z takových věcí, do kterých jsme se ponořili se 7letým synem, je skládání
[twisty puzzles](//ruwix.com/twisty-puzzles/). Kromě toho, že je to zábava ---
jak mentální, tak fyzická --- a obsahuje spoustu zajímavých problémů (třeba moje
oblíbená [chyba parity](//en.wikipedia.org/wiki/Parity_(mathematics)#Group_theory))
tak, to přináší i zajímavé asociace.

Kostku [3x3x3](//en.wikipedia.org/wiki/Rubik%27s_Cube) složí můj syn rychleji, než
já. Stejně tak v běhání i v programování --- já už nikdy nebudu tak rychlej, jako
ti mladí kluci (a holky). Ale pokud jde o složité, komplexní a dlouhodobé problémy
(algoritmy), tak těžím ze svých zkušeností, znalostí a tvrdé práce. 🤓

## Co bude dál?

Nikdo neví, do čeho se někdy přimotá. Rád bych psal pořád dál. Jenom bych se neměl
posouvat jen já sám... možná bych měl posunout i téma tohoto blogu. 🤔 Párkrát už
jsem ty myšlenky měl. Je to jen otázka, jestli se chci tou cestou vydat.

## Související články

* [Cesta samuraje, rok sedmý](/2018/05/cesta-samuraje-rok-sedmy)
* [Cesta samuraje, rok šestý](/2017/06/cesta-samuraje-rok-sesty/)
* [Cesta samuraje, rok čtvrtý](/2015/05/cesta-samuraje-rok-ctvrty/)
* [Cesta samuraje, rok třetí](/2014/06/cesta-samuraje-rok-treti/)
* [Cesta samuraje, rok druhý](/2013/05/cesta-samuraje-rok-druhy/)
* [Cesta samuraje, rok první](/2012/04/cesta-samuraje-rok-prvni/)
