+++
title = "Remcání proti Javě"
date = 2019-02-01T17:52:54+01:00
description = """Na některé věci potřebuje mít člověk odstup. Odstup a zkušenosti
    z jiných oblastí. Co mě udeřilo do očí, když jsem se po čase vrátil k Javě?"""
tags = ["java"]
hero_image = "hero.jpg"
+++

{{< figure src="/2019/02/Java-rant.png" >}}

Po 3/4 roce práce v _Golangu_ --- což mi vyhovovalo (viz článek [Golang micro-services,
první ohlédnutí](/2018/09/golang-micro-services-prvni-ohlednuti/)) --- jsem se vrátil
na chvíli k _Javě_. Doufám, že jen dočasně. Protože, když člověk na dostatečně dlouhý
čas vypadne z prostředí, kde dlouhodobě pobýval, a vrátí se zpět, najednou mu dojde,
jak jsou některé věci obskurní. A jak některé věci, které byly vymyšleny s nejlepšími
úmysly pomáhat, ve skutečnosti práci zatemňují a ztěžují. Je to, jako když žijete
v prasečáku a už si pak neuvědomujete, že prasata smrdí. 🤭

## Nekonečné přelévání dat mezi vrstvami

{{< figure src="/2019/02/Slinky-toy.jpg" >}}

Určitě to znáte --- pokud jste v posledních 10 letech dělali nějakou "klasickou"
webovou aplikaci v _Javě_, je všechno rozděleno do vrstev a mezi těmi vrstvami
si předáváte data. Data, která přijdou z webové služby, nebo z GUI následně
přechroustáte v business logice a uložíte.

Na každou z těch vrtev nejspíš používáte nějaký framework a ten framework...
očekává data v určitém formátu/struktuře (typicky
[POJO](//en.wikipedia.org/wiki/Plain_old_Java_object), protože OOP, že jo), nebo
s určitými metadaty (anotační rakovina 😈).

Takže ty pořád stejný _hodnoty_ (nejspíš nějaký `String`, nebo `int`) posíláte
kaskádou vrstev tam a zpátky. K tomu je nejspíš(?) pořád(?) validujete a ošetřujete
chyby. (Nebo na to kašlete? 😉). A to všechno jen proto, že vám to diktuje nějaký
framework, nebo [design pattern](//en.wikipedia.org/wiki/Software_design_pattern).

Mimochodem, pokud jste se nad tím někdy zamýšleli, dobrá polovina návrhových vzorů
existuje jenom proto, aby řešila problémy objektového programování. Existují jazyky
(třeba [Ruby](//www.ruby-lang.org/), nebo [Clojure](//clojure.org/)), kde většina
návrhových vzorů nedává smysl. 😂

## Proliferace modelových tříd

{{< figure src="/2019/02/SIM-card.jpg" >}}

S výše zmíněným problémem souvisí, že všechno to přelévání dat mezi vrstvami musíte
realizovat pomocí modelových tříd, které... nejen že na sebe "nesedí" (tuhle jeden
field chybí, támhle druhý přebývá), ale jsou inherentně jiného typu. Takže vezmete
do ruky lopatu a pěkně ručně, nebo nějakým mapovacím frameworkem ty data přehážete
z jednoho modelu do druhého. A to nemluvím o tom, že často ty třídy vracíte
v nějaké kolekci.

Proč je problém, že jsou modelové třídy různého typu? Občas se to tak pěkně sejde,
že máte dvě takovéhle třídy:

{{< gist sw-samuraj 92c7bf90bdffe55cdb62d72ba0a50985 >}}

Všimněte si, že obě třídy obsahují _naprosto identické_ členy --- jak názvem, tak
typem. Je vám to k nečemu? Ne. Prostě vezmete do ruky lopatu... Smutné je, že
většinou nad těmi hodnotami nepotřebujete dělat žádné transformace, chcete jenom
dostat data od uživatele do databáze.

Proč k tomu dochází? Na jedné straně máte dejme tomu _Swagger_, _JAX-RS_ apod.
a na druhém konci je nějaká (No)SQL perzistence, často zastřešená
[ORM](//en.wikipedia.org/wiki/Object-relational_mapping) abstrakcí. Takže
_Hibernate_, _Spring Data_ atd.

Všechny tyhle věci vám generujou, nebo očekávají... hádejte co? Ano, modelové
třídy. A do těch modelových tříd generujou, nebo v nich očekávají... hádejte
co? Ano, metadata (tj. anotace). A od vás, jako programátora, očekávají...
hádejte co? Ano, že vezmete do ruky lopatu a...

## Starý dobrý _Maven_ 🤕

{{< figure src="/2019/02/Old-maven.jpg" >}}

Řeknu vám, že jestli jsem na něčem poslední roky tvrdě pracoval, tak na tom, aby
bylo jasné, že jsem _Maven_ kverulant. Já si prostě nemůžu pomoct --- vždycky,
když vidím nějakého spokojeného uživatele _Mavenu_ (nebo _Applu_ 😉), tak si
musím (přátelsky) rýpnout.

Protože, upřímně soudruzi, který buildovací, nebo automatizační nástroj umožňuje,
ba pobízí uživatele k takovémuto projektovému designu:

{{< figure src="/2019/02/Maven-madness.png" >}}

Je to tak: na **99** _Java_ souborů (z níchž tak třetina až polovina je výše
zmíněný OOP balast) je potřeba **23** _Maven_ konfiguráků! Či jinými slovy,
**17** _Maven_ modulů produkujících JAR. Ani vám nebudu ukazovat projektovou
hierarchii, protože kdybych si vyjel:

{{< highlight none >}}
tree -P pom.xml -I 'src|target'
{{< /highlight >}}

tak se mi to nevejde na obrazovku notebooku. Jen vám řeknu, že ta hierarchie je
tříúrovňová. 🤦‍♂️

No, a kdybych se ještě pustil do toho, jak skvěle se tuní _Maven_ pluginy, tak
bychom tady byli ještě zítra. Jenže to už by nebylo remcání... to by byla svatá
válka! ⚔️  😂

## Kam ten svět spěje?

Řekl jsem dneska něco pozitivního? Ne, neřekl. Ale to neznamená, že bych _Javu_
neměl celkem rád --- za těch 12 let, co jsme spolu zatím strávili, by bylo divné,
kdybychom se čas od času nepoškorpili. Když pominu svoji ženu, tak _Java_ je
vlastně jeden z nejdelších vztahů, který jsem kdy měl. 😈 Za tu dobu, už člověk
ztratil iluze a ví, jak to v životě chodí. A naučí se být trochu tolerantní.
