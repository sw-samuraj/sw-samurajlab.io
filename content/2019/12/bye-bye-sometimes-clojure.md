+++
title = "Bye, bye, Sometimes Clojure"
date = 2019-12-27T16:42:23+01:00
description = """Před 9 lety jsem začal psát blog Sometimes Clojure.
    Dnes jsem ho zrušil. Teda... částečně."""
tags = ["blogging", "clojure", "knihy"]
+++

{{< figure src="/2019/12/Grave.jpg" link="/2019/12/Grave.jpg" >}}

Blog _SoftWare Samuraj_ bude mít na jaře další narozeniny. A ačkoliv --- jak
to sám vnímám --- je to takový blogísek 3. kategorie, mám ho rád a jeho psaní
mě baví. Primárním důvodem, proč jsem ho začal psát a proč v jeho psaní
stále pokračuju, je... sebe-vzdělávání. Nicméně, plné čtyři měsíce předtím,
než jsem začal psát _SoftWare Samuraje_ jsem začal psát jiný blog. 👀

## Asi za to může Pragmatic Programmer

Jedna z prvních knih o softwarovém inženýrství, kterou jsem kdy četl, byl
[Pragmatic Programmer](//www.goodreads.com/book/show/8214124) a spousty
myšlenek v knize obsažených se držím dodnes. Jedna taková myšlenka se
nachází v 5. kapitole _Your Knowledge Portofolio_, speciálně tip č. 8
_Invest Regularly in Your Knowledge Portfolio_:

> **Learn at least one new language every year.**

A tak jsem se přes vánoce 2010 pustil do studia _Clojure_. Byla to zároveň
láska na první pohled a zároveň děsivé setkání s realitou. Matrix hadr ---
objevil jsem paralelní vesmír funkcionálního programování a _Lispu_. 🤯

A abych své studium zúročil a "zaperzistoval". Začal jsem (si) psát blog:

## Sometimes Clojure

_Clojure_ bylo, je a bude minoritním jazykem. Tak to prostě chodí --- ne
každý se může stát rytířem _Jedi_. 🤭 A tak, i když jsem v něm nic produkčního
nenapsal, vždy pro mě bylo silnou inspirací, která se odrážela v myšlení
a programování v jiných jazycích.

Během těch let jsem měl dvě silné vlny, kdy jsem napsal většinu článků
na blogu _Sometimes Clojure_. Jedna byla logicky v počátku, ta druhá
je výsledkem setkání s _Carin Meier_, a.k.a.
[@Gigasquid](//twitter.com/gigasquid) na konferenci v roce 2016.
Pak opět blog upadl v hibernaci.

Celkově se mi podařilo napsat 31 článků
proměnlivé úrovně. Některé jsou naivní a začátečnické, některé nejsou
úplně špatné. Dva mi dokonce uveřejnili na serveru
[Zdroják.cz](//www.zdrojak.cz/n/clojure/).

{{< figure src="/2019/12/Sometimes-Clojure.png"
    link="/2019/12/Sometimes-Clojure.png" >}}

## Bye, bye 👋

Letos jsem se rozhodl do toho konečně říznout a blog _Sometimes Clojure_
zrušit. Nechal jsem vypršet doménu [clojure.cz](//clojure.cz), na které
jsem blog hostoval a postupně převedl _Clojure_ články pod blog
_SoftWare Samuraj_.

Všechny tehdejší články jsou nyní dostupné pod tagem
[clojure](/tags/clojure). Třeba jich časem ještě pár přibyde. 🤞
