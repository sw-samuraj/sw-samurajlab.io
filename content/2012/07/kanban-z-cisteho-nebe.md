+++
title = "Kanban z čistého nebe"
date = 2012-07-10T17:42:00Z
updated = 2017-03-10T21:09:12Z
description = """Přišel jsem ke Kanbanu jak slepý k houslím. Ale já se
    na ty housle hrát naučím! Kde s Kanbanem začít a od čeho se odpíchnout?"""
tags = ["teamleading", "kanban", "knihy", "agile", "sw-engineering"]
aliases = [
    "/2012/07/kanban-z-cisteho-nebe.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatright" src="/2012/07/Sticky-notes.jpg" >}}

Trochu nečekaně jsem dostal na stávajícím projektu na starost nový, teprve vznikající tým. A že prý (nařízeno shora) jedeme [Kanbanem](//en.wikipedia.org/wiki/Kanban_(development)). Inu, proč ne. Akorát že o něm nic nevím. Takže jsem se pustil do studia, protože něčemu nerozumět, to pro mne není příjemný stav.

Pročetl jsem několik internetových článků, z nichž bych vypíchnul:

* [Introduction to Kanban](//www.drdobbs.com/architecture-and-design/introduction-to-kanban/225702051)
* [Kanban Applied to Software Development](//www.infoq.com/articles/hiranabe-lean-agile-kanban) --- aneb od tradičního Kanbanu k softwarovému vývoji.
* [Vizualizing Agile Projects using Kanban Board](//www.infoq.com/articles/agile-kanban-boards) --- pravidlo č. 1: vizualizovat.
* [One day in Kanban land](//blog.crisp.se/2009/06/26/henrikkniberg) --- komiks :-)
* [Tutorial --- Tracking a Kanban Team](//confluence.atlassian.com/display/GH/Tutorial+-+Tracking+a+Kanban+Team) --- [JIRA](//www.atlassian.com/software/jira/overview) tutoriál pro [GreenHopper](//www.atlassian.com/software/greenhopper/overview).

{{< figure class="floatleft" src="/2012/07/Kanban.jpg"
    link="/2012/07/Kanban.jpg" width="200" >}}

Dále, protože jsem nelítostný čtenář, jsem zakoupil knihu. Vybral jsem si titul [Kanban](//www.amazon.com/Kanban-ebook/dp/B0057H2M70/ref=sr_1_2?ie=UTF8&amp;qid=1341932747&amp;sr=8-2&amp;keywords=kanban) :-) od Davida J. Andersona, což je jeden z prvních [early adopters](//en.wikipedia.org/wiki/Early_adopter) Kanbanu v SW vývoji, takže jistě člověk povolaný.

Knížku mám zatím rozečtenou, tak se o ní nebudu předčasně rozepisovat. Ale když už jsem ji zmínil, ocituju z ní principy, na kterých Kanban stojí. Principy samotné jsou vysvětleny ve výše uvedeném článku [Introduction to Kanban](//www.drdobbs.com/architecture-and-design/introduction-to-kanban/225702051) a jelikož k nim zatím nemám z vlastní zkušenosti co podotknout, skromně pomlčím. Tak tedy:

1. Vizualizuj workflow.
1. Limituj rozdělanou práci (work-in-progress).
1. Měř a spravuj flow.
1. Udělej procesní politiky explicitní.
1. Používej modely pro rozpoznání příležitosti ke zlepšení.

Poslední věc, kterou bych zmínil (jako výchozí stanovisko), je nástrojová podpora, kterou máme k dispozici. Jako issue tracking nástroj používáme [JIRu](//www.atlassian.com/software/jira/overview), do které máme plugin [GreenHopper](//www.atlassian.com/software/greenhopper/overview) pro agilní projekt management, který umí mmj. i Kanban.

Zatím jsem si s JIRou dosti hrál, abych vyzkoušel možnosti, které nabízí a prozatím můžu říct, že pro Kanban má dobrou podporu. Jenom není úplně jednoduché to nějak nastavit. Respektive, nastavení je triviální, ale vzhledem k jednoduchosti Kanbanu je potřeba celý proces vymyslet --- Kanban vás v tomto nijak nenavede --- a to jednoduché být nemusí.
