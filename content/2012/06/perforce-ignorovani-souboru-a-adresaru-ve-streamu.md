+++
title = "Perforce, ignorování souborů a adresářů ve streamu"
date = 2012-06-26T15:25:00Z
updated = 2013-04-27T13:17:54Z
tags = ["perforce", "version-control"]
aliases = [
    "/2012/06/perforce-ignorovani-souboru-adresaru-ve.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

<div dir="ltr" style="text-align: left;" trbidi="on"><div class="separator" style="clear: both; text-align: center;"><a href="http://3.bp.blogspot.com/-mCp7woHf4Iw/T8SrgxRv9XI/AAAAAAAADBA/KeaFEDz57is/s1600/p4.png" imageanchor="1" style="clear: left; float: left; margin-bottom: 1em; margin-right: 1em;"><img border="0" src="http://3.bp.blogspot.com/-mCp7woHf4Iw/T8SrgxRv9XI/AAAAAAAADBA/KeaFEDz57is/s1600/p4.png" /></a></div>Jak se nám tak rozrůstá aktuální projekt&nbsp;(a postupuje k nasazování na další prostředí), vyvstala nám potřeba branchovat zdrojový kód. V <a href="http://www.perforce.com/products/perforce">Perforce</a> (P4) se dá jednak klasicky branchovat, nebo se dají používat <a href="http://www.perforce.com/product/product_features/perforce_streams">streamy</a>. Protože streamy poskytují pro správu kódu daleko větší komfort, vybrali jsme si právě tento způsob.<br /><div><br /></div><div>Pro používání streamů je potřeba mít vytvořený streamovaný depot. Ten jsme vytvořili, naimportovali data a ... bylo ještě potřeba nastavit ignore list, protože způsob popsaný v <a href="http://www.sw-samuraj.cz/2012/05/perforce-ignorovani-souboru-adresaru.html">minulém zápisku</a> pro streamovaný depot nefunguje. OK, jak na to?<br /><ol><li>V menu <i>View</i> -&gt; <i>Streams</i>, nebo ikona <a href="http://4.bp.blogspot.com/-iLQS8YbrEwI/T-m0EJLHh2I/AAAAAAAADCo/RE_GEj65I6c/s1600/P4+Stream+icon.png" imageanchor="1" style="margin-left: 0.3em; margin-right: 0.3em;"><img border="0" src="http://4.bp.blogspot.com/-iLQS8YbrEwI/T-m0EJLHh2I/AAAAAAAADCo/RE_GEj65I6c/s1600/P4+Stream+icon.png" /></a>.</li><li>Kliknout pravým tlačítkem na daný stream (obvykle <i>mainline</i>) -&gt; <i>Edit stream &lt;stream&gt;.</i></li><li>Na záložce <i>Advanced</i>&nbsp;nastavit sekci <i>Ignore</i>.</li></ol><table align="center" cellpadding="0" cellspacing="0" class="tr-caption-container" style="margin-left: auto; margin-right: auto; text-align: center;"><tbody><tr><td style="text-align: center;"><a href="http://3.bp.blogspot.com/-rNTGy9EOvr0/T-m2b6Dr1fI/AAAAAAAADC0/83JBy1fnmGQ/s1600/P4+Stream.png" imageanchor="1" style="margin-left: auto; margin-right: auto;"><img border="0" src="http://3.bp.blogspot.com/-rNTGy9EOvr0/T-m2b6Dr1fI/AAAAAAAADC0/83JBy1fnmGQ/s1600/P4+Stream.png" /></a></td></tr><tr><td class="tr-caption" style="text-align: center;">P4 stream s nastaveným ignore-listem</td></tr></tbody></table>Jednou z výhod streamovaného depotu je, že ignore list je nastavený na úrovni streamu. Není tedy potřeba jej nastavovat pro každý workspace zvlášť, tak jako v případě nestreamovaného depotu.</div></div>
