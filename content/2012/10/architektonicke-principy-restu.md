+++
title = "Architektonické principy RESTu"
date = 2012-10-04T05:24:00Z
updated = 2014-06-30T17:59:41Z
description = """Při studiu na Java certifikaci pro webové služby jsem se
    musel připravit i na část zahrnující REST. A jak se líp připravit, než
    sdílením informací?"""
tags = ["web-services", "knihy", "rest"]
aliases = [
    "/2012/10/architektonicke-principy-restu.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
thumbnail = "2012/12/RESTful-Java.jpg"
+++

{{< figure class="floatleft" src="/2012/10/RESTful-Java.jpg"
    link="/2012/10/RESTful-Java.jpg" width="300" >}}

Jedním ze zdrojů, který jsem použil jako přípravu na certifikaci [Java EE 6 Web Services Developer](//education.oracle.com/pls/web_prod-plq-dad/db_pages.getpage?page_id=41&amp;p_exam_id=1Z0_897&amp;p_org_id=1001&amp;lang=US) (o které jsem [psal nedávno](/2012/09/certifikace-java-ee-6-web-services/)), je kniha [RESTful Java with JAX-RS](//www.amazon.com/RESTful-JAX-RS-Animal-Guide-ebook/dp/B0043D2ELI/ref=sr_1_3?ie=UTF8&amp;qid=1341061342&amp;sr=8-3&amp;keywords=jax-rs) od [Billa Burkeho](//bill.burkecentral.com/about-me/). Bill je určitě vhodným autorem, poněvadž byl/je leaderem projektu [RESTEasy](//www.jboss.org/resteasy) od JBossu, což je certifikovaná implementace [JAX-RS](//en.wikipedia.org/wiki/Java_API_for_RESTful_Web_Services).

Co se týká knihy, můžu ji doporučit. Je dobře napsaná, Bill nijak netlačí svoje RESTEasy, ale objektivně zmiňuje i další alternativy --- referenční implementaci [Jersey](//jersey.java.net/) a [Apache CXF](//cxf.apache.org/). Mě osobně trochu mrzelo, že celá druhá polovina knihy je workbook, jakýsi guide k příkladovému projektu --- přece jenom, jak se pohybuju v architektonických sférách, tak to pro mne není až tak přínosný. Nicméně někomu jinému může tato "konrétně implementační" část pomoci dostat se (rychleji) do problému.

Ohledně RESTu jsem byl, až do přečtení knihy, panensky nepoznamenán. Teoreticky již nyní mám načteno (i z jiných zdrojů), ovšem RESTový projekt, který by mne pokřtil ohněm, mě teprve čeká. (To že jsem jednou restovou architekturu navrhl v nabídce, se nepočítá) V tomto postu (aby mi něco z toho v hlavě zůstalo) bych se zaměřil na to, co pro mne bylo nejpřínosnější a nejzajímavější: zcela jednoznačně to jsou architektonické principy RESTu.

Že termín _representational state transfer_ pochází z dizertačky [Roye Fieldinga](//en.wikipedia.org/wiki/Roy_Fielding) (2000), bylo na webu napsáno již milionkrát, takže to tady nebudu omílat a skočím rovnou rovnýma nohama do principů, na kterých stojí.

## Adresovatelnost

Celý REST se točí kolem zdrojů ([resources](//en.wikipedia.org/wiki/Resource_(Web))). Každý takový zdroj by měl být dosažitelný pomocí jedinečného identifikátoru ([URI](//en.wikipedia.org/wiki/Uniform_Resource_Identifier)). Tohle je celkem jednoznačný u dokumentů, aplikací a služeb. Ale když vezmeme v potaz např. Java objekty deployované na aplikačním serveru, tak u nich chybí jednotný/jednoznačný způsob identifikace, který se navíc bude lišit podle různých implementací/dodavatelů (např. JNDI).

Pro identifikaci zdroje se používá [URI](//en.wikipedia.org/wiki/Uniform_Resource_Identifier), které má standardizovanou podobu:

```bash
scheme://host:port/path?queryString#fragment
```

Přičemž `scheme` je protokol (např. HTTP), `host` je DNS nebo IP adresa,
`port` je... port, `path` je virtuální hierarchická struktura, `queryString`
je množina párů `klíč=hodnota`, oddělených znakem `&` a `fragment` je identifikátor,
který označuje nějakou část zdroje (např. nadpis nebo obrázek v dokumentu).

## Jednotné rozhraní

Tak jako se REST točí kolem zdrojů, tak se také točí kolem [HTTP](//en.wikipedia.org/wiki/Hypertext_Transfer_Protocol), jehož jednotlivý metodám připisuje specifický účel a význam.

* [GET](//www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.3) je určena pro read-only operace, jako je načtení seznamu zdrojů (dokumentů), nebo obsahu jednoho zdroje. Je jednak [idempotentní](//en.wikipedia.org/wiki/Idempotent) (lze ji volat opakovaně se stále stejným výsledkem) a jednak bezpečná (safe, nezpůsobuje žádné vedlejší efekty).
* [PUT](//www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.6) umožní daný resource (tělo zprávy/requestu) uložit na serveru na určitém [URL](//en.wikipedia.org/wiki/URL). Předpokládá, že klient zná potřebný identifikátor. Metoda je idempotentní, technicky bývá implementovaná jako insert nebo update.
* [DELETE](//www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.7) odstraní daný resource. Je idempotentní.
* [POST](//www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.5) je funkčně hodně podobná metodě PUT. Zásadní rozdíl je, že je to jediná metoda která _není_ idemopotentní. Čili s každým dalším requestem měním stav daného zdroje/služby.
* [HEAD](//www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.4) je podobná metodě GET, s tím rozdílem, že response nevrací body, ale pouze [response code](//www.w3.org/Protocols/rfc2616/rfc2616-sec10.html) a [HTTP hlavičky](//www.w3.org/Protocols/rfc2616/rfc2616-sec14.html).
* [OPTIONS](//www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.2) vrací komunikační možnosti a schopnosti serveru pro dané URI.

## Orientace na reprezentaci

Při komunikaci pomocí HTTP metod dostáváme a posíláme na dané URI nějakou reprezentaci zdroje. Podle toho, čím je daný zdroj reprezentován, tak dostáváme/posíláme XML, JSON, YAML apod.

Metodou GET například můžu z nějakého URI dostat takovouto reprezentaci:

```xml
<product id="42">
    <name>Kindle Touch</name>
    <currency>USD</currency>
    <price>139</price>
</product>
```

Pokud bych chtěl zdroj na serveru změnit, pak pošlu metodou PUT (nebo POST) na stejné URI následující reprezentaci (změna ceny):

```xml
<product id="42">
    <name>Kindle Touch</name>
    <currency>USD</currency>
    <price>99</price>
</product>
```

Jestli bych použil PUT nebo POST, by záleželo na designu této služby. Obecně, protože PUT je, podle definice, [idempotentní](//en.wikipedia.org/wiki/Idempotence#Computer_science_meaning), tak se používá pro update, POST idempotentní není a používá se pro insert.

Nebo jinak definováno --- s prvním PUT requestem se založí na serveru zdroj a s každým dalším stejným PUT requestem se tento zdroj aktualizuje. Pokud request obsahuje stejná data, zdroj se nijak nemění (to je ta idempotentnost). Naopak s každým POST requestem by měl na serveru vždy vzniknout nový zdroj. To v reálu často nebývá pravda --- POST funguje jako update (stále stejného zdroje), což ale není čistý RESTový design.

## Bezstavová komunikace

Bezstavovost znamená, že žádná klientská data nejsou spravována na serveru, tzn. žádná klientská session. Pokud aplikace potřebuje udržovat stavovost, spravuje si ji sama (jako kdyby to byl tlustý klient) a pouze propaguje na server změněné zdroje.

Mezi běžně uváděné benefity bezstavovosti patří: jednoduchost business logiky, škálovatelnost a cacheování resourců.

## HATEOAS

Význam této ošklivé zkratky je _Hypermedia As The Engine Of Application State_. S pochopením tohoto principu jsem měl největší problém. Myšlenka je jednoduchá --- datový formát, [hypermedia](//en.wikipedia.org/wiki/Hypermedia) (rozšíření [hypertextu](//en.wikipedia.org/wiki/Hypertext)), poskytuje speciální informaci, jak měnit stav zdroje. A dotaženo do konce --- klient by měl komunikovat se serverem pouze pomocí hypermedia a nepotřebuje k tomu žádnou další (technologickou) informaci.

V případě webových služeb je HATEOAS přítomen v podobě odkazů uvnitř (XML/JSON) dokumentů, které představují zdroje. U XML dokumentů se pro odkazy často používá specifikace [Atom](//en.wikipedia.org/wiki/Atom_(standard)) a z ní konkrétně element [link](//tools.ietf.org/html/rfc4287#section-4.2.7).

```xml
<product id="42">
    <link rel="self"
          href="http://amazon.com/product/42"
          type="application/xml"/>
    <link rel="payment"
          href="http://amazon.com/checkout/42"
          type="application/xml"/>
    <name>Kindle Touch</name>
    <currency>USD</currency>
    <price>139</price>
</product>
```

Jak pojmenovávat jednotlivé relace uvnitř odkazů je [částečně standardizováno](//www.iana.org/assignments/link-relations/link-relations.xml), takže se používají např. následující (samopopisná) jména: _chapter, edit, first, icon, last, next, payment, previous, self, stylesheet_ ad.

## Zamyšlení

Musím přiznat, že v rámci teoretického načtení, se mi RESTová architektura hodně líbí, zejména svojí čistotou a jednoduchostí. Je otázka (času), jak se tento můj pohled změní při použití RESTu v praxi. Tu praxi, bohužel, zatím vidím na nějaký menší projektík, protože jak se poslední dobou věnuju [SOA](//en.wikipedia.org/wiki/Service-oriented_architecture) v enterprise sféře, tak musím konstatovat, že REST je v této oblasti opomíjen jak dodavateli řešení, tak zákazníky. Ale snad se časem začne blýskat na lepší časy.
