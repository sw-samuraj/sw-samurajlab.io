+++
title = "Technologické blogování"
date = 2012-11-30T00:29:00Z
updated = 2016-12-02T22:05:10Z
description = """Recenze knihy o technologickém (odborném) blogování.
    Aneb jak se z neznámého, katatonického bloggera stane celosvětová
    hvězda softwarového světa, vydělávající miliony dolarů. 😎"""
tags = ["blogging", "knihy"]
aliases = [
    "/2012/11/technologicke-blogovani.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2012/11/Technical-blogging.jpg"
    link="/2012/11/Technical-blogging.jpg" >}}

</div>Už tomu bude skoro dva roky, co jsem začal blogovat (na SW Samuraj trochu míň). To samozřejmě není mnoho a ještě mě čeká dlouhá cesta.

Na jaře jsem si dělal [malou rekapitulaci](/2012/04/zmena-domeny-mala-rekapitulace/), jak se můj blog vyvíjí a chtěl bych se k tomu pravidelně (ročně) vracet. Tady mě silně inspiroval [Otec Fura](//www.google.com/search?q=%22rok+my%C5%A1lenek+otce+fura%22&amp;oq=%22rok+my%C5%A1lenek+otce+fura%22&amp;ie=UTF-8), byť své shrnutí chci pojímat trochu jinak.

Pro blogování můžou být různé důvody. Třeba pro mne je to způsob, jak se dále vzdělávat, zlepšovat, posouvat své hranice. Z hlediska vzdělávání se, je blogování trochu specifické a trochu to připomíná dávné doby SW inženýrství --- informací je pomálu, ti kdo to pořádně umí, o tom moc nepíšou, dobrou radu aby člověk pohledal (Google moc nepomůže. No, možná jsem se jen málo snažil).

Jako knihomol ocením, když je k nějakému tématu kniha. Et Voilà, letos vyšla kniha, která se věnuje technologickému blogování (přesnější překlad by byl odbornému): [Technical Blogging](//pragprog.com/book/actb/technical-blogging). Její autor [Antonio Cangiano](//antoniocangiano.com/) postupně prochází jednotlivé fáze, kdy se z neznámého bloggera stane světová hvězda vydělávající miliony dolarů :-)

Zas tak divoký to samozřejmě není. Antonio píše velmi čtivě, metodicky a má spoustu dobrých rad, vykoupených vlastní zkušeností. Kniha je v podstatě takové how-to, kdy se do hloubky probírají témata jako:

* **Plánování blogu**. Proč to vlastně chci dělat? Budu psát sám, nebo to bude kolektiv? Bude to obecnější blog, nebo bude specificky zaměřen? Apod. A taky založení domény.
* **Vytvoření blogu**. Antonio doporučuje rozjet vlastní instanci [WordPressu](//wordpress.org/), ale neopomíjí další řešení jako [Blogger](//blogger.com/), [WordPress.com](//wordpress.com/), nebo statické generátory jako [Jekyll](//github.com/mojombo/jekyll).
* **Vyšperkování (vytunění) blogu**. To jsou takový ty serepetičky, co má každý blog v pravý liště a v patičce. Dá se tam sice nacpat skoro cokoliv, ale měly by tam být hlavně věci, které slouží čtenářům (pro lepší orientaci) a autorovi (k rozumné propagaci).
* **Vytváření mimořádného obsahu**. To je alfa a omega každého blogu. Lidi to čtou, protože je to zajímá, pomáhá jim to, baví je to, odpovídá to jejich očekávání atd. Je to v podstatě jednoduché ;-)
* **Pravidelné publikování**. Antonio píše, že je přímá úměra mezi četností a pravidelností článků a množstvím čtenářů --- čím častěji (a pravidelněji), tím víc.
* **Promoting blogu**. Jak se z neznámého autora stát známým (a čteným). Něco o [SEO](//en.wikipedia.org/wiki/Search_engine_optimization), sociálních sítích a komunikaci.
* **Statistiky návštěvnosti**. Aneb jak porozumět výrazům jako _visit, unique visitor, pageview, average pageview, time on site, bounce rate_ a _new visit_. A samozřejmě [Google Analytics](//www.google.com/analytics/).
* **Benefity blogování**. Sem spadá kromě monetizace také promo vlastního businessu, zlepšení skillů a rozvoj kariéry. Plus pár dalších drobností.
* **Škálování**. Věřte nebo nevěřte, blog se dá taky vertikálně a horizontálně škálovat. Buď najmout více lidí (autorů), nebo založit blogovací impérium.
* **Strategie pro sociální média**. Blogy nejsou odříznutý od světa, ale existujou v oceánu internetu, kterému čím dál více vládnou sociální média. Neškodí tedy o nich alespoň něco málo vědět (a využít je).

Tak, komu bych knihu doporučil? Určitě začínajícím a mírně pokročilým blogerům, pro ně bude zdrojem neocenitelných informací. Ostříleným blogujícím mořským vlkům bych spíš poradil investovat těch $20 do něčeho jiného. No a doporučením samo o sobě je edice: [Pragmatic Programmer](//pragprog.com/).

Na závěr ještě pár citátů, který jsem si na [Kindlu](//www.amazon.com/Kindle-Ereader-ebook-reader/dp/B007HCCNJU) podtrhnul:

> Sadly, the most common type of blog is the abandoned one.

> Visitors value eye candy.

> Remember that blogging is mostly about spreading ideas.

> Whatever voice you develop, become a bit of a storyteller. People love strories that go with useful information.

> So-called trolls feed on replies. The good old Usenet mantra says, "Don't feed the trolls."

> When you are researching a topic for an article, you'll be doing focused learning for a specific and practical purpose (something that turns out to be very effective).

> There is a strong correlation between how much you publish and how popular your blog is.

> Building a remarkable online presence takes years, not days.
