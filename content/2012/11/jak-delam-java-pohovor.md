+++
title = "Jak dělám Java pohovor"
date = 2012-11-08T17:10:00Z
updated = 2018-07-21T13:05:42Z
description = """Svého času mi na internetu hodně chyběl popis interview
    z pohledu pohovorujícího. A protože se posledních pár let věnuju
    technickým pohovorům, nechávám nahlédnout do své kuchyně."""
tags = ["teamleading", "interview", "sw-engineering", "java"]
aliases = [
    "/2012/11/jak-delam-java-pohovor.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
thumbnail = "2012/11/Interview.jpg"
+++

{{< figure class="floatleft" src="/2012/11/Interview.jpg" >}}

[UPDATE] _Ačkoliv je tento článek bezkonkurenčně nejčtenější na mém blogu, je **značně zastaralý**. Samozřejmě zde můžete najít nějakou inspiraci, ale doporučuji radši některý z následujících dvou článků, anebo kerýkoli z kategorie_ [interview](//www.sw-samuraj.cz/search/label/interview).

* [Jak dělám Java pohovor IV: Java workshop](/2017/03/jak-delam-java-pohovor-iv-java-workshop/)
* [Jak dělám Java pohovor III: phone screen](/2015/06/jak-delam-java-pohovor-iii-phone-screen/)
[/UPDATE]

Už je to nějaký čas, co jsem četl výbornou knížku [Managing Humans](//www.amazon.com/Managing-Humans-Humorous-Engineering-ebook/dp/B008GTZ4NK/ref=pd_sim_kstore_1), na kterou mne kdysi upozornil post Lukáše Křečana [Vedeme lidské bytosti](//blog.krecan.net/2009/06/21/vedeme-lidske-bytosti/) (můj post [Manažerem humorně a kousavě](/2011/07/manazerem-humorne-kousave/)). Jedna z kapitol, která mě zaujala (a inspirovala) pojednávala o tom, jak [Rands](//www.randsinrepose.com/about.html) (autor) řeší pohovory s kandidáty.

Už delší dobu si pohrávám s myšlenkou, že bych něco pododbného sepsal taky --- jednak, pro lidi, kteří jdou na podobný pohovor, protože takových informací moc není a jednak, abych si udělal jakési interní review. Když před nedávnem o něčem podobném psal Dagi ([Jak si (ne)zruinovat životopis](//www.dagblog.cz/2012_10_07_archive.html#9210922464294543140)), řekl jsem si, že to je správný impuls, podívat se na toto téma zevrubněji.

Abych to uvedl trochu do kontextu, řeknu něco o tom v jaké roli v tom figuruju já. Ač nejsem na manažerské pozici, tak určitá míra neustále se zvyšující seniority :-) mě přivedla k tomu, že lidé z HR, salesu/businessu a naší outsorcové agentury (naše firma mám vlastní) mě začali zvát na pohovory s javistama jako technického přísedícího, což se postupně vyvinulo (v rámci [hiringu](//en.wikipedia.org/wiki/Recruitment)) v regulérní technické java pohovory. V současnosti tak dělám cca  25 java pohovorům ročně.

## CV

Než jdu na pohovor, tak samozřejmě dostanu k dispozici [CV](//en.wikipedia.org/wiki/Curriculum_vitae). Než se CV dostane ke mně, projde rukama ještě jiných lidí, kteří mají různá kritéria výběru, ale podle čeho se rozhoduje paní z [HR](//en.wikipedia.org/wiki/Human_resource_management), nebo pán z agentury, to vám věru neřeknu. Nicméně mě zajímá:

**Jméno.** První co udělám s novým CV, podívám se, jestli má daný člověk (říkejme mu kandidát) profil na [LinkedIn](//www.linkedin.com/). Tam se dají občas zjistit zajímavý (profesní) věci, které v CV nejsou. Třeba doporučení. Nebo že někdo, koho znám já, zná toho kandidáta. Nebo projekty, který v CV nejsou.

**Projekty.** Projekty jsou pro mne gró celého CV. Dá se z toho odvodit, pro jaké firmy a zákazníky kandidát pracoval, jak dlouho byl na daném projektu, někdy jeho role, technologie a domény se kterými se mohl setkat apod.

Většinou mám nějakou představu, na jaký náš projekt by kandidát šel, nebo proč ho vůbec chceme přijmout, takže si v CV podtrhnu inkriminované technologie a ptám se na ně u pohovoru. Taky si můžu podtrhnout technologie, které mne zajímají ve spojitosti se senioritou --- pokud je někdo senior, tak očekávám že u [SVN](//subversion.apache.org/) umí víc než komit a checkout. Zrovna tak u [Mavenu](//maven.apache.org/) čekám, že bude umět víc jak `mvn clean install`. Naopak, pokud si junior napíše do CV [continuous integration](//en.wikipedia.org/wiki/Continuous_integration), tak je nejspší jasný, že to používali na projektu, ale nastavit to neumí.

**Školení a certifikace.** Tohle je pro mne míň podstatné. ale je to střípek do mozaiky. Dělá někdo certifikace? Evidentně na sobě pracuje. Chodí někdo na školení? Evidentně má zajem se vzdělávat a umí si to zařídit. S těma školeníma je to docela zajímavý --- spousta lidí má kvanta školení, který rázem končí rokem 2007. Pak přišla krize a šmitec. Pokud má někdo školení po roce 2008, je dost pravděpodobné, že si to musel vybojovat.

Tak, to bylo, co mě zajímá. A co mě vůbec nezajímá:

**Vzdělání.** Ze zvědavosti se kouknu, ale nepřikládám tomu žádný význam. Obecně, v životě už jsem viděl příliš mnoho vysokoškolsky vzdělaných idiotů. Pokud se budu držet tématu, je tady jakási trojka škol, které by měly kralovat IT --- [Matfyz](//www.mff.cuni.cz/), [FEL](//www.fel.cvut.cz/cz/) a [FI MUNI](//www.fi.muni.cz/index.xhtml.cs). Ale v praxi neznamená škola vůbec nic. Viděl jsem výborného vývojáře z [VŠE](//www.vse.cz/), nebo excelentního vývojáře bez vejšky. (jo, taky jsem takovej bejval :-)

**Zájmy.** Ze zvědavosti se někdy kouknu a nepřikládám tomu žádný význam. Že hraje někdo fotbal? Hm, spíš než "týmového hráče" vidím, že budu v openspace poslouchat jak zase hrála Sparta nebo Barcelona :-/ Má někdo rád literaturu? Hm, co to znamená? Sci-fi, fakta, fantasy, historie, Viewegh, Kierkegaard? Hm, hm, taky čtu knížky (jako miliarda dalších).

**Summary.** Takový to: 15 let praxe, bla, bla, bla... Zcela ignoruju a nikdy nečtu.

## Pohovor

Ještě před pohovorem si nechám od kandidáta poslat ukázku jeho kódu _dle vlastního výběru_. Kód si natáhnu do [IDE](//en.wikipedia.org/wiki/Integrated_development_environment), projdu ho a pomocí [TODO](//en.wikipedia.org/wiki/Comment_(computer_programming)#Tags) si označím, na co se budu ptát u pohovoru (většinou tak kolem pěti věcí). Pak už přijde ten velký den a sejdeme se s kandidátem v jedné místnosti.

Po formalitách --- podání ruky, kafíčko/voda/čaj a ev. přeptání se na cestu, pokud je zdaleka --- zasedneme ke stolu a jdeme na to. Pohovor mám naplánovaný na hodinu a má následující agendu, kterou zmíním hned na začátku:<br />

1. představím se já,
1. [updated] prostor pro kandidátovy otázky, [/updated]
1. projdeme kandidátovo CV,
1. podíváme se na kandidátův kód,
1. podíváme se na můj kód,
1. překvapení na závěr ;-)

Podrobněji:

**1. představím se já.** velice krátce vysvětlím svoji roli a pozici ve firmě. Něco jako: "Já jsem Guido, solution architect a leader Java kompetence, bla, bla." Celkem 3-4 věty.

**2. [updated] prostor pro kandidátovy otázky.** Tady se mně kandidát může na cokoliv zeptat. Bývám velmi otevřený, ovšem jsem schopen zodpovědět pouze technické a projektové otázky. Věci finanční a alokační se řeší v jiných kolech pohovoru. Z osobní zkušenosti můžu říct, že se lidé ptají velmi málo. To, že by někdo měl připravené otázky dopředu se stává velmi výjimečně. Pokud se tak však stane, je to jasné plus.[/updated]

**3. projdeme kandidátovo CV.** Požádám kandidáta, aby se představil z hlediska posledních projektů, které dělal, nebo z hlediska projektů, které mají vztah ke konkrétní požadované znalosti (např. integrace). Tady většinou podrobněji projdu roli, kterou kandidát na projektu měl a vůbec jeho orientaci v tom, jak byl projekt nastavený --- týmy, role, struktura, metodiky apod.

Pak si popopvídáme o technologiích, které jsem si předtím podtrhl v CV (viz Projekty výše). Můžu se zeptat např. na [JPA](//en.wikipedia.org/wiki/Java_Persistence_API): co to je, jak to funguje, nějaký detail. Můžeme jít dost do hloubky, záleží na okolnostech. V této fázi se jasně obnaží, jak moc je CV "vyšperkovaný". Taky je to část, kdy si začnu dělat názor na kandidátovu senioritu.

**4. podíváme se na kandidátův kód.** Promítnu kandidátův kód, tak aby ho všichni viděli a formou diskuze se pobavíme nad věcmi, které jsem si v kódu předtím označil pomocí [TODO](//en.wikipedia.org/wiki/Comment_(computer_programming)#Tags). Většinou si označím věci, které považuju za (potenciální) chyby, chybný design, věci kterým nerozumím, nebo věci který mě zaujmou. Tady vycházejí najevo věci, které jsou hodně technického charakteru. A z jejich severity si opět skládám kandidátovu senioritu.

Jak jsem psal výše, jde o kód _dle kandidátova vlastního výběru_. Předpokládám tedy, že kandidát svůj kód zná a případné dotazy dokáze zodpovědět a rozhodnutí obhájit. Taky předpokládám, že když jde o pohovor, tak pošle ten nejlepší kód, kterým se může prezentovat. Pokud tomu tak není, tak to o něčem vypovídá a pro mne jsou to jasné body dolů.

**5. podíváme se na můj kód.** Před časem jsem dělal jeden "rozsáhlý [refactoring](//en.wikipedia.org/wiki/Code_refactoring)", ehm, bylo potřeba přepsat třetinu aplikace. Protože aplikace byla už nasazená na produkci, byl od zákazníka velký tlak na korektní provedení a review nové verze. Proto vznikl prvně prototyp, který ověřil nový koncept a po jeho akceptaci se teprve implementovala nová verze. Proč o tom mluvím? Ten prototyp je hrozně vymazlený, doslova briliantový --- krystalicky čistý design, kvanta statické analýzy kódu, Javadocy atd.

Právě tenhle prototyp (celý, ne moc velký projekt), natažený v IDE a opět promítaný, dám na notebooku k dispozici kandidátovi a nechám ho nahlas komentovat a nahlas přemýšlet, co ho na projektu/kódu zaujme, co si o tom myslí atp. Tady mám opět nějaká, senioritou daná očekávání, třeba že zkušenější člověk může (sám od sebe) něco říct o layoutu projektu, nebo nastavení [Mavenu](//maven.apache.org/). Pokud řeč vázne, trošku debatu směruju k nějakému zajímavému tématu.

V téhle části nemám dopředu připravené konkrétní věci, které bych chtěl slyšet, nebo na které se ptát. Improvizuju. Což si, myslím, můžu dovolit --- ten kód velmi dobře znám a už předešlá diskuze nad kandidátovým kódem by měla navodit komunikativní atmosféru.

**6. překvapení na závěr.** Posleních cca 10 minut pohovoru a... vytáhnu hlavolam 8-o Mám rád hlavolamy japonské firmy [Hanayama](//hanayama.cz/) a dva z nich si beru na pohovor. Kupuju si ty s těžší řešitelností a jelikož nejsem žádný genius, trvá mi jejich vyřešení hodiny až dny. Postavím tedy dva hlavolamy před kandidáta a hned na úvod ho uklidním, že_ v žádném případě nechci, aby nějaký z nich vyřešil_. Pokud by se mu to náhodou povedlo, byl by to příjemný, ale zcela nepodstatný bonus.

Následně mám pro kandidáta dvoudílné zadání: ať si vybere hlavolam, se kterým mu půjde to zadání lépe naplnit a zkusí slovně popsat:<br />

* samotný hlavolam --- z čeho se skládá, na jakém principu může fungovat apod.
* postup, jak by hlavolam šel vyřešit. Ideálně tak, aby podle tohoto postupu jej byl schopen vyřešit někdo jiný.

{{< figure class="floatright" src="/2012/11/Helix.jpg"
    link="/2012/11/Helix.jpg" width="400" >}}

Hlavolam je tady samozřejmě zástupným problémem. Smyslem téhle části je, jestli je kandidát schopen _poslouchat_ zadání a zkusit ho naplnit. A ne se vrhnout na _řešení_ problému, to zadavatel nechtěl.

U první části navíc pobízím kandidáta, aby se nebál říct byť triviální věci. Ono se může zdát hloupé říct, že nějaká část je kovová, nebo stříbrná. Protože když člověk "drží" problém v ruce "je to přece jasné". Ale pokud to máte někomu jinému vysvětlit (třeba businessu), tak už to tak zřejmé být nemusí.

Pokud se kandidát začne odchylovat od zadání druhé části, zkusím ho navést zpátky příkladem z vodopádového vývoje --- analytik nebo architekt něco zanalyzuje a předá to vývojáři a ten podle toho vyvíjí. Teoretických postupů, jak hlavolam může být řešen je mnoho a fantazii se meze nekladou.

No a to je všechno. Pak už jsou jenom takové ty závěrečné formality a pak sepíšu hodnocení. Ale to už je jiný příběh.

## Závěr

Myslím, že jsem svůj postup popsal celkem otevřeně a dokážu si představit, že někdo nevěřícně kroutí hlavou, proč někdo vyzrazuje své know-how, ba dokonce může ohrozit věrohodnost a smysluplnost celého procesu. Takový přístup chápu, nicméně nejsem jeho stoupencem. Naopak.

Jsem odkojenej [open source](//en.wikipedia.org/wiki/Open_source) a i v manažmentu (v mém případě spíš [teamleadingu](//en.wikipedia.org/wiki/Team_leader)) vyznávám přístup založený na šíření informací, než na jejich zadržování. Stejně tak i ve vývoji, mám rád, když se informace sdílí --- pokud senior mentoruje juniory, tak to není o tom, že jim zadarmo předá svoje znalosti (zkušenosti jsou nepřenosné), ale že tím obohatí i sám sebe. Myslím si, že sdílení znalostí je znakem vyšší formy seniority. V reálu to sice má své hranice (a ani já nejsem [libertarián](//en.wikipedia.org/wiki/Libertarianism)), ale je to legitimní způsob života.

Na a jestli si nějaký budoucí kandidát tenhle postup přečte a připraví se podle toho na můj pohovor? Říkám: skvěle! Pokud se někdo jakýmkoliv (korektním) způsobem připraví na pohovor, je vidět, že chce uspět a to je jednoznačné plus.

A úplným závěrem. Budu moc rád, pokud se v diskuzi podělíte o své názory a zkušenosti, ať už z jedné nebo druhé strany pohovoru.
