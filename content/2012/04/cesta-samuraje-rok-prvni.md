+++
title = "Cesta samuraje, rok první"
date = 2012-04-12T15:57:00Z
updated = 2014-06-29T22:49:14Z
description = """Blog SoftWare Samuraj slaví první narozeniny. Malé ohlédnutí,
    z něhož se časem jistě vyvine silná tradice."""
tags = ["sw-samuraj"]
aliases = [
    "/2012/04/zmena-domeny-mala-rekapitulace.html",
    "/2012/04/zmena-domeny-mala-rekapitulace/"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/img/samurai.gif" >}}

_[update 29. 6. 2014] Změna názvu, původní titul:_ Změna domény a malá rekapitulace _[/update]_

Po roce příležitostného psaní jsem se rozhodl vzít to trochu vážně a tak jsem si pořídil pro svůj blog doménu: [sw-samuraj.cz](//sw-samuraj.cz/). Snad se mi podaří i další krok --- psát trochu pravidelněji.

Cca po roce ještě stále hledám zaměření blogu --- nechtěl jsem zakládat další blog o Javě, což je (kromě architektury) hlavní náplní mé práce. Takových (a kvalitních) blogů už je v Česku dost. Co mi naopak chybí je nějaký blog o softwarovém inženýrství, architektuře apod.

Když si zpětně zrekapituluju témata, o nichž jsem psal, tak nejvíc zápisů bylo inspirovaných odbornými knížkami, [které jsem četl](/2012/03/rok-s-kindlem.html). Většinou byly zaměřeny na, pro mne aktuální, projektová témata. Je to dáno tím, že jsem neúnavný čtenář a čtení je pro mne jedním z primárních zdrojů informací, poznání a osobního rozvoje. Poslední rok mi v tomhle hodně pomohl [Kindle](//www.amazon.com/Kindle-eReader-eBook-Reader-e-Reader-Special-Offers/dp/B0051QVESA).

S osobním rozvojem také souvisí další téma --- [certifikace](/tags/certifikace/). Téma pro někoho kontroverzní, pro mne užitečné. Kromě toho, že certifikace s sebou nesou jisté příjemné bonusy, jsou pro mne přínosné zejména ze studijního/znalostního hlediska, vždycky mě posunuly v dané doméně o slušný kousek dál.

Trochu mne zarazilo, že jsem málo psal o technologických a projektových záležitostech, tak to bych chtěl do budoucna napravit a více se této oblasti věnovat. Technologickým tématem číslo jedna pro mne v uplynulém roce byla určitě [integrace](/tags/integrace/) a zejména [messaging](/tags/messaging/), což bude v následujícím období nejspíše pokračovat (i když půjde spíš o web services).

Na závěr bych zmínil ještě jedno téma, kterému bych se chtěl věnovat --- sice jsem o něm psal jenom jednou, ale je to nejčastější zdroj návštěvníků z vyhledávačů na tomto blogu --- [odhady pracnosti sotfwaru](/2011/05/odhady-pracnosti.html). Řekl bych, že je to téma, které řadu lidí zajímá a přitom neví, jak k němu přistoupit, jak ho uchopit, kde začít apod. Takže myslím, že by mohlo být přínosné, se tomu trochu pověnovat.

Tak. Pokud letos nebude konec světa, za rok zase uvidím, kam jsem se já (i svět) posunul.
