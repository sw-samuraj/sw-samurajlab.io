+++
title = "Leiningen, jak nemít vlasy v ohni"
date = 2012-04-12T12:47:00Z
updated = 2017-03-18T22:04:12Z
description = """Leiningen je buildovací a projektový nástroj pro Clojure,
    který se velmi silně inspiroval Javovským Mavenem. Jeho podtitulem je
    "automating Clojure projects without setting your hair on fire"."""
tags = ["clojure", "build"]
blogimport = true 
[author]
	name = "Vít Kotačka"
	uri = "https://www.blogger.com/profile/12147991495120816879"
+++

{{< figure src="/2012/04/Leiningen.png" >}}

[Leiningen](//github.com/technomancy/leiningen/blob/master/README.md) je buildovací a projektový nástroj pro Clojure, který se velmi silně inspiroval Javovským [Mavenem](//maven.apache.org/) nebo Groovyovským [Gradle](//www.gradle.org/). Jeho podtitulem je _"automating Clojure projects without setting your hair on fire"_. Po vlastních zkušenostech musím říct, že podtitul je zvolen velmi trefně. Leiningen bych rozhodně doporučil, jako startovací bod pro seznamování se s Clojure.

Pravdou je, že prvotní rozchození Clojure je tvrdým oříškem i pro zkušené hackery a pro nováčky může být trochu stresující. (OK, tak pro ty z nás, kteří jsou opravdu dobří, to zas takový problém není ;-)

## Instalace

Instalace na linuxu je triviální (viz [README](//github.com/technomancy/leiningen/blob/master/README.md)):

1. stáhnne se [skript](//raw.github.com/technomancy/leiningen/stable/bin/lein),
1. přidá se do `$PATH`,
1. udělá se spustitelným (`chmod 755 lein`).

Na Windows je to o něco málo složitejší (opět, viz [README](//github.com/technomancy/leiningen/blob/master/README.md)).

Korektní instalaci ověříme příkazem `lein version`:

```bash
Leiningen 1.7.1 on Java 1.7.0_147-icedtea OpenJDK 64-Bit Server VM
```

## REPL

[REPL](//en.wikipedia.org/wiki/Read%E2%80%93eval%E2%80%93print_loop) bývá silnou zbraní skriptovacích jazyků. Nejinak je tomu i v Clojure. Pokud máme nainstalovaný Leiningen, můžeme rovnou začít používat Clojure (bez jeho instalace) příkazem: `lein repl`

REPL ukončíme buď klávesovou zkratkou `Ctrl-C`, nebo příkazem:

```clojure
(System/exit 0)
```

## První projekt

Leiningen umožňuje vytvořit kostru nového projektu pomocí příkazu: `lein new <project>`

{{< figure src="/2012/04/Lein-new.png" caption="Vytvoření a layout projektu" >}}

V layoutu projektu vidíme klasické rozdělení na zdrojový/produkční kód a testy (adresáře `src` a `test`) a také soubor s definicí projektu `project.clj`.

Pokud chceme mít v projektu jiné [namespacy](//clojure.org/namespaces), než je název projektu můžeme použít pro vytvoření projektu syntaxi: `lein new project directory`

{{< figure src="http://2.bp.blogspot.com/-wef6_lp2ls8/T4Vrtu8fP0I/AAAAAAAAC-g/DLfQBZqWAzg/s1600/lein+new+ns.png" caption="Projekt s jiným namespacem" >}}

## Definice projektu

Konfiguračním souborem pro projekt (a Leiningen) je `project.clj`, jehož iniciální podoba je na následujícím výpisu:

```clojure
(defproject my-project "1.0.0-SNAPSHOT"
  :description "FIXME: write description"
  :dependencies [[org.clojure/clojure "1.3.0"]])
```

Nejzajímavější položkou je [keyword](//clojure.org/data_structures#Data%20Structures-Keywords) `:dependencies`, které (překvapivě) definuje závislosti projektu. Podobně jaku v [Mavenu](//maven.apache.org/) jsou závislosti stahovány z repository --- defaultními úložišti jsou [Clojars](//clojars.org/) a [Maven Central](//search.maven.org/). Další repository lze přidat pomocí [keywordu](//clojure.org/data_structures#Data%20Structures-Keywords) `:repositories`.

```clojure
:repositories {
  "my-local" "http://my-local/repository"
  "java.net" "http://download.java.net/maven/2"}
```

Pokud chceme použít některé závislosti pouze při vývoji, použijeme pro jejich definici [keyword](//clojure.org/data_structures#Data%20Structures-Keywords) `:dev-depencencies`. Já ho používám např. pro [Midje](/2012/04/lepsi-testovani-v-clojure-midje/).

```clojure
:dev-dependencies [[midje "1.3.2-SNAPSHOT"]
                   [lein-midje "1.0.8"]]
```

Pokud budeme výsledný projekt distribuovat jako spustitelný JAR soubor, hodí se nám [keyword](//clojure.org/data_structures#Data%20Structures-Keywords) `:main`, které definuje [namespace](//clojure.org/namespaces), ve kterém se nachází funkce `-main` (která bude defaultně zavolána při spuštění `lein run` (viz dále)); a také se zpropaguje do položky `Main-Class` v [MANIFEST.MF](//java.sun.com/developer/Books/javaprogramming/JAR/basics/manifest.html).

Když už jsme u manifestu, existuje i [keyword](//clojure.org/data_structures#Data%20Structures-Keywords) :manifest, které umožní vložit specifické položky do souboru [MANIFEST.MF](//java.sun.com/developer/Books/javaprogramming/JAR/basics/manifest.html).

```clojure
:main blog-lein.core
:manifest {"Build-Version" ~project-version
           "Built-Time" ~(str (Date.))}
```

## Další tasky

Kompletní seznam tasků Leiningenu lze vypsat příkazem: `lein help`

Ty které jsou shodné s [Mavenem](//maven.apache.org/) není potřeba představovat/vysvětlovat:

* `clean`
* `compile`
* `deploy`
* `install`
* `test`

Z těch ostatních jsou zajímavý:

* `classpath` --- vypíše classpath aktuálního projektu.
* `deps` --- dotáhne z repozitářů závislosti definované v `:dependencies` a `:dev-dependencies`. Defaultní umístění je adresář `lib`, ev. `lib/dev` a dá se ovlivnit [keywordem](//clojure.org/data_structures#Data%20Structures-Keywords) `:library-path`.
* `pom` --- vytvoří z `project.clj` soubor `pom.xml` pro [Maven](//maven.apache.org/).
* `repl` --- spustí [REPL](//en.wikipedia.org/wiki/Read%E2%80%93eval%E2%80%93print_loop), buď samostatně (standalone), nebo v rámci projektu --- v tom případě přidá do classpath všechny knihovny definované v `:dependencies`.
* `run` --- spustí `-main` funkci projektu. [Namespace](//clojure.org/namespaces) funkce musí být definován [keywordem](//clojure.org/data_structures#Data%20Structures-Keywords) `:main`.
* `search` --- prohledá repozitáře a vypíše dostupné závislosti.
* `uberjar` --- zabalí všechny soubory a závislosti do jednoho JAR souboru. Vhodné pro standalone distribuci. Pokud má být JAR spustitelný, musí [namespace](//clojure.org/namespaces) s funkcí `-main` obsahovat ve své deklaraci direktivu `:gen-class`.

## Zkušební projekt

Pro studijní důvody jsem vytvořil jednoduchý projekt, na kterém se dají vyzkoušet výše uvedené příkazy (samozřejmě kromě `lein new`):

* [~~blog-lein.zip~~](//docs.google.com/open?id=0B1KFHUMaUcOcRHJUa3loMklEYW8)

_[Update]_ Projekt je k dispozici na Bitbucketu:

* [blog-lein](//bitbucket.org/sw-samuraj/blog-lein)

_[/Update]_

Projekt má následující definici (soubor `project.clj`):

```clojure
(import java.util.Date)
(def project-version "1.0.0-SNAPSHOT")
(defproject blog-lein project-version
  :description "Sometimes Clojure, Leiningen"
  :dependencies [[org.clojure/clojure "1.3.0"]]
  :dev-dependencies [[midje "1.3.2-SNAPSHOT"]
                     [lein-midje "1.0.9"]]
  :main blog-lein.core
  :manifest {"Build-Version" ~project-version
             "Built-Time" ~(str (Date.))})
```

Doporučoval bych vyzkoušet si:

* `lein classpath`
* `lein jar` (a mrknout do MANIFEST.MF)
* `lein pom`
* `lein repl` (a zkusit si nějaké [Midje funkce](/2012/04/lepsi-testovani-v-clojure-midje/))
* `lein run`
* `lein uberjar` (a spustit si kód pomocí `java -jar <jarfile>.jar`)

Věřím, že po tomto malém úvodu se vám bude s Clojure začínat mnohem jednoduššeji (a nemít přitom vlasy v jednom ohni ;-)
