+++
title = "Maven, buildovací a konfigurační profily"
date = 2012-04-26T12:29:00Z
updated = 2017-06-17T11:55:00Z
description = """Maven profily můžou být silným nástrojem, pokud
    potřebujeme upravit build. Podíváme se, jak přes profily
    nastavit konfiguraci resources."""
tags = ["build", "maven"]
aliases = [
    "/2012/04/maven-buildovaci-konfiguracni-profily.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2012/04/Maven.gif" width="200" >}}

Shodou okolností jsem teď byl v jednom týdnu vypomáhat na dvou projektech jako jako "problem solver", kde jsem mmj. řešil věci kolem [Mavenu](//maven.apache.org/). V obou případech bylo potřeba vytvořit buildovací profily pro konfiguraci na různá prostředí. Chtěl bych si tady na jednom místě shrnout, jak nastavit [Maven](//maven.apache.org/) pro konfiguraci _resources_ a _webResources_.

Správa konfigurací se dá v [Mavenu](//maven.apache.org/) řešit různými způsoby (řekl bych, že dost často se to dělá přes [maven-antrun-plugin](//maven.apache.org/plugins/maven-antrun-plugin/)). Já jsem aktuálně použil [filtering](//maven.apache.org/plugins/maven-resources-plugin/examples/filter.html). Zadání je jednoduché --- mám konfigurační properties soubor a potřebuju v něm mít jiné hodnoty pro různá prostředí, např. DEV a TEST.

## Filtrování resources

Filtrování v Mavenu funguje následovně. Do standardního layoutu přidám adresář `src/main/filters` a v něm jsou _filtrovací_ properties soubory s vkládanými hodnotami (je potřeba použít "plain" properties --- s XML properties to v Mavenu nefunguje). V adresáři `src/main/resources` pak mám _"normální" _properties soubory s placeholdery pro vložení hodnot. Placeholder má formát `${property.klíč}`.

{{< figure src="/2012/04/Maven-filter-layout.png" caption="Layout projektu" >}}

Obsah souboru `default.properties`:

```bash
my.filter.value=world
```

Obsah souboru `hello.txt`:

```bash
Hello, ${my.filter.value}!
```

Aby došlo k filtrování, je potřeba je mít v pom.xml zapnuté a mít uvedenou cestu k filtrovacímu souboru:

```xml
<build>
  <filters>
    <filter>src/main/filters/default.properties</filter>
  </filters>
  <resources>
    <resource>
      <directory>src/main/resources</directory>
      <filtering>true</filtering>
    </resource>
  </resources>
</build>
```

Pokud teď pustíme zpracování _resources_ příkazem:

```bash
mvn resources:resources
```

najdeme v adersáři `target/classes` soubor `hello.txt` s nově vloženou hodnotou:

```bash
Hello, world!
```

## Buildovací profily

V předešlé sekci jsme do properties souboru vložili definovanou hodnotu (můžeme ji nazývat defaultní). Pro vložení jiné hodnoty je potřeba vytvořit profil v souboru `pom.xml`:

```xml
<profiles>
  <profile>
    <id>profile-1</id>
    <build>
      <filters>
        <filter>src/main/filters/profile-1.properties</filter>
      </filters>
    </build>
  </profile>
</profiles>
```

Profil zapneme přepínačem `-P <profile>`:

```bash
mvn resources:resources -P profile-1
```

Za předpokladu, že soubor `profile-1.properties` vypadá takto:

```bash
my.filter.value=profile-1 world
```

najdeme v souboru `target/classes/hello.txt` nově vloženou hodnotu:

```bash
Hello, profile-1 world!
```

## Filtrování webResources

Výše uvedený postup funguje pouze nad adresářem `src/main/resources`. Pokud chceme totéž provést v adresáři s web resources ([WEB-INF](//en.wikipedia.org/wiki/WAR_file_format_(Sun)) atd.), musíme nakonfigurovat obdobným způsobem [maven-war-plugin](//maven.apache.org/plugins/maven-war-plugin/):

```xml
<build>
  <plugins>
    <plugin>
      <groupId>org.apache.maven.plugins</groupId>
      <artifactId>maven-war-plugin</artifactId>
      <version>2.2</version>
      <configuration>
        <webResources>
          <resource>
            <directory>src/main/webapp/WEB-INF</directory>
            <targetPath>WEB-INF</targetPath>
            <filtering>true</filtering>
            <includes>
              <include>web.xml</include>
            </includes>
          </resource>
        </webResources>
      </configuration>
    </plugin>
  </plugins>
  <filters>
    <filter>src/main/filters/default.properties</filter>
  </filters>
</build>
```

Pro úplnost ještě doplním layout projektu:

{{< figure src="/2012/04/Maven-filter-web-layout.png" caption="Layout web projektu" >}}
