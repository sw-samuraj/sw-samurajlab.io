+++
title = "Flex, pár drobností"
date = 2012-04-10T14:17:00Z
updated = 2012-04-28T16:09:27Z
tags = ["flex"]
aliases = [
    "/2012/04/flex-par-drobnosti.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft"
    src="//3.bp.blogspot.com/-BEnp5cg1DY8/T3pkMa5WYTI/AAAAAAAAC84/KxGWdQi2ydk/s1600/Flex.png"
    link="//3.bp.blogspot.com/-BEnp5cg1DY8/T3pkMa5WYTI/AAAAAAAAC84/KxGWdQi2ydk/s1600/Flex.png"
>}}

Při studiu na nedávnou [Flexovou certifikaci](/2012/03/flex-certifikace/) jsem se musel poměrně hodně hluboko ponořit do Flexové dokumantace. A to jak do reference [ActionScriptu](//help.adobe.com/en_US/FlashPlatform/reference/actionscript/3/index.html), tak do konceptů a architektury samotného Flexu [Using Flex](//help.adobe.com/en_US/flex/using/index.html). Důvod byl jednoduchý --- nedostatek praxe jsem musel nahradit porozuměním.

Protože se mi architektura Flexu líbíla, rád bych se k němu vrátil a pro ten případ si tady chci zaarchivoval pár věcí, které mne zaujaly.

## Bidirectional binding

Pokud chci mít obousměrné datové propojení dvou komponent, mám k dispozici tři způsoby. Buď je vzájemně provázat jednosměrným bindingem:

{{< highlight xml >}}
<s:TextInput id="input1" text="{input2.text}"/>
<s:TextInput id="input2" text="{input1.text}"/>
{{< /highlight >}}

Nebo na zdroji říct, že propojení je obousměrné pomocí znaku zavináče (`@`):

{{< highlight xml >}}
<s:TextInput id="input1" text="@{input2.text}"/>
<s:TextInput id="input2"/>
{{< /highlight >}}

Anebo použít tag [&lt;fx:Binding&gt;](//help.adobe.com/en_US/FlashPlatform/reference/actionscript/3/mxml/binding.html) s nastavenou `twoWay` property:<br />

{{< highlight xml >}}
<fx:Binding source="input1.text"
            destination="input2.text"
            twoWay="true"/>
{{< /highlight >}}

## Event flow

Flex je framework postavený mmj. na [Even-driven architekturě](//en.wikipedia.org/wiki/Event-driven_architecture). Události zde proto hrají jednu z ústředních rolí. Pokud je na nějakém komponentu spuštěna (triggered) událost, má Flex definovány tři fáze, během kterých zjišťuje, zda události naslouchají nějaký [event  listeneři](//livedocs.adobe.com/flex/3/html/help.html?content=16_Event_handling_6.html):

<table align="center" cellpadding="0" cellspacing="0" class="tr-caption-container" style="margin-left: auto; margin-right: auto; text-align: center;">
<tbody>
<tr>
<td style="text-align: center;">
<a href="//1.bp.blogspot.com/-IGXTxrbqcNE/T4QMtPKbBYI/AAAAAAAAC-I/r2QSP8o0l3E/s1600/Flex+Event+Flow.png" imageanchor="1" style="margin-left: auto; margin-right: auto;">
<img border="0" src="//1.bp.blogspot.com/-IGXTxrbqcNE/T4QMtPKbBYI/AAAAAAAAC-I/r2QSP8o0l3E/s1600/Flex+Event+Flow.png" /></a>
</td>
</tr>
<tr>
<td class="tr-caption" style="text-align: center;">Tři fáze event flow</td>
</tr>
</tbody>
</table>

* **<span style="color: #274e13;">Capturing Phase</span>** probíhá od kořenového uzlu (root node) komponentového stromu až k rodičovskému uzlu cílového komponentu (target node). Na obrázku _Application_ &rarr; _Group_ &rarr; _Panel_. Flash Player na každém uzlu kontroluje, zda má na spuštěnou událost zaregistrovaný listener, pokud ano, listener zavolá.
* **<span style="color: #b45f06;">Targeting Phase</span>** zahrnuje pouze cílový/spouštěcí komponent (target node). Na komponentu jsou zavolány registrovaný listenery.
* **<span style="color: #660000;">Bubbling Phase</span>** probíhá od rodičovského uzlu cílového komponentu zpět ke kořenovému uzlu. Na obrázku _Panel_ &rarr; _Group_ &rarr; _Application_. Opět jsou zavolány registrované listenery.

Jak to všechno funguje ukazuje následující kód. V [MXML](//en.wikipedia.org/wiki/MXML) je definován komponentový strom z obrázku (_Application_ &rarr; _Group_ &rarr;_Panel_ &rarr; _Button_). Ve fázi inicializace (funkce `initializeHandler`) jsou na komponenty navěšeny listenery, na každý dva --- jeden pro [fázi capturing](//help.adobe.com/en_US/flex/using/WS2db454920e96a9e51e63e3d11c0bf69084-7cdb.html#WS2db454920e96a9e51e63e3d11c0bf69084-7cde), jeden pro [fázi bubbling](//help.adobe.com/en_US/flex/using/WS2db454920e96a9e51e63e3d11c0bf69084-7cdb.html#WS2db454920e96a9e51e63e3d11c0bf69084-7cce). Zbytek funkcí jsou event handlery, které vypisují text do debug konzole (metoda `trace`).

{{< highlight xml >}}
<?xml version="1.0" encoding="utf-8"?>
<s:Application xmlns:fx="http://ns.adobe.com/mxml/2009"
               xmlns:s="library://ns.adobe.com/flex/spark"
               initialize="initializeHandler(event)">

  <fx:Script>
    <![CDATA[
      import mx.events.FlexEvent;

      protected function clickHandler(event:MouseEvent):void {
          trace("Button has been clicked.");
      }

      protected function groupEventHandler(event:Event):void {
          trace("Event has been dispatched to: " + event.currentTarget);
      }

      protected function panelEventHandler(event:Event):void {
          trace("Event has been dispatched to: + event.currentTarget);
      }

      protected function buttonEventHandler(event:Event):void {
          trace("Event has been dispatched to: + event.currentTarget);
      }

      protected function initializeHandler(event:FlexEvent):void {
          group.addEventListener(MouseEvent.CLICK,
                                 groupEventHandler,
                                 true);
          group.addEventListener(MouseEvent.CLICK,
                                 groupEventHandler);
          panel.addEventListener(MouseEvent.CLICK,
                                 panelEventHandler,
                                 true);
          panel.addEventListener(MouseEvent.CLICK,
                                 panelEventHandler);
          button.addEventListener(MouseEvent.CLICK,
                                  buttonEventHandler,
                                  true);
          button.addEventListener(MouseEvent.CLICK,
                                  buttonEventHandler);
      }
    ]]>
  </fx:Script>

  <s:Group id="group">
    <s:Panel id="panel" title="Panel">
      <s:Button id="button"
                label="Button Event"
                click="clickHandler(event)"/>
    </s:Panel>
  </s:Group>

</s:Application>
{{< /highlight >}}

Po kliknutí na tlačítko _Button Event_ se na debug konzoli vypíše následující výpis (řádky jsou zalomeny). Jednotlivé barvy představují výše popsané fáze.

<pre>
<span style="color: #38761d;">Event has ween dispatched to: Work.ApplicationSkin2
    ._ApplicationSkin_Group1.contentGroup.group
Event has been dispatched to: Work.ApplicationSkin2
    ._ApplicationSkin_Group1.contentGroup.group.panel</span>
<span style="color: #f1c232;">Button has been clicked.</span>
<span style="color: #990000;">Event has been dispatched to: Work.ApplicationSkin2
    ._ApplicationSkin_Group1.contentGroup.group.panel
    .PanelSkin7._PanelSkin_Group1.contents
    .contentGroup.button
Event has been dispatched to: Work.ApplicationSkin2
    ._ApplicationSkin_Group1.contentGroup.group.panel
Event has been dispatched to: Work.ApplicationSkin2
    ._ApplicationSkin_Group1.contentGroup.group</span>
</pre>

## HTTPService

Pro zaslání HTTP requestu a příjem odpovědi na/z nějaké URL obsahuje Flex třídu [HTTPService](//help.adobe.com/en_US/FlashPlatform/reference/actionscript/3/mx/rpc/http/mxml/HTTPService.html). Třída funfuje asynchronně --- metodou [send()](//help.adobe.com/en_US/FlashPlatform/reference/actionscript/3/mx/rpc/http/HTTPService.html#send()) se odešle request a události [result](//help.adobe.com/en_US/FlashPlatform/reference/actionscript/3/mx/rpc/http/HTTPService.html#event:result) se nastaví handler, který zpracuje response. Celé je to jednoduché a elegantní:

{{< highlight xml >}}
<fx:Script>
  <![CDATA[
    import mx.rpc.events.ResultEvent;

    protected function init():void {
        categoryService.send();
    }

    protected function resultHandler(event:ResultEvent):void {
        trace(event.result);
    }
  ]]>
</fx:Script>

<fx:Declarations>
  <s:HTTPService id="categoryService"
         url="http://www.flexgrocer.com/category.xml"
         resultFormat="e4x"
         result="resultHandler(event)"/>
</fx:Declarations>
{{< /highlight >}}
