+++
title = "Lepší testování v Clojure: Midje"
date = 2012-04-02T15:57:00Z
updated = 2017-03-18T22:04:12Z
description = """Midje je úspěšná TDD knihovna pro Clojure unit testy.
    Jejím autorem je jeden ze signatářů Agile Manifesto, Brian Marick."""
tags = ["clojure", "testing"]
blogimport = true 
[author]
	name = "Vít Kotačka"
	uri = "https://www.blogger.com/profile/12147991495120816879"
+++

{{< figure class="floatleft" src="/img/logos/Clojure.png" width=170" >}}

Dneska navážu na [unit testování v Clojure](/2011/08/testovani-v-clojure/), nicméně dostanu se k tomu oklikou. Minulé léto se celkem hojně psalo o desátém výročí deklarace [Manifesto for Agile Software Development](//agilemanifesto.org/). Internetem (a mojí čtečkou) tehdy protekly nějaké rozhovory s někdejšími účastníky/signatáři. Jedním z nich byl i [Brian Marick](//en.wikipedia.org/wiki/Brian_Marick), autor [Midje](//github.com/marick/Midje#readme), testovacího frameworku pro Clojure (ano, to je to [Midje](//github.com/marick/Midje#readme), které jsem zmiňoval ve svém postu o [ThoughtWorks Radar](/2011/08/thoughtworks-radar-zminuje-clojure/)).

## Instalace/zprovoznění

[Midje](//github.com/marick/Midje#readme) je nejjednodušší začít používat pomocí [Leiningenu](//github.com/technomancy/leiningen#readme) (určitě jeden z příštích zápisků). Definice projektu může vypadat třeba takto:

```clojure
(defproject blog-midje "1.0.0-SNAPSHOT"
  :description "Midje for Sometimes Clojure"
  :dependencies [[org.clojure/clojure "1.3.0"]]
  :dev-dependencies [[midje "1.3.2-SNAPSHOT"]])
```

REPL se pak spustí příkazem `lein repl`.

## Fakta

Základem testování v Midje jsou [fakta](//github.com/marick/Midje/wiki/Facts). Fakta o budoucím kódu ([test first](//www.extremeprogramming.org/rules/testfirst.html)):

```clojure
(ns blog-midje (:use midje.sweet))
; nil
(fact (+ 1 1) => 2)
; true
(fact 1 => odd?)
; true
(fact "truth about one" 1 => even?)
; FAIL at (NO_SOURCE_FILE:11)
; Actual result did not agree with the checking function.
;         Actual result: 1
;     Checking function: even?
; false
```

Fakt může mít více klauzulí. Dá se použít makro `fact` nebo `facts`:

```clojure
(facts "about42"
       (+ 21 21) => 42
       (* 6 7) => 42)
; true
```

Pokud je potřeba fakt prezentovat pomocí sady hodnot, nabízí [Midje](//github.com/marick/Midje#readme) tzv. tabulková fakta:

```clojure
(defn xor [x y]
  (if (= x y) false true))

; #'blog-midje/xor
(tabular
  (fact "The rules of XOR"
        (xor ?x ?y) => ?expected)
  ?x    ?y    ?expected
  0     0     false
  0     1     true
  1     0     true
  1     1     false
  false false false
  false true  true
  true  false true
  true  true  false)
; true
```

Vyhození výjimky se dá otestovat pomocí funkce ([checkeru](//github.com/marick/Midje/wiki/Checkers)) `throws`:

```clojure
(fact (/ 42 0) => (throws ArithmeticException))
; true
```

## Spuštění testů

Midje předpokládá pro spuštění (všech) testů nějaký nástroj, jako např. [Leiningen](//github.com/technomancy/leiningen#readme) nebo [Cake](//github.com/ninjudd/cake#readme). Pojďme se podívat, jak by vypadalo rozchození projektu a testů pomocí Leiningenu. Nový projekt vytvoříme příkazem: `lein new blog-midje`

Struktura projektu by měla vypadat takto:

{{< figure src="/2012/04/Midje-tree.png" caption="Layout projektu" >}}

Do definice projektu (`project.clj`) je potřeba přidat závislosti. Kromě těch obligátních je to hlavně knihovna `lein-midje`:

```clojure
(defproject blog-midje "1.0.0-SNAPSHOT"
            :description "Midje for Sometimes Clojure"
            :dependencies [[org.clojure/clojure "1.3.0"]]
            :dev-dependencies [[midje "1.3.2-SNAPSHOT"]
                               [lein-midje "1.0.8"]])
```

Prvně napíšeme test (soubor `test/blog_midje/test/core.clj`):

```clojure
(ns blog-midje.test.core
  (:use [blog-midje.core])
  (:use [midje.sweet]))
(tabular
  (fact "The rules of XOR"
        (xor ?x ?y) => ?expected)
  ?x    ?y    ?expected
  0     0     false
  0     1     true
  1     0     true
  1     1     false
  false false false
  false true  true
  true  false true
  true  true  false)
```

Potom napíšeme testovanou funkci (`src/blog_midje/core.clj`):

```clojure
(ns blog-midje.core)
(defn xor
    "Returns true if x and y are mutually exclusive."
    [x y]
    (if (= x y) false true))
```

Testy potom spustíme příkazem `lein test`, nebo lépe `lein midje`:

{{< figure src="/2012/04//Midje-results.png" caption="Spuštění testů" >}}

~~Celý projekt je ke stažení zde: [blog-midje.zip](//docs.google.com/open?id=0B1KFHUMaUcOcTDFpMi16aEpSbmFDalo1TlNERzZrdw).~~

_[Update]_ Celý projekt je dispozici na Bitbucketu:

* [blog-midje](//bitbucket.org/sw-samuraj/blog-midje)

_[/Update]_

To je pro dnešek vše. Pokud se chcete podívat na úvod do Midje od samotného Briany Maricka, zkuste  videa uvedená na [wiki stránce Midje](//github.com/marick/Midje/wiki).

{{< youtube a7YtkcIiLGI >}}
