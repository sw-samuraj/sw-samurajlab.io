+++
title = "Lean ze zákopů"
date = 2012-08-14T22:39:00Z
updated = 2017-05-09T15:01:22Z
description = """Recenze knížky Lean from the Trenches. Má krátký rozsah
    a je nabitá informacemi. 🔫"""
tags = ["kanban", "knihy", "agile"]
aliases = [
    "/2012/08/lean-ze-zakopu.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2012/08/Lean-from-the-trenches.png"
    link="/2012/08/Lean-from-the-trenches.png" width="300" >}}

Když už se do něčeho pustím, tak pořádně. Sotva jsem tak dočetl jednu [knihu o Kanbanu](/2012/08/kanban-ultimatni-kniha/), už jsem schroustal další. Tentokrát jsem si vybral dílko od [Henrika Kniberga](//blog.crisp.se/author/henrikkniberg), které vyšlo v mojí oblíbené edici [The Pragmatic Programmers](//pragprog.com/).

Henrik pracuje jako _Agile &amp; Lean coach_ a v knize [Lean from the Trenches](//pragprog.com/book/hklean/lean-from-the-trenches), s podtitulem _Managing Large-Scale Projects with Kanban_, zpracoval formou [case study](//en.wikipedia.org/wiki/Case_study) svoji zkušenost s koučováním 60ti členného týmu švédské policie :-) který interně vyvíjel nový "digitální vyšetřovací systém". To zní skoro až exoticky, ale nebojte, slovníček švédštiny hledat nemusíte --- celá knížka je napsána s ohledem na projekt a softwarový vývoj a reálie jsou zcela nepodstatné. Co naopak je podstatné, je že tým používal [Scrum](//en.wikipedia.org/wiki/Scrum_(development)), [XP](//en.wikipedia.org/wiki/Extreme_Programming) a [Kanban](//en.wikipedia.org/wiki/Kanban_(development)) a projekt úspěšně zvládl.

Knížka je rozdělena do dvou částí, z nichž ta první _How We Work_ obsahuje zmiňovanou [case study](//en.wikipedia.org/wiki/Case_study) a jednotlivé kapitoly jsou často koncipovány formou _How We ..._, např:

* How We Sliced the Elephant
* How We Involved the Customer
* How We Handle Urgent Issues
* Why We Limit the Number of Bugs in the Bug Tracker
* Why We Don't Use Story Points
* How We Do Version Control

Druhá část, nazvaná _A Closer Look at the Techniques_, potom krátce rozebírá jednotlivé techniky, principy: [Agile](//en.wikipedia.org/wiki/Agile_software_development), [Lean](//en.wikipedia.org/wiki/Lean_software_development), [Scrum](//en.wikipedia.org/wiki/Scrum_(development)), [XP](//en.wikipedia.org/wiki/Extreme_programming) a [Kanban](//en.wikipedia.org/wiki/Kanban_(development)).

Knížka se velmi dobře čte a i přes svůj krátký rozsah (176 str.) je nabitá cennými informacemi. Zájemcům o Kanban/Lean rozhodně doporučuji.</div>
