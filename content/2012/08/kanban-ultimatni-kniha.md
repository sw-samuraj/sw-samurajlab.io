+++
title = "Kanban, ultimátní kniha"
date = 2012-08-09T23:02:00Z
updated = 2012-11-30T00:45:15Z
description = """Bible Kanbanu. Krátká recenze a pár citací."""
tags = ["kanban", "knihy", "agile"]
aliases = [
    "/2012/08/kanban-ultimatni-kniha.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2012/08/Kanban.jpg"
    link="/2012/08/Kanban.jpg" >}}

[Posledně](/2012/07/kanban-z-cisteho-nebe/) jsem psal, jak jsem přišel ke [Kanbanu](//en.wikipedia.org/wiki/Kanban_(development)) jako slepej k houslím. Jedním ze zmiňovaných zdrojů v článku je knížka [Kanban](//www.amazon.com/Kanban-ebook/dp/B0057H2M70/ref=sr_1_2?ie=UTF8&amp;qid=1344541743&amp;sr=8-2&amp;keywords=kanban) od Davida J. Andersona. David je jedním z pionýrů Kanbanu v softwarovém vývoji a jeho kniha má aspiraci (a velmi slušný potenciál), stát se biblí této zajímavé metody.

Na to, jak je Kanban jednoduchej je kniha poměrně obšírná (280 str.). Tematika je velkoryse rozebrána ve dvaceti kapitolách, rozdělených do čtyř částí:

1. Introduction
1. Benefits of Kanban
1. Implementing Kanban
1. Making Improvements

Každá kapitola je ukončena sekcí _Takeaways_, kde je v hutných bodech shrnut její obsah. To je šikovná věc, protože až se bude člověk jednou k daným tématům vracet, tak takový rychlý přehled se jednak umožní rychle zorientovat a jednak může sloužit jako reference (bez té omáčky v textu).

Jako zajímavější mi přišly první tři části knihy, které byly zaměřeny velmi prakticky. V poslední části už jsem se občas ztrácel, protože se zde řeší ekonomické modely, teorie omezení ([Theory of constraints](//en.wikipedia.org/wiki/Theory_of_constraints)) apod., což není úplně můj šálek kávy (nicméně, přečet jsem to).

Pokud bych měl z knihy vypíchnout nějaké zajímavé momenty, tak to budou spíše ty, které nesouvisí přímo s Kanbanem (ale velmi dobře mu konvenují) a které rezonují i s mojí osobní zkušeností:

> "Issue management and escalation are core disciplines that provide a big return. Improving them should be a priority even for the most immature teams."

> "Use of peer reviews, pair programming, unit tests, automated testing frameworks, continuous (or very frequent) integration, small batch sizes, cleanly defined architectures, and well-factored, loosely coupled, highly cohesive code design will greatly reduce defects."

> "As we all know, there really is no such thing as multi-tasking in the office; what we do is frequent task switching."

> "Many 'softies' prefer conference calls for coordination meetings rather than face-to-face. This has a negative impact on the level of trust and social capital in their workforce, but it facilitates efficiency."

> "Trust is a hard thing to define. Sociologists call it social capital. What they've learned is that trust is event driven and that small, frequent gestures or events enhance trust more than larger gestures made only occasionally."

Knihu můžu opravdu vřele doporučit. uvnitř najdete opravdu hard-core Kanban, se spoustou rad, jak jej implementovat, široké uvedení do kontextu, plus navrch i nějaká ta teorie.
