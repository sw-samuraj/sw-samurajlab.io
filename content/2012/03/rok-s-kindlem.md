+++
title = "Rok s Kindlem"
date = 2012-03-01T11:16:00Z
updated = 2013-08-17T22:45:38Z
description = """Koupil jsem si Kindle a musím říct, že za ten rok
    jsem četl, jak urvaný ze řetězu... 17 odborných knih! 🤦‍♂️"""
tags = ["knihy"]
aliases = [
    "/2012/03/rok-s-kindlem.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2012/03/Kindle-3.jpg"
    link="/2012/03/Kindle-3.jpg" width="300" >}}

Tak už je tomu rok, co jsem si koupil [Kindle](//www.amazon.com/Kindle-eReader-eBook-Reader-e-Reader-Special-Offers/dp/B0051QVESA). Rok je vhodná doba k bilancování, takže co mi za tu dobu prošlo čtečkou? (Jde o odborné knížky, které jsem buď přečetl, nebo "významně" rozečetl. Pořadí je náhodné, tak jak to zrovna teď mám srovnaný ve čtečce.)

* [**JBoss in Action**](//www.manning.com/jamae/) --- výborná kniha o 5-kové verzi [JBoss aplikačního serveru](//www.jboss.org/jbossas).
* [**Log4j**](//shop.oreilly.com/product/9780596805326.do) --- kratičký a hodnotný spisek o [logování v Javě](//logging.apache.org/log4j/1.2/).
* [**The Passionate Programmer**](//pragprog.com/book/cfcar2/the-passionate-programmer) --- aneb jak se stát mimořádně úspěšným vývojářem. Zajímavá témata. Myšlenkově a obsahově to trochu kopíruje  [The Pragmatic Programmer](//pragprog.com/the-pragmatic-programmer).
* [**The Five Dysfunctions of a Team**](//www.amazon.com/Five-Dysfunctions-Team-Leadership-Lencioni/dp/0787960756) --- čtivá knížka o principech, na nichž stojí úspěšný tým. V praxi jsem si, bohužel, ověřil, že první princip --- [absence důvěry](/tags/teamleading) --- je klíčový. A byť o tom člověk ví a snaží se na tom pracovat, tým to nemusí přijmout.
* [**Software Estimation: Demystifying the Black Art**](//shop.oreilly.com/product/9780735605350.do) --- výborně a precizně napsaná kniha o softwarových odhadech. Již žádné odhady vycucané z prstu!
* [**UML Distilled**](//martinfowler.com/books.html#uml) --- stručné a praktické základy [UML od Martina Fowlera](/2011/08/destilovane-uml/).

{{< figure src="/2012/03/Kindle-books-1.png"
    link="/2012/03/Kindle-books-1.png" >}}

* [**Managing Humans: Biting and Humorous Tales of a Software Engineering Manager**](//www.amazon.com/gp/product/159059844X?ie=UTF8&amp;tag=beigee-20&amp;linkCode=as2&amp;camp=1789&amp;creative=9325&amp;creativeASIN=159059844X) --- jak říká podtitul, [humorné a kousavé příběhy](/2011/07/manazerem-humorne-a-kousave/). Jakési bajky o SW inženýrství. Pravdivé a poučné.
* [**The Joy of Clojure**](//www.manning.com/fogus/) --- aneb krása novodobé [Java inkarnace Lispu](//clojure.org/). Velmi kvalitní kniha.
* [**Enterprise Integration Patterns**](//www.amazon.com/Enterprise-Integration-Patterns-Designing-ebook/dp/B000OZ0N9E/ref=tmm_kin_title_0?ie=UTF8&amp;m=AG56TWVU5XWC2) --- excelentní kniha o (messagingových) integračních vzorech ([EIP](//www.eaipatterns.com/)). Bible pro obor [Message-Oriented Middleware](//en.wikipedia.org/wiki/Message-oriented_middleware) (MOM) a [Enterprise Application Integration](//en.wikipedia.org/wiki/Enterprise_application_integration) (EAI)
* [**Adobe Flex 4.5 Fundamentals**](//www.amazon.com/Adobe-Flex-4-5-Fundamentals-ebook/dp/B005EFKGCS/ref=tmm_kin_title_0?ie=UTF8&amp;m=AG56TWVU5XWC2) --- výborný úvod do [Flexu](//www.adobe.com/products/flex.html) a doporučený podklad pro Flex certifikaci.
* [**Flex 4 in Action**](//www.manning.com/ahmed2/) --- trochu slabší verze předešlého. Stejný obsah, trochu ukecaná.
* [**ActiveMQ in Action**](//www.manning.com/snyder/) --- výborná kniha o [MOM od Apache](//activemq.apache.org/) postaveném nad [JMS](//en.wikipedia.org/wiki/Java_Message_Service) a [EIP](//www.eaipatterns.com/).

{{< figure src="/2012/03/Kindle-books-2.png"
    link="/2012/03/Kindle-books-2.png" >}}

* [**Programming Clojure**](//pragprog.com/book/shcloj/programming-clojure) --- první kniha, která o [Clojure](//clojure.org/) vyšla. Už trochu "outdated", ale dobrá. V současnosti se dokončuje druhé, [aktualizované vydání](//pragprog.com/book/shcloj2/programming-clojure).
* [**Flex on Java**](//www.manning.com/allmon/) --- o integraci mezi Flexem a Javou. Java část mírně zastaralá, hodnotná část o [BlazeDS](//opensource.adobe.com/wiki/display/blazeds/BlazeDS).
* [**Grails in Action**](//www.manning.com/gsmith/) --- výborná kniha o [Grails](//grails.org/).
* [**The Mythical Man-Month**](//www.amazon.com/Mythical-Man-Month-Software-Engineering-Anniversary/dp/0201835959) --- legendární kniha o SW inženýrství. Trochu, až trochu více zastaralá. Není moc čtivá, taková "(vysoko)školská". I po více než čtvrt století stále platný esej [No Silver Bullet](//en.wikipedia.org/wiki/No_Silver_Bullet).
* [**The Pragmatic Programmer**](//pragprog.com/the-pragmatic-programmer) ---  legendární kniha o SW inženýrství. Pravdivá a čtivá.

{{< figure src="/2012/03/Kindle-books-3.png"
    link="/2012/03/Kindle-books-3.png" >}}
