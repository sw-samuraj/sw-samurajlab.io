+++
title = "Změna syntax highlightingu a konvence kódu"
date = 2012-03-31T18:03:00Z
updated = 2017-04-02T22:24:06Z
description = """Všechno má svůj vývoj a tak jsem se rozhodl změnit
    jednak syntax highlighting a jednak způsob zápisu Clojure kódu.
    Obojí - samozřejmě - k lepšímu ;-)"""
tags = ["clojure", "vim"]
blogimport = true 
[author]
	name = "Vít Kotačka"
	uri = "https://www.blogger.com/profile/12147991495120816879"
+++

{{< figure class="floatleft" src="/img/logos/Vim.png" >}}

## Změna syntax highlightingu

Když jsem začínal psát tenhle blog, řešil jsem, jak prezentovat ukázky kódu, tedy [syntax highlighting](//en.wikipedia.org/wiki/Syntax_highlighting). Tehdy jsem zvolil řešení, které jsem dost často vídal na jiných blozích --- [SyntaxHighlighter](//alexgorbatchev.com/SyntaxHighlighter/). Řešení je to sice pěkné, ale bohužel pro Clojure neobsahuje oficiální _brush_ a ten který byl k dispozici neoficiálně, nepokrýval plně syntaxi Clojure.

Shodou okolností, tento rok mám takové malé jubileum --- už deset let jsem uživatelem Linuxu a protože hned na počátku jsem řešil klasické linuxové dilema, jestli programovat v Vi(m)u nebo v [Emacsu](//www.gnu.org/software/emacs/) a tehdy jsem zvolil [Vim](//www.vim.org/) (a dodones nelituju); můžu zároveň říct, že jsem i desetiletý uživatel [Vimu](//www.vim.org/).

No, zpátky k syntaxi.Vim obsahuje funkci na převod kódu do HTML:

* v textové verzi se zadá příkaz `:TOhtml`,
* v GUI verzi je to v menu _Syntax_ -&gt; _Convert to HTML_.

Pro úplnost bych ještě doplnil, že [Vim](//www.vim.org/) neumí syntaxi Clojure sám od sebe, ale je potřeba "doinstalovat" plugin [VimClojure](//www.vim.org/scripts/script.php?script_id=2501).

{{< figure class="floatleft" src="/img/logos/Clojure.png" width="130" >}}

## Změna code convention

Když už jsem řešil změnu syntax highlightingu (protože jsem to musel ručně poměnit u všech publikovaných postů), rozhodl jsem se změnit i [code convention](//en.wikipedia.org/wiki/Coding_conventions) --- to, jak budu zapisovat ukázky kódu. Sympatická mi přijde konvence použitá v knize [The Joy of Clojure](//manning.com/fogus/), která spočívá v tom, že se:

* píše čistý Clojure kód,
* nepíše se výzva [REPLu](//en.wikipedia.org/wiki/REPL), nebo příkazového řádku a
* výpisy na [standard output](//en.wikipedia.org/wiki/Standard_output#Standard_output_.28stdout.29) se vypisují za příkaz jako komentáře.

Výhodou téhle konvence je, že kód lze rovnou bez úprav zkopírovat a spustit ho v [REPLu](//en.wikipedia.org/wiki/REPL) nebo vložit do skriptu a zároveň je vidět, co kód dělá (jeho výstup).

Takže místo:

```clojure
user=> (def my-ref (ref "immutable data"))
#'user/my-ref
user=> my-ref
#<Ref@467d8ddd: "immutable data">
user=> @my-ref
"immutable data"
user=> (deref my-ref)
"immutable data"
```

vypadá kód takto:

```clojure
(def my-ref (ref "immutable data"))
; #'user/my-ref
my-ref
; #<ref@18352d8: "immutable data">
@my-ref
; "immutable data"
(deref my-ref)
; "immutable data"
```

To je celkem přehledný, ne?
