+++
title = "Migrace z Blogger na Hugo a GitLab Pages"
date = 2018-11-25T14:59:46+01:00
description = """Migrace 100 článků z jednoho blogovacího systému do jiného. Kolik je
    s tím práce a co to obnáší? Co všechno je potřeba nastavit a upravit? Blog
    SoftWare Samuraj se přestěhoval z Blogger.com na dvojkombinaci Hugo + GitLab Pages."""
tags = ["blogging"]
hero_image = "hero.jpg"
+++

{{< figure src="/2018/11/Blogger-Hugo-GitLab.png" >}}

Když jsem před sedmi lety začal psát blog _SoftWare Samuraj_, nevědel jsem, jestli mi psaní
vydrží. Chtěl jsem hlavně rychle začít --- abych neztratil motivaci --- a nechtěl jsem se
vrtat v nějakém _PHP_, nebo redakčním systému. A tak jsem si založil blog
na [Blogger.com](//www.blogger.com). A byl jsem s tím docela spokojený.

Nedávno jsem brouzdal po Twitteru a narazil jsem na článek Tomáše Klinera
[Create a blog within 20 minutes with Hugo, a static page generator written in
Go](//blog.stagemedia.cz/2018/07/create-a-blog-within-20-minutes-with-hugo-a-static-page-generator-written-in-go/).
Musím říct, že celý ten humbuk kolem [JAM Stacku](//jamstack.org/) a [static site
generátorů](//www.netlify.com/blog/2017/05/25/top-ten-static-site-generators-of-2017/) kolem mne
prošuměl, jako kdyby nebyl a tak jediný, co mě na článku zaujalo, bylo, že [Hugo](//gohugo.io/)
je napsaný v _Golangu_. (A pro _Golang_ mám [poslední dobou
slabost](//sw-samuraj.cz/2018/09/golang-micro-services-prvni-ohlednuti/).)

Takže, bohužel --- neřeknu vám nic o tom, proč jsou statické generátory super a jak je _JAM Stack_
cool-hustý-parádní. Bylo to jen chvilkové hnutí mysli, nic víc.

Nicméně, věcí, co jsem musel pořešit, bylo docela dost a tak přináším návod a inspiraci, jak na to.

## Průzkum

### Statické generátory

Jak jsem zmínil, u statických generátorů jsem měl jasno od začátku --- [Hugo](//gohugo.io/) a hotovo.
Prostě _Golang rules!_ a matlat se s nějakým _Ruby_ (a.k.a. [Jekyll](//jekyllrb.com/)) by měl ani nenapadlo. 😉

### Hosting

Hosting už jsem řešil trochu víc:

1. Pročetl jsem si z _Hugo_ dokumentace stránku [Hosting & Deployment](//gohugo.io/hosting-and-deployment/).
1. Zúžil výběr na:
   1. [Netlify](//www.netlify.com/),
   1. [GitHub Pages](//pages.github.com/),
   1. [GitLab Pages](//docs.gitlab.com/ee/user/project/pages/).
1. Finálně vybral _GitLab Pages_, hlavně proto, že mají:
   * all-in-one solution (tj. Git repository + CI/CD + Pages),
   * excelentní CI/CD (o kterém si _GitHub_ jen může nechat zdát),
   * podporu SSL/TLS a custom domains.

~~Jedinou nevýhodou~~ _GitLab Pages_ ~~je, že nemají integrovanou podporu pro~~ [Let's Encrypt](//letsencrypt.org/)
~~a certifikáty je potřeba měnit ručně. To je nepohodlné, ale pro mne akceptovatelné.~~

_[Update 13. 12. 2019]_ Od [verze 12.1](https://about.gitlab.com/blog/2019/07/22/gitlab-12-1-released/)
má _GitLab_ podporu automatické obnovy _Let's Encrypt_ certifikátů._[/Update]_

## Migrace

### Hugo projekt

Na začátku jsem si vytvořil prázdný _Hugo_ projekt a na něm odladil šablonu:

```bash
$ hugo new site blog               # nový Hugo projekt
$ cd blog
$ hugo new posts/first-article.md  # první, testovací článek
$ hugo server -D                   # lokální statická site (http://localhost:1313)
$
```

Původně jsem si vybral jedno z [Hugo témat](//themes.gohugo.io/), ale pak jsem do něj začal dělat
úpravy, takže jsem ho nakonec forknul a do projektu ho přidal jako [Git
submodule](//git-scm.com/book/en/v2/Git-Tools-Submodules).

### GitLab projekt

Jakmile jsem měl novou verzi blogu odladěnou lokálně, publikoval jsem ho do vzdálené Git repozitory
(klasický `git init` &rarr; `git add remote` &rarr; `git push`).

Výhodou _GitLabu_ je, že mají opravdu odladěný CI/CD a tak jediný, co je potřeba, aby pro `git push`
došlo k publikování stránek je následující jednoduchá konfigurace, uložená v souboru `.gitlab-ci.yml`:

{{< gist sw-samuraj 0cca37c22016fc2073d41ad70d8a0d93 >}}

Voilà, a jsme live! Magic! 🎉🎆

### Blogger export, Hugo import

Export z _Bloggeru_ je jednoduchý: _Blogger_ &rarr; Settings &rarr; Other &rarr; Back up Content

{{< figure src="/2018/11/Blogger-export.png" >}}

Výsledkem je obrovské XML, se zálohou všech článků, které lze následně naimportovat do _Hugo_ projektu.
Pro import existuje [několik utilitek](//gohugo.io/tools/migrations/#blogger), já jsem si vybral
[blogimport](//github.com/natefinch/blogimport), který dělá jen to nejnutnější --- rozseká články
do souborů a přidá _Hugo_ hlavičky --- takže záleží, co čekáte od výsledku. V mém případě to pak
zahrnovalo ještě nějakou ruční práci na formátování, úpravě hlaviček a adresářové struktuře.

### Konfigurace domény

(Re)konfigurace domény je něco, co dělám jen jednou za pár let, takže to může být trochu frustrující
a já s tím vždycky lehce bojuji. Konkrétní nastavení se bude lišit vzhledem k vašemu poskytovateli
domény. V zásadě to ale znamená poeditova následující DNS záznamy:

* Přidat `A` záznam pro doménu 2. řádu.
* Přidat `CNAME` záznam pro doménu 3. řádu (třeba _www_ apod.).
* Přídat `TXT` záznam(y) pro verifikaci vlastnictví domény.

{{< figure src="/2018/11/Dns-records.png" >}}

Pro ladění, jestli je DNS správně nastavený se mi osvědčily příkazy `nslookup` a `dig`.

```bash
$ dig +nocomments www.sw-samuraj.cz

; <<>> DiG 9.11.3-1ubuntu1.2-Ubuntu <<>> +nocomments www.sw-samuraj.cz
;; global options: +cmd
;www.sw-samuraj.cz.		IN	A
www.sw-samuraj.cz.	681	IN	CNAME	sw-samuraj.gitlab.io.
sw-samuraj.gitlab.io.	81	IN	A	35.185.44.232
;; Query time: 0 msec
;; SERVER: 127.0.0.53#53(127.0.0.53)
;; WHEN: Tue Nov 13 10:47:49 CET 2018
;; MSG SIZE  rcvd: 96
```

### HTTPS a GitLab custom domény

HTTPS je dneska standard, takže to samozřejmě budeme chtít i pro nové stánky. GitLab poskytuje automatické HTTPS
pro domény 3. řádu na [gitlab.io](//gitlab.io), tedy např. [sw-samuraj.gitlab.io](//sw-samuraj.gitlab.io).
Pro [custom domény](//docs.gitlab.com/ee/user/project/pages/getting_started_part_three.html) je potřeba každou
z nich přidat a nastavit jí TLS certifikát.

Přidání _custom domény_ je triviální, ale důležitá je její verifikace, tedy přidání `TXT` DNS záznamu.

{{< figure src="/2018/11/Pages-domain.png" >}}

Po přidání TLS certifikátu (viz dále) je HTTPS funkční.

{{< figure src="/2018/11/Domain-certificate.png" >}}

Výsledek by měl v GitLab konzoli vypadat takto:

{{< figure src="/2018/11/Custom-domains.png" >}}

### Let's Encrypt!

_[Update 13. 12. 2019]_ Od [verze 12.1](https://about.gitlab.com/blog/2019/07/22/gitlab-12-1-released/)
má _GitLab_ podporu automatické obnovy _Let's Encrypt_ certifikátů, takže tuto část již není potřeba
řešit._[/Update]_

[Let's Encrypt](//letsencrypt.org/) je sympatická certifikační autorita (CA), která _zdarma_ vydává
TLS certifikáty. Výhodou je, že pokud to váš poskytovatel podporuje, je vydávání a obnova certifikátů
automatická. Bohužel, v případě _GitLab Pages_ automatika (zatím) nefunguje a je potřeba certifikáty
měnit ručně. To je sice trošku nepříjemné, ale zase na druhou stranu žádná velká věda --- platnost
_Let's Encrypt_ certifikátů je 3 měsíce, takže mám v kalendáři připomínku (podobně jako pro
[Lítačku](//www.pidlitacka.cz/home)) a získat a nainstalovat nový certifikát je otázka 10 minut.

Pro vydání certifikátu slouží utilita [Certbot](//certbot.eff.org/) a v našem případě využijeme
její [manual](//certbot.eff.org/docs/using.html#manual) plugin. Následujícím příkazem si zažádám
o dva nové/obnovené certifikáty pro dvě domény `sw-samuraj.cz` a `www.sw-samuraj.cz`:

```bash
sudo certbot certonly --manual -d sw-samuraj.cz -d www.sw-samuraj.cz
```

_Let's Encrypt_ následně vygeneruje [challenge](//certbot.eff.org/docs/challenges.html),
abychom potvrdili vlastnictví domény:

```bash
Create a file containing just this data:

jwPa1N7aT-C3Qtm5FI5ZcYxsylxJZqeq3zfdmXTFrmk.oxjh1Mqila9LFc6d965eCQTwh3h82chnQed8WtgKsN4

And make it available on your web server at this URL:

http://sw-samuraj.cz/.well-known/acme-challenge/jwPa1N7aT-C3Qtm5FI5ZcYxsylxJZqeq3zfdmXTFrmk
```

U _Hugem_ generovaných stránek to může být trochu výzva (anglicky challenge 😈) takovou
_challenge_ zprostředkovat. Mě se osvědčilo přidat do adresáře
`static/.well-known/acme-challenge/` soubor s daným názvem a obsahem a následně ho publikovat.
(Pokud to není jasný, tady je
[ukázka](//gitlab.com/sw-samuraj/sw-samuraj.gitlab.io/blob/master/static/.well-known/acme-challenge/).)

Jakmile úspěšně potvrdíme _challenge_, najdeme nově vydané certifikáty v adresáři
`/etc/letsencrypt/live/<název-domény>` a speciálně pro _GitLab Pages_ nás bude zajímat obsah
souborů `fullchain.pem` a `privkey.pem`, které nakopírujeme do příslušného formuláře
v konfiguraci _GitLab Pages_ (viz výše, obrázek v sekci _HTTPS a GitLab custom domény_).

### Přesměrování

_Blogger_ a _Hugo_ generují z článků sice dost podobné, ale přeci jenom rozdílné URL. Sice se
dá _Hugo_ instruovat, aby používal tzv. [ugly URL](//gohugo.io/content-management/urls/#ugly-urls),
které věšinou _Blogger_ URL odpovídají, ale já jsem chtěl
[pretty URL](//gohugo.io/content-management/urls/#ugly-urls). Pro tyto případy má _Hugo_
[aliasy](//gohugo.io/content-management/urls/#aliases) --- mechanizmus pro přesměrování URL.

Do hlavičky _Hugo_ markdownu se přidá klíč `aliases` který může obsahovat seznam původních,
přesměrovávaných URL:

```bash
aliases = [
    "/2018/01/spring-security-saml-adfs-implementace.html"
]
```

_[Update 10. 12. 2018]_
Jak mě na Twitteru upozornil [BanterCZ](//twitter.com/banterCZ/status/1067378059077173248),
zapomněl jsem přesměrovat RSS kanál. Tedy přesměrovat "Bloggerovské"
[/feeds/posts/default](/feeds/posts/default) na "Hugovské" [/index.xml](/index.xml).

Tady mě _Hugo_ a _GitLab Pages_ trochu zklamali --- jednoduché přesměrování nejde.
Po nahlédnutí do dokumentace ([Redirects in GitLab
Pages](//docs.gitlab.com/ee/user/project/pages/introduction.html#redirects-in-gitlab-pages))
jsem to tedy nabouchal "na hulváta":

Do souboru `content/feeds/posts/default/index.html` jsem přidal klasický
[HTTP Meta refresh](//en.wikipedia.org/wiki/Meta_refresh) tag:

{{< gist sw-samuraj ccdf1524c1cf181fa77f409f4f5a8563 >}}

_[/Update]_

### Refaktoring

Výše zmiňovaný [blogimport](//github.com/natefinch/blogimport) funguje hodně jednoduše ---
vlastně jen vyextrahuje HTML `<body>` daného článku a přidá k němu _Hugo_ hlavičku. Což by mohlo
i stačit, ale já mám rád věci jaksi čistší. Takže jsem pak články ještě ručně čistil --- převádím
postupně HTML do Markdownu. Tady záleží, jak moc je člověk perfekcionista (zlí jazykové o mně tvrdí,
že to já jsem 🙉).

Čistil jsem ve [Vimu](//www.vim.org) a vytunil jsem si na to pár pěkných regulárních
výrazů. Jen taková malá ochutnávka:

```vim
" převedení nadpisů
%s/<h2.\{-}>\(.\{-}\)<\/h2>/## \1\r\r/cg
" převedení odkazů
%s/<a href="https\?:\(.\{-}\)">\(.\{-}\)<\/a>/[\2](\1)/cg
" převedení monospace textu
%s/<span.\{-}monospace.\{-}>\(.\{-}\)<\/span>/`\1`/cg
" převedení tučného textu a kurzívy
%s/<b>\(.\{-}\)<\/b>/**\1**/cg
%s/<i>\(.\{-}\)<\/i>/_\1_/cg
" převedení Gist JavaScriptu na Hugo Shortcode
%s/<script .\{-}\(sw-samuraj\)\/\(.\{-}\)\.js">/{{< gist \1 \2 >}}/cg
```

## Finální pocity

I když jsem migraci blogu udělal jen z pouhého rozmaru, jsem nakonec dost spokojený. Jednak jsem
se něco nového naučil, jednak jsem blog trochu redesignoval, jednak mám věci víc pod kontrolou.
Navíc jsem unikl ze spárů zlého Googlu (Pamatujete [Don't be evil](//en.wikipedia.org/wiki/Don%27t_be_evil)? 😈).
Prostě samé benefity a sociální jistoty. Teď už jen zbývá psát a psát.

## Související externí odkazy

* [Create a blog within 20 minutes with Hugo, a static page generator written in
  Go](//blog.stagemedia.cz/2018/07/create-a-blog-within-20-minutes-with-hugo-a-static-page-generator-written-in-go/)
* [Statický web s Jekyll](//blog.zvestov.cz/software%20development/2017/04/24/staticky-web-s-jekyllrb.html)
* [Migrace na statický generátor Hugo z Ghost a
  Octopress](//blog.prskavec.net/blog/2017/09/migrace-na-statick%C3%BD-gener%C3%A1tor-hugo-z-ghost-a-octopress/)
