+++
title = "Správa proprietárních závislostí v Golang"
date = 2018-06-30T22:51:00Z
updated = 2018-07-20T14:59:07Z
description = """Golang není zas až tak nový jazyk, a ačkoliv se v něm příjemně
    píše, má vývoj v Golangu určitá úskalí a výzvy. Poslední tři měsíce jsem se
    tématu verzování, závislostí, reprodukovatelnosti a automatizaci Go buildů
    intenzivně věnoval a řekl bych, že to soudruzi z U.S.A \"nedotáhli úplně dokonce\"."""
tags = ["golang", "gradle", "version-control", "git"]
aliases = [
    "/2018/06/sprava-proprietarnich-zavislosti-v.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/img/logos/Golang.png"
    link="/img/logos/Golang.png" width="200" >}}

Se změnou zaměstnání přišly nové výzvy --- prototypujeme teď nový produkt a jako primární technologie byl zvolen [Golang](//golang.org/). Myslím si, že vzhledem k povaze produktu (smečka mikro-servis) a cílové infrastruktuře ([IaaS](//en.wikipedia.org/wiki/Iaas)) je to dobrá volba.

_Golang_ není zas až tak nový jazyk (je tady s námi [nějakých 9 let](//opensource.googleblog.com/2009/11/hey-ho-lets-go.html)), a ačkoliv se v něm příjemně píše, má vývoj v _Golangu_ určitá úskalí a výzvy --- to buď v případě, že vám úplně nevyhovuje, jak v Googlu vymysleli vývojový proces, anebo pokud očekáváte vlastnosti běžné na některé zralejší platformě.

Jak já, tak celý tým máme silné _Java_ zázemí a je tedy možné, že náš přístup ke _Golang_ infrastruktuře není úplně adekvátní. Nicméně poslední tři měsíce jsem se tématu verzování, závislostí, reprodukovatelnosti a automatizaci _Go_ buildů intenzivně věnoval a řekl bych, že to soudruzi z U.S.A "nedotáhli úplně dokonce".

## Požadavky

{{< figure class="floatright" src="/2018/06/Exclamation-mark.png" >}}

Pro náš projekt jsme měli, řekl bych, celkem běžné nároky:

1. **Privátní Git repository za firemní proxy.** Kvůli legálním záležitostem a firemním politikám nemůžeme dát projektový kód na _GitHub_.
1. **Samostatná projektová repozitory.** OK, chápu, že v Googlu milují [monorepo](//trunkbaseddevelopment.com/monorepos/), ale většina z nás v Googlu nepracuje...
1. **Reprodukovatelný build.** To je samozřejmost, k tomu asi není co dodat.
1. **Spolehlivá správa závislostí a jejich verzí.** V _Javě_, nebo na _Linuxu_ je to [no-brainer](//en.wiktionary.org/wiki/no-brainer) --- stačí mít package manager a repozitory, ne?
1. **Plně automatizované.** Zase by se řeklo samozřejmost, ale pořád a pořád potkávám lidi, kteří smolí (klidně i produkční) buildy ručně.

## Problémy

{{< figure class="floatright" src="/2018/06/Puzzle.png" width="32" >}}

Pokud budete chtít výše zmíněné požadavky naimplementovat v _Golangu_, narazíte na problém: out-of-the-box to nejde vůbec a pokud nebudete ochotni část nároků oželet, čeká vás world-class hackování. (Teda ne, že bych byl takovej hustej hacker... spíš jsme narazili na hluché místo.)

Esenciální potíž je, že **celý _Golang_ ekosystém je navržený jako jedno velké open-source monorepo** (a.k.a. _GitHub_). Pokud nechcete/nemůžete mít svůj kód na _GitHubu_, máte problém. Protože:

* **_Golang_ nemá jednotný/dominantní nástroj na automatizaci.** Většina projektů na _GitHubu_ builduje [makem](//www.gnu.org/software/make/). `make` je fajn na kompilaci, ale dělat v něm komplexnější automatizaci je masochismus.
* **_Golang_ nemá dořešený verzování dependencí.** Člověk by řekl, že po těch letech to už bude nějak usazený, ale stačí nahlédnout do oficiální wiki stránky, nazvané lapidárně [Package Management Tools](//github.com/golang/go/wiki/PackageManagementTools) a zjistit, že pro správu dependencí a verzí existuje nějakejch 20-30 nástrojů. Naštěstí se od loňského léta masivně prosazuje nástroj [dep](//github.com/golang/dep) a velmi slibně to vypadá s oficiálním návrhem [vgo](//research.swtch.com/vgo), který by se snad měl cca na podzim dostat do _Go_ verze 1.11.
* **Oficiální Golang nástroje neumí pracovat se závislostmi, které nejsou ve veřejných repozitářích** (_GitHub_, _Bitbucket_, _GitLab_). Jediný podporovaný způsob je nastavit na repozitory HTML meta tag [go-import](//golang.org/cmd/go/#hdr-Remote_import_paths). Což většinou(?) nemáte pod kontrolou.

## GOPATH odbočka

{{< figure class="floatright" src="/img/logos/Golang.png" width="32" >}}

Malé vysvětlení pro čtenáře, kteří nejsou _Gophers_ (fanoušci _Golangu_). Proč jsou výše zmíněné problémy reálnými problémy? Všechny _Golang_ nástroje předpokládají, že vaše zdrojáky jsou soustředeny pod tzv. `GOPATH` --- což je fakticky lokální [monorepo](//trunkbaseddevelopment.com/monorepos/).

Protože v _Golangu_ se všechno linkuje a kompiluje staticky, je potřeba mít **všechny** zdrojové kódy na jednom místě, neexistují binární závislosti. Pro stažení externích závislostí (v _Go_ nazývaných _package import_) slouží nástroj `go get`, který de facto naklonuje Git repository dané závislosti do adresářové struktury pod `GOPATH`.

Pokud bychom pominuli verzování a reprodukovatelnost a soustředili se jenom na kompilaci, máme tady dva základní konflikty:

* `go get` neumí klonovat z non-public repozitářů.
* Pokud nemáte váš projekt pod `GOPATH`, budete muset s `GOPATH` nějak manipulovat, aby _Go_ nástroje vůbec fungovaly.

{{< tweet 1012720923395358721 >}}

## Řešení

{{< figure class="floatright" src="/2018/06/Cog.png" width="32" >}}

Protože jsem husto-krutej drsňák, před kterým bledne jak [Phil Marlowe](//en.wikipedia.org/wiki/Philip_Marlowe), tak [Dirty Harry](//en.wikipedia.org/wiki/Dirty_Harry_(character)), tak jsem se nezaleknul, zatnul zuby a všechny problémy vyřešil. Svým způsobem...

Pravdou je, že řešení, ke kterému jsme prozatím doiterovali, byl průzkum bojem --- tj. paralelně řešit business požadavky (proč vlastně ten prototyp děláme) a zároveň vyřešit automatizaci buildů (nebo minimálně aspoň kompilaci _Golang_ projektů).

Když začnu s výše popsanými požadavky od konce --- pro automatizaci jsme zvolili [Gradle](//gradle.org/). Původně jsme počítali s nějakým polyglot řešením (něco v _Javě_, něco v _Golang_), ale nakonec jsme skončili s čistě _Golang_ moduly.

Ironií je, že v celém řešení víceméně žádná _Java_ není (výjimkou jsou _Gradle_ pluginy). Každopádně, _Gradle_ se výborně osvědčil --- buildujeme v něm _Go_, _RPM_ balíčky, _Docker_ image, stahujeme a re-packagujeme binárky z internetu, pouštíme integrační testy, deployujeme do repozitářů a do cloudu atd.

To všechno je hezké, jak ale v _Gradlu_ skompilovat _Golang_ zdrojáky? Samozřejmě, vezmu nějaký hotový plugin. Bohužel, výše naznačené problémy s verzováním _Golang_ závislostí způsobily, že nám žádný plugin úplně nevyhovoval.

A tak jsem si napsal [svůj plugin](//github.com/sw-samuraj/gradle-godep-plugin). Řekl jsem si, že základní nástroje v _Go_ fungují dobře a tak je jenom zorchestruju. Takže můj plugin není nic jiného než: wrapper + orchestrátor + lifecycle. Plugin řeší následující věci: <br />

1. Manipulace s `GOPATH`, aby fungovaly nástroje, které očekávají zdrojáky v... `GOPATH`.
1. Stažení externích závislostí z public repozitářů (pomocí nástroje [dep](//github.com/golang/dep)).
1. Stažení externích závislostí z non-public repozitářů (pomocí _Git_ klonování).
1. Spuštění testů.
1. Kompilace binárky.

O správu verzí _public_ závislostí se stará nástroj [dep](//github.com/golang/dep). O správu verzí _non-public_ závislostí se stará plugin (task [proprietaryVendors](//github.com/sw-samuraj/gradle-godep-plugin#tasks)). V případě, že _non-public_ závislosti mají tranzitivní závislosti na _public_ závislostech, je to kombinace obojího. Technický popis je trochu komplexnější, takže zájemce odkazuji do dokumentace: [How to handle proprietary vendors](//github.com/sw-samuraj/gradle-godep-plugin#how-to-handle-proprietary-vendors); nicméně použití by mělo být (v rámci možností) jednoduché.

{{< figure src="/2018/06/Gradle-godep-plugin.png"
    caption="Sekvenční diagram build life-cyclu godep pluginu" >}}

## Problem solved?

{{< figure class="floatright" src="/2018/06/Checked.png" width="32" >}}

Takže... problém vyřešen? Nový _Gradle _plugin splňuje všechny v úvodu uvedené požadavky a za ty tři měsíce má za sebou stovky úspěšných buildů (build se v [GitLab Pipelině](//docs.gitlab.com/ee/ci/pipelines.html) pouští pro každý `git push`). Tudíž zatím ano, funguje to k naší spokojenosti.

Otázkou ale je, jak dlouho to vydrží --- verzování závislostí je žhavé téma jak _Golang_ komunity, tak core vývojářů _Go_. Všichni s očekáváním vzhlížejí ke zmíněnému [vgo](//research.swtch.com/vgo) a tak za půl roku možná bude všechno jinak a nějaký _Gradle_ plugin už nebude potřeba.

Ale nijak se tím netrápím --- pár takových _Gradle_ pluginů [už jsem napsal](//plugins.gradle.org/search?term=swsamuraj): v daný moment vyřešily mou situaci, avšak časem přestaly být potřeba. A to se mi líbí --- nástroje, které jsou dostatečně flexibilní a zároveň mají hezký a moderní design. A dobře se v nich dělá. A to jak _Golang_, tak _Gradle_ splňují.

## Externí odkazy

{{< figure class="floatright" src="/img/logos/GitHub.png" width="32" >}}

* [gradle-godep-plugin](//github.com/sw-samuraj/gradle-godep-plugin)

## Mind Map

{{< figure src="/2018/06/Golang-proprietary-vendor-dependencies.png"
    link="/2018/06/Golang-proprietary-vendor-dependencies.png" >}}
