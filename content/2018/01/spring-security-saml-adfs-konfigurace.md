+++
title = "Spring Security, SAML & ADFS: Konfigurace"
date = 2018-01-17T12:48:00Z
updated = 2018-01-29T21:16:24Z
description = """Posledně jsme se podívali jaký je mechanismus SAML autentikace.
    Tenhle článek se bude zaměřovat na konfiguraci potřebnou pro to, aby nám SAML
    autentikace fungovala."""
tags = ["security", "spring", "saml", "java"]
aliases = [
    "/2018/01/spring-security-saml-adfs-konfigurace.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2018/01/Spring-Security-and-ADFS.png" >}}

[Minule](/2017/12/spring-security-saml-adfs-uvod.html) jsme se podívali --- z obecnějšího pohledu --- jak SAML funguje pro autentikaci aplikace. Kromě toho, že byste měli znovu zkouknout ty pěkné barevné diagramy, zobrazující _SSO_ (Single Sign-On) a _SLO_ (Single Logout), by se vám mohl hodit SAML a ADFS slovníček --- od teď už očekávám, že termíny máte našprtané ;-)

Tenhle článek se bude zaměřovat na konfiguraci potřebnou pro to, aby nám SAML autentikace fungovala. V realitě pak tato konfigurace půjde nejčastěji ruku v ruce s implementací, protože abyste získali _SP_ (Service Provider) metadata, budete potřebovat ho mít už funkční (pokud nejste SAML-Superman, který to zvládne nabouchat ručně, nebo externím nástrojem).

## Registrace metadat

Aby nám SAML fungoval, musíme nejdřív vzájemně zaregistrovat jednotlivé _IdP_ (Identity Providery) a _SP_ (Service Providery). Jak jsme si říkali v minlém díle, vztah _IdP_ --- _SP_ může být M:N, nicméně pro zbytek článku (a i v tom následujícím) budeme uvažovat jenom vztah 1:1, tedy náš _SP_ (aplikace) se autentikuje vůči jednomu _IdP_.

Registrace metadat se může provést dvojím způsobem --- buď můžeme poskytnou URL, na kterém si _IdP_/_SP_ metadata sám stáhne při svém startu, nebo (asi častější) metadata poskytneme jako statický soubor. Zde budeme pracovat s druhým případem.

### Registrace (ADFS) Federation metadat

Registrace _Federation Metadat_ je jednoduchá --- stáhneme XML soubor z daného URL a protože pro implementaci _SP_ používáme [Spring Security SAML](//projects.spring.io/spring-security-saml/), poskytneme ho jako resource pro [MetadataProvider](//www.atetric.com/atetric/javadoc/org.opensaml/opensaml/2.6.1/org/opensaml/saml2/metadata/provider/MetadataProvider.html).

Metadata na ADFS serveru najdeme na následujícím URL:

* [https://&lt;ADFS server&gt;/FederationMetadata/2007-06/FederationMetadata.xml](//localhost/FederationMetadata/2007-06/FederationMetadata.xml)

_Federation Metadata_ je dlouhý, ošklivý XML soubor. Způsob, jak ho poskytnout naší aplikaci je trojí:

* dát ho na classpath a načíst třídou `ClasspathResource`
* dát ho na file systém a načíst třídou `FilesystemResource`
* načíst ho přímo z ADFS třídou `HttpResource`

Pokud např. soubor `FederationMetadata.xml` umístíme do adresáře `src/main/resource/metadata`, můžeme ho načíst následujícím způsobem:

{{< gist sw-samuraj aa1c7bd3bc6a230b6e0b02f9b4da411f >}}

Pokud potřebujete trochu víc (Spring) kontextu, může se podívat do kompletní Spring SAML konfigurace:<br />

* [SecurityConfiguration.java](//bitbucket.org/sw-samuraj/blog-spring-security/src/tip/src/main/java/cz/swsamuraj/wicketspring/security/SecurityConfiguration.java)

### Registrace (Spring) SAML metadat

Registraci SAML metadat na ADFS si rozdělíme do dvou kroků:

* Získání metadat z aplikace.
* Registraci metadat na ADFS.

#### Generování SAML metadat (z aplikace)

Tady přichází ke slovu, co jsem předesílal --- abyste byli schopný si vygenerovat SAML metadata, budete potřebovat mít Spring SAML aspoň částečně naimplementovaný. O generování metadat se stará Springovský filtr [MetadataGeneratorFilter](//docs.spring.io/spring-security-saml/docs/current/api/org/springframework/security/saml/metadata/MetadataGeneratorFilter.html).

Generování metadat out-of-the-box funguje dobře (a s ADFS ho rozchodíte). Pokud chcete, nebo musíte metadata upravit, tohle je to správné místo. Například specifické (SP) _Entity ID_ se dá nastavit tímto způsobem:

{{< gist sw-samuraj efaed2fcb885fb533938a2203cd7511f >}}

Filter pak necháme naslouchat na určitém URL endpointu, kde nám bude metadata poslušně generovat:

{{< gist sw-samuraj 70dbf0547a194df313430ca9ec2a6b72 >}}

Situace je samozřejmě trochu složitější, takže pokud jste netrpělivý, nebo vám chybí potřebné Spring beany, nahlídněte do zmiňované [SecurityConfiguration.java](//bitbucket.org/sw-samuraj/blog-spring-security/src/tip/src/main/java/cz/swsamuraj/wicketspring/security/SecurityConfiguration.java).

Ještě než si metadata vygenerujete a stáhnete z daného URL, jedno důležité upozornění! Při nesplnění následujících podmínek vám SAML před ADFS nebude fungovat.

* Na URL musíte přistoupit přes **HTTPS** (a mít tedy odpovídajícím způsobem nakonfigurovaný aplikační server/servlet kontejner).
* Na URL musíte přistoupit přes **hostname** nebo **IP adresu**, které jsou z _IdP_ (ADFS) viditelné. (Takže ne [https://localhost](//localhost/).)

#### Registrace metadat na ADFS

Teď se magicky přeneseme na ADFS server (typicky přes _Remote Desktop_), kam si zkopírujeme vygeneraovaný SAML metadata soubor. Spring ho defaultně nazývá `spring_saml_metadata.xml`, nicméně na jméně nezáleží.

V _AD FS Management_ konzoli nás bude zajímat jediná věc --- položka _Relying Party Trust_, kde metadata našeho SP zaregistrujeme, resp. naimportujeme.

{{< figure caption="AD FS Management konzole" src="/2018/01/ADFS-Management-console.png" >}}

Import metadat se provádí pomocí wizardu (jak jinak ve Windows). Je to jednoduché a přímočaré: zadáme import ze souboru a pak už se jen doklikáme nakonec:

{{< figure caption="Import SP metadat ze souboru" src="/2018/01/Import-SP-from-file.png" >}}

V závěrečném shrnutí je dobré si zkontrolovat, že v sekci _Endpoints_ jsou splněny dvě výše uvedené podmínky (HTTPS a hostname/IP adresa):

{{< figure caption="Kontrolní shrnutí SP endpointů" src="/2018/01/SP-endpoints.png" >}}

ADFS defaultně očekává, že hash algoritmus použitý při podepisování SAML zpráv bude _SHA-256_. Bohužel, _Spring SAML_ posílá out-of-the-box _SHA-1_. Máte tedy dvě možnosti:

* Upravit _Spring SAML_, aby používal _SHA-256_ (jak to ohackovat vám prozradím příště),
* nebo říct ADFS, aby očekávalo _SHA-1_.

Nastavení _Secure hash algorithm_ najdete po rozkliknutí _Properties_ na záložce _Advanced_ (je potřeba to udělat dodatečně --- při importu metadat je tato volba nefunkční):

{{< figure caption="Nastavení Secure hash algorithm" src="/2018/01/Secure-hash-algorithm.png" >}}

Pokud si to na obou stranách nesladíte, budete na straně Springu dostávat nic neříkající výjimku:

{{< highlight bash >}}
2018-01-17 12:15:01 DEBUG org.springframework.security.saml.SAMLProcessingFilter --- Authentication request failed: org.springframework.security.authentication.AuthenticationServiceException: Error validating SAML message
org.springframework.security.authentication.AuthenticationServiceException: Error validating SAML message
        at org.springframework.security.saml.SAMLAuthenticationProvider.authenticate(SAMLAuthenticationProvider.java:100) ~[spring-security-saml2-core-1.0.3.RELEASE.jar:1.0.3.RELEASE]
        at org.springframework.security.authentication.ProviderManager.authenticate(ProviderManager.java:174) ~[spring-security-core-5.0.0.RELEASE.jar:5.0.0.RELEASE]
Caused by: org.opensaml.common.SAMLException: Response has invalid status code urn:oasis:names:tc:SAML:2.0:status:Responder, status message is null
        at org.springframework.security.saml.websso.WebSSOProfileConsumerImpl.processAuthenticationResponse(WebSSOProfileConsumerImpl.java:113) ~[spring-security-saml2-core-1.0.3.RELEASE.jar:1.0.3.RELEASE]
        at org.springframework.security.saml.SAMLAuthenticationProvider.authenticate(SAMLAuthenticationProvider.java:87) ~[spring-security-saml2-core-1.0.3.RELEASE.jar:1.0.3.RELEASE]
{{< /highlight >}}

A na straně ADFS nápomocnější:

{{< highlight bash >}}
Microsoft.IdentityServer.Protocols.Saml.SamlProtocolSignatureAlgorithmMismatchException:
    MSIS7093: The message is not signed with expected signature algorithm.
    Message is signed with signature algorithm http://www.w3.org/2000/09/xmldsig#rsa-sha1.
    Expected signature algorithm http://www.w3.org/2001/04/xmldsig-more#rsa-sha256.
{{< /highlight >}}

Takže, prozatím _SHA-1_. "Dvě stě padesát šestka" bude příště ;-)

### Claim Rules

Poslední věc, kterou zbývá nastavit je mapování _claims_ na _assertions_. Na naší nově vytvořené _Relying Party Trust_ dáme _Edit Claim Rules..._ a přidáme následující pravidlo, které nám z _Active Directory_ vytáhne _Name ID_ a doménové skupiny daného uživatele:

{{< figure caption="Editace Claim Rules" src="/2018/01/Claim-rules.png" >}}

## To be continued...

Tak a máme hotovo! Teda konfiguraci. Ovšem, jak už jsem naznačoval, v tento moment už stejně většinou máte hotovou i zbývající implementaci, takže pokud jsme nic neopomněli, mělo by nám fungovat jak _SSO_, tak _SLO_, přesně podlě těch krásných diagramů z minulého dílu.

V příštím, závěrečném díle, se podíváme, jak nabastlit zbytek Springovských věcí --- můžete se těšit na Java konfiguraci (což je zatím vzácnost, protože [Reference Documentation](//docs.spring.io/spring-security-saml/docs/current/reference/htmlsingle/) stále jede na XML) a samozřejmě pofrčíme na aktuálním _Spring 5_ (Cože?!? Vy jedete ještě na čtyřce?! No, nekecej 8-)

## Související články

* [Spring Security, SAML &amp; ADFS: Úvod](/2017/12/spring-security-saml-adfs-uvod.html)
* [Spring Security, SAML &amp; ADFS: Implementace](/2018/01/spring-security-saml-adfs-implementace.html) 
