+++
title = "  Spring Security, SAML & ADFS: Implementace"
date = 2018-01-29T21:13:00Z
updated = 2018-01-29T21:13:00Z
description = """V minulém dílu jsme se vyřádili na konfiguraci, tak teď už
    jen zbývá to nabouchat v tom Springu, ne? Mám pro vás jednu dobrou a
    jednu špatnou zprávu."""
tags = ["security", "spring", "saml", "java"]
aliases = [
    "/2018/01/spring-security-saml-adfs-implementace.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2018/01/Spring-Security-and-ADFS-2.png" >}}

[Posledně](/2018/01/spring-security-saml-adfs-konfigurace/) jsme se vyřádili na konfiguraci, tak teď už jen zbývá to nabouchat v tom Springu, ne? Dobrá zpráva je, že pokud budete následovat [Reference Documentation](//docs.spring.io/spring-security-saml/docs/current/reference/htmlsingle/), bude vám _Spring SAML_ a _ADFS_ fungovat out-of-the-box.

Špatná zpráva je, že pokud budete chtít použít Java configuration, nemáte se moc kde inspirovat. Pokud vím, tak k dnešnímu dni jsou k dispozici jen dva příklady:

* [vdenotaris/spring-boot-security-saml-sample](//github.com/vdenotaris/spring-boot-security-saml-sample) a speciálně jeho [WebSecurityConfig](//github.com/vdenotaris/spring-boot-security-saml-sample/blob/master/src/main/java/com/vdenotaris/spring/boot/security/saml/web/config/WebSecurityConfig.java)
* a potom můj skromný příspěvek [sw-samuraj/blog-spring-security](//bitbucket.org/sw-samuraj/blog-spring-security), který je předmětem tohoto článku.

Dalším benefitem mého příspěvku a ukázkového projektu je, že používají aktuální verzi _Spring Frameworku_ a _Spring Security_, tedy **verzi 5** (v tomhle asi budu chviličku unikátní). Třešničkou na dortu je pak buildování pomocí _Gradle_ (protože kdo by ještě chtěl v dnešní době používat _Maven_, že jo? 😜

## Závislosti

Pro zdar operace budeme potřebovat následující závislosti:

{{< gist sw-samuraj e15d1ff37f306c0251e22389e4285510 >}}

Drobná _Gradle_ poznámka:  Protože používám současnou verzi _Gradlu_, používám konfiguraci `implementation`. Pro starší verze _Gradle_ (2.14.1-) použijte původní (nyní [deprecated](//www.youtube.com/watch?v=7ll-rkLCtyk&amp;feature=youtu.be&amp;t=29m18s)) konfiguraci `compile`.

## Spring SAML Configuration

Ať už se použije XML, nebo Java konfigurace, bude to v každém případě velmi dlouhý soubor. Velmi. I když nebudu počítat téměř 40 řádek importů, i tak zabere ta nejzákladnější konfigurace zhruba 5 obrazovek. Víc se mi to ořezat nepodařilo.

Ale nebojte se, nebudu vás oblažovat každým detailem. Jen vypíchnu to zajímavé, vynechám co jsem zmiňoval v [minulém díle o konfiguraci](/2018/01/spring-security-saml-adfs-konfigurace/) a pro zbytek konfigurace vás odkážu do svého repozitáře, kde si to můžete vychutnat celé: [SecurityConfiguration.java](//bitbucket.org/sw-samuraj/blog-spring-security/src/tip/src/main/java/cz/swsamuraj/wicketspring/security/SecurityConfiguration.java).

## Nastavení HttpSecurity

Nebudu příliš zabíhat do podrobností, jak funguje samotné _Spring Security_ (prostě chrání pomocí filtrů určité URL/zdroje) a podívám se na jedno konkrétní nastavení:

{{<gist sw-samuraj 4a2819e4ad4af9edd3584c8f3983527c >}}

* Vypnuté [CSRF](//www.owasp.org/index.php/Cross-Site_Request_Forgery_(CSRF)). U SAMLu nedává CSRF moc smysl --- SAML requesty jsou podepsané privátním klíčem daného _SP_, jehož veřejný klíč je zaregistrován na použitém _IdP_.
* Přídání dvou filtrů: jeden pro SAML metadata (`metadataGeneratorFilter`), druhý řeší samotný SAML mechanismus (`samlFilter`).
* Definice URL, které vyžadují autentikaci (`/user`).
* Podstrčení [SAML entry pointu](//docs.spring.io/spring-security-saml/docs/current/api/org/springframework/security/saml/SAMLEntryPoint.html) namísto přihlašovacího formuláře (`loginPage("/saml/login")`).
* Přesměrování na root kontext aplikace po úspěšném odhlášení (`logoutSuccessUrl("/")`).

## SAML filtry

Základem jak _Spring Security_, tak _Spring Security SAMLu_ jsou filtry --- odchytí HTTP(S) komunikaci a transparentně aplikují zabezpečení aplikace. V případě SAMLu je těch filtrů celá smečka, ale v zásadě řeší jen tři věci: přihlášení (_SSO_), odhlášení (_SLO_) a metadata. Čtvrtým mušketýrem může být ještě [IdP discovery](//docs.spring.io/spring-security-saml/docs/current/reference/htmlsingle/#configuration-discovery), ale tu v našem případě nemáme.

{{<gist sw-samuraj c433dd8e72e843472359c96e32d6b44f >}}

## Key manager

Všechny SAML zprávy, jež si _IdP_ a _SP_ vyměňují jsou podepsané privátním klíčem dané strany. Doporučuji mít pro SAML podpisový klíč separátní key store (nemíchat ho třeba s key storem, který potřebuje aplikační server pro HTTPS).

V naší ukázkové aplikaci je SAML key store na classpath --- v jakémkoli jiném, než lokálním vývojovém prostředí, key store samozřejmě externalizujeme (nepřibalujeme do WARu) a hesla kryptujeme.

{{<gist sw-samuraj fcc7e19bba13dadb14103af80871d5c1 >}}

## Podepisování SHA-256

V [minulém díle](/2018/01/spring-security-saml-adfs-konfigurace/) jsem zmiňoval, že _Spring SAML_ defaultně používá při podepisování algoritmus SHA-1, kdežto _ADFS_ očekává SHA-256. Jedna strana se musí přizpůsobit. Doporučuji upravit aplikaci --- použít SHA-256 není nic těžkého.

Výběr podpisového algoritmu se provádí při inicializaci SAMLu pomocí třídy [SAMLBootstrap](//docs.spring.io/spring-security-saml/docs/current/api/org/springframework/security/saml/SAMLBootstrap.html), která bohužel není konfigurovatelná. Pomůžeme si tak, že od třídy podědíme a potřebný algoritmus podstrčíme:

{{<gist sw-samuraj 742c408960d655bf4794b0015ba284fb >}}

V konfiguraci pak třídu instancujeme následujícím způsobem. Mimochodem, povšimněte si, že beana je instancovaná jako `static`. To proto, aby inicializace proběhal velmi záhy při vytváření kontextu.

{{<gist sw-samuraj 8229f5e778228f56b2d12b973573a204 >}}

## That's All Folks!

Tím se náš 3-dílný mini seriál o _Spring Security_, _SAMLu_ a _ADFS_ uzavírá. Samozřejmě, že bych mohl napsat ještě mnoho odstavců a nasdílet spoustu dalších [gistů](//gist.github.com/sw-samuraj). Ale už by to bylo jen nošení housek do krámu.

Lepší bude, pokud si teď stáhnete ukázkový projekt [sw-samuraj/blog-spring-security](//bitbucket.org/sw-samuraj/blog-spring-security), trochu se povrtáte ve zdrojácích a na závěr v něm vyměníte soubor `FederationMetadata.xml` a zkusíte ho rozchodit vůči vašemu ADFS. Při troše štěstí by to mělo fungovat na první dobrou 🙂

Jako bonus pro odvážné --- pokud se opravdu pustíte do těch zdrojových kódů --- můžete v historii projektu najít další _Spring Security_ ukázky (je to celkem rozumně otagovaný):

* Výměna CSRF tokenu mezi _Springem_ a _Wicketem_ (tag `local-ldap`).
* [Multiple HttpSecurity](//docs.spring.io/spring-security/site/docs/5.0.0.RC1/reference/htmlsingle/#multiple-httpsecurity) --- v jedné aplikaci: autentikace uživatele přes formulář a mutual-autentication REST služeb přes certifikát (tag `form-login`).
* Autentikace vůči lokálnímu (embedovanému) LDAPu (tag `local-ldap`).
* Autentikace vůči _Active Directory_ (tag `remote-ad`).

## Související články

* [Spring Security, SAML &amp; ADFS: Úvod](/2017/12/spring-security-saml-adfs-uvod/)
* [Spring Security, SAML &amp; ADFS: Konfigurace](/2018/01/spring-security-saml-adfs-konfigurace/)
