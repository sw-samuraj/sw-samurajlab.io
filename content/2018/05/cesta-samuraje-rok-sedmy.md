+++
title = "Cesta samuraje, rok sedmý"
date = 2018-05-30T20:16:00Z
updated = 2018-06-06T08:23:06Z
description = """Byl to těžký rok. A byť, z hlediska blogování, to byl rok
    velice úspěšný, tak co mi v hlavě utkvívá, jsou ty negativní věci.
    Ale nesmutněme - SoftWare Samuraj má narozeniny!"""
tags = ["sw-samuraj"]
aliases = [
    "/2018/05/cesta-samuraje-rok-sedmy.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/img/samurai.gif" >}}

Je květen a blog _SoftWare Samuraj_ má narozeniny. Něž sfouknu pomyslné svíčky, trochu si zavzpomínám, co se od minulé oslavy událo.

Asi to nebude veselý článek --- dlouho jsem se na něj mentálně chystal a stejně si nejsem úplně jistý, jak to dopadne. Emoce jsou někdy silnější, než rozum.

## Co bylo

Mám za sebou těžký rok. A byť, z hlediska blogování, to byl rok velice úspěšný, tak co mi v hlavě utkvívá, jsou ty negativní věci.

### Doba temna

{{< figure class="floatright" src="/2018/05/Red-gauge.jpg"
    link="/2018/05/Red-gauge.jpg" width="200" >}}

Je to rok a měsíc, co jsem v minulé práci oznámil, že odcházím. A začaly se dít věci. Nebylo to rozhodnutí z čistého nebe --- už měsíce a měsíce to k tomu spělo a ačkoliv jsem o tom otevřeně referoval svému bývalému šefovi (říkejme mu BŠ) na [pravidelných 1:1](/2017/10/11-nejdulezitejsi-nastroj-team-leadera/) a vysílal jasné varovné signály, tak BŠ s chápavým úsměvem sledoval, jak klasám pod hladinu řeky Styx.

Je to takový ten klasický příběh: [Šéfové nejvíc přetěžují svoje nejlepší pracovníky](//archiv.ihned.cz/c1-65656110-sefove-nejvic-pretezuji-svoje-nejlepsi-pracovniky). A tak, když jsem pozvolna vyhodnotil situaci, že k žádné změně nedojde, udělal jsem dvě věci:

1. Aktivně jsem se vyvázal ze svých povinností, tedy hlavně [budování týmu](/2018/02/jak-se-stavi-tym/) a [technical recruitmentu](/tags/interview/) a taky [technical leadingu](/2017/10/technical-leader-myticke-stvoreni/) pod-staffovaného, špatně obsazeného a špatně řízeného projektu.
1. S konečnou platností řekl, že končím.

Odezva na sebe nedala dlouho čekat --- BŠ mi obratem zrušil 1:1 a zavedl [rádiové ticho](//en.wikipedia.org/wiki/Radio_silence):

* Po devět měsíců se mnou nekomunikoval,
* pracovně, společensky a lidsky mě ignoroval (ačkoli jsem pořád byl manažer týmu a ohledně projektu jsem měl největší zákaznickou, businessovou i technickou znalost),
* řešil věci za mými zády
* a když si myslel, že se nikdo nedívá, tak se mi zbaběle a nemorálně mstil.

Ale co už. I když jsem přemýšlel, že bych z toho vytěžil článek, který pracovně nazývám _"O špatných šéfech"_ (BŠ by se umístil na stříbrné pozici), nevím, jestli se mi chce v takových sračkách rýpat. Některé běsy je lepší nechat spát.

Jak jsem psal, mé rozhodnutí zrálo dlouho a ve výsledku nebylo emocionální --- neběžel jsem na HR třísknout výpovědí. Jenom jsem tváří v tvář oznámil, že budu končit a začal si hledat novou práci.

Během onoho "zrání" jsem si uvědomil, že potřebuji zásadnější změnu, než jen zaměstnání. Chci změnu více aspektů své práce: technologie, domény, role atd. A zároveň, některé své vysoce seniorní skilly, buď dočasně, nebo trvale opustit. A tak, když jsem se ocitnul na pracovním trhu, trvalo to dlouho, než jsem si něco vybral (a byl vybrán). Své zkušenosti jsem popsal v článku [Smutná zpráva o stavu IT trhu](/2017/09/smutna-zprava-o-stavu-it-trhu/). Bylo to většinou tristní a doufám, že minimálně dalších pět let to nebudu muset absolvovat.

{{< figure class="floatright" src="/2018/05/Merkur-Wichterle.jpg"
    link="/2018/05/Merkur-Wichterle.jpg" width="200" >}}

Kromě těhle negativních věcí musím vyzvednout aspoň jednu pozitivní --- našel jsem si na projektu (po odevzdání svých zodpovědností) svoji niku a mohl se tak po měsíce věnovat svojí oblíbené činnosti: prototypování. Vzniklo z toho několik článků (i když jsem jich původně plánoval víc), z nichž nejvíc si cením [miniseriálu o SAMLu](/tags/saml/).

Nadešel poslední den v mé práci a... BŠ se se mnou ani nerozloučil. Ačkoliv si se mnou naplánoval schůzku (na nejposlednější možnou minutu), nakonec na mě neměl čas... měl totiž telefon 🤣, jistě důležitý. No, aspoň byl v té ne-komunikaci konzistentní. 🙉

### Doba světla

Po 9 měsících temna (vidíte tu symboliku? 😁) jsem nastoupil do nové práce a... všude bylo plno světla, tráva zelenější, obloha modřejší, vzduch krásně voněl. No, každopádně jsem poslední tři měsíce velmi spokojený:

* Mám fajn šéfa (i když, to jsem o BŠ před pár roky [říkal taky](/2014/06/cesta-samuraje-rok-treti/) 😲).
* Jsem v týmu inspirativních lidí, od kterých se mám hodně co učit.
* Přesedlal jsem z _Javy_ na _Golang_.
* Přesedlal jsem korporátních 3-vrstvých aplikací na _cloud_ a _big data_.
* Nedělám projektově, ale produktově.
* Hlavním runtime prostředím pro mne (momentálně) není aplikační server, ale _Docker_.
* Z _Continuous Integration_ jsem se přesunul na _Continuous Delivery_, konkrétně [GitLab Pipelines](//docs.gitlab.com/ee/ci/pipelines.html). _Jenkins_ je pro mne dnes stejný vtip, jako _Maven_.
* Sedím u okna s výhledem na fontánu a mám to autobusem 10 minut do práce.
* Letos jsem za 3 měsíce naběhal tolik, co loni za celej rok. 🏃

Má to i své stinné stránky --- už si cestou do práce tolik nepočtu. 📖

## Co bude?

I když moje křišťálová koule leccos naznačuje, je ošidné dělat předpovědi. Zejména, pokud jde o budoucnost (ha, ha, ha). O čem bych tak mohl psát, naznačuje předešlá sekce --- vždycky jsem psal o tématech, která se týkala mé práce. Kromě těch nových témat, mi ale zbývá ještě pár oblastí z minulosti. Možná o nich napíšu, možná ne. Dejte vědět v komentářích, co by vás zajímalo.

{{< tweet 1001819689717325827 >}}

## Související články

* [Cesta samuraje, rok šestý](/2017/06/cesta-samuraje-rok-sesty/)
* [Cesta samuraje, rok čtvrtý](/2015/05/cesta-samuraje-rok-ctvrty/)
* [Cesta samuraje, rok třetí](/2014/06/cesta-samuraje-rok-treti/)
* [Cesta samuraje, rok druhý](/2013/05/cesta-samuraje-rok-druhy/)
* [Cesta samuraje, rok první](/2012/04/cesta-samuraje-rok-prvni/)
