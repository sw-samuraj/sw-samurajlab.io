+++
title = "GopherCon UK 2018"
date = 2018-08-31T17:51:00Z
updated = 2018-09-06T22:25:13Z
description = """Londýnská konference o Golangu se konala ve dnech 1.–3. srpna
    a ne jen tak ledaskde, ale přímo v The City, v prostorách bývalého pivovaru
    z 18. století, The Brewery. Luxusní místo pro konferenci."""
tags = ["golang", "konference"]
aliases = [
    "/2018/08/gophercon-uk-2018.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2018/08/GopherCon-UK.jpg"
    link="/2018/08/GopherCon-UK.jpg" width="200" >}}

## Should I Stay or Should I Go

Poté, co jsem měl poslední dva roky utrum s technicko-pracovním vzděláváním, se na mě usmálo štěstí a zásluhou nového zaměstnavatele jsem vyrazil na svoji první zahraniční konferenci.

Nových věcí v nové práci je (a bylo) dost a tak jsem měl poměrně široký výběr (technických) domén, ale nejvíc mě to táhlo ke [Golangu](//golang.org/) --- i když jsem k němu přišel jak slepý k houslím, tak jsem si ho velmi rychle oblíbil. A tak, když se v mém Twitter feedu mihnul [@GopherConUK](//twitter.com/gopherconuk), bylo rozhodnuto --- vyrazím do Anglie!

## London Calling

Konference se konala ve dnech 1.-3. srpna v Londýně a ne jen tak ledaskde, ale přímo v [The City](//en.wikipedia.org/wiki/City_of_London), v prostorách bývalého pivovaru z 18. století, [The Brewery](//goo.gl/maps/enh1gc6zy2w). Luxusní místo pro konferenci.

První den byly workshopy (kterých jsem se neúčastnil) a pak dva dny klasických přednášek (to je gró, proč na konference chodím). No a samozřejmě, sociální události (kterým se, jakožto introvert, striktně vyhýbám).

{{< figure src="/2018/08/The-Brewery.jpg"
    link="/2018/08/The-Brewery.jpg"
    caption="The Brewery, konference venue" >}}

## Thursday's Child

Celkově, čvrteční přednášky mi přišly slabší, než ty páteční. Možná je to i záměr organizátorů?

### 🔑 You Might Be a Go Contributor Already and Not Know It

Úvodní keynote Aditya Mukerjeeho ([@chimeracoder](//twitter.com/chimeracoder)) se točila kolem různých způsobů kontribuce --- jako _gopher_ (fanoušek Golangu) můžete:

* ptát se a odpovídat na [StackOverflow](//stackoverflow.com/questions/tagged/go),
* reportovat issue na [GitHubu](//github.com/),
* pořádat [MeetUp](//www.meetup.com/Prague-Golang-Meetup/),
* publikovat [články](/search/label/golang)
* a samozřejmě, sdílet [svůj Go kód](//github.com/sw-samuraj/oci-sd) na GitHubu.

Obecně, budování a participace na Golang komunitě bylo téma, které se rekurentně na konferenci vracelo.

[Záznam přednášky](//www.youtube.com/watch?v=2mgKDqD5Ga8)

### 👍 Three Billy GOats Gruff --- a developer's tale from VMs to serverless

Občas mají přednášky obskurní názvy, ovšem tahle byla dobrá. Michael Hausenblas ([@mhausenblas](//twitter.com/mhausenblas)) prezentoval přechod z jednoduchého monolitu (majícího 2 [goroutines](//golang.org/ref/spec#Go_statements)), přes rozpad stejné funkcionality do kontejnerovaných micro-servis (běžících na [Kubernetes](//kubernetes.io/)) a skončil u serverless funkcí (implementovaných jako [AWS Lambdas](//aws.amazon.com/lambda/)).

Michael pracuje v Red Hatu (a tudíž [OpenShift](//www.openshift.com/)), takže nepřekvapí, že jeho doménou byly hlavně kontejnery (v Lambdách měl dokonce jeden anti-pattern), ale jeho prezentace pěkně a jednoduše ukázala, jak by taková transition mohla vypadat.

[Záznam přednášky](//www.youtube.com/watch?v=GUGYJup0xLc)

{{< figure src="/2018/08/Canary-Wharf.jpg"
    link="/2018/08/Canary-Wharf.jpg"
    caption="Canary Wharf" >}}

### 😐 Broadcasting Channels: Writing a timeout-aware abstraction to enable asynchronous fanout in Go

Sean Kelly ([@StabbyCutyou](//twitter.com/StabbyCutyou)) má hodně rád svého psa --- jeho slidy se jím jenom hemžily. Nicméně celkem vtipné to bylo. Sean si jen tak pro radost (a studium) naimplementoval broadcasting pomocí [channels](//golang.org/ref/spec#Channel_types).

V podstatě bylo vyznění jeho přednášky víc o [Enterprise Integration Patterns](//www.enterpriseintegrationpatterns.com/) (ačkoliv je nikde explicitně nezmínil), než o Go (kterého stejně moc neukázal). I když si původně myslel, že to bude jednoduché, tak se postupně dopracoval k věcem jako Error ~~Queue~~ Channel, re-tries, time-outs atd. Prostě to, co je v EIP už dávno vyřešené.

[Záznam přednášky](//www.youtube.com/watch?v=nG2djhqmSQk)

### 👍 Orchestration of microservices

Bernd Rücker ([@berndruecker](//twitter.com/berndruecker)) je spolu-zakladatel workflow enginu [Camunda](//camunda.com/). Ani v jeho případě nebyla přednáška moc o Go, jako spíš o BPMN, workflow enginech a problémech distribuovaných architektur. Pokud do toho posledního něco málo děláte, asi by vás v jeho prezentaci nic moc nepřekvapilo, ale přednáška byla vtipná a svižná.

Dva, tři nejzajímavější body zahrnovaly:

* Strategie pro [Fallacies of distributed computing](//en.wikipedia.org/wiki/Fallacies_of_distributed_computing) (ignore | re-throw error | retry | stateful retry)
* Konzistence --- těžký problém všech distribuovaných architketur. Tady mě pobavila hláška _"Grownups don't use distributed transactions"_. 😁 Lepší je použít kompenzace ([Saga pattern](//en.wikipedia.org/wiki/Saga_(pattern))).
* [Orchestrace a Choreografie](//stackoverflow.com/a/29808740/7864001) --- choreografie zatemňuje viditelnost procesu, tj. je obtížnější a víc náchylná k (designovým) chybám.

[Záznam přednášky](//www.youtube.com/watch?v=NBo7d5AG-3s)

{{< figure src="/2018/08/Cutty-Sark.jpg"
    link="/2018/08/Cutty-Sark.jpg"
    caption="Cutty Sark" >}}

### 👎 Component and integration tests for micro-services

Jednoznačně nejslabší přednáška celé konference --- Dmitry Matyukhin měl prvně dlouhé intro pro začátečníky o základech testování a pak se podělil o to, jak napsali nějaké služby v Go, ale testovali je... v Ruby!?! Takže místo Golangu jsme koukali, jak se dělá BDD v [Cucumber](//cucumber.io/) 😩 Já jsem teda valil oči a přemýšlel, jestli si chlapec nespletl konferenci. 🤦

[Záznam přednášky](//www.youtube.com/watch?v=Hm10y2tP5QQ)

### 👍 Documenting Go Code with Beautiful Tests

Přednáška Pawła Słomky ([@pawel_slomka](//twitter.com/pawel_slomka)) byla jedna z nejtechničtějších a taky nejvtipnějších toho dne: hodně jsem se zasmál jeho vtipu o _Schrödinger's TDD --- is it TDD alive, or dead?_ 😹

Ohledně testování v Go zmiňoval:

* Používání [Testing flags](//golang.org/cmd/go/#hdr-Testing_flags), zejména `-race` pro detekci _data race_.
* Assertion knihovnu [Testify](//github.com/stretchr/testify)
* Check functions --- inspirace z [httptest package](//golang.org/pkg/net/http/httptest/).

Paweł byl jediný, s kým jsem si na konferenci pokecal --- jednak jsem chtěl něco dovysvětlit ohledně těch _check functions_ a jednak jsme probrali běhání maratonů. 🏃

[Záznam přednášky](//www.youtube.com/watch?v=TGg6cc0QCzw)

{{< figure src="/2018/08/Royal-Observatory.jpg"
    link="/2018/08/Royal-Observatory.jpg"
    caption="Canary Wharf, pohled od Royal Observatory, Greenwich" >}}

### 🔑 Code, Content &amp; Crafting Your Voice

Závěrečná keynote Amy Chen ([@TheAmyCode](//twitter.com/TheAmyCode)) byla velmi... milleniálská. Jak formou, tak obsahem. Doporučuju, pokud chcete budovat svůj _personal brand_. Zejména formou YouTube kanálů a všech těch moderních sociálních sítí, bez kterých [Generace Y](//en.wikipedia.org/wiki/Millennials) nedokáže žít. Možná, že Amy je dobrá kóderka... ale já to zjišťovat nebudu.

Nicméně, abych to úplně nezahodil, jeden dobrý bod tam byl --- pokud chcete být jakkoli virtuálně přítomni ve veřejném prostoru, tak prvotní by mělo být si definovat _identitu_ --- jak se chcete prezentovat, jak chcete, aby vás vaše _audience_ vnímala. Aneb _"Followers follow your identity."_

[Záznam přednášky](//www.youtube.com/watch?v=C5wkhwBhYzI)

{{< figure src="/2018/08/The-City.jpg"
    link="/2018/08/The-City.jpg"
    caption="Západ slunce za The City (bedlivým okem můžete rozeznat The Shard a The Gherkin)" >}}

## Friday I'm in Love

Na přednášky ze (zajímavějšího) druhého dne GohperConu se podíváme v příštím článku: [GopherCon UK 2018, den 2](/2018/09/gophercon-uk-2018-den-2/). Plus nějaké celkové shrnutí konference.

## Mind Map

{{< figure src="/2018/08/GopherCon-day-1.png"
    link="/2018/08/GopherCon-day-1.png"
    caption="GopherCon UK 2018, den 1." >}}

## Související články

* [GopherCon UK 2018, den 2](/2018/09/gophercon-uk-2018-den-2/)
* [GeeCON Prague 2016, den 1](/2016/10/geecon-prague-2016-den-1/)
* [GeeCON Prague 2016, den 2](/2016/11/geecon-prague-2016-den-2/)
* [Můj pohled na Agile Prague 2014](/2014/09/muj-pohled-na-agile-prague-2014/)
