+++
title = "Golang micro-services, první ohlédnutí"
date = 2018-09-30T19:50:00+02:00
updated = 2018-09-12T22:17:41Z
description = """Je to zhruba půl roku, co jsme začali vyvíjet nový produkt:
    smečku mikro-servis, běžících v cloudu (nebo v Dockeru). Všechny jsou napsány
    v Golangu a protože to pro náš 2-pizza team byla nová technologie, bylo
    dobré si udělat retrospektivu."""
tags = ["golang"]
hero_image = "hero.jpg"
+++

{{< figure src="/2018/09/Gopherize-me-1.png" class="floatleft" >}}

Je to zhruba půl roku, co jsme začali vyvíjet nový produkt: smečku mikro-servis,
běžících v cloudu (nebo v _Dockeru_). Všechny jsou napsány v [Golangu](//golang.org/)
a protože to pro náš 2-pizza team byla nová technologie, bylo dobré si udělat retrospektivu.

Hodnocení a pojetí bylo různorodé --- od kritického náhledu na to, co generuje kompilátor,
až ready-steady-go feeling (a.k.a. rapid start development). Já jsem si tradičně sepsal
mind mapu a aby nepřišla nazmar, tak si ji převyprávím.

## Intuitivní pocit

Čím jsem starší, tím jsem intuitivnější. A je to věc, která se špatně kvantifikuje a zejména
se špatně někomu vysvětluje. Každopádně...

Když jsem se letos na jaře dostal ke [Golangu](//golang.org/) (jako slepý k houslím), byl jsem
chvíli nejistý --- první, co moje oko ze syntaxe vypíchlo, byly `*` před definicí typů.
"Šmarjapano, pointry!", chytal jsem se za hlavu. C-čko nikdy nebylo můj oblíbený jazyk.

Ale pak jsem začal v _Go_ programovat a zjistil jsem, že:

* se v něm dobře píše,
* dá se to velmi rychle naučit
* a celkově působí _Golang_ velice odlehčeně (v dobrém smyslu).

Jenže jak píšu, intuice se dost špatně prodává. Takže když se na to podíváme trochu objektivněji...

## Pozitiva

{{< figure src="/2018/09/Pink-glasses.jpg" caption="Zdroj: pixabay.com" >}}

### Clojure-like 😲

Jedna z časných věcí, co ve mně rychle zarezonovala, byly občasné paralelní podobnosti s... [Clojure](//clojure.org/).
Možná teď kroutíte hlavama a možná tam vidím něco, co tam není --- jako polyglot např. tvrdím, že
Čeština a Španělština mají velmi podobnou gramatiku --- ale já bych řekl, že mezi společné
body patří:

* Functional(-like) přístup --- _Golang_ sice není funkcionální, ale dá se v něm tak psát
  (a já v něm taky tak píšu --- na věšení funkcí na `struct` mě moc neužije).
  Navíc, [funkce](//golang.org/ref/spec#Function_types) je vestavěný typ, takže psát
  [high-order funkce](//en.wikipedia.org/wiki/Higher-order_function#Go) je pohoda.
* HTTP abstrakce --- Když jsem se poprvé setkal s [gorilla/mux](//github.com/gorilla/mux),
  byl jsem hned doma: `gorilla/mux` = [ring](//github.com/ring-clojure/ring) +
  [compojure](//github.com/weavejester/compojure), tedy starý dobrý _handlery_ a _middlewary_,
  plus trošku toho routování.
* Jazykový puristi by mě asi pěkně sjeli, ale pro mě jsou _Golang_ [interfacy](//golang.org/ref/spec#Interface_types)
  ekvivalentem _Clojure_ [protokolů](//clojure.org/reference/protocols).
* _Golang_ [channels](//golang.org/ref/spec#Channel_types) jsou jasnou (a přiznanou) inspirací
  pro _Clojure_ knihovnu [core.async](//github.com/clojure/core.async).
* Poslední aspekt míří za hranice obou jazyků --- jak _Golang_, tak _Clojure_ komunita
  silně preferuje knihovny před frameworky.

### Core funkcionality

Líbí se mi, že na úrovni jazyka je k dispozici mraky vestavěných (a šikovných) funkcí. Speciálně,
ohledně mikro-servis jde o package [net/http](//golang.org/pkg/net/http/), který obsahuje [HTTP
Server](//golang.org/pkg/net/http/#Server), [HTTP Handler](//golang.org/pkg/net/http/#Handler) a
[HTTP klienta](//golang.org/pkg/net/http/#Client).

Stejně tak je přímo na úrovni jazyka vyřešený [Context](//golang.org/pkg/context/#Context), ať už
pro potřeby libovolného klienta, nebo třeba jako mechanismus pro kontrolu [konkurence](//blog.golang.org/context).

A nesmím zapomenout na přímou podporu pro současnou [linguu francu](https://en.wikipedia.org/wiki/Lingua_franca):
JSON. Chcete
[marshallovat](//golang.org/pkg/encoding/json/#Marshal)/[demarshallovat](https://golang.org/pkg/encoding/json/#Unmarshal)
z/do `struct`? Chcete enkódovat/dekódovat JSON? Máte to mít --- out of the box.

### Konkurence

K tomu snad není co dodat --- _Golang_ konkurence je parádní: kombo [goroutines](//golang.org/ref/spec#Go_statements)
a [channels](//golang.org/ref/spec#Channel_types) je zkrátka bez-_konkurenční_. 😸 👍

### Jednoduchost

V sekci _Intuitivní pocit_ jsem zmiňoval, že _Golang_ je "light-weight". K tomu přispívá zejména, že:

* **nemá objekty** --- pokud chcete, můžete psát jenom procedurálně/funkcionálně. A pokud bez toho nemůžete být, můžete si
  na [structy](//golang.org/ref/spec#Struct_types) navěsit funkce a přidat
  [constructor-like factory](//golang.org/doc/effective_go.html#composite_literals) funkci.
* **nemá výjimky** --- místo toho může funkce vracet více hodnot a jedna z nich je [error](https://golang.org/ref/spec#Errors).
  Může to být diskutabilní design, ale než se pustíte do plamenného zatracování, přečtěte si článek
  [Why Go gets exceptions right](//dave.cheney.net/2012/01/18/why-go-gets-exceptions-right).
* preferuje _composition over inheritance_.
* Na to, abyste vytvořili "production-ready" mikro-servisu, stačí vám jazyk samotný a jeho core knihovny. Nepotřebujete:
  * žádný kontejner (a.k.a. servlet),
  * žádný aplikační server,
  * žádné runtime prostředí (vyjma operačního systému).

A musím říct, že po 12 letech v Javě je to osvobozující.

### Testy

Psaní testů v _Golangu_ je fajn: jednoduché, stručné a čitelné. Hodně se mi líbí (a používám)
[tabulkové testy](//github.com/golang/go/wiki/TableDrivenTests). Také se mi líbí (a zatím nepoužívám)
[benchmarky](//golang.org/pkg/testing/#hdr-Benchmarks) a [examply](//golang.org/pkg/testing/#hdr-Examples).

Pro testování mikro-servis je šikovná (core) knihovna [httptest](//golang.org/pkg/net/http/httptest/), která
umožňuje testovat produkční handlery vůči lokálnímu, z testu spuštěnému HTTP(S)
[Serveru](//golang.org/pkg/net/http/httptest/#Server).

Mikro-servisy musí někde běžet --- my je do cloudu deployujeme [Terraformem](//www.terraform.io/). _Terraform_
a ([Packer](//www.packer.io/)) se dají dobře testovat [Terratestem](//github.com/gruntwork-io/terratest).
Je to trochu obsáhlejší záležitost, o kterém možná časem napíšu samostatně. (Mmch. koncept
[Infrastructure as Code](//en.wikipedia.org/wiki/Infrastructure_as_Code) jde ruku v ruce
s [Continuous Delivery](//en.wikipedia.org/wiki/Continuous_delivery), což je téma, které mám v roadmapě.)

### Domain fit & synergie

Ačkoliv je _Golang_ "general-purpose language", vnímám to tak, že si (prozatím?) vydobyl určitou doménu ---
jednak systémové programování (takové ty CLI, typicky Unix utilitky) a jednak věci kolem cloud infrastruktury:

* Počínaje [Dockerem](//www.docker.com/) a [Kubernetes](//kubernetes.io/), což může být gró dané platformy,
* přes [Terraform](//www.terraform.io/) pro provisioning,
* až po [Prometheus](//prometheus.io/) monitoring.

V podstatě na co jsem posledního půlroku sáhnul, tak bylo napsaný v _Golangu_. Je to jenom náhoda? 🤔 Čistě
jen pro úplnost ještě přihodím distribuovaný key-value store [etcd](//coreos.com/etcd/) a
[GitLab Runner](//docs.gitlab.com/runner/) jako komponent distribuovaného _Continuous Delivery_.

## Negativa

{{< figure src="/2018/09/Sad-Teddy.jpg" caption="Zdroj: pixabay.com" >}}

### Chybějící "standardy"

_Go_ má distribuovanou komunitu, která je hodně fragmentovaná --- to, že se koncentruje na GitHubu je
signifikantní (totéž platí částečně pro _Clojure_). Pokud k tomu přičteme rozšířený a podporovaný
přístup [DIY](//en.wikipedia.org/wiki/Do_it_yourself), je výsledkem, že spoustu věcí si každý řeší
po svém.

Typickým příkladem je např. logování --- chybí jednotný logovací interface, existuje kolem 15
soupeřících (a nekompatibilních) implementací a nějaká konvergence je v nedohlednu.

{{< figure src="/2018/09/xkcd-standards.png" caption="Zdroj: xkcd.com" >}}

Podobná situace vládne ve formátu konfiguračních souborů -- někdo je má v [TOML](//github.com/toml-lang/toml),
někdo v [YAML](//yaml.org/), někdo v [JSON](//json.org/)... 🤦

Samostatnou kapitolou je _version_ a _dependency management_ --- to byl můj nejzásadnější problém za uplynulý
půlrok. Ani se mi to nechce moc rozebírat. Tady snad ale problémy vyřeší
[Go Modules](//golang.org/doc/go1.11#modules) (ale že to trvalo!).

### Enterprise připravenost

Věc, kterou jsme v práci dost řešili --- jak používat _Golang_ v enterprise sféře. Takový ty věci jako:

* proprietární source code repository,
* práce za proxy (to je bolest! 🤕),
* reprodukovatelnost buildů,
* cachování závislostí,
* proprietární závislosti (knihovny, co nemůžete mít na GitHubu).

Částečně jsem se tomu věnoval v článku [Správa proprietárních závislostí
v Golang](/2018/06/sprava-proprietarnich-zavislosti-v-golang/) a ještě se k tomu vrátím v postu
_Správa proprietárních závislostí v Golang: go modules_ (TBD).

Tady je to jasné --- _Golang_ ještě není "enterprise ready" a člověk musí vymyslet a naimplementovat
pár workaroundů, aby to přijatelně fungovalo. Ale snad se blýská na lepší časy
s projektem [Athens](//github.com/gomods/athens) (o kterém jsem [referoval
z GopherConu](/2018/09/gophercon-uk-2018-den-2/)).

### Pointers

Už jsem to naznačoval v úvodu --- [Pointry](//golang.org/ref/spec#Pointer_types) nejsou můj šálek kávy.
No ale budiž, na druhou stranu jsem zastáncem, že když už člověk nějaký nástroj používá, měl by ho používat
správně a efektivně (ne nadarmo má tenhle blog podtitul _Master your tools!_).

U pointrů v _Go_ mi ale tak nějak schází motivace/rationale, proč tam vlastně jsou:

* schází _pointer arithmetic_,
* kompilátor se téměř(?) vždycky rozhodne lépe než člověk, jak daný kód zoptimalizovat,
* (zejména u generovaného kódu) dochází k absurditám jako `*bool`, či `*string`.

### Kolekce

V _Javě_ jsem začínal na verzi *1.4* a tak jsem zhruba přes 10 let strávil psaním v "moderní" _Javě_,
tedy _Javě_, která má [Collections API](//en.wikipedia.org/wiki/Java_collections_framework). Pokud jsem
někdy používal _Java_ [Array](//docs.oracle.com/javase/10/docs/api/java/lang/reflect/Array.html),
tak jenom proto, že jsem upravoval nějaký prastarý legacy kód, nebo musel používat nějaký obstarožní
framework (naposled třeba [CMP](//www.bouncycastle.org/docs/pkixdocs1.5on/org/bouncycastle/cert/cmp/package-summary.html)
modul od [Bouncy Castle](//bouncycastle.org/) 😢).

(Střih do současnosti) _Golang_ má v podstatě jenom [pole](//golang.org/ref/spec#Array_types) a
[mapu](//golang.org/ref/spec#Map_types). Sice existuje třeba [list](//golang.org/pkg/container/list/), ale
za posledního půl roku jsem prolezl opravud _hodně_ zdrojáků na GitHubu a stejně nikdo nic jiného,
než _pole_ a _mapy_ nepoužívá.

Tak nevím --- používat na veškeré data operace ten nejprimitivnější sekvenčně-datový typ mi přijde...
primitivní.

## (Zatím) netečný

{{< figure src="/2018/09/Crocodiles.jpg" caption="Zdroj: pixabay.com" >}}

Je pár věcí, ke kterým jsem se v _Golangu_ zatím ještě nedostal, hlouběji je neprozkoumal, anebo je vlastně
ještě nepotřeboval. Nespadají tak ani do pozitivních, ani negativních věcí. Možná se do jedné z těchto
kategorií časem přehoupnou, možná že zůstanou tam kde jsou.

### gRPC

Dnešním světem vládne REST. [gRPC](//grpc.io/) může být zajímavá alternativa, ale nejsem si moc jistý,
jak je to etablovaný a jak se to globálně rozšíří. Nechám tomu nějaký čas a uvidím.

### Interfaces

_Golang_ [interfacy](//golang.org/ref/spec#Interface_types) jsou zajímavě pojaté a je to jedno z témat,
do kterých bych se rád víc ponořil. Prozatím, přiznávám se, jsem nenapsal ani jediný. Možná je to mým
zlomovým útěkem od _Javy_, že se jim podvědomě vyhýbám. Radši si je prvně z povzdálí pořádně očíhnu,
než si s nima ušpiním ruce.

### Security

Nic komplexnějšího, týkajícího se security, jsem v _Go_ zatím nedělal. Samozřejmě, nemám na mysli
takové ty drobnosti, jako [TLS Server](//golang.org/pkg/net/http/#ServeTLS), nebo načtení/vygenerování
[privátního klíče](//golang.org/pkg/crypto/rsa/#PrivateKey). Tak snad se časem něco zajímavého vyvrbí.

## Shrnutí

_Golang_ retrospektivu jsme v práci dělali tři programátoři a skončilo to výsledkem 2:1 ve prospěch
_Go_. Za mne jednoznačně palec nahoru 👍 a klidně bych další projekt _podobného_ typu dělal znova
v _Golangu_.

Pokud by příští projekt neměl být postavený na mikro-servisách, ale na komplikovanější architektuře,
bylo by dobré si to znovu vyhodnotit --- přece jenom, _Go_ (a vývoj v něm) má svoje úskalí.
Ale zatím, zatím dobrý!

## Mind Map

{{< figure src="/2018/09/Golang-Microservices.png" link="/2018/09/Golang-Microservices.png" >}}
