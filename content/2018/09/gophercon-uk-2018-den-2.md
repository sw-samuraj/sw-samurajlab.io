+++
title = "GopherCon UK 2018, den 2"
date = 2018-09-06T22:18:00Z
updated = 2018-09-07T12:16:46Z
description = """V minulé části jsme se podívali na první den londýnské
    Golang konferencce GopherCon UK. Čvrteční přednášky nebyly až tolik
    Golang-technické (a žádná z nich nebyla advanced) a z tohoto pohledu
    byl páteční den přínosnější i zajímavější."""
tags = ["golang", "konference"]
aliases = [
    "/2018/09/gophercon-uk-2018-den-2.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2018/08/GopherCon-UK.jpg"
    link="/2018/08/GopherCon-UK.jpg" width="200" >}}

Nevím, jestli někdo odhalil smysl nadpisů v [minulé části](/2018/08/gophercon-uk-2018/) (jednoduché 4-dílné řešení můžete psát do komentářů, plus jedno bonusové i v tomto textu), kdy jsme se v článku podívali na první den londýnské Golang konferencce [GopherCon UK](//www.gophercon.co.uk/), která se konala ve dnech 1.-3. srpna.

Po středečních workshopech, kterých jsem se neúčastnil, proběhly dva dny přednášek. Už jsem zmiňoval, že čvrteční přednášky nebyly až tolik Golang-technické (a žádná z nich nebyla _advanced_) a z tohoto pohledu byl páteční den přínosnější i zajímavější.

### 🔑 Growing a Community of Gophers

Úvodní keynote Cassandry Salisbury ([@Cassandraoid](//twitter.com/Cassandraoid)) jsem neviděl --- ať už to bylo proto, že jsem se rozhodnul snídat déle, či jít z hotelu na konferenci cca 50 minut pěšky (Londýn je obrovský), anebo proto, že mě téma moc nezajímalo. Jak už jsem psal, téma budování komunity na konferenci (aspoň mezi speakery) silně rezonovalo.

[Záznam přednášky](//www.youtube.com/watch?v=awmsp2N6trY)

{{< figure src="/2018/09/London-Eye.jpg"
    link="/2018/09/London-Eye.jpg"
    caption="London Eye" >}}

### 👍 From source code to Kubernetes, a Continuous Deployment tale

Z přenášky Alexandra Gonzáleze ([@agonzalezro](//twitter.com/agonzalezro)) si toho moc nepamatuju --- jen to, že měl pekné live-demo, ve kterém ukazoval:

1. Jednoduché _Hello, world!_ s využitím [gorilla/mux](//github.com/gorilla/mux).
1. Push do GitHub repozitory.
1. [Webhook](//en.wikipedia.org/wiki/Webhook) + push na (public) Jenkins.
1. Deployment do Kubernetes clusteru.

Nic světoborného, takhle si představuju [Continuous Deployment](//en.wikipedia.org/wiki/Continuous_delivery#Relationship_to_continuous_deployment). Samozřejmě, chyběly tam testy atd. a čekal bych nějaké [modernější Pipelines](//docs.gitlab.com/ee/ci/pipelines.html), než obstarožní Jenkins, ale jinak pěkná a svižná přednáška.

[Záznam přednášky](//www.youtube.com/watch?v=MuxkMzFVkKI)

### 👍 Goroutines: the dark side of the runtime

Konečně přednáška, která přesahovala mé Golang technické znalosti! Roberto Clapis ([@empijei](//twitter.com/empijei)) se ponořil do záludností [goroutines](//golang.org/ref/spec#Go_statements). Zkuste si tipnout, jaký bude výstup tohoto programu:

{{< gist sw-samuraj 0226f3a4416129b0421d5040fc01c4e9 >}}

Že to bude deset desítek asi nikdo z vás netipoval, co? 🤔

To podstatné z Robertovy přednášky:

* Go má jak goroutines, tak closures --- nemíchat!
* Runtime is not preemptive --- goroutine is done when it says it's done.
* Garbage collection (stop the world) --- goroutines are asked to yield execution. Čekání na goroutines může blokovat GC &rarr; freeze all the cores.
* Goroutine cannot be stopped. Měly by explicitně vracet `return` a přijímat [context](//golang.org/pkg/context/) (s [cancel funkcí](//golang.org/pkg/context/#CancelFunc)), nebo [done channel](//gobyexample.com/closing-channels).
* Check for cancellation wherever possible. Pokud knihovna/funkce cancellation neumožňuje, dodejte svoji vlastní via closure.

A pro zvědavce, jak by měl předešlý kód správně vypadat, aby dával očekávaný výstup (ďábel 😈 je skrytý v detailu).

{{< gist sw-samuraj 60f7fff1190dea6ea5256de1c6e566c6 >}}

Pro vysvětlení se podívejte buď na přednášku, nebo si přečtěte [Effective Go](//golang.org/doc/effective_go.html).

[Záznam přednášky](//www.youtube.com/watch?v=4CrL3Ygh7S0)

{{< figure src="/2018/09/St-Paul.jpg"
    link="/2018/09/St-Paul.jpg"
    caption="St Paul Cathedral a The City" >}}

### 👍 Understanding Go's Memory Allocator

André Carvalho ([@andresantostc](//twitter.com/andresantostc)) doručil druhou z nejtechničtějších přednášek --- vysvětlení, jak funguje _Go Memory Allocator_:

* Založený na [TCMalloc](//gperftools.github.io/gperftools/tcmalloc.html) (Thread-Caching Malloc).
* Navíc přidává _tiny allocations_ (k původním _small_, _medium_ a _large_ alokacím).
* Go (očividně) nemá `malloc`, ani `free`.
* [Pragma](//dave.cheney.net/2018/01/08/gos-hidden-pragmas) `//go:noinline` prevents [inlining](//en.wikipedia.org/wiki/Inline_expansion).
* Kompilátor se snaží držet většinu proměnných na _stacku_ (levnější) a jenom ty, které uniknou (escape) dává na _heap_.
* Volání funkce [mallocgc()](//golang.org/src/runtime/malloc.go) vkládá kompilátor.
* GC Sweeping:
  1. Scan all objects.
  1. Mark objects that are live.
  1. Sweep objects that are not alive.
* Šikovná funkce [ReadMemStats()](//golang.org/pkg/runtime/#ReadMemStats) --- runtime statistiky memory allokátoru.

[Záznam přednášky](//www.youtube.com/watch?v=3CR4UNMK_Is)

### 👍 Building Resilient Data Pipelines in Go

Přednáška Granta Griffithse ([@griffithsgrant](//twitter.com/griffithsgrant)) byla primárně o případu užití Go --- vytvoření data pipelines, které přesouvají data z [IoT](//en.wikipedia.org/wiki/Internet_of_things) do zákaznických aplikací. Pipelines, které přesouvají data z [Apache Kafka](//kafka.apache.org/) nodů do [Apache Cassandra](//cassandra.apache.org/) nodů, měli původně napsané v Javě, ale kvůli provozním nákladům je začali přepisovat do Golang. Pro porovnání:

* 30 Kafka nodes &rarr; 144 Java pipelines nodes &rarr; 150 Cassandra nodes = 900.000 writes/sec.
* 4 Kafka nodes &rarr; 32 Go pipelines &rarr; 9 Cassandra nodes = 450.000 writes/sec.

Po náběhu na stejnou úroveň nodů jako u Java řešení očekávají dvojnásobné množství zápisů do Cassandry.

Design Go data pipelines vypadá následovně:

* Go micro-services
* Go channels
  * Message channel --- čte data z Kafky
  * Notification channel --- [rebalance](//stackoverflow.com/a/30988129/7864001) notifikace
  * Error channel --- chyby v [Offset Managementu](//cwiki.apache.org/confluence/display/KAFKA/Offset+Management)
  * OS Signals channel --- naslouchání signálům OS pro _graceful shutdown_
* Message transformation
* Deployované na [K8s](//kubernetes.io/docs/concepts/overview/what-is-kubernetes/#what-does-kubernetes-mean-k8s) cluster

Zajímavé bylo také [Reliability Testing](//en.wikipedia.org/wiki/Reliability_engineering#Reliability_testing), něco vy stylu:

1. Pošli 50 zpráv,
1. "Za-pauzuj" Docker po 10 zprávách,
1. "Od-pauzuj" Docker po 30 zprávách.

Celkově, velmi zajímavá přednáška o real-world, large-scale use casu, jakých na koferenci moc nebylo (což mi chybělo).

{{< figure src="/2018/09/Golden-Hind.jpg"
    link="/2018/09/Golden-Hind.jpg"
    caption="Golden Hind" >}}

### 👍 How do you structure your Go apps?

Od prezentace Kat Zien ([@kasiazien](//twitter.com/kasiazien)) jsem nečekal žádné zjevení, ale ve výsledku to byla příjemná přednáška a i něco nového jsem se v závěru přiučil. Jak strukturovat Go projekt je vousatá otázka. Přednáška shrnula různé layouty a krátce vypíchla jejich charakteristiky. Tedy:

* **Flat structure** --- všechno je v `main` packagi, vhodné pro malé projekty. Z pohledu na soubory je většinou nejasná business doména.
* **Group by function** --- klasika, která většinou vede k něčemu, jako je [MVC](//en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller).
* **Group by module** --- seskupení podle business domény. Vede k repetitivnosti (`user.User`, `user.AddUser()` atd.) a není intuitivní (kam přidat novou funkci?).
* **Group by context** --- vhodné pro komplikovanější aplikace:
  * [DDD](//en.wikipedia.org/wiki/Domain-driven_design) přístup,
  * [Hexagonal Architecture](//fideloper.com/hexagonal-architecture)
  * [Actor Model](//en.wikipedia.org/wiki/Actor_model) = [goroutines](//golang.org/ref/spec#Go_statements) + [channels](//golang.org/ref/spec#Channel_types)

Takže, jako vždycky --- [No Silver Bullet](//en.wikipedia.org/wiki/No_Silver_Bullet), strukturu svého Golang projektu si budete muset vyřešit sami. 😉

[Záznam přednášky](//www.youtube.com/watch?v=VQym87o91f8)

### 🔑 Athens --- The Center of Knowledge

Brian Ketelsen ([@bketelsen](//twitter.com/bketelsen)), mající závěrečnou keynote, přinesl asi nejzajímavější téma celé akce. Obecně, problém verzování a dependency managementu Golang packagů mi na konferenci hodně chybělo --- je to nejsilnější a nejbolavější téma posledního půlroku, který jsem s Go strávil. Občas se sice někdo zeptal, kdo používá [vgo](//github.com/golang/go/wiki/vgo) a kdo používá [dep](//github.com/golang/dep), ale žádná přednáška se tomu nevěnovala.

Brian představil zbrusu nový projekt --- [Athens](//github.com/gomods/athens), který zastřešuje několik aktivit. Mimo jiné poskytuje:

* **Caching proxy server**, který může běžet za firewallem.
* **Distributed trust**, potvrzující autenticitu balíčků.
* **Decentralized verification**, nezávisející na jednom centrálním poskytovateli (a.k.a. evil corporation).

Co to všechno nabízí (resp. bude poskytovat)?

* **Repeatable builds** (i pokud je GitHub/GitLab/Bitbucket nedostupný).
* **Module validation**, využívající hash v `go.sum` souboru (nyní součást [Go modules](//golang.org/cmd/go/#hdr-Module_downloading_and_verification)).
* **Module signing**, pomocí sítě distribuovanýcn "notářství" (notaries).
* **Discoverability** = Notaries + Publishers (collect certificates from Notaries).

Hlavním heslem je: **Decentralized, Federated, Independent**. Bude hodně zajímavé tenhle projekt sledovat!

[Záznam přednášky](//www.youtube.com/watch?v=_wXf5Oixz28)

{{< figure src="/2018/09/Tower-Bridge.jpg"
    link="/2018/09/Tower-Bridge.jpg"
    caption="Tower Bridge" >}}

## Gopher --- to be, or not to be?

Celkově hodnotím konferenci pozitivně. Co bych vypíchnul, byla profesionalita, která byla všudypřítomná. Nevím, jestli je v Česku něco podobně srovnatelného. Samozřejmě, nesmím zapomenout na dobré jídlo a catering.

{{< figure class="floatleft" src="/2018/09/Food.jpg"
    link="/2018/09/Food.jpg" width="400" >}}

Orientaci na komunitu jsem opakovaně zmiňoval. Což je pozitivní --- Go je velice otevřená platforma a byť je to jazyk, který se blíží první dekádě, je zatím relativně mladý a řekl bych, že --- minimálně v oblasti cloudu (a cloudové infrastruktury) --- ještě stále nabírá momentum (které do Česka zatím ještě moc nedorazilo).

Co mi na konferenci chybělo? Advanced přednášky --- pokud má konference 3 streamy, jsou dvě pokročilé prezentace za dva dny málo. Taky mi chybělo víc use case použití, real-world scénářů, kdy Go je dobrá/správná/excelentní volba.

{{< figure class="floatright" src="/2018/09/Desert.jpg"
    link="/2018/09/Desert.jpg" width="400" >}}

Musím ocenit, že na konferenci bylo relativně hodně holek --- na jednu stranu je to trochu překvapující (Go je docela low-level jazyk), na druhou stranu, na konferenci bylo znát, že nastupující/etablovanou generací jsou [Milleniálové](//en.wikipedia.org/wiki/Millennials), což možná jde ruku v ruce.

Takže, za mne palec nahoru 👍 a snad se jednou dočkáme Golang konference i v Česku. 🤞

## The Loneliness of the Long Distance Runner

Konference je jedna věc, co dělat ve zbývajícím volném čase je jiná záležitost. Londýn samozřejmě nabízí mnoho lákadel, od volného vstupu do mnoha muzeí a galerií, přes kvantum pamětihodností, až po obrážení tradičních pubů a rozmanitých restaurací s národními jídly.

Byť jsem některé z těchto možností také využil, hlavně jsem si užil Londýn běžecky --- sice to není úplně intuitivní volba, ale když to člověk zkusí, zjistí, že tak pokryje docela velký akční rádius, včetně bonusových překvapení. Můžu doporučit. Doprovodné fotografie (vyjma 3 konferenčních) jsou pořízeny právě z těchto běžecko-průzkumných výprav.

## Mind Map

{{< figure src="/2018/09/GopherCon-day-2.png"
    link="/2018/09/GopherCon-day-2.png"
    caption="GopherCon UK 2018, den 2." >}}

## Související články

* [GopherCon UK 2018](/2018/08/gophercon-uk-2018/)
* [GeeCON Prague 2016, den 1](/2016/10/geecon-prague-2016-den-1/)
* [GeeCON Prague 2016, den 2](/2016/11/geecon-prague-2016-den-2/)
* [Můj pohled na Agile Prague 2014](/2014/09/muj-pohled-na-agile-prague-2014/)
