+++
title = "Maximální počet otevřených souborů v Ubuntu"
date = 2018-03-17T10:25:00Z
updated = 2018-03-17T10:25:36Z
description = """Operační systémy a někdy i přímo jazyky, či jejich runtimy
    mají omezený maximální počet otevřených souborů. Z bezpečnostních a
    performance důvodů. Občas se vám stane, že na tento limit narazíte a
    potřebujete ho upravit. Jak to pořešit na Ubuntu?"""
tags = ["linux"]
aliases = [
    "/2018/03/maximalni-pocet-otevrenych-souboru-v.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/img/logos/Ubuntu.png" >}}

Operační systémy a někdy i přímo jazyky, či jejich runtimy mají omezený maximální počet otevřených souborů. Z bezpečnostních a performance důvodů. Občas se vám stane, že na tento limit narazíte a potřebujete ho upravit. Jak to pořešit na _Ubuntu_?

Začal jsem teď na stávajícím Java projektu a první, co jsem zkusil --- zbuildovat ho. Bylo tam plno testů (a mraky logování z testů) a běželo to dlouho. Život je plný překvapení: Vyskočila na mne výjimka, co jsem zatím ještě neviděl:

{{< highlight bash >}}
java.io.FileNotFoundException: /path/to/file (Too many open files)
{{< /highlight >}}

Java používá pro maximální počet otevřených souborů limit operačního systému. Pro nastavení (nejen) tohoto limitu slouží soubor `/etc/security/limits.conf`. Limity se nastavují per uživatel/skupina a nastavují se dva: _soft_ a _hard_. Přiznám se, úplně nechápu, jak (pro soubory) funguje _soft_ limit --- z [dokumentace](//man7.org/linux/man-pages/man2/getrlimit.2.html) jsem to úplně nepochopil. Nicméně nás bude (pro Javu) stejně zajímat pouze _hard_ limit, který procesu řekne: [Hasta la vista, baby!](//www.youtube.com/watch?v=Q73gUUr8Zlw)

Jaký limit otevřených souborů máte momenálně k dispozici, zjistíte příkazy:

* `ulimit -Sn` (_soft_ limit maxima otevřených souborů)
* `ulimit -Hn` (_hard_ limit maxima otevřených souborů)

{{< figure src="/2018/03/Soft-and-hard-limit.png"
    caption="Výpis soft a hard limitu max. počtu otevřených souborů" >}}

Příklad nastavení:

{{< gist sw-samuraj 4b46302a1383cb1157157aa47e74256d >}}

Tak. A mohli bysme mít hotovo --- budete to fungovat... pokud se přihlašujete z terminálu. Typický use case by byl třeba nastavení limitů pro webový server. Pokud vás to ale trápí jako vývojáře na lokálním prostředí, tak budete muset použit příkaz `su`, aby se změna v terminálu projevila. Problém je, že v grafickém prostředí se soubor `limits.conf` ignoruje

{{< figure src="/2018/03/Soft-and-hard-limit-after-su.png"
    caption="Výpis limitů po změně uživatele" >}}

Aby limit pro počet otevřených souborů vzalo v potaz i GUI, je potřeba ještě poeditovat soubor `/etc/systemd/system.conf` (a v některých případech i soubor `/etc/systemd/user.conf`):

{{< gist sw-samuraj 062a21493c37ccd7a0c3f3f69fd0d526 >}}

Pak už stačí jen systém restartovat a máme hotovo. (Možná to jde i bez restartu pomocí nějaké [systemctl](//www.freedesktop.org/software/systemd/man/systemctl.html) magie, ale na to jsem nepřišel.)
