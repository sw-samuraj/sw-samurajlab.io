+++
title = "Monitoring OCI instancí pomocí Prometheus (Service Discovery)"
date = 2018-12-05T12:51:15+01:00
description = """Monitorování je důležitou součástí produkčních aplikací. Co když
    ale chceme monitorovat cloudovou infrastrukturu, nebo aplikace běžící v cloudu?
    Prometheus je etablovaným řešením pro tyto případy, kdy out-of-the-box
    umoňuje monitorovat většinu známých platforem. Bohužel, podpora pro OCI
    (Oracle Cloud Infrastructure) zahrnuta není a tak jsem si ji musel napsat sám."""
tags = ["cloud", "monitoring"]
hero_image = "hero.jpg"
thumbnail = "2018/12/Monitoring.jpg"
+++

{{< figure src="/2018/12/Monitoring.jpg" >}}

Cloudů je dneska plnej internet. Já vím, že když nepoužíváte AWS, tak jste
totální loser, kterej si ani neumí zavázat tkaničky... (aspoň tak se mi to jeví
podle některých siláckých prohlášení), ale já bych se dneska podíval
na jinou cloudovou alternativu a hlavně --- jak ji monitorovat.

## OCI

{{< figure class="floatright" src="/img/logos/OCI-compute.png" width="80" >}}

[Oracle Cloud Infrastructure](//cloud.oracle.com/iaas) (OCI) je IaaS platforma,
která se --- nejen z pohledu dnes probíraného monitoringu, ale třeba i architektury
--- nijak neliší od toho, co můžete potkat v [AWS](//aws.amazon.com/),
[Azure](//azure.microsoft.com), nebo [GCP](//cloud.google.com/). OCI se skládá
z několika [produktů](//cloud.oracle.com/en_US/cloud-infrastructure), nicméně
dneska se zaměřím pouze na [Compute](//cloud.oracle.com/compute).

Jak název napovídá, _Compute_ poskytuje výpočetní zdroje --- můžeme
nastartovat VM instance s požadovanými parametry (jako počet CPU a velikost
paměti), tyto VM-ka běží ve specifické VCN ([Virtual Cloud
Network](//cloud.oracle.com/en_US/networking)) a v daném subnetu atd.
A samozřejmě, můžeme si nakonfigurovat nějaké load-balancery, nebo
omezit síťovou komunikaci pomocí [security
lists](//docs.cloud.oracle.com/iaas/Content/Network/Concepts/securitylists.htm).

Dobrá... máme nainstancovaná nějaká VM-ka a chtěli bychom je monitorovat --- jak na to?
Jedním, v cloudu velmi dobře etablovaným řešením na monitorování je
[Prometheus](//prometheus.io/) suita. Takže, jak zapřáhneme _Prometheus_, aby
nám monitoroval OCI instance?

## Prometheus

{{< figure class="floatright" src="/img/logos/Prometheus.png" width="70" >}}

Cíle (targets), které _Prometheus_ monitoruje, se definují pomocí [scrape
configuration](//prometheus.io/docs/prometheus/latest/configuration/configuration/#%3Cscrape_config%3E).
A ačkoliv out-of-the-box obsahuje _Prometheus_ podporu pro _service discovery_
většiny etablovaných cloudů, OCI mezi ně nepatří.

Nevadí, říkám si --- napíšu si _OCI Service Discovery_ sám. Proštudoval jsem si,
jak funguje _service discovery_
v [AWS](//github.com/prometheus/prometheus/tree/master/discovery/ec2),
v [Azure](//github.com/prometheus/prometheus/tree/master/discovery/azure),
v [GCP](//github.com/prometheus/prometheus/tree/master/discovery/gce),
v [OpenStack](//github.com/prometheus/prometheus/tree/master/discovery/openstack), a
v [Kubernetes](//github.com/prometheus/prometheus/tree/master/discovery/kubernetes);
[forknul Prometheus](//github.com/sw-samuraj/prometheus/) a během celkem krátké
doby napsal a odladil [service discovery pro OCI](//github.com/sw-samuraj/prometheus/tree/master/discovery/oci).

Bohužel, vývojáři _Promethea_ si uložili moratorium na přidávání nových poskytovatelů
_service discovery_ a můj pull request [zamítli](//github.com/prometheus/prometheus/issues/4322).
Místo toho mi doporučili adaptovat [file-based service
discovery](//prometheus.io/docs/prometheus/latest/configuration/configuration/#%3Cfile_sd_config%3E)
jako preferovanou alternativu.

Tento způsob _discovery_ znamená, že _Prometheus_ periodicky načítá určitý soubor
(nebo množinu souborů), který obsahuje seznam monitorovaných cílů. Obsahem tohoto
souboru je JSON struktura v následujícím formátu:

{{< highlight json >}}
[
  {
    "targets": [ "<host>", ... ],
    "labels": {
      "<labelname>": "<labelvalue>", ...
    }
  },
  ...
]
{{< /highlight >}}

Tudíž otázka je, jak tento soubor naplnit seznamem OCI instancí, ideálně s nějakými
smysluplnými labely?

## OCI Service Discovery

Long story short, napsal jsem [oci-sd](//github.com/sw-samuraj/oci-sd), které funguje
úplně stejně, jako to, co jsem dopsal do _Promethea_, akorát nakonci vyprodukuje
zmíněný soubor. Navíc se dá spustit jako command-line aplikace (daemon), nebo se dá
naimportovat jako _Golang_ package do jiné aplikace.

{{< figure caption="Sekvenční diagram OCI Service Discovery"
    src="/2018/12/OCI-SD-sequence.png"
    link="/2018/12/OCI-SD-sequence.png"
>}}

Jak je vidět z obrázku, _OCI-SD_ se periodicky dotazuje [OCI API](//docs.cloud.oracle.com/iaas/api/)
pro získání seznamu instancí (v daném
[compartmentu](//docs.cloud.oracle.com/iaas/Content/GSG/Concepts/settinguptenancy.htm#two)).
Potom instace transformuje na _targets_ s určitými metadaty a na konci zapíše výsledek
do souboru. Soubor je poté periodicky načítán _Prometheem_, který "pře-labeluje"
([re-label](//www.prometheus.io/docs/prometheus/latest/configuration/configuration/#%3Crelabel_config%3E))
metadata a eventuálně zahodí cíle, které nemají být monitorovány.

## Let's try it!

Žijeme v "éře GitHubu", takže mít dobrou dokumentaci a příklady/ukázky je
kritické. Proto jsem jako součást `oci-sd` projektu připravil krátký
[tutoriál](//github.com/sw-samuraj/oci-sd/tree/master/example),
kde je popsáno jak:

* nastartovat testovací OCI instance pomocí [Terraformu](//www.terraform.io/),
  každá instance s běžícím [node_exporterem](//github.com/prometheus/node_exporter/),
* nakonfigurovat a spustit `oci-sd` aplikaci, která "objeví" spuštěné OCI instance,
* nastartovat _Prometheus_ s `file_sd` konfigurací, který začne instance monitorovat.

### Testovací OCI instance

Potřebné testovací instance lze, samozřejmě, vytvořit ručně v OCI
admin konzoli, nicméně současným trendem (hype? 😉) je [Infrastructure as
Code](//en.wikipedia.org/wiki/Infrastructure_as_code) (IaC), tak proč to
nevyužít? V adresáři [example/terraform](//github.com/sw-samuraj/oci-sd/tree/master/example/terraform)
je sada _Terraform_ skriptů, pro jejichž spuštění jsou potřeba dvě věci:
_Terraform_ [binárka](//www.terraform.io/downloads.html) a _OCI tenance_
s politikou, která umožňuje spravovat zdroje.

Pokud jste se pustili do tohoto scénáře, tak předpokládám, že pro vás
není magií [OCI CLI konfigurace](//docs.cloud.oracle.com/iaas/Content/API/Concepts/sdkconfig.htm).
Pro spuštění _Terraform_ skriptů je potřeba nastavit několik
environment variables: nejjednodušší cesta je poeditovat soubor
[env-vars](//github.com/sw-samuraj/oci-sd/blob/master/example/terraform/env-vars)
a uložit ho s vašima specifickýma hodnotama:

{{< gist sw-samuraj 51cefb449f89f7d3c9640b6fdab80d8a >}}

Pak můžeme proměnné vyexportovat a spustit _Terraform_:

1. `. env-vars`
1. `terraform init`
1. `terraform plan`
1. `terraform apply -auto-approve`

Během několika minut bychom měli vidět následující (zkrácený) _Terraform_
výstup a rovněž běžící instance v OCI konzoli:

{{< highlight none >}}
Apply complete! Resources: 8 added, 0 changed, 0 destroyed.

Outputs:

private-ips = [
    10.1.0.3,
    10.1.0.4,
    10.1.0.2
]
public-ips = [
    129.146.78.36,
    129.146.121.86,
    129.146.125.2
]
{{< /highlight >}}

{{< figure caption="Přehled instancí v OCI konzoli"
    src="/2018/12/OCI-instances.png"
    link="/2018/12/OCI-instances.png"
>}}

Na každé z těchto instancí by měl běžet `node_exporter` na portu `9100`.
Tento port by měl být otevřený v nakonfigurovaném _security listu_:

{{< figure caption="Security list s otevřeným portem pro node_exporter"
    src="/2018/12/Security-list.png"
    link="/2018/12/Security-list.png"
>}}

Že je všechno v pořádku můžeme zkontrolovat utilitkou `curl`:

{{< highlight none >}}
curl <public-ip-of-your-instance>:9100/metrics
{{< /highlight >}}

Pokud se v konzoli objeví následující (zkrácený) výstup (`node_exporter`
metriky), jsme připraveni na _service discovery_.

{{< highlight none >}}
# HELP go_gc_duration_seconds A summary of the GC invocation durations.
# TYPE go_gc_duration_seconds summary
go_gc_duration_seconds{quantile="0"} 2.5834e-05
go_gc_duration_seconds{quantile="0.25"} 5.6118e-05
go_gc_duration_seconds{quantile="0.5"} 6.008e-05
go_gc_duration_seconds{quantile="0.75"} 6.812e-05
go_gc_duration_seconds{quantile="1"} 0.000120973
go_gc_duration_seconds_sum 0.118491416
go_gc_duration_seconds_count 1997
{{< /highlight >}}

### oci-sd

Pro běh `oci-sd` jako standalone aplikace je potřeba dodat konfigurační
soubor. Standardně se nazývá `oci-sd.toml` a měl by obsahovat všechny
ty OCI přístupové hodnoty, které jsme už využili u _Terraform_ skriptů:

{{< gist sw-samuraj 61a3bef0f7f8871305c6e46ab34b2ad5 >}}

Po spuštění `oci-sd` aplikace, např.

{{< highlight none >}}
./oci-sd -c oci-sd.toml -o oci-sd.json
{{< /highlight >}}

bychom měli (zhruba po 1 minutě) vidět obdobný obsah v souboru
`oci-sd.json`:

{{< gist sw-samuraj 1832d7b188fc804384d5d8484c5218c4 >}}

Voilà, už to skoro máme!

### Prometheus

Nakonfigurovat _Prometheus_, aby získal monitorované cíle ze souboru
`oci-sd.json`, je jednoduché. Trochu náročnější může být
[re-labeling](//prometheus.io/docs/prometheus/latest/configuration/configuration/#%3Crelabel_config%3E),
tak tady je krátká ukázka:

{{< gist sw-samuraj 7d94c930bb7de4d916206a27302174a4 >}}

Tato konfigurace řekne _Prometheovi_, aby:

* načetl cíle ze souboru `oci-sd.json`,
* sestavil URL monitorovaného cíle z veřejné IP adresy dané OCI instance a portu `9100`,
* pár jednoduchých pře-labelování (`instance_id`, `instance_name` atd.),
* zahodil všechny cíle, které nemají [free-form
  tag](//docs.cloud.oracle.com/iaas/Content/Identity/Concepts/taggingoverview.htm#workfreeform)
  `node_exporter`

Pokud spustíme _Prometheus_ s touto konfigurací, měli bychom v GUI vidět
nové monitorované cíle:

{{< figure caption="Nové OCI instance jako Prometheus targets"
    src="/2018/12/Prometheus-targets.png"
    link="/2018/12/Prometheus-targets.png"
>}}

Když se ještě vrátím k _Prometheusovskému_ re-labelování --- kompletní množinu
dostupných meta-labelů lze najít v `oci-sd` dokumnetaci:
[Metadata labels](//github.com/sw-samuraj/oci-sd#metadata-labels).

## Time to play!

Nyní můžeme monitorovat naše OCI instance v daném kompártmentu a tak je čas
trochu prozkoumat, že _service discovery_ je opravdu dynamické --- můžeme
zkusit zastavit, nebo zrušit nějakou instanci, vytvořit novou instanci manuálně,
nebo _Terraformem_ a tak dál.

Je dobré mít na paměti dvě věci. Za prvé, chviličku trvá, než se změny
v _Prometheovi_ objeví --- propagace změny je výsledkem tří(!) refresh
intervalů v `oci-sd` a v _Prometheovi_.

A za druhé, speciálně pro nově vytvářené instace, definovali jsme
filtrování instancí pomocí _free-form tagu_, tak aby nás nezaskočilo,
že instance bez tohoto tagu se neukazují jako cíle.

Tak, to je všechno! _Happy monitoring_ a ozvěte se mi s vašimi nápady
a problémy!
