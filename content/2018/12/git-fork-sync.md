+++
title = "Git fork synchronizace"
date = 2018-12-14T22:01:00+01:00
description = """Fork repozitory na GitHubu/GitLabu/Bitbucketu je otázka jednoho
    kliknutí. Stejně tak triviální je fork lokálně naklonovat. Jak ale řešit
    synchronizace mezi dvěma remote repozitory? Krátký cheat-sheet (tahák) jak na to."""
tags = ["git", "version-control"]
hero_image = "hero.jpg"
+++

{{< figure src="/2018/12/Fork.jpg" >}}

Letos jsem se nějak rozjel v kontribucích na _GitHubu_ a už po několikáté jsem musel řešit
(vzpomínat), jak synchronizovat forknuté repo.

## Proč je synchronizace potřeba?

V případě, že se jedná o jednorázovou kontribuci, není potřeba synchronizaci řešit. Postup
je většinou následující:

1. _Fork_ původní repozitory.
1. _Klon_ forknuté repozitory.
1. Lokální změny a _push_ do forknuté repozitory.
1. _Pull request_ do původní repozitory.
1. (Případně) Opakování kroků 3-4.
1. Akceptace _pull requestu_.
1. Dobrý pocit. 😀 😎

Pokud se ale vaše kontribuce opakuje, už je potřeba synchronizaci řešit, protože pokud
jste si pro přispívání nevybrali mrtvý (anebo hodně stabilní) projekt, tak na původním
repo probíhá aktivní vývoj a během doby, kdy probíhal proces vývoje a akceptace vašeho
prvního _pull requestu_, už tam nejspíš někdo nakomitoval další změny.

## Jak vypadá proces synchronizace ?

V následujícím obrázku přibyly k popsanému postupu dva kroky: _5. fetch_ a _6. push_.
To je právě ona synchronizace.

V obrázku pak už chybí další, cyklicky se opakující kroky, tj. pro druhý _pull request_
by ještě přibyl krok _7. pull request_, pro třetí _pull request_ by to byly kroky
_8. fetch_ + _9. push_ + _10. pull request_ atd.

{{< figure src="/2018/12/Git-fork-sync.png"
    caption="Schéma synchronizace mezi Git repozitory"
>}}

## Jak se to dělá v Gitu?

Hlavní důvod, proč píšu tenhle článek je, abych měl zdokumentovanou sadu následujících
příkazů.

Předpokládám, že _fork_ už máme udělaný. Pro potřeby tohoto příkladu použiju jako
původní repozitory [gruntwork-io/terratest](//github.com/gruntwork-io/terratest),
kterou mám forknutou jako [sw-samuraj/terratest](//github.com/sw-samuraj/terratest).

_(Všechny následující příkazy si můžete bez obav pouštět u sebe lokálně. Pouze
pokud byste chtěli zkusit i_ push_, tak si samozřejmě musíte udělat svůj vlastní_
fork_.)_

Nyní si uděláme _klon_ forknuté repo:

```bash
git clone git@github.com:sw-samuraj/terratest.git
```

a vypíšeme si výchozí seznam _remote_ repozitory:

```bash
$ git remote -v
origin	git@github.com:sw-samuraj/terratest.git (fetch)
origin	git@github.com:sw-samuraj/terratest.git (push)
```

### Přidání původního repozitory

Původní repozitory -- většinou nazývanou `upstream` -- přidáme příkazem:

```bash
$ git remote add upstream git@github.com:gruntwork-io/terratest.git
```

Opět si vypíšeme seznam _remote_ repozitory a vidíme, že máme dvě:

* `origin` je náš nový _fork_.
* `upstream` je původní repozitory,

```bash
$ git remote -v
origin	git@github.com:sw-samuraj/terratest.git (fetch)
origin	git@github.com:sw-samuraj/terratest.git (push)
upstream	git@github.com:gruntwork-io/terratest.git (fetch)
upstream	git@github.com:gruntwork-io/terratest.git (push)
```

### Stažení změn z původního repozitory

Samotná synchronizace se skládá ze čtyřech kroků:

1. `fetch` změn v původní repozitory (`upstream`).
1. `checkout` lokálního branche, kam chceme změny zpropagovat.
1. `merge` daného `upstream` branche.
1. `push` do forknuté (`origin`) repozitory.

```bash
$ git fetch upstream
$ git checkout master
$ git merge upstream/master
$ git push
```

Je jen na vás, jestli vaše změny uděláte před, nebo po `merge` z `upstream`
repozitory. Dobré pravidlo je, udělat synchronizaci před zahájením práce
na novém _pull requestu_.

A to je vše. _Happy contributing!_
