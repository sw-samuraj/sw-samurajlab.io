+++
title = "Jak se staví tým"
date = 2018-02-22T11:33:00Z
updated = 2018-02-22T12:01:42Z
description = """Když dostanete možnost postavit nový tým, nebo třeba významně
    doplnit ten stávající, měli byste mít nějakou vizi, jak ten tým bude vypadat.
    Protože budování týmu nekončí přijímacím pohovorem. Nekončí ani po zkušební
    době, či úplném zapracování. Ono totiž nekončí nikdy."""
tags = ["teamleading", "interview"]
aliases = [
    "/2018/02/jak-se-stavi-tym.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure
    src="/2018/02/Building-a-team.jpg"
    link="/2018/02/Building-a-team.jpg"
>}}

Tenhle článek jsem chtěl napsat už několik let. Pořád jsem to odkládal s tím, že časem ještě získám víc zkušeností a tak to bude mít větší, komplexnější váhu. Že pořád na to bude jednou dost času. Ale tak to v životě nechodí... jednou přijde čas a uvědomíte si, že věci, která vás kdysi extrémně přitahovaly, vám najednou nic neříkají. A že pokud to neuděláte teď, tak už to neuděláte nikdy.

Přesně takovým obdobím teď procházím. Přičinil jsem se o zlom ve své kariéře a tak po 8 letech, kdy jsem dělal team leadera a souběžných 6 letech, kdy jsem se intenzivně věnoval technickému rekruitmentu, tyto dvě oblasti (dobrovolně) opouštím. A tak je tenhle článek jakýmsi rozloučením a předáním štafety.

## Na začátku by měla být vize

Když dostanete možnost postavit nový tým, nebo třeba významně doplnit ten stávající, měli byste mít nějakou vizi, jak ten tým bude vypadat. Proč vize? Protože budování týmu nekončí přijímacím pohovorem. Nekončí ani po zkušební době, či úplném zapracování. Ono totiž nekončí nikdy --- je to cesta, která je svým vlastním cílem. A proto potřebujete [úběžník](//cs.wikipedia.org/wiki/Perspektiva), ke kterému budete po celou dobu existence týmu směřovat.

{{< figure class="floatright" width="320"
    src="/2018/02/Garden-perspektive.jpg"
>}}

Termín z perspektivy jsem si nevybral náhodou:

* Vize i úběžník jsou virtuální entity, kterých v realitě nejde dosáhnout.
* Vize i úběžník jsou "lehce pohyblivé cíle" --- záleží na perspektivě, úhlu pohledu, času a pozorovateli.
* O skutečném vývoji/postupu vypovídá historie --- to když se ohlédnete zpět a uvidíte, jakou cestu jste urazili.
* Existují nástroje, které vám ve složitem terénu pomůžou udržet směr.

Smutnou pravdou je, že většina teamleaderů a manažerů --- pokud už si dali tu práci a takovou týmovou vizi si definovali --- vám nebude schopna tuto vizi popsat. Samozřejmě, takový ty soft-skill/HR/people-management řeči vám řekne každý. Ale to jsou jen [newspeak](//en.wikipedia.org/wiki/Newspeak) keci.

Jestli mě [Kanban](/tags/kanban/) něčemu naučil, tak _"make policies explicit"_. Pro vizi to platí taky. Nemusíte ji s nikým sdílet. Ale sepište si to. Vyslovte to nahlas. Není to žádná magie, ale funguje to zázračně. Jinak budete po léta bloudit v pustinách. Vy i celý tým.

## Přijímací pohovor

O (technických) přijímacích pohovorech jsem tady na blogu [psal po léta](/tags/interview/). Ačkoliv to jsou, nijak překvapivě, ty nejčtenější články, jejich vliv byl minimální. Když jsem se po 5 letech vrátil na pracovní trh, byla to [velmi tristní zkušenost](/2017/09/smutna-zprava-o-stavu-it-trhu/).

Tak jako jsem v minulé sekci zdůrazňoval důležitost vize, pro pohovory platí to samé --- nepokažte si to hned na začátku. Může vám v tom pomoci pár pravidel:

* **Buďte konzistentní**. Všichni lidi v týmu by měli projít stejným procesem. (Ale udělejte výjimku, když to dává smysl.)
* **Udělejte technické kolo co nejbližší skutečné práci, kterou děláte.** Tohle většině českých firem ještě nedošlo. Jste SW inženýři, dělejte věci racionálně a nedržte se bludných mýtů.
* **Nedělejte síto příliš úzké a husté.** Nemáte ani představu, jak je život rozmanitý. Pravda, čím budete starší, tím méně věcí vás bude překvapovat. Ale stejně vždycky přijde něco, co jste nečekali (pokud jste před tím nezavřeli oči). Výjimkou z tohoto pravidla je, pokud jste si ve vizi definovali, že chcete postavit tým ze samých klonů tzv. ideálního kandidáta (mmch. to chce většina pražských firem).
* **Směřujte k vizi.** Ptejte se: bude tenhle kandidát za 1/2 roku, za rok sedět do naší (týmové) vize?

Neexistuje ideální pohovor. Pokud to s pohovorem myslíte vážně a neberete kohokoli z ulice, stojí za to se inspirovat u zkušených (článků je plný internet), něco seriózního si o tom přečíst a pracovat na tom a pohovor zlepšovat. Za sebe doporučuji třeba [Esther Derby](//twitter.com/estherderby), [Johanna Rothman](//twitter.com/johannarothman), [Gerald Weinberg](//twitter.com/jerryweinberg), [Andy Lester](//twitter.com/petdance), či [Michael Lopp](//twitter.com/rands) (Rands in Repose).

Já jsem svůj způsob pohovorování vybrušoval pět let. Je to proces, který odpovídá mé vizi, takže pro někoho může být nevhodný. Některé věci jsou dokonce nepřenosné.

Pokud nevíte kde začít, zkuste 2-a-půl kolové schéma (obsah už si doplníte sami):

* **Phone screen.** Krátký a jednoduchý technický filtr --- má smysl se vidět tváří v tvář a strávit spolu víc času? (Pro inspiraci, [jak jsem to dělal já](/2015/06/jak-delam-java-pohovor-iii-phone-screen/).)
* **Technické kolo.** Viz výše. A opět můžete nakouknout [ke mně do kuchyně](/2017/03/jak-delam-java-pohovor-iv-java-workshop/).
* **Netechnické kolo.** Podívejte se na kandidáta i jinak, než přes technologie --- není to robot. Pokud si později nebudete sedět, bude to spíš kvůli osobním, netechnickým kvalitám. Ale i tady platí _"nedělejte síto příliš husté"_.

## Neúprosná pohovorová statistika

Postavit dobrý tým je těžká makačka. Je to plnohodnotný (IT) projekt. Věc, kterou si členové vašeho týmu nejspíš nikdy neuvědomí (a spousta dalších lidí okolo), je, kolik je za tím práce, dát dohromady 5 až 10 lidí.

Po léta si vedu pohovorovou statistiku. Tady je jeden konkrétní, řekl bych průměrný rok:

* **Phone screen:** celkově **46** pohovorů, z toho **33** postoupilo do dalšího kola (úspěšnost 72 %).
* **Technické interview:** celkově **29** pohovorů, z toho **20** postoupilo do dalšího kola (úspěšnost 69 %).
* **Personální intervew:** pro toto kolo nemám data (v grafu níže extrapoluji).
* _**Do týmu nastoupilo 6 lidí.**_ Trvalo to rok.

{{< figure caption="Statistika pohovorů"
    src="/2018/02/Interview-statistics.png"
>}}

Ke statistice pár poznámek:

* To že se lidi dostali na _phone screen_, znamená, že už byli minimálně profiltrovaný přes CV, buď interním HR, nebo agenturou. Tzn. že většinou už absolvovali headhunterský telefon. A samozřejmě prošli přes moje CV review.
* Reálně do dalšího kola nastoupí méně kandidátů, než kolik jich bylo úspěšných (z různých důvodů nepokračují dál).
* Interview byla během roku konzistentně rozvrstvena. Zhruba to znamená udělat za měsíc 4 phone screeny a 2-3 technická interview. A jednou za dva měsíce zapracovat nového člověka.
* Počítejte, že ze všech lidí, kteří vám projdou pohovory, vám do týmu nastoupí cca 10 % z nich. I míň.
* Povšimněte si poměrně vysoké úspěšnosti kandidátů v jednotlivých kolech. Domnívám se, že je to dáno tím, že jsem měl celý proces pod kontrolou a zůčastnil jsem se _všech_ pohovorů (vyjma _personálních interview_). Pokud vám dělají interview různí lidé a nekonzistentně, bude neúspěšnost vyšší.

## Zapracování

Fajn. Člověk vám nastoupil a dostal počítač a stravenky. Zpravidla bude nadšený a iniciativní. Neprošvihněte to! Samozřejmě, musí se naučit všechny ty procesy nástroje, projekty a produkty. Zaručeně tím ztráví celou zkušební dobu a velmi pravděpodobně celý úvodní semestr. Pokud je vaše doména složitá, může zapracování trvat i dva roky (true story).

Tohle období ubíhá tak nějak samo od sebe, samospádem. Možná to bude dost překotné. Ale nepodceňte to --- je to první čas, kdy budete daného člověka opravdu poznávat. Jak pracuje, jak žije, jak se chová, jak na něj reagují ostatní (členové týmu)?

Je to jedinečné období, kdy můžete zasadit semínko kultury a pomáhat mu zakořenit a růst. Pomůžou vám v tom nástroje jako [1:1](/2017/10/11-nejdulezitejsi-nastroj-team-leadera/) a ochota naslouchat a řešit problémy.

## Týmová kultura

Jakmile máte v týmu prvního člověka, je týmová kultura to nejdůležitější, o co byste měli jako team leadeři pečovat. Je otázka, jaká je konstelace a kolik na to budete mít reálně času. Ale pokud má tým zdravě funovat v následujících letech a překoná různé (personální, pracovní a další) krize, bude to díky živoucí kultuře. Pokud týmová kultura churaví, nebo dokonce umře (taky jsem to zažil), bude to jen o tom, jak přinést domů pytel peněz na kus žvance.

Každá kultura funguje na tom, že se lidé potkávají. Čím širší komunikační kanál, tím lepší (zdravější). Nejlépe osobně, když to nejde tak aspoň video, až pak telefon a úplně na konci instant messaging. Pokud si píšete už jen emailem, tak už vlastně nekomunikujete.

Mějte pravidelné týmové schůzky. Potkávejte se nad problémy, u jídla a (v rozumné míře) mimo práci.

Každá kultura, která se rozvíjí, má nějaké (samo)korektivní mechanizmy, historii a způsob zpracování zpětné vazby. V případě SW inženýrství máme skvělý a ozkoušený nástroj: týmové retrospektivy. Retrospektivy nemusí být jen o projektech a iteracích. Můžou mít zaměření i na tým a jeho kulturu.

{{< figure caption="Retrospektiva: team radar" width="600" src="/2018/02/Team-radar.jpg" >}}

Mít dobrou týmovou kulturu není nic zas až tak těžkého --- chce to jen pár rutinních a pravidelných úkonů. Jako když pečujete o zahrádku. Když to zanedbáte, začne vám zvolna zarůstat plevelem. V určitý moment se může stát, že se vám zahrádka vylidní. Přeju, ať se to nestane.

## Související články

* [Jak dělám Java pohovor III: phone screen](/2015/06/jak-delam-java-pohovor-iii-phone-screen/)
* [Jak dělám Java pohovor IV: Java workshop](/2017/03/jak-delam-java-pohovor-iv-java-workshop/)
* [Smutná zpráva o stavu IT trhu](/2017/09/smutna-zprava-o-stavu-it-trhu/)
* [Geek, který zapadne](/2013/04/geek-ktery-zapadne/)
* [1:1, nejdůležitější nástroj team leadera](/2017/10/11-nejdulezitejsi-nastroj-team-leadera/)
