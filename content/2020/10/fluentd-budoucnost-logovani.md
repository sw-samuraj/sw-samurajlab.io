+++
title = "Fluentd, budoucnost logování (část 1)"
date = 2020-10-02T13:31:09+02:00
description = """Fluentd je modernější alternativou k etablovanému řešení
    pro sbírání distribuovaných logů. The ELK is dead, long live
    the Fluentd! První část."""
tags = ["cloud", "logging"]
hero_image = "hero.jpg"
thumbnail = "2020/10/Logs.jpg"
+++

{{< figure src="/2020/10/Logs.jpg" link="/2020/10/Logs.jpg" >}}

Dlouhá léta byl synonymem pro sbírání distribuovaných logů, tzv.
[ELK stack](//www.elastic.co/what-is/elk-stack),
(dnes nazývaný _Elastic Stack_), kdy _ELK_ je akronym
pro [Elasticsearch](//www.elastic.co/elasticsearch/),
[Logstash](//www.elastic.co/logstash), [Kibana](//www.elastic.co/kibana).
A je to framework věru letitý (což není samo o sobě na škodu). Bohužel,
letitost sebou nese v IT jednu nepříjemnou věc --- ukotvenost v čase
a určitém kontextu.

Tím kontextem je v případě _ELKu_ tehdy ještě převažující důraz
na server-based architekturu, která se teprve zvolna začínala
přesouvat do cloudu. Ta doba už je pryč... cloudová řešení jsou plně
etablovaná a dnešní doba si žádá jiný přístup --- více distribuovaný,
více granulární, více autonomní.

To vše jsem si uvědomil, když jsme v práci začali pracovat na novém
produktu (nativní cloudová (infrastrukturní) služba) a já jsem dostal
na starost část logování.
Ukázalo se, že jak proprietární logování, tak případné využití _ELKu_
je pro můj případ nedostatečné až nepoužitelné. A tak jsem se ponořil
do světa [Fluentd](//www.fluentd.org/).

## Trocha historie

Když jsem začínal programovat, bylo to jednoduché --- byla jedna aplikace,
která logovala na jedno místo (teda, ehm, pokud vůbec logovala) a všechno
bylo krásné, tráva zelenější atd.

Pak se začaly používat clustery. Pořád ještě relativně jednoduché: hlavní
bylo nějak sehnat všechny logy po všech nodech a nějakým způsobem je pročesat.
Starý dobrý [grep](//en.wikipedia.org/wiki/Grep) 🤗

Už u clusterovaných řešení býval často problém, dohledat tok určité
business logiky (některým vývojářům můžete opakovaně vtloukat do hlavy
důležitost [correlation ID](//www.enterpriseintegrationpatterns.com/patterns/messaging/CorrelationIdentifier.html),
ale marně) a s rozvojem (distribuovaného) messagingu,
[SOAP](//en.wikipedia.org/wiki/SOAP) web services a následně
[SOA](//en.wikipedia.org/wiki/Service-oriented_architecture), se to stávalo
stále více komplikovanější a pracnější.

Tady přichází ke slovu _ELK_ a na dlouhá léta nastaví nepsaný "industrial
standard". Pokud vás _ELK_ minul, tady je krátké shrnutí:

1. _Logstash_ posbírá lokálně data v [různých formátech](//www.elastic.co/guide/en/logstash/current/input-plugins.html),
   podle potřeby je volitelně transformuje a publikuje na cílovou
   platformu, kterou je většinou _Elasticsearch_, nicméně podobně jako u vstupů,
   existuje spousta [výstupních pluginů](//www.elastic.co/guide/en/logstash/current/output-plugins.html).
1. _Elasticsearch_ data oindexuje a umožní je filtrovat, či analyzovat pomocí
   dotazovacího jazyka.
1. _Kibana_ pak výstupy z _Elasticsearche_ graficky zobrazí.

Ale všechno jednou končí a cloud už klepe na dveře. A ne jenom cloud.
Ale i [Docker](//www.docker.com/) a [Kubernetes](//kubernetes.io/). 🤯
Je potřeba to začít dělat trochu jinak.

## Je _Fluentd_ další generací logování?

{{< figure src="/2020/10/Scales.jpg" link="/2020/10/Scales.jpg" >}}

Krátká odpověď je ano. Avšak musím upřesnit jednu věc --- výše napsané
může svádět k myšlence, že _Fluentd_ je náhradou _ELKu_. Není to tak.
Není to úplně přesné, ale _Fluentd_ je spíše náhradou _Logstashe_ (tedy
jen části _ELKu_) a například _Elasticsearch_ & _Kibana_ můžou být použity
společně s _Fluentd_ v jednom řešení.

V tomhle článku nechci zmíněné technologie srovnávat (stačí
[zagooglovat](//www.google.com/search?q=logstash+fluentd+comparison)),
ale abych se z toho úplně nevyvlíkl, uvedu dva důvody, proč je _Fluentd_
o kus dál v budoucnosti:

* _Docker_ má vestavěný [logging driver](//docs.docker.com/config/containers/logging/fluentd/)
  pro _Fluentd_.
* _Fluentd_, stejně jako _Kubernetes_ jsou graduované projekty [CNCF](https://www.cncf.io/)
  (Cloud Native Computing Foundation).

Samozřejmě, není žádná zásadní překážka, proč nepoužít _ELK_ v _Kubernetes_.
Ale _Fluentd_ je prostě lepší "domain match" a netáhne za sebou historická
(architekturní a jiná) rezidua.

## Co je to _Fluentd_?

> Fluentd is an open source data collector, which lets you unify the data
  collection and consumption for a better use and understanding of data.

U základů _Fluentd_ je představa, že logy jsou primárně pro stroje. Logovací
infrastruktury minulosti (viz historické okénko výše) byly designovány pro lidi.
To v dnešní době geometricky rostoucího škálování a granularity
[serverless](//en.wikipedia.org/wiki/Serverless_computing) řešení není možné
--- lidé (vývojáři, DevOps atd.) vidí a analyzují pouze mikro-zlomek logů
a metrik.

Jestliže máme data určená přednostně pro stroje, nabízí se několik osvědčených
řešení. _Fluentd_ si vybralo dnes všudypřítomný JSON, hlavně z toho důvodu,
že i když JSON hůře performuje, nežli třeba binární formáty, tak na druhou
stranu je JSON buď nativně, nebo jednoduše podporován na většině platforem
z nichž _Fluentd_ logy sbírá, nebo kam je publikuje. Navíc, JSON je dnes
už standardním výstupem většiny zralých logovacích knihoven.

{{< figure src="/2020/10/Fluentd.png" link="/2020/10/Fluentd.png"
    caption="Zdroj: https://www.fluentd.org/" >}}

Pokud odhlédneme od datového formátu a podíváme se na zbytek _Fluentd_
(pominu pokročilejší věci jako reliability apod.), jedná se vlastně
o jednoduchý [pipelines pattern](//en.wikipedia.org/wiki/Pipeline_%28software%29),
seskládaný ze spousty pluginů. Ještě jsem to neřekl? _Fluentd_ je
[plugable](//en.wikipedia.org/wiki/Plug-in_(computing)). 🤓

1. Data sbírají [input pluginy](//docs.fluentd.org/input), což jsou vlastně
   listenery, které naslouchají pro definované _log events_, nové logovací
   záznamy.
1. Než data opustí _Fluentd_, můžou projít smečkou procesních pluginů:
   1. [parser pluginy](//docs.fluentd.org/parser) (JSON, regex, ad.),
   1. [filter pluginy](//docs.fluentd.org/filter) (grep 🤗, parser, ad.),
   1. [buffer pluginy](//docs.fluentd.org/buffer) (memory, file),
   1. a ještě celou řadou dalších pluginů.
1. Přechroustaná data se odešlou některým z [output pluginů](//docs.fluentd.org/input).

Příklad několika jednoduchých generických pipeline:

* `Input -> Output` --- nejjednoduší pipeline, data se načtou a odešlou,
  bez nějakého procesování.
* `Input -> Filter -> Output` --- druhá nejjednodušší pipeline, data se
  mezi načtením a odesláním filtrují.
* `Input -> Filter -> Parser -> Formatter -> Output` --- co k tomu dodat? 🤷‍♂️

Dobře, dobře, starý bobře, "plugable pipelines". 🙄 Co nějaká [killer
feature](//en.wikipedia.org/wiki/Killer_feature)? Ano, ano, _Fluentd_
má ještě nějaké to eso v rukávu --- data, která prochází skrz _Fluentd_
tečou po tzv. [routách](https://docs.fluentd.org/configuration/routing-examples),
kdy lze pomocí labelů a tagů usměrňovat dráhu dat.

Tato feature sice může vypadat nenápadně, ve skutečnosti ale umožňuje, aby
se toky dat ve _Fluentd_ rozdělovaly a spojovaly a taktéž zajišťuje, že mezi
všemi pluginy může být vztah `M:N`. Jinými slovy, můžu sbírat data z více
zdrojů a publikovat je do více cílů, ale zároveň přesně specifikovat, který
data kam půjdou a co se s nima v průběhy cesty stane. A to vše velmi přehledně
a jednoduše. 👍

Abychom si routování trochu představili, podíváme se na (z prstu vycucaný)
příklad. Máme dvě aplikace, jedna loguje klasicky do souboru, druhá posílá
logované události přes HTTP. Obojí chceme publikovat do _Elasticsearche_,
ale události přes HTTP chceme ještě dále zpracovávat a tak je pošleme
navíc do [Kafky](//kafka.apache.org/).

{{< figure src="/2020/10/Fluentd-routing.png"
    link="/2020/10/Fluentd-routing.png"
    caption="Příklad routování ve _Fluentd_" >}}

## Pokračování příště

Zase jednou se mi článek rozrostl víc, než jsem předpokládal, takže ho
rozdělím na dva díly. Příště bychom tedy zakončili dvěma tématy:

* Jednoduchý příklad emulující _ELK_
* Reálný use case

## Související články

* [Fluentd, lehký úvod (část 2)](/2020/12/fluentd-lehky-uvod/)
* [Fluentd, case study (část 3)](/2021/03/fluentd-case-study/)
* [Nešvary logování](/2017/12/nesvary-logovani/)
