+++
title = "Hledám do našeho týmu backend vývojáře"
date = 2020-01-10T18:21:34+01:00
description = """Opět jsem se rozhodl "jít tomu štěstíčku trochu naproti"
    a publikovat zde další pracovní inzerát. Protože hledám nové lidi k nám
    do týmu a rád bych našel někoho, kdo bude kompatibilní s mým přístupem
    k softwarovému inženýrství"""
tags = ["interview", "java", "golang", "python", "kotlin", "cloud", "big-data", "teamleading"]
hero_image = "hero.jpg"
+++

{{< figure src="/2020/01/Color-pencils.jpg" link="/2020/01/Color-pencils.jpg" >}}

Když jsem před 7-8 lety publikoval článek [Hledám do svého týmu Java vývojáře](/2013/07/hledam-do-sveho-tymu-java-vyvojare/)
mělo to dvě konsekvence. Jednak mě začali zvát na kafe různí head-hunteři,
😒 aby ode mne nasáli trochu toho know-how (které jim --- přirozeně 😏
--- schází).

Podstatnější ale bylo, že jsem tímto způsobem našel do svého týmu dva ze čtyř
nejlepších vývojářů, kteří mým tehdejším týmem prošli. 👍

## Proč o tom píšu?

Sice takovéhle články běžně nepíšu, ale tehdejší zkušenost byla natolik
pozitivní, že bych to rád zkusil znovu. Zásadní změna je, že tehdy jsem
--- jako team leader --- hledal vývojáře _do svého_ týmu, týmu, který jsem
nově stavěl. Nyní --- když už nejsem leader, ale jen obyčejný
programátor --- hledám vývojáře _do našeho_ týmu (který vede někdo jiný).
Čili bude to kolega na stejné úrovni se mnou. 👉 👈

Takovým skrytým, nicméně pro mne asi nejdůležitějším cílem tohoto článku,
je najít takového kolegu, který by mi pomohl posílit určité "kulturní
trendy" v týmu. Poněvadž, i když mám rád diverzitu a různorodost, je dobré,
když více lidí "hledí stejným směrem".

## Disclaimer

_Následující text je čistě mým subjektivním náhledem, potencionálně chybným,
který se nemusí slučovat s přítupem mého manažera, nebo oficiálními stanovisky
společnosti, ve které pracuji._

## Koho hledáme

> You don’t hire for skills, you hire for attitude. You can always teach
  skills. ~ Simon Sinek

Už dlohodobě se řídím uvedenou zásadou a na pohovorech jsem se vždycky
snažil zjistit, jaká je potencionální trajektorie daného kandidáta. Takže
bez ohledu, co říká oficiální inzerát (který tady nebudu linkovat), či co
by vám řekl můj šéf; já bych to formuloval takto:

Hledáme _backend vývojáře_ (kterému není cizí DevOps, nebo QA).
Technologie není podstatná --- důležité je rozumnět základním principům,
mít technologický rozhled a mít chuť se neustále učit nové věci: jazyky,
standardy, technologie, domény, atd. Zkrátka: užitečné věci.

Nehledáme juniora, ale jinak seniorita není podstatná.

{{< figure src="/2020/01/Thinker.jpg" link="/2020/01/Thinker.jpg" >}}

## Doména, ve které se pohybujeme

Náš team se jmenuje něco jako _Big Data Cloud Service_, takže bysme měli
dělat něco s _Big Data_. Takový ty věci jako [Hadoop](//hadoop.apache.org/),
[Spark](//spark.apache.org/) apod. A mělo by to běžet v cloudu.

Pravdou je, že za ty dva roky, co jsem v týmu, jsem si na _Big Data_ ani
jednou nesáhl a vlastně celou dobu pracuju na cloudové infrastruktuře. To
si nestěžuju, to jen, že jména mohou být zavádějící. 🤭

Momentálně máme čerstvě na produkci jeden produkt (nebudu ho zde jmenovat),
který nabízí "Big Data as a Service". A pracujeme na druhém (zatím ve fázi
[R&D](//en.wikipedia.org/wiki/Research_and_development)), který bude řešit
"distribuované dotazování mezi databází a různými Big Data úložišti (jako
je třeba [Object Storage](//en.wikipedia.org/wiki/Object_storage))".

A za rok to může být něco úplně jiného. 😈

## Charakter práce

Jsme technologická firma a děláme _produkty_. Neděláme _projekty_. Je docela
(hořce) úsměvný, že svým stávajícím kolegům jsem neuměl pořádně vysvětlit,
co je to _projekt_ a naopak svým bývalým kolegům, co je to _produkt_. 🤦‍♂️
(Možná k tomu jednou napíšu článek.)

### Děláme produkt

To, že děláme produkt, znamená hlavně, že práce na něm nekončí, dokud neskončí
jeho životnost. Oproti mé předchozí projektové zkušenosti, deadliny jsou velmi
měkké a tvárné. Cesta bývá často klikatá a málo ohraničená. Začíná se z vize a
dospěje se k nějaké podobě, která se rozvíjí a udržuje.

Ne vždycky se produkt dostane na produkci --- někdy se ukáže, že z různých
důvodů není životaschopný. Co mi tak říkal můj šéf (jestli si to správně
pamatuju), na produkci se dostanou 2 produkty z 10, který tým začne (to je
lepší skóre, než mají start-upy).

### Děláme R&D

Na začátku každého potencionálního produktu (a někdy i problému) je research.
To je chvíle, kdy si můžeme osahat mnoho nových technologií. Tahle fáze může
trvat pár týdnů, někdy i měsíců. Někdy se táhne průběžně celou implementační
fází.

Občas je naopak potřeba prozkoumat určitou doménu. Často je pak lepší, zůstat
u osvědčených technologií, aby aspoň něco fungovalo. 😈

Někdy se tahle výzkumná práce zahodí, někdy rozkvete v nový produkt. Každopádně,
nejcennější na téhle fázi je [knowledge
gain](//translate.google.com/?op=translate&sl=en&tl=cs&text=knowledge%20gain).

### Děláme DevOps

Jsme velká firma a zákazníci jsou od nás velmi daleko. Přesto (nebo právě
proto) se aktivně snažíme vědět, co se děje na produkci a preemptivně to
řešit. Sbíráme metriky, zachytáváme alerty, každý v týmu by měl být schopný
evaluovat incident.

Tohle je věc, na které ještě musíme s týmem zapracovat a také, naneštěstí,
narážíme na rozdílnou kulturu jiných týmů. Ale myslím, že jsme na dobré
cestě.

{{< figure src="/2020/01/Chemist.jpg" link="/2020/01/Chemist.jpg" >}}

## Technologie, které používáme

Jsme "unix shop" --- lidi dělají buď na linuxu, nebo na Macu. Technologicky
je naše prostředí nejsilněji prorostlé _Javou_, ale může to být variabilní
--- občas se dají strávit týdny a měsíce na _Golangu_, či _Pythonu_, běžně
se píší _shellové skripty_. Minulý rok jsem si to neuhájil, ale teď si aktuálně
píšu svoji mikro-servisu v _Kotlinu_.

Primární platformou jsou pro nás buď:

* virtuální mašiny v [computu](//en.wikipedia.org/wiki/Computing#Cloud_Computing),
* _Docker_ kontejnery,
* _Kubernetes_.

Architektonicky většinou píšeme "něco na způsob micro-servis" (což se nám ale
úplně nedaří --- tady je ještě prostor pro zlepšení). Tzn. že jsme silně
REST-oriented (jako ostatně všechno v cloudu).

Stav se uchovává v nějaké "persistent storage". Pokud si vzpomínám,
za poslední dva roky jsem nesáhnul na relační databázi: vždy šlo
o nějaký [key-value store](//en.wikipedia.org/wiki/Key-value_database).

Poměrně dobře se nám daří implementovat _IaC_ ([Infrastructure as
Code](//en.wikipedia.org/wiki/Infrastructure_as_code)). Jsme v tom tak
dobří, že o tom máme přednášku na MatFyzu. 💪 😂

## Jak si organizujeme práci

Nemáme formální proces a neděláme agile. Já bych to zlehka přirovnal
k takovému "živelnému, chaotickému kanbanu" (ale _Kanban_ neděláme). Máme
denní mítinky a většinou jednou týdně nějaký synchronizační mítink
s remotními týmy.

Jak říká můj šéf, _"jsme takoví pankáči v korporaci"_.

Snažíme se dělat [continuous delivery](//en.wikipedia.org/wiki/Continuous_delivery)
(to se nám většinou daří) a časté, pravidelné releasy (tady to hodně závisí
a liší se na/v externích závislostech).

## Team

Jsme malý tým, 6 vývojářů. Zhruba půl na půl, co se týká seniority, juniory
nemáme. Náš šéf je silný _technical leader_ (asi jako jsem ~~za mlada~~ dříve
býval já 😂).

Lidi v týmu jsou výrazné osobnosti a každý z nich je autonomní. To je hodně
důležité --- každý musí být (v rámci své seniority) samostaný a naturelem
_problem solver_.

Nikoho za ruku nevodíme, ale každý rád pomůže. _Nemáme macho kulturu._

{{< figure src="/2020/01/Wood-figures.jpg" link="/2020/01/Wood-figures.jpg" >}}

## Finance

K financím vám nic neřeknu --- od té doby, co jsem přestal dělat _technical
recruitment_, ztrácím přehled, jaká je situace na trhu. Znám jen svůj plat
(se kterým jsem spokojený) a ten vám neřeknu.

Takže budete mít, co si domluvíte s mým šéfem. 😉 Každopádně, nemusíte se
bát říct si o "běžný pražský vývojářský plat". 💰

## Společnost

Tak, už to nemůžu déle skrývat: společnost, kde pracuju se jmenuje...
_Oracle_.

Ať už jste o _Oraclu_ slyšeli cokoli, je to nejspíš pravda. 🤭 Pravda taky
je, že ještě nikdy jsem nepracoval ve firmě, kde by byly týmy/oddělení/organizace
s tak navzájem rozdílnou kulturou.

Každopádně, v našem týmu se dá svobodně dýchat. Možná je to způsobeno i tím,
že polovina týmu (včetně séfa) pochází ze [Sun
Microsystems](//en.wikipedia.org/wiki/Sun_Microsystems). Myslím, že to má
svou váhu.

## Tak já bych to teda zkusil... 🤔

### První krok(y)

Jestli byste to u nás v týmu chtěli zkusit, nebo třeba jen získat víc informací,
napište mi na _Twitter_ ([@sw_samuraj](//twitter.com/sw_samuraj)), nebo
na [LinkedIn](//www.linkedin.com/in/vitkotacka/). Zajdeme na kafe a popovídáme
si. ☕

### Jak to bude probíhat?

Pokud byste do toho opravdu chtěli jít, tak následně:

1. Zavoláte si s mým šéfem a popovídáte si.
1. Vypracujete na GitHubu/Bitbucketu zadaný projekt.
1. Sejdete se on-site se členy týmu.

A pokud všechno bude OK a vzájemně si všichni sedneme, budete sedět na židli
vedle mne. 😂 Doslova.

## Související články

* [Hledám do svého týmu Java vývojáře](/2013/07/hledam-do-sveho-tymu-java-vyvojare/)
