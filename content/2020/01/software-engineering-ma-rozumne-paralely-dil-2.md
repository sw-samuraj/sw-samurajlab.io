+++
title = "Software Engineering, má rozumné paralely? Díl 2, hudba"
date = 2020-01-31T12:11:20+01:00
description = """Software engineering je mladé odvětví, které není ještě
    úplně pochopeno a popsáno. Proto se někdy vyplatí hledat paralely
    v jiných oborech, které lépe osvětlí daný problém, či myšlenku.
    Jedna taková paralela pochází z říše hudby."""
tags = ["sw-engineering"]
blogimport = true 
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

> Konstantou [Software Engineeringu](//en.wikipedia.org/wiki/Software\_engineering)
(SE)  je jeho dynamika. Snad právě proto hledáme paralely a podobenství v  jiných
oblastech lidské činnosti --- abychom lépe porozuměli jeho  zákonitostem, uchopili
jeho abstraktní nehmotnost a někdy 😉 i jen  našli pevnou půdu pod nohoma.
~ SoftWare Samuraj

{{< figure src="/2020/01/Rachmaninov.png"
    caption="Cadenza z 3. klavírního koncertu Sergeje Rachmaninova" >}}

To jsme takhle jednou seděli s [@banterCZ](//twitter.com/banterCZ) u oběda a
filozofovali o analogiích k softwarovému inženýrství. A hned jsem o tom chtěl
napsat článek. Protože kromě těch obligátních --- stavebnictví a auto-průmysl
--- mě napadly dvě další, které nejsou tak obvyklé, ale velmi, velmi trefné.

Začal jsem psát článek, a abych to vzal z gruntu, sepsal jsem prvně ty "standardní"
paralely. Pak mi rok, dva trvalo, než jsem článek [Software Engineering, má rozumné
paralely?](/2016/10/software-engineering-ma-rozumne-paralely/)
publikoval. A další rok, dva uběhly, než jsem se dostal k té původní myšlence.

Ale nebojte se --- nejsem jenom fantastický programátor, ale i originální filozof
a tak jsou mé myšlenky vskutku nadčasové. No, ony to nebudou zas až tak moje myšlenky.
Ale budu to já, kdo to sepíše a koho si budou příští generace pamatovat. A možná
se dostanu i do (programátorských) čítanek. 😈

## Hudba sfér

Musím se přiznat, že softwarovým inženýrem --- tak jak ho chápu dnes --- jsem se stal
až někdy kolem 28. roku. Do té doby, jsem se sice "motal kolem počítačů", ale byl to
pro mne způsob, jak si... vydělat na kytary. Ano, muzika byla moje největší vášeň.

Takže tak jako je teď posledních 15+ let mojí vášní softwarové inženýrství, tak těch
předchozích 15+ to byla hudba. A přirozeně, pokud je člověk v určité doméně delší
dobu (a nerezignoval), začne vidět určité zákonitosti. U mne je to navíc umocněno
tím, že jako analyticko-intuitivní-itrovert mám potřebu vyhledávat ve vesmírném
chaosu archetypy a patterny.

## Terra incognita

> V softwarovém inženýrství, stejně jako v hudbě, se vždy můžeme vydat
  do neprozkoumaných oblastí. ~ SoftWare Samuraj

{{< figure src="/2020/01/Landscape.jpg" link="/2020/01/Landscape.jpg" >}}

Jedna z věcí, které mě na muzice i na softwarovém inženýrství nesmírně přitahují,
že jsou to neohraničené virtuální oblasti, potencionálně nekonečné --- můžete se
jim věnovat celý život a pořád budete nacházet něco nového, pořád vám to může
dávat nezkalenou radost, pořád budete mít možnost se zlepšovat a pořád můžete
objevovat netušené oblasti, které vás budou uvádět v úžas.

Samozřejmě, platí to, pokud máte srdce dobrodruha a objevitele a v žilách vám
proudí neklidná krev. Jinými slovy, pokud nevystrčíte nos z vlastního dvorečku,
tak vám výše zmíněné věci nejenom uniknou, ale dost možná budete skeptičtí, že
vůbec existují. A dvoreček, to je všední realita, kde se zajímavé věci nedějí
(teda pokud někdo není vyznavačem poezie všedního dne a estetiky banálna 😉).

## Rozmanitost stylů

> V softwarovém inženýrství, stejně jako v hudbě, jsou myriády stylů,
  které můžeme adoptovat a dále míchat. ~ SoftWare Samuraj

{{< figure src="/2020/01/Les-Paul.jpg" link="/2020/01/Les-Paul.jpg" >}}

[Mozartovo requiem](//en.wikipedia.org/wiki/Requiem_(Mozart)) _d moll_ jsem zpíval
mockrát --- s [Jaroslavem Krčkem](//cs.wikipedia.org/wiki/Jaroslav_Kr%C4%8Dek),
s [Markem Štrynclem](//www.marekstryncl.cz/) a jednou 😊 dokonce v televizi.
Taky jsem zpíval Dvořákovo [Stabat Mater](//cs.wikipedia.org/wiki/Stabat_Mater_(Dvo%C5%99%C3%A1k))
a Stravinského [Oedipus rex](//en.wikipedia.org/wiki/Oedipus_rex_(opera)).

Kromě toho, že jsem zpíval v sakrální hudbu ve sboru, hrál jsem sólovou
a rytmickou kytaru v [death metalové](//en.wikipedia.org/wiki/Death_metal)
kapele. Hrál jsem na baskytaru a zpíval hrdelním hlasem v undergroundové
kapele. Hrál jsem s kotlíkama (pardon, trempama) na kytaru u ohýnků. (Ah! Kde
jsou ty časy, kdy jsem znával zpaměti 300 písní... 😢) Hrával jsem rytmickou
kytaru v 25členném jazzovém big bandu.

Rozmanitost hudebních stylů je nesmírná. A stejně rozmanitý je i svět softwarového
inženýrství. Už jenom těch programovacích jazyků, ze kterých si člověk může
vybrat. Jak z předešlého výčtu vyplývá, býval jsem slušný (univerzální)
kytarista (lokálního významu). Ale dokážu zahrát v podstatě na jakýkoli
nástroj, pokud rozumím, jak se na něm vytváří tón.

Podobně u programování, mám jeden dva (tři) hlavní jazyky, ale pak (příležitostně,
pro radost, ponaučení) píšu i v lecčems dalším. Profesionál samozřejmě
pozná, že jsem amatér, ale to mi radost nijak nekazí.

A když už má člověk vybraný nástroj --- dá se na něj hrát (programovat)
různými styly. Funkcionálně, objektově, procedurálně. Intuitivně, analyticky...

A pak je tedy nejenom programování, ale i _operations_ & _administrations_,
_DevOps_, různé _intangible_ věci, jako _CI/CD_, _Governance_ atd. atp.

## Hudba má vnitřní řád

> V softwarovém inženýrství, stejně jako v hudbě, existuje vnitřní řád.
  Jen nemnohým je dáno jej pochopit a prožít. ~ SoftWare Samuraj

{{< figure src="/2020/01/Stones.jpg" link="/2020/01/Stones.jpg" >}}

Na muzice je krásný, že ji může dělat --- pokud chce --- kdokoli (prosím,
nechte si ty řeči o talentu, či chybějícím hudebním sluchu). A většina
takových lidí (stejně jako programátorů) přistupuje k tématu intuitivně
bez nutného porozumnění esenciálních principů a věci se naučí pouhým
napodobováním a opakováním. Ano, tak prosté to je.

Teorie hudby, stejně jako různé počítačové teoretické záležitosti, se dají
studovat. A pár vybraných jedinců se tou teorií může i živit. Ale většina
z nás žije na světě, aniž by takové hluboké teoretické znalosti měla,
či potřebovala.

Kromě toho, že menšina má solidní teoretické znalosti, existuje ještě menší
skupina, která se dobrala jakéhosi vnitřního řádu (v hudbě i v inženýrství).
Je to stejný rozdíl, jako o něčem vědět vs. "opravdu tomu rozumnět", mít to
prožitý a internovaný.

Samozřejmě, spousta lidí si (mylně) myslí, že tohoto stavu již dosáhla. 🤭

## Od jednoduchosti ke komplexnosti a zpět

> V softwarovém inženýrství, stejně jako v hudbě, lze postupovat od
  atomických prvků ke složitým, komplexním, či abstraktním konceptům.
  A obráceně. ~ SoftWare Samuraj

{{< figure src="/2020/01/Snails.jpg" link="/2020/01/Snails.jpg" >}}

Od cihel ke katedrále, od not k symfonii, od funkcí k distribuovanému
systému. Je to vlastně to, co v softwarovém vývoji dávno známe --- dva
protichůdné systémy: _top-down_ vs. _bottom-up_.

Pokud budu studovat (třeba) nový vícehlasý chorál (projekt na GitHubu), podívám
se prvně na celkovou strukturu (jednotlivé moduly), pak půjdu o úroveň
níž u konkrétní věty/části a dostanu se na úroveň frází (metod/funkcí).
Pak můžu ještě rozpitvat frázování, či melodickou linku výseku 2-3 taktů
(implementace funkce).

Naopak, pokud se budu danou skladbu učit, nebo ji psát (bez ohledu na to,
jestli ji zapisuji do not), bud postupovat od jednotlivých tónů, riffů,
licků a sub-frází, ke smysluplné hudební frázi (metodě). Fráze (které
mají logický vývoj) budu (většinou intuitivně, ale někdy i logicky)
organizovat do vyšších celků: slok, částí (hudebních) vět (tj. tříd,
souborů funkcí atd.). Až se dostanu k úplnosti celé skladby (aplikace).

## Tři a víc...

> V softwarovém inženýrství, stejně jako v hudbě, je potřeba někdo
  s vizí a autoritou, pokud má skupina více jak dva členy.
  ~ SoftWare Samuraj

{{< figure src="/2020/01/Chess.jpg" link="/2020/01/Chess.jpg" >}}

Věc, kterou jsem si nechal na konec, je ta nejdůležitější. A vlastně mě
původně k této analogii přivedla. Jak už jsem psal výše, strávil jsem
v hudebním světě, zhruba tolik času, co v tom softwarovém. To je dlouhá
doba a za tu se člověk leccos naučí --- ať už v oboru, tak obecně
pro život.

Na hudební dráze jsem urazil přirozený a logický vývoj --- naučil jsem
se na nástroj a hrál jednotlivé písně. Pak jsem se přidával a zakládal
různé kapely, většinou tak o 3-6 členech. Pak jsem se angažoval v orchestru
25 hráčů. Pak jsem se dostal do 60hlavého sboru. A zazpíval jsem si
skladbu v tělese několika stovek lidí.

V programování jsem začínal podobně --- jako samouk jsem dělal jednoduché
webové stránky. Pak jsem začal dělat webové aplikace a jedna byla už
"tak veliká", že jsme ji museli udělat s kolegou ve dvou. Pak přišly roky
kdy jsem veškerý software psal v týmech "agilní velkokosti". Pak přišly
projekty, kdy týmy měly mezi 20 a 50 členy. A pak přišel projekt, kde
bylo mezi 400-600 lidmi.

Když jste sám, děláte si věci po svém. Když jste dva tak se domluvíte,
někdy jen kývnete hlavou. Když jste v kapele (týmu), tak formálně, či
neformálně se tam "objeví" leader --- někdo, kdo muziku (práci) sklene
do smysluplného celku a zároveň zajistí praktické fungování.

Jakmile přesáhnete velikost hudební kapely (agilního týmu), musíte začít
hierarchizovat --- kromě dirigenta, budete potřebova leadery jednotlivých
sekcí/hlasů (scrum-of-scrums). A jakmile jdete do stovek lidí, bude tam
minimálně jedna vrstva leadershipu navíc.

Co je podstatné, úspěch takového hudebního (softwarového) počinu stojí
a padá s kvalitou leadera. Můžete mít hvězdného sólového kytaristu,
fenomenální sopranistku, bohem obdařeného perkusionistu (softwarové
ekvivalenty si jistě najdete sami), ale pokud zbytek nestojí za to,
jste odsouzeni k neúspěchu, živoření, záhubě.

Ten leader není jen někdo, kdo máchá taktovkou... on musí mít vizi,
jak má vypadat výsledný celek. Celek, který si nikdo z řadových
hráčů neumí ani představit a kolikrát... ani pochopit. A od vize
je ještě dlouhá cesta k realizaci.

Na konci bude buď standing-ovation, zdvořilý potlesk, nesouhlasné
hvízdání, nebo házení rajčaty a pivními kelímky.

## Hudba se nedá spoutat pravidly

V předěšlých odstavcích jsem popsal některé výrazné paralely mezi světem
hudby a softwarového inženýrství. Ono jich je daleko víc, ale už jsou
více subtilní a pokud někdo není rozkročen mezi těmi dvěma světy, asi
by brzo ztratil nit.

Zároveň je potřeba mít pořád na paměti, že _každé srovnání kulhá_ a je
potřeba je brát s rezervou a v kontextu. Pokud to pomůže osvětlit
nějaký problém, myšlenku, jen dobře. Pokud to přinese větší cit pro
harmonii, ještě lépe.

Přeju, ať vaše softwarové projekty ladí! 🎻

## Osamělost maratonského běžce

Téma paralel k softwarovému inženýrství nosím v hlavě už řadu let (jen se
ne a ne dostat k tomu, to postupně všechno napsat). Jedna z těch paralel,
kterou mám rozmyšlenou více, než méně je _příprava a zaběhnutí maratonu_.

Sice nebudu slibovat nic konkrétního, kdy to napíšu, ale aspoň malá
ochutnávka:

> Overtime is like sprinting: It makes some sense for the last hundred yards
  of the marathon for those with any energy left, but if you start sprinting
  in the first mile, you’re just wasting time. --- Tom DeMarco, Peopleware

## Související články

* [Software Engineering, má rozumné paralely?](/2016/10/software-engineering-ma-rozumne-paralely/)
