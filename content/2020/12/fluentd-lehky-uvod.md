+++
title = "Fluentd, lehký úvod (část 2)"
date = 2020-12-18T19:49:15+01:00
description = """Fluentd je modernější alternativou k etablovanému řešení
    pro sbírání distribuovaných logů. The ELK is dead, long live
    the Fluentd! Druhá část, s praktickým příkladem."""
tags = ["cloud", "logging"]
hero_image = "hero.jpg"
thumbnail = "2020/12/Logs.jpg"
+++

{{< figure src="/2020/12/Logs.jpg" link="/2020/12/Logs.jpg" >}}

Dnes budeme pokračovat dalším dílem minisérie o [Fluentd](//www.fluentd.org/).
Minule jsme si řekli něco o historii logování a zasadili si _Fluentd_
do kontextu. A trochu jsme si načrtli architekturu a design.

Dnes se podíváme, jak si nakonfigurovat jednoduchou logovací
infrastrukturu. Ale ještě před tím, se krátce vrátím k minulému článku.

## Lesk a bída bulvárních titulků

Když jsem předešlý článek nazval [Fluentd, budoucnost
logování](/2020/10/fluentd-budoucnost-logovani/), měl jsem u toho postranní
úmysly. Respektive, nemyslel jsem to úplně vážně. Jako na krev. (Jinak,
pravda to samozřejmě je. 😈) Ale zafungovalo to --- na Twitteru se lehce
rozproudila diskuze a u dvou názorů bych se rád pozastavil.

### Je JSON budoucnost logování? Ne

{{< tweet 1312023349565022220 >}}

Ten původní odstavec (ve kterém jsem 4x napsal slovo JSON 🙄) jsem neformuloval
úplně šťastně --- nechtěně jsem zdůraznil přenosový formát, který je vlastně
úplně nepodstatný.

To, co se mi na _Fluentd_ líbí (a proč si myslím, že je to dobré moderní řešení),
souvisí s designem, architekturou, otevřeností založenou na pluginech a
dobrým pokrytím vstupních a výstupních kanálů (zajištěnou core pluginy).

O JSONu si každý můžeme myslet co chceme, ale buď jak buď, je to [lingua
franca](//en.wikipedia.org/wiki/Lingua_franca) současného cloudu. Ať už
na úrovni infrastruktury (moje doména), nebo aplikací. 🤷‍♂️

### Je telemetry budoucností logování? Ne

{{< tweet 1312499113372520449 >}}

Hodně zajímavá a (pro mne) přínosná byla diskuze
o [telemetry](//en.wikipedia.org/wiki/Telemetry#Software)
a jestli nahrazuje (a je lepší než) logování.
Myslím si, že jsou to nesouvisející témata. Alespoň z hlediska _Fluentd_
---  tomu je totiž jedno, jaký obsah přenáší (hlavně, že je strukturovaný).
Takže klidně může přenášet metriky. Nebo může přenášet [logy ve formátu
events](//docs.honeycomb.io/learning-about-observability/events-metrics-logs/#events-metrics-and-logs).
Je to jen na vás, co budou vaše aplikace produkovat. 🤷‍♂️

Mimochodem, odkazované [OpenTelemetry](//opentelemetry.io/), je také součástí
[CNCF](//www.cncf.io/), stejně jako _Fluentd_, nebo třeba
[Prometheus](//prometheus.io/) (etalon metrik). Co může vést k neporozumění
na základě výše zmíněného článku, API a výstupní formát může být identický
jak pro events, tak pro metriky, tak pro logy. Ale jejich smysl a použití
je diametrálně jiné. Už proto, že jde o rozdílné typy dat --- log record (event)
je jednorozměrná textová zpráva (může mít atributy), metrika je/může být
vícerozměrná číselná hodnota, reprezentovaná jako [číselná
řada](//en.wikipedia.org/wiki/Time_series_database).

Každopádně, (_Fluentd_) metrikám se budu alespoň letmo věnovat v příštím článku.

## Jednoduchá Fluentd pipeline

Dost bylo filozofování, pojďme se podívat na reálný kód. S něčím takovým budete
pravděpodobně začínat, možná to bude vaše [PoC](//en.wikipedia.org/wiki/Proof_of_concept),
či prostě pouhé vyzkoušení _Fluentd_. Vezměme si jednoduchý use case:

* Logovací soubory v JSON formátu budou uloženy v definované adresářové struktuře.
* Uděláme jednoduchou sanitizaci, že logy jsou ve formátu UTF-8.
* Logy pošleme do [Elasticsearch](//www.elastic.co/elasticsearch/) (čistě jen
  z důvodu, abychom si je jednoduše a agregovaně zobrazili).

### Načtení logů

Naším úkolem bude načíst logy z následující struktury:

{{< figure src="/2020/12/Directory-tree.png"
    link="/2020/12/Directory-tree.png"
    caption="Adresářová struktura a výpis jednoho logu" >}}

Čili:

* Každý log má svůj adresář,
* log je strukturovaný (atributy `msg`, `level`, `cell-id`),
* časový identifikátor (atribut `ts`) je Unix timestamp v milisekundách.

Pro tento účel je ideální [tail plugin](//docs.fluentd.org/input/tail), který se chová
obdobně jako klasický unixový [tail](//www.man7.org/linux/man-pages/man1/tail.1.html)
--- čte z konce souboru, resp. nově přidané řádky.

{{< gist sw-samuraj 49c9101cf7dd51e4fb6755f48e1a099b >}}

Atributy konfigurace jsou docela samopopisné, takže nepůjdu do detailu, ale u dvou
z nich bych se zastavil.

Pokud aplikace hned po vzniku logovacího souboru do něj ihned velmi rychle zapisuje
(v mém případě to byly milisekundy a desítky záznamů), může se stát, že tyto úvodní
zprávy budou ignorovány. 😢 Důvod je ten, že _Fluentd_ nejprve daný soubor
zaregistruje pro sledování (viz následující výpis) a až teprve potom začne sledovat
přibývající záznamy.

```bash
2020-12-18 10:54:06 [info]: following tail of /home/guido/dev/fluent/cell-1/cell.log
```

Řešením je nastavit atribut `read_from_head` na `true`. Nový soubor je tak čten
vždy od začátku.

Samozřejmě, není efektivní číst soubor a nevědět, kde jsme se čtením skončili
minule. Proto je dobré nastavit _position file_ (atribute `pos_file`), který
drží ukazatel na poslední čtenou pozici v každém sledovaném souboru.

{{< figure src="/2020/12/Positions-file.png"
    link="/2020/12/Positions-file.png"
    caption="Pozice čtení pro jednotlivé logy" >}}

### Filtrování logů

OK, logy máme načtený (někde uvnitř _Fluentd_) a než je převážeme mašlí a doručíme,
chceme je trochu porychtovat. Přichází ke slovu [filter pluginy](//docs.fluentd.org/filter).
Ty mají na starosti filtrovat, či/a transformovat jednotlivé záznamy, než
se posunou do výstupních pluginů.

Řekněme, že máme následující use case: představme si (čistě hypoteticky 😈), že se
nemůžeme spolehnout na to, že nám vývojáři sypou do logů záznamy v očekávaném
kódování. Takže než je posuneme dál, chceme záznamy pročistit, sanitizovat.

Poslouží nám [record_transformer plugin](//docs.fluentd.org/filter/record_transformer).
Jak název napovídá, transformuje jednotlivé záznamy. Za zmínku stojí jeho atribut
`enable_ruby`, který umožní používat [Ruby](//www.ruby-lang.org/en/) výrazy (není
to tak zlé, jak to zní 🤭).

{{< gist sw-samuraj 90a179100abcc98e300f9aa233d0c75c >}}

Konkrétně zde projdeme všechny záznamy a pokud tento obsahuje nějaké illegální
UTF-8 znaky, 😱 nahradí je řetězcem `__NON_ASCII_CHAR__`.

### Publikování logů

Už máme skoro hotovo --- zbývá záznamy někam publikovat, stačí si vybrat vhodný
[output plugin](//docs.fluentd.org/output). V případě, že chceme záznamy
publikovat do více cílových platforem, použije se [copy
plugin](//docs.fluentd.org/output/copy). V našem případě to ale uděláme jednoduše,
starý dobrý _Elasticsearch_:

{{< gist sw-samuraj b4c45268ef8bdb73080b60ad1533ccda >}}

Nastavování _Elasticsearch_ a případně [Kibany](//www.elastic.co/kibana) zde
přeskočíme (to už přece dávno umíte 😉) a podíváme se rovnou na výsledek:

{{< figure src="/2020/12/Kibana.png"
    link="/2020/12/Kibana.png"
    caption="Logy přenesené do _Elasticsearch_, zobrazené v _Kibaně_" >}}

### Celá konfigurace

{{< gist sw-samuraj e74710a1722e2f676de3eb90ad4e8d84 >}}

## Pokračování příště

Ne, nebojte se --- nebude to nekonečný seriál. Ale jak se situace
vyvíjí, budu muset přidat ještě jeden díl. Tentokrát už určitě
závěrečný. 😈

A na co, že se podíváme? Na reálný use case, který se časem
vyvinul v docela komplexní řešení.

## Související články

* [Fluentd, budoucnost logování (část 1)](/2020/10/fluentd-budoucnost-logovani/)
* [Fluentd, case study (část 3)](/2021/03/fluentd-case-study/)
* [Nešvary logování](/2017/12/nesvary-logovani/)
