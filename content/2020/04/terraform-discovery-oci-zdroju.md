+++
title = "Terraform discovery OCI zdrojů"
date = 2020-04-30T12:52:57+02:00
description = """Terraform je vůdčí nástroj v oblasti Infrastructure as Code.
    OCI je etablovaná cloud infrastruktura. Jak jdou tyhle dvě věci dohromady?
    Překvapivě dobře, ale to není nic nového. Zajímavá je ale jedna vlastnost
    - Resource Discovery (alias Reverse Engineering). A na tu se dnes podíváme."""
tags = ["cloud", "oci", "terraform", "infrastructure-as-code"]
hero_image = "hero.jpg"
+++

{{< figure src="/2020/04/Compute-instances.png"
    link="/2020/04/Compute-instances.png" >}}

_Nedávno náš tým publikoval novou službu (o které se nebudu moc šířit)
a jelikož součástí jsou i věci, týkající se oblasti_ Infrastructure as Code
_(což mě hodně zajímá) napsal jsem k tomu v angličtině nějaké články, které
teď uvádím v češtině._

Pojďme začít krátkým a stručným úvodem, co to je _OCI_ a _Terraform_.

* [Oracle Cloud Infrastructure](//cloud.oracle.com/cloud-infrastructure) (OCI), aka.
  _Oracle Cloud_ nabízí moderní set cloudových zdrojů --- od síťové infrastruktury,
  přes _compute_ instance, až po _Kubernetes_ a _Hadoop_ clustery. A spoustu dalších
  věcí.
* [Terraform](//www.terraform.io/) je vedoucí a trend-setting řešení pro koncept
  [Infrastructure as Code](//en.wikipedia.org/wiki/Infrastructure_as_Code) ---
  umožňuje definici a provisioning cloudové infrastruktury pomocí deskriptivního
  konfiguračního jazyka a podporuje spoustu cloudových poskytovatelů --- mmj.
  [AWS](//aws.amazon.com/), [GCP](//cloud.google.com/), [Azure](//azure.microsoft.com/)
  a samozřejmě, OCI.

Jak jdou tyhle dvě věci --- OCI & Terraform --- dohromady? OCI poskytuje několik
způsobů, jak komunikovat s cloudovými produkty a zdroji:

* UI console
* REST API
* SDK pro různé jazyky (Java, Python, Golang, Ruby)
* CLI klient
* [Terraform provider](//www.terraform.io/docs/providers/oci/index.html)

## Terraform Discovery

{{< figure class="floatright" src="/2020/04/Terraform.png"
    link="/2020/04/Terraform.png" >}}

_Terraform_ discovery --- "objevení", či reverse-engineering existující zdrojů
--- je relativně nová feature, která umožňuje zmapovat OCI zdroje v určitém
[kompártmentu](//docs.cloud.oracle.com/en-us/iaas/Content/GSG/Concepts/concepts.htm)
a následně vytvořit potřebné _Terraform_ konfigurační soubory.

Existuje řada use casů, kdy se tato discovery feature může hodit:

* migrujeme na _Infrastructure as Code_,
* určité zdroje byly vytvořeny jiným způsobem (console, SDK, atd.) a my bychom
  chtěli reprodukovatelné řešení založené na industriálním standardu,
* zálohování komplexní infrastruktury, ad.

Dokumentaci pro _Terraform_ discovery lze najít na stránce: [Discovering Terraform
resources in an Oracle Cloud Infrastructure compartment](//www.terraform.io/docs/providers/oci/guides/resource_discovery.html).

## Discovery OCI zdrojů

Řekněme, že máme následující jednoduchý use case:

1. Máme vytvořeno několik základních (core) OCI zdrojů.
1. Chceme stav a konfiguraci těchto zdrojů dokumentovat a uložit.

### Existující OCI zdroje

Pro jednoduchost, máme vytvořené 2 _compute_ instance, s následující
konfigurací. Samozřejmě, jsou tam další příslušné zdroje, jako: VCN,
subnet, seclist atd. Každopádně si dovememe představit daleko složitější
infrastrukturu pro nějaký reálný scénář.

{{< figure src="/2020/04/Compute-instance-detail.png"
    link="/2020/04/Compute-instance-detail.png" >}}

### Nastavení Terraformu

Vynechám podrobnosti jak nainstalovat a nakonfigurovat _Terraform_ a pouze
ukážu, jak získat potřebnou verzi _Terraform OCI Provideru_. Pro autentikaci
vás odkážu na [oficiální dokumentaci](//www.terraform.io/docs/providers/oci/guides/resource_discovery.html#prerequisites).

Protože začínáme v prázdném adresáři, musíme _Terraformu_ nějak říct,
s jakou verzí provideru chceme pracovat. To můžeme udělat pomocí
následujícího souboru:

{{< gist sw-samuraj d93b9f4aeb7dcaf537b8a6f37c4c1db4 >}}

Teď jen potřebujeme spustit příkaz `terraform init` a stáhne se nám daná
verze provideru.

{{< figure src="/2020/04/Terraform-oci-provider-init.png"
    link="/2020/04/Terraform-oci-provider-init.png" >}}

Plugin provideru je umístěn v adresáři `.terraform/plugins/<architecture>`.

{{< figure src="/2020/04/Terraform-oci-provider-plugin.png"
    link="/2020/04/Terraform-oci-provider-plugin.png" >}}

Nebude od věci si pro pozdější použití vytvořit symbolický link:

```bash
ln -s .terraform/plugins/linux_amd64/terraform-provider-oci_v3.69.0_x4 terraform-provider-oci
```

### Terraform discovery

Jakmile máme připraveno, je použití přímočaré:

{{< gist sw-samuraj acca70a1e3174601bcdc47b6504eff6c >}}

Plugin se připojí do OCI, udělá discovery a zapíše konfigurační soubory
do výstupního adresáře:

{{< figure src="/2020/04/Terraform-discovery-output.png"
    link="/2020/04/Terraform-discovery-output.png" >}}

Pokud se podíváme do souboru `core.tf` uvidíme povědomé hodnoty
z naší _compute_ instance:

{{< gist sw-samuraj fe61df40769c2f5fcc9af36cd9f3ed3d >}}

### Generování Terraform stavu

Jedna ze silných stránek _Terraformu_ je _Terraform State_ --- ačkoliv se
_Terraform_ chová ke zdrojům jako by byly _immutable_, tak interně si
drží jejich stav. 🤓 (Tento stav může být buď lokální, nebo remote, ale 
to už je úplně jiný příběh.)

Naštěstí, _Terraform OCI Provider_ negeneruje jenom konfigurační soubory,
ale také soubor pro (lokální) stav. Jediný co potřebujeme, je pustit
výše uvedený příkaz s přepínačem `-generate_state`.

A voilà, máme stav (podívejte se do souboru `terraform.tfstate`):

{{< figure src="/2020/04/Terraform-discovery-with-state.png"
    link="/2020/04/Terraform-discovery-with-state.png" >}}

Tak, a teď už můžeme používat _Terraform_ tak, jak jsme běžně zvyklí.

## Které zdroje jsou objevitelné?

Jak OCI, tak _Terraform_ jsou rychle se rozvíjející dynamické systémy
a nové vlastnosti organicky přibývají. Discovery je relativně nová
feature a tak ne všechny OCI zdroje jsou podporované pro reverse-engineering.

Seznam objevitelných zdrojů získáme příkazem:

```bash
terraform-provider-oci -command=list_export_resources
```

Aktuálně podporuje plugin následující zdroje a služby:

* `identity`
* `core` (instances, network etc.)
* `database`
* `load_balancer`
* `object_storage`
* `tagging`
* `bds` (Big Data Service)

## Původní články v angličtině

* [Terraform Discovery of OCI Resources](//medium.com/@sw_samuraj/terraform-discovery-of-oci-resources-6cf3e9cf09ef)
* [Terraforming of OCI Big Data Service](//medium.com/@sw_samuraj/terraforming-of-oci-big-data-service-1e293328499)
