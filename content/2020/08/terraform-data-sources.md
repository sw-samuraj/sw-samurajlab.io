+++
title = "Terraform Data Sources, jak se dotazovat na zdroje v cloudu"
date = 2020-08-21T17:18:29+02:00
description = """Vytvořit a zrušit infrastrukturu pomocí Terraformu je
    přímočaré. Jak se ale na jednotlivé zdroje dotazovat? Pomocí
    Terraform Data Sources."""
tags = ["infrastructure-as-code", "cloud", "terraform"]
hero_image = "hero.jpg"
thumbnail = "2020/08/Sources.jpg"
+++

{{< figure src="/2020/08/Sources.jpg" link="/2020/08/Sources.jpg" >}}

[Terraform](//www.terraform.io/) je skvělý nástroj, který se profiluje jako
jeden z leaderů nastupujícího trendu [Infrastructure as Code](//en.wikipedia.org/wiki/Infrastructure_as_code)
(IaC). Vytvořit a zrušit infrastrukturu pomocí příkazů `terraform apply` a
`terraform destroy` je jednoduché a přímočaré.

Když se nad tím ale zamyslíme, _Terraform_  nedělá nic jiného, než že
mapuje abstraktní [DSL](//en.wikipedia.org/wiki/Domain-specific_language)
na API jednotlivých providerů. (Protože my znalci a evangelisti IaC víme, že
všechny cloudy jsou [API-driven](//hub.packtpub.com/architects-love-api-driven-architecture/).)
Takže, když to hodně zjednoduším, většina zdrojů v cloudu se manipuluje pomocí
[CRUD](//en.wikipedia.org/wiki/Create,_read,_update_and_delete) volaným přes REST. 😈

_Create_ a _delete_ je v _Terraformu_ jednoznačný. _Update_ celkem taky. Co ale
_read_ (a eventuálně _list_)? 🤔 Částečně sem spadají [output values](//www.terraform.io/docs/configuration/outputs.html),
ale tak nějak cítíme, že to není opravdový _read_, ale spíš vedlejší produkt (či
něco jako response) operací _create_ a _delete_.

## Data Sources

Naštěstí má _Terraform_ koncept [data sources](//www.terraform.io/docs/configuration/data-sources.html).
Ovšem jejich použití není úplně intuitivní, takže nebude na škodu si uvést
malý příklad. Nicméně, first-thing-first --- co to je _data sources_?

> Data sources allow data to be fetched or computed for use elsewhere in Terraform
  configuration. Use of data sources allows a Terraform configuration to make use
  of information defined outside of Terraform, or defined by another separate
  Terraform configuration.

> Each provider may offer data sources alongside its set of [resource
  types](//www.terraform.io/docs/configuration/resources.html#resource-types-and-arguments).

Čili _data source_ je objekt, který drží hodnoty daného zdroje --- ať už byl
ten zdroj vytvořen danou _Terraform_ konfigurací, jinou _Terraform_ konfigurací,
anebo úplně externě.

Jak konkrétně daný _data source_ vypadá, je daný specifickým [providerem](//www.terraform.io/docs/configuration/providers.html)
a jeho zdroji. Abychom si ukázali konkrétní příklad (a já si přihřál
polívčičku, na čem jsem poslední rok a půl pracoval 😳), vezměme si
instanci [Big Data Service](//docs.cloud.oracle.com/en-us/iaas/big-data/index.html)
(BDS), což je velmi komplexní zdroj, avšak chceme-li se na něj dotázat, stačí
následující kód:

```terraform
data "oci_bds_bds_instance" "tf_bds_instance" {
    #Required
    bds_instance_id = "ocid1.bigdataservice.oc1.iad.abcdefg"
}
```

Kromě toho, že definujeme, že jde o _data source_ (keyword `data`) daného typu
(keyword `oci_bds_bds_instance`), s daným jménem (`tf_bds_instance`), tak už stačí
jen jeden povinný parametr --- identifikátor zdroje (`bds_instance_id`).

## Statické a dynamické reference

Je dobré zmínit, že daný identifikátor můžu zadat buď jako konstantu (tj. dané
ID už znám), nebo může jít o hodnotu, která teprve dynamicky vznikne (např.
pokud daný zdroj _Terraformem_ vytvářím/modifikuji). V druhém případě zadám
identifikátor jako [referenci na atribut
zdroje](//www.terraform.io/docs/configuration/expressions.html#references-to-resource-attributes).

Pokud mám v definici _data source_ pouze statické hodnoty, potom jsou hodnoty daného
_data source_ načteny během `refresh` fáze (která probíhá před vytvořením _Terraform_
plánu). Výsledek tak dostaneme příkazem `terraform refresh`.

Pokud mám v definici _data source_ dynamické hodnoty, potom je potřeba, aby
všechny tyto hodnoty byly prvně spočteny. V tom případě je vytvoření _data
source_ odloženo až do fáze `apply`, tj. je potřeba spustit příkaz `terraform
apply`.

## Kompletní příklad

Pokud mám definovaný _data source_, je potřeba ho "nějak" zasadit do naší
_Terraform_ konfigurace. Je možné ho přidat ke stávajícím zdrojům, anebo mít
jen konfiguraci na dotazování (která žádné zdroje nevytváří).

Příkladem takové minimalistické dotazovací konfigurace je následující kód,
který definuje [providera](//www.terraform.io/docs/configuration/providers.html),
_data source_ a [výstupní hodnotu](//www.terraform.io/docs/configuration/outputs.html):

{{< gist sw-samuraj 6763565ac244d06455d4cf3eb7a6bba1 >}}

Pokud daná _BDS instance_ existuje (a poskytli jsme pořebné [vstupní
proměnné](//www.terraform.io/docs/configuration/variables.html)), dostaneme
příkazem `terraform refresh` následující výpis:

{{< figure src="/2020/08/Terraform-refresh.png"
    link="/2020/08/Terraform-refresh.png"
    caption="Výpis data source pro BDS (pouze malá část, zkráceno)" >}}

## Související články

* [Infrastructure as Code, lehký úvod](/2020/05/infrastructure-as-code/)
* [Terraform discovery OCI zdrojů](/2020/04/terraform-discovery-oci-zdroju/)
