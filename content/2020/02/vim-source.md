+++
title = "Vim: (dávkové) spouštění ex příkazů"
date = 2020-02-21T12:26:23+01:00
description = """Čas od času si člověk vytuní ve Vimu nějaký příkaz,
    nebo regex a rád by se k němu vrátil. Nebo ho opakovaně používal.
    Genialita Vimu nezná mezí a tak jdou tyto věci udělat
    na dvě-tři klepnutí prstu. Lighning speed. ⚡"""
tags = ["vim"]
hero_image = "hero.jpg"
+++

{{< figure class="floatleft" src="/img/logos/Vim.png" >}}

Když jsem před časem [zmigroval blog na Hugo](/2018/11/migrace-z-blogger-na-hugo-a-gitlab-pages/),
bylo to celkem jednoduché --- tehdejší verze _Hugo_ bez problémů požrala
HTML čurbes z _Bloggeru_ a tak s tím nebylo moc starostí.

Bohužel, od verze [v0.60.0](//github.com/gohugoio/hugo/releases/tag/v0.60.0) změnil
_Hugo_ Markdown knihovnu --- z [Blackfriday](//github.com/russross/blackfriday)
na [Goldmark](//github.com/yuin/goldmark/) a mě se přestaly renderovat všechny
nevyčištěné stránky (tedy ty, nepřevedené na čistý Markdown). 😱 😬

Asi by se to dalo vyřešit přes konfiguraci, nastavením méně striktního
renderování, ale rozhodl jsem se na to jít z gruntu a všechen ten HTML
balast odstranit. To je hodně práce. A protože tenhle blog píšu ve _Vimu_,
tak jsem čistil... ve _Vimu_. 😎

## Okno s historií příkazů

Přítelem každého Vim-uchyláka je bravurní znalost (Vim-flavored) regexů.
A když vás po čase přestane bavit je psát zpaměti, začnete je hledat
v historii.

Knížce [Practical Vim](//www.goodreads.com/book/show/20741778-practical-vim)
vděčím mimo jiné za to, že vím, k čemu je ten příkaz, který mě vždycky
přiváděl k šílenství, když jsem chtěl zavřít buffer, a omylem jsem se
překlepl, takže jsem místo `:q` napsal `q:`

{{< figure src="/2020/02/Vim-cmd-history.png"
    caption="Vim buffer s otevřeným command-line oknem s historií příkazů" >}}

Příkazy `q:`, `q/` a `q?` totiž otevřou [command-line window](//vimdoc.sourceforge.net/htmldoc/cmdline.html#q:)
s načtenou historií `:`, či `/` příkazů. Výhodou je, že se v tom okně
můžete chovat jako ve _Vimu_. 🤓 Můžete v něm vyhledávat, kopírovat,
editovat... A znovu spouštět dané příkazy.

## Spuštění příkazů ze souboru

Vyhledání a spuštění nějakého složitější příkazu je sice fajn, ale
jsou tu dva problémy:

* Jednak jde o historii a ta má nějaký limit (třeba já mám defaultní
  `history=200`), takže po čase může příkaz zmizet.
* Pokud zpracovávám více souborů, je to repetitivní a chtělo by to
  nějak naskriptovat. 🤔

Ke slovu tak přichází příkaz [source](//vimhelp.org/repeat.txt.html#using-scripts),
který načte sadu příkazů ze souboru a postupně je provede.

Pro potřeby čištění blogu jsem si vytvořil následující sadu příkazů:

{{< gist sw-samuraj 514514a6b03d4ef182071c315e581bb4 >}}

Teď stačí zadat v editoru tenhle příkaz a mám vyčištěno!

```bash
:source srcipts/html-to-markdown.vim
```

## Fakt je to tak jednoduchý?

Jasně, je. Pořeší se tím všechno, co se dá zvládnout regulárním
výrazy. Samozřejmě, my, co jsme měli na škole dva semestry předmětu
_Regulární a bezkontextové jazyky_ a není nám neznámá [Chomského
hierarchie jazyků](//en.wikipedia.org/wiki/Chomsky_hierarchy) 🤓
víme, že některé problémy se regexem vyřešit nedají. Ale na čištění
HTML to bohatě stačí... (tak z 95 % 😈).

> Happy sourcing!

## Související články

* [Migrace z Blogger na Hugo a GitLab Pages](/2018/11/migrace-z-blogger-na-hugo-a-gitlab-pages/)
* [Práce s Vim buffers](/2019/01/vim-buffers/)
* [vimdiff, nástroj drsňáků](/2017/11/vimdiff-nastroj-drsnaku/)

## Externí odkazy

* [:help source](//vimhelp.org/repeat.txt.html#using-scripts)
