+++
title = "Twitter Cards a Hugo šablony"
date = 2020-06-24T19:33:42+02:00
description = """Zachtělo se mi mít pro blog pěkné linky na Twitteru.
    Aneb, jak vytvořit Twitter Cards v Hugo šabloně."""
tags = ["blogging", "golang"]
hero_image = "hero.jpg"
thumbnail = "2020/06/Twitter-Cards.jpg"
+++

{{< figure src="/2020/06/Twitter-Cards.jpg" link="/2020/06/Twitter-Cards.jpg" >}}

Když jsem před časem začal psát blog v [Hugo](//gohugo.io/), byl jedním
z kroků výběr (grafického) [tématu](//themes.gohugo.io/). Témata udržuje
komunita a tak nepřekvapí, že jsou rozdílné kvality.

U obou blogů, které momentálně provozuji jsem potřeboval vybraná témata
upravit. Kromě různých CSS úprav a přizpůsobení layoutu a dalších feature
jsem se už nějaký čas chystal na _Twitter Cards_.

Je dost možný, že _Twitter Cards_ už jsou ve vašem tématu nachystaný, ale
v mém případě tomu tak nebylo ani v jednom případě. I když úplně ochuzen
jsem taky nebyl --- v mých tématech byly alespoň (některá) metadata
pro [Open Graph](//ogp.me/), který je Twitterem částěčně podporovaný.
Ale úplně správný výsledky mi to nedávalo.

## Co je to Twitter Card

> With Twitter Cards, you can attach rich photos, videos and media experiences
  to Tweets, helping to drive traffic to your website. Simply add a few lines
  of markup to your webpage, and users who Tweet links to your content will have
  a "Card" added to the Tweet that's visible to their followers.

Neznám marketingový mumbo-jumbo o tom, co přesně to _Twitter Card_ je, ale
pro mne to dělá takovej ten hezkej tweet, odkazující na článek, či webovou
stránku:

{{< figure src="/2020/06/Twitter-Card-large.png"
    caption="Ukázka _Twitter Card_ s velkým obrázkem" >}}

Existují čtyři druhy karet:

* _Summary Card_ --- základní jednoduchá karta: má titulek, popis a náhled
  obrázku.
* _Summary Card with Large Image_ --- stejný jako _Summary Card_, ale
  s velkým obrázkem.
* _App Card_ --- karta s odkazem na mobilní aplikaci.
* _Player Card_ --- karta se zobrazením videa/audia.

## Jak vytvořit Twitter Card

Vytvoření _Twitter Card_ je jednoduché --- stačí do hlavičky stránky
umístit potřebná metadata:

```html
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@sw_samuraj">
<meta name="twitter:title" content="Cesta samuraje, rok devátý">
<meta name="twitter:description" content="Člověk se ani nenaděje a už jsou
    tady další narozeniny. Tentokrát to pojmu trochu jinak a nebudu psát
    o osobních věcech, ale podívám se statisticky trochu zpět.">
<meta name="twitter:image" content="https://sw-samuraj.cz/img/hero.jpg" />
```

Referenci všech dostupných tagů lze nalézt v [dokumentaci](//developer.twitter.com/en/docs/tweets/optimize-with-cards/guides/getting-started),
navrženou kartu je možné si prohlédnout ve [validátoru](//cards-dev.twitter.com/validator).

## Jak dostat Twitter Card do Hugo šablony

V případě použití _Hugo_ (či jiného generátoru statických stránek) je otázka,
jak hodnoty pro _Twitter Card_ generovat dynamicky. 🤔 Jednoduše --- odpovědí
jsou šablony, proměnné a různé funkce.

Příklad _Twitter Card_ v _Hugo_ šabloně (plná verze
na [GitHubu](//github.com/sw-samuraj/vienna/blob/master/layouts/partials/header.html#L6-L17)):

```html
{{ if .IsPage }}
  <meta name="twitter:card" content="summary" />
  <meta name="twitter:site" content="@{{ .Site.Params.twitter }}" />
  <meta name="twitter:title" content="{{ .Title }}" />
  <meta name="twitter:description" content="{{ .Description }}" />
{{ if isset .Params "thumbnail" }}
  <meta name="twitter:image"
        content="{{ .Site.BaseURL }}{{ .Params.thumbnail }}" />
{{ else }}
  <meta name="twitter:image"
        content="{{ .Site.BaseURL }}{{ .Site.Params.avatar }}" />
{{ end }}
{{ end }}
```

Aniž bych příliš zabředával do [Golang](//golang.org/) templatování
(_Hugo_ používá přímo [template](//golang.org/pkg/text/template/) balíček
z _core_ knihovny):

* _Twitter Card_ se generuje, jen pokud je daný objekt typu `page`.
* Hodnoty pro metadata `twitter:title` a `twitter:description` se
  berou z parametrů `title` a `description` z [Front Matter](//gohugo.io/content-management/front-matter/).
* Hodnota pro metadata `twitter:site` se bere z parametru `twitter`
  ze souboru [config.toml](//gohugo.io/getting-started/configuration/)
  ze sekce `[params]`.
* Obrázek (`twitter:image`) se generuje buď:
  * z parametru `thumbnail` ve _Front Matter_, pokud je přítomen,
  * nebo z parametru `avatar` z globálního `config.toml`.

{{< figure src="/2020/06/Twitter-Card-summary.png"
    caption="Ukázka _Twitter Card_ typu _summary_" >}}

## Související články

* [Nový blog živě ve 30 minutách](/2020/03/blog-ve-30-minutach/)
* [Migrace z Blogger na Hugo a GitLab Pages](/2018/11/migrace-z-blogger-na-hugo-a-gitlab-pages/)
* [Technologické blogování](/2012/11/technologicke-blogovani/)
