+++
title = "Infrastructure as Code, lehký úvod"
date = 2020-05-06T19:32:29+02:00
description = """Infrastructure as Code (IaC) je relativně novým trendem
    v softwarovém inženýrství. Proč si nepřečíst, co si o tom myslí
    tenhle Samurájek, co se prsí, že to přednáší na univerzitě? 🤭
    Nadpis nelže - jde o opravdu lehký úvod do problematiky. (Abychom
    nikoho nevystrašili.)"""
tags = ["infrastructure-as-code", "cloud", "terraform"]
hero_image = "hero.jpg"
thumbnail = "2020/05/Scaffolding.jpg"
+++

{{< figure src="/2020/05/Scaffolding.jpg" link="/2020/05/Scaffolding.jpg" >}}

Už druhým rokem máme s kolegou krátký seminář na [MatFyzu](//www.mff.cuni.cz/)
o tématu [Infrastructure as Code](//en.wikipedia.org/wiki/Infrastructure_as_code).
Takže když už jsem to dopracoval tak daleko, že rozdávám moudra na univerzitě, 🤭
tak by neškodilo si trochu té ~~slávy~~ znalosti odložit i zde.

Rád bych se chviličku zastavil u toho, proč jsme posunuli téma někam jinam, než
škola původně chtěla --- předmět se jmenuje _Vývoj cloudových aplikací_ a primárně
po nás chtěli, abychom popovídali o [našem cloudu](//cloud.oracle.com/cloud-infrastructure).

My jsme si ale řekli, že než vykládat o konkrétním cloudu, kdy bude všechno za pár
měsíců/let úplně jinak, bude lepší studentům předložit obecnější a trvalejší
koncept. A voilà, přednáška _Infrastructure as Code_ byla na světě!

_[Update 14. 5. 2020]_
Vzhledem ke koronavirové šarádě 🤦‍♂️ byly všechny session přes _Zoom_
a pro jistotu jsme je nahrávali. A poněvadž pak studenti požádali
o jejich sdílení, tak jsem je publikoval na _YouTube_.

* Playlist: [Infrastructure as Code](//www.youtube.com/playlist?list=PL4HzyUYJLfsX2i6DxiuVL6XY_qw4K9bsy)
_[/Update]_

## Definice 3x jinak

{{< figure src="/2020/05/Foreword.jpg" link="/2020/05/Foreword.jpg" >}}

Když jsem se na přednášku připravoval, začal jsem tím, co o tématu říkají chytřejší
něž já. Například _Wikipedie_ říká:

> Infrastructure as code (IaC) is the process of managing and provisioning computer
  data centers through **machine-readable definition files**, rather than physical
  hardware configuration or interactive configuration tools. The IT infrastructure
  managed by this comprises both physical equipment such as bare-metal servers
  as well as virtual machines and associated configuration resources.

> The definitions may be in a **version control system**. 

Software-špióni z _ThoughtWorks_ to zase vidí takhle:

> Infrastructure as code, or programmable infrastructure, means writing code
  (which can be done using a high level language or any descriptive language)
  to manage configurations and **automate provisioning of infrastructure** in addition
  to deployments. ... using tested and proven software development practices that
  are already being used in application development. For example: **version control,
  testing, small deployments, use of design patterns** etc. In short, this means you
  write code to provision and manage your server, in addition to automating processes.

No a _Azure DevOps_ to popisuje jako:

> Infrastructure as Code (IaC) is the management of infrastructure (networks, virtual
  machines, load balancers, and connection topology) in a descriptive model, using
  the same versioning as DevOps team uses for source code. Like the principle that
  the same source code generates the same binary, an **IaC model generates the same
  environment every time** it is applied.

> **Idempotence** is a principle of Infrastructure as Code. ... a deployment command
  always sets the target environment into the same configuration, regardless
  of the environment’s starting state.

Jak je vidět, definic existuje mnoho (vybral jsem ty, jež mi nejlíp konvenují) a
žádná z nich není kompletní, ani jednoduchá. Za sebe bych si tedy dovolil
vypíchnout ty nejpodstatnější aspekty:

* Infrastruktura je definována (většinou deskriptivním) kódem. **Kód má být
  ve verzovacím systému.** (Používá se ještě něco jiného než _Git_? 🤔)
* Tenhle **kód se používá během CI/CD** (_Continuous Integration/Continuous Delivery_).
* Kód si zaslouží stejné praktiky, jako běžný "vývojářský" kód. Tedy:
  **testování, inkrementální deployment, code review, clean code, good design**, atd.
* Kód generuje **opakovatelný a idempotentní(!)** výsledek.
 
## Co to je zase za novinku? 🙄

{{< figure src="/2020/05/Swallows.jpg" link="/2020/05/Swallows.jpg" >}}

Samozřejmě, je legitimní otázka, proč by se o něco takového měl (náš) tým snažit?

### Benefity

Mezi sociální jistoty IaC patří:

_**Vývojový tým je vlastníkem (virtuální) infrastruktury.**_ Tohle jde ruku v ruce
se všemi těmi proudy jako je [DevOps](//en.wikipedia.org/wiki/DevOps),
[kros-funkční týmy](//en.wikipedia.org/wiki/Cross-functional_team), atd. Pokud
pracujete v _silu_, tak IaC asi nedosáhne svého potencionálu.

_**Developer mindset**_. Možná si někdo ještě myslí, že mít (rasově 😈) čisté
role, je dobrý nápad. Vývojáři a operations/admini se můžou navzájem hodně
obohatit. A sdílený kód je nejlepším pojítkem, průnikem a závazkem. Plus,
vývojáři prostě vědí, jak s kódem zacházet.

_**Version control --- a single source of truth.**_ Proč mít duležité věci
rozeseté po různých wiki, privátních návodech, proč mít různé aspekty
softwarového vývoje v různých formátech? Kód je kód, jsme přece
softwarový inženýři. Verzovací systém a textový soubor ---
nic transparentnějšího ještě nikdo nevymyslel. Šup tam i s infrastrukturou!

_**Knowledge sharing**_. Známý [bus factor](://en.wikipedia.org/wiki/Bus_factor),
co k tomu dodat?

_**Automatizace**_. Opět ohraná písnička: odstranění lidských chyb a nekonzistencí,
neúplnost řešení atd.

_**CI/CD**_. Už to bylo řečeno, ale je potřeba to znovu zdůraznit --- bez CI/CD
není žádný IaC.

### Proč zrovna teď? 🤔

Že se IaC vynořilo v uplynulých letech má svoje důvody. Ono jich bude víc, ale
jako tři dominantní bych viděl:

* vzestup a prominence _DevOps_,
* cloudová infrastruktura je převážně API-driven,
* infrastuktura je vysoce elastická.

Poslední bod bych trochu rozvedl, poněvadž nemusí být úplně jasné, co mám
na mysli. Jedním pojmem, umožňujícím elasticitu je _disposable infrastructure_.
Česky bychom řekli něco jako "infrastruktura, která se dá zahodit".

Jde o to, že pokud jsou v dnešní době zdroje převážně virtuální, není problém
celou infrastrukturu zahodit a znovu ji postavit lusknutím prstů --- nemusíme
ji budovat v potu a krvi z fyzických strojů.

A s tím souvisí i druhý aspekt, který má význačné finanční konotace --- zdroje
(v cloudu) dnes žijou povětšinou krátkodobě: spíše hodiny, dny až týdny, než měsíce
a roky.

A tyhle dvě položky samozřejmě tlačí k větší automatizaci. Přičemž odpovědí je...
IaC. 🤓

### Jaké jsou alternativy?

Alternativy všichni už dávno známe:

* Manuální nastavení přes UI konzoli.
* Manuální nastavení nějakým CLI nástrojem.
* Sada custom/in-house nástrojů (takové ty smečky neudržovatelných skriptů,
  na koleně zrobených frameworků atd.).

To nechcete. 🤢

## Dvě kategorie, dva přístupy

{{< figure src="/2020/05/Turtles.jpg" link="/2020/05/Turtles.jpg" >}}

Jak už jsme viděli u definic(e), výklad IaC je různorodý. Stejně tak,
pokud se budeme snažit jej dále katalogizovat. Jeden takový pohled vychází
z toho, jak se IaC dívá na komponenty, které spravuje:

### Immutable objekty

1. Objekt se ne-updatuje,
1. místo toho se odstraní,
1. a potom se vytvoří nový.

### Mutable objekty

* Změny jsou propagovány do běžících komponent.

Jiný pohled kategorizuje použité nástroje:

### Orchestrační nástoje

* [Terraform](//www.terraform.io/)
* [AWS CloudFormation](//aws.amazon.com/cloudformation/)
* [Azure Resource Manager](//docs.microsoft.com/en-us/azure/azure-resource-manager/management/overview), ad.

### Nástroje pro konfigurační manažment

* [Puppet](//puppet.com/)
* [Chef](//www.chef.io/solutions/)
* [Ansible](//www.ansible.com/overview/it-automation), ad.

## Testování

IaC nedělají jenom nástroje, ale také proces --- už jsem opakovaně zmiňoval
CI/CD. Neoddělitelnou a nutnou součástí tohoto procesu je i testování.

Jak takové testování vypadá? Zjednodušeně ho popisuje následující "3-kostičkový"
diagram:

{{< figure src="/2020/05/Infra-testing.png" link="/2020/05/Infra-testing.png"
    caption="Schéma IaC testování" >}}

1. V rámci CI/CD se pustí _provisioning_: buď vytvoření infrastruktury
   from-the-scratch, nebo z nějaké definované baseline.
1. Spustí se sada testů, _které testují infrastrukturu_. Např. jestli se
   správně vytvořily sítě, jestli nastartoval určitý počet virtuální mašin,
   jestli jsou dostupné určité porty, atd.
1. Spustí se _de-provisioning_, čili odstranění vytvořené infrastruktury.

## Konkrétní příklad

{{< figure src="/2020/05/Railways.jpg" link="/2020/05/Railways.jpg" >}}

Zatím jsme se bavili o IaC obecně, tak je na čase zmínit něco konkrétního
--- popíšu, jak jsme IaC využili v rámci vytváření nového produktu.

### Kontext

Stavěli jsme novou cloud-native službu --- takovou, která bude později
součástí cloudové platformy.

Následující diagram zobrazuje infrastukturu, která byla kompletně spravována
přístupem IaC. Nejedná se o celou architekturu, ale pouze o řídící část služby
(_control plane_). Druhá (ta větší) skupina zdrojů --- která měla na starosti
exekuční část služby (_data plane_) --- se vytvářela dynamicky on-demand
(elastický výpočetní cluster) a jako taková, nebyla pro IaC vhodná.

{{< figure src="/2020/05/Terraform-infra.png" link="/2020/05/Terraform-infra.png"
    caption="Control Plane infrastruktura vytvářená Terraformem" >}}

### Deliverables

* RPM balíčky
* Docker image
* Cloud-native image (to z čeho se startuje _compute_ instance)

### Nástroje

* [Terraform](//www.terraform.io/) pro tvorbu a správu infrastruktury.
* [Terratest](//github.com/gruntwork-io/terratest) pro testování infrastruktury.
* [GitLab Pipelines](//docs.gitlab.com/ee/ci/pipelines/index.html) pro CI/CD.

### Praktiky

* Všechny _Terraform_ skripty byly uloženy v _Gitu_.
* Změny skriptů procházely _code review_.
* Struktura a rozdělení skriptů odpovídala _best practices_ a konvencím daného
  _Terraform Providera_.
* Vytvoření validní infrastruktury bylo pokryto _Terratest_ testy.
* _Provisioning_ infrastruktury, její _testování_ a _de-provisioning_
  byly součástí CI/CD pipeliny.

{{< figure src="/2020/05/Pipelines.png" link="/2020/05/Pipelines.png"
    caption="GitLab Pipelines se zvýrazněnými Terraform testy" >}}

## Související články

* [Terraform discovery OCI zdrojů](/2020/04/terraform-discovery-oci-zdroju/)

## YouTube přednášky

* [Infrastructure as Code, Part 1](//www.youtube.com/watch?v=MJSuvN05XE4) Intro & OCI
* [Infrastructure as Code, Part 2](//www.youtube.com/watch?v=1eVmkY2oG9I) IaC & Terraform
* [Infrastructure as Code, Part 3](//www.youtube.com/watch?v=anVVU28RGYA) Terraform & Terratest

## Slidy k přednáškám

* [Infrastructure as Code, Part 1](//docs.google.com/presentation/d/110Qutjv7kdzyBQXecC7gJGkuyVZ2-EXlkfK3KtrIBB8/edit?usp=sharing) Intro & OCI
* [Infrastructure as Code, Part 2](//docs.google.com/presentation/d/1jcY42SOjvbJ1yUK8lgWicq1gXK85Ij9YnQciBdSl0yw/edit?usp=sharing) IaC & Terraform
* [Infrastructure as Code, Part 3](//docs.google.com/presentation/d/1IoqiRQkrPZVioQWWb_w8NNNgnTCxim79K4yDOFLNHfQ/edit?usp=sharing) Terraform & Terratest
