+++
title = "Cesta samuraje, rok devátý"
date = 2020-05-22T12:12:03+02:00
description = """Člověk se ani nenaděje a už jsou tady další narozeniny.
    Tentokrát to pojmu trochu jinak a nebudu psát o osobních věcech, ale
    podívám se statisticky trochu zpět. Heslo dne? Tagy & grafy."""
tags = ["sw-samuraj"]
hero_image = "hero.jpg"
+++

{{< figure class="floatleft" src="/img/samurai.gif" >}}

Rok se s rokem sešel a blog _SoftWare Samuraj_ slaví už 9. narozeniny. Dneska
to pojmu trochu jinak, než bylo mým zvykem a podívám se ne na svůj uplynulý
rok, ale na historii blogu. To se dá pojmout různě --- třeba někdo by se blýsknul
statistikami návštěvnosti (ale tím já se pyšnit nemohu), jiný by nabídl
sofistikovanou data science (v tom jsem sotva začátečník).

Ne, já to vezmu jednoduše --- pár základních součtů a agregací, proložených
barevnými grafy. Třeba i tak z toho něco zajímavého vyleze (i když, spíš ne).
Konec konců, tyhle bilanční články pro mne byly vždycky hlavně retrospektivou.

## Suma sumárum

Začnu tím, že za těch devět let jsem napsal 146 článků. Jak život šel, někdy
jsem psal víc, někdy míň a někdy jsem psát (téměř) přestal.

{{< figure src="/2020/05/Articles-per-year.png"
    link="/2020/05/Articles-per-year.png"
    caption="Počet článků za rok" >}}

Z hlediska témat, mých historií daných _Top 10_ tagů je:

1. Programovací jazyk [Clojure](/tags/clojure/). 🤗
1. Recenze a reference na [knihy](/tags/knihy/).
1. Věci kolem [Javy](/tags/java/).
1. Záležitosti a starosti [Teamleadingu](/tags/teamleading/).
1. Široká škála věcí, které spadají pod pojem [Software Engineering](/tags/sw-engineering/).
1. [Interview](/tags/interview/) a pohovory.
1. Různé nástroje na [Version Control](/tags/version-control/).
1. Různé nástroje na [build](/tags/build/) softwaru.
1. [Messaging](/tags/messaging/).
1. [SOA](/tags/soa/).

{{< figure src="/2020/05/Top-10-tags.png"
    link="/2020/05/Top-10-tags.png"
    caption="Témata článků v Top 10" >}}

Samozřejmě, _Top 10_ témat je jen špička ledovce. Za ta léta jsem nasbíral a
utřídil nějakých 66 tagů. Čas od času je trochu zrefaktoruju, ale i tak je jich
docela dost:

{{< figure src="/2020/05/Tag-cloud.png"
    link="/2020/05/Tag-cloud.png"
    caption="Tag cloud" >}}

## Jak šel čas

Je přirozené, že témata se v průběhu času měnila. Když vezmu _Top 3_
za jednotlivé roky, vypadala situace takto:

{{< figure src="/2020/05/Top-3-annually.png"
    link="/2020/05/Top-3-annually.png"
    caption="Top 3 témata za jednotlivé roky" >}}

Pokud vezmu v potaz, že jsem k danému tématu napsal aspoň 7 článků (což je shodou
okolností i kritérium pro _Top 10_), bude zajímavý vidět časový trend:

{{< figure src="/2020/05/Top-5-trend.png"
    link="/2020/05/Top-5-trend.png"
    caption="Trend pro Top 5" >}}

{{< figure src="/2020/05/Next-Top-5-trend.png"
    link="/2020/05/Next-Top-5-trend.png"
    caption="Trend pro Next Top 5" >}}

## Delfská věštírna

Už léta letoucí se v [Cestách Samuraje](/tags/sw-samuraj/) vizionářsky
zahledím do budoucnosti... 🧐 abych se většinou mýlil. A neodpustím si to
ani tentokrát. 🤭 Takže, o čem bych rád v nadcházejícím roce psal?

O [Golangu](/tags/golang/) --- začínáme psát nový produkt a velká část
v něm bude napsaná v _Golangu_. 🤗 Na to už se moc těším.

O _Kubernetes_ --- stejný projekt, spousta objevování. 🤯

O [Infrastructure as Code](/tags/infrastructure-as-code) a [Terraformu](/tags/terraform/)
--- už druhým rokem jsme měli s kolegou seminář na _MatFyzu_ a zároveň se
snažíme IaC implementovat v rámci našich projektů.

O [teamleadingu](/tags/teamleading/) --- i když se teď vedení týmu/ů nehodlám
nějaký čas věnovat, mám tady pár nedořešených restů. 😈

{{< figure src="/2020/05/Future-tag-cloud.png"
    link="/2020/05/Future-tag-cloud.png"
    caption="Budoucí témata? 🤔" >}}

## Související články

* [Cesta samuraje, rok osmý](/2019/06/cesta-samuraje-rok-osmy/)
* [Cesta samuraje, rok sedmý](/2018/05/cesta-samuraje-rok-sedmy/)
* [Cesta samuraje, rok šestý](/2017/06/cesta-samuraje-rok-sesty/)
* [Cesta samuraje, rok čtvrtý](/2015/05/cesta-samuraje-rok-ctvrty/)
* [Cesta samuraje, rok třetí](/2014/06/cesta-samuraje-rok-treti/)
* [Cesta samuraje, rok druhý](/2013/05/cesta-samuraje-rok-druhy/)
* [Cesta samuraje, rok první](/2012/04/cesta-samuraje-rok-prvni/)
