+++
title = "Nový blog živě ve 30 minutách"
date = 2020-03-20T18:37:50+01:00
description = """Založil jsem si nový blog a od rozhodnutí začít do chvíle
    kdy byl blog online, včetně domény, mohlo uběhnout 30 minut. 😎
    Dobře, kecám. 🤭 V některých věcech jsem puntičkář, takže další
    věci (jako CSS) jsem ladil dalšího půl dne. 😇"""
tags = ["blogging"]
hero_image = "hero.jpg"
+++

{{< figure src="/2020/03/Hugo-GitLab-Render.png" link="/2020/03/Hugo-GitLab-Render.png" >}}

Už nějaký čas jsem si pohrával s myšlenkou založit si takový "blogovací
občasníček". Jelikož jde obsahem o netechnické téma, tak to tady nebudu
rozmazávat a podívám se jenom na technologické aspekty: co to znamená,
založit si v dnešní době blog.

## Blog ve 30 minutách

Vždycky, když vidíte podezřele malé číslo, má to svoje _ale_. Nejinak je
tomu tady. Pokud však máte určité předpoklady (znalosti, zkušenosti,
nástroje atd.), tak to číslo je reálný. Samozřejmě, počítáme s tím,
že to dělá profík. 💪 (Takže doma to raději nezkoušejte. 🤭)

### Definition of Done

Aby bylo jasné, o čem se bavíme --- co by (po těch hypotetických) 30
minutách mělo být hotovo:

* blog by měl být veřejně online,
* měl by mít svoji doménu,
* měl by být zapezpečený přes SSL.

A jen pro úplnost --- bude se jednat o [Jamstack](//jamstack.org/).

_A všechno by mělo být zdarma!_ (Teda, kromě domény. 😈)

### Předpoklady

Abychom se vešli do zmíněného limitu, je potřeba mít svoji dílnu vybavenou
nutnými nástroji --- pokud nám něco z toho chybí a budeme si to teprve
zařizovat, tak nám celkový čas odpovídajícím způsobem naroste.

Takže, budeme potřebovat:

* účet u nějakého registrátora domény,
* účet na nějaké public _Git_ repository,
* učet u nějakého poskytovatele [CDN](//en.wikipedia.org/wiki/Content_delivery_network).

Co se týká znalostí a praxe je potřeba:

* umět pracovat s _Gitem_ na úrovni běžného kontributora (tj. `clone`,
  `commit` & `push`),
* umět pracovat s nějakým [Static Site Generator](//www.staticgen.com/)em.

### Nástroje

Výběr mých nástrojů je čistě subjektivní, založený na dobré zkušenosti,
osobních preferencích a minimálním _research_. Všechny jsou "rock solid"
a do velké míry zaměnitelné.

Začnu _statickým generátorem_. Blog [SoftWare Samuraj](//sw-samuraj.cz)
píšu a generuju pomocí **[Hugo](//gohugo.io/)**. Je napsaný
v [Golangu](//golang.org/) (pro který mám slabost), je rychlý jak sviňa,
má vlastní _templates_ (taky v _Golangu_) a použití je triviální.
Zkrátka a dobře... pro nový blog jsem o ničem jiném ani neuvažoval.

Pokud volba _statického generátoru_ není důležitá, volba _Git_ repository
už může být spřažená s určitým poskytovatelem _CDN_. Pokud má váš
_statický generátor_ stejně dobrou dokumentaci jako _Hugo_ (viz
[Hosting & Deployment](//gohugo.io/hosting-and-deployment/)), není
na škodu si v tom udělat jasno.

Pro _Jamstack_ je totiž důležitá
[CI](//en.wikipedia.org/wiki/Continuous_integration)/[CD](https://en.wikipedia.org/wiki/Continuous_delivery),
která je buď integrovaná
v _Git_ repository, poskytuje ji _CDN_ provider, nebo je v některých
případech úplně externí. Já jsem zvolil jednoduchost a osvědčenost:
**[GitLab](//gitlab.com/)**. Jednoznačně nejlepší _CI/CD_ na trhu
([GitLab Pipelines](//about.gitlab.com/stages-devops-lifecycle/continuous-integration/))
a zároveň i podpora mnou zvoleného _CDN_ poskytovatele.

U _CDN_ jsem chtěl něco jednoduchého, co splní moje požadavky:

* custom domény,
* automatická podpora [Let's Encrypt](//letsencrypt.org/),
* a podpora daného _CI/CD_.

Nijak jsem to nezkoumal a vzal první vyhovující:
**[Render](//render.com/)**.

### Registrace domény

Tady to nebudu nijak detailně popisovat, protože jednak v tom nemám
přehled a jednak si myslím, že se jednotlivý registrátoři dost liší
v nabízených službách.

Můj registrátor má pro mne dvě stěžejní věci:

* novou doménu si můžu registrovat online a hned ji zaplatit kartou,
* umožňuje mi online spravovat DNS záznamy.

Nic víc nepotřebuju. Takže:

1. Ověřil jsem si dostupnost domény.
1. Doménu zaregistrovat a zaplatil.
1. Za 10-15 minut byla doména online. 👍

### Vygenerování lokálni site

Tady budu jenom parafrázovat, co říká _Hugo_ ve svém [Quick
Start](//gohugo.io/getting-started/quick-start/):

Vytvoření nové site:

```bash
hugo new site stepnibezec
```

Přidání nového tématu:

```bash
git init
git submodule add https://github.com/sw-samuraj/vienna.git themes/vienna
echo 'theme = "vienna"' >> config.toml
```

Vytvoření prvního článku:

```bash
hugo new 2020/stepni-bezec.md
```

Trochu poeditovat obsah (a odstranit z hlavičky proměnnou `draft: true`):

{{< figure src="/2020/03/First-post.png" link="/2020/03/First-post.png" >}}

A vyzkoušet, že se nám to správně vygeneruje:

```bash
hugo server
```

### Push to remote repository

Lokální _Git_ repository už jsme si inicializovali (v části _přidání
nového tématu_). Teď je potřeba všechny _Hugem_ vygenerované soubory
`push`nout do vzdálené repository.

Založíme si na _GitHubu_, nebo _GitLabu_ (obojí je podporováné _Renderem_)
novou repository a lokální změny tam nahrajeme:

```bash
git remote add origin git@gitlab.com:sw-samuraj/stepnibezec.git
git add .
git commit -m "Initial import"
git push -u origin master
```

### Založení statických stránek na Renderu

Pokud si čerstvě zakládáte účet na _Renderu_, jste vyzváni, ať si
slinkujete _GitHub_, či _GitLab_, či oboje.
Pak si vytvoříte _New Web Service_ typu _Static Site_. Všechno je
to intuitivní a na pár kliknutí. 👍

Potřebná konfigurace pak vypadá takto:

{{< figure src="/2020/03/Render-config.png"
    link="/2020/03/Render-config.png"
    caption="Render, konfigurace statických stránek" >}}

Velkou výhodou tady je, že _Render_ dělá _CI/CD_ out-of-the-box,
takže to nemusíte nijak dále konfigurovat.

### Přidání custom domény

Na stránce _Settings_ vaší nové služby ve _Renderu_ je dole sekce _Custom
Domains_. Opět je to intuitivní, ale kdyby se v tom někdo ztrácel, je to
přehledně [zdokumentované](//render.com/docs/custom-domains).

Jakmile přidáme u našeho registrátora domény potřebné DNS záznamy, ještě
se sem jednou vrátíme, abychom je potvrdili tlačítkem _Refresh_:

{{< figure src="/2020/03/Render-custom-domain.png"
    link="/2020/03/Render-custom-domain.png"
    caption="Render, konfigurace custom domény" >}}

### Přidání DNS záznamů

Tohle může být trochu oříšek --- sám to málokdy dám na první dobrou
a navíc, různí registrátoři poskytují různé možnosti ohledně DNS
záznamů (například ten můj neumožňuje přidávat `ANAME` a `ALIAS`
záznamy).

Takže, tady je to na vás. Ale trocha experimentování nikoho nezabije
(ani nic nerozbijete, zatím 😉). Možná vás bude trochu frustrovat,
než se DNS záznamy zpropagují na potřebné jmenné servery a vy si
budete moci ověřit svůj úspěch.

Ale nakonec to dobře dopadne... věřte mi.

{{< figure src="/2020/03/DNS-records.png"
    link="/2020/03/DNS-records.png"
    caption="DNS záznamy" >}}

### Čas otevřít šampáňo a sklízet benefity

Teď zadejte do browseru vaši zbrusu novou adresu a zmáčkněte stopky.
Je to tam! 29 minut a 48 sekund. 🤯 Nový blog je na světě!

## Opravdu jenom 30 minut? 🤔

Tak, a teď s pravdou ven --- opravdu mi to trvalo jenom 30 minut?
Čestný pionýrský? Odchovanci [woodcraftu](//en.wikipedia.org/wiki/Woodcraft_(youth_movement))
nelžou, takže...

Na blogu, který zde popisuju jsem strávil... půl dne. Protože některé
věci mi nedají pokoj, dokud nejsou správně tak, jak mají být:

* Ladil jsem [Hugo konfiguraci](//gohugo.io/getting-started/configuration/)
* Ladil jsem téma/šablonu:
  * udělal jsem si [fork](//github.com/sw-samuraj/vienna),
  * fixoval jsem to-be-depracated [Hugo funkce](//gohugo.io/functions/),
  * trošku jsem originál přiohnul svému vkusu,
  * pošteloval CSS,
  * vyrobil [favicony](//en.wikipedia.org/wiki/Favicon),
  * natvrdo jsem téma přepnul do čestiny,
  * fixnul jsem nefunkční odkazy na tagy,
* Založil jsem _Twitter_ účet: [@StepniBezec](//twitter.com/StepniBezec)
* Založil jsem _Facebook_ stránku: [@StepniBezec](//www.facebook.com/StepniBezec/)
* A napsal jsem první článek.

Nicméně to všechno už je takové provozní blogování. Ten čistý čas --- i když jsem
ho neměřil --- od prvního `hugo` příkazu, k zobrazení blogu na vlastní doméně,
bych opravdu odhadnul na zhruba těch 30 minut.

Ale o to ani nejde. Vlastně mě překvapilo a potěšilo, jak snadné
a rychlé je založit si vlastní blog, postavený a provozovaný
na moderních technologiích. 👍

## Související články

* [Migrace z Blogger na Hugo a GitLab Pages](/2018/11/migrace-z-blogger-na-hugo-a-gitlab-pages/)
* [Technologické blogování](/2012/11/technologicke-blogovani/)
