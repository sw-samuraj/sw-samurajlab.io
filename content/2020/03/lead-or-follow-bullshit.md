+++
title = "Lead or Follow? 🤔 Bullshit!"
date = 2020-03-06T17:18:10+01:00
description = """Jsou to dva roky, co jsem opustil team leaderské pozice
    a vrátil se k čistému programování. Nechal jsem téma dozrát a
    v průběhu času přemýšlel nad vhodnou formou. A mimochodem... není
    nad to, začít článek úderným titulem! 🤭"""
tags = ["teamleading"]
hero_image = "hero.jpg"
+++

{{< figure src="/2020/03/Ducks.jpg" link="/2020/03/Ducks.jpg" >}}

Jsou to dva roky, co jsem nastoupil do nové práce. Byl to výsledek dlouho
se kulminujících trendů a jednou z největších, či nejzásadnějších změn
bylo, že jsem po 10 letech přestal dělat team leadera a stal se (opět)
úplně obyčejným programátorem.

Udělal jsem ze své vůle krok, který většina lidí v kariéře nedělá.
Tedy... nedělá ho vědomě a dobrovolně.

{{< figure src="/2020/03/Twitter-hint.png"
    link="//twitter.com/Kojotak/status/1001830365579837440" >}}

## Když to někdo chce vidět černobíle 🤷‍♂️

{{< figure src="/2020/03/Zebra.jpg" link="/2020/03/Zebra.jpg" >}}

Existuje jeden letitý článek (r. 2005) od _Scotta Berkuna_ s názvem
[Why you must lead or follow](//scottberkun.com/essays/42-why-you-must-lead-or-follow/),
který může být pro leckoho inspirativní:

> The dictum **lead or follow** means you have to decide for yourself where
  you stand. ~ Scott Berkun

Ten článkek není špatný --- pokud člověk odolá _Scottovým_ imperativům, může
někoho ponouknout k tomu, nebát se vzít na sebe (i jen dočasně) vůdčí úlohu,
aby posunul věci k lepšímu. To se cení.

Podobné články (a jejich uvádění v praxi) sebou nese dva problémy:

* Jednak, ty drobné, cenné rady zůstanou skryty a přehlušeny úderným sloganem.
  (Což pomáhá čtenosti, ale nové, inspirativní leadery nevytvoří.)
* Druhak, ačkoliv _Scott Berkun_ má do věcí pronikavý vhled a často je
  inspirativní... on nežije to, co káže: _Scott_ je totiž spisovatel a
  přednášející ([Speaker for Hire](//scottberkun.com/speaking/)). Sice
  za mlada pracoval jako team leader v _Microsoftu_, ale zas tolik praxe
  nemá.

Tím chci říct, že věci je potřeba brát v kontextu. Nicméně chápu, proč takový článek,
takové téma v lidech rezonuje --- teamleading je složitý, namáhavý, často
vyčerpávající. A tak leckdo vezme za vděk práškem na bolest, či zkratkou.

Svět samozřejmě není černobílý. Mimochodem, zajímalo by mě, co na to sám
_Scott_ říká s odstupem let.

## Proč ne leader

{{< figure src="/2020/03/Lion.jpg" link="/2020/03/Lion.jpg" >}}

Když jsem před lety začal aspirovat na roli leadera (to bylo ještě předtím,
než jsem začal psát tenhle blog), dělal jsem to hlavně proto, že jsem
chtěl mít na věci vliv. Věci jako technologie, architektura, project
management, složení týmu atd.

Když někdo dělá něco s přesvědčením a (většinou) dobře, lidé okolo vás si
toho všimnou. A ti, co o tom rozhodují vám na základě referencí dají
na starost větší koláč. Tak to na světě chodí.

Když to děláte pár let (a máte na starosti pořád větší koláče) přijde
určitá saturace. A vy se musíte rozhodnout, co dál --- chci na vyšší
level? Chci dělat to co dělám, ale ještě lépe? Zkusím něco jiného?

Já jsem z leadingu odešel, protože na vyšší level jsem nechtěl
a to co jsem dělal se po těch 10 letech stávalo rutinou a zároveň
přicházela únava.

Vytvořil jsem několik týmů a stěžejní pro mne vždycky bylo, založit
a udržet určitou kulturu. Když to děláte po několikáté a téměř vždycky
začínáte od nuly a spoustu lidí učíte znova a znova základní hygienické
návyky, začne si to vybírat daň. Já jsem se rozhodl neupadnout do rutiny
a marastu a vydat se někam jinam.

Ale to, co jste se jako leader naučili, to už vám nikdo nevezme. Ale
neoddělíte to od sebe ani vy sami --- stalo se to integrálně vaší součástí.

## Proč ne follower

{{< figure src="/2020/03/One-way-street.jpg" link="/2020/03/One-way-street.jpg" >}}

Výše už jsem naznačil cosi o černobílosti základní teze. Ono problém
s followery je... že to je nedefinovaný, nediferencovaný guláš.

* Vezměte si juniora (po škole) --- následuje, protože své práci (ještě)
  nerozumí a tak vykonává magické úkony, které mu zjevil seniornější kolega.
* Vezměte si zkušeného seniorního matadora, jehož manažer se může zjančit,
  ale on si to stejně udělá po svém.
* Vezměte si technického leadera, který nechá svého project manažera
  skrze tvrdou realitu doiterovat k předvídatelnému výsledku.
* Vezměte si desítky různých případů a kolegů, které jste ve své
  softwarové praxi zažili --- jestli v nich vidíte followery, zkuste
  se nad sebou zamyslet, jestli se vám nezužuje vidění.

To jen tak na okraj k tomu _"... or follow"_. 🙄

Takže i když podle _Scotta Berkuna_ nyní (opět) spadám do té druhé kategorie,
tak nejenže se jako _follower_ necítím. Ale fakticky jím ani nejsem.
Ono v praxi je jedno, jestli dělám kompromisy se svým šéfem, nebo
se svými kolegy. Jestli architekturu vymyslel team leader, nebo
ten autistický "matfyzák" v koutě. Jestli...

V reálném životě je důležité, jestli jsem si:

* správně vybral práci,
* správně vybral tým,
* správně vybral šéfa (protože každý --- kromě ředitele zeměkoule
  -- má nad sebou šéfa)

a mít dostatek sebedůvěry a sebereflexe, abych změnil a ovlivnil
věci, které ovlivnit a změnit lze.

Pokud budu mluvit za sebe, moje team leaderská minulost mi v současnosti
dává větší vhled do věcí --- interní fungování týmu, dynamika softwarového
projektu, vztahy s externími týmy, politika (a politikaření) na pracovišti,
vztahy se zákazníky a všechny tyhle věci, které běžní programátoři
zhusta blaženě ignorují.

Mě to pomáhá a obohacuje mě to. Ovšem nedělá to ze mne lepšího programátora.
Je to cesta, kterou jsem si zvolil a která mi vyhovuje. Ale programovat
--- v rámci měnících se stupňů volnosti --- musím jen já sám.

## Související články

* [Jak se staví tým](/2018/02/jak-se-stavi-tym/)
* [Technical Leader, mytické stvoření](/2017/10/technical-leader-myticke-stvoreni/)
* [Programátor -> Vývojář -> Software Engineer](/2017/02/programator-vyvojar-software-engineer/)
* [Smutná zpráva o stavu IT trhu](/2017/09/smutna-zprava-o-stavu-it-trhu/)

## Externí odkazy

* [Why you must lead or follow](//scottberkun.com/essays/42-why-you-must-lead-or-follow/)
