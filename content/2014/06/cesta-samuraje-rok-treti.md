+++
title = "Cesta samuraje, rok třetí"
date = 2014-06-30T22:52:00Z
updated = 2014-06-30T22:52:27Z
description = """Blog SoftWare Samuraj má třetí narozeniny. Tradiční retrospektiva."""
tags = ["sw-samuraj"]
aliases = [
    "/2014/06/cesta-samuraje-rok-treti.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/img/samurai.gif" >}}

Uběhl třetí rok a samurajský meč je stále ostřejší. Jen poslední dobou nějak zahálí. Má smysl brousit měč, když se pak nikdy nepoužije? Někdy je to možná lepší. Ale dost planého filozofování, [panta rhei](//en.wikipedia.org/wiki/Heraclitus#Panta_rhei.2C_.22everything_flows.22).

## Výpadek psaní

V uplynulých měsících jsem toho moc nenapsal. Poslední čtyři měsíce loňského roku vůbec nic a za celý letošek jsem se dopracoval ke čtyřem článkům. To je velmi tristní výsledek. Má to svoje důvody a jak to tak bývá, sešlo se jich víc dohromady. Čím to bylo?

V první řadě, loni v létě jsem začal po čase opět běhat a v zápětí nato jsem se (v období přechodného sebevědomí) přihlásil na [Pražský maraton](//www.runczech.com/cs/akce/volkswagen-maraton-praha-2014/zavody/volkswagen-maraton-praha/index.shtml). Do té doby to pro mne byl nepředstavitelný cíl, ale říkal jsem si, že když na tom budu tvrdě pracovat, tak to zvládnu.

V rámci přípravy jsem naběhal tisíc tréninkových kilometrů, které sice zajistily, že jsem maraton v květnu bez problémů zaběhnul (a užil si to), ale také to ukrojilo pořádný kus mého volného času. Času, ve kterém jsem dříve psal.

Dalším, byť minoritním, důvodem ne-psaní je [Gradle tutorial](/tags/gradle), který jsem začal loni psát. Chtěl jsem trochu diverzifikovat své čtenáře a tak jsem se domluvil se serverem [Zdroják](//www.zdrojak.cz/), že bych publikoval [primárně u nich ](//www.zdrojak.cz/serialy/gradle-moderni-nastroj-na-automatizaci/)a pak teprve u sebe na blogu.

Měl jsem tehdy vypracovaný pravidelný rytmus tří článků za měsíc, který vyplýval z toho, že jsem pravidelně psal. Bohužel, nepředvídatelnost publikování na Zdrojáku mi tento rytmus rozhodila, což ovlivnilo i pravidelnost psaní --- ve výsledku to totiž znamenalo napsat o jeden článek měsíčně navíc. Což jsem chvíli zkoušel, ale nefungovalo to, bylo to zkrátka už moc.

Posledním důvodem je, že jsem de facto neměl doma k dispozici notebook na psaní. Sdílet v rodiné konstalaci notebook je totéž, jako se prát o ovladač k televizi. A jelikož já svým psaním na blog peníze nevydělávám, bylo o vytížení notebooku rozhodnuto.

Bylo by hezké říct, že jsem tyto problémy a výzvy vyřešil a nyní budu zase psát jako dřív. Částečně ano, ale co se počítá, jsou hotové, dodělané věci, takže uvidím. Rád bych se dostal do tempa dva články za měsíc, tak mi držte palce.

## Práce

Hodně článků, které jsem napsal, nějakým způsobem kolidují s mojí prací. A protože třetí rok Software Samuraje se kryje se změnou zaměstnání, rád bych se ohlédl i zde.

Když jsem před rokem hledal novou práci, chtěl jsem něco, kdy bych mohl skloubit tři věci, které mne baví: team leading, SW architekturu a Java development. Štěstí se na mne usmálo a přesně takovou práci jsem našel. A po roce jsem stále spokojený a moc se těším na ten další. Kdyby vás zajímalo, o co jde, sepsal jsem o tom článek [Hledám do svého týmu Java vývojáře](/2013/07/hledam-do-sveho-tymu-java-vyvojare.html), článek je pořád aktuální.

Z témat bych vypíchnul:

**Team leading:** mám dosud nevídaný luxus --- můžu si sestavit svůj vlastní vývojářský tým. Už vybírám víc jak rok a zatím jsme na osmi lidech. Už dlouhodobě o tom plánuju sepsat článek, řekl bych, že bude hodně zajímavý :-)

Jako team leader jsem si vyzkoušel nové věci a zlepšil ty stávající. Třeba mentoring, 1:1 pohovory a retrospektivy.

**Teamová kultura:** i tady jsem vyhrál loterii, je to asi nejlepší pracovní prostředí, kde jsem jako vývojář pracoval. A mám skvělého šéfa.

**Hiring:** technical recruitment mě baví. A rád bych o tom napsal pár článků. Za ten rok jsem absolvoval desítky pohovorů a je to interesantní materiál.

**Cestování:** za poslední půl rok jsem pracovně navštívil tři kontinenty (Evropa, Asie, Afrika) a brzy to bude asi Amerika. Cestování mě baví a je to výborná zkušenost (ne vždy pozitivní ;-)

**Metodiky:** nějaké metodiky už u nás máme. A já mám možnost je vylepšovat a zavádět nové. Stal jsem se tak trochu [Kanban](//en.wikipedia.org/wiki/Kanban_(development)) evangelistou. A to je taky vděčné téma ke psaní.

## Budoucnost

Co přinese další rok? Chtěl bych se víc věnovat psaní. Dva články měsíčně.

Chtěl bych psát o _Kanbanu_. Praktické zkušenosti (aktuálně máme za sebou další projekt), i víc osvětové a teoretické věci.

V práci používáme na verzování [Mercurial](//mercurial.selenic.com/). Moc se mi ten nástroj líbí, takže nějaký ten článek by mu slušel.

[Gradle](//www.gradle.org/). Můj hřích --- chtěl bych pokračovat v tutorialu (a dokončit ho). A taky udělat evangelizaci u nás v práci.

Technical recruitment: chtěl bych napsat o vytváření týmu. A chtěl bych (opět) napsat o tom, jak dělám Java pohovory.

## Mind Map

[Myšlenkové mapy](//en.wikipedia.org/wiki/Mind_map) používám už dlouho, léta. Je to skvělý nástroj. Teď zrovna mám období znovu oživeného zájmu o ně a napadají mě další způsoby jejich použití. Třeba připravit si pomocí mind mapy strukturu článku. Je to docela zajímavý proces. Tak si zkusím zaexperimentovat a tyhle přípravné mapy k daným článkům publikovat.

{{< figure src="/2014/06/SW-Samuraj-R3.jpg"
    link="/2014/06/SW-Samuraj-R3.jpg" >}}

## Související články

* [Cesta samuraje, rok druhý](/2013/05/cesta-samuraje-rok-druhy/)
* [Cesta samuraje, rok první](/2012/04/cesta-samuraje-rok-prvni/)
