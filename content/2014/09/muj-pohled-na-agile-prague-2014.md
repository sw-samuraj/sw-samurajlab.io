+++
title = "Můj pohled na Agile Prague 2014"
date = 2014-09-17T21:28:00Z
updated = 2016-10-19T17:06:36Z
tags = ["konference", "agile"]
description = """Byl jsem na konferenci Agile Prague. Bylo to poprvé a hned tak se
    tam nevrátím. Ne, že bych své účasti litoval, ale celkový dojem z konference
    mám rozpačitý - pro koho je vlastně určena?"""
aliases = [
    "/2014/09/muj-pohled-na-agile-prague-2014.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2014/09/Agile-Prague.png" >}}

Byl jsem na konferenci [Agile Prague](//agileprague.com/). Bylo to poprvé a hned tak se tam nevrátím. Ne, že bych své účasti litoval --- našel jsem si tam pár zajímavých myšlenek a odkazů na dodatečné zdroje. Ale celkový dojem z konference mám rozpačitý --- pro koho je vlastně určena?

## Shrnutí

Můžu se krutě mýlit, ale podle obecenstva bych odhadoval, že tak 70-90 % byli SW inženýři, s převahou vývojářů. Většina z nich buď už má zkušenost s agile, nebo ví o co jde, nebo aspoň po agilitě touží. Pro tuhle kategorii, obávám se, konference mohla nabídnout stěží něco nového, něco inspirativního. Teda kromě "kmenové družby". Sám za sebe bych to shrnul následujícím tweetem:

{{< tweet 511799127290966016 >}}

Přišlo mi, že většina přednášek by byla nejvíc přínosná pro kategorii, která na konferenci zcela/téměř absentovala --- střední manažment. Nevím, čím to bylo, ale skoro všichni řečníci se drželi na velmi abstraktní rovině. Pořád se mluvilo o problémech, které agile adresuje, ale jen minimálně o konkrétních řešeních. A hlavně, srovnávat (opakovaně!) v roce 2014 (13 let po [Agilním manifestu](//agilemanifesto.org/)!) vodopád s agilním vývojem... hm, mlácení prázdné slámy?

## Co se mi nelíbilo

_V následující sekci nebudu psát pozitivní věci --- chtěl bych proto předeslat: jde o můj, striktně subjektivní pohled. Pokud něco kritizuji, tak proto, že to nedosáhlo mých očekávání. Nemusí to ale říkat nic o skutečné, nebo obecně přijímané kvalitě. Jsem někdy [solipsista](//en.wikipedia.org/wiki/Solipsism)... už od dětství._

_A směrem k pořadatelům: jsem rád, že takovouto konferenci pořádáte. Vím, že je za tím spousta práce a je fajn mít něco takového v Praze._

### Agile != Scrum

Tenhle pocit už mám dlouhodobě. Když "běžní" lidi mluví o agile --- a řečníci na konferenci to zhusta autorizovali --- tak zazní něco (byť ne tak explicitně) jako:

> "Být agile" = "dělat" Scrum

Zajímalo by mě, jestli máte jinou zkušenost? Já když poslouchám diskuze o agile, tak cca 80 % času/prostoru je věnováno klábosení o Scrumu.

Jen výjimečně (a díky za to) zaznělo:

> **Individuals and interactions** over processes and tools

### Case Studies

[Case studies](//en.wikipedia.org/wiki/Case_study) jsem viděl dvě (Home Credit a Skype) a obě patřily k (informačně) nejslabším příspěvkům na konferenci. _Když jdu na case study, tak očekávám, že se dozvím něco z toho, **"jak to dělali"**_. Nezajímá mě historie a struktura firmy. A když je na talk 20 minut, tak mě nezajímá ani kontext, v jakém to probíhalo --- není na to čas. Chci vědět, jak to dělali! To jsem se ani z jedné case study nedozvěl.

Co si z toho odnáším, tak že v Home Creditu měli problém telefonovat do Asie (technické problémy) a ve Skypu použili na odhady story points, které měly konkrétní velikost (hodiny a dny --- takže už jaksi nešlo o relativní, ale absolutní odhady). To asi není to, co řečníci zamýšleli předat publiku.

### Zaměření, pointa přednášek

Možná jsem přespříliš kritický, ale poučen [Scottem Berkunem](//www.amazon.com/Confessions-Public-Speaker-Scott-Berkun-ebook/dp/B002VL1CGM) očekávám, že přednáška bude mít nějakou pointu, myšlenku, kterou chce sdělit. O to spíš, že trvá pouze 20, 30, max. 45 minut.

Přiznám se, občas jsem se musel během prezentace opakovaně podívat do programu na název přednášky, abych si připomenul o čem je téma. A párkrát jsem se usilovně snažil do posledních sekund nalézt pointu toho, co jsem zrovna půl hodiny poslouchal.

### Nekonkrétnost

Několikrát jsem na přednáškách zaslechl rady jako: _"We should embrace the Agile Manifesto!", "We need to find leaders!"_ apod. Aspoň jak jsem to pochopil, nebyly to mobilizační výkřiky --- byly to vážně míněné rady, jak řešit předeslané problémy. Ehm, jak? To je to, proč jsem přišel na tuhle přednášku, konferenci. Jak se to dá udělat? Nebo alespoň, kde se mám inspirovat, kde si to můžu dostudovat? Nevím. Nevím, v těchto otázkách víc, než jsem věděl před konferencí/přednáškou.

### Bonding

Nejsem [mizantrop](//en.wikipedia.org/wiki/Misanthropy), jsem jen bytostný introvert. A nesnáším, když mě někdo z pozice autority (řečník, nebo pořadatel) nutí dělat věci, které jsou mi jako introvertovi nepříjemné. Což je třeba diskuze ve veřejném prostoru s cizími lidmi. Nebo se ptát sponzora konference na heslo k wifi!?!?

### Wifi

Myslím si, že je v dnešní době [faux pas](//en.wikipedia.org/wiki/Faux_pas), když není na konferenci k dispozici slušná wifi... pro všechny. Aby to bylo jasné: heslo na wifi pro prvních 100 (z cca 250), není _pro všechny_. A to tím spíš, pokud poskytnu program konference v elektronické verzi, která vyžaduje být online.

Ano, můžou být legitimní důvody, proč není wifi připojení uspokojivé, či dostačující. Pak je ale férové to říct narovinu. A ne žoviálně odtušit _"better be quick"_. Failure.

## Co se mi líbilo

### Linda Rising

Jednoznačnou hvězdou konference pro mne byla [Linda Rising](//www.lindarising.org/) ([@RisingLinda](//twitter.com/RisingLinda)). Tahle agilní babička (72 let!!) doručila jedinou inspirativní keynote celé konference a i její druhá přednáška byla velmi pěkná.

Její keynote měla název _The Power of the Agile Mindset_ a dala by se zhustit do věty _Na počátku bylo slovo_. Celé to bylo o tom, jakou úžasnou schopnost máme k dispozici --- jedinou větou můžeme určit trajektorii skupiny na dlouhou dobu a doširoka rozevřít nůžky mezi úspěšnými a neúspěšnými. Mezi těmi, kteří hledají řešení a těmi, kdo hledají chyby a viníky. Mezi těmi, kdo na sobě pracují a kdo o sobě lžou (aby vypadali lépe). A mezi agilním a fixním mindsetem.

### Paul Klipp

[Paul Klipp](//paulklipp.com/blog/) byl jediný řečník, kterého jsem před konferencí znal. Těšil jsem se na něj a nezklamal mě. Ba co víc, překvapil mě. Jeho přednáška se jmenovala _Improve your communication skills dramatically without saying a word_. Jako jediný speaker mluvil bez slidů(!) a součástí byla i 3minutová meditace(!!), která však do přednášky organicky zapadla.

Je zbytečné, abych tady přepisoval a parafrázoval Paulovu myšlenku --- doporučuji přečíst si jeho blogpost [Improve your communication skills by learning to listen](//paulklipp.com/blog/improve-your-communication-skills-by-learning-to-listen/), nebo se aspoň podívat na přípravnou [mind mapu](//paulklipp.com/images/listeningmm.pdf).

### Organizace jako doprava

Zábavná a osvěžující byla přednáška [Hanse Brattberga](//blog.crisp.se/author/hansbrattberg) _Traffic, Organizations and Lean_, kde přirovnával fungování firmy (a hlavně přístup k projektům) k dopravním situacím. Křižovatka v Hanoi, kruhový objezd na Champs-Élysées, šestiproudá dálnice, couvání v jednosměrce atd.

Každé srovnání kulhá. Ale tady nešlo o nalezení analogie, ale o zobrazení určitých zákonitostí pomocí nadsázky. Třeba že couvání v jednosměrce je vlastně podvádění (protože se nám nechce objíždět blok). Na druhou stranu, jak _Lean_, tak doprava pracuje s flow, kapacitou apod., takže něco podobného tam bude.

### Produktové flow nestačí

I když osobně nepovažuju Kanban za (striktně) agilní záležitost, přece jen mě dost překvapilo, že na konferenci o něm nebylo vůbec nic! Nejvíc se mu blížila přednášky [Martina Burnse](//everydaylean.info/author/martin/) _Product Flow Is Not Enough!_. De facto to bylo o Kanbanu, i když, jestli si dobře vzpomínám, tak slovo Kanban tam ani jednou nezaznělo. Nebo to bylo o Lean? ;-)

Martin měl taky velmi pěkné, ručně malované slidy, které bohužel nejsou (zatím?) dostupné. Můžete je ale vidět [v jeho přednášce](//www.youtube.com/watch?v=KBiln9WKemw) na YouTube. (V Praze mluvil o moc líp = plynuleji, ale Kanban fakt neřekl.)

### Total Recall, eh, Terraforming

Další (nejen) zábavná přednáška byla od [Caludia Perroneho](//agilesensei.com/): _Terraforming Organisations: Journey of a Lean Changer_. Tahle přednáška hodně vycházela z Toyoty a Lean, plus _A3 Thinking_, ale směřovala k originálním nástrojům (["myslící" karty](//agilesensei.com/deck/) a Popcorn flow, viz slidy níže).

Nicméně, to co mi hlavně utkvělo v hlavě, je slide kdesi z prostředka prezentace:

{{< figure src="/2014/09/Enterprise-agile.png" link="/2014/09/Enterprise-agile.png" width="800" >}}

* SlideShare: [Terraforming organisations](//www.slideshare.net/cperrone/terraforming-organisations-39150169") from [Claudio Perrone](//www.slideshare.net/cperrone")

Krásně to ilustruje můj pocit z většiny implementací Scrumu, co jsem jich v enterprise viděl --- vývojáři si vesele "scrumují" a nad nima je zabetonovaný klasický hierarchický manažment.

### #NoEstimates

Na _#NoEstimates_ jsem poprvé narazil v knize [Kanban in Action](//www.manning.com/hammarberg/). Je to zajímavá myšlenka, s Kanbanem se velmi dobře pojí a řečník [Vasco Duarte](//softwaredevelopmenttoday.blogspot.de/) ji přednesl srozumitelně. Čím se Vasco, bohužel, moc nezabýval --- jak to udělat na začátku projektu. Studijními zdroji taky zrovna neplýtval --- prostě to stavěl jako takové postuláty. A publikum bylo asi dost zaskočené, protože se na to taky nikdo nezeptal.

### Catering

Ruku na srdce --- dobré jídlo ke konferenci patří a já jsem byl spokojený. Obědy byly chutné, dortíky dobré, kafe (na konferenci) slušné, džusů dostatek, nemůžu si stěžovat :-)

## Co jsem si odnesl

Ačkoliv jsem šel na agilní konferenci, tak to, co jsem si odnesl, se netýká agile, ale leadershipu --- víc na sobě pracovat, jako [servant leader](//en.wikipedia.org/wiki/Servant_leadership). Takže z tohohle pohledu jsem spokojený. Ale z pohledu agile jsem zklamaný --- vzhledem ke své pozici mám v práci (omezenou) možnost prosadit určité věci, nastavit kulturu, doporučit nástroje apod. A bohužel, v tomto směru si z _Agile Prague 2014_ neodnáším žádnou inspiraci.

## Související externí články

* [Agile Prague 2014](//blog.krecan.net/2014/09/16/agile-prague-2014/) (Java crumbs)
