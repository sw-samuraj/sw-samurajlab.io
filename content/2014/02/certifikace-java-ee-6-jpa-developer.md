+++
title = "Certifikace Java EE 6 JPA Developer"
date = 2014-02-02T21:53:00Z
updated = 2014-02-02T21:53:59Z
description = """Absolvoval jsem Java EE 6 JPA Developer certifikaci.
    O čem to je, co jsem studoval a co mi to přineslo?"""
tags = ["oracle", "java", "certifikace", "knihy"]
aliases = [
    "/2014/02/certifikace-java-ee-6-jpa-developer.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2014/02/OCE-JavaEE6JavaPersistAPIDev.gif" >}}

Sbírám certifikace a navlíkám je na nit, jako korálky. Už je jich pěkná šňůra. Tak nějak mě to i baví. A člověk si přitom přečte spoustu zajímavých knížek. Tuhle jsem si zase říkal, co mi tak ještě chybí ve sbírce? Ahá! [Oracle Certified Expert, Java EE 6 Java Persistence API Developer](//education.oracle.com/pls/web_prod-plq-dad/db_pages.getpage?page_id=458&amp;get_params=p_track_id:JEE6JP), to bude něco pro mne.

## Trocha historie

[JPA](//en.wikipedia.org/wiki/Java_Persistence_API) vychází historicky z [Entity Bean](//en.wikipedia.org/wiki/Entity_Bean), které byly původně součástí [EJB](//en.wikipedia.org/wiki/Enterprise_JavaBeans) specifikace. V _Java EE 5_ byla sice už definovaná první verze JPA, která ale pořád tak nějak zůstávala součástí EJB. V aktuální verzi _Java EE 6_ už jsou EJB 3.1 a JPA 2.0 dvě samostatné technologie (byť stále úzce kooperující).

Tomuto vývoji odpovídají i certifikace: pro _Java EE 5_ byla k dispozici pouze certifikace [Business Component Developer](//education.oracle.com/pls/web_prod-plq-dad/db_pages.getpage?page_id=458&amp;get_params=p_track_id:JEE5BCD), která obsahovala jak EJB 3.0, tak JPA 1.0. Pro _Java EE 6_ už jsou to dvě oddělené větve --- [Enterprise JavaBeans Developer](//education.oracle.com/pls/web_prod-plq-dad/db_pages.getpage?page_id=458&amp;get_params=p_track_id:JEE6JPE) a [Java Persistence API Developer](//education.oracle.com/pls/web_prod-plq-dad/db_pages.getpage?page_id=458&amp;get_params=p_track_id:JEE6JP).

A právě tu posledně jmenovanou certifikaci jsem čerstvě absolvoval (jak říkám, sbírám to jak houby :-)  tak bych se rád podělil o své studijní úsilí. Ještě než se k tomu dostanu, předestřu pár informací o certifikaci samotné.

## Certifikace v kostce

Jako u všech certifikací téhle úrovně, jedná se o test na počítači v nějakém testovacím středisku. Pokud vás zajímají věci jako cena, trvání, _passing score_, nebo detailnější rozpis témat, můžete se podívat na [stránku zkoušky](//education.oracle.com/pls/web_prod-plq-dad/db_pages.getpage?page_id=5001&amp;get_params=p_exam_id:1Z0-898&amp;p_org_id=&amp;lang=). Já bych uvedl jen hrubší přehled témat:<br />

* Configure and package JPA Entity classes in enterprise modules
* Create basic JPA Entity classes
* Design a Java application that uses a RDMS
* Ensure data integrity by using Transactions and Locking
* Java Persistence Criteria API
* Java Persistence Query Language
* Optimize database access
* Use JPA relationships
* Use advanced JPA features
* Utilize Entity classes by creating code that uses the EntityManager API

Ne všechny témata jsou zastoupena rovnoměrně --- tak nějak subjektivně mi přijde, že velký důraz se kladl na věci jako:<br />

* používání [Criteria API](//docs.oracle.com/javaee/6/tutorial/doc/gjitv.html)
* mapování [Map](//docs.oracle.com/javase/7/docs/api/java/util/Map.html) ([@Entity](//docs.oracle.com/javaee/6/api/javax/persistence/Entity.html) a [@Embeddable](//docs.oracle.com/javaee/6/api/javax/persistence/Embeddable.html) typy jako klíče a jako hodnoty, tj. kdy použít [@OneToMany](//docs.oracle.com/javaee/6/api/javax/persistence/OneToMany.html) a kdy [@ElementCollection](//docs.oracle.com/javaee/6/api/javax/persistence/ElementCollection.html))
* [transakce](//docs.oracle.com/javaee/6/tutorial/doc/bncih.html) a vyhazování výjimek (tohle není až tak JPA, jako spíš EJB a [JTA](//en.wikipedia.org/wiki/Java_Transaction_API))
* typy a chování zámků ([LockModeType](//docs.oracle.com/javaee/6/api/javax/persistence/LockModeType.html)) na [EntityManageru](//docs.oracle.com/javaee/6/api/javax/persistence/EntityManager.html)
* nastavení [Cache Mode](//docs.oracle.com/javaee/6/tutorial/doc/gkjjj.html) v [persistence.xml](//docs.oracle.com/cd/E19798-01/821-1841/bnbrj/index.html), cachování pomocí [@Cacheable](//docs.oracle.com/javaee/6/api/javax/persistence/Cacheable.html) a čištění cache pomocí [Cache.evict()](//docs.oracle.com/javaee/6/api/javax/persistence/Cache.html#evict(java.lang.Class))
* použití různých typů EntityManagera (aplikační, [transakční](//docs.oracle.com/javaee/6/api/javax/persistence/PersistenceContextType.html#TRANSACTION), [extended](//docs.oracle.com/javaee/6/api/javax/persistence/PersistenceContextType.html#EXTENDED))
* přepisování atributů a asociací v [@Embeddable](//docs.oracle.com/javaee/6/api/javax/persistence/Embeddable.html) pomocí anotací [@AttributeOverride](//docs.oracle.com/javaee/6/api/javax/persistence/AttributeOverride.html) a [@AssociationOverride](//docs.oracle.com/javaee/6/api/javax/persistence/AssociationOverride.html)
* dostupnost [persistence unit](//docs.oracle.com/cd/E19798-01/821-1841/bnbrj/index.html) v rámci různých typů archivů (JAR, WAR, EAR)

Naopak v minimální míře jsem zaznamenal témata jako základní mapování a [Java Persistence Query Language](//docs.oracle.com/javaee/6/tutorial/doc/bnbtg.html) (JPQL). 

## Co studovat

### Kniha Pro JPA 2

{{< figure class="floatright" src="/2014/02/Pro-JPA-2.png"
    link="/2014/02/Pro-JPA-2.png" width="200" >}}

Když jdu do certifikace, začínám knihou. Nic lepšího se mi neosvědčilo. V případě JPA 2.0 existuje bezkonkurenční bible [Pro JPA 2](//www.apress.com/9781430219569). Knížka je sice z roku 2009, ale to (výjimečně) vůbec nevadí --- pokud vám jde o verzi 2.0, ještě s ní pár let vydržíte. Přece jenom, specifikace tak rychle nestárnou.

(Nicméně pokud netrpělivě vyhlížíte _Java EE 7_, jejíž součástí bude JPA 2.1, tak už vyšla [second edition](//www.apress.com/9781430249269) téhle knihy.)

Podtitul tohohle "špalku" je _Masterinig Java Persistence API_ a titul nelže --- provede vás všemi zákoutími JPA a byť z vás neudělá mistra (to bez praxe jaksi nepůjde), poměrně významně pozvedne úroveň vašich znalostí.

Kniha je velmi dobře napsaná, má promyšlený koncept a jednotlivá témata a kapitoly na sebe logicky a plynule navazují. Může po ní sáhnout jak úplný (JPA) začátečník, tak někdo kde přechází z předešlé verze 1.0 a stejně tak, pokud si jen potřebujete oživit některé oblasti. A dá s použít i jako prvotní reference (pokud nepotřebujete jít příliš do hloubky). Tak všestranná kvalitní kniha se jen tak nevidí. A je to ideální zdroj pro přípravu na certifikaci.

Aby byla tahle kniha briliantová, chybí jí jen jeden aspekt --- propojení JPA se [Springem](//projects.spring.io/spring-framework/). Je pochopitelné, že je JPA provázáno s EJB, poněvadž obojí je součástí _Java EE_. Na druhou stranu, kombinace Spring + [Hibernate](//hibernate.org/) táhla v uplynulých letech vítězně světem a to tak masivně, že bych čekal, že to nelze opominout. Lze. Takže ve výsledku je kniha jen diamantová :-)

### Kniha Java Persistence with Hibernate

{{< figure class="floatright" src="/2014/02/Java-Persistence-with-Hibernate.jpg"
    link="/2014/02/Java-Persistence-with-Hibernate.jpg" width="200" >}}

Když to jde, tj. je čas a je to vůbec k dispozici, snažím se, pro přípravu na certifikaci, přečíst knihy dvě. V tomto případě byl mojí sekundární volbou opus [Java Persistence with Hibernate](//www.manning.com/bauer3/), na kterém mě lákaly dvě věci --- jednak je od [Manningu](//www.manning.com/), jehož počiny mě, když ne nadchnou, tak aspoň nezklamou; a jednak je jedním z autorů Gavin King, stvořitel zmiňovaného Hibernate. Plus navíc je tahle kniha žhavě aktuální --- ještě stále je ve stavu _early access_ ([MEAP](//www.manning.com/about/meap.html#meapfaq)) a finální by měla být letos v březnu.

Na tuhle knihu jsem se hodně těšil, ale mám z ní rozporuplný pocit. Předně, je hrozně ukecaná. Ale hrozně. Než se autoři k něčemu podstatnému dostanou, tak to trvá a vysvětlí vám to fakt pořádně.

Pak mi tady chybí, to co se mi tak líbilo na předešlé knize --- propracovaný koncept. Přijde mi, že byť autoři směřují k určitému cíli, plují proti větru a křižují perzistentní vody sem a tam. S tím souvisí i další věc: tuhle knihu určitě nepoužijete jako referenci --- jakékoliv téma se v ní bude obtížně hledat.

Co je naopak na tomto počinu cenné, je popis vlastností, které má Hibernate nad rámec JPA. Hibernate je totiž jeho implementací:

* Hibernate 3.2 implementoval JPA 1.0
* Hibernate 3.5 implementoval JPA 2.0
* Hibernate 4.3 implementuje JPA 2.1

Protože Hibernate je starší a zralejší technologií, než JPA, tak nijak nepřekvapí, že je i bohatší na různé vlastnosti a je v lecčems napřed. A právě tyhle aspekty (kromě samotného JPA), kniha popisuje. Za všechny bych zmínil třeba možnost definovat anotace platné pro celou [persistence unit](//docs.oracle.com/cd/E19798-01/821-1841/bnbrj/index.html) (třeba [@NamedQuery](//docs.oracle.com/javaee/6/api/javax/persistence/NamedQuery.html)) na tříde [package-info.java](//docs.oracle.com/javase/specs/jls/se7/html/jls-7.html), což JPA neumí.

Celkovému hodnocení téhle knihy bych se prozatím vyhnul. Ovšem čistě z pohledu certifikace to není moc dobrý zdroj --- je příliš rozvláčný, ne vhodně strukturovaný a nejspíš (z hlediska JPA) ani kompletní. Nicméně pro rozšíření kontextu to rozhodně není špatné.

### Training tests

Pokud má někdo hodně dobrou paměť, orientovanou na detaily a přesné znění, možná může jít do certifikace jen po načtení literatury. To není můj případ. Většinou si po čase pamatuju cosi ve stylu _něco takového tam je, ale přesně si to už nepamatuju_.

Takže vždy tak maximálně 14 dní před zkouškou a poslední dobou spíš už jen týden, používám nějakou sadu zkušebních testů, které mi pomohou se "naladit" na ostrý test. Pro JPA jsem dal na doporučení na [JavaRanch](//www.coderanch.com/t/570794/OCEJPA/certification/Passed-tips-study-sources) a si vybral sadu od [Enthuware](//enthuware.com/index.php/mock-exams/oracle-certified-expert/java-persistence-api-oce-jpa).

Jedná se o sadu čtyř testů, které rozsahem odpovídají certifikaci (tj. 64 otázek, 135 minut). Testy jsou o něco težší, než ten reálný. Po vyhodnocení testu se dají otázky znovu projít, tentokrát s vysvětlením, které je sice dost úsporné, ale pokud jste se předtím neflákali, mělo by to být dostačující. A cena $20 není špatná.

## Co mě to přineslo?

Jedním z hlavních přinosů certifikací pro mne vždycky bylo prohloubení znalostí. V případě JPA jsem:

* upgradoval z verze 1.0 na 2.0,
* naučil se pořádně _Criteria API_,
* přeorientoval se z cachování poskytovaném providerem k tomu definovanému ve specifikaci,
* oživil si věci, které se v nové verzi JPA "až tak neměnily"
* a přečetl dvě dobré knihy.

To vůbec není špatné. Já jsem spokojený :-) 

## Související články

* [Certifikace Java EE 6 Web Services Developer](/2012/09/certifikace-java-ee-6-web-services/)
* [Oracle SOA certifikace](/2013/03/oracle-soa-certifikace/)
* [Flex certifikace](/2012/03/flex-certifikace/)
* [UML certifikace, OCUP Intermediate](/2011/11/uml-certifikace-ocup-intermediate/)
