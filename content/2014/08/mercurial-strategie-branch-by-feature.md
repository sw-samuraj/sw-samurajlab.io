+++
title = "Mercurial, strategie branch-by-feature"
date = 2014-08-26T23:27:00Z
updated = 2014-08-26T23:27:58Z
description = """V distribuovaných version control systémech se člověk
    nějakému způsobu branchování nevyhne. Je dobré si nastavit nějaké
    konvence a ideálně - branchovací strategii."""
tags = ["mercurial", "version-control", "sw-engineering"]
aliases = [
    "/2014/08/mercurial-strategie-branch-by-feature.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/img/logos/Mercurial.png" >}}

[Mercurial](//mercurial.selenic.com/) je skvělý, distribuovaný [Version Control System](//en.wikipedia.org/wiki/Revision_control) (VCS, či DVCS), který nabízí velkou míru volnosti, jak s nakládat s verzováním zdrojových kódů. Svobodu většinou chápeme jako pozitivní věc, někdy je ale přílišná nespoutanost na škodu. A tak definování nějaké verzovací strategie prospěje týmu i projektu.

## Proč mít verzovací strategii?

Verzovací strategii _branch-by-feature_ jsme s úspěchem použili na stávajícím projektu. Důvody, proč jsme si něco takového definovali byly dva:

* Když jsem se mihnul na předcházejícím projektu (taky Mercurial), žádná strategie, či konvence definovaná nebyla . Člověk pak slýchal řeči jako: "Proč to furt merguješ?", "Vznikaj ti tam anonymní branche." a "Tam by's měl používat [rebase](//mercurial.selenic.com/wiki/RebaseExtension).". Prostě klasický, _tohle-tady-všichni-ví_ a _takhle-to-děláme-ale-tobě-jsme-to-neřekli_.
* Chtěli jsme mít na nadcházejícím projektu formalizované [code review](//en.wikipedia.org/wiki/Code_review).

Takže jsme se zamysleli, chvilku nad tím špekulovali a pak jsme, spolu s podporou dalších nástrojů, došli k následující strategii.

## Strategie branch-by-feature

Princip téhle strategie je jednoduchý a dá se popsat jednou větou: _Pro každou novou feature (nebo bug) se založí nový branch_. Hotovo, vymalováno.

A teď trochu obšírněji. Na počátku je v repozitory pouze jeden _permanentní_ branch. Zpravidla se mu říká _development_ branch. Z něj se odlamují další branche pro vývoj --- _feature_ branche. Co přesně tyto branche představují, záleží na _granularitě_ položek, do kterých jsme si práci rozdrobili. Může to být [user story](//en.wikipedia.org/wiki/User_story), [feature](//en.wikipedia.org/wiki/Feature_(software_design)), [requirement](//en.wikipedia.org/wiki/Requirement), [task](//en.wikipedia.org/wiki/Task_(project_management)). Ideálně je to něco, co evidujeme v [issue tracking systému](//en.wikipedia.org/wiki/Issue_tracking_system) (ITS).

Probíhá běžný vývoj a všechny komity jdou do (odpovídajícího) _feature_ branche. Když je vývoj hotový, proběhne [code review](//en.wikipedia.org/wiki/Code_review) a pokud jsou změny akceptovány, zamergují se do _development_ branche a _feature_ branch se zavře. Pokud jsou změny zamítnuty, provede se náprava, komitne se do _feature_ branche, následuje code review atd.

Pokud jsou v této fázi nalezeny nějaké bugy, přistupujeme k nim stejně jako k feature. To jest, _bug_ branch -&gt; code review -&gt; merge do _development_ branche.

{{< figure src="/2014/08/Mercurial-branching.png" caption="Verzovací strategie branch by feature" >}}

Po nějakém čase vznikne _release_ branch. Pokud jsou v něm nalezeny chyby, vzniká _bug_ branch a po code review přichází merge jak do _release_, tak do _development_ branche.

Celý postup se dá shrnout do následujících bodů:

1. Vytvoření nového _feature/bug_ branche.
1. Development.
1. Změny projdou code review.
1. Uzavření _feature/bug_ branche.
1. (Bugfixy jsou zamergovány do _release_ branche.)
1. Změny jsou zamergovány do _development_ branche.

Z pohledu příkazové řádky vypadá proces takto:

```bash
$ hg branche fb-logging
$ hg commit -m 'Logging feat. has been developed.'
$ hg commit -m 'Close branch fb-logging.' --close-branch
$ hg update default
$ hg merge fb-logging
$ hg commit -m 'Merge branch fb-logging -&gt; default.'
```

Zcela záměrně se vyhýbám popisu code review. To proto, abych nekradl materiál svému kolegovi [Banterovi](//blog.zvestov.cz/), který již jistě brzy napíše článek, o tom, jak to děláme (je to fakt cool, tak se těšte). Nicméně, ve vztahu k Mercurialu si nemůžu odpustit, jak to vypadá procesně. Přepokládám, že všichni čtete plynně [BPMN](//en.wikipedia.org/wiki/BPMN) ;-)  takže dalších slov netřeba. Mercurial aktivity jsou v oranžovém rámečku.

{{< figure src="/2014/08/Code-Review-flow.png"
    caption="Code Review proces (oranžové jsou aktivity v Mercurialu, modré v RhodeCode (https://rhodecode.com/))" >}}

## Drobná a příjemná vylepšení

Což o to, proces, jako takový, je pěkný. Na někoho je ale možná trochu komplexní. Přece jenom, když mastíte celý život všechno jenom do [trunku](//stackoverflow.com/questions/698313/what-is-trunk-branch-and-tag-in-subversion), může to být trochu moc věcí najednou. Tady je pár věcí, které nám to o něco usnadnili.

### Konvence

Konvence jsme měli nastavené jednak pro názvy branchů a jednak pro komitovací komentáře. Název nového branche se skládal z prefixu a čísla issue v [ITS](//en.wikipedia.org/wiki/Issue_tracking_system). Prefix byl buď `fb-` (_feature_ branch), nebo `bb-` (_bug_ branch). Např. `fb-42`, `bb-1024`.

Konvence pro komentáře by se daly rozdělit do tří skupin: běžné komentáře, při zavírání branche a při vytváření releasu. Konvence samotná by se v Mercurialu dala pohlídat pomocí [hooku](//hgbook.red-bean.com/read/handling-repository-events-with-hooks.html), ale nedokopal jsem se k tomu, ho napsat. Pro "běžné", vývojové komentáře jsme měli konvenci `WIP #nnn; some comment.` WIP je zkratka pro Work In Progress, `nnn` je id issue v ITS. Např. `WIP #1561; Payment button has been removed.`

Zavření branche se skládá ze čtyř Mercurial příkazů (commit, update, merge, commit, viz výše) a obsahuje dva komentáře ve formátu:

* `Close branch <branch-name>.`
* `Merge branch <branch-name> -> default.`

Za zmínku stojí mergovací komentář, ze kterého by mělo být jasné, odkud kam merge proběhnul.

{{< figure src="/2014/08/Mercurial-merge-comments.png" caption="Komentáře při zavírání branche" >}}

Releasovací komentáře byly celkem čtyři a obsahovaly klíčové slovo `[Release]`.

{{< figure src="/2014/08/Mercurial-release-comments.png" caption="Komentáře při vytváření releasu" >}}

V předešlém popisu vás možná zarazilo, že jak pro zavření branche, tak pro vytvoření releasu je potřeba spustit čtyři Mercurial příkazy. Dělat to ručně by byla značná otrava a hlavně by celá konvence brzo erodovala do chaosu. Šťastni kdož používají automatizaci, neboť jen ti vejdou do křemíkového nebe.

### Mercurial alias

S používáním [aliasu](//mercurial.selenic.com/wiki/AliasExtension) přišel kolega [Banter](//blog.zvestov.cz/), takže mu za to patří dík a rád ho zde zvěčním. Alias samotný vypadá dost ošklivě, zato jeho použití je elegantní. Jedinou vadou na kráse je, že jsme nepřišli, jak alias spustit z grafických nástojů, jako je [SourceTree](//www.atlassian.com/software/sourcetree/overview), nebo [TortoiseHg](//tortoisehg.bitbucket.org/), takže ještě budeme muset zapracovat na tom, aby tuto fičurku mohli používat i kolegové, kteří ještě nepřišli na to, jak cool je používat command line ;-)

Alias stačí přidat do souboru `~/.hgrc` a spouští se jednoduchým příkazem `hg close-feature nnn`, kde `nnn` je (v našem případě) čístlo issue v ITS, tj. to, co je za prefixem `fb-`.

```bash
[alias]
close-feature = ![ -z "$1" ] && echo "You didn't specify a branch!" && exit 1; \
    if hg branches | grep -q "fb-$1"; \
        then $HG up fb-$1; \
             $HG commit -m 'Close branch fb-$1.' --close-branch; \
             $HG pull; \
             $HG up default; \
             $HG merge fb-$1; \
             $HG commit -m 'Merge branch fb-$1 -&gt; default.'; \
        else echo "The branch fb-$1 does NOT exist!"; \
    fi
```

### Gradle release task

Náš projekt buildujema [Mavenem](//maven.apache.org/). Chvilku jsem se snažil napasovat [Maven Release plugin](//maven.apache.org/maven-release/maven-release-plugin/) na naši branchovací strategii, ale pořád to nějak nebylo ono --- je to prostě moc šitý na [SVN](//subversion.apache.org/). Pak jsem ztratil trpělivost a odpovídající chování si napsal jako [Gradle](//www.gradle.org/) task. Časem se z toho snad vyklube Gradle plugin.

Task samotný tady uvádět nebudu, protože je asi tak na tři stránky. A taky potřebuje ještě trochu poladit. Zkrátka ještě není zralý na to, abych ho dal z ruky. V podstatě ale dělá pouze to, že přepíná branche, šolichá s Mercurialem a manipuluje s verzí v `pom.xml`. A dělá to ty hezké komentáře, co jsem uváděl výše.

## Pros &amp; Cons

Celý výše popsaný koncept jsme vymysleli ještě před projektem. Pak přišel projekt a emotivně musím říct, že nám to krásně fungovalo. Jako hlavní benefit vidím, že jsme si formalizovali proces _code review_ a s výše uvedenou branchovací strategií to bylo velice efektivní. Dalším plusem byla, "úhledná" práce s Mercurialem, kdy byla radost se prohrabávat verzovanou historií.

K negativům bych zařadil komplexnost. Pokud by nám nešlo o _code review_, asi by tato strategie byla zbytečná. Zatím mi taky chybí nějaká podpora v grafických a automatizačních nástrojích. Jako milovníkovi [CLI](//en.wikipedia.org/wiki/Command-line_interface) mi to osobně nijak nevadí, ale musím myslet taky na ostatní členy týmu (bývalý kolega jim říkal "makáči" :-)  pro které by mělo být používání nástrojů co nejsnadnější.

## Je to všechno?

Abyste dostali plný obrázek, jak to celé fungovalo, chybí nám jeden velký dílek skládanky --- jo, _code review_. Takže pokud vám něco nedává smysl, tak je to možná proto, že jsem v tomto článku celkem důsledně odpáral použití dalších dvou nástrojů, které byly s branchovací strategii organicky provázané --- issue tracking system a [RhodeCode](//rhodecode.com/), nástroj, ve kterém jsme, pomocí [pull requestů](//www.atlassian.com/git/workflows#!pull-request), řešili _code review_.

Popis _code review_ je na samostatný článek. Tak snad ho Banter brzo napíše a já sem pak lísknu odkaz. A jinak doufám, že vám zmiňovaná strategie branch-by-feature přijde inspirativní.

_Happy branching! :-)_

## Mind Map

{{< figure src="/2014/08/Mercurial-branching-mind-map.jpg"
    link="/2014/08/Mercurial-branching-mind-map.jpg" >}}

## Související články

* [Mercurial, jak nastavit P4Merge jako nástroj pro vizuální merge a diff](/2013/05/mercurial-jak-nastavit-p4merge-jako-nastroj-pro-vizualni-merge-a-diff/)
