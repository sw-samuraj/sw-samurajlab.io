+++
title = "Kanban, zprávy z fronty II"
date = 2014-08-12T21:08:00Z
updated = 2017-03-10T21:09:12Z
description = """Podařilo se nám na projektu celkem pěkně naimplementovat
    Kanban. Jak to v našem podání vypadalo? Pěkně!"""
tags = ["teamleading", "kanban", "agile", "sw-engineering"]
aliases = [
    "/2014/08/kanban-zpravy-z-fronty-ii.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2014/08/Kanban-done.png"
    link="/2014/08/Kanban-done.png" width="200" >}}

Máme za sebou, s týmem, další, úspěšnou implementaci [Kanbanu](//en.wikipedia.org/wiki/Kanban_(development)). Projekt pomalu končí, je čas se ohlédnout. Jak to vypadalo, co fungovalo, co je potřeba zlepšit?

## O projektu

Vzhledem k tomu, že z bezpečnostního hlediska se jedná o citlivé téma, nebudu psát nic o architektuře a technologiích. Což ale nevadí, protože z pohledu Kanbanu je obsah a typ projektu nepodstatný. Ale ať nám to povídání nevisí ve vzduchu, nastíním business case.

Výsledkem projektu je/bude webová aplikace, s jejíž pomocí si zájemci mohou online zažádat o různé typy víz a povolení (pracovní, k pobytu apod.) a také je eventuálně online zaplatit. Aplikace, jako taková, je velmi hloupoučká, veškerá business logika a workflow zpracování žádosti se děje na backendu, který už není součástí naší dodávky.

## Team

Šlo o malý projekt, takže i malý tým --- 2 vývojáři, 1-2 testeři/integrátoři, 1 technical leader, 1 project leader. Celkově (a konstantně během celého projektu) nějakých pět lidí. Pouze dva členové týmu měli předešlou zkušenost s agilním vývojem, s Kanbanem pak pouze jeden. Tým byl technicky seniorní, žádný junior.

## Vizualize

Základním přikázáním Kanbanu je _vizualizuj!_ Všechno, co jde (a dává smysl), vše, co dá lepší viditelnost projektu.

### Kanban Board

První věc, která se (obvykle) vyzualizuje, je projektové/procesní workflow. Použili jsme jak fyzický, tak elektronický board. Ten elektronický byl založen na [issue tracking](//en.wikipedia.org/wiki/Issue_tracking_system) (ITS) nástroji [Redmine](//www.redmine.org/).

Bohužel, Redmine je, jako ITS, dosti nezralý/problematický a pro Kanban nemá žádný použitelný plugin. Problém jsme vyřešili "oklikou přes ruku" (= workaround) pomocí pluginu [Wiki Lists](//www.r-labs.org/projects/wiki_lists/wiki/Wiki_Lists_en), který dává přijatelný výsledek. Elektronický board byl primárním zdrojem informací a stavu.

{{< figure src="/2014/08/Kanban-board-Redmine.png"
    link="/2014/08/Kanban-board-Redmine.png"
    caption="Elektronický Kanban board (Redmine a Wiki Lists plugin)" >}}

Fyzický board byl druhotný zdroj informací. Používali jsme ho jako přirozené místo setkávání týmu (stand-upy). Další výhodou byla jeho neustálá přítomnost v open spacu. Takže všichni viděli :-)

Každý člen týmu byl zodpovědný za synchronizaci svých tasků mezi ITS a fyzickým boardem.

{{< figure src="/2014/08/Kanban-board-II.jpg"
    link="/2014/08/Kanban-board-II.jpg"
    caption="Fyzický Kanban board (stav na koneci projektu)" >}}

Co stojí za zmínku na fyzickém boardu: je zde název projektu a číslo aktuálního releasu, grafy statistik (viz dále), bankovky přivezené ze služební cesty. Předpokládám, že stavy workflow jsou samo-vysvětlující. Assignment jsem řešili pomocí labelů (Vit, Lub, Seb atd.).

## Design kartiček

Na kartičkách, které na fyzickém boardu představují jednotlivé tasky, může být spousta informací. Na rozdíl od elektronické verze, kde bývá spousta atributů, má kartička jen omezený prostor. Proto je potřeba zvážit, jaké informace na ni dát, aby byla zároveň informačně dostačující a přitom srozumitelná. Velmi podrobně se tomuto tématu věnuje (výborná) kniha [Kanban in Action](//www.manning.com/hammarberg/).

My jsme začali a skončili u jednoduchého designu:

* ID z [ITS](//en.wikipedia.org/wiki/Issue_tracking_system) ve formátu `#nnn` (levý horní roh).
* Zjednodušený název/summary tasku (uprostřed).
* Zkratka komponentu/feature/user story (zeleně, pravý horní roh).
* Délka setrvání ve stavu _In Progress_ (červené tečky dole, tečka = den).
* Indikátor kritické priority (červený vykřičník, nahoře uprostřed).
* Indikátor blokace (červené B, pravý dolní roh).

{{< figure src="/2014/08/Kanban-task-info.jpg"
    link="/2014/08/Kanban-task-info.jpg"
    caption="Design kartiček" >}}

### Class of Service

Když jsme projekt začínali, měli jsme všechny kartičky žluté. Postupně jsme pak zavedli _classes of service_. U tohoto konceptu je podstatné, že každá _třída_ má přiřazenou specifickou politiku, jak s ní zacházet. Má třeba jiné workflow, jinou prioritu atd.

My jsme prvně odlišili tasky a bugy jiným workflow a bugy jsme na boardu odlišili růžovými lístečky (červené nebyly ;-)  Protože jsme měli _zero bug policy_, měl každý bug vždy vyšší prioritu než task.

Později jsme zavedli ještě jednu třídu pro tasky --- má neintuitivní název _intangible_ (termín přejímám z knihy [Kanban in Action](//www.manning.com/hammarberg/)). Do téhle třídy spadají tasky, které nemají přímou, _hmatatelnou_ business hodnotu, nicméně je potřeba je udělat. Jsou to věci jako různé konfigurace, interní dokumentace, refactoring apod. Tato třída měla (s výjimkou přípravy releasu) prioritu nižší, než task.

{{< figure src="/2014/08/Kanban-class-of-service.jpg"
    link="/2014/08/Kanban-class-of-service.jpg"
    caption="Třídy služeb (Classes of Service): žlutý --- task, růžový --- bug, zelený --- intangible" >}}

## Limit WIP

Moje zkušenost je zatím taková, že limitování WIP (Work In Process), je pro účastníky tou nejproblematičtější položkou Kanbanu --- lidi vždycky nadávají, když jsou alokovaný na víc než jeden projekt, ale budou do krve hájit svůj "multi-taskinig". Myslím, že tady nás čeká ještě dlouhá cesta pomalé a trpělivé implantace principu _stop starting, start finishing_.

Malou podporu Kanbanu v [Redminu](//www.redmine.org/) už jsem zmiňoval. Limit WIP v něm nejde nastavit, tak jsme si ho definovali "jenom" jako politiku --- každý by měl pracovat v daný čas jenom na jednom tasku. Fungovalo to jen z půlky dobře, vývojáři obvykle toto "omezení" dodržovali, testeři obvykle ne. Celkově bych ale řekl, že pokud je WIP nastavený jenom politikou, může to i přesto dobře fungovat. Chce to jen trpělivě lidi vzdělávat, jít příkladem a s klidem to vyžadovat. Poučení pro mne --- být důslednější.

S WIP jde velmi často ruku v ruce otázka, co s blokovanými tasky? Není na to jednoduchá odpověď, částečně se to dá řešit flow, nebo politikou. Ale asi nejlepší je mít poučený tým --- jak jsem kdesi četl (možná že [Agile Samurai](//pragprog.com/book/jtrap/the-agile-samurai)?) _"zkušený agilní vývojář nikdy nezačne task, který není schopný dokončit"_. Čili prvně si task "před-nalyzovat" a odstanit rezistence a pak se teprve do něj pustit. Ne vždycky to jde, ale autonomní a uvědomnělý přístup členů týmu (opak skočím do toho pohlavě, neboli jdu rovnou bušit) mi dost často chybí.

## Manage Flow

Kanban má rád autonomní lidi. Já taky. Kanban je _pull system_ --- když dám něco do TODO, vyhovuje mi, když to nemusím nikomu přiřazovat, ale libovolný člen týmu si to (podle svých schopností) vezme na starost. Jako team leaderovi mi to šetří obrovské množství času.

Jeden člen týmu měl s tímhle přístupem silný problém, bylo to pro něj příliš volné, "bez pravidel". Opět platí: vysvětlovat, vzdělávat. Plus, Kanban holt není pro každého, někdy si lidé a proces nesednou.

Workflow tasku na tomhle projektu je jedna z věcí, které se nám, myslím, dost povedly. Jedním z podstatných rysů flow byla silná podpora formalizovaného [code review](//en.wikipedia.org/wiki/Code_review) (můžete se těšit, až o tom [Banter](//blog.zvestov.cz/) napíše článek).

Za zmínku stojí stav _merge_. Původně to byl jenom pseudo-stav (neměl svůj obraz v ITS). Ale vzhledem k asynchronní povaze našeho code-review procesu a občasné distribuovanosti týmu (home office, služební cesty), se vyplatilo z něj udělat svébytný stav.

{{< figure src="/2014/08/Task-workflow.png" caption="Workflow tasku" >}}

Na počátku projektu jsme měli také samostatný stav pro blokované tasky. Ale pak jsme ho zrušili a nahradili blokujícím atributem přímo na issue. Důvod je prostý. Toto byl už druhý projekt, kde jsem měl v rámci Kanbanu stav _blocked_ a vždycky to dopadlo stejně --- tento stav se stal odkladištěm (až smetištěm) pro blokované tasky a nikdo (ani já) se moc nesnažil s tím něco dělat, odblokovat je. Řekl bych, že _blocked_ atribute v tomhle funguje lépe a navíc není svázaný jen s jedním stavem --- věci můžou být blokované ve vývoji, testování, integraci atd.

Další věc, kterou jsme v rámci flow zavedli byla fronta pro tasky čekající na otestování. Projekt byl z hlediska testerů podhodnocený, takže tasky se nám tam hromadily zcela přirozeně. V Kanbanu je to pěkně vidět --- když se vám někde začnou štosovat lístečky, máte (možná) problém. Z hlediska teorie jde o úzké hrdlo a obvykle stojí za to ho trochu rozšířit, nebo odstranit.

My jsme to řešili dvěma způsoby: jednak jsme se zaměřili na automatizaci ([Selenium](//www.seleniumhq.org/) testy), což pomohlo pouze částečně (testy fungovaly spíše regresně, než že by testovaly nově vyvinuté tasky) a jednak jsme úspěšně zalobovali za alokaci dalšího testera.

### Statistiky

Se správou flow souvisí také jeho měření. Statistiky jsme používali, ale až takový přínost to pro nás nemělo. Z několika důvodů:

* Se sbíráním statistik jsme začali pozdě. To bylo dané hlavně téměř nulovou podporou a využitelností reportingu v Redminu. Polovinu údajů pro statistiky bylo potřeba získávat ručně. I při malém týmu to bylo dost pracné.
* Změny v našem flow byly celkem drobné. Ve spojení s předešlým bodem, je otázka, jak moc by se změny projevily a jak by to bylo průkazné.

Takže to byla spíš taková zajímavost a zkušenost pro příští projekt. Metriky, které jsme sbírali, byly _throughput_ (propustnost, velocita) a _lead time_ (trvání dokončení tasku).

{{< figure src="/2014/08/Throughput.png" caption="Týdenní velocita" >}}

Statistiky jsme dělali jak pro "obecné" issue, tj. task bez ohledu na jeho typ a velikost, tak pro tasky odhadnuté pomocí triček (T-shirts). Ze statistiky tak např. vyplývá, že průměrná velocita týmu za týden byla 6 tasků, a průměrné dokončení tasku odhadnutého jako S trvalo 2,5 dne. (Dokončení je myšleno tak, jak ho chápe Kanban, takže vyvinuto, zreviewováno, otestováno, deployováno.)

{{< figure src="/2014/08/Lead-time.png" caption="Lead Time podle relativní velikosti tasků" >}}

## Explicit Policies

Explicitní politiky jsou důležité. Je pak nad čím diskutovat a není to hospodská tlachačka o něčem "co všichni ví" a "takhle se to vždycky dělá". Obecně, nemělo by jich být moc, měly by být jednoduché a měly by být snadno přístupné. Naše politiky jsme měli na projektové wiki a zahrnovaly:

* **Stand-up**: jaký je jeho smysl a jaké má pravidla.
* **WIP = 1**: každý dělá v daný čas pouze na jediném tasku.
* **Zero Bug Policy**: každý reportovaný bug je automaticky kritický a musí být opraven, než se začne pracovat na novém tasku.
* **Pravda je v ITS**: primární zdrojem stavu projektu je trackovací systém. Každý je zodpovědný za aktuálnost a synchronizaci svých tasků.

Co mi chybělo a ocenil bych, je _Definition of Done_. Tak příště.

## Implement Feedback, Improve Collaborativelly

Na rovinu, tyhle dvě věci nám moc nešly. Sice jsme měli retrospektivy, ale nevzpomínám si, že by to nějak ovlivnilo věci kolem Kanbanu, flow apod. Na jednu stranu to trochu chápu --- tým byl Kanbanem nepolíbený, a autonomním se člověk nestane přes noc. Vysvětlovat, vzdělávat, působit. Chce to čas.

## Pravidelné týmové aktivity

Následující praktiky nejsou součástí Kanbanu. Ale velice dobře se s ním pojí.

### Stand-up

Denní stand-up, agilní to klasika. Pro popis bude nejjednodušší, když ocituju naši politiku:

**The main purpose of the stand-up is that the team get the current context of the project.**

The stand-up should be:

* **short** --- max. 10 minutes,
* **focused** --- see the _main purpose_ above,
* with participation of the **all team members**,
* in front of the _Kanban board_.

The stand-up shouln't be used for:

* discussions which don't involve all the team,
* solving the technical problems.

If such situation happens (discussion, problem),

* it should be rather solved after stand-up,
* ideally in the _phone box_ rather than in open-space,
* only with people who are interested/involved in the problem.

The typical stand-up participation is:

* **What did I do yesterday?**
* **What will I do today?**
* **What is blocking me.**

U stand-upů se mi opět potvrdilo, to co u jiných agilních praktik --- lidi, kteří s tím nemají zkušenost, můžou prvně "frfňat". Ale jen do té doby, než "to začnou dělat". Pak přijde něco jako _prozření_. Problém s akceptací jsme měli u jednoho člena týmu, ale potom, co jsme si to vysvětlili a deklarovali výše uvedenou politiku, už to bylo v pořádku.

Že to dobře fungovalo, můžu ilustrovat faktem, že tým nepotřeboval týdenní statusy (byly by zbytečné), protože stav všichni pobrali během stand-upů.

### Retrospektivy

Retrospektivy jsme začali dělat cca v půlce projektu. Pro mne to bylo premiérové téma, proto jsem nechal retrospektivy řídit zkušenějšího kolegu a sám jsem se ponořil do studia. Hodně mi v tom pomohla kniha [Agile Retrospectives](//pragprog.com/book/dlret/agile-retrospectives).

Retrospektivy jsou pro mne ještě příliš čerstvé téma, takže je zatím nebudu hodnotit. Ale pocitově, intuitivně z nich mám dobrý pocit a chci je použít na dalším projektu.

### Prioritizace

Prioritizovat je potřeba snad na každém projektu. Někdy víc, někdy méně formálně. V Kanbanu to může být stěžejní záležitost. U nás jsme se domluvili, že prioritizaci budou dělat technical a project leader. Naplánovali jsme to týdně na páteční ráno, aby bylo od pondělí jasno, co si lidi můžou brát.

Moc to nefungovalo, sešli jsme se jen párkrát. Přesto to nějak jelo bez větších zádrhelů, možná tomu napomohla automatická prioritizace vyplývající z _classes of service_ (viz výše). U většího projektu by to asi byl dost problém. Prostě další oblast pro zlepšení.

### Odhady

Na projektu jsme zavedli relativní odhady tasků pomocí triček (T-shirt estimations). Používali jsme velikosti S, M, L a XL. Pokud něco mělo velikost XL, bylo potřeba to dále rozbít na menší části. Pro odhadování samotné jsme používali [planning poker](//en.wikipedia.org/wiki/Planning_poker) s odpovídajícími hodnotami karet.

Odhady jsme dělali jednou týdně, většinou to zabralo hodinu. Členové týmu si stěžovali, že to jde pomalu (odhadne se toho málo), na druhou stranu oceňovali diskuzi, která nad tasky vznikla a umožnila tak lépe pochopit daný problém.

Odhady jsme začali dělat přibližně ve stejné době jako retrospektivy, takže neměly tak velký přínos, jak by mohly, kdybysme s nimi začali už na začátku projektu. Pozitivně vnímám, že je dělal celý tým. Naučili jsme se, jak používat trička a na dalším projektu budeme pokračovat.

## Zhodnocení

Toto je druhý projekt, kde jsem aplikoval Kanban a potvrdilo se mi, že je to životaschopný a přínosný nástroj. Jako u všeho, co je tvořeno "měkkými" pravidly, hodně záleží na poučenosti lidí v týmu. Jednou z hlavních zkušeností je pro mne: vysvětlování není nikdy dost --- tady se budu muset ještě hodně zlepšit. Ale doufám, že už jsem své kolegy naočkoval a příště to bude zase o něco snadnější.

Myslím, že učednická léta už mám v Kanbanu za sebou. Teď je potřeba zaměřit se na pokročilejší témata a zlepšit a dotáhnout do konce věci, jako jsou metriky, třídy služeb, plánování a odhady, používání modelů ad.

Už teď je jasné, že Kanban použiju na příštím projektu. Takže cca za rok si budete moci přečíst další díl tohoto článku. Nebo seriálu? ;-)

## Mind Map

{{< figure src="/2014/08/Kanban-II.jpg"
    link="/2014/08/Kanban-II.jpg" >}}

## Související články

* [Kanban, lehký úvod](/2014/03/kanban-lehky-uvod/)
* [Kanban, zprávy z fronty](/2013/01/kanban-zpravy-z-fronty/)
* [Kanban z čistého nebe](/2012/07/kanban-z-cisteho-nebe/)
