+++
title = "Code review checklist"
date = 2014-07-20T22:54:00Z
updated = 2014-07-20T22:54:21Z
description = """To jsem se zase jednou připletl do diskuze o code review. 🤦‍♂️
    Tak jsem se nad tím zamyslel a něco si k tomu sepsal."""
tags = ["teamleading", "sw-engineering"]
aliases = [
    "/2014/07/code-review-checklist.html"
]
blogimport = true 
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2014/07/Joel-test.png" >}}

Nedávno jsem v práci prezentoval, jaké přínosné věci používáme na aktuálním projektu. Vyzkoušeli jsme si spoustu zajímavých nástrojů a praktik a v podstatě to byla taková laboratoř, kdy ty funkční záležitosti použijeme na dalším projektu. [Mind mapa](//en.wikipedia.org/wiki/Mind_map) níže shrnuje přehled prezentovaných témat.

Jedním z nejcennějších realizovaných konceptů pro mne je, že se nám podařilo naimplementovat funkční a efektivní [code review](//en.wikipedia.org/wiki/Code_review). (Doufám, že kolega [Banter](//blog.zvestov.cz/) o tom brzy napíše článek.) A co čert nechtěl, po zmiňované prezentaci se nejvíc diskutovalo právě _code review_. Jedním z výstupů téhle diskuze je, že by bylo dobré mít nějaký _code review **checklist**_.

Já takový checklist nemám, protože ke _code review_ přistupuju intuitivně (což ale neznamená, že nevím, co přesně chci, naopak). Nicméně pro potřeby diskuze jsem si sesumíroval, co by v takovém checklistu _mohlo_ být.

{{< figure src="/2014/07/TechGem-Ghana.png" caption="Pozitivní věci na projektu (code review je fialový)" >}}

## Co je to code review?

Ač se pojem _code review_ používá v oblasti softwarového inženýrství dosti zhusta, má celkem nejednoznačný obsah. Pro někoho to je výsledek nástrojů jako je [SonarQube](//www.sonarqube.org/), [PMD](//pmd.sourceforge.net/), [FindBugs](//findbugs.sourceforge.net/) ad. Tyto nástroje řeší tzv. [statickou analýzu kódu](//en.wikipedia.org/wiki/Static_program_analysis) a jsou výbornými pomocníky při udržování kvalitního kódu.

Ale _code review_, tak jak ho chápu já, začíná tam, kde tyto nástroje končí. Prostě tam, kde stroje selhávají, či nestačí, přichází ke slovu "stará dobrá ruční práce". Dalo by se to také nazvat jako asynchronní [peer review](//en.wikipedia.org/wiki/Software_peer_review).

## Co je to code review checklist?

[Checklist](//en.wikipedia.org/wiki/Checklist) ((kontrolní) seznam bodů) slouží k tomu, abychom na něco nezapomněli. Třeba koupit chleba a mlíko cestou z práce. V případě _code review_ jde o to, nezapomenout projít některý z aspektů, které chceme v rámci _review_ kontrolovat.

## Hlavní oblasti

Věci, které tak nějak intuitivně kontroluji při _code review_ by se daly shrnout do těchto základní oblastí:

* Konvence
* Design
* Best-practices
* Závislosti
* Pokrytí testy

## Konvence

Kdekoliv dochází k nějaké sociální interakci, jsou přítomny konvence. Buď již existují, nebo se začnou vytvářet. V dnešní době, kdy je vývoj software téměř vždy týmovou prací, je taková sociální (u některých programátorů a adminů spíše asociální) komunikace nevyhnutelná. Z hlediska _code review_, bych vypíchnul dva body, pro které je dobré konvence nastavit a dodržovat/kontrolovat:

* **Formátování** zdrojového kódu napomáhá jeho čitelnosti, pochopitelnosti, orientaci v něm atd. Tahle oblast se dá z větší části kontrolovat pomocí statické analýzy kódu (např. v Javě nástroj [Checkstyle](//checkstyle.sourceforge.net/)), ale některé věci zkrátka nejde nacpat do (automatických) pravidel. Domluvte se na nich, dodržujte je a váš reviewer vás bude mít rád ;-)
* **Pojmenování.** Věci by měly mít správná jména. Bude pak jasné, k čemu slouží, když se o nich budeme bavit, budeme více méně na stejné platformě a kdokoliv nový to lépe a rychleji vstřebá. Typicky, je dobré mít jmennou konvenci pro _komponenty_, _balíčky_, _třídy_, _metody_ a _proměnné_. A cokoliv dalšího, co dává smysl a bude se vyskytovat ve více instancích.

Konvence jsou velmi rozsáhlé téma. A stejně jako u spousty dalších věcí, o kterých budu psát, dochází k jejich přesahu do jiných oblastí. Berme to rozřazení do základních kategorií jako velmi volné.

## Design

Tohle je moje oblíbené téma, a tak zde budu mít nejvíc položek. Je to taky z toho důvodu, že kontrola designu je pro mne jedním z hlavních cílů _code review_. Kdybych si měl vybrat jenom jeden aspekt, který revidovat, byl by to jednoznačně design.

* **Konceptuální diskuze.** Důvod, proč často zamítnu reviewovaný kód je, že zavádí nějakou konceptuální změnu designu, která nebyla předem diskutovaná. Tohle má dvě složky. Jedna je subjektivní --- mám určité designové preference a jelikož jsem většinou zodpovědný za architektonická rozhodnutí, tak je to moje právo a zodpovědnost. Druhá složka je týmová --- pokud někdo "partizánsky" propašuje změnu, která bude ovlivňovat ostatní členy týmu, je to jasný důvod k zamítnutí. (Jen pro jistotu, _partizánský_ zde má negativní konotace.) Obojí se dá jednoduše řešit zavedením _designových review_, kterých se účastní celý tým a kde se řeší design ještě před implementací.
* **Testovatelnost.** Nejsem [TDD](//en.wikipedia.org/wiki/Test-driven_development) evangelista (v dnešní době?!), ale koncept a zkušenosti s [unit testy](//en.wikipedia.org/wiki/Unit_testing) mne jako vývojáře hluboce ovlivnily. Myslím si, že největší přínos a benefit unit testů je, že mají pozitivní vliv na design produkčního kódu. Kód, který je obtížně testovatelný, je prostě špatný.
* **Konzistence.** Systém/aplikace by měl být konzistentní napříč různými vrstvami, tj. odpovědnost jednotlivých vrstev/komponent, přístup ke zpracování výjimek, používané datové typy (třeba by pomohl [kanonický datový model](//en.wikipedia.org/wiki/Canonical_model)), přístup k logice (objektově, funkcionálně) atd.
* **Znovupoužitelnost.** Na úrovni knihoven, komponent, tříd, metod.
* **[SOLID](//en.wikipedia.org/wiki/SOLID_(object-oriented_design)).** Systém/aplikace by měl respektovat dané/zvolené paradigma. V případě [OOP](//en.wikipedia.org/wiki/Object-oriented_programming) by měl být "SOLIDní". Takže: [Single responsibility](//en.wikipedia.org/wiki/Single_responsibility_principle), [Open-close](//en.wikipedia.org/wiki/Open/closed_principle), [Liskov substitution](//en.wikipedia.org/wiki/Liskov_substitution_principle), [Interface segregation](//en.wikipedia.org/wiki/Interface_segregation_principle), [Dependency inversion](//en.wikipedia.org/wiki/Dependency_inversion_principle). A objektový. Atd.</li>
* **Logování** by mělo být _smysluplné_, _odpovídající_ a se správnou _severitou_ a _formátováním_. Občas mě zaráží, jak málo vývojáři přemýšlí u logování nad tím, že aplikace poběží většinu svého životního cyklu na produkci.
* **Vyvarovat se:** duplicity, komplexity, zanořené logiky (cykly, podmínky), věcí napevno napsaných v kódu (hardcoded). A smrtelně nebezpečné choroby [DIY](//en.wikipedia.org/wiki/Do_it_yourself).

{{< figure src="/2014/07/Dilbert-code-review.gif" caption="Zdroj: Dilbert.com (https://dilbert.com/strips/comic/1995-11-13/)" >}}

## Best-practices

[Best-practices](//en.wikipedia.org/wiki/Best_coding_practices) asi není úplně nejlepší název pro tuto kategorii. A určitě není vyčerpávající a jistě mi leccos propadlo sítem.

* **Kód** by měl být **čitelný** a **srozumitelný**. Čitelný znamená, že po něm "oko dobře plyne", čemuž můžou napomoci konvence. A srozumitelný ve smyslu, že business logika by měla být jednoduše pochopitelná.
* **Externalizace.** Některé věci by v kódu neměly být vůbec: konfigurace, internacionalizace, to co patří do properties, řetězce literálů. Často je něco řešeno konstantama, místo použití [enumů](//en.wikipedia.org/wiki/Enumerated_type).
* **Okomentovaný kód.** Jestli je v kódu [Javadoc](//en.wikipedia.org/wiki/Javadoc) se dá zkontrolovat [statickou analýzou kódu](//en.wikipedia.org/wiki/Static_program_analysis). Jestli jsou ty komentáře aktuální, smysluplné a říkají to, co by měly, to už nám žádný nástroj neřekne. Pokud je kód čitelný a pochopitelný, mělo by v komentáři být popsaný hlavně výjimečné, či překvapující chování.
* **Zakomentovaný kód**. Jednoznačně vyhodit! Už nikdy se nepoužije a bude tam hnít roky.
* **Neadresné TODO.** Podobně jako zakomentovaný kód. Pokud mají vaši vývojáři potřebu si psát do kódu [TODO](//en.wikipedia.org/wiki/Comment_(computer_programming)#Tags), ať se tam aspoň podepíší. Stejně už se k tomu nejspíš nikdy nevrátí. Možná je to moje úchylka, ale nesnáším (měsíce, či roky staré) TODO v produkčním kódu.
* **Komity** do [VCS](//en.wikipedia.org/wiki/Revision_control) by měly být _malé_, _časté_, _smysluplné_ a měly by řešit _pouze jedinou věc_. A měly by mít rozumný komentář, ideálně nastavený konvencí. Když vidím komit/[changeset](//en.wikipedia.org/wiki/Changeset), kde někdo opravil "půlku internetu", otevírá se mi imaginární kudla v kapse.

## Závislosti

Ve zkratce, měli bychom si dát pozor, co nám kdo do aplikace/systému zatáhne. To se týká hlavně externích knihoven, ale také interní závislostí mezi jednotlivými vrstvami a komponentami.

Není to tak dávno, co jsem si tuhle říkal _"proč je ta_ (Java EE) _aplikace tak veliká?"_. Vypíšu si strom závislostí a ona je tam přibalená půlka [Springu](//projects.spring.io/spring-framework/)?!? Uf.

## Pokrytí testy

Přiznám se, jednou jsem dělal na aplikaci, která měla 96% pokrytí testy. Ale jinak, nejsem žádný fanatik přes testy. Nicméně "rozumné" a "dostatečné" pokrytí testy by aplikace měla mít. Zejména business logiky. Naopak, není potřeba testovat platformu, či frameworky.

## A kde je ten checklist?

Jak jsem psal v úvodu, tento článek je zamyšlením, co by v _code review checklistu_ být mohlo. Možná, kdybych přemýšlel dost dlouho, tak bych dal dohromady i nějaký reálný checklist. Ale nechci. Mám rád, když jsou nastavená nějaká pravidla, ale musí umožňovat dostatek volnosti. Aby se dalo dýchat, aby nepotlačovaly invenci a motivaci. Diskuze je daleko důležitější, než mít nějaký papír na odškrtávání.

## Mind map

{{< figure src="/2014/07/Code-review-checklist.png" link="/2014/07/Code-review-checklist.png" >}}
