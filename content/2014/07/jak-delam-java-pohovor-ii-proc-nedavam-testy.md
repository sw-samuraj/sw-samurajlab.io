+++
title = "Jak dělám Java pohovor II: proč nedávám testy?"
date = 2014-07-07T22:33:00Z
updated = 2017-09-18T09:32:52Z
description = """Dávání testů při přijímacích pohovorech považuji
    za hloupost a nepochopení smyslu interview. Zkuste testy
    nedávat, nebo se nad tím aspoň zamyslet - proč to děláte?"""
tags = ["teamleading", "interview", "java"]
aliases = [
    "/2014/07/jak-delam-java-pohovor-ii-proc-nedavam.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2014/07/Interview.jpg" caption="Image courtesy of Michal Marcol" >}}

Je to už nějaký pátek, co jsem napsal (úspěšný) článek [Jak dělám Java pohovor](/2012/11/jak-delam-java-pohovor/). Byl to pro mne výsledný stav určitého vývoje a shrnutí zkušeností z vedení technických (převážně Java) pohovorů, kterých jsem měl tehdy za sebou pár desítek.

Hned od počátku jsem měl štěstí, že mi nikdo nemluvil do toho, jak má interview vypadat. A jsem za tu důvěru vděčný. Taková svoboda mi vyhovuje, takže jsem si jednotlivé kroky pohovoru sestavil a vymyslel podle sebe.

Není to úplně jednoduchá věc, začít takhle z ničeho --- není na to žádný mustr, informací je pomálu, není se moc koho zeptat, protože HR vám nejspíš neporadí a ten kdo dělal technický pohovory před váma, už nejspíš ve firmě nepracuje.

## Proč nedávám testy

Jednu věc jsem věděl jistě --- nechci používat žádné testy. Je s podivem, jak moc jsou testy na pohovorech rozšířené. Protože když uvážíme, jak malou mají, v daném kontextu, vypovídací hodnotu (osobně bych dokonce řekl mizivou) a jak velkou vyžadují režii na údržbu, aby aspoň k něčemu byly; člověk by řekl, že to za to nestojí.

Trochu to chápu. Když už jednou _někdo_ ty testy vytvoří, tak je může dát kandidátovi klidně slečna z HR. On už to pak _někdo_ vyhodnotí. Takže na první pohled se to zdá jako jednoduchý, efektivní a objektivní způsob ohodnocení kandidáta. Ani jedno z toho není pravda.

V první řadě, testy testují něco, na co kandidát téměř jistě nebyl připravený. Je téměř jisté, že ten obskurní syntaktický příklad, který v testu máte, nikdy v životě v praxi nepotkal. Ať už vám ta "chytrá" knížka, nebo internet, podle kterých jste to sestavili, tvrdí cokoliv. A nejde jen o to, že jsou příklady vycucané z prstu, problém je, že se na ně nedá nijak připravit --- když si chci udělat [certifikaci](//www.sw-samuraj.cz/search/label/certifikace), vím, co se potřebuju naučit, jaký bude rozsah, co se dá použít ke studiu apod. Když jdu na pohovor, nevím nic --- dostanu (nejspíš nekvalitní) test a záleží jen na štěstí, nakolik se moje zkušenost z obrovského Java kontinentu kryje s tématy v testu.

Šíře Java landscape je další problém. Co chcete vměstnat do rozumné délky testu? Dejme tomu, že by test měl trvat hodinu, předpokládaný čas na otázku je tři minuty (takže kandidát nad tím nebude moc přemýšlet a jen vysype z hlavy, na co si vzpomene), což vychází na 20 otázek. Kolik otázek věnujete Java SE? Kolik Java EE, kolik Springu? A co něco souvisejícího, třeba databáze, nebo skriptování? Taky máte dojem, že to jen škrábete po povrchu?

Možná si říkáte, zaměříme se jen na technologie, které používáme. OK, používáme na projektech Spring, tak budou testy o Springu. Právě jste se připravili o polovinu schopných lidí, kteří by u vás mohli pracovat. Na druhou stranu, třeba to tak chcete --- jednodruhové, vyšlechtěné, single-malt vývojáře.

Dalším problémem testů je jejich aktuálnost. Jak často vychází [major verze](//en.wikipedia.org/wiki/Software_versioning) nějakého frameworku/platformy? Když vezmu v potaz Java EE (verze 5 až 7), tak za poslední čtyři roky bych takový test musel předělat dvakrát až třikrát (beru v úvahu, že reálné využití se nekryje s [vydáním specifikace](//en.wikipedia.org/wiki/Java_EE_version_history). Kdybych řešil jen Spring (verze 2.5-4.0), jsem na tom podobně.

A zmíním ještě jednu věc. Máte uni-sex testy "one-size-fits-all", které dáváte všem, bez ohledu na úroveň seniority? Máte pro každou senioritu zvláštní test? Jestli chcete dělat testy v rozumné kvalitě, budete s tím mít dost práce.

## Co nějaká výjimka?

V tom, proč nedávat testy bych se mohl pitvat ještě dlouho, ale myslím, že pár zdatných (a dostačujících) důvodů jsem uvedl. Na druhou stranu, přeci jenom, nenašel by se nějaký případ, kdy testy u pohovoru dávají smysl? Přiznám se, nedávno jsem testy taky jednou použil. Či spíše přesněji: akceptoval je a podílel se na nich.

Byl to ale velmi specifický případ. Měl jsem možnost podílet se na [nabírání Java vývojářů na Filipínách](/2014/01/jak-se-nabiraji-javisti-na-filipinach/). Nabírat vývojáře na druhé straně světa není úplně jednoduché. Jádrem přijímacího procesu byl osobní pohovor, kdy za stranu zaměstnavatele se na něm podílely osoby z Česka (já), Francie a USA. Když už si to konečně naplánujete interně, že se sejdete na druhé straně glóbu, je podstatný, aby vám na pohor přišli už jenom relevantní lidé.

První filtrování udělalo místní HR. Jako druhý filtr jsem navrhoval klasický _phone screen_, ale vzhledem k časovému posunu (Česko-Filipíny +7 hodin) jsme se nakonec shodli právě na testech --- hlavně z důvodu jejich asynchronicity. Napsal jsem tedy (s vydatnou a převážnou pomocí kolegy [Bantera](//blog.zvestov.cz/)) test, který by byl takovým "rozumným" filtrem. Opravdu jenom filtrem, nic víc --- pokud někdo neprošel testem, nemělo smysl se s ním vidět osobně.

Na tomto testu jsou podstatné tři věci, které ospravedlňují jeho existenci: kontext, účel a trvanlivost. Kontext je, myslím, jasný --- pohovory na jiném kontinentu zkrátka normálně nedělám. Účel jsem už také zmínil, šlo o pouhý filtr, při celkovém hodnocení kandidáta jsem k testu nijak nepřihlížel. No a "trvanlivost" --- šlo o jednorázovou záležitost, ten test jsem od té doby neviděl, vidět nechci a kdyby náhodou, bylo potřeba řešit najímání vývojářů v Jižní Americe ;-)  začal bych ze stejné pozice: testy raději ne.

## Výzva

Jestli děláte pohovory a dáváte na nich testy --- zkuste se nad těmi testy zamyslet. Dávájí vám to, co od nich očekáváte? Je nějaká jiná cesta, jak dosáhnout stejného výsledku? Troufám si tvrdit, že bez testů se vám podaří najmout kvalitnější lidi. Zkuste to, jen tak se posunete dál.

## Mind Map

{{< figure src="/2014/07/Java-tests-mind-map.jpg"
    link="/2014/07/Java-tests-mind-map.jpg" >}}

## Související články

* [Jak dělám Java pohovor](/2012/11/jak-delam-java-pohovor/)
* [Jak se nabírají Javisti na Filipínách](/2014/01/jak-se-nabiraji-javisti-na-filipinach/)
* [Jak dělají Java pohovor jinde](/2013/02/jak-delaji-java-pohovor-jinde/)
* [Měl by mít vývojář portfolio?](/2013/06/mel-by-mit-vyvojar-portfolio/)
* [Kontrakt místo pohovoru, je to reálné?](/2013/08/kontrakt-misto-pohovoru-je-to-realne/)
* [Geek, který zapadne](/2013/04/geek-ktery-zapadne/)
