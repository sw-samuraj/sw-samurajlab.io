+++
title = "Třetí rok s Kindlem"
date = 2014-03-22T19:31:00Z
updated = 2014-03-27T20:39:28Z
description = """Je to už třetí rok, co čítávám odborné knihy
    výhradně na Kindlu. Letos se docela urodilo - celkem jsem
    přečetl 20 knih, z toho třetinu o leadershipu."""
tags = ["knihy"]
aliases = [
    "/2014/03/treti-rok-s-kindlem.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2014/03/Kindle-Touch.jpg" >}}

Tak už jsou tomu tři roky, co jsem si koupil _Kindle_. [Loni](/2013/03/druhy-rok-s-kindlem/) jsem psal o vynuceném upgradu z _Kindle 3 (Keyboard)_ na _Kindle Touch_ (ani jeden z nich už Amazon neprodává). Letos mne nic takového bohužel a bohudík nepotkalo.

Bohudík, protože mám rád, když věci dobře slouží --- předposlední telefon ([Motorola Razr2](//en.wikipedia.org/wiki/Motorola_Razr2)) jsem měl 5,5 roku a ještě pořád bych ho mohl povolat ze zálohy. A _Kindle Touch_ se mě drží jako klíště a snáší se mnou dennodenní útrapy. Už jsme starý parťáci, tak spolu ještě vydržíme.

Bohužel, protože minimálně čtyři vlastnosti aktuálního _Kindle Paperwhite_ se mi moc líbí:

* je o polovinu lehčí a tenčí než _Touch_,
* má vestavěné osvětlení,
* Vocabulary Builder
* a napojení na [Goodreads](//www.goodreads.com/).

Když už jsme u toho Goodreads, zmigroval jsem tam svůj reading-list, takže se můžete porochnit v [mých poličkách](//www.goodreads.com/vit-kotacka). A pokud tam náhodou jste taky a trpíte podobnou úchylkou jako já --- lunetická potřeba číst a číst a číst, klidně mě kontaktujte. 

Ze ten rok se mi podařilo přečíst 30 knih, taková, řekl bych, všehorodá směska. Následujou ty, které bych označil širokým pojmem "odborné".

## Software Engineering

{{< figure src="/2014/03/Kindle-SW-engineering-books.png"
    link="/2014/03/Kindle-SW-engineering-books.png" >}}

* **[Agile Retrospectives](//pragprog.com/book/dlret/agile-retrospectives)** --- hodně dobrá kniha o (agilních) retrospektivách. Zaměřuje se převážně na retrospektivu _iterace_, ale zmiňuje i rozdílný přístup pro _release_, nebo _projekt_ retrospektivu. Většina knihy obsahuje popis aktivit pro jednotlivé fáze retrospektivy.
* **[Good Math](//pragprog.com/book/mcmath/good-math)** --- říkal jsem si, že se trochu dovzdělám (a oživím si) v matematice. No, není to špatný, ale moc mě to nenadchlo. Tři hlavní témata jsou čísla, [predikátová logika](//en.wikipedia.org/wiki/First-order_logic) a [Turing Machine](//en.wikipedia.org/wiki/Turing_machine) + [Lambda kalkul](//en.wikipedia.org/wiki/Lambda_calculus).
* **[The Agile Samurai](//pragprog.com/book/jtrap/the-agile-samurai)** --- vůbec ne špatný úvod do agilních záležitostí, taková směska praktických věcí. Ale opravdu jenom úvod.
* **[Kanban for Skeptics](//leanpub.com/kanbanforskeptics)** --- není to špatná knížka, když už víte, co je to [Kanban](/2014/03/kanban-lehky-uvod/) a potřebujete někomu vysvětlit, proč to funguje --- systematických argumentů je tu dost. Bohužel se tam nedozvíte nic o tom, co to Kanban je.
* **[Pro JPA 2](//www.apress.com/9781430219569)** --- bible _Java Persistence API 2.0_. Použil jsem ji jako podklad pro JPA certifikaci a můžu ji doporučit i jako referenci. Chybí ale jakákoliv zmínka o integraci se Springem. Krátce se o knize zmiňuju v článku [Certifikace Java EE 6 JPA Developer](/2014/02/certifikace-java-ee-6-jpa-developer/).
* **[Hibernate Search by Example](//www.packtpub.com/hibernate-search-by-example/book)** --- pokud potřebujete zároveň použít [full-text vyhledávání](//en.wikipedia.org/wiki/Full_text_search) spolu s [ORM](//en.wikipedia.org/wiki/Object-relational_mapping), tohle je dobrý zdroj i reference.
* **[Mastering Redmine](//www.packtpub.com/mastering-redmine/book)** --- [Redmine](//www.redmine.org/) je open source [issue tracking](//en.wikipedia.org/wiki/Issue_tracking_system) nástroj, napsaný v [Ruby on Rails](//rubyonrails.org/). Pokud Redmine používáte, nebo spravujete, je tahle kniha tak 12x lepší než oficiální dokumentace (která je mizerná).
* **[The Healthy Programmer](//pragprog.com/book/jkthp/the-healthy-programmer)** --- tahle kniha vám neprozradí tajemství věčného života. Za to vám ale řekne, jak si vytvořit udržitelný životní styl, který vám umožní dělat programátora ještě za 10 (a víc) let. Napsal jsem o tom blog post [Zdravý programátor](/2013/08/zdravy-programator/).
* **[Book of Vaadin](//vaadin.com/book)** (Vaadin 7 Edition) --- pokud fandíte komponentovým frameworkům v Javě (třeba já jo), je [Vaadin](//vaadin.com/home) dobrou volbou (byť je jeho rozšíření na trhu hodně minoritní). _Book of Vaadin_ je oficiální dokumentací, je zdarma a předčí nejednu komerční knihu k danému tématu (kterých moc není). Jediný, co mi v knize chybí, je integrace Vaadinu na další vrstvy a frameworky (Spring, EJB, CDI).
* **[Vaadin 7 Cookbook](//www.packtpub.com/creating-rich-internet-applications-in-vaadin-7/book)** --- jednoznačně nejhorší kniha, kterou jsem loni četl. Nekoncepční, zmatená, s tipy diskutabilní kvality (trošku autory podezírám z absence praxe). Opravdu si v knize za $20 potřebuju přečíst, jak "přinutím" tlačítko, aby se chovalo jako odkaz?!? I když mi knihu koupil zaměstnavatel, byl jsem naštvaný za vyhozený peníze :-(
* **[JBoss AS 7 Configuration, Deployment and Administration](//www.packtpub.com/jboss-as-7-configuration-deployment-administration/book)** --- výborná kniha o sedmičkovém [JBossu](//www.jboss.org/jbossas). _Must have_, pokud to s ním myslíte vážně. Kniha je výborně strukturovaná, dobře se čte, má rozumnou úroveň detailu a dozvíte se z ní o věcech, o kterých se moc nemluví, třeba o [JBoss CLI](//community.jboss.org/wiki/CommandLineInterface), nebo [Infinispanu](//infinispan.org/).
* **[Log4J](//shop.oreilly.com/product/9780596805326.do)** --- kratičký spisek (74 str.) o [Log4J](//logging.apache.org/log4j/1.2/). Mnoha, mnoha vývojářům by prospělo, kdyby si to přečetli (a pochopili ;-)
* **[Mercurial: The Definitive Guide](//mercurial.selenic.com/wiki/MercurialBook)** --- bible [Mercurialu](//mercurial.selenic.com/). Zdarma. Na můj vkus se trochu moc mluví o MQ ([Mercurial Queues](//mercurial.selenic.com/wiki/MqExtension)) a chybí některá pokročilejší témata (třeba trošku [víc o branchování](//stevelosh.com/blog/2009/08/a-guide-to-branching-in-mercurial/)), ale jinak perfektní opus.
* **[Gradle Effective Implementation Guide](//www.packtpub.com/gradle-effective-implementation-guide/book)** --- první a skvělá kniha o [Gradlu](//www.gradle.org/). Ideální začátek --- ačkoliv má Gradle výbornou dokumentaci, pro pochopení kontextu a jak spolu jednotlivé věci fungují, bych doporučil spíš tuhle knihu.

## Leadership

{{< figure src="/2014/03/Kindle-leadership-books.png"
    link="/2014/03/Kindle-leadership-books.png" >}}

* **[Start With Why](//www.amazon.com/Start-Why-Leaders-Inspire-Everyone-ebook/dp/B002Q6XUE4)** --- inspirativní kniha, která trpí repetitivností a zbožňováním Applu. Základní idea je prostá: prvně je potřeba vědět _proč_ něco dělám, z toho vyplyne _jak_ to budu dělat a výsledkem je_ co_ budu dělat. Aplikovatelné na všechno, od osobních věcí, po globální společnosti. Inspirativní jsou ti (jednotlivci i společnosti), kdo mají (a začínají u) definovaný _proč_.
* **[The Power of Habit](//www.amazon.com/Power-Habit-Why-What-Change-ebook/dp/B0055PGUYU)** --- všichni známe sílu zvyku. Málokdo si ale uvědomuje, jak moc nás zvyky ovlivňují (jednotlivce, společnosti i státy či národy). A pokud chceme něco změnit --- u sebe, v týmu, ve společnosti, je potřeba se zvyky počítat. Nebo vytvořit nové a nahradit jimi ty staré.
* **[Tha Last Lecture](//www.amazon.com/Last-Lecture-Randy-Pausch-ebook/dp/B00139VU7E)** --- emocionálně silná výpověď o tom, že _dreams come true_. (Teda pokud zrovna nemáte rakovinu.) A většinou je za tím cílevědomost a tvrdá práce. Jak říká autor: _"I find the best shortcut is the long way, which is basically two words: work hard."_
* **[Team Geek](//www.amazon.com/Team-Geek-Software-Developers-Working-ebook/dp/B008EKF87S%C2%A8)** --- nepostradatelná příručka moderního team leadera. Pokud nemáte ponětí, co team leadování obnáší, nebo se to chcete "naučit", tohle je určitě (mnou) doporučená literatura. Psal jsem o téhle knize v článku [Team Geek, team leader se srdcem](/2013/07/team-geek-team-leader-se-srdcem/).
* **[Steve Jobs: Ten Lessons in Leadership](//www.amazon.com/Steve-Jobs-Ten-Lessons-Leadership-ebook/dp/B0078VZKD6)** --- snůška blábolů a nekritické uctívání Steva, taková mikro hagiografie. Za $3 to rozhodně nestojí a o leadershipu se nedozvíte nic.
* **[Scrappy Project Management](//www.amazon.com/Scrappy-Project-Management-Predictable-Avoidable-ebook/dp/B0011CXNO6)** --- knížka o tom, že projekťák "musí mít koule" (platí obrazně i pro ženy ;-)  Jinak je on i váš tým odsouzen k pomalé, bolestivé (a zasloužené) smrti. Doporučené čtení pro všechny, kdo se jakkoliv motají kolem (softwarových) projektů.

## Související články

* [Druhý rok s Kindlem](/2013/03/druhy-rok-s-kindlem/)
* [Rok s Kindlem](/2012/03/rok-s-kindlem/)
