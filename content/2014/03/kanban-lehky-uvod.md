+++
title = "Kanban, lehký úvod"
date = 2014-03-04T13:46:00Z
updated = 2014-03-04T13:46:10Z
tags = ["kanban", "sw-engineering"]
description = """Po roce používání Kanbanu jsem se rozhodl sepsat úvod
    do Kanbanu. Citace z knih a příklady konkrétních kanban boardů. 🤓"""
aliases = [
    "/2014/03/kanban-lehky-uvod.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2014/03/Kanban-board.jpg" >}}

Rok se sešel s rokem (skoro), takže je nejvyšší čas, říct si zase něco o [Kanbanu](//en.wikipedia.org/wiki/Kanban_(development)) :-)  Přiznám se, v těch [minulých článcích](//www.sw-samuraj.cz/search/label/kanban) jsem to trochu flákal, moc se mi nechtělo do popisu, co to vlastně Kanban je. Tak to napravím.

## Co je to Kanban?

Když mám definovat Kanban jednou větou, obvykle říkám: _Kanban je metoda-nástroj na zefektivnění procesu_. Kdybych měl přidat druhou: _Kanban lze aplikovat na libovolný proces --- je jedno, jestli jde o [Scrum](//en.wikipedia.org/wiki/Scrum_(development)), nebo [vodopád](//en.wikipedia.org/wiki/Waterfall_model)._ A konečně: _Ten proces už musí existovat --- Kanban neříká nic o tom, jak vyvíjet software._

Abych pravdu řekl, nikdy jsem nepřemýšlel na tím, jestli je Kanban využitelný i mimo oblast softwarového vývoje --- jestli ano, tak mě to asi ani moc nezajímá. Jestliže kdekoliv dál v článku mluvím o procesech, mám na mysli procesy, jejichž smyslem je vytvoření, výroba, doručení software.

Abych vás neochudil o plnokrevnou definici, tak [Wikipedia říká](//en.wikipedia.org/wiki/Kanban_(development)):

> _**Kanban** is a _method_ for managing knowledge work with an emphasis on _just-in-time_ delivery while not overloading the team members. In this approach, the process, from definition of a task to its delivery to the customer, is displayed for participants to see and developers pull work from a queue._

A abych vás obohatil ještě o trochu víc, tak David J. Anderson (jeden z duchovních otců Kanbanu a autor [jeho bible](//www.amazon.com/Kanban-David-J-Anderson-ebook/dp/B0057H2M70)) praví:

> _In software development, we are using a _virtual kanban system_ to limit (team's) work-in-progress ... to a set capacity and to balance the demand on the team against the throughput of their delivered work. By doing this we can achieve a sustainable pace of development so that all individuals can achieve a work versus personal life balance._

## Co Kanban (určitě) není?

Lidé mají velmi často pomýlené představy, co to Kanban je. Pojdmě si ho proto definovat i negativně:

* **Kanban není metodologie.** Často se setkávám se snahou srovnávat _Kanban_ a _Scrum_. To je nesmyslná snaha. Kanban neříká vůbec nic o tom, jak máte dělat software. Neříká nic o tom, jak dělat (a jak často) plánování, nebo releasování. Neříká vůbec nic o tom, kdy máte co(koliv) udělat. Neobsahuje žádné ceremonie (denní meetingy, review, statusy projektu atd.). Chcete-li dělat vývoj software metodicky, najděte si nějakou metodiku, Kanban vám v tom nijak nepomůže.
* **Kanban není proces**. Pokud děláte software, musíte mít nějaký proces --- můžete ho mít definovaný explicitně (třeba pomocí nějaké metodiky), nebo implicitně (což může v lepším případě znamenat intuitivně, nebo v tom horším chaoticky). Každopádně, pokud chcete používat Kanban, _musíte už takový proces mít_. Kanban se totiž vždy _aplikuje na proces_, je to taková "poleva".
* **Kanban není agilní praktika.** Kanban bývá často zmiňován ve společnosti agilního vývoje. Což je daný tím, že on se s agilními metodikami velmi dobře pojí, velmi dobře je doplňuje. Ale Kanban stejně tak dobře můžeme aplikovat na krystalicky čistý _vodopád_, či libovolný jiný "rigidní" způsob vývoje.
* **Kanban není nástroj na project management.** Pokud jste projekťák, může vám Kanban pomoci --- asi největším přínosem pro vás bude _předvídatelnost_ procesu a dodávek. Ale nijak vám nepomůže projekt řídit. Neřekne vám nic o potřebných zdrojích, o termínech, nijak nezohledňuje rozpočet projektu atd. Platí to samé jako u procesů a metodik --- Kanban vám neřekne "jak to máte dělat".
* **Kanban není všelék ([stříbrná kulka](//en.wikipedia.org/wiki/No_Silver_Bullet)).** Kanban může některé vaše problémy vyřešit. Ale jenom některé. Lidé jsou nepoučitelní a tak se pořád najdou tací, kteří čekají na zázrak, který nepřichází. Kanban je dostatečně nový (a neznámý), aby sváděl některé manažery a "decision makery" k nereálným očekáváním. Je to ohraná písnička, ale... ani tentokrát mesiáš nepřijde.

## Jaké přináší benefity?

V předešlé sekci jsem popsal, co Kanban není, což může na někoho působit negativně a vzbudit otázky: je to vůbec k něčemu? Nepomůže nám to ani s metodikou, ani s procesem, ani s project managementem. Proč se tím tedy zabývat?

V prezentaci na konci článku těch důvodů najdete víc, já bych z nich vypíchnul tři, které považuji za nejdůležitější:

1. **Kanban odstraní z procesu neefektivitu**. Tohle je jeho kvintesence. Kanban pojímá práci jako proud, plynutí. Vše, co brání plynulosti procesu se snaži odstranit.
1. **Omezením rozpracované práce se zvýší kvalita.** Tím, že se vývojáři (a další role) soustředí (ideálně) pouze na jeden úkol, měla by se zvýšit kvalita jejich výstupů.
1. **Kanban zavádí změny postupně, inkrementálně.** Tohle je zajímavé z "politických" důvodů. Jednak směrem nahoru, k managementu, kdy se drobné změny rozložené v čase prosazují snáze a jednak směrem dolů, k lidem, jež jsou/budou denními účastníky procesu --- lidé bývají vůči změnám rezistentní a konzervativní, takže opět --- drobné změny se spíše skousnou.

## Kořeny Kanbanu

Prapůvod Kanbanu lze hledat v [Toyota Production System](//en.wikipedia.org/wiki/Toyota_Production_System) (TPS). Součástí TPS je i [kanban system](//en.wikipedia.org/wiki/Kanban) --- systém pro řízení [just-in-time](//en.wikipedia.org/wiki/Just_In_Time_(business)) produkce, nicméně (softwarový) Kanban se inspiroval i v jiných aspektech TPS, třeba filozofii [kaizen](//en.wikipedia.org/wiki/Kaizen) (kontinuální zlepšování), nebo konceptu [muda](//en.wikipedia.org/wiki/Muda_(Japanese_term)) (odpad, neproduktivní činnost).

TPS je silně inspirativní systém. S ohledem na softwarový vývoj je jednou z jeho prvních adaptací [Lean Software Development](//en.wikipedia.org/wiki/Lean_software_development) (LSD) --- agilní metodika, která sice není tak populární, jako [Scrum](//en.wikipedia.org/wiki/Scrum_(software_development)), ale třeba pro oblast enterprise určitě vhodnější. Kanban přejímá z LSD některé jeho [nástroje](//en.wikipedia.org/wiki/Lean_software_development#Lean_software_practices), za všechny bych zmínil třeba [teorii front](//en.wikipedia.org/wiki/Queuing_theory).

Kanban také intenzivně čerpá z teorie omezení --- [Theory of Constraints](//en.wikipedia.org/wiki/Theory_of_constraints) (TOC). Jednou z implementací TOC je metodika [Drum-Buffer-Rope](//www.goldratt.co.uk/resources/drum_buffer_rope/) (DBR), která je přímým předchůdcem Kanbanu.

## 5 základních principů

Kanban je definován pomocí pěti základních (core) principů, které si postupně rozebereme.<br />

1. Vizualizuj workflow.
1. Limituj rozdělanou práci.
1. Měr a spravuj flow.
1. Udělej procesní politiky explicitní.
1. Používej modely pro rozpoznání příležitosti ke zlepšení.

### 1. Vizualizuj workflow

Celý Kanban se hodně točí kolem vizualizace a _vizualizace workflow_, patří k tomu nejviditelnějšímu, na co můžeme narazit. Tato technika není ničím specifická, už před vznikem Kanbanu ji využívaly ostatní agilní metodiky.

{{< figure src="/2014/03/Kanban-board-full.jpg"
    link="/2014/03/Kanban-board-full.jpg"
    caption="Fyzický Kanban board" >}}

Princip je jednoduchý:

* Vezmeme tabuli ([Kanban board](//en.wikipedia.org/wiki/Kanban_board)) a rozdělíme ji na vertikální sloupce (swim-lines) podle jednotlivých stavů našeho workflow (tyto stavy se často kryjí se stavy v [issue tracking systému](//en.wikipedia.org/wiki/Issue_tracking_system) (ITS)).
* Práci, kterou potřebujeme udělat, rozdělíme na části (nazývané tasky) stejné, či podobné granularity. Tasky jsou prezentovány lístečky/kartičkami.
* Každý, takto identifikovaný task, umístíme na Kanban board do odpovídající swim-liny.

{{< figure src="/2014/03/Kanban-board-full-2.jpg"
    link="/2014/03/Kanban-board-full-2.jpg"
    caption="Fyzický Kanban board" >}}

_Kanban board_ může mít buď fyzickou, nebo elektronickou podobu. Fyzický board se většinou řeší klasickou bílou tabulí ([whiteboard](//en.wikipedia.org/wiki/White_board)), kam lze lístečky přilepit, připnout magnetem apod. Elektronický board je někdy poskytován [ITS](//en.wikipedia.org/wiki/Issue_tracking_system) (třeba [JIRA Agile](//www.atlassian.com/software/jira-agile/overview) (dříve GreenHopper plugin)), nebo lze použít samostatný nástroj (zkuste [zagooglovat](//www.google.com/search?q=electronic+kanban+board), je jich spousta).

Je lepší použít fyzický, nebo elektronický _Kanban board_? Záleží na kontextu. Fyzický se dobře vyjímá v openspacu a dobře se před ním dělají [stand-up meetingy](//en.wikipedia.org/wiki/Stand-up_meeting). Pozitivní psychologický efekt je, že board je vidět neustále a tak každý člen týmu (i náhodný kolemjdoucí manažer) mají okamžitý přehled o stavu projektu. I z osobní zkušenosti (pokud to jde) doporučuji použít fyzický _Kanban board_.

{{< figure src="/2014/03/GreenHopper-board.png"
    link="/2014/03/GreenHopper-board.png"
    caption="Elektronický Kanban board (JIRA plugin GreenHopper (nyní JIRA Agile))" >}}

Omezením fyzického boardu je jeho, ehm fyzičnost, čili jde použít pouze lokálně. Pokud máte distribuovaný tým, bude elektronický board lepší volba --- alespoň jako primární zdroj pravdy. Fyzický board se dá nicméně v této situaci také využít, byť jen jako sekundární nositel informace.

Další výhodou elektronického boardu je jeho provázanost s ITS. Kanban board pak bývá "pouhým" snapshotem, pohledem na tasky. Tzn. že odpadá duplikování informace a synchronizace v ITS a na fyzickém boardu.

{{< figure src="/2014/03/Kanban-board-Redmine.png"
    link="/2014/03/Kanban-board-Redmine.png"
    caption="Elektronický Kanban board (Redmine)" >}}

### 2. Limituj rozdělanou práci

Všichni to ví, všichni to tak nějak tuší, ale Kanban to říká nahlas: _nedělejte na více věcech najednou_. Vemte si jednu věc, dodělejte ji a vezměte si další. _Nepřepínejte kontext!_

Jak říká David J. Anderson v knize [Kanban](//www.amazon.com/Kanban-David-J-Anderson-ebook/dp/B0057H2M70): _"As we all know, there really is no such thing as multi-tasking in the office; what we do is frequent task switching"._

Přepínání kontextu nemá žádný pozitivní efekt (snad kromě toho, že udržuje mozek v obrátkách). Kanban se k němu staví negativně a otevřeně říká: _nastavte explicitní limit, kolik tasků může být v daném stavu workflow_. Tomuto principu se říká omezení _Work-in-Progress_ (WIP).

Pokud používáme fyzický board, napíšeme limit tasků nad každou swim-line. Kromě maximálního počtu tasků lze nastavit i minimální, tj. pod jaké množství by počet tasků ve flow neměl klesnout. Problém s WIP na fyzickém boardu je zřejmý --- jeho dodržování je potřeba kontrolovat manuálně.

{{< figure src="/2014/03/WIP.jpg"
    caption="Nastavení WIP na fyzickém boardu" >}}

Výhodou elektronického boardu je, že za vás nastavené WIP hlídá. Buď vám vůbec nedovolí tento limit překročit (prostě task nad limit nejde přesunout do "vyčerpaného" stavu), nebo vás aspoň upozorní, že limit byl překročen.

Ať už hlídáme WIP libovolným způsobem, podstatné je, jak se zachováme při jeho překročení. Můžou nastat tři modelové situace:</div>

* Limit WIP máme dobře nastavený. Jeho vyčerpání nám signalizuje [bottleneck](//en.wikipedia.org/wiki/Bottleneck) ve flow. Ten může být buď dočasný, nebo chronický. V prvním případě by se měl zapojit celý tým, aby dočasný, jednorázový problém ostranil. V druhém případě bychom měli zapřemýšlet o změně procesu, flow apod.
* Limit WIP máme dobře nastavený, ale jeho překročení ignorujeme. V takovém případě je Kanban pouze formální záležitostí a je zbytečné ho dělat. Nebo od něj očekávat nějaké pozitivní výsledky.
* Limit WIP máme nastavený příliš vysoko, nebo nízko. Kanban je adaptivní, takže WIP upravíme na přiměřenější hodnoty.

{{< figure src="/2014/03/GreenHopper-WIP.png"
    caption="Nastavení WIP na elektronickém boardu (JIRA plugin GreenHopper)" >}}

Kruciální otázkou je, jak správně WIP nastavit. Je to podobné, jako s odhady pracnosti. Pokud můžeme, vezme v potaz podobný proces z minulosti. Pokud nemáme s čím srovnávat, vezmeme "expertní odhad". Nastavit WIP podle pravidla 1 vývojář, 1 task, taky není špatný začátek. Podstatné není, jestli WIP nastavíme správně na začátku. Ale že ho v průběhu projektu přizpůsobujeme podle aktuálního vývoje situace.

### 3. Měř a spravuj flow

Jedna z věcí, které Kanban převzal z [TPS](//en.wikipedia.org/wiki/Toyota_Production_System) je filozofie [kaizen](//en.wikipedia.org/wiki/Kaizen), kontinuální zlepšování procesu. Abychom věděli, že se proces zlepšuje, je potřeba ho měřit.

Jednou ze stěžejních veličin, kterou Kanban používá (a měří) je [lead time](//en.wikipedia.org/wiki/Lead_time) --- čas, za jak dlouho se task dostane z jednoho stavu do jiného, typicky ze stavu _To Do_ do stavu _Done_. Ale jeden task nás na projektu nevytrhne --- proto Kanban častěji pracuje s kumulovaným _lead time_, tj. za jak dlouho projde flow _průměrný_ task.

Pokud pracujeme s _lead time_, bude nás zajímat, kolik z něj bylo stráveno "produktivní" prací. Protože jeho "neproduktivní" část je to, co Kanban nazývá _waste_ (a TPS [muda](//en.wikipedia.org/wiki/Muda_(Japanese_term))) a tento "odpad", neproduktivní činnost se snaží (postupně) odstranit.

Na druhou stranu, ne vždycky musí být _lead time_, který obsahuje _waste_, špatnou věcí. Protože to tak může být záměrně. Pokud například máme definovaný stav _To Release_, ve kterém nám tasky čakají na... release (protože změny nasazujeme hromadně), může být "neproduktivní" čekání tasku v takovémto stavu v pořádku. Nebo taky nemusí, musíme se zamyslet, jestli třeba změna procesu, která umožní kontinuální nasazování tasků nezlepší jak _lead time_, tak kvalitu celého produktu.

{{< figure src="/2014/03/Kanban-cumulative-flow.jpg"
    link="/2014/03/Kanban-cumulative-flow.jpg"
    caption="Cumulative Flow diagram (zdroj Paul Klipp http://paulklipp.com/images/cfd.jpg)" >}}

Průběh a vývoj flow se v Kanbanu často zobrazuje pomocí [Cumulative Flow diagramu](//en.wikipedia.org/wiki/Cumulative_flow_diagram), což je jeden z nástrojů [teorie front](//en.wikipedia.org/wiki/Queuing_theory). Diagram umožňuje rychlý přehled, jestli jsou dodržováný limity WIP, jestli se v průběhu času zlepšuje lead time a taky "plynulost" našeho systému --- pokud Kanban "pracuje" správně, měly by být jednotlivé pruhy v diagramu hladké a s konstantní výškou.

### 4. Udělej procesní politiky explicitní

Podstatou tohoto kroku je přesvědčení, že pokud definujeme na projektu _explicitní_ definice procesních politik a shodneme se na jejich porozumnění; přinese nám to benefit, že budeme schopni racionálně a objektivně diskutovat nad jednotlivými issues a následně zapracovat na jejich zlepšení, nebo mitigování s nimi spojených rizik.

Příklady takových politik může být:

* Definition of Done (DoD),
* Work-in-Progress (WIP),
* adresování issues,
* eskalační mechanizmus,
* zodpovědnost v jednotlivých procesních stavech.

Konkrétním příkladem takové politiky může být např. _zero-bug-policy_:

> _Když je na projektu nalezen bug (oproti specifikaci), má automatickou prioritu _Critical_ a musí být přednostně opraven. Tj. nezačínáme nové tasky, dokud nejsou opraveny kritické chyby._

To, že jsou politiky explicitní nejspíš znamená, že jsou někde sepsány. To ale nestačí --- pokud jsou pohřbený někde na SharePointu apod., tak jsou v podstatě bezcenné. Naopak, měly by být jednodušše dostupné. Ideální místo je například wiki, která je součástí Issue Tracking systému.

### 5. Používej modely pro rozpoznání příležitostí ke zlepšení

Poslední (a nejvíc zanedbávaný) krok v Kanbanu je opět o vizualizaci --- používá modely procesů a workflow, na základě kterých pak můžeme zlepšit _celkové_ fungování projektu/procesu. Důvod onoho zanedbávání (já to dělám taky) je prostý, jedná se o dost teoretickou záležitost. Modely, které se běžně používají jsou:

* [Theory of Constraints](//en.wikipedia.org/wiki/Theory_of_constraints) (studie [bottlenecků](//en.wikipedia.org/wiki/Bottleneck))
* [System of Profound Knowledge](//www.deming.org/theman/theories/profoundknowledge) (studie odchylek)
* Lean ekonomický model (koncept _waste_)

David J. Anderson k tomu v knize _Kanban_ říká:

> By modeling the workflow of a software development lifecycle as a value stream and then creating a tracking and visualization system to track state changes of emerging work as it „flowed“ through the system, I could see bottlenecks. The ability to identify a bottleneck is the first step in the underlying model for the Theory of Constraints.

## Prezentace

Záměr napsat tenhle článek jsem nepojal jen tak z čista jasna. Chystal jsem se na projekt, kde nyní Kanban používáme. A tak, v rámci sdílení znalostí (i pro získání politické podpory) jsem měl o Kanbanu přednášku u nás v práci:

* SlideShare: [Kanban Overview](//www.slideshare.net/VtKotaka/kanban-27996524)

## Zdroje

* [Kanban](//www.amazon.com/Kanban-David-J-Anderson-ebook/dp/B0057H2M70), David J. Anderson
* [Lean from the Trenches](//pragprog.com/book/hklean/lean-from-the-trenches), Henrik Kniberg
* [Kanban (development)](//en.wikipedia.org/wiki/Kanban_(development)) heslo na Wikipedii

## Související články:

* [Kanban, zprávy z fronty](/2013/01/kanban-zpravy-z-fronty/)
* [Kanban, ultimátní kniha](/2012/08/kanban-ultimatni-kniha/)
* [Lean ze zákopů](/2012/08/lean-ze-zakopu/)
* [Kanban z čistého nebe](/2012/07/kanban-z-cisteho-nebe/)
