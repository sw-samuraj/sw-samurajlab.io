+++
title = "Jak se nabírají Javisti na Filipínách"
date = 2014-01-05T22:56:00Z
updated = 2017-03-10T21:09:12Z
description = """Měl jsem výjimečnou zkušenost - podílet se na zakládání
    nového vývojářského centra... na Filipínách. Neopakovatelný
    zážitek! Aneb, jak jsou na tom filipínští javisti?"""
tags = ["teamleading", "interview", "java"]
aliases = [
    "/2014/01/jak-se-nabiraji-javisti-na-filipinach.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2014/01/Philippines.png" >}}

Tak nějak se přihodilo, že poslední tři, čtyři roky dělám permanentně technické pohovory s Java vývojáři. Za tu dobu to byly desítky pohovorů. V nové práci jsem to povýšil na další stupeň, když jsem začal dělat občasné _remote interview_ přes Skype a mohl si tak popovídat třeba s lidmi z Brazílie, nebo Indie.

Minulý měsíc se to posunulo ještě dál. Naše společnost otevírá nové delivery centrum v [Manile](//en.wikipedia.org/wiki/Manila) na [Filipínách](//en.wikipedia.org/wiki/Philippines). Bude to základna naší divize pro asijské projekty. Jedním z primárních kroků je vytvořit _core team_ --- [technical lead](//en.wikipedia.org/wiki/Lead_programmer), Java vývojář, C# vývojár, integrátor a validator (tester) --- který bude schopný autonomně fungovat a eventuálně se postupně rozrůstat.

Že dělám Java pohovory se o mně (minimálně interně) ví, takže jsem dostal nabídku podílet se na výběru lidí pro nový core team.

## Místo činu

Pokud jste, tak jako já, neabsolvovali zeměpis v 80tých letech (do dneška si pamatuju skoro všechny hlavní města světa), možná vám přijde vhod pár informací pro navození prostředí.

[Filipíny](//en.wikipedia.org/wiki/Philippines) jsou lidnatá země (98 mil. obyv.), s Českem má bezvízový styk, uředními jazyky jsou [Filipínština](//en.wikipedia.org/wiki/Filipino_language) a Angličtina. Hlavním městem je [Manila](//en.wikipedia.org/wiki/Manila), která je jedním ze 16 [cities](//en.wikipedia.org/wiki/Cities_in_the_Philippines#City_classification), které tvoří metropolitní oblast [Metro Manila](//en.wikipedia.org/wiki/Metro_Manila) (12 mil. obyv.). Každá pořádná metropole má svůj [business district](//en.wikipedia.org/wiki/Business_district) --- v případě Manily je to [Makati City](//en.wikipedia.org/wiki/Makati), kde se také nachází pobočka naší firmy.

{{< figure src="/2014/01/Makati-City.jpg"
    caption="Panorama Makati City (https://en.wikipedia.org/wiki/Makati_city)" >}}

Makati je celkem bezpečný region, kde se nemusíte, jako Evropan, bát chodit po ulicích. Nevím, nakolik je to pravda, ale byl jsem místními opakovaně varován, ať si dávám pozor (nebrat si taxíka na letišti, nenosit telefon v ruce, neustále sledovat svoje věci). Plus určitých věcí si člověk všimne sám: taxikář ihned po vašem nastoupení zamkne dveře, časté kontroly zavazadel v budovách, strážní se samopaly v mrakodrapech. A v den, kdy jsem odlétal, [zastřelili na letišti starostu](//www.bbc.co.uk/news/world-asia-25459574) jednoho z filipínských měst. Zkrátka trochu jiný svět, než na který jsem zvyklý.

## Pár nezávazných generalizací

Strávil jsem v Makati jeden týden a byl to můj první výlet mimo Evropu, takže by bylo pošetilé z toho vyvozovat nějaké obecné závěry. Spíš to berte jako něco, s čím se můžete setkat, pokud budete v podobné situaci. Takže...

_Úroveň kandidátů je o řád nižší než v Praze._ Když v Manile někdo řekne, že je senior (a třeba i má odpovídající léta praxe), bude to z našeho pohledu spíš junior. A to co se týče znalostí, zkušeností a bohužel i možností (viz dále). Setkal jsem se třeba s relativně seniorním člověkem, který rutinně psal [unit testy](//en.wikipedia.org/wiki/Unit_testing), ale bez jediného [assertu](//en.wikipedia.org/wiki/Assertion_(computing))!?! Prostě to, že produkční kód provolám, je dostatečný test (vážně, on nevěděl, že asserty existují a byl vděčný, že se na pohovoru něco naučil).

_Asie je továrna na [code monkeys](//en.wikipedia.org/wiki/Code_monkey)._ Slyšeli jste někdy, že by programátoři dělali na směny? Tak na Filipínách je to běžná praxe. Jeden z nejčastějších důvodů pro změnu zaměstnání je, že nechtějí dělat noční směny. Velice často pracují u firem zvučných jmen (Accenture, Oracle, IBM, Logica/CGI), ale pouze jako [offshore](//en.wikipedia.org/wiki/Offshoring#IT-enabled_services_offshoring) [potrava pro děla](//en.wikipedia.org/wiki/Cannon_fodder). Analýza a [QA](//en.wikipedia.org/wiki/Quality_assurance) jsou téměř vždy dělány onshore (z pohledu Filipínců distribuovaně, vzdáleně) a vývojáři dělají jenom mechanickou implementaci tasků, často bez porozumnění principům a souvislostem.

_Holky jsou daleko schopnější, než kluci._ Sám pro sebe jsem si to nazval "Asian Girl Power". Přiznám se, mám (pracovní ;-) slabost pro holky programátorky. Jsou zodpovědné, pečlivé, cílevědomé, odvádějí velmi dobrou práci (true story). Přesně takhle na mne zapůsobila většina kandidátek na pohovorech. Navíc, oproti českému IT, na Filipínách je běžné, že se holky věnují počítačům --- poměr pohlaví na pohovorech byl lehce nakloněn k mužským účastníkům, ale ne nijak výrazně.

_School  matters_. Když dělám pohovory v Česku, tak zcela ignoruju, jakou má kandidát školu. Kolikrát ani neregistruju, jestli vůbec má univerzitní vzdělání. Prostě české zglajchšaltované školství. Ne tak na Filipínách, kde je situace dost podobná anglosaskému modelu. Pokud jste absolvent [University of the Philippines](//www1.up.edu.ph/) (UP), patříte do extra ligy. Ještě ucházející je [Polytechnic University of the Philippines](//www.pup.edu.ph/) (PUP). A pak už je to jedno. Hodně taky záleží na studovaném programu --- pokud místo _Computer Engineeringu_ studujete _Electronics Communications Engineering_, budete na tom, jako vývojář, dost špatně (co se týká znalostí, teorie a vyhlídek).

Zajímavé také bylo, že všichni kandidáti, bez výjimky, měli bakalářský titul. Žádný magistr, nikdo bez univerzitního vzdělání. Většinou je to tak, že po dodělání bakaláře jdou hned pracovat a školu si pak už nedodělávají.

_Všichni Javisti dělají v [Eclipse](//www.eclipse.org/)._ No comment. (bez ironie ;-)

_Místní [Silicon Valley](//en.wikipedia.org/wiki/Silicon_valley) je [Singapur](//en.wikipedia.org/wiki/Singapore)._ Je to tak, pro [Jihovýchodní Asii](//en.wikipedia.org/wiki/Southeast_Asia) je Singapur houba, která nasává ty schopné a telentované. A pokud je nasaje, už se nechtějí vrátit zpět.

_Jídlo je fantastický!_ Nemusíte být fanoušek asijské kuchyně. Ale ta bohatost a rozmanitost chutí je v Evropě nepředstavitelná. Jeden příklad za všechny --- čerstvé mango, oh yeah!! Navíc, filipínská kuchyně je úlně jiná, než thajská, vietnamská, čínská... Pro mne určitě důvod, proč se vracet.

{{< figure src="/2014/01/Krab.jpg"
    link="/2014/01/Krab.jpg"
    caption="Krab (900 g) v thajské čili omáčce" >}}

## Jak dělat interview na druhém konci světa?

Cesta na Filipíny trvá letadlem něco mezi 17 a 22 hodinama. Takže si tam nezaletíte jen tak na otočku. Plus je tady časový posun --- Filipíny leží v časovém pásmu [GMT](//en.wikipedia.org/wiki/Gmt) +8, což znamená dvě věci: jednak budete mít solidní [jet lag](//en.wikipedia.org/wiki/Jet_lag) a jednak se špatně domlouvají _remote interview_.

Když dělám pohovory v Praze, mám s každým kandidátem [phone screen](//randsinrepose.com/archives/the-sanity-chec/) --- je to prvotní filtr, jestli vůbec má smysl se s kandidátem vidět osobně. Vzhledem k časovému posunu Filipín jsme tuto možnost zavrhli a vydali se cestou testu. Nemám rád testy. Nesnáším je jako kandidát a nesnáším je jako pohovorující. Nicméně v tomto výjimečném případě jsem je vzal na milost jako přijatelný způsob screeningu.

Na tomto místě bych rád poděkoval svému kolegovi [Banterovi](//blog.zvestov.cz/), který velkou část testu vytvořil, já jsem do toho jenom tak filozoficko-strategicky kafral. Test je taková směska vytvořená s ohledem na potřebný profil --- takže je tam něco z Java language, Java API, Enterprise Javy, SQL, unit testů, UML a Banterův oblíbený [FizzBuzz](//www.codinghorror.com/blog/2007/02/why-cant-programmers-program.html) ;-)

{{< figure class="floatright" src="/2014/01/Ayala-skycrapper.png"
    link="/2014/01/Ayala-skycrapper.png" width="200" >}}

Pokud byste se náhodou chtěli pouštět do podobného testu a chcete, aby měl nějakou vypovídací hodnotu, nezapomeňte si otázky ohodnotit --- ne všechny mají stejnou váhu. Taky je dobrý si test na někom nanečisto vyzkoušet, abyste ověřili, že je splnitelný --- dal jsem ho kolegovi tady v Praze, seniornímu Javistovi (stihnul to za předpokládanou hodinu, na plný počet bodů).

Výše, v sekci generalizací, jsem psal o nižší úrovni kandidátů. Pro představu, dva nejlepší výsledky z testu byly 17,5 a 16,5 bodu z 24. Průměr byl kolem 10-12 bodů. A výjimečný nebyly ani výsledky se 4 body.

Kandidáti, které jsem viděl na místě, byli profiltrovaný dvěma způsoby --- jednak testem a dále už prošli i HR pohovory. Interview byly naplánovány na celý týden, většinou vycházely tak tři na jeden den. Docela záběr.

Agenda pohovorů byla dosti podobná tomu, jak už jsem popisoval v článku [Jak dělám Java pohovor](/2012/11/jak-delam-java-pohovor/). Od té doby jsem to zase o něco vylepšil (o tom někdy příště), ale pro filipínské prostředí jsem to upravil do podoby:

1. Představení pohovorujících.
1. Představení naší firmy.
1. Představení kandidáta.
1. Technické otázky.
1. Java workshop.

První tři body, předpokládám, není potřeba vysvětlovat. Co se ale skrývá pod termíny _technické otázky_ a _Java workshop_?

## Technické otázky

_Technické otázky_ začínám nabídkou: _"Můžete mi popsat Váš poslední, nebo poslední **úspěšný** projekt, z technického pohledu?"_ Je to pozvolný přechod od CV k technickým záležitostem. Co mě zajímá, je:

* _Architektura daného projektu_ --- chci ji namalovat (ideálně na tabuli, nebo aspoň na papír) a popsat. Je zde široké pole pro diskuzi --- nad komponentami (technologie), vztahy mezi nimi (protokoly), plusy a mínusy, performance, v jakém kontextu je aplikace vyvíjena atd.
* _Jak vypadal tým a jakým způsobem byl řízen._ Sem spadají role v týmu, metodologie a procesy, komunikace v rámci týmu a mimo tým, daily work atd.
* _Jak vypadalo řízení projektu z technického pohledu?_ Čili opět metodiky a procesy, ale i správa [issues](//en.wikipedia.org/wiki/Software_project_management#Issue) a nástroje na podporu (a automatizaci) projektu, [release management](//en.wikipedia.org/wiki/Release_management) atd., atd.

Hlavním cílem "projektové" otázky je zjistit, jak je kandidát zakotven v [softwarovém inženýrství](//en.wikipedia.org/wiki/Software_engineering). Pokud není a říká o sobě, že je senior, má u mne docela vážný problém.

Pak už následují čistě technické otázky, které se odvíjejí od kandidátova CV a praxe. Zpravidla se ptám na věci, které by měl kandidát dobře znát. A naopak --- neptám se ho na věci, s nimiž nemá zkušenost, byť je bude v budoucí práci u nás potřebovat. Nejčastěji se otázky točí kolem nějaké komponety, či technologie z oblasti "enterprise" Javy, takže buď něco z [Java EE](//en.wikipedia.org/wiki/Java_Platform,_Enterprise_Edition), nebo ze [Springu](//en.wikipedia.org/wiki/Spring_Framework).

Někdy se zeptám na věci, které jsou sice minoritně důležité, ale zaujmou mě (catch my eye) --- třeba [SoapUI](//www.soapui.org/), nebo [Selenium](//docs.seleniumhq.org/). Třeba o tom něco ví. Nebo si to tam dali jen tak pro zajímavost. Tak, nebo tak, zajímá mne, jak se k tomu v odpovědi postaví.

Ať už je otázka jakákoliv, začínám zvolna. _"Jaký je hlavní princip za technologií XY? Jak to funguje?"_ A pak jdeme postupně hloubš a hloubš. Na určité úrovni se pak kandidátovy (nebo moje :-) znalosti zastaví. Což je moje ryska na stupnici kandidátovy seniority. A další otázka podobným způsobem. Většinou jsou takové technologické otázky 2 až 3 a strávíme tím zhruba půl hodiny.

### Java workshop

{{< figure class="floatright" src="/2014/01/Ayala-skycrapper-2.png"
    link="/2014/01/Ayala-skycrapper-2.png" width="200" >}}

Ohledně _Java workshopu_ se nebudu moc rozepisovat a odkážu vás na budoucí článek. Obecně jde o to, že s kandidátem strávím cca půl až hodinu programováním a diskuzí nad kódem v [IDE](//en.wikipedia.org/wiki/Integrated_development_environment).

Zadáním je [UML](//en.wikipedia.org/wiki/Unified_Modeling_Language) diagram "určitého abstraktního řešení" (omlouvám se, zatím nebudu konkrétní). Pokud ho kandidát zná, může ho zkusit vysvětlit. Pokud nezná, vysvětlím ho já. Pak otevřu notebook a to co je na diagramu může kandidát vidět jako kostru projektu v IDE.

Kandidát by měl řešení naimplementovat a napsat k němu [unit testy](//en.wikipedia.org/wiki/Unit_testing). To, jak kandidát píše kód a jak dobře ovládá IDE je podstatné a zajímavé. Ale daleko důležitější je, jak se mnou komunikuje. Zadání je totiž dosti vágní a tak záleží na kandidátovi, na co se zeptá, s jakým přijden nápadem apod. Jak reaguje na otázky, které průběžně pokládám.

Celá seance by měla ideálně připomínat [párové programování](//en.wikipedia.org/wiki/Pair_programming), nebo [code review](//en.wikipedia.org/wiki/Code_review). Mým cílem je, aby kandidát nebyl ve stresu a v nejlepším případě si to i užil. Ne vždy se to podaří. Ale to je v pořádku, protože to co hledám, není jen technické ohodnocení.

Minimálně stejně důležitý je i _cultural fit_. Takže pokud někdo nepíše testy, neumí diskutavat nad designem a neumí zdůvodnit své rozhodnutí/implentaci, vidím to jako závažný problém. Stejně tak, pokud někdo dělá pět let v nějaké IDE a neumí ho adekvátně ovládat (fakt se najdou senioři, kteří neznají _Ctrl+Space_).

(Více o Java workshopu napíšu ve slibovaném článku, kde bych se chtěl podívat na to, proč si myslím, že je to dobrý způsob, jak si otestovat kandidáta.)

## Po interview

Tak a máme po interview. Možná měl kandidát ještě nějaké otázky, pár zdvořilostí na závěr a hotovo! Ne tak docela. Je potřeba si kandidáta ohodnotit, možná udělat předběžné rozhodnutí. Pokud je víc intreview v řadě, je dobré si kandidátovo hodnocení nějak "znormalizovat" a zadasadit do relativního kontextu k ostatním --- pokud nabírám (i průběžně) více lidí na stejnou pozici, stačí mi, že kandidát překročí určitou laťku. Pokud ale obsazuji jednu pozici z více kandidátů, je dobré mít možnost nějak si je porovnat.

Já jsem si na Filipínách udělal jednoduchou tabulku:

{{< figure src="/2014/01/Candidates-evaluation.png" >}}

To je, čistě za mne, hodnocení technického pohovoru. Což je, samozřejmě, pouze část přijímacího a rozhodovacího procesu. Je to podklad, který společně s mými poznámkami na CV používám pro diskuzi a argumenaci s ostatním členy pohovorujícího týmu.

## A jak to dopadlo?

Možná čekáte, že vám teď řeknu, kolik kanditátů jsme přijali, jaký je jejich profil atd. Neřeknu. Jednak, tenhle článek není o tom, jak se staví core team, ale jak vypadá interview v mém pojetí. A pak, najmout lidi do týmu není otázka jednoho týdne pohovorů --- je tam spousta dalších aspektů a i časově je to daleko náročnější.

Nicméně, pokud by vás takové téma zajímalo, můžu vás navndadit na jeden z budoucích článků, který bude o tom, jak se (v Česku) staví tým vývojářů. Zůstaňte na příjmu!

{{< figure src="/2014/01/Jeepney.jpg"
    link="/2014/01/Jeepney.jpg"
    caption="Jeepney, tradiční veřejná doprava na Filipínách (https://en.wikipedia.org/wiki/Jeepney)" >}}

## Související články

* [Jak dělám Java pohovor](/2012/11/jak-delam-java-pohovor/)
* [Jak dělají Java pohovor jinde](/2013/02/jak-delaji-java-pohovor-jinde/)
* [Měl by mít vývojář portfolio?](/2013/06/mel-by-mit-vyvojar-portfolio/)
* [Kontrakt místo pohovoru, je to reálné?](/2013/08/kontrakt-misto-pohovoru-je-to-realne/)
