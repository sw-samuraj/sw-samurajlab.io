+++
title = "Master your tools!"
date = 2011-05-03T14:42:00Z
updated = 2012-04-10T16:14:53Z
description = "Master your tools! Nový blog o softwarovém inženýrství."
tags = ["blogging", "sw-engineering"]
aliases = [
    "/2011/05/master-your-tools.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/img/samurai.gif" >}}

Už od malička jsem vášnivě četl. Časem se k tomu přidala neodbytná touha a potřeba po vzdělá(vá)ní. A kromě toho, že dělám práci, která mě baví, je její neoddělitelnou součástí (pro mne radostná) nutnost "učit se nové věci".

Protože jsem nechtěl zakládat další (tuctový) blog o Javě (která je momentálně mojí hlavní pracovní náplní) ani jsem se nechtěl svazovat nějakou konkrétní technologií, rozhodl jsem se psát o čemkoliv z oblasti SW inženýrství a brát to jako "ostření nástrojů", které se mi jednou budou hodit.
