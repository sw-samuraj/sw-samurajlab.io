+++
title = "Currying"
date = 2011-02-03T18:00:00Z
updated = 2017-04-04T11:18:08Z
description = """Currying je zajímavý koncept funkcionálního programování,
    dostupný v mnoha jazycích. Jak se používá v Groovy a v Clojure?"""
tags = ["groovy", "clojure", "functional-programming"]
blogimport = true 
[author]
	name = "Vít Kotačka"
	uri = "https://www.blogger.com/profile/12147991495120816879"
+++

{{< figure class="floatleft" src="/2011/02/Groovy-dsl.jpg"
    link="/2011/02/Groovy-dsl.jpg" width="200" >}}

Čtu teď knížku [Groovy for Domain-Specific Languages](//www.amazon.com/Groovy-Domain-Specific-Languages-Fergal-Dearle/dp/184719690X) a zaujala mě tam technika, kterou jsem doposud neznal --- [currying](//en.wikipedia.org/wiki/Currying). V krátkosti jde o transformaci funkce, která má více argumentů (nebo pole argumentů) tak, že může být zavolána jako řetěz funkcí s jedním argumentem. Zapsáno matematicky: `g(y) = (y -> f(x,y))`. Funkce `g` je "(z)kariovaná"  (_curried_) verze funkce `f`.

Pojem _currying_ je spojován hlavně se jménem matematika [Haskella Curryho](//en.wikipedia.org/wiki/Haskell_Curry). Ano, je to ten Haskell, po kterém je pojmenován (čistě) funkcionální jazyk [Haskell](//en.wikipedia.org/wiki/Haskell_(programming_language)). _Currying_ je vlastní v podstatě všem funkcionálním jazykům, za zmínku stojí speciálně Haskell a [ML](//en.wikipedia.org/wiki/ML_(programming_language)), kde všechny funkce mají přesně jeden argument, tj. jsou "kariované". "Kariované" funkce by také měly jít použít/vytvořit ve všech jazycích, které podporují [closures](//en.wikipedia.org/wiki/Closure_(computer_science)), namátkou:

* [Scala](//www.scala-lang.org/node/135)
* [Haskell](//www.haskell.org/haskellwiki/Currying)
* [Ruby](//www.khelll.com/blog/ruby/ruby-currying/)
* [Groovy](//www.ibm.com/developerworks/java/library/j-pg08235/index.html), [Groovy API](//groovy.codehaus.org/api/groovy/lang/Closure.html#curry(java.lang.Object[]))
* [JavaScript](//www.dustindiaz.com/javascript-curry/)

No a jak vypadá _currying_ konkrétně? V _Groovy_:

{{< gist sw-samuraj dbe92816a7a7b208f102bf3b90149af4 >}}

No a tenhle blog je o _Clojure_, takže jak to vypadá v _Clojure_?

{{< gist sw-samuraj 1542b459b1f1f0db36bd083cc52d20e7 >}}

V _Clojure_ není potřeba syslit argumenty po jednom, ale dá se jich zadat víc naráz (1-n):

{{< gist sw-samuraj a219f596786ac16b930ee6b936ff0708 >}}

Závěrem, k čemu je vlastně _currying_ dobrý? Kromě toho, že může zpřehlednit/zjednodušit funkce s mnoha argumenty ([lambda kalkul](//en.wikipedia.org/wiki/Lambda_calculus) v matematice), je hlavní výhoda použití v doplňování argumentů _on-the-fly_ --- doplním argumenty, které aktuálně znám a ostatní doplním, až budou k dispozici. Vzhledem k tomu, že _Clojure_ je [lazy-init](//en.wikipedia.org/wiki/Lazy_initialization), může být _currying_ konvenující metoda.
