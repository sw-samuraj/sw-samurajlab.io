+++
title = "Uzávěr v laboratoři (jak začít)"
date = 2011-02-12T13:57:00Z
updated = 2017-04-11T08:49:45Z
description = """Způsobů, jak si vyzkoušet Clojure je celá řada - od online
    interpreterů a tutoriálů, až po lokálně zpustitelný "labrepl", tutoriál
    zpustitelný z REPL."""
tags = ["clojure"]
blogimport = true 
[author]
	name = "Vít Kotačka"
	uri = "https://www.blogger.com/profile/12147991495120816879"
+++

{{< figure class="floatleft" src="/img/logos/Clojure.png" width="160" >}}

Dneska bych tady měl dvě "drobnosti" pro ty, kdo chtějí s _Clojure_ začít. Prvně to kratší: Chcete zkusit _Clojure_ a přitom nic neinstalovat, jen si vyzkoušet jazyk? Na stránce [Try Clojure](//www.try-clojure.org/) je k dispozici online [REPL](//en.wikipedia.org/wiki/REPL), včetně krátkého tutoriálu. Jako perlička --- k dispozici je [Google Chrome](//www.google.com/chrome) rozšíření [try-clojure-in-chrome](//chrome.google.com/extensions/detail/lhmgejcdhmollecbianopflcfdaennle), které zmíněnou stránku využívá pro zobrazení _REPLu_ v _Chrome_.

~~[Updated: Zdá se, že posledních pár dní je stránka [Try Clojure](//try-clojure.org/) mrtvá. :-/ ]~~

Hlavní věc, o které bych ale dnes chtěl psát, je výborný _Clojure_ tutoriál [labrepl](//github.com/relevance/labrepl). Tutoriál vytvořil [Stuart Halloway](//twitter.com/stuarthalloway), autor vůbec první knihy o _Clojure_ [Programming Clojure](//pragprog.com/titles/shcloj/programming-clojure) a také jeden z hlavních contributorů do _Clojure_ samotného.

Musím říct, že _labrepl_ mě vyloženě nadchnul a to jak obsahem, tak implementací. Celý tutoriál je totiž v _Clojure_ jak napsaný (HTML je generovaný pomocí _Clojure_ web frameworku [Compojure](//github.com/weavejester/compojure)), tak zbuildovaný (konrétně [Leiningenem](//github.com/technomancy/leiningen), o kterém bych chtěl něco napsat příště). Tutoriál se spustí jako _REPL_, přičemž v rámci něho na pozadí běží web server [Jetty](//jetty.codehaus.org/jetty/) s HTML tutoriálem a v již spuštěném _REPLu_ se dají zkoušet jednotlivé příklady. Tento neohrabaný popis asi není moc názorný, takže radši doporučuji přímo vyzkoušet.

Nebudu tady popisovat obsah tutoriálu, jen bych zmínil věc, na kterou jsem zatím online nenarazil. A to je dokumentace přímo v _REPLu_. To je další vlastnost, která mne nadchla --- zatím jsem se nesetkal s jazykem (s částečnou výjimkou v [Pythonu](//www.python.org/)), který by tak snadno a v takovém rozsahu něco podobného poskytoval.

Dokumentace funkce:

```clojure
(doc zipmap)
; -------------------------
; clojure.core/zipmap
; ([keys vals])
;   Returns a map with the keys mapped to the corresponding vals.
```

Vyhledání dokumentace:

```clojure
(find-doc "zip")
; -------------------------
; clojure.zip/seq-zip
; ([root])
;   Returns a zipper for nested sequences, given a root sequence
; -------------------------
; clojure.zip/vector-zip
; ([root])
;   Returns a zipper for nested vectors, given a root vector
; -------------------------
; clojure.zip/xml-zip
; ([root])
;   Returns a zipper for xml elements (as from xml/parse),
;   given a root element
```

Funkce `javadoc` otevře stránku [Javadocu](//en.wikipedia.org/wiki/Javadoc) v prohlížeči (buď lokální, nebo vzdálenou):

```clojure
(javadoc java.util.Date)
; "http://java.sun.com/javase/6/docs/api/java/util/Date.html"
; Created new window in existing browser session.
```

A nakonec funkce, která je nejvíc cool! `source` zobrazí implementaci dané funkce:

```clojure
(source drop)
; (defn drop
;   "Returns a lazy sequence of all but the first n items in coll."
;   {:added "1.0"}
;   [n coll]
;   (let [step (fn [n coll]
;                (let [s (seq coll)]
;                  (if (and (pos? n) s)
;                    (recur (dec n) (rest s))
;                    s)))]
;     (lazy-seq (step n coll))))
```
