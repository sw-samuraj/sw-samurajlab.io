+++
title = "Sní androidi o Clojure?"
date = 2011-02-09T12:56:00Z
updated = 2017-03-18T21:54:39Z
description = """Jak to vypadá s podporou Clojure na Adroidu?"""
tags = ["clojure"]
blogimport = true 
[author]
	name = "Vít Kotačka"
	uri = "https://www.blogger.com/profile/12147991495120816879"
+++

{{< figure class="floatleft" src="/img/logos/Clojure.png" width="120" >}}

Jsem asi poslední javista, který ještě nemá telefon s [Androidem](//en.wikipedia.org/wiki/Android_(operating_system)). Samozřejmě, že si ho jednou pořídím --- je mi zkrátka sympatičtější než [iOS](//en.wikipedia.org/wiki/IOS_(Apple)) nebo [Symbian](//en.wikipedia.org/wiki/Symbian). [Billův paskvil](//en.wikipedia.org/wiki/Windows_mobile) radši ani nebudu zmiňovat jménem :-) Každopádně mě napadlo, jak je na tom _Android_ s _Clojure_ (nebo naopak?).

Dobrá zpráva je, že _Clojure Core_ počítá s [podporou Androidu](//archive.clojure.org/design-wiki/display/design/Android%2BSupport.html). Doslova je uvedeno _"Clojure should run well there."_. Horší je, že start aplikace zatím není příliš svižný (_sluggish_). Taky velikost aplikace je řádově rozdílná: _Java_ 20 KB, _Clojure_ 4 MB :-(

Nicméně jsem optimista --- až si toho Androida jednou koupím, _Clojure_ na něm bude svištět jako vítr :-)
