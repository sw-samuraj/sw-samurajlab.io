+++
title = "First mission"
date = 2011-02-02T12:43:00Z
updated = 2017-03-18T22:03:28Z
description = """Vyzkoušel jsem si první reálnou úlohu v Clojure - produktivita
    nula, ale učící křivka mocně poposkočila. 😅"""
tags = ["clojure"]
blogimport = true 
[author]
	name = "Vít Kotačka"
	uri = "https://www.blogger.com/profile/12147991495120816879"
+++

{{< figure class="floatleft" src="/img/logos/Clojure.png" width="160" >}}

Tak mám za sebou první prográmek v Clojure. Kolega potřeboval "přestrukturovat" data v [CSV](//en.wikipedia.org/wiki/Comma-separated_values) souboru a tak jsem si řekl, že je to vhodná příležitost vyzkoušet si Clojure v akci. V principu šlo o následující problém: zdrojová data vypadala nějak takto:

{{< figure src="/2011/02/Telco-hilfe-1.png" >}}

Cílova data takto:

{{< figure src="/2011/02/Telco-hilfe-2.png" >}}

Největší problém na který jsem narazil, byla produktivita. Zatímco v [Groovy](//groovy.codehaus.org/) bych měl úlohu vyřešenou cca za dvě hodiny, v Clojure mi to trvalo dva dny. Na druhé místo bych umístil [I/O operace](//en.wikipedia.org/wiki/I/o) --- postupy uvedené na webu nejsou aktuální vůči stávající verzi Clojure **1.2**, takže malá ukázka:

Makro `with-open` je použito, aby se otevřené streamy na konci zavřely (automaticky zavolá `.close`), instance `readeru` a `writeru` jsou [BufferedReader](//download.oracle.com/javase/6/docs/api/java/io/BufferedReader.html) a [BufferedWriter](//download.oracle.com/javase/6/docs/api/java/io/BufferedWriter.html).

```clojure
; read file
(with-open [reader (clojure.java.io/reader "source.csv"
                   :encoding "Cp1250")]
    (println (line-seq reader)))
; write file

(with-open [writer (clojure.java.io/writer "target.csv"
                   :encoding "ISO8859_2")]
    (.write writer "string"))
```

A za třetí bych uvedl svoji neznalost "funkcionálního způsobu myšlení". Přece jenom, byl to boj, pracovat s neměnitelnými [datovými strukturami](//clojure.org/data_structures). Vyzkoušel jsem si při tom trochu práci s [Vars](//clojure.org/Vars).

_Vars_ poskytují mechanismus jak odkazovat na úložiště, které se může dynamicky měnit. Pomocí [formu](//clojure.org/special_forms) `def` vytvořím (globální) _Var_ objekt, který inicializuju nějakou hodnotou. Pokud chci, aby vytvořený objekt dočasně odkazoval na jinou hodnotu, můžu ho, _v rámci jednoho threadu_, přesměrovat na úložiště s jinou hodnotou pomocí [makra](//clojure.org/macros) `binding`:

```clojure
(def x 42)
; #'user/x
x
; 42
(binding [x 12] x)
; 12
x
; 42
```
