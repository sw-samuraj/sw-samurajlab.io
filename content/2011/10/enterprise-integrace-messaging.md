+++
title = "Enterprise integrace, messaging"
date = 2011-10-10T15:35:00Z
updated = 2016-12-02T22:14:45Z
description = """Messaging může být jedním ze způsobů integrace enterprise
    komponent. Jaké jsou základní koncepty messagingu? (A knížka jako bonus.)"""
tags = ["soa", "messaging", "integrace", "knihy", "eip", "wmq"]
aliases = [
    "/2011/10/enterprise-integrace-messaging.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2011/10/EIP-book.jpg"
    link="/2011/10/EIP-book.jpg" width="200" >}}

Dostal jsem se jako teamleader na integrační projekt, založený na proprietárním řešení/technologii. To proprietární (o kterém nechci psát) je nicméně postaveno nad [WebSphere Message Brokerem](//en.wikipedia.org/wiki/IBM_WebSphere_Message_Broker) (WMB). Právě kvůli WMB, jsem se pustil do čtení výborné knížky [Enterprise Integration Patterns](//www.amazon.com/Enterprise-Integration-Patterns-Designing-ebook/dp/B000OZ0N9E/ref=tmm_kin_title_0?ie=UTF8&amp;m=AG56TWVU5XWC2&amp;qid=1316526108&amp;sr=8-1) (EIP). A jelikož je pro mne jak WMB, tak EIP nové, rozhodl jsem se o tom napsat (v rámci studia) pár článečků. Takže...

## Základní koncepty messagingu

WMB je, jak napovídá název, založený na [messagingu](//en.wikipedia.org/wiki/Message-oriented_middleware), takže se prvně podíváme, co to ten _messaging_ je a na čem jsou postaveny jeho základy (inspirováno a citováno z [EIP](//www.amazon.com/Enterprise-Integration-Patterns-Designing-ebook/dp/B000OZ0N9E/ref=tmm_kin_title_0?ie=UTF8&amp;m=AG56TWVU5XWC2&amp;qid=1316526108&amp;sr=8-1)). Vezměme si následující obrázek, který obsahuje všechny základní koncepty messagingu:

{{< figure src="/2011/10/Integration-concepts.png"
    link="/2011/10/Integration-concepts.png"
    caption="Příklad messaging integrace pomocí základních konceptů" >}}

* **Messages** (zprávy). Kolem zpráv se to celé točí. Zprávy jsou atomickým paketem dat, která jsou přenášeny z jednoho systému do druhého. Každá zpráva se skládá z hlavičky a z těla (nic překvapivého). Zprávy mají často hierarchickou podobu (např. XML, nebo struktura Java objektů).
* **Channels** (kanály). Virtuální "trubky", kterými tečou zprávy. Kanály jsou _jednosměrný_ --- jedna aplikace do kanálu zapisuje a druhá z něj čte.
* **Endpoints** (koncové body). Rozhraní pro připojení aplikace k [messagingovému systému](//en.wikipedia.org/wiki/Message_oriented_middleware). Umožňuje aplikaci posílat a přijímat zprávy. Instance endpointu je svázána s konkrétním kanálem --- z toho vyplývá, že může zprávy buď přijímat, nebo odesílat (ne obojí najednou).
* **Routing** (směrování). Pokud máme hodně aplikací a kanálů, může být tok zprávy velmi komplikovaný. Namísto aby se o její trasu musel starat odesílatel, pošle ji do _message routeru_, který se postará o navigaci zprávy skrze topologii kanálů tak, aby dorazila k příjemci (známe z TCP/IP).
* **Pipes and Filters** (trubky a filtry). Pokud potřebujeme se zprávou během jejího toku nějak pracovat, je vhodné ji přes několik kanálů prohnat potřebnými škatulkami --- princip, který dobře znají uživatelé Unixu (`ls -l /dev/ | grep sd[a-f] | wc -l`), nebo električtí kytaristé (kytara &rarr; ladička &rarr; overdrive &rarr; chorus &rarr; kombo).
* **Transformation** (transformace). To jsou ty krabičky. Potřebuju konvertovat data z jednoho formátu do jiného? Potřebuju zprávu něčím obohatit? Potřebuju změnit transportní protokol? Použiju odpovídající transformaci/krabičku.

Jak je vidět z uvedeného přehledu základních principů, jde o v podstatě triviální koncepty. Přínosem výše uvedené EIP knížky je, že jednak tyto principy probírá do hloubky a v různých variantách, jednak je uvádí komplexně (můžou být věci, které člověku na první, druhý, třetí pohled nedojdou) a jednak je kombinuje do důmyslných a praxí ověřených řešní (přeci jenom, jsou to vzory).
