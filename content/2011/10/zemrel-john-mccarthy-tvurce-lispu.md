+++
title = "Zemřel John McCarthy, tvůrce Lispu"
date = 2011-10-27T21:01:00Z
updated = 2012-04-28T16:15:01Z
description = """24. října 2011 se uzavřela životní cesta Johna McCarthyho,
    tvůrce Lispu."""
tags = ["clojure", "lisp", "sw-engineering"]
blogimport = true 
[author]
	name = "Vít Kotačka"
	uri = "https://www.blogger.com/profile/12147991495120816879"
+++

{{< figure class="floatleft" src="/2011/10/John-McCarthy.jpg" width="360" >}}

V pondělí 24. října zemřel ve věku 84 let vynálezce [Lispu](//en.wikipedia.org/wiki/Lisp_(programming_language)) [John McCarthy](//en.wikipedia.org/wiki/John_McCarthy_(computer_scientist)). Je zajímavé, že McCarthy Lisp pouze navrhl --- v roce 1958 publikoval článek [Recursive Functions of Symbolic Expressions and Their Computation by Machine](//www-formal.stanford.edu/jmc/recursive/recursive.html). Jazyk samotný byl později implementován [Stevem Russellem](//en.wikipedia.org/wiki/Steve_Russell).

Zajímavé také je, že McCarthy původně používal  [M-expressions](//en.wikipedia.org/wiki/M-expression) (meta expressions) v hranatých závorkách, které se ovšem dají přeložit na klasické "lispovské" [S-expressions](//en.wikipedia.org/wiki/S-expression) (symbolic expressions). Když byl ale Lisp implementován, začali programátoři používat právě _s-expressions_ a tak McCarthy od používání _m-expressions_ upustil.

McCarthy se do historie nezapsal pouze jako tvůrce Lispu, ale i mnoha dalšími počiny, např. vynálezem [garbage collection](//en.wikipedia.org/wiki/Garbage_collection_(computer_science)) (mmj. právě kvůli problémům ohledně Lispu), nebo zavedením termínu [Artificial Intelligence](//en.wikipedia.org/wiki/Artificial_Intelligence).
