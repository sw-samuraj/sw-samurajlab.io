+++
title = "PragPub píše o ClojureScriptu"
date = 2011-08-10T16:21:00Z
updated = 2017-03-18T21:56:22Z
description = """Můj oblíbený e-časopis PragPub obsahuje článek o ClojureScriptu. 👍"""
tags = ["clojure"]
blogimport = true 
[author]
	name = "Vít Kotačka"
	uri = "https://www.blogger.com/profile/12147991495120816879"
+++

{{< figure class="floatleft" src="/2011/08/PragPub-2011-August.jpg"
    link="/2011/08/PragPub-2011-August.jpg" width="200" >}}

V uplynulých dnech se mojí ([Google Reader](//reader.google.com/)) čtečkou prohnala smršť zpráviček a anoncí o [ClojureScriptu](//github.com/clojure/clojurescript). Nevím, jestli to není jen hype, který se kumuluje kolem [CoffeeScriptu](//jashkenas.github.com/coffee-script/). Navíc _ClojureScript_ je již nějaký čas součástí knihovny [clojure-contrib](//github.com/clojure/clojure-contrib/).

Každopádně [aktuální číslo](//pragprog.com/magazines/2011-08/content) mého oblíbeného e-časopisu [PragPub](//pragprog.com/magazines) obsahuje [článek o ClojureScriptu](//pragprog.com/magazines/2011-08/hello-clojurescript). Kromě HTML jsou k dispozici i formáty [mobi](//pragprog.com/magazines/download/26.mobi), [epub](//pragprog.com/magazines/download/26.epub) a [PDF](//pragprog.com/magazines/download/26.PDF).
