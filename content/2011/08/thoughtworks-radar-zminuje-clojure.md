+++
title = "ThoughtWorks Radar zmiňuje Clojure"
date = 2011-08-16T14:20:00Z
updated = 2017-04-02T22:52:16Z
description = """Firma ThoughtWorks nedávno zveřejnila svůj Technology Radar
    a hned třikrát tam zmínila Clojure. To nebude náhoda."""
tags = ["clojure"]
blogimport = true 
[author]
	name = "Vít Kotačka"
	uri = "https://www.blogger.com/profile/12147991495120816879"
+++

{{< figure class="floatleft" src="/2011/08/Clojure-radar.png"
    link="/2011/08/Clojure-radar.png" width="180" >}}

Firmu [ThoughtWorks](//www.thoughtworks.com/) znám hlavně protože zde pracuje můj oblíbený SW guru [Martin Fowler](//en.wikipedia.org/wiki/Martin_Fowler). Nedávno vyšel jejich [Technology Radar](//www.thoughtworks.com/articles/technology-radar-july-2011), který zmiňuje _"rozvíjející se technologie a trendy, které ovlivňují dnešní trh"._

Pro tento blog je podstatné, že je zde třikrát zmíněno Clojure: jednak jako samotný jazyk:

> _"Clojure is a dynamic, functional language that runs on the JVM. Although its roots are in Lisp, one of the oldest computer languages, it also embodies many modern programming concepts, including lazy evaluation and advanced concurrency abstractions. Clojure has spawned a vibrant community of programmers who are contributing a rich set of frameworks and tools. One example of these is Midje, an innovative spin on unit testing and mocking frameworks."_

Clojure se v sekci _Languages_ nachází v kruhu _Assess_, to znamená _"Worth exploring with the goal of understanding how it will affect your enterprise."_.

{{< figure src="/2011/08/Radar-languages.png" caption="ThoughtWorks Radar 2011, Languages kvadrant" >}}

Dále je v sekci _Interesting Tools_ uveden framework [Midje](//github.com/marick/Midje#readme), _"nástroj pro čitelné testy v Clojure"_. A konečně je Clojure zmíněno v souvislosti s [cloud platformou](//en.wikipedia.org/wiki/Cloud_platform#Platform) [Heroku](//www.heroku.com/), kam bylo přidáno jako jeden z jazyků.

Z trochu jiného úhlu pohledu píšu o _Radaru_, na svém blogu o softwarovém inženýrství, [SoftWare Samuraj](/2011/08/thoughtworks-radar-zajimave-technologie/).
