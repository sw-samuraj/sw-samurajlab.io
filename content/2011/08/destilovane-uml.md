+++
title = "Destilované UML"
date = 2011-08-11T10:46:00Z
updated = 2013-08-17T22:45:38Z
description = """Recenze knížky UML Distilled od Martina Fowlera. Pokrývá UML verze 2.0
    a zabývá se zasazením diagramů do kontextu SW vývoje."""
tags = ["certifikace", "knihy", "uml"]
aliases = [
    "/2011/08/destilovane-uml.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure src="/2011/08/UML-Distilled.jpg" class="floatleft" width="200"
    link="/2011/08/UML-Distilled.jpg" >}}

V rámci přípravy na [druhý stupeň](//www.omg.org/uml-certification/Intermediate.htm) [UML certifikace](//www.omg.org/uml-certification/index.htm) jsem si koupil (a přečetl) knížku [Martina Fowlera](//en.wikipedia.org/wiki/Martin_Fowler) [UML Distilled](//www.amazon.com/UML-Distilled-Standard-Modeling-ebook/dp/B000OZ0N8A/ref=tmm_kin_title_0?ie=UTF8&amp;m=AG56TWVU5XWC2) s podtitulem _A Brief Guide to the Standard Object Modeling Language_. Fowler se zaměřuje na dvě hlavní témata: jednak celkový přehled všech UML diagramů ve verzi 2.0 (kde až na výjimky nejde moc do hloubky - opravdu _brief guide_) a jednak zasazení diagramů do kontextu SW vývoje/designu/analýzy - zde můžou být (pro někoho) cenné jeho postřehy a doporučení o použitelnosti jednotlivých diagramů.

Knížka to není špatná (dal jsem jí čtyři hvězdičky), nicméně pro moji aktuální potřebu se moc nehodí. Celkový přehled diagramů byl fajn pro oživení UML oblasti. Ovšem vzhledem k účelu, ke kterému jsem si knihu pořídil tam byl nevhodný poměr mezi specifikací UML a popisem/vysvětlováním různých oblastí SW engineerství (= málo UML). Navíc celý ten "projektový" kontext, kterým Fowler UML ve své knize obalil (např. iterativní vývoj), už byl mnohokrát popsán jinde. Pro certifikaci samotnou tedy spoléhám spíše na doporučený (oficiální) studijní materiál [UML 2 Certification Guide](//www.amazon.com/UML-Certification-Guide-Fundamental-Intermediate/dp/0123735858/ref=sr_1_1/104-6522369-5078368?ie=UTF8&amp;s=books&amp;qid=1192475524&amp;sr=8-1) a sadu [zkušebních testů](//www.ucertify.com/exams/OMG/UM0-200.html).

Na závěr ještě poznámka ke [Kindle](//en.wikipedia.org/wiki/Amazon_Kindle) edici knížky. Vzhledem k tomu, že Fowler je u daného diagramu někdy velmi stručný, velmi často odkazuje do bibliografie a doporučuje další čtení k prohloubení tématu. Bohužel, biblio reference jsou uvedeny pouze jako klíč obyčejným textem - pokud si tedy v textu přečtu, že výborným doplňujícím zdrojem k tématu je [McConnell] a nemůžu si jednoduše kliknout a navíc navigace knihy neumožňuje jednoduše skočit do sekce bibliografie, tak je to otravné až frustrující. Dalším, již méně otravným (nicméně také bych jej párkrát využil) nedostatkem je nemožnost listovat mezi kapitolami.

P.S.: Pro ty, co jsou zvědaví, co se skrývá pod klíčem [McConnell], tak je to [Rapid Development: Taming Wild Software Schedules](//www.amazon.com/Rapid-Development-Taming-Software-Schedules/dp/1556159005/ref=sr_1_1?ie=UTF8&amp;qid=1313052191&amp;sr=8-1) od Steva McConnella.
