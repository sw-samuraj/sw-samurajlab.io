+++
title = "Testování v Clojure"
date = 2011-08-15T16:11:00Z
updated = 2017-03-18T22:04:12Z
description = """I když Rich Hickey říká, že nepíše unit testy, tak my
    všichni ostatní samozřejmě testy píšeme. A to i v Clojure!"""
tags = ["clojure", "testing"]
blogimport = true 
[author]
	name = "Vít Kotačka"
	uri = "https://www.blogger.com/profile/12147991495120816879"
+++

{{< figure class="floatleft" src="/img/logos/Clojure.png" width="180" >}}

Ačkoliv Rich Hickey říká, že [nepíše unit testy](/2011/06/rich-hickey-radsi-travim-cas/), přece jenom do Clojure zahrnul ["framework" pro unit testy](//clojure.github.io/clojure/clojure.test-api.html). Já sám, naopak, jsem již léty [TDD](//en.wikipedia.org/wiki/Test-driven_development) infikován a testy píšu rád. Takže, jak se to dělá v Clojure? Nejprve natáhneme knihovnu `clojure.test`:

```clojure
(ns my-tests (:use clojure.test))
; nil
```

## Aserce (tvrzení)

Základem testů je [makro](//clojure.org/macros) `is`:

```clojure
(is (= 2 (+ 1 1)))
; true
(is (odd? 1))
; true
(is (even? 1) "truth about one")
; 
; FAIL in clojure.lang.PersistentList$EmptyList@1 (NO_SOURCE_FILE:30)
; truth about one
; expected: (even? 1)
;   actual: (not (even? 1))
; false
```

Více [assertions](//en.wikipedia.org/wiki/Assertion_(computing))/parametrů se dá testovat pomocí makra `are`:

```clojure
(are [x y] (= 42 (+ x y))
     21 21
     20 22
     42 0)
; true
```

Vyhození výjimky se dá otestovat pomocí funkce `thrown?`:

```clojure
(is (thrown? ArithmeticException (/ 42 0)))
; #<ArithmeticException java.lang.ArithmeticException: Divide by zero>
(is (thrown? ArithmeticException (/ 42 42)))
; 
; FAIL in clojure.lang.PersistentList$EmptyList@1 (NO_SOURCE_FILE:48)
; expected: (thrown? ArithmeticException (/ 42 42))
;   actual: nil
; nil
```

## Definice testů

OK, to byly tvrzení (_assertions_). Jak definuji test? Jedna z možností je makro `with-test`:

```clojure
(with-test
    (defn add [x y]
        (+ x y))
    (is (= 42 (add 21 21)))
    (is (= 42 (add 20 22)))
    (is (= 42 (add 42 0))))
```

Druhou možností je použít makro `deftest`. Jeho výhodou je, že vytváří funkci (v našem případě `ultimate-addition`), kterou lze zavolat z libovolného [namespace](//clojure.org/namespaces). To umožňuje mít testy v samostatném souboru tak, jak jsme z unit testů zvyklí.

```clojure
(deftest ultimate-addition
    (is (= 42 (add 21 21)))
    (is (= 42 (add 20 22)))
    (is (= 42 (add 42 0))))
```

## Spuštění testů

Máme definované dva testy (a šest asercí). Jak je spustíme? Testy v aktuálním _namespace_ odpálíme funkcí `run-tests`:

```clojure
(run-tests)
;
; Testing my-tests
;
; Ran 2 tests containing 6 assertions.
; 0 failures, 0 errors.
; {:type :summary, :pass 6, :test 2, :error 0, :fail 0}
```

Testy v jiném _namespace_ spustíme obdobně:

```clojure
(run-tests 'some.namespace 'some.other.namespace)
```

## Automatizace testů

Tak. Konec srandiček! Velký kluci buildujou (a spouští testy) [Leiningenem](//github.com/technomancy/leiningen). Prvně vytvoříme projekt příkazem `lein new lein-tests`:

{{< figure src="/2011/08/Lein-tests-create.png" caption="Vytvoření a layout projektu" >}}

[Test first!](//www.extremeprogramming.org/rules/testfirst.html) Napíšeme testy (soubor `core.clj` v adresáři `test`):

```clojure
(ns lein-tests.test.core
  (:use [lein-tests.core])
  (:use [clojure.test]))

(deftest split-line-test
  (is (= ["foo" "bar"]
         (split-line "foo;bar" #";")))
  (is (= ["foo" "bar"]
         (split-line "foo,bar" #", ?")))
  (is (= ["foo" "bar"]
         (split-line "foo, bar" #", ?"))))

(deftest get-phone-test
  (is (= "123456789"
         (get-phone "123456789;Guido")))
  (is (= ""
         (get-phone ";Guido"))))
```

A napíšeme (testované) funkce (soubor `core.clj` v adresáři `src`):

```clojure
(ns lein-tests.core
  (:use [clojure.string :only (split)]))

(defn split-line [line separator]
  (split line separator))

(defn get-phone [line]
  (first (split-line line #";")))
```

No a na závěr testy spustíme příkazem `lein test`. Nádhera!

{{< figure src="/2011/08/Lein-tests-result.png" caption="Spuštění testů" >}}
