+++
title = "Nazdar světe! říká ClojureScript"
date = 2011-08-11T20:52:00Z
updated = 2017-03-18T22:02:26Z
description = """Vyzkoušel jsem si ClojureScript v alpha verzi. Zatím je to
    použití dost hard-core, jen pro silné jedince. Ale to se časem jistě
    změní."""
tags = ["clojure"]
blogimport = true 
[author]
	name = "Vít Kotačka"
	uri = "https://www.blogger.com/profile/12147991495120816879"
+++

{{< figure class="floatleft" src="/img/logos/ClojureScript.png" width="170" >}}

Tak jsem se neudržel a [ClojureScript](//github.com/clojure/clojurescript) si vyzkoušel. Následuje popis (jednoho zápasu), jak si vyrobit [Hello, world!](//en.wikipedia.org/wiki/Hello_world) v ClojureScriptu. Vzhledem k tomu, že ClojureScript je zatím v [alpha verzi](//en.wikipedia.org/wiki/Software_release_life_cycle#Alpha) a Windows (kde jsem to musel z časových důvodů vyzkoušet) tradičně hází vývojářům klacky pod nohy :-/ se instalace, kompilace a vyzkoušení triviálního příkladu protáhlo na několik hodin. Nicméně zvítězil jsem a tady je výsledek.

Nebudu se zabývat instalací ClojureScriptu --- je dobře popsána ve [wiki projektu](//github.com/clojure/clojurescript/wiki/Quick-Start), [Windows instalace](//github.com/clojure/clojurescript/wiki/Windows-Setup) je trochu dramatičtější. Navíc v případě Windows je problém, pokud se kompilace provádí v cestě s mezerami (`Documents and Settings` apod.), je při optimalizovaném překladu vyhozena výjimka.

Tak, máme nainstalováno? Spustíme z příkazové řádky příkaz `cljsc` (ClojureScript compiler):

```powershell
C:\clojure-clojurescript\samples\my-hello>cljsc
Usage: "cljsc <file-or-dir> > out.js"
       "cljsc <file-or-dir> {:optimiztions :advanced} > out.js"
```

No. Co/k čemu ten ClojureScript vlastně je? ClojureScript je dialekt Clojure (spíš [podmnožina](//github.com/clojure/clojurescript/wiki/Differences-from-Clojure)). Nejdůležitější je jeho kompilátor, který umožňuje zkompilovat Clojure(Script) do [JavaScriptu](//en.wikipedia.org/wiki/JavaScript). Pokud by někoho zajímaly filozofické důvody pro vznik ClojureScriptu, jsou uvedeny ve wiki na stránce [Rationale](//github.com/clojure/clojurescript/wiki/Rationale) (odůvodnění). V podstatě jde asi o to, dostat Clojure i na jinou platformu než [JVM](//en.wikipedia.org/wiki/Jvm), v tomto případě _JavaScript VM_.

ClojureScript kompilátor generuje JavaScript, který (záměrně) odpovídá konvenci JavaScriptové knihovny [Google Closure](//code.google.com/intl/cs/closure/). Důvod je zřejmý --- (Google) [Closure Compiler](//code.google.com/intl/cs/closure/compiler/). Tím se vysvětlují dva způsoby použití kompilátoru `cljsc`:

* `cljsc <file-or-dir> > out.js` spustí pouze ClojureScript kompilátor a výsledný soubor "slinkuje" s knihovnou [Closure Library](//code.google.com/closure/library/).
* `cljsc <file-or-dir> {:optimiztions :advanced} > out.js` dělá totéž, plus ještě následně prožene výsledný kód [Closure Compilerem](//code.google.com/intl/cs/closure/compiler/), který kód zoptimalizuje a vytvoří jeden kompaktní soubor.

Takže, pojďme se podívat na ten příklad. Vytvořím soubor `hello.cljs`. Metadata `^:export` u definice funkce říkají, aby název výsledné funkce nebyl kompilátorem [minifikovaný](//en.wikipedia.org/wiki/Minification_(programming)).

```clojure
(ns hello)

(defn ^:export greet [n]
    (str "Hello, " n "!"))
```

Následně ho zkompiluju:

```bash
cljsc hello.cljs {:optimizations :advanced} > hello.js
```

A výsledný kód zavolám v prohlížeči:

```html
<html>
<head>
    <title>ClojureScript</title>
    <script src="hello.js" type="text/javascript">
</script>
</head>
<body>
    <script type="text/javascript">
            alert(hello.greet("ClojureScript"));
    </script>
</body>
</html>
```

A výsledek?

{{< figure src="/2011/08/Cljs-hello.png" >}}

Uf! Byla to dřina. :-|

Když to vezmu prakticky, tak zatím ClojureScript moc použitelný není. Problém jsou hlavně chybějící nástroje. Pokud ale jednou bude zaintegrován například do [Leiningenu](//github.com/technomancy/leiningen) nebo Mavenu, tak si dokážu představit, že třeba ve spojení s [Compojure](//github.com/weavejester/compojure/wiki) půjde vytvořit kompletní webovou (klient/server) aplikaci pouze v Clojure. To by bylo komfortní.
