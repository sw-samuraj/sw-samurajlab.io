+++
title = "ThoughtWorks Radar, zajímavé technologie"
date = 2011-08-16T14:18:00Z
updated = 2017-06-17T11:57:16Z
description = """ThoughtWorks Radar monitoruje zajímavé technologie v oblasti
    SW inženýrství. Jáká byla edice 2011 a co zajímavého v něm shledal
    blog SoftWare Samuraj?"""
aliases = [
    "/2011/08/thoughtworks-radar-zajimave-technologie.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

Firma [ThoughtWorks](//www.thoughtworks.com/) (kde pracuje můj oblíbený SW guru [Martin Fowler](//en.wikipedia.org/wiki/Martin_Fowler)) nedávno zveřejnila svůj [Technology Radar](//www.thoughtworks.com/radar), jehož účelem je _"to help decision makers understand emerging technologies and trends that affect the market today."_ Ještě než se dostanu k technologiím, které mne zaujaly, uvedu k _Radaru_ krátkou legendu. _Radar_ je rozdělen do kvadrantů _Techniques, Tools, Platforms_ a _Languages_. Jednotlivé technologie jsou umístěny na kruzích:

* **Adopt**: We feel strongly that the industry should be adopting these items. We use them when appropriate on our projects.
* **Trial**: Worth pursuing. It is important to understand how to build up this capability. Enterprises should try this technology on a project that can handle the risk.
* **Assess**: Worth exploring with the goal of understanding how it will affect your enterprise.
* **Hold**: Proceed with caution.

{{< figure src="/2011/08/ThoughtWorks-radar-2011.png" caption="ThoughtWorks Radar 2011, Tools kvadrant" >}}

To, co mě na _Radaru_ zaujalo nejvíc, je nadějná pozice [Clojure](//clojure.org/). Vzhledem k tomu, že tomuto se věnuji na svém dalším [blogu věnovaném Clojure](/2011/08/thoughtworks-radar-zminuje-clojure/), nebudu to zde rozebírat. Co dalšího?

* **Continuous Delivery**. Aktuální agilní technika z dílny samotných _ThoughtWorks_, aneb jak efektivně dostat na produkci každý dobrý build. [Knížku](//www.amazon.com/Continuous-Delivery-Deployment-Automation-Addison-Wesley/dp/0321601912/ref=sr_1_1?ie=UTF8&amp;qid=1313497777&amp;sr=8-1) už mám zakoupenou, teď jen najít čas si ji přečíst.
* **[Sonar](//www.sonarsource.org/)**. Integrovaný nástroj pro kontrolu a vizualizaci metrik zdrojového kódu. Sám jsem ho sice, bohužel, na projektu ještě nepoužil, ale už mockrát jsem nad ním přemýšlel - procházet jednotlivé metriky vygenerované [Mavenem](//maven.apache.org/) je otravné a člověk přitom ztrácí celkový kontext.
* Vzestup **[NoSQL](//en.wikipedia.org/wiki/Nosql)** databází. Už bych se měl na nějakou konečně podívat.
* **[Gradle](//www.gradle.org/)**. Nástroj na automatizaci projektu, něco jako [Maven](//maven.apache.org/) používající [Groovy](//groovy.codehaus.org/) [DSL](//en.wikipedia.org/wiki/Domain-specific_language). Jeden čas jsem ho používal na malé Java a [Groovy](//groovy.codehaus.org/) projekty. Možná nastal čas se k němu vrátit.
* **[JRuby](//www.jruby.org/)** --- [Ruby](//www.ruby-lang.org/en/) implementace pro [JVM](//en.wikipedia.org/wiki/Java_Virtual_Machine). Jestli mi do toho něco nevleze, rád bych se Ruby naučil jako další jazyk. _Ruby_/_JRuby_ se mi jako Javistovi hodí.
* Špatné umístění **[GWT](//code.google.com/intl/cs/webtoolkit/)**. Doslova je řečeno: _"GWT is a reasonable implementation of a poor architectural choice."_. O GWT jsem přemýšlel ze strategického hlediska, jako o možné frontendové Java technologii při návrhu architektury [AJAX](//en.wikipedia.org/wiki/Ajax_(programming)) aplikací. Tak teď nevím, nevím.

ThoughtWorks Radar ve formátu [PDF](//assets.thoughtworks.com/assets/technology-radar-july-2011.pdf).
