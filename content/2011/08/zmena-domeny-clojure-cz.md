+++
title = "Změna domény: clojure.cz"
date = 2011-08-13T12:50:00Z
updated = 2011-08-13T12:50:01Z
description = """Z rozmaru jsem se podíval, jestli je volná doména clojure.cz
    a... byla. Tak jsem si ji vzal."""
tags = ["clojure"]
blogimport = true 
[author]
	name = "Vít Kotačka"
	uri = "https://www.blogger.com/profile/12147991495120816879"
+++

{{< figure class="floatleft" src="/img/logos/Clojure.png" width="160" >}}

Tak je to už sedm měsíců, co píšu _Sometimes Clojure_. Včera jsem se jen tak z rozmaru podíval, jestli je volná doména [clojure.cz](//clojure.cz/). Byla. Tak jsem si ji vzal. Zatím se mi zdá, že jsem jediný, kdo o Clojure v Česku píše, tak co. Třeba jednou vznikne komunitní web, nebo něco. Ale teď je moje! [My precious!!](//www.youtube.com/watch?v=Iz-8CSa9xj8&amp;NR=1) :-)
