+++
title = "Second mission"
date = 2011-04-27T16:09:00Z
updated = 2017-03-11T22:45:39Z
description = """Taková drobnůstka z použití Clojure na realný problém
    - transformace dat v tabulce."""
tags = ["clojure"]
blogimport = true 
[author]
	name = "Vít Kotačka"
	uri = "https://www.blogger.com/profile/12147991495120816879"
+++

{{< figure class="floatleft" src="/img/logos/Clojure.png" width="160" >}}

Navázal bych na post [First Mission](/2011/02/first-mission/) --- napsal jsem další transformační skriptík pro stejný projekt. Opět šlo o to, přečíst vstupní soubor a na základě daných podmínek změnit některé hodnoty. Soubor obsahoval informaci o sdílených telefonech:

{{< figure src="/2011/04/Telco-limits-1.png" >}}

Tam, kde byla pro stejný telefon různá osobní čísla mělo dojít ke změně typu z PERSONAL na SHARED:

{{< figure src="/2011/04/Telco-limits-2.png" >}}

Měl jsem trochu obavy z "performance" (soubory mají 10k --- 50k řádků), tak jsem to udělal na dva průchody --- při prvním průchodu se do mapy vložily telefony, které měly více os. čísel a při druhém průchodu, kdy se všechny záznamy zapisovaly do upraveného souboru, se tyto záznamy měnily.

Skriptík je samozřejmě triviální, ale podstatné jsou věci, které jsem se přitom naučil --- pořádně pracovat s [Refs](//clojure.org/Refs) a transakcema ([STM](//en.wikipedia.org/wiki/Software_transactional_memory)):

```clojure
(def phones (ref {}))

(defn add-phone [phone pers-num]
  (if (contains? @phones phone)
      (if (contains? (@phones phone) pers-num)
          (println "Duplicate record:" phone pers-num)
          (dosync
            (ref-set phones (update-in @phones [phone]
                                       conj pers-num))))
      (dosync
        (ref-set phones (assoc @phones phone
                               #{pers-num})))))

(add-phone :123456789 :12)
(add-phone :123456789 :42)
(add-phone :123456789 :12)
(add-phone :123456790 :36)

(println @phones)
; Duplicate record: :123456789 :12
; {:123456790 #{:36}, :123456789 #{:12 :42}}
```

Další šikovná věc je "filrování mapy":

```clojure
(defn get-shared [m]
  (select-keys m (for [[k v] m :when
                       (> (count v) 1)] k)))

(println
  (get-shared
    {:123456790 #{:36}, :123456789 #{:12 :42}})
; {:123456789 #{:12 :42}}
```

K tématu _Refs_ bych se v budoucnu ještě rád vrátil --- hned potom, co ze zásobníku vyndám a zpracuju [sekvence](//clojure.org/sequences).
