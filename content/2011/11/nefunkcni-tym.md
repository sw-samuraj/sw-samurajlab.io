+++
title = "(Ne)funkční tým"
date = 2011-11-06T22:42:00Z
updated = 2017-03-10T21:09:12Z
description = """Občas se vám stane, že jako team leader vyfasujete nepřátelský
    tým. Inspiraci jak řešit tuto těžkou situaci možná najdete v knize The Five
    Dysfunctions of a Team. Krátká recenze + pár citátů."""
tags = ["teamleading", "knihy"]
aliases = [
    "/2011/11/nefunkcni-tym.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2011/11/FiveDysfunctionsofaTeam.jpg"
    link="/2011/11/FiveDysfunctionsofaTeam.jpg" width="200" >}}

V rámci projektu, na němž se podílí kromě zákazníka několik firem, jsem přebíral tým vývojářů. Nebylo to úplně přátelské převzetí (z hlediska projektu) a tak jsem se obrátil na radu k moudrým knihám. Z nabídky Amazonu jsem nakonec zvolil [The Five Dysfunctions of a Team](//www.amazon.com/dp/B000UCUX0K/ref=r_soa_w_d) od Patricka Lencioniho.

Kniha je rozdělena do dvou částí. Tu první, větší charakterizuje podtitul knihy --- _A Leadership Fable_. Sledujeme v ní příběh IT společnosti ([startupu](//en.wikipedia.org/wiki/Startup_company)), který má (jak jinak, vzhledem k tématu) nefunkční [executive team](//en.wikipedia.org/wiki/Senior_management). Do firmy přichází nová [CEO](//en.wikipedia.org/wiki/Chief_Executive_Officer), která má na starosti tým sjednotit, motivovat a dovést společnost k rozkvětu :-)

Tato část je napsána velmi čtivě, až napínavě a celkem přirozeně představuje a vysvětluje principy (ne)fungování úspěšného týmu. Tyto principy jsou pak precizně shrnuty v druhé části, nazvané _The Model_. No a co že je oněmi pěti principy?

{{< figure src="/2011/11/Five-disfunctions-team.png" caption="Model 5 Dysfunctions of a Team, zdroj: www.tablegroup.com"
    link="https://www.tablegroup.com/download/the-five-dysfunctions"
>}}

V podstatě nejde o nic překvapivého (když je to takto zjeveno a poskládáno v logických souvislostech), ale člověku takové "zjevné" věci kolikrát ani nedojdou. Pro mne nejpřínosnější byl hned ten první princip _Absence důvěry_. Dovolil bych si zde z něj (a z knihy) dvakrát ocitovat:

> _"In the context of building a team, trust is a confidence among team members that their peers' intentions are good, and that there is no reason to be protective or careful around the group. In essence, teammates must get comfortable being vulnerable with one another."_

<!-- -->
> _"The most important action that a leader must take to encourage the building of trust on a team is to demonstrate vulnerability first. ... What is more, team leaders must create an environment that does not punish vulnerability."_
