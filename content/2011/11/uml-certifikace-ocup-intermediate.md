+++
title = "UML certifikace, OCUP Intermediate"
date = 2011-11-04T11:30:00Z
updated = 2013-07-18T11:03:42Z
description = """Jak se připravit na UML certifikaci OCUP Intermediate? SoftWare
    Samuraj sdílí svoje tipy a triky."""
tags = ["certifikace", "knihy", "uml"]
aliases = [
    "/2011/11/uml-certifikace-ocup-intermediate.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft"
    src="/2011/11/UML-certification.gif"
    link="/2011/11/UML-certification.gif" >}}

Právě jsem se čerstvě UML certifikoval, ještě mám vedle sebe "kouřící" report. Celá certifikace se jmenuje [OMG-Certified UML Professional Intermediate Exam](//www.omg.org/uml-certification/Intermediate.htm) :-) zkráceně [OCUP Intermediate](//www.omg.org/uml-certification/index.htm). Nebylo to úplně jednoduché, nicméně jsem prošel se skóre 45/70 (passing score je 31/70).

Jako přípravu jsem primárně použil prep-kit [UM0-200](//www.ucertify.com/exams/OMG/UM0-200.html) od [uCertify](//www.ucertify.com/). Buď už mám těch certifikací moc a jsem zmlsaný, nebo zrovna tohle nebyl úplně povedený kit, každopádně ho nedoporučuju použít jako jediný zdroj na zkoušku. Nicméně i když jsem s jeho kvalitou nebyl úplně spokojený, i tak mi dost pomohl.

{{< figure class="floatright"
    src="/2011/11/UML-Certification-Guide.png"
    link="/2011/11/UML-Certification-Guide.png" >}}

Naopak bych vřele doporučil knížku [UML Certification Guide: Fundamental &amp; Intermediate Exams](//www.amazon.com/UML-Certification-Guide-Fundamental-Intermediate/dp/0123735858/ref=sr_1_1?ie=UTF8&amp;qid=1320401124&amp;sr=8-1). Bohužel jsem jí nevěnoval dostatek času - určitě bych pak měl lepší skóre. Jenom je škoda, že knížka už neobsahuje poslední, závěrečnou certifikaci ([OCUP Advanced](//www.omg.org/uml-certification/Advanced.htm)). Tu bych si chtěl časem také udělat a zatím nevím, jaké materiály pro to zvolit.

{{< figure src="/2011/08/UML-Distilled.jpg" class="floatleft" width="160"
    link="/2011/08/UML-Distilled.jpg" >}}

Kvůli certifikaci jsem si pořídil ještě jednu knížku ([už jsem o ní psal](/2011/08/destilovane-uml/)): [UML Distilled](//www.amazon.com/UML-Distilled-Standard-Modeling-ebook/dp/B000OZ0N8A/ref=tmm_kin_title_0?ie=UTF8&amp;m=AG56TWVU5XWC2) od [Martina Fowlera](//martinfowler.com/). Jednak jsem si chtěl trochu šířeji oživit UML samotné a jednak jsem ji použil jako sekundární zdoj. Řekl bych, že se celkem hezky doplňovala s prep-kitem od uCertify.

Závěrem. Myslím, že zdroje jsem si zvolil správně, jenom jsem měl být trochu důslednější (= méně líný) při čtení _UML Certification Guide_.
