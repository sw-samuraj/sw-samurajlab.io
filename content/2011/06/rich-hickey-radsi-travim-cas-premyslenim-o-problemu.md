+++
title = "Rich Hickey: Radši trávím čas přemýšlením o problému"
date = 2011-06-15T15:52:00Z
updated = 2017-04-02T22:43:35Z
description = """Rich Hickey je osobnost s vyzrálými myšlenkami, kterého stojí
    za to číst a nechat se jím inspirovat. Odkaz na rozhovor, který s Richem
    vedl Michael Fogus."""
tags = ["clojure", "sw-engineering", "knihy"]
blogimport = true 
[author]
	name = "Vít Kotačka"
	uri = "https://www.blogger.com/profile/12147991495120816879"
+++

{{< figure class="floatleft" src="/2011/06/Rich-Hickey.jpg"
    link="/2011/06/Rich-Hickey.jpg" width="260" >}}

Skončil mi v Readeru [zajímavý rozhovor](//www.codequarterly.com/2011/rich-hickey/) s [Richem Hickeym](//en.wikipedia.org/wiki/Rich_Hickey). Zajímavý byl i dotazovatel --- [Michael Fogus](//blog.fogus.me/), autor knihy [The Joy of  Clojure](//www.manning.com/fogus/). Rozhovor je trochu delší a dost špatně (typograficky) formátovaný, ale kvůli tomu obsahu stojí za to se tím prokousat.

Rich hovoří o svých zkušenostech programátora, co bylo před _Clojure_, co _Clojure _pozitivně a negativně ovlivnilo, apod. Zmíněn je také Richův seznam na Amazonu [Clojure Bookshelf](//www.amazon.com/Clojure-Bookshelf/lm/R3LG3ZBZS4GCTH) (mmch. inspirativní seznam pro čtení, už před časem jsem si tam pár kousků vybral).

Mmj. mě zaujalo, že Rich je spíš na debuggování (debugging master) a nepíše testy ([TDD](//en.wikipedia.org/wiki/Test-driven_development)):

> _"If people think that spending fifty percent of their time writing tests maximizes their results—okay for them. I’m sure that’s not true for me—I’d rather spend that time thinking about my problem. I’m certain that, for me, this produces better solutions, with fewer defects, than any other use of my time."_

A co dělá Rich pro zlepšení svých programátorských skillů?

> _"I read relentlessly. I don’t do any programming not directed at making the computer do something useful, so I don’t do any exercises. I try to spend more time thinking about the problem than I do typing it in."_

_[Update 23. 12. 2019]_ Původní článek už není k dispozici online, nicméně zde je jeho záloha:

* https://gist.github.com/rduplain/c474a80d173e6ae78980b91bc92f43d1

_[/Update]_
