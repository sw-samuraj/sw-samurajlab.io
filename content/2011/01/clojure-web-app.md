+++
title = "Clojure web-app"
date = 2011-01-20T16:01:00Z
updated = 2017-03-18T21:50:18Z
description = """Trošku jsem na internetu zapátral, jak je to s využitím
    Clojure pro webové aplikace. A ejhle! Něco je k dispozici."""
tags = ["clojure"]
blogimport = true 
[author]
	name = "Vít Kotačka"
	uri = "https://www.blogger.com/profile/12147991495120816879"
+++

{{< figure class="floatleft" src="/img/logos/Clojure.png" width="200" >}}

Hledal jsem na webu nějaké reference, nebo projekty, které by používaly Clojure a narazil jsem na zajímavou prezentaci od lidí, kteří vytvořili aplikaci [TheDeadline](//the-deadline.appspot.com/). Jde o aplikaci pro [GTD](//en.wikipedia.org/wiki/Getting_Things_Done), která by měla obsahovat nějakou umělou inteligenci. Zajímavé jsou samozřejmě technologie, které se za touto aplikací skrývají.

Aplikace samotná běží na [Google App Enginu](//code.google.com/appengine/) (GAE) a původně používala Clojure jak pro komunikaci s (Google) Datastorem, tak pro zobrazení HTML. Jak jsem pochopil z jiné, pozdější prezentace, tak v současnosti by měli pro UI používat [Google Closure Templates](//code.google.com/closure/templates/) (další zajímavá technologie).

Datastrore v GAE nabízí dva druhy interfaců --- high-level pro Java objekty a low-level, který umožňuje přímo pracovat s key-value strukturami. Což je přesně to, co nativně nabízí Clojure --- [StructMaps](//clojure.org/reference/data_structures#StructMaps). Je to jedna z myšlenek, které mě v prezentaci nejvíc oslovily --- pokud vytvářím webovou aplikaci v Javě, musím udělat (nebo použít framework) mapování tabulka-objekty ([ORM](//en.wikipedia.org/wiki/Object-relational_mapping)) a pak pro prezentaci zase převod objekty-HTML. Tato dvojnásobná transformace, abych zobrazil data z databáze v prohlížeči, by při použití Clojure měla odpadnout, protože obojí by mělo jít přímo. Ono to samozřejmě tak jednoduché nebude, ale ta myšlenka se mi líbí.

* SlideShare: [How a Clojure pet project turned into a full-blown cloud-computing web-app](//www.slideshare.net/smartrevolution/how-a-clojure-pet-project-turned-into-a-fullblown-cloudcomputing-webapp)

Kromě této "technické" prezentace je zajímavá i druhá "manažerská" prezentace, která téma probírá spíše z designového hlediska (a zmiňuje se právě o _Google Closure Templates_).

* SlideShare: [Writing HTML5 apps with Google App Engine, Google Closure Library and Clojure](https://www.slideshare.net/smartrevolution/writing-html5-apps-with-google-app-engine-google-closure-library-and-clojure)

A na závěr ještě rozhovor, který vyšel na [InfoQ](//www.infoq.com/):

* [Scaling Clojure Web Apps with Google AppEngine](//www.infoq.com/articles/deadline-clojure-appengine)
