+++
title = "Sometimes Clojure"
date = 2011-01-18T09:21:00Z
updated = 2017-03-11T22:45:39Z
description = """Sometimes Clojure... nový blog o funkcionálním
    jazyku... Clojure."""
tags = ["blogging", "clojure"]
blogimport = true 
[author]
	name = "Vít Kotačka"
	uri = "https://www.blogger.com/profile/12147991495120816879"
+++

{{< figure class="floatleft" src="/img/logos/Clojure.png" width="200" >}}

OK, Sometimes Clojure. Přišel čas naučit se další jazyk, a protože fandím jazykům, které běží v JVM, zalovil jsem primárně v těchto vodách. Kromě (mého) oblíbeného Groovy, které už pár let používám, jsem chtěl, aby to byl trošku "jiný" jazyk. A jelikož už nějakou dobu pošilhávám po funkcionálních jazycích, zvolil jsem [Clojure](//clojure.org/). A zdá se, že je to láska na první pohled.

Proč trochu "jiný" jazyk? Představte si, že umíte celkem slušně anglicky, celkem slušně španělsky, cizí slova z latiny vám taky nejsou cizí a rozhodnete se pro francouzštinu. Z vlastní zkušenosti můžu říct, že francouzština je (zatím) nejtěžší evropský jazyk, který jsem se učil. Nicméně výše uvedené znalosti znamenají/způsobí, že učící křivka nového jazyka je velmi strmá. Co když se ale začnu učit nějaký neevropský jazyk? Jazyk, který vzniknul v jiné kultuře, postavený na jiných principech? Tak přesně to, takový neevropský jazyk, je pro mne momentálně _Clojure_.

Zatím jsem si _Clojure_ nainstaloval v Linuxu i Win, vyzkoušel si základy a zkouknul pár prezentací. Doporučil bych:

* [Clojure](//www.infoq.com/presentations/hickey-clojure), kterou prezentuje jeho tvůrce [Rich Hickey](//en.wikipedia.org/wiki/Rich_Hickey),
* [Clojure for Java programmers](//blip.tv/file/982823) (opět Rich Hickey),
* [Casting Spells in Lisp (Clojure version)](//www.lisperati.com/clojure-spels/casting.html) --- hravý tutoriál, aneb naprogramujte si textovou hru v _Clojure_.

A závěrem, proč vznikl tenhle blog? V češtině toho zatím o _Clojure_ moc není, jen sem tam pár zmínek, že něco takového existuje. Takže než se _Clojure_ v Česku trochu rozšíří, třeba se mezitím stanu _Clojure guru_ :-)
