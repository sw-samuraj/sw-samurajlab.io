+++
title = "Konečně kód!"
date = 2011-01-21T15:45:00Z
updated = 2017-03-11T22:45:39Z
description = """Kratičký Clojure zápis, hlavně na ladění
    syntax highlightingu. :-)"""
tags = ["clojure"]
blogimport = true 
[author]
	name = "Vít Kotačka"
	uri = "https://www.blogger.com/profile/12147991495120816879"
+++

{{< figure class="floatleft" src="/img/logos/Clojure.png" width="160" >}}

Uf! Tak jsem se pěkně dlouho mordoval se [syntax highlighting](//en.wikipedia.org/wiki/Syntax_highlighting). Člověk by řekl, že když je _Blogger_ od Googlu, že už by to mohl tak nějak defaultně umět. Nebo na to mít nějaký plugin. No, možná je to věc použité šablony --- každopádně, skončil jsem její úpravou, kdy jsem do ní doplnil reference na [SyntaxHighlighter](//alexgorbatchev.com/SyntaxHighlighter/), plus externí [skript pro Clojure](//www.undermyhat.org/blog/wp-content/uploads/2009/09/shBrushClojure.js). Zkoušel jsem i [google-code-prettify](//code.google.com/p/google-code-prettify/), ale nepodařilo se mi dosáhnout toho, aby to "prettyfájovalo".

Takže nějaký kód. Zatím jen trivialitky na okoštování a vyzkoušení _highlightingu_. Ale už se těším, až se ponořím do studia [transakčních referencí](//clojure.org/refs), [agentů](//clojure.org/agents) a [atomů](//clojure.org/atoms) :-)

```clojure
(+ 1 2 3)
; 6
(defn hello [name] (println "Hello," name))
; #'user/hello
(hello "Guido")
; Hello, Guido
; nil
(map + [1 2 3] [4 5 6])
; (5 7 9)
```

Jen vysvětlení pro Javisty ;-) výše uvedený výstup je z prostředí [REPL](//en.wikipedia.org/wiki/REPL) a pro pochopení lispovské syntaxe:

**Java:**

```java
void processSomething(Type valueOne, Type valueTwo) {
    // some code
}

processSomething(valueOne, valueTwo);
```

**Lisp/Clojure:**

```clojure
(defn process-something [valueOne valueTwo] ())

(process-something valueOne valueTwo)
```
