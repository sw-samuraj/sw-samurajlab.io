+++
title = "PragPub se věnuje Clojure"
date = 2011-07-17T17:27:00Z
updated = 2017-04-02T22:39:34Z
description = """Můj oblíbený e-časopis PragPub věnuje celé číslo Clojure! 👍"""
tags = ["clojure"]
blogimport = true 
[author]
	name = "Vít Kotačka"
	uri = "https://www.blogger.com/profile/12147991495120816879"
+++

{{< figure class="floatleft" src="/2011/07/PragPub-2011-July.png"
    link="/2011/07/PragPub-2011-July.png" width="200" >}}

Vydavatelství [The Pragmatic Bookshelf](//pragprog.com/) --- které svého času založili někdejší signatáři [Manifestu agilního vývoje softwaru](//agilemanifesto.org/) _Andy Hunt_ a _Dave Thomas_ --- vydává (zdarma) příjemný časopis, měsíčník [PragPub](//pragprog.com/magazines). Aktuální [červencové číslo](//pragprog.com/magazines/2011-07/content) se věnuje převážně _Clojure_. Jsou zde mmj. články o neměnnosti dat a kolekcích, nebo o [DSL](//en.wikipedia.org/wiki/Domain-specific_language).

Kromě odkazovaného HTML vydání jsou na [stránce vydaných čísel magazínu](//pragprog.com/magazines) k dispozici i formáty [mobi](//pragprog.com/magazines/download/25.mobi), [epub ](//pragprog.com/magazines/download/25.epub)a [PDF](//pragprog.com/magazines/download/25.PDF).
