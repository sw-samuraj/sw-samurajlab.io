+++
title = "Nekonečná lenost sekvencí"
date = 2011-07-24T14:32:00Z
updated = 2017-03-11T22:45:39Z
description = """Clojure obsahuje lazy (líné) sekvence, což je velmi šikovný
    funkcionální koncept. Zejména, pokud je taková sekvence nekonečná! 🤭"""
tags = ["clojure", "functional-programming"]
blogimport = true 
[author]
	name = "Vít Kotačka"
	uri = "https://www.blogger.com/profile/12147991495120816879"
+++

{{< figure class="floatleft" src="/img/logos/Clojure.png" width="200" >}}

Velmi silnou (a zajímavou) zbraní Clojure jsou [sekvence](//clojure.org/sequences) (neboli _seq_, čti [si:k]). Sekvence je logický seznam, který implementuje jednoduché rozhraní [ISeq](//code.google.com/p/clojure/source/browse/trunk/src/jvm/clojure/lang/ISeq.java) a umožňuje sekvenční přístup k datům --- a to nejenom k těm, u kterých bychom to čekali (kolekce = seznamy, mapy, apod.), ale i k těm, kde potřebné sekvenční implementační detaily chybí, např. stromové struktury (XML, adresáře), databázové _result sety_ či textové soubory (buď jeden velký řetězec, nebo vektor řádků), [I/O streamy](//download.oracle.com/javase/tutorial/essential/io/streams.html), anebo obyčejné znakové řetězce ([String](//download.oracle.com/javase/6/docs/api/java/lang/String.html)).

Věc, na kterou bych se chtěl podívat je jednak lenost (_laziness_) a jednak (možná) nekonečnost (_infiniteness_) sekvencí. _Laziness_ je známá např. z databázového/[ORM](//en.wikipedia.org/wiki/Object-Relational_Mapping) světa, kdy se _lazy_ typicky dotahují data v [1:N vztazích](//download.oracle.com/javaee/5/api/javax/persistence/OneToMany.html). Výhodou _lazy_ sekvencí je:

* odsunutí výpočtu, který možná nebude potřeba,
* možnost práce s velkými daty, která se nevejdou do paměti,
* odsunutí I/O operací, až budou opravdu potřeba.

Co se týká nekonečnosti, tam je to jasné --- některé sekvence prostě jsou nekonečné: [přirozená čísla](//en.wikipedia.org/wiki/Natural_number), [prvočísla](//en.wikipedia.org/wiki/Prime_number), [Fibonacciho posloupnost](//en.wikipedia.org/wiki/Fibonacci_numbers), atd.

Ukázkový příklad jsem převzal z knížky [Programming Clojure](//pragprog.com/book/shcloj/programming-clojure) a sice protože mi přišel tak dobrý, že se mi zdálo zbytečné vymýšlet příklad vlastní (ach ta lenost :-). Zajímá vás, jak vypadá milionté prvočíslo?

```clojure
(use '[clojure.contrib.lazy-seqs :only (primes)])
; nil
(def ordinals-and-primes
  (map vector (iterate inc 1) primes))
; #'user/ordinals-and-primes
(first (drop 999999 ordinals-and-primes))
; [1000000 15485863]
```

Pro vysvětlení, [Var](//clojure.org/vars) `primes` obsahuje _lazy_ sekvenci prvočísel,
[Var](//clojure.org/vars) `ordinals-and-primes` obsahuje dvojice hodnot `[pořadové-číslo prvočíslo]`.
Poslední příkaz (`first (drop ...))` provede samotný výpočet sekvence prvočísel, zahodí prvních
999.999 hodnot a vrátí (pomocí `first`) tu 1.000.000tou. Vypočítat milion prvočísel chvilku trvá,
takže třetí příkaz chvilku poběží. Spočítanou sekvenci už má pak ale Clojure nacachovanou,
takže hodnoty pod milion nebo lehce nad vrací okamžitě.
