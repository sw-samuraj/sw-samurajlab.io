+++
title = "Jak měnit neměnitelné. Refs"
date = 2011-07-29T12:54:00Z
updated = 2017-03-11T22:45:39Z
description = """Clojure používá immutable data. Pokud potřebujeme mutable
    data, řeší to Clojure "měnitelnou referencí na neměnitelný objekt". :-)
    Jedním z prostředků, které to řeší jsou Refs - transakční reference."""
tags = ["clojure", "concurrency"]
blogimport = true 
[author]
	name = "Vít Kotačka"
	uri = "https://www.blogger.com/profile/12147991495120816879"
+++

{{< figure class="floatleft" src="/img/logos/Clojure.png" width="120" >}}

Clojure používá neměnitelná (immutable) data/objekty. Pokud potřebujeme měnitelná (mutable) data, řeší to Clojure "měnitelnou referencí na neměnitelný objekt" :-) Jedním z prostředků, které toto řeší jsou [Refs](//clojure.org/Refs): jsou to transakční reference, které umožňují bezpečné sdílení měnitelných úložišť pomocí systému [STM](//en.wikipedia.org/wiki/Software_transactional_memory) (Software Transactional Memory).

Měnitelná reference (_ref_) se vytvoří funkcí `ref`, její hodnotu vrací buď funkce `deref`, nebo makro `@`:

{{< gist sw-samuraj 150178faa17f670fe1eff6992fca3251 >}}

Pokud chceme referenci nastavit na jinou (neměnitelnou) hodnotu, slouží k tomu funkce `ref-set`:

{{< gist sw-samuraj 14084e0bf3b2f86b8f6f1fc649e21442 >}}

Jejda! Zapomněli jsme na transakci :-)

{{< gist sw-samuraj 141122457881aaaf7a00d700efb86e9f >}}

Pokud chceme provést čtení hodnoty a zároveň její změnu v jednom kroku (= aplikovat na hodnotu funkci), je vhodné použít funkci `alter`:

{{< gist sw-samuraj 14e09de155b1b1ee4f942fcff8d06b16 >}}

Na reference je také možné přidat validace:

{{< gist sw-samuraj cd0c97c540846b0e07e0e9bfe4771cab >}}

O _Refs_ jsem už [jednou (trochu) psal](/2011/04/second-mission). Z dnešního pohledu k tomu mám dvě výhrady:

* místo `ref-set` jsem měl použít `alter`,
* místo reference jsem měl použít [atom](//clojure.org/atoms).

`ref-set` je vhodné použít tehdy, pokud přiřazuji novou hodnotu (nepočítám ji).
`alter` tehdy, pokud nad hodnotou provádím nějakou funkci (např. inkrementace, přidání hodnoty do kolekce apod.).

Při rozhodování, jestli použít _ref_ nebo _atom_ je podstatné, jestli využiju transakci --- v transakci můžu updatovat více referencí. Pokud budu měnit pouze jedinou hodnotu (bez vazby na cokoli jiného) je vhodnější použít _atom_ (a o těch až někdy příště).
