+++
title = "Manažerem humorně a kousavě"
date = 2011-07-29T14:22:00Z
updated = 2017-03-10T21:09:12Z
description = """Recenze knížky Managing Humans. Softwarový inženýři jsou také lidé - aspoň
    tak se na ně s nadhledem a humorem dívám Michael Lopp (Rands in Repose)."""
tags = ["teamleading", "knihy", "sw-engineering"]
aliases = [
    "/2011/07/manazerem-humorne-kousave.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2011/07/Managing-Humans.png"
    link="/2011/07/Managing-Humans.png" width="250" >}}

Přečetl jsem výbornou knížku [Managing Humans](//www.amazon.com/Managing-Humans-Humorous-Software-Engineering/dp/159059844X) s podtitulem _Biting and Humorous Tales of a Software Engineering Manager._ Autorem je Michael Lopp, kterému se _"nikdy nepodařilo uprchnout ze Silicon Valley"_, a který mmj. pracoval pro společnosti Apple Computer, Netscape Communications, Symantec Corporation, či Borland International jako _sw engineering manager_. Michael také píše populární blog [Rands In Repose](//randsinrepose.com/), kde většina(?) kapitol ze zmíněné knihy vyšla.

Knížka, která je velmi vtipná a čtivá (s trochu těžší angličtinou), popisuje různé aspekty softwarového inženýrství pomocí příběhů. Jelikož jsem četl na _Kindlu_, tak jsem si hodně podtrhával. Z mnoha myšlenek namátkově vybírám ty, které by mohly fungovat i vytržené z kontextu:

> _"This basic what-do-you-do disconect between employees and managers is at the heart of why folks don't trust their managers or find them to be evil."_

<!-- -->
> _"You instinctively know that telling your boss that you had a beer at lunch is a bad idea, not because he'd know it, but because the organization would."_

<!-- -->
> _"If you're sitting in a meeting where you're unable to identify any players, get the hell out. This is a waste of your time."_

<!-- -->
> _"Rule of thumb: When the debate is no longer productive, it's time to make a decision."_

<!-- -->
> _"I know engineers want to solve every problem in the product in any given major release, but that never ever happens ever. Better is the enemy of done, and if it's your project, you need to draw a line on what topics/ideas you intend to tackle and stick to it."_

<!-- -->
> _"In just about every company I've worked at, the only source of measurable truth regarding the product is the bug database."_

<!-- -->
> _"Yes, this guy is a C++ god. The next question is, OK, so he's a god; is he going to piss off the rest of the team by being godlike?"_

<!-- -->
> _"A view: Like the drink, the view is a mental break, an escape to somewhere else that provides a brief alternation of perspective. This is why everyone in the office wants a window. It's not a status symbol, it's an escape."_

<!-- -->
> _"A useful meeting is not a speech; it's a debate."_

Updated: O knížce už před časem psal blog [Java crumbs](//blog.krecan.net/2009/06/21/vedeme-lidske-bytosti/trackback/).
