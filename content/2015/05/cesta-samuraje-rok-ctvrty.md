+++
title = "Cesta samuraje, rok čtvrtý"
date = 2015-05-31T14:52:00Z
updated = 2015-05-31T14:52:46Z
description = """Blog SoftWare Samuraj má čtvrté narozeniny. Tradiční retrospektiva."""
tags = ["sw-samuraj"]
aliases = [
    "/2015/05/cesta-samuraje-rok-ctvrty.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/img/samurai.gif" >}}

Normálně by se řeklo, že _SoftWare Samuraj_ slaví čtvrté narozeniny. Ale tentokrát to na žádnou velkou oslavu není --- z hlediska blogování to bylo velmi slabý. A důvody jsou dost podobné jako [vloni](/2014/06/cesta-samuraje-rok-treti/).

## Maratony

Z volného času mi nejvíc ukrojilo běhání. Vloni jsem běžel dva maratony, letos také dva a jeden až dva mě ještě čekají. Myslím, že trénování na maraton (a maraton samotný) je výborná analogie k softwarovým projektům.

Třeba, že je to tvrdá dřina. Že je nesmysl sprintovat hned na začátku. Že zvítězí (či dokončí) ten, kdo má strategii. Že pokud je _commitment_, tak je nesrovnatelně větší šance na úspěch. A hlavně, maratony (a projekty) se "běhají hlavou".

## Distribuovaný projekt

Pracovně jsem absolvoval velmi náročný projekt a jeho začátek se kreje s koncem mého psaní. Projekt byl sice krátký (od listopadu do května), ale za to vyčerpávající --- je to první projekt v mé kariéře, kdy si musím vzít dovolenou, abych si odpočinul.

Proč vyčerpávající? V první řadě, to byl distribuovaný projekt. A nejen distribuovaný, ale vskutku globální --- zákazník byl v Urugayi, project management ve Francii a (menší) půlka implementačního týmu v Praze a druhá půlka na Filipínách. Sice je to fráze, ale na tomhle projektu to platilo beze zbytku --- _communication is the key_.

Druhým stěžejním důvodem byla relativní juniorita týmu. To nebyl problém z technického hlediska --- technické problémy jsou ty jednodušší k řešení. Problém byl, že se nepodařilo vytvořit silnou týmovou kulturu. Samozřejmě, tohle jde z větší části za mnou, za team leaderem, a vidím to jako své selhání. Už několik měsíců jsem tak ve stadiu analýzi, proč k tomu došlo a co jsem mohl udělat jinak, nebo lépe. Hodně mi v tom pomáhá výborná kniha [Coaching Agile Teams](//www.goodreads.com/book/show/8703232-coaching-agile-teams).

Na druhou stranu, když team nechce dát _commitment_, tak s tím nic nenaděláte a můžete se snažit, jak chcete. A je to potom náročnější pro project managera, team leadera a další, protože team na ně přesouvá zodpovědnost. A na tomhle projektu to bylo náročné velmi.

Ale nechci si stěžovat, projekt považuji za úspěšný --- dodali jsme v čas, v budgetu, v rozumně dobré kvalitě a hlavně, zákazník je spokojený a chce s námi dělat další projekt.

## Závazek

Již dvakrát jsem v tomto článku zmínil termín _commitment_. Ano, momentálně ho považuji za klíčový nástroj, který odděluje úspěšné od neúspěšného. A protože bych zase chtěl začít pravidelně psát, tady je můj závazek:

* Napsat alespoň jeden článek měsíčně.

## Související články

* [Cesta samuraje, rok třetí](/2014/06/cesta-samuraje-rok-treti/)
* [Cesta samuraje, rok druhý](/2013/05/cesta-samuraje-rok-druhy/)
* [Cesta samuraje, rok první](/2012/04/cesta-samuraje-rok-prvni/)
