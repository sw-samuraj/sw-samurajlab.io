+++
title = "Jak dělám Java pohovor III: phone screen"
date = 2015-06-25T21:28:00Z
updated = 2017-03-10T21:09:12Z
description = """Jedna z věcí, ke kterým jsem došel a považuji ji za nutnost
    při technickém recruitingu, je phone screen. Jediný případ, kdy ho nedělám,
    je buď že mám s daným člověkem přímou pracovní zkušenost, anebo jsme se
    předtím už osobně setkali. Jak takový phone screen může vypadat?"""
tags = ["teamleading", "interview", "java"]
aliases = [
    "/2015/06/jak-delam-java-pohovor-iii-phone-screen.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" width="400"
    src="/2015/06/Matrix-Trinity-phone.jpg"
    link="/2015/06/Matrix-Trinity-phone.jpg" >}}

Technickému recruitingu se věnuji už nějaké čtyři roky. Je to činnost, která mě hodně baví a tak jako u jiných aspektů své práce, jsem si vypěstoval určitý postup. Zároveň se snažím věci pořád zlepšovat a korigovat, jak studiem, tak praxí.

## Phone Screen

Jedna z věcí, ke kterým jsem došel a považuji ji za nutnost, je _phone screen_. Jediný případ, kdy ho nedělám, je buď že mám s daným člověkem přímou pracovní zkušenost, anebo jsme se předtím už osobně setkali --- to se stává, pokud třeba někdo zareaguje na [můj inzerát](/2013/07/hledam-do-sveho-tymu-java-vyvojare).

Poslední dobou si hodně čtu o agilním přístupu (i po těch letech se člověk pořád má co učit) a věc, která se mi stále vrací a furt si ji musím připomínat --- každý mítink musí mít smysl. Fajn, jaký je tedy pro mne smysl _phone screenu_? Podívejme se, co stojí v soukromém samurajském slovníku:

**Phone Screen:** _Je to první, krátký a efektivní kontakt s potencionálním kolegou, který zodpoví jedinou otázku --- má smysl se osobně vidět a pokračovat ve vzájemném získávání informací?_

Proč používám termín _phone screen_ a ne třeba "telefonní pohovor"? Inu, to proto, že je to krátké a přesně to vystihuje svůj účel. Uznejte --- "telefonní filtrování" zní blbě samo o sobě a poslat kandidátovi pozvánku s takovým předmětem by asi nebylo zrovna vstřícné.

## Struktura

Můj _phone screen_ má vždy stejný začátek. Zavolám, představím se jménem a firmou a řeknu: _"Tento_ phone screen _by měl trvat 30 minut a projdeme si následující kroky_:

1. _představím se já,_
1. _představí se kandidát z hlediska technologií,_
1. _technické otázky,_
1. _kandidátovy otázky."_

Pak si cca 30 minut povídáme, během té doby bedlivě sleduji čas, a po půl hodině rozhovor ukončím.

Pokud se podívám blíže na jednotlivé body:

_**Představím se já**_, tedy, znovu zopakuji svoje jméno, řeknu svoji formální pozici a krátce vysvětlím obsah své práce. Myslím si, že je to fér vůči kandidátovi --- dát mu nějaký kontext, aby věděl, s kým asi tak mluví.

_**Představí se kandidát z hlediska technologií**_. Jediná informace, kterou o kandidátovi v tento moment mám, je zpravidla pouze jeho CV. Což je dost slabá a nevěrohodná informace. Požádám proto kandidáta, aby se mi představil z hlediska technologií. Zdůrazním, že mě nezajímá klasické "odříkávání" životopisu, stejně tak, jaký byl business význam projektů, nebo aplikací, na kterých se podílel. Co mě zajímá, je jeho současný technologicko-seniorní "snapshot".

Myslím, že je to celkem logické --- nenajímám odborníka na nějakou business doménu, ani business analytika, ale vývojáře. Takže mě zajímají technologie, frameworky, architektura atd. Bohužel, lidé dost často tuto otázku ignorují a spustí naučený kolovrátek. Pokud se tak stane (pořád sleduju ten čas), vrátím je zpátky k původní otázce.

_**Technické otázky**_. Jedním z cílů předchozí části je zjistit kandidátovy silné technologické stránky. Říkám o sobě (ohledně pohovorů), že jsem hodný, ale spravedlivý. Takže se ho ptám na věci, o kterých říká, že je dobře zná. Samozřejmě, musím ty věci dobře znát i já. Ale vzhledem ke svému stáří a senioritě 😉 vždycky najdeme slušný průnik. Někdy se i kandidáta přímo zeptám: _"Jaké jsou momentálně dvě Vaše nejsilnější technologie?"_.

No a pak začneme na povrchu, u triviálních a základních věcí a jdeme hloub a hloub, až se někde naše znalosti zastaví --- narazíme na [lokální minumum](//cs.wikipedia.org/wiki/Extr%C3%A9m_funkce). Pokud se to stane, nebo se téma vyčerpá, nebo pokud přes veškerou snahu zůstaneme stále na hladině, přejdu k dalšímu (silnému) tématu.

Pro představu jak může tahle část vypadat, dejme tomu, kandidát říká, že dobře zná [JPA](//en.wikipedia.org/wiki/Java_Persistence_API), či [Hibernate](//hibernate.org/orm/). Můžeme začít třeba otázkou _"Na jakém principu tahle technologie funguje?"_, nebo _"Jaký je vztah mezi JPA a Hibernate?"_.

Pak se podíváme na mapování pomocí metadat. Můžou to být otázky typu _"Které dvě anotace/XML elementy jsou nezbytně nutné, aby framework mohl používat (POJO) třídu jako entitu?"_, či _"Které anotace při mapování běžně používáte?"_. Podstatné je, že nevyžaduji přesné názvy, ale jestli kandidát rozumí o čem mluví a principům, na kterých to funguje.

A jdeme hlouběji --- jaké mohou být vztahy mezi entitami, jaké jsou [strategie pro získávání dat](//docs.oracle.com/javaee/6/api/javax/persistence/FetchType.html) a která z nich je defaultní pro daný typ vztahu? Jak se řeší složené klíče? Na téhle úrovni složitosti často opustíme mapování entit a podíváme se na jiná zajímavá témata --- přece jenom, děláme _phone screen_ a času je málo.

Můžeme si popovídat třeba o [EntityManageru](//docs.oracle.com/javaee/6/api/javax/persistence/EntityManager.html), nebo o [Session](//docs.jboss.org/hibernate/orm/4.3/javadocs/org/hibernate/Session.html). Pokud nám to spolu klape, můžeme se dostat až k transakcím, nebo co to je [PersistenceContext](//docs.oracle.com/javaee/6/api/javax/persistence/PersistenceContext.html) a [PersistenceUnit](//docs.oracle.com/javaee/6/api/javax/persistence/PersistenceUnit.html). Někdy dokonce dojde i na lifecycle entit 😀

Ať už se dostaneme kamkoliv, je podstané, jak jsme si o tom popovídali. Už jsem zmiňoval, nejde mi o žádné odříkávání slovníkových dat a in-memory znalostí. Jde mi o porozumnění a rozsah, které by mělo odpovídat _dané úrovni seniority_. Jednotlivé otázky, jak úvodní, tak usměrňující vybírám intuitivně, nemám žádný mustr, podle kterého jedu.

Když se čas nachýlí, je prostor pro _**kandidátovy otázky**_. Kandidát mi věnoval cca 25 minut svého času a tak je fér mu otevřeně zodpovědět, co by ho zajímalo. Možná si říkáte, že 5 minut na otázky není moc, ale většinou to bohatě stačí --- moje zkušenost je taková, že nadpoloviční většina lidí nemá žádné otázky, nebo tak jednu dvě. Pokud je někdo zvědavější a má otázek víc, i já mu (rád) věnuji víc času.

## Zakončení

Jakmile jsou otázky za náma, zbývá říct už jen dvě věci --- co bude následovat a rozloučení. Co následuje po rozhovoru? Za pomocí svých poznámek (viz dále) sepíšu jednoduché hodnocení s pozitivním, nebo negativním výsledkem, který se kandidát vzápětí dozví. Pozitivní skóre se rovná pozvání na osobní pohovor.

## Jak si píšu poznámky

Když jsem s _phone screeny_ začínal, psával jsem si poznámky přímo do vytištěného CV. Není to dobrý způsob --- každé CV je jinak formátované, velikost bílých ploch na psaní je proměnlivá, je to neuspořádané atd. Jedinou výhodou je, že CV a poznámky jsou pohromadě.

Časem jsem ale zakomponoval lepší metodu --- přímo během pohovoru si kreslím mind mapu. Stejnou metodu pak používám i u osobního pohovoru (Je to konzistentní a dobře se v tom hledá). Výhodou mind mapy je, že si daleko lépe připomenete, o čem jste si povídali. A taky to lépe vypadá. Navíc, mind mapa se mnou zůstane --- zpracovaná CV vyhazuji, ale mind mapy si schovávám a archivuji.

{{< figure
    src="/2015/06/PhoneScreen.jpg"
    link="/2015/06/PhoneScreen.jpg" >}}

## Související články

* [Jak dělám Java pohovor](/2012/11/jak-delam-java-pohovor)
* [Jak dělám Java pohovor II: proč nedávám testy?](/2014/07/jak-delam-java-pohovor-ii-proc-nedavam-testy)
* [Jak se nabírají Javisti na Filipínách](/2014/01/jak-se-nabiraji-javisti-na-filipinach)
* [Jak dělají Java pohovor jinde](/2013/02/jak-delaji-java-pohovor-jinde)
* [Měl by mít vývojář portfolio?](/2013/06/mel-by-mit-vyvojar-portfolio)
* [Kontrakt místo pohovoru, je to reálné?](/2013/08/kontrakt-misto-pohovoru-je-to-realne)
* [Geek, který zapadne](/2013/04/geek-ktery-zapadne)
