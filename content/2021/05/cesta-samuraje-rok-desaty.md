+++
title = "Cesta samuraje, rok desátý"
date = 2021-05-21T13:49:47+02:00
description = """Blog SoftWare Samuraj slaví jubileum - desáté narozeniny! 🎂 🔟"""
tags = ["sw-samuraj"]
hero_image = "hero.jpg"
thumbnail = "2021/05/Birthday.jpg"
+++

{{< figure src="/2021/05/Birthday.jpg" link="/2021/05/Birthday.jpg" >}}

Už několik měsíců to nade mnou viselo --- blog, který jsem před deseti lety
založil se blížil ke svému jubilejnímu výročí --- a já jsem přemýšlel, jak
tento tradiční článek pojmout.

V minulých letech jsem vždycky nějak zhodnotil daný rok, co se v mém profesním
(a někdy i osobním) životě událo, často jsem přidal nějakou prognózu, či přání
do budoucna. Vloni jsem z tohoto mustru vykročil --- poprvé za tu dobu jsem si
udělal jednoduchou statistiku všech článků, které jsem zde na blogu napsal.

Nakonec jsem dospěl k závěru, že k jubileu udělám také přehled těch uplynulých let.
Ale jinak --- rozhodl jsem se, že projdu všechny články a vždycky k určitému roku
vyberu jeden, maximálně dva, které stojí za přečtení a přidám krátký komentář,
proč si za nimi i po létech stojím.

Původně jsem si říkal, že k danému roku musím vybrat jen jeden článek, což bude
takové _crème de la crème_, ale čím víc jsem nad tím přemýšlel, tím víc jsem
viděl jeden rozpor --- vybrat technický článek na úkor článků
o [teamleadingu](/tags/teamleading), nebo obráceně. A tak, pokud k tomu došlo,
vybírám pro ten rok článek z obou těchto oblastí.

Vítejte ve stroji času!

## Rok první

### [Enterprise integrace, messaging](/2011/10/enterprise-integrace-messaging/)

Rok kdy jsem začal psát blog _SoftWare Samuraj_, byl obdobím, kdy jsem se začínal
nořit do světa systémových integrací a (převážně) webových služeb. To období
trvá dodnes, i když původní technologie jsou buď mrtvé, nebo umírající. Každopádně,
[Enterprise Integration Patterns](//www.enterpriseintegrationpatterns.com/patterns/messaging/index.html)
(EIP) jsou nesmírně nadčasové a minimálně polovina současných _cloud patterns_
jsou jenom převlečený EIP.

### [(Ne)funkční tým](/2011/11/nefunkcni-tym/)

Blog jsem začal psát jako seniorní vývojář a juniorní team leader. Je několik
knih, které mne v managementu silně ovlivnily a [The Five Dysfunctions
of a Team](//www.goodreads.com/book/show/21343.The_Five_Dysfunctions_of_a_Team)
je jedna z nich. Nevybral jsem si ji tehdy náhodou a dodnes používám její
principy i mimo pracovní kolektiv.

## Rok druhý

### [Architektonické principy RESTu](/2012/10/architektonicke-principy-restu/)

Technicky jsem byl v té době hodně ponořený do světa webových služeb postavených
na [SOAPu](//en.wikipedia.org/wiki/SOAP) a projekty, na kterých jsem tehdá pracoval,
REST zapovídaly. Ale už tehdy jsem myslel na budoucnost a voilà, REST je posledních
několik let mým denním chlebem. Tenhle článek bylo moje první seznámení s RESTem
a hned jsem cítíl potřebu se o své znalosti podělit.

### [Kanban, zprávy z fronty](/2013/01/kanban-zpravy-z-fronty/)

Druhý rok _SoftWare Samuraje_ byl pro mne daleko důležitejší z hlediska project
managementu, než kvůli technickým záležitostem --- ten rok jsem totiž potkal
[Kanban](/tags/kanban), který jsem od té doby aplikoval na cokoliv, čím jsem
se pracovně zabýval. Formálně, neformálně, pořád. Myslím, že je to nejlepší
způsob, jak pracovat v malém softwarovém týmu.

## Rok třetí

### [Gradle, moderní nástroj na automatizaci](/2013/05/gradle-moderni-nastroj-na-automatizaci/)

Ten rok jsem se po čase opět vrátil k automatizačnímu nástroji _Gradle_ a
začal jsem ho používat kde to jen šlo. Tohle lehce evangelizační úsilí mi
vydrželo v podstatě až do závěru mé _Java_ kariéry. Dnes už je _Gradle_
zase jinde a jeho migrace na _Kotlin_ je krok správným směrem.

### [Jak se nabírají Javisti na Filipínách](/2014/01/jak-se-nabiraji-javisti-na-filipinach/)

Někdy v těch dobách jsem prožíval zlaté období, které jsem trávil _technical
hiringem_. Napsal jsem o tom [několik článků](/tags/interview), ale žádný
z nich není tak exotický jako ten, kde popisuju svůj pracovní výlet do Asie.

## Rok čtvrtý

### [Code review checklist](/2014/07/code-review-checklist/)

Čtvrtý rok byl na úrodu článků chudý a tak není až tak z čeho vybírat.
Nicméně článek o _code review_ by patřil ke zlatému fondu v jakékoliv
době --- týmů, které kontinuálně bojují s _code review_ je stále dost
a dost a vysvětlovat k čemu to je a jak se to dělá je nekončící
sysifovská práce.

## Rok pátý

### [Jak dělám Java pohovor III: phone screen](/2015/06/jak-delam-java-pohovor-iii-phone-screen/)

Jelikož jsem v pátém roce publikoval jen jeden jediný článek, tak čistě
formálně je to ten nejlepší článek, který jsem daný rok napsal. 🤭 A on
není vůbec špatný. O tom, jak dělám technické interview jsem psal během
let opakovaně a _phone screen_ je jedna z důležitých komponent, kterou
jsem si za ta léta vytvořil a vycizeloval.

## Rok šestý

### [Programátor -> Vývojář -> Software Engineer](/2017/02/programator-vyvojar-software-engineer/)

V tomhle roce jsem hodně přehodnocoval svou kariéru a přemýšlel, kam se posunout,
co opustit atd. Často jsem přemýšlel nad svojí vlastní cestou, kterou jsem
pracovně v IT urazil a postupně přišel s konceptem, který je popsaný
v tomto článku. Zajímavé je, že dnes si opět říkám _programátor_.

## Rok sedmý

### [Spring Security, SAML & ADFS: Úvod](/2017/12/spring-security-saml-adfs-uvod/)

Sedmý rok _SoftWare Samuraje_ se nesl ve znovunalezení radosti z řešení technických
problémů. Hodně jsem prototypoval a nasával nové technologie a znalosti. Je toho
víc, co bych z technických článků vybral, ale asi nejtrvalejší se mi jeví
miniseriál o _SAMLu_. Bylo to taky poprvé, kdy jsem si pořádně sáhnul
na [federated security](//en.wikipedia.org/wiki/Federated_identity).

### [Smutná zpráva o stavu IT trhu](/2017/09/smutna-zprava-o-stavu-it-trhu/)

Jestliže jsem v minulém roce přehodnocoval svou kariéru, byl tento rok
časem uskutečnění té změny. Ponořil jsem se do víru IT pohovorů a byl
to velmi tristní zážitek. Myslel jsem si, že za tu dobu, co píšu
o _technical hiringu_ se český trh třeba někam posunul... ale bylo to
kruté vystřízlivění. Z dnešního pohledu bych ještě asi doplnil,
že se na tom podílel i skrytý [ageism](//en.wikipedia.org/wiki/Ageism).

## Rok osmý

### [Golang micro-services, první ohlédnutí](/2018/09/golang-micro-services-prvni-ohlednuti/)

S novým zaměstnáním přišly nové výzvy. Přesunul jsem se kompletně do cloudu
a jako slepý k houslím jsem se dostal ke _Golangu_. Byla to láska na první
pohled. Zbytek už je historie.

## Rok devátý

### [Infrastructure as Code, lehký úvod](/2020/05/infrastructure-as-code/)

S nástupem _DevOpsu_ se začalo objevovat spoustu zajímavých oblastí a
_Infrastructure as Code_ je jedna z nich. Vždycky jsem tíhnul k buildům a
deploymentům, takže tohle jsem si nemohl nechat ujít. Jsem v tom tak dobrej,
že to s kolegou dokonce vyučujeme na MatFyzu. 💪 🙄

### [Lead or Follow? 🤔 Bullshit!](/2020/03/lead-or-follow-bullshit/)

Tím, jak jsem přehodnotil svou roli, potřeboval jsem se trochu vymezit
vůči různým [macho](//en.wikipedia.org/wiki/Machismo) manažerům, kteří
sice mají rad na rozdávání, ale životních zkušeností se jim zas až tak
nedostává. Pravda, když víte, co chcete, tak vás tyhle týpci nemusí
trápit. Ale občas mi to nedá někoho popíchnout. 😈

## Rok desátý

### [Distribuovaný monolit](/2020/09/distribuovany-monolit/)

Vytváření monolitů má v IT obrovskou tradici a setrvačnost a je
prorostlý hlavama lidí jako rakovina. Není jenom monolitická
architektura... je toho mnohem víc. Jako správný Lucifer 😈 jsem se
rozhodnul vnést světlo do temnoty.


## Související články

* [Cesta samuraje, rok devátý](/2020/05/cesta-samuraje-rok-devaty/)
* [Cesta samuraje, rok osmý](/2019/06/cesta-samuraje-rok-osmy/)
* [Cesta samuraje, rok sedmý](/2018/05/cesta-samuraje-rok-sedmy/)
* [Cesta samuraje, rok šestý](/2017/06/cesta-samuraje-rok-sesty/)
* [Cesta samuraje, rok čtvrtý](/2015/05/cesta-samuraje-rok-ctvrty/)
* [Cesta samuraje, rok třetí](/2014/06/cesta-samuraje-rok-treti/)
* [Cesta samuraje, rok druhý](/2013/05/cesta-samuraje-rok-druhy/)
* [Cesta samuraje, rok první](/2012/04/cesta-samuraje-rok-prvni/)
