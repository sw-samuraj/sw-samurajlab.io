+++
title = "Fluentd, case study (část 3)"
date = 2021-03-13T16:57:19+01:00
description = """Fluentd je modernější alternativou k etablovanému řešení
    pro sbírání distribuovaných logů. The ELK is dead, long live
    the Fluentd! Třetí část, s case study."""
tags = ["cloud", "logging"]
hero_image = "hero.jpg"
thumbnail = "2021/03/Logs.jpg"
+++

{{< figure src="/2021/03/Logs.jpg" link="/2021/03/Logs.jpg" >}}

Pamatujete si na film [Inception](//www.csfd.cz/film/254156-pocatek/prehled/)?
Šlo o to, že ve snu se odehrává sen, ve kterém se odehrává sen a něm... se
odehrává sen. 🤯 Tak něco podobného jsem implementoval ve [Fluentd](//www.fluentd.org/).

Jak už jsem psal v úvodu této minisérie o _Fluentd_, dostal jsem se
k této technologii v rámci práce na novém produktu. Nebudu ho jmenovat,
ale zaměřím se na statickou architekturu a jak na ní aplikovat _Fluentd_.

## Technický popis produktu

Byznysově spadá _nejmenovaný produktu_ od oblasti infrastrukturní nativní
cloudové služby na pomezí [IaaS](//en.wikipedia.org/wiki/Infrastructure_as_a_service)
a [PaaS](//en.wikipedia.org/wiki/Platform_as_a_service). Jak je u cloudových
služeb běžné, je _nejmenovaný produkt_ rozdělen na dvě části _Control Plane_
a _Data Plane_. Inkriminovaná část, která používá _Fluentd_ spadá do _Data Plane_

(Poznámka pro ne-cloudové lidi: _Control Plane_ se stará o vytváření a lifecycle
instancí _Data Plane_. _Data Plane_ pak dělá samotný business.)

Proč jsem v úvodu zmiňoval film _Inception_? Protože tahle část _nejmenovaného
produktu_ je ve skutečnosti _compute_ postavený na _computu_. Navíc proložený
[Kubernetes](//kubernetes.io/).

(Poznámka pro ne-cloudové lidi: _Compute_ je infrastruktura v cloudu, která
poskytuje výpočetní výkon.)

Základní jednotkou našeho _computu_ je něco, čemu říkáme _cell_, buňka.
(Pro potřeby článku není podstatné, co _cell_ počítá.) _Cell_ je bezstavová
aplikace, která běží v [Docker](//www.docker.com/) kontejneru. Kontejnery
běží v [Kubernetes](//kubernetes.io/). _Kubernetes_ nody běží na standardním
cloudovém _computu_ (provider není důležitý).

Zatím mě stačíte sledovat? Pojďme dál, protože to ještě není tak složitý.
Důležité jsou tři věci:

1. Důvodem existence _cell_ je paralelizace výpočtu a agregace výsledků.
1. Popisované řešení je [multitenant](//en.wikipedia.org/wiki/Multitenancy).
1. Řešení používá [autoscale](//en.wikipedia.org/wiki/Autoscaling).

To má důležité implikace, ale pro náš technický popis je podstatné, že to
vede k mnohočetným _Kubernetes_ clusterům. Zároveň je potřeba říct,
že instance _cell_ (resp. daný _Docker_ kontejner) může vzniknout v (téměř)
libovolném clusteru a samozřejmě, pokud se kontejner s _cell_ z nějakého
důvodu restartuje, může ho _Kubernetes_ nově instancovat na libovolném
nodu daného clusteru. No a autoscale způsobuje, že _cell_ dynamicky vznikají
a zanikají.

Poslední dílek do skládačky je kvantitativní velikost řešení. Bavíme se
o stovkách až tisících _cell_ rozložených v jednotkách clusterů v rámci
jednoho cloud regionu. Jen pro představu, naše vývojářské testovací
prostředí má momentálně ve 2 _clusterech_ cca 400 _nodů_.

{{< figure src="/2021/03/Kubernetes-cell-layout.png"
    link="/2021/03/Kubernetes-cell-layout.png"
    caption="Rozložení _cell_ v _Kubernetes_ clusterech" >}}

## Design Fluentd řešení

Teď, když víme, v jaké škále a v jakém designu nám běží všechny _cell_, je
úkol _Fluentd_ přímočarý --- posbírat všechny logy ze všech clusterů.
Schválně říkám "všechny logy". Protože v tom _Kubernetes_ neběží jen
_cell_, ale i další kontejnery, které podporují dané řešení --- ať už
jako [sidecars](//docs.microsoft.com/en-us/azure/architecture/patterns/sidecar)
běžící ve stejném [podu](//kubernetes.io/docs/concepts/workloads/pods/) jako _cell_,
nebo jako [daemon set](//kubernetes.io/docs/concepts/workloads/controllers/daemonset/)
běžící na daném nodu clusteru.

(Poznámka pro ne-kubernetes lidi: _daemon set_ je _pod_, který běží na daném
nodu pouze jednou. _Pod_ je nejmenší deployment jednotka, kterou _Kubernetes_
spravuje.)

Jelikož všichni rádi logují do souborů, vydali jsme se touhle cestou --- všechny
log soubory jsou namapovány do nějakého adresáře na daném _Kubernetes_
[nodu](//kubernetes.io/docs/concepts/architecture/nodes/). U _daemon setů_ je to
mapování 1:1. U ostatních _podů_ je to mapování do adresářové struktury ---
každá _cell_ má svoje ID a (její kontejner) mapuje log do adresáře s názvem
tohoto ID. Například _cell_ s ID `cell-v1-42` mapuje do adresáře
`/tmp/cells/cell-v1-42`.

_Fluentd_ běží na daném nodu jako _daemon set_ a pomocí [tail](//docs.fluentd.org/input/tail)
pluginu sleduje všechny tyto log soubory a dále je zpracovává.
Velkou výhodou _tail_ pluginu je, že kromě jednotlivých souborů umí
zpracovávat soubory v hierarchické adresářové struktuře. 👍

{{< figure src="/2021/03/Logs-component-diagram.png"
    link="/2021/03/Logs-component-diagram.png"
    caption="Komponent diagram logů na Kubernetes nodu" >}}

Co v tomto článku pominu, jsou použité [output pluginy](//docs.fluentd.org/output),
protože to není nijak zajímavé, ani zprostředkovatelné --- výstupním
systémem je proprietární řešení, které nebudu popisovat, ale vy si tam
můžete představit [Elasticsearch](//www.elastic.co/elasticsearch/).

[Filter pluginy](//docs.fluentd.org/filter) také více méně přeskočím, krátce
je zmíním v sekci _Problémy_.

## Problémy

Samozřejmě, jedním ze základních problémů v distribuovaném prostředí je
[reliability](//en.wikipedia.org/wiki/Reliability_engineering) (a s ním často
související [final consistency](//en.wikipedia.org/wiki/Eventual_consistency)).
Pro _Fluentd_, zrozeném v cloudovém paradigmatu, to není žádný problém ---
out-of-the-box má zabudovaný štědrý
[retry](//docs.fluentd.org/troubleshooting-guide#my-logs-have-stopped-sending-data-to-my-backend)
mechanizmus s [exponential backoff](//docs.fluentd.org/buffer#how-exponential-backoff-works)
sekvencí.

OK, jeden problém vyřešen, ale místo něj máme hned dva další: 1) jak zjistit, že
dochází k opakovanému volání cílové platformy a 2) jaká chyba ho způsobuje? Bod
číslo jedna má jednoduchou odpověď --- monitoring (viz další sekce) a na něj navázané
alerty.

Bod číslo dva záleží na prostředí --- u výše popsaného řešení to znamenalo hlavně
být schopný:

* získat logy pro daný _pod_ a _daemon set_,
* být [fluent](//translate.google.com/?sl=en&tl=cs&text=fluent&op=translate)
  v `kubectl` a umět debuggovat v _Kubernetes_,
* dohledat konkrétní _node_ a přes `ssh` udělat lokální investigaci.

### Ladění bufferu

Konkrétní problém, který jsem řešil v této oblasti, bylo jednak odmítání
logů cílovým systémem a jednak jeho zahlcení počtem requestů.
_Fluentd_ odesílá logy v tzv. [chunks](//docs.fluentd.org/buffer#how-buffer-works).
Kromě toho, že byla výchozí velikost chunku příliš veliká, tak se také posílaly
v příliš velkých dávkách. Ohledně velikosti to chce trochu experimentovat,
protože vy zpravidla víte počet vstupních řádků, ale nevíte velikost v bytech.
Navíc ve výstupních chuncích můžou být data v jiném formátu, či různě odekorovaná.

[Velikost chunku](//docs.fluentd.org/configuration/buffer-section#buffering-parameters)
ale není všechno. Dalším důležitým parametrem je nastavení odesílání
([flushing](//docs.fluentd.org/configuration/buffer-section#flushing-parameters)),
tedy jak často se buffer vyprázdní a v kolika threadech odesílání probíhá.
Aby toho nebylo málo, je dobré se zamyslet nad typem bufferu --- paměť, nebo soubor.
Je tam mimo jiné velký rozdíl v maximální velikosti bufferu (8 MB vs. 50 MB).

Ve finále jsem skončil s nastavováním parametrů `chunk_limit_size`, `queue_limit_length`,
`flush_interval` a `flush_thread_count`. (Konkrétní hodnoty zde neuvádím, protože
bez znalosti daného prostředí nedávají smysl.)

{{< figure src="/2021/03/Queue.jpg" link="/2021/03/Queue.jpg" >}}

### Sanitizace záznamů

Další problém, který jsem řešil, byl v chybně formátovaných a nevalidních
(originálních) logovacích záznamech, tedy těch, které produkovala přímo ona
zmíněná aplikace, _cell_. Pominu takové drobnosti, jako chybně
zformátované datum, či nesoulad mezi [unix timestamp](//en.wikipedia.org/wiki/Unix_time)
a timestamp v milisekundách.

Zajímavější byl případ nezpracovaných chunků, které chyběly v cílové systému.
To bylo docela kuriózní, protože vývojáři oné _cell_ byli low level C-čkaři
(do morku kostí), zcela nepolíbení cloudem a distribuovanými systémy a tak se
občas a náhodně stávalo, že thread zapisující do logu spadnul před ukončením
zápisu a v logu tak byly zapsány nevalidní znaky. To se tak stává, když
do souboru zapíšety místo 8 bitů třeba jen 4 nebo 5.

Na úrovni _Fluentd_ je potřeba to řešit na dvou úrovních --- jednak, aby to 
_tail_ plugin vůbec načetl a jednak, jak to ošetřit dál v pipeline. V prvním
případě jde o to, jak tolerantní je JSON parser. Záleží na vaší instalaci
_Fluentd_, jaký _Ruby_ JSON parser se používá --- občas si můžete v logu
_Fluentd_ povšimnout hlášky:

```bash
Oj is not installed, and falling back to Yajl for json parser
```

`Oj` a `Yajl` jsou dva _Ruby_ parsery. Nijak se v tom nevyznám, který
z nich je lepší, či vhodnější, ale vzhledem k mému problému --- `Oj`
byl schopný invalidní znaky načíst, zatímce `Yajl` ne (prostě spadnul).
Pomoc je jednoduchá --- nainstaloval buď `Oj`, nebo `ruby-bigdecimal`.
Viz také [Fluentd FAQ](//docs.fluentd.org/quickstart/faq#fluentd-warns-oj-is-not-installed-and-falling-back-to-yajl-for-json-parser).

Jak vyřešit druhý případ jsem naznačil ve minulém díle tohoto miniseriálu
--- stačí zapojit [filter](//docs.fluentd.org/filter) plugin a nevalidní
znaky odfiltrovat, nahradit apod.

{{< gist sw-samuraj 90a179100abcc98e300f9aa233d0c75c >}}

### Zálohování chunků

Třetí záležitost, kterou jsem řešil, nebyl problém sám o sobě, ale spíš
optimalizace vyhledávání a prozkoumávání chyb. Pokud mi vývojáři _cell_
nahlásili, že v cílovém systému (pravděpodobně) chybí "nějaké logy",
znamenalo to zdlouhavou investigaci. Ve snaze zkrátit tento čas bylo
primární zjistit, zda _cell_ daný log v první řadě vůbec vyprodukovala.

Přišel jsem tedy s řešením --- v rámci debug módu --- zálohovat "raw"
logy a jejich chunky na [object storage](//en.wikipedia.org/wiki/Object_storage)
a v případě problému dané logy stáhnout a progrepovat. Celým technickým
řešením se zde nebudu zabývat, jen zmíním implementaci týkající se
_Fluentd_ --- chunky čekající na odeslání jsem zálohoval pomocí
[exec](//docs.fluentd.org/output/exec) pluginu, kterým jsem provolával
jednoduchou CLI aplikaci (napsanou v _Golangu_), která pomocí
SDK daného _object storage_ řešení ukládala chunky do zvoleného
[bucketu](//en.wikipedia.org/wiki/Object_storage#Abstraction_of_storage).

{{< gist sw-samuraj dd904277012b286a8728f3ee67ee6ad1 >}}

## Monitoring

Monitoring _Fluentd_ by si zasloužil samostatný článek, ale já už bych
rád tenhle miniseriálek ukončil, takže to vezmu hopem.

Synonymem pro monitoring v cloudu je [Prometheus](//prometheus.io/), který
je, tak jako _Fluentd_, graduovaným [CNCF](//www.cncf.io/projects/) projektem.
_Fluentd_ nabízí _Prometheus_ plugin, který se dá jednoduše zprovoznit a
nakonfigurovat, viz dokumentace [Monitoring by Prometheus](//docs.fluentd.org/monitoring-fluentd/monitoring-prometheus).
Tím dostaneme zadarmo spoustu prometheovských metrik, otázka zní, které
z nich jsou (pro troubleshooting) důležité. Ty, které jsem sledoval já,
jsou následující:

* `fluentd_input_status_num_records_total` počet vstupních záznamů,
* `fluentd_output_status_num_records_total` počet výstupních záznamů,
* `fluentd_output_status_num_errors` počet výstupních chyb (při volání cílového systému),
* `fluentd_output_status_buffer_queue_length` délka fronty pro buffer,
* `fluentd_output_status_retry_count` počet opakovaných volání po výstupní chybě.

To, že _Fluentd_ vystavuje metriky samozřejmě nestačí --- je potřeba metriky
sbírat, agregovat, vizualizovat a v případě problému, spustit alarm. Na sbírání
metrik je ideální... _Prometheus_ 🤭. Jak přesně bude vypadat architektura
sbírání metrik je závislé na prostředí a návazných systémech, takže nelze
poradit ideální řešení.

V našem případě bylo cílové prostředí pro metriky proprietární, takže jsem
skončil s řešením, kdy byl _Prometheus_ deployovaný jako _daemon set_ v jednom
_podu_ s _Fluentd_ a ještě další aplikací, která překládala metriky pro cílový systém
a taky je do něj publikovala. A na vizualizaci obligátní [Grafana](//grafana.com/grafana/).
Alarmy opět proprietární, nicméně [prometheovské alerty](//prometheus.io/docs/practices/alerting/)
jsou víc než dostačující.

{{< figure src="/2021/03/Grafana.png"
    link="/2021/03/Grafana.png"
    caption="Grafana dashboard s Fuentd metrikami" >}}

## Závěr

Výše popsané řešení jsem více méně full time implementoval, ladil a vylepšoval
zhruba dva měsíce. Po skončení této periody musím říct, že _Fluentd_ je
vhodné technické řešení pro daný use case, out-of-the-box má spoustu
vhodných nástrojů a co se týká performance, bez problémů a spolehlivě
zvládá zpracovat desítky GB logů denně v netriviálním distribuovaném
prostředí.

Vlastně jsem zatím nenarazil na žádný problém, který by nebyl řešitelný
skrze dostupné pluginy, či pomocí podpůrných koexistujících aplikací.
Na druhou stranu, kdyby se vyskytl zásadní problém, který by vyžadoval
implementaci nativního pluginu napsaného v _Ruby_, do toho by se mi moc
nechtělo. Škoda že _Fluentd_ není napsané v _Golangu_ --- myslím, že by
tím byl svět cloudových vývojářů o něco jednodušší a přívětivější.

## Související články

* [Fluentd, budoucnost logování (část 1)](/2020/10/fluentd-budoucnost-logovani/)
* [Fluentd, lehký úvod (část 2)](/2020/12/fluentd-lehky-uvod/)
* [Nešvary logování](/2017/12/nesvary-logovani/)
