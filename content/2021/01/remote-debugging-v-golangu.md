+++
title = "Remote debugging v Golangu"
date = 2021-01-08T19:53:38+01:00
description = """Návod jak z Goland IDE (nebo IntelliJ IDEA) debuggovat
    Golang aplikaci běžící v Dockeru."""
tags = ["golang"]
hero_image = "hero.jpg"
thumbnail = "2021/01/Gopher.png"
+++

{{< figure src="/2021/01/Gopher.png" link="/2021/01/Gopher.png" >}}

V Golangu dělám už druhým rokem a je to láska. Je to první jazyk,
po kterém sáhnu, když potřebuju udělat nějaký [PoC](//en.wikipedia.org/wiki/Proof_of_concept#Software_development).
Nejčastěji používám [GoLand](//www.jetbrains.com/go/) IDE, občas píšu
ve _Vimu_ s [vim-go](//github.com/fatih/vim-go) pluginem. Zkoušel jsem
i [VS Code](//code.visualstudio.com/), ale nějak jsme se neskamarádili.

Vzhledem k tomu, že poslední tři roky píšu cloudovou infrastrukturu,
jsou moje aplikace relativně jednoduché micro-servisy. A tak jsem si
prozatím vystačil jen s lokálním debuggingem přímo z IDE. Ale na každého
jednou dojde...

> Pokud tomu rozumím, stačí logování a testy. Debugguju, jen když nechápu,
  jak to funguje. ~ SoftWare Samuraj

Tak co se to stalo, že jsem najednou potřeboval [remote
debugging](//en.wikipedia.org/wiki/Debugging#Techniques)? Tož, to bylo tak...

Měl jsem napsanou aplikaci, krásně otestovanou, lokálně běhala jak
víno. Pak jsem ji vrazil do _Dockeru_ a najednou nefungovala. Marně
jsem měnil parametry, marně jsem přidával logovací hlášky, marně jsem
pročítal zdrojáky třetích stran. Musel jsem si přiznat, že nic jiného,
než remote debugging mi nepomůže.

Čili otázka zní: jak debuggovat _Golang_ aplikaci běžící v _Dockeru_?

## Golang debugger

Golang debugger se jmenuje [Delve](//github.com/go-delve/delve) a žije
na _GitHubu_. Instalace je přímočará a jakmile ji dokončímě, měli bychom
vidět:

```bash
$ go get github.com/go-delve/delve/cmd/dlv
$ dlv version
Delve Debugger
Version: 1.5.1
Build: $Id: bca418ea7ae2a4dcda985e623625da727d4525b5 $
```

Jelikož se chci věnovat pouze remote debuggingu, tak pominu ostatní finesy,
které _Delve_ nabízí a zaměřím se pouze na dva následující příkazy:
`dlv attach` & `dlv exec`.

### dlv attach

Příkaz `dlv attach` způsobí, že se debugger připojí k běžícímu procesu
a začne novou debug session. Po skončení session je možné proces buď
nechat běžet, nebo ukončit. Syntaxe je jednoduchá:

```bash
dlv attach <pid>
```

### dlv exec

Příkaz `dlv exec` spustí danou binárku a nastartuje novou debug session.
Při exitování se opět můžeme rozhodnout, jestli proces killnout, nebo
nechat žít. Syntaxe taktéž jednoduchá:

```bash
dlv exec <path-to-binary>
```

### Options pro remote debugging

Když jsem před chvílí říkal, že syntaxe je jednoduchá, tak jsem trochu
lhal. Aby se ty dva předešlé příkazy daly použít pro remote debugging,
je potřeba je trochu vylepšit pár nutnými, či vhodnými přepínači. Jsou to:

* `--listen=:2345` port, ev. adresa, na kterém naslouchá debug server.
* `--headless=true` spustí debug server
  v [headless](//en.wikipedia.org/wiki/Headless_computer) modu.
* `--api-version=2` nutné, aby si s debug serverem rozuměl _Goland_ (_IDEA_).
* `--accept-multiclient` debug server akceptuje více klientských spojení.

## Nastavení Goland IDE

Konfigurace v _Golandu_ (identické platí pro _IntelliJ IDEA_) je
jednoduchoučká: stačí nastavit adresu `host` a `port` na kterém
běží debug server.

Jelikož my budeme debugovat proces běžící v (lokálním) _Dockeru_,
můžeme nechat `localhost`.

{{< figure src="/2021/01/Debug-configuration.png"
    link="/2021/01/Debug-configuration.png"
    caption="Nastavení Go remote debuggingu v Goland IDE (taktéž IntellJ IDEA)" >}}

## Příprava Docker image

Nachystat si _Docker_ image pro remote debugging také není nijak složité,
stačí dvě věci:

1. Zkopírovat binárku debuggeru `dlv` do _Docker_ image.
1. Spustit debugovanou aplikaci pomocí `dlv exec`.

Spouštěcí příkaz bude vypadat takto:

```bash
dlv --listen=:2345 --headless=true --api-version=2 \
    --accept-multiclient exec ./remote-debug
```

Ukázkový `Dockerfile`:

{{< gist sw-samuraj c9c64667809a64b983d4f5e13a959b1d >}}

## Remote debugging in Action

Nyní již máme všechno nachystáno a můžeme začít debuggovat:

### 1) Spustit _Docker_ image

_Docker_ image se spustí běžným příkazem --- _Docker_ o probíhajícím
debuggování nic neví. Debuggovaná aplikace čeká na připojení
debuggeru z IDE. Na výpisu vidíme: `API server listening at: [::]:2345`.

```bash
container="golang-remote-debug"
image="sw-samuraj/${container}"

docker run --rm --net=host --name "${container}" "${image}"
```

{{< figure src="/2021/01/Debug-log.png"
    link="/2021/01/Debug-log.png"
    caption="Logování aplikace běžící v _Dockeru_. V úvodu je vidět, jak debug server naslouchá." >}}

### 2) Nastavit breakpoint v IDE a spustit debug session

Debuggovat umíte, ne? A jak se to dělá ve vašem IDE snad taky víte, ne? 😉

### 3) Spuštění kódu s breakpointem

V ukázkovém projektu uvedeném na konci článku, běží v _Dockeru_ jednoduchá
webová služba poslouchající na portu `4040`. Stačí ji provolat pomocí `curl`:

```bash
curl -v localhost:4040
```

{{< figure src="/2021/01/Curl.png"
    link="/2021/01/Curl.png"
    caption="Provolání debuggované aplikace pomocí `curl`" >}}

### 4) Klasický debugging

Teď by se mělo ozvat naše IDE a je čas na klasický debugging:
_step over_, _step into_, atd.

{{< figure src="/2021/01/Goland-debug.png"
    link="/2021/01/Goland-debug.png"
    caption="Debuggování v Goland IDE" >}}

### 5) Ukončení debuggingu

Našli jsme chybu? Paráda! 🙌

V IDE ukončíme debug session a rozhodneme, jestli ukončíme i debuggovaný
process v _Dockeru_ nebo ho nacháme běžet dál.

## Ukázkový projekt

Jednoduchý projekt na otestování popsaného postupu lze nají na _GitHubu_:

* [golang-remote-debugging](//github.com/sw-samuraj/golang-remote-debugging)

Pokud jsem nic neopomněl, mělo by stačit pustit následující příklady a
nastavit ve vašem IDE remote debugging session.

```bash
./build-docker.sh
./run-docker.sh
```
