+++
title = "FOSDEM 2021, Golang room"
date = 2021-02-08T22:45:03+01:00
description = """Moje první virtuální konference, dvoudenní FOSDEM.
    Navštívil jsem Golang a Monitoring roomy."""
tags = ["golang", "konference"]
hero_image = "hero.jpg"
thumbnail = "2021/02/Fosdem-Golang.png"
+++

{{< figure src="/2021/02/Fosdem-Golang.png" link="/2021/02/Fosdem-Golang.png" >}}

Nejsem vymetač konferencí. V dobách před pandemií to nebylo úplně jednoduché
se dostat na zahraniční konferenci --- stálo to dost peněz (od registrace,
přes cestu a ubytování) a ne každá firma to chtěla zaplatit. Dnes, kdy se
všechny konference přesunuly do virtuálního prostoru je situace v mnohém
jednodušší.

Ale nějak to není ono. Nemůžu si dát kafe a catering, nemůžu si pokecat
s lidma (většinou to nedělám, ale když je řečník excelentní, tak mu to
řeknu), nemůžu poznávat nová místa. Těším se, až budou konference jako dřív
(a ony budou).

Když jsem ale na Twitteru zachytil, že součástí [FOSDEM 2021](//fosdem.org/2021/)
je i [Go Room](//fosdem.org/2021/schedule/track/go/) a že je to zadarmo, tak jsem
se zúčastnil. Mimochodem, FOSDEM znamená _Free and Open source Software Developers'
European Meeting_.

## Deploy a Go HTTP server in your browser

[Nicolas Lepage](//fosdem.org/2021/schedule/speaker/nicolas_lepage/) měl takový
nápad --- nechat běžet Golang webovou aplikaci v browseru. Asi to nemá nějaké
valné využití, kromě znovupoužití už hotového kódu (aplikace).

Fór je v tom, zkompilovat Golang kód jako [WebAssembly](//github.com/golang/go/wiki/WebAssembly)
a výslednou binárku provolávat pomocí [Servicer
Worker](//developers.google.com/web/fundamentals/primers/service-workers/).
Jediným problémem/omezením je, že ve WebAssembly se nedá nastartovat
Golang [http.Server](//golang.org/pkg/net/http/#Server). Ale zato se tam dá
spustit [http.Handler](//golang.org/pkg/net/http/#Handler).
Aby to fungovalo, tak je potřeba v Golangu přeložit JavaScript request
na Golang [http.Request](//golang.org/pkg/net/http/#Request) a pak už to sviští.

Zajímavá blbůstka. Takže hadlery se dají bez problému reusovat. Jenom je otázka,
jak moc je komplexní business logika v hadleru --- ty by měly být spíše
bezstavové.

{{< figure src="/2021/02/Go-WebAssembly.png"
    link="/2021/02/Go-WebAssembly.png" >}}

## Go Without Wires: Programming Bluetooth Using Go and TinyGo

[Ron Evans](//fosdem.org/2021/schedule/speaker/ron_evans/) měl přednášku
o tom, jak používat [Go Bluetooth](//github.com/tinygo-org/bluetooth)
pro připojování k různým zařízením a jak vlastně fungují [Bluetooth
Low Energy](//en.wikipedia.org/wiki/Bluetooth_Low_Energy) zařízení.

Bylo to docela zajímavé, ale bohužel stream měl tak špatné rozlišení
a zpoždění videa, že z přednášky skoro nic nebylo. Nakonec se _Ron_ připojil
ke dronu a zobrazoval live kameru. Pěkné a efektní, ale škoda toho
pokaženého streamu.

## Drones, Virtual Reality and Multiplayer NES Games: The fun you can have with Pion WebRTC!

[Sean DuBois](//fosdem.org/2021/schedule/speaker/sean_dubois/) sice taky trochu
mluvil o dronech, ale jen okrajově --- jeho tématem bylo [WebRTC](//webrtc.org/)
a speciálně jeho Go implementace [Pion](//github.com/pion/awesome-pion).
WebRTC je technologie pro _real-time communication_ (RTC), která může přenášet
audio, video a binární data a to vše pěkně v browseru.

Je to [peer-to-peer](//en.wikipedia.org/wiki/Peer-to-peer) komunikace mezi dvěma
klienty, z nichž ani jeden nemá veřejnou IP adresu. To se děje pomocí [NAT
Traversal](//en.wikipedia.org/wiki/NAT_traversal).

Po vysvětlení jak funguje WebRTC následovalo demo, jak v Golangu (v _Pionu_)
naimplementovat vytvoření spojení, poslání dat (binární a video) a jak
data přijmout. Kód vypdal velmi čistě a [idiomaticky](//en.wikipedia.org/wiki/Idiom_(language_structure)).


## Calling Python from Go In Memory: Using 0 Serialization and ε Memory

[Miki Tebeka](//fosdem.org/2021/schedule/speaker/miki_tebeka/) měl celkem
klasickou přednášku --- jak z "nějakého" jazyka volat [data science](//en.wikipedia.org/wiki/Data_science)
knihovny v Pythonu. V tomto případě byl tím "nějakým" jazykem Golang.
Už jsem pár takových přednášek viděl, takže to nebylo nic moc objevnýho.

Co bylo ale zajímavý, tak informace, že Golang `float64` a [NumPy](//numpy.org/)
`float64` mají v paměti stejnou reprezentaci. A taky že _NumPy_ `array` je
v paměti spojitý, stejně jako Golang [slices](//blog.golang.org/slices-intro).

_Miki_ tedy ukazoval celý postup, jak z Golangu volat _NumPy_ funkce. Podmínkou
byl _CPython_ a v podstatě šlo o kopírovní (spíš přesouvání) polí z paměti
spravované Pythonem do paměti spravované Golangem. To vše bez jakékoliv
serializace. Hodně zajímavé řešení! 👍

{{< figure src="/2021/02/Go-Python.png"
    link="/2021/02/Go-Python.png" >}}

## Writing a Go Client for Photoprism

Tady bohužel vypadla přednáška o použití Go v krypto-analýze, na kterou jsem se
nejvíc těšil. Uznání patří organizátorům, kteří sedli na Twitter a během hodiny
našli náhradu. Kloubouk dolů!

Náhradou byla [Kris Nova](//fosdem.org/2021/schedule/speaker/kris_nova/), která
live odpřednášela jak psala Go klienta pro [PhotoPrism](//photoprism.app/),
včetně integračních testů. Téma nebylo nějak extrémně zajímavé, ale je jasné,
že _Kris_ je hackerka každým coulem.

Asi nejzajímavější bylo, že u integračních testů nesnáší mockupy --- chce co nejvíc
živých aplikací, aby bylo v testu vidět, jak se systém chová. To si musím
zapamatovat!

## Building cross-platform Go GUIs fast using Fyne: Platform agnostic is the future of app development, and Go is the language of choice

[Andrew Williams](//fosdem.org/2021/schedule/speaker/andrew_williams/) měl sice
nudnou, ale zajímavou 🤔 přednášku o [Fyne](//fyne.io/) --- cross-platform
GUI toolkit pro psaní nativních aplikací v Golangu. Kdybych někdy potřeboval
napsat (desktop) GUI v Golangu, tak po _Fyne_ určitě sáhnu.

Proč byla přednáška nudná? Protože přehlídka widgetů moc sexy není.
Takže spíš potencionální využití do budoucna (i když v cloudu to asi
moc nevyužiju).

{{< figure src="/2021/02/Fyne.png"
    link="/2021/02/Fyne.png" >}}

## Go at Tailscale: From writing Go to using Go

Na [Brada Fitzpatricka](//fosdem.org/2021/schedule/speaker/brad_fitzpatrick/)
jsem se těšil --- to je přece ten chlápek, co 10 let psal Go! Zejména
[std](//golang.org/pkg/#stdlib) a [http](//golang.org/pkg/net/http/) packages.
Teď dělá ve start-upu _Tailscale_, kde vyvíjejí VPN nové generace.

Tahle VPNka běží na všech platformách a zajímavější než ona same je to,
jak tuhle multiplatformnost řešili. Na všech platformách je to (téměř) čisté
Go, všechno OpenSource, občas (Windows, Mac) něco trochu proprietárního
(třeba Windows [system tray](//en.wikipedia.org/wiki/Taskbar#Taskbar_elements))
a třeba na Macu volání Golangu ze [Swiftu](//developer.apple.com/swift/).
Na Adroidu třeba zas něco málo [JNI](//en.wikipedia.org/wiki/Java_Native_Interface).
Na serveru pak (výhradně) čisté Go a databáze [etcd](//etcd.io/) (to je taky Go).

Zajímavá pak byla ještě diskuze o IP adresách v Go --- jednak existuje typ
[net.IP](//golang.org/pkg/net/#IP) a pak ještě [net.IPAddr](https://golang.org/pkg/net/#IPAddr).
Ani jeden typ chlapcům z _Tailscale_ nevyhovoval (jsou _mutable_, nejsou
_comparable_ a `IP []byte` se nedá použít jako klíč v mapě, plus ještě pár
další věcí) a tak si napsali vlastní typ [netaddr.IP](//pkg.go.dev/inet.af/netaddr#IP),
který zmíněnými neduhy netrpí.

Jo a pak taky _Tailscale_ používá již výše zmiňovaný [NAT
Traversal](//en.wikipedia.org/wiki/NAT_traversal).

## The State of Go: What's new since Go 1.15

Poslední přednáška byla od pořadatelky [Maartje Eyskens](//fosdem.org/2021/schedule/speaker/maartje_eyskens/)
o novinkách v Go 1.15. Bylo to fajn, holčina věděla o čem mluví, ale musím
se přiznat, že si z toho moc nepamatuju. Jediné, co si vybavím, tak
že nejsou žádné změny v jazyce a že `go build` a `go test` už teď nemodifikují
`go.mod` --- místo toho je potřeba zavolat `go mod tidy`.

Když se teď zpětně dívám, tak přednáška více méně kopírovala obsah
[Go 1.15 Release Notes](//golang.org/doc/go1.15), ale bylo tam i něco
o nadcházejícím _Go 1.16_.

## Suma sumárum

Bylo fajn si poslechnout po čase něco nového o Golangu. Až mě překvapilo,
že ačkoliv měly všechny přednášky pouhých 30 minut, tak se do nich
vměstnalo překvapivé množství informací. Pěkně to odsejpalo a nebyl
čas se nudit.

Poklona patří pořadatelům, protože paralelních streamů [bylo
požehnaně](//fosdem.org/2021/schedule/), ale všechno klapalo
jako dobře promazaný stroj. Well done!

## Související články

* [FOSDEM 2021, Monitoring room](/2021/02/fosdem-2021-monitoring-room/)
