+++
title = "FOSDEM 2021, Monitoring & Observability room"
date = 2021-02-12T14:58:18+01:00
description = """Moje první virtuální konference, dvoudenní FOSDEM.
    Navštívil jsem Golang a Monitoring roomy."""
tags = ["monitoring", "konference"]
hero_image = "hero.jpg"
thumbnail = "2021/02/Fosdem-Monitoring.png"
+++

{{< figure src="/2021/02/Fosdem-Monitoring.png" link="/2021/02/Fosdem-Monitoring.png" >}}

Protože první den na _FOSDEM 2021_ v [Golang room](/2021/02/fosdem-2021-golang-room/)
se mi líbil, podíval jsem se, co ještě zajímavého dávají druhý den. Jednotlivých
roomů [bylo hodně](//fosdem.org/2021/schedule/), ale nic moc mě nezaujalo (že by se
v open source nic zajímavého nedělo? 🤔).
Nicméně protože jsem se nedáno hodně zabýval [Fluentd](//www.fluentd.org/)
a před dvěma lety zase [Prometheem](//prometheus.io/), vybral jsem si
nakonec [Monitoring and Observability room](//fosdem.org/2021/schedule/track/monitoring_and_observability/).
A nebylo to špatný.

## Observability for beginners

[Atibhi Agrawal](//fosdem.org/2021/schedule/speaker/atibhi_agrawal/) byla na stáži
v [Grafana Labs](//grafana.com/), kde --- logicky --- přičuchla k distribuovanému
monitoringu. Její přednáška byla krátká 10minutovka, velmi, velmi lehký úvod
do _observability_ a jak se to liší/doplňuje s _monitoringem_.

Observability zahrnuje: logging, metrics a tracing, které spolu dohromady
vytvářejí kontext, co se v systému děje. Letmo zmínila pár nástrojů, které jsou
de facto standard pro monitorování v cloudu: [Prometheus](//prometheus.io/),
[Fluentd](//www.fluentd.org/), [Jaeger](//www.jaegertracing.io/) a
(samozřejmě) [Grafana](//grafana.com/oss/grafana/).

Dobrá rada (pro začátečníky) byla udělat si side-projekt a nastavit pro něj
metriky, logování a traceování.

## A Google Monitoring System, Monarch… in Open Source?

Přednáška [Barteka Plotka](//fosdem.org/2021/schedule/speaker/bartek_plotka/) a [Bena
Ye](//fosdem.org/2021/schedule/speaker/ben_ye/) byla založena na srovnávání systému
[Monarch](//www.youtube.com/watch?v=LlvJdK1xsl4), globálního monitorování používaného
Googlem, a projektu [Thanos](//thanos.io/), což je CNCF [incubating
project](//www.cncf.io/projects/). Srovnání proto, že _Monarch_ je proprietární
a _Thanos_ je open source. Čili jestli má open source odpověď na dané řešení.

Chlapci postupně procházeli klíčové pasáže z [Monarch white paperu](//www.vldb.org/pvldb/vol13/p3181-adams.pdf)
a srovnávali je s architekturou _Thanosu_. Někde to chtělo trochu představivosti,
ale většinou srovnávání fungovalo. Samozřejmě, je potřeba mít na zřeteli, že
_Monarch_ je battle-proved řešení, kdežto _Thanos_ je rozvíjející se projekt.
Ale vypadá nadějně.

{{< figure src="/2021/02/Monarch-Thanos.png"
    link="/2021/02/Monarch-Thanos.png" >}}

## Getting Started with Grafana Tempo: High Volume Distributed Tracing Built on Top of Object Storage

[Joe Elliott](//fosdem.org/2021/schedule/speaker/joe_elliott/) je zaměstnancem
_Grafana Labs_ a stvořitelem [Grafana Tempo](//grafana.com/oss/tempo/), což je
high-scale distributed tracing system, využívající [OpenTelemetry](//opentelemetry.io/).
Přednáška byla převážně live demo s ukazování spousty Grafana dashboardů
a jelikož nejsou k dispozici slidy, tak si z ní nic napamatuji. Ale pěkné
to bylo.

## PostgreSQL Network Filter for EnvoyProxy

[Fabrízio Mello](//fosdem.org/2021/schedule/speaker/fabrizio_mello/) a
[Alvaro Hernandez](//fosdem.org/2021/schedule/speaker/alvaro_hernandez/)
jsou z firmy, která nabízí support pro [PostgreSQL](//www.postgresql.org/)
a přišli se zajímavou myšlenkou --- proč dělat klasický monitoring _Postgresu_
se všemi agenty a restarty databází, když všechny potřebné informace jsou
už obsaženy ve [Frontend/Backend Protocolu](//www.postgresql.org/docs/current/protocol.html),
kterým komunikuje klient s databází?

Fígl je v tom, dekódovat zmíněnou komunikaci a metriky z ní vytáhnout. Takže
dopsali do [Envoy Proxy](//www.envoyproxy.io/) network filter, který právě
takové dekódování a sbírání metrik dělá. Elegantní řešení!

{{< figure src="/2021/02/Envoy-Postgres-filter.png"
    link="/2021/02/Envoy-Postgres-filter.png" >}}

## Proper Monitoring: Applying development practices to monitoring

[Jason Yee](//fosdem.org/2021/schedule/speaker/jason_yee/) měl lightning talk
ve formě [vlogu](//en.wikipedia.org/wiki/Vlog). Bylo to sice osvěžující a
zábavné, ale taky si z toho po pár dnech už nic napamatuju. Nicméně myšlenka
byla trochu podobná tomu, co řeší [Infrastructure as Code](//en.wikipedia.org/wiki/Infrastructure_as_code)
--- převzít vývojářské techniky a best practices a aplikovat je v nové
oblasti, v tomto případě monitoringu. Například mít _Grafana_ dashboardy
v Gitu, dělat testování dashborardů (že reagují podle očekávání) apod.

## Monitoring MariaDB Server with bpftrace on Linux: Problems and Solutions

[Valerii Kravchuk](//fosdem.org/2021/schedule/speaker/valerii_kravchuk/) měl
hodně low-level přednášku jak na Linuxu monitorovat [MariaDB](//mariadb.org/).
Jakmile začal _Valerij_ mluvit o linux kernelu, tak mě ztratil. 🤷‍♂️
Takže myšlenku přednášky bych shrnul jako:

> Use modern Linux tracing tools while troubleshooting MariaDB server!

{{< figure src="/2021/02/Linux-tracing.png"
    link="/2021/02/Linux-tracing.png" >}}


## Performance Analysis and Troubleshooting Methodologies for Databases

[Peter Zaitsev](//fosdem.org/2021/schedule/speaker/peter_zaitsev/) měl
obecný přehled tří hlavních performance metodologií, které se využívají
v distribuovaných řešeních:

* [The USE Method](//www.brendangregg.com/usemethod.html)
* [The RED Method](//grafana.com/blog/2018/08/02/the-red-method-how-to-instrument-your-services/)
* [The Four Golden Signals](//sre.google/sre-book/monitoring-distributed-systems/)

A ačkoliv to mělo v názvu _pro databáze_, tak to bylo hodně obecný a o databázích
to moc nebylo. Což vůbec nevadilo --- jako přehled (pro začátečníky) to byl fajn.

## Production Machine Learning Monitoring: Outliers, Drift, Explainers & Statistical Performance

Poslední přednášku měl [Alejandro Saucedo](//fosdem.org/2021/schedule/speaker/alejandro_saucedo/)
a moc k ní nemám co napsat (opět chybí jak slidy, tak video), jen že byla
pěkná a zajímavá --- jak monitorovat machine-learning rešení a to jak
při trénování modelů, tak při produkčním využití.

## Suma sumárum

Musím říct, že z roomu _Monitoring and Observability_ jsem měl stejně příjemný
pocit jako ze včerejšího [Golangového](/2021/02/fosdem-2021-golang-room/).
V případě monitoringu se hodně přednášek točilo v ekosystému
[Promethea](//prometheus.io/) a [Grafany](//grafana.com/)
(a vlastně znova i toho _Golangu_) a z nich vycházejících řešení. Budu se muset
na tuhle oblast trochu zaměřit, protože mi tady pořád zbývá pár studijních restů.

## Související články

* [FOSDEM 2021, Golang room](/2021/02/fosdem-2021-golang-room/)
