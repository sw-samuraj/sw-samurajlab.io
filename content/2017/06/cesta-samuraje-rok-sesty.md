+++
title = "Cesta samuraje, rok šestý"
date = 2017-06-18T17:00:00Z
updated = 2017-06-18T17:00:55Z
description = """Blog SoftWare Samuraj má šesté narozeniny. Tradiční retrospektiva."""
tags = ["sw-samuraj"]
aliases = [
    "/2017/06/cesta-samuraje-rok-sesty.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/img/samurai.gif" >}}

Normálně má blog _SoftWare Samuraj_ narozeniny v květnu, ale myslím, že je každému jedno, že (téměř) tradiční retrospektiva přichází o měsíc později. A po dvou letech.

## Wind of Change

Huš je to ták. Po čtyřech letech jsem došel k bodu, kdy je potřeba nejen změnit zaměstnání. Ale trochu i přesměrovat kariéru. A tak chodím poslední dva, tři měsíce po pohovorech. Je to tristní zkušenost, svět se pořád ještě nepoučil. No, myslím, že o tom bude výživný článek --- zatím mám pracovní název: [Smutná zpráva o stavu IT trhu](/2017/09/smutna-zprava-o-stavu-it-trhu/).

Poslední dva roky jsem málo programoval. Téměř vůbec. Asi se to projevilo --- poslední půlrok programuju hodně. V _Pythonu_, v _Clojure_, ve _Scale_, v _Javě_. Je to moc příjemné. A frustrující --- přišel jsem o hodně praxe a potřebuju dohnat hodně znalostí.

Třeba _Java_ 8. Kde kdo už ji rutinně mastí a já se teprve postupně učím, jak ty nové vlastnosti, na které jsem léta čekal, postupně uvádět do praxe. Přitom to není nic světoborného --- lambdy jsou jen převlečené closures z _Groovy_ (syntakticky spíš ze _Scaly_), streamy jsou vlastně jen sekvence z _Clojure_. Takže věci, které jsem v jiných jazycích používal už před pěti lety. Jen v tý _Javě_ mi to nějak neleze z klávesnice. Dost možná je to tím, že ta _Java_ syntaxe je dost ošklivá a užvaněná.

Nebo _Clojure_. Naposled jsem s ním dělal ve verzi 1.2, teď je ve verzi 1.8, téměř 1.9. Tudíž věci jako [protocols](//clojure.org/reference/protocols), [reducers](//clojure.org/reference/reducers), [transducers](//clojure.org/reference/transducers) a [clojure.spec](//clojure.org/about/spec) mě kompletně minuly. O boomu [ClojureScriptu](//clojurescript.org/) nemluvě. (Pro úplnost, protokoly ve verzi 1.2 už byly, ale ne v [knize, podle které jsem studoval](//www.goodreads.com/review/show/748211304).)

Anebo _Scala_. Z ošklivého akademického kačátka se vyprofiloval cool jazyk, který si vydobyl svou úzkou a specifickou doménu v oblasti big data a paralelního zpracování. Tady se musím přiznat k určité kariérní chybě. Když jsem se před více než 8 lety rozhodoval, jaký další jazyk se naučím, volil jsem mezi _Groovy_ a _Scalou_. Vybral jsem _Groovy_ a z dnešního pohledu je jasné, že to byla slepá ulička.

Zatímco _Groovy_ je dnes taková zestárlá Popelka, skriptovací holka pro všechno a jedinou rozumnou a rostoucí implementací je skvělý [Gradle](//gradle.org/); tak _Scala_ vyrostla do síly, kdy i na českém trhu jsou vám ochotni za ni zaplatit jako za hlavní jazyk. To se o jiném JVM jazyku říct nedá.

## Minulost

Zmiňoval jsem, co mi uteklo. Co mi naopak minulé roky přinesly? Hlavně věci týkající se team leadingu. Postavil jsem si svůj vlastní team. Získal slušnou praxi v technické hiringu. Naučil jsem se dělat distribuované projekty. Naučil jsem se řídit distribuovaný tým (další potencionální článek).

Zlepšil jsem si mluvenou angličtinu a svoji španělštinu jsem dotáhnul na pracovní úroveň (jak říká LinkedIn, full working proficiency) --- dělal jsem projekt, kde španělština byla oficiální jazyk. Všechny workshopy, specifikace a dokumentace byly ve španělštině. _Trabajé como líder técnico, responsable de la implementación de la solución compleja en América Latina._

Mám za sebou dvě neúspešné implementace _Kanbanu_. Vina bude na mé straně, protože jsem nedokázal inspirovat team.

Mám za sebou úspěšné zavedení [Hg Flow](//bitbucket.org/yujiewu/hgflow/overview) na několika projektech.

Mám za sebou neúspěšné budování teamové kultury. Opakovaně.

Vychoval jsem tři dobré technical leadery.

Nepodařilo se mi posunout tři juniory na seniornější úroveň.

Nalítal jsem kolem 230 000 km po čtyřech kontinentech.

## Budoucnost

Chtěl bych si od team leadování odpočinout --- aspoň rok se věnovat jen full time programování. Chtěl bych si odpočinout i od technických pohovorů --- cítím rutinu a potřebu změny.

Chtěl bych změnit business doménu. Vypadnout z korporátního prostředí, ve kterém se pohybuji posledních 12 let (celou svou _Java_ kariéru). Hodně mě teď zajímá oblast _machine learningu_ a _data science_. Zatím jsem na úrovni čtení článků a okukování kurzů na [Coursera](//www.coursera.org/).

Technicky, rád bych se odklonil od _Javy_. Ideálně, zůstal na JVM a dělal v _Clojure_, nebo aspoň ve _Scale_. Možná i něco jinýho --- _Erlang_ by byl fajn. Aspoň půl, na půl.

Chtěl bych potkat inspirativní lidi.

## Související články

* [Cesta samuraje, rok čtvrtý](/2015/05/cesta-samuraje-rok-ctvrty/)
* [Cesta samuraje, rok třetí](/2014/06/cesta-samuraje-rok-treti/)
* [Cesta samuraje, rok druhý](/2013/05/cesta-samuraje-rok-druhy/)
* [Cesta samuraje, rok první](/2012/04/cesta-samuraje-rok-prvni/)
