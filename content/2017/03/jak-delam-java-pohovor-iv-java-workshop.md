+++
title = "Jak dělám Java pohovor IV: Java workshop"
date = 2017-03-10T21:07:00Z
updated = 2017-03-10T21:09:12Z
description ="""SoftWare Samuraj odtajnil svoje know-how: jak dělat
    na technickém pohovoru Java workshop. Vhodné i pro jiné jazyky
    a technologie."""
tags = ["teamleading", "interview", "java"]
aliases = [
    "/2017/03/jak-delam-java-pohovor-iv-java-workshop.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/img/logos/Java.png" >}}

Zcela bezkonkurenčně nejčtenějším zápisem na mém blogu je opus magnum [Jak dělám Java pohovor](/2012/11/jak-delam-java-pohovor). Jeho čtenost je řádově vyšší, než u zbytku veškerých textů. Ten článek už je skoro pět let starý a neodpovídá (mojí) realitě.

Věci, které za to stojí, se snažím vylepšovat, takže je v důsledku dělám jinak. A pokud máte to štěstí, že si můžete vybírat lidi k sobě do týmu, tak by vám na tom mělo záležet. Hodně. A tak si ty poslední tři, čtyři roky říkám, že bych měl svůj zápis zaktualizovat. Jak tedy dělám pohovor dnes?

## Agenda Java interview

Agenda pohovoru zůstala hodně podobná:

1. Představím se já.
1. Prostor pro otázky kandidáta a/nebo představení společnosti, business domény, našeho oddělení a projektů.
1. Diskuze nad kandidátovým projektem.
1. Review kandidátova kódu.
1. Java workshop

Drobnou změnou je bod číslo 3, _diskuze nad kandidátovým projektem_. Velkou a stěžejní změnou je pak titulní _Java workshop_.

## Diskuze nad kandidátovým projektem

Dříve jsem v tomto bodě probíral kandidátovo CV, ale postupně se to vyvinulo v diskuzi nad konkrétní zkušeností. Přijde mi to zajímavější a přínosnější, než diskuze nad "suchou historií".

(Mimochodem, kandidáti mají neuvěřitelně zažitou historizaci svojí praxe. I když jim opakovaně explicitně zdůrazníte, že nechcete slyšet chronologicky odříkanou pracovní zkušenost, většinou vás naprosto ignorují a spustí naučený kolovrátek. Hanba vám HR a head-hunteři!)

V dnešní době se tedy ptám na projektovou zkušenost ve stylu:

_"Mohl byste mi popsat Váš poslední, nebo poslední úspěšný projekt? Zajímají mě hlavně technické aspekty --- mohl byste mi popsat a ideálně namalovat architekturu, či design vašeho vybraného projektu? Dále by mě zajímalo, jak byl projekt řízen, jaké role se na něm podílely a jaká byla Vaše role? Co třeba nějaké disciplíny SW inženýrství? Release management? Source code management? Issue tracking?"_

Většinou se mi podaří kandidáta (přátelsky) přimět k namalování nějakého schématu na whiteboard. To má dva aspekty. Jednak se nad vizualizovanou architekturou/designem dobře diskutuje. A jednak poznáte, jak je na tom kandidát ohledně schopnosti vysvětlit technické věci, znalost modelování, uvědomění si "big picture", kontextu daného projektu a spoustu dalších věcí.

{{< figure src="/2017/03/Whiteboard-architecture.jpg"
    link="/2017/03/Whiteboard-architecture.jpg"
    caption="Ilustrační příklad whiteborard architektury" >}}

## Java workshop

Když jsem pozměněnou podobu interview vymýšlel, chtěl jsem něco, co bude co nejblíž způsobu, jakým bych chtěl, abysme v týmu pracovali. Tedy: komunikovali spolu a uměli diskutovat nad kódem a designem. A samozřejmě... psali kód, ideálně s unit testy.

Začínám tím, že si s kandidátem sedneme vedle sebe a předložím mu --- podle vybraného příkladu --- následující obrázek (pro nadcházející odstavce, považujme za dané zadání [návrhový vzor Observer](//en.wikipedia.org/wiki/Observer_pattern)):

{{< figure src="/2017/03/Observer-pattern.png"
    caption="Class diagram návrhového vzoru Observer" >}}

Zeptám se kandidáta, jestli zná [UML](//en.wikipedia.org/wiki/Unified_Modeling_Language) a daný návrhový vzor. Pokud ano, tak ať jedno, druhé, či oboje vysvětlí. Pokud ne, řeknu nevadí a obojí vysvětlím já. Tahle část trvá krátce, většinou tak do pěti minut a jejím hlavním smyslem je porozumět zadání. A teď přijde to hlavní.

Otevřu notebook a... ukážu kandidátovi "skeleton" projektu natažený v IDE. Vypadá to nějak takhle:

{{< figure src="/2017/03/Eclipse-Observer.png"
    caption="Skeleton Java projektu v IDE" >}}

Jak je vidět na obrázku, i na UML diagramu výše, v projektu jsou dvě rozhraní a dvě implementační třídy. Ty konkrétní třídy "implementují" metody z interfaců, ale jsou prázdné. Čili, jde to zkompilovat, ale nic to nedělá.

{{< figure src="/2017/03/Eclipse-Subject-code.png"
    caption="Rozhraní a jeho prázdná implementace" >}}

Malá vsuvka, pokud vás to napadlo --- v tom IDE to mám připravený v [Eclipse](//www.eclipse.org/ide/) a v [IntelliJ IDEA](//www.jetbrains.com/idea/). Dřív jsem to měl připravený i pro [NetBeans](//netbeans.org/), ale pak jsem to přestal udržovat --- za ty čtyři roky mi na pohovor nepřišel nikdo, kdo by chtěl v NetBeansech dělat.

Zpátky k projektu. Kromě produkčního kódu jsou tam připravený --- opět prázdný --- třídy pro testy. Pokud by vás zajímalo, jak přesně ten startovní projekt v IDE vypadá, podívejte se na můj repositář na Bitbucketu: [https://bitbucket.org/sw-samuraj/hiring/overview](//bitbucket.org/sw-samuraj/hiring/overview)

Vysvětlím kandidátovi strukturu projektu a řeknu něco jako:

_"Takže, tady máme prázdný projekt (pro návrhový vzor Observer) a zkusíme si ho naimplementovat. Pokud znáte, nebo jste dělal nějakou reálnou implementaci tohoto vzoru, můžete zkusit udělat ji. Anebo můžeme zkusit naimplementovat ten vzor jako takový, dejme tomu ukázkový příklad._

_Záleží jen na Vás s čím začnete, jestli s implementací, nebo s testy a já jsem tady proto, abych Vám pomohl v případě problému. A v průběhu toho, jak budete psát, se Vás budu ptát na některé aspekty Vašeho kódu._

_V projektu můžete cokoliv změnit, je zde jen jedno pravidlo --- nesmíte nijak upravovat interfacy."_

A je to. Kandidát začne psát, já se dívám, občas se na něco zeptám. Když se nám to zadrhne, jemně kandidáta navnadím, nebo popostrčím. Občas něco do kódu napíšu i já sám.

Každý kandidát je naprosto jiný. Někdo to celé vystřihne za půl hodiny i s pěknými unit testy. Někdo za stejnou dobu stačí vyrobit jednu metodu, která přidává prvky do kolekce. V průměru strávím touhle částí zhruba 40-50 minut.

A je to. Interview krátce zakončím a rozejdeme se.

## Jak workshop vyhodnotit?

V momentě, kdy workshop skončí, už mám většinou jasno, jestli chci mít kandidáta v týmu, nebo ne. Někdy jsem na pochybách a je lepší se na to vyspat, nebo prodiskutovat s někým nezúčastněným.

Jinak vás asi zklamu --- neprozradím vám žádné tajemství, jak z té cca hodiny programování udělat výsledek. Pro mne to byla dlouhá cesta, během které jsem mířil k jakémusi ideálu. Proto nepotřebuju nějaký checklist, abych věděl, jestli tam --- s kandidátem --- jsme, nebo ne.

Pokud vám minulý odstavec nedává smysl, nebo mu nerozumíte a přesto byste chtěli vědět, jak worshop vyhodnotit, zkuste se inspirovat těmihle aspekty:

* Jak kandidát komunikuje? Během vysvětlování zadání a během programování.
* Jak efektivně pracuje v IDE? (Na oblíbené IDE se ptám ještě před pohovorem, takže by to neměl být stresující faktor, že programuju v něčem nezvyklém.)
* Jak hezky/čitelně/čistě/efektivně píše kód?
* Jak designuje/strukturuje metody, třídy, testy?
* Jak píše testy? Jestli vůbec. A kdy? Předtím, potom, zároveň?
* Zajímají vás nějaké seniornější aspekty? Klidně se můžete ponořit do věcí, jako je specifická implementace některého Java API, nebo třeba do Reflection API (fakt, vyplyne to úplně přirozeně).
* Chcete si ověřit znalost konkrétní verze Javy? 6, 7, 8? Žádný problém, jde to.
* Jak kandidát komunikačně a znalostně reagoval na navržené alternativy?

Určitě vás napadne něco dalšího.

## Co už nedělám

S odkazem na [původní článek](/2012/11/jak-delam-java-pohovor), bych vypíchnul věci, od kterých jsem upustil. V první řadě, už se nevrtám v CV. Ani před pohovorem, ani během. Životopis si většinou jen rychle proběhnu před [phone screenem](/2015/06/jak-delam-java-pohovor-iii-phone-screen), jestli mě tam něco nezaujme --- třeba jestli má kandidát zkušenost s (business) analýzou, architekturou, team leadingem apod. Prostě nějaký přesah za vývojařinu.

A potom, už nedávám hlavolam. I když tahle část bývala celkem zábavná, tak jsem ji odstranil --- když jsem před těmi čtyřmi lety nově definoval, jak bych chtěl pohovor pojmout, zaměřil jsem se na to, aby to bylo co nejbližší denní realitě. A tam prostě vývojáři mechanické hlavolamy neřeší. (Čest výjimkám.)

## Jak dál?

Pohovor tímto způsobem dělám poslední čtyři roky a musím přiznat, že po té době je to pro mne už příliš velká rutina. Chce to změnu. Zatím čekám na inspiraci. Je možné, že tenhle článek čtete v přelomovém období --- takže jestli se někdy potkáme spolu na pohovoru, dost možná, že bude vypadat jinak.

## No dobře, Java. Ale co .Net?

Z minulých dvou let mám zajímavou zkušenost. Poprvé v životě mě potkalo štěstí, že jsem se stal mentorem. Jako tím opravdovým, kdy se vztah mentor-[mentee](//cs.wikipedia.org/wiki/Mentee) "samovolně", organicky vytvoří. Pikantní na tom je, že můj mentee není Javista... je to .Neťák!

No. Kromě jiného jsme spolu řešili také .Net pohovory. Z dnešního pohledu to vlastně nebylo nic těžkého --- vzali jsme můj Java pohovor, vytvořili .Net workshop a bylo to.

Podstatné je, že to výborně fungovalo a dnes tak máme úspěšný nový .Net tým. Zkušenosti zkrátka občas přijdou ze strany, odkub byste to nečekali.

## Předešlé díly

* [Jak dělám Java pohovor](/2012/11/jak-delam-java-pohovor)
* [Jak dělám Java pohovor II: proč nedávám testy?](/2014/07/jak-delam-java-pohovor-ii-proc-nedavam-testy)
* [Jak dělám Java pohovor III: phone screen](/2015/06/jak-delam-java-pohovor-iii-phone-screen)

## Související články

* [Jak se nabírají Javisti na Filipínách](/2014/01/jak-se-nabiraji-javisti-na-filipinach)
* [Jak dělají Java pohovor jinde](/2013/02/jak-delaji-java-pohovor-jinde)
* [Měl by mít vývojář portfolio?](/2013/06/mel-by-mit-vyvojar-portfolio)
* [Kontrakt místo pohovoru, je to reálné?](/2013/08/kontrakt-misto-pohovoru-je-to-realne)
* [Geek, který zapadne](/2013/04/geek-ktery-zapadne)
