+++
title = "Catalanova čísla a syntax highlighting"
date = 2017-03-18T18:25:00Z
updated = 2017-05-01T21:12:23Z
description = """Jak naimplementovat Catalanova čísla v Clojure? Buď
    pomocí kombinačních čísel, anebo stylovou rekurzí. GitHub projekt
    included."""
tags = ["clojure"]
blogimport = true 
[author]
	name = "Vít Kotačka"
	uri = "https://www.blogger.com/profile/12147991495120816879"
+++

{{< figure class="floatleft" src="/img/logos/Clojure.png" width="180" >}}

Je to téměř 5 let, co jsem naposled napsal něco na tento blog. Ale doufám, že se to teď změní --- moje láska ke [Clojure](//clojure.org/) opět propukla s neztenčenou silou a tak snad ponese nějaké užitečné ovoce.

Chvilku ale potrvá, než uvedu blog do použitelného stavu, takže začnu takovým zahřívacím tématem. Tedy, dvěma tématy --- sice to není úplně fér, ale vrazil jsem do titulku dva nesouvisející motivy.

## Syntax highlighting 3.0

Když jsem s Clojure před šesti lety začínal, bylo ve [verzi 1.2](//clojure.blogspot.com/2010/08/clojure-12-release.html) a jeho podpora v různých nástrojích byla nízká, nebo žádná. Jednu z věcí, které jsem tehdy řešil, bylo jak zobrazit na blogu [syntax highlighting](//en.wikipedia.org/wiki/Syntax_highlighting).

Moje první volba padla na tehdy všudypřítomný [SyntaxHighlighter](//github.com/syntaxhighlighter/syntaxhighlighter). Musel jsem použít jakýsi externí Clojure brush, protože SyntaxHighlighter dodnes [nemá nativní podporu Clojure](//github.com/syntaxhighlighter/syntaxhighlighter/wiki/Brushes-and-Themes).

S výsledkem jsem nebyl spokojen, takže jsem po čase zvolil nové řešení --- jako letitý uživatel famózního editoru [Vim](//www.vim.org/), jsem se rozhodl pro trochu ruční práce a zaangažoval vimovskou funkci [:TOhtml](//vim.wikia.com/wiki/Pasting_code_with_syntax_coloring_in_emails). S tou jsem maximálně spokojen a používám ji i na svém druhém blogu [SoftWare Samuraj](//www.sw-samuraj.cz/). Přesto jsem se rozhodl potřetí pro změnu syntax highlightingu.

Ten třetí důvod má konsekvence s oživením blogu. Aniž bych si tady vylíval srdíčko, jeden z důsledků mých aktuálních rozhodnutí je, vybudovat si na [svém GitHubu](//github.com/sw-samuraj) jakési Clojure portfolio (už jsem [o tom kdysi psal](/2013/06/mel-by-mit-vyvojar-portfolio)).

Volba GitHubu pro mne nebyla úplně přímočará --- nejsem "git guy" a pokud můžu, preferuji [Mercurial](//www.mercurial-scm.org/). Pravdou ale je, že veškerá Clojure komunita je GitHubu, tak proč se tomu vzpírat. No, a když GitHub, tak [Gist](//gist.github.com/), to dá rozum.

## Catalanova čísla

V rámci svého Clojure-zmrtvýchvstání jsem procházel své staré Clojure kódy a narazil jsem na jeden, který se mi líbil --- funkce pro výpočet [Catalanových čísel](//en.wikipedia.org/wiki/Catalan_number). Pikantní je, že si po těch pěti letech nepamatuju, proč jsem to napsal, nicméně funkce se mi líbily natolik, že jsem se rozhodl se o ně --- v rámci zahřátí na provozní teplotu --- podělit.

Catalanova čísla jsou sekvencí přirozených čísel, která má zajímavé využití v [kombinatorice](//en.wikipedia.org/wiki/Catalan_number#Applications_in_combinatorics) --- [binární stromy](//en.wikipedia.org/wiki/Binary_tree), [průchod mřížkou](//en.wikipedia.org/wiki/Lattice_path)(?), [rozdělení polygonu na trojúhelníky](//en.wikipedia.org/wiki/Polygon_triangulation) ad. Sekvence je definována následujícím vztahem:

{{< figure src="/2017/03/Catalan-numbers.gif" >}}

Pokud, tak jako já, nejste úplně kovaní v matematice, tak chviličku googlování potrvá, než zjistíte, že pro implementaci tohoto vzorečku budete potřebovat _kombinační číslo_ ([binomial coefficient](//en.wikipedia.org/wiki/Binomial_coefficient)) a tím pádem [faktoriál](//en.wikipedia.org/wiki/Factorial). Pak už je jednoduché poskládat tyto základní funkce do sebe, jako matrojšku:

{{< gist sw-samuraj 5d2a0b922e40dc16f75035a789db8e97 >}}

Když už jsem se v tom tak vrtal, říkal jsem si: nedá se to udělat nějak jednodušeji, efektněji, víc "Clojure"? A dá. Jen je potřeba vyjít z jiného vzorečku, který definuje Catalanova čísla rekurzivně:

{{< figure src="/2017/03/Catalan-numbers-open.png" >}}

Při použití posledního vzorečku nám tak vyjde pěkná [Clojure rekurze](//clojure.org/about/functional_programming#_recursive_looping). Klíčová slova jsou speciální operátory [loop](//clojure.org/reference/special_forms#loop) a [recur](//clojure.org/reference/special_forms#recur):

{{< gist sw-samuraj 41a0e46e05298c3a941c98531ac81648 >}}

## GitHub project

Pokud si chcete pohrát s Catalanovými čísly trochu víc, anebo dostat malý bonus navíc, podívejte se na můj projekt na GitHubu:

* [sw-samuraj/blog-catalan](//github.com/sw-samuraj/blog-catalan)

Navíc dostanete [Leiningen](//github.com/technomancy/leiningen) projekt, testy v [Midje](//github.com/marick/Midje) a [lazy sekvenci](//clojure-doc.org/articles/language/laziness.html) Catalanových čísel.

## Související články

* [Konečně kód!](/2011/01/konecne-kod)
* [Změna syntax highlightingu a konvence kódu](/2012/03/zmena-syntax-highlightingu-a-konvence-kodu/)
