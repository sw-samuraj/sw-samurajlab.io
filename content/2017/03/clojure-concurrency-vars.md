+++
title = "Clojure concurrency: Vars"
date = 2017-03-25T19:14:00Z
updated = 2017-05-01T21:11:04Z
description = """Významnou vlastností Clojure jsou neměnitelné datové
    struktury. Vyvstává otázka: jak s neměnitelnými daty pracovat?
    Částečnou odpovědí na to jsou Vars - základní stavební kámen
    pokročilejších konceptů."""
tags = ["clojure", "concurrency"]
blogimport = true 
[author]
	name = "Vít Kotačka"
	uri = "https://www.blogger.com/profile/12147991495120816879"
+++

{{< figure class="floatleft" src="/2017/03/Meerkat.jpg" >}}

Významnou vlastností _Clojure_ jsou [neměnitelné datové struktury](//clojure.org/about/functional_programming#_immutable_data_structures). Je to taková dvojsečná vlastnost (i když benefity výrazně převažují). Na jednu stranu to vývoj zjednodušuje, protože se nemusíme bát, že se nám data změní pod rukama.

Nadruhou stranu vyvstává otázka, jak s neměnitelnými daty pracovat --- nic v našem světě není neměnné (a všechno jednou pomine). Jak tedy _Clojure_ řeší změnu stavu a s tím související konkurenci?

K dispozici jsou čtyři mechanizmy, jak měnit stav:

* [Vars](//clojure.org/reference/vars) jako globální úložiště, pro eventuální per-thread změny.
* [Atoms](//clojure.org/reference/atoms) pro synchronní, nekoordinované změny.
* [Refs](//clojure.org/reference/refs) pro synchronní, v [transakci](//en.wikipedia.org/wiki/Software_transactional_memory) koordinované změny.
* [Agents](//clojure.org/reference/agents) pro asynchronní, nekoordinované změny.

Postupně bych se chtěl podívat na všechny způsoby, ale dnes začneme tím nejjednodušším, čemu se žádný začátečník v _Clojure_ nevyhne.

## Vars

[Var](//clojure.org/reference/vars) je způsob, jakým _Clojure_ odkazuje na úložiště. Toto úložiště obsahuje "standardní", neměnná data. _Var_, ale můžeme dočasně přesměrovat na jiné úložiště. Podstatnou vlastností je, že toto dočasné přesměrování --- [binding](//clojure.github.io/clojure/clojure.core-api.html#clojure.core/binding) --- je vidět jenom v rámci aktuálního vlákna, ostatní vlákna vidí pořád původní hodnotu.

### Statické Vars

Klasické _Var_ se vytvoří speciálním formem [def](//clojure.github.io/clojure/clojure.core-api.html#clojure.core/def): `(def x 42)`. Takto definováno, je _Var_ statické --- pokud bychom ho chtěli svázat s jiným úložištěm, vyhodí _Clojure_ výjimku [IllegalStateException](//docs.oracle.com/javase/8/docs/api/java/lang/IllegalStateException.html).

{{< gist sw-samuraj 1cca7c6e808bf2ad507f06fa5c3b05c1 >}}

### Dynamické Vars

Pokud chceme, aby _Var_ ukazovalo na jiné úložiště, musíme ho explicitně definovat jako dynamické pomocí instrukce `^:dynamic`. Samotné přesměrování se provádí makrem [binding](//clojure.github.io/clojure/clojure.core-api.html#clojure.core/binding). Po dobu trvání bloku `binding` směřuje _Var_ na nové úložiště, aby se po jeho skončení vrátilo ke své původní hodnotě.

[Dle konvence](//github.com/bbatsov/clojure-style-guide#earmuffs-for-dynamic-vars) se dynamické _Vars_ uzavírají do `*earmuffs*`. Označují se tak věci, určené pro re-binding.

{{< gist sw-samuraj 774c71d072e56e36d6c235d5ef832ec0 >}}

### Vars jsou globální

_Vars_ jsou globální --- viditelné pro všechny thready. Pokud tedy vytvoříme _Var_ v jiném vlákně, je dosažitelné také ze všech ostatních vláken.

V následujícím příkladu vytvoříme _Var_ v jiném vlákně pomocí makra [future](//clojure.github.io/clojure/clojure.core-api.html#clojure.core/future):

{{< gist sw-samuraj 5779aa15507e8defaaef863d0a4e962e >}}

### Namespace 

V rámci [namespace](//clojure.org/reference/namespaces), v němž byly vytvořeny, jsou _Vars_ přístupné svým názvem. Pokud se přepneme do jiného namespacu, je potřeba na _Var_ odkazovat plně kvalifikovaným názvem, nebo ho "naimportovat" z původního namespace pomocí funkce [refer](//clojure.github.io/clojure/clojure.core-api.html#clojure.core/refer).

{{< gist sw-samuraj e42f3643290cb3408781f8fddb058a47 >}}

## Funkce jsou také Vars

Ve _Vars_ se neukládají jenom data, ale také funkce. Všechno výše napsané tedy platí pro funkce úplně stejně, včetně toho, že je můžeme dynamicky re-bindovat.

To může být zajímavý mechanismus, jak v runtimu dynamicky měnit chování funkcí. Nicméně, užívejte s mírou --- jazyk to umožňuje, ale je to výjmečné řešení.

{{< gist sw-samuraj 1e3e8f95e7daf0d5d245a73717c707f0 >}}

## A kde je ta konkurence?

Se samotnými _Vars_ si moc konkurentního programování neužijeme. Je to ale základní stavební kámen, od kterého se odvíjejí ostatní způsoby. Konec konců, všechny v úvodu zmíněné mechanizmy --- [Atomy](//clojure.org/reference/atoms), [Refs](//clojure.org/reference/refs) a [Agenti](//clojure.org/reference/agents) --- jsou ve výsledku uloženy ve _Vars_.

## GitHub projekt

_Clojure concurrency_ chci pojmout jako mini-seriál (viz Související články níže), který bude podložený projektem na GitHubu, kam budou postupně přibývat jednotlivé příklady:

* [sw-samuraj/blog-concurrency](//github.com/sw-samuraj/blog-concurrency)

## Související články

* TBD: Clojure concurrency: Atoms
* TBD: Clojure concurrency: Refs
* TBD: Clojure concurrency: Agents

## Starší související články

* [Jak měnit neměnitelné. Refs](/2011/07/jak-menit-nemenitelne-refs)
