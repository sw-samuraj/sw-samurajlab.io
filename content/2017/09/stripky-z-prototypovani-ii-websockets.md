+++
title = "Střípky z prototypování II: WebSockets"
date = 2017-09-19T20:00:00Z
updated = 2017-09-20T10:51:49Z
description = """Jak přinutit Wicket, aby se choval víc reaktivně? Stačí
    zapojit WebSockets. Není to úplně triviální, ale dá se to zprovoznit."""
tags = ["wicket", "sw-engineering", "java", "web-sockets"]
aliases = [
    "/2017/09/stripky-z-prototypovani-ii-websockets.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2017/09/Shards-II.jpg"
    link="/2017/09/Shards-II.jpg" width="200" >}}

V [úvodním díle](/2017/08/stripky-z-prototypovani-wicket-spring.html) jsme se dívali na jednoduchý prototyp --- jak spojit dvě etablované webové technologie: [Wicket](//wicket.apache.org/) a [Spring](//projects.spring.io/spring-framework/). Bylo to takové zahřívací kolo, ještě o nic nešlo. Spíše o to, připravit si prototypovací platformu, než vyřešit zapeklitý technický problém.

V dnešním díle se podíváme na prototyp, jehož negativním výsledkem mohla být výměna GUI technologie, což by vedlo k refaktoringu cca 1/3 aplikace.

## Kontext

> πάντα χωρεῖ καὶ οὐδὲν μένει ~ Ἡράκλειτος

Jak říká [Hérakleitos z Efesu](//cs.wikipedia.org/wiki/H%C3%A9rakleitos): "všechno se mění a nic nezůstává stejné". To se tak stane, že zákazníkovi prodáte určité řešení. Všude --- v odpovědi na [RFQ](//en.wikipedia.org/wiki/Request_for_quotation), v kontraktu, na workshopech se zákazníkem --- prezentujete, že GUI určité aplikace bude _"wizard-like"_.

Vyberete technologie, nastřelíte aplikační prototyp a začnete vyvíjet business [features](//en.wikipedia.org/wiki/Software_feature). Vývojáři studují a postupně si osvojují nové technologie. Svět je krásný...

A pak přijde [UX designer](//en.wikipedia.org/wiki/User_experience_design) a hodí vám do toho vidle. Řekne, že uživatelé milují skrolování a cool, že jsou [Single Page Aplications](//en.wikipedia.org/wiki/Single-page_application) (SPA). A vy si uvědomíte, že GUI technologie, kterou jste zodpovědně vybrali (možná i nějaká bezesná noc tam byla), se se změnou paradigmatu není schopná vyrovnat.

## Use Case

Cíl tohoto prototypu byl přímočarý: zpropagovat data, která přijdou na server z _bezstavové_ RESTové služby do prohlížeče konkrétního uživatele a překreslit určitou část obrazovky, aby se tato data zobrazila. S nadsázkou jsem tomu říkal: přidat _"reactive-like"_ chování.

Podmínkou samozřejmě bylo, aby zůstaly zachovány stávající technologie (tedy Spring a Wicket). Pro úplnost dodám, že Wicketovské komponenty jsou vždy _stavové_.

{{< figure src="/2017/09/Reactive-like-sequence.png" >}}

## Implementace

Implementaci jde rozdělit do dvou kroků:

1. Zpracování RESTového volání a propagaci dat do Wicket komponenty na serveru.
1. Push dat ze serveru do konkrétního prohlížeče.

### Observable Cache

První bod můžeme realizovat pomocí [Observer patternu](//en.wikipedia.org/wiki/Observer_pattern) --- do řešení přidáme další element, který zatím budeme nazývat _observable cache_ (a více si o něm povíme v příštím díle). _Observable cache_ nám bude fungovat jako synchronizační mechanizsmus:

1. Wicket komponent se zarigistruje jako _observer_ do _observable cache_.
1. REST kontroler vloží data do _observable cache_.
1. _Observable cache_ notifikuje zaregistrované _observery_ (wicket komponenty).

{{< figure src="/2017/09/Observable-cache.png" >}}

Jelikož _observable cache_ je pro nás zatím abstraktní komponent, jehož technologie/implementace bude vybrána později (a navíc pro nás momentálně není podstatná), vytvoříme si ji pro začátek jako jednoduchou [observable](//docs.oracle.com/javase/8/docs/api/java/util/Observable.html) HashMapu. Pro začátek budeme chtít notifikace pro metody `put` a `remove`.

{{< gist sw-samuraj c02d9967a46dfcddd909f9ab1cb8d8b5 >}}

Následně zaregistrujeme Wicket komponentu ([Panel](//ci.apache.org/projects/wicket/apidocs/org/apache/wicket/markup/html/panel/Panel.html)) jako observera. Potřebná WebSocket logika půjde do metody [update()](//docs.oracle.com/javase/8/docs/api/java/util/Observer.html#update-java.util.Observable-java.lang.Object-). Prozatím ji necháme prázdnou, než probereme, jak se Wicket staví k WebSocketům.

{{< gist sw-samuraj beae01c78ef7b9bc1c308b0884612761 >}}

### Wicket a WebSockety

Je to taková matrjoška. Existuje [WebSocket specifikace](//tools.ietf.org/html/rfc6455). Ta je implementována [Java API for WebSocket](//jcp.org/en/jsr/detail?id=356) (JSR 356). A Java API je pak obaleno Wicktovskou implementací/rozšířením. Wicketovská dokumentace je popsaná v referenční příručce v kapitole [Native WebSockets](//ci.apache.org/projects/wicket/guide/8.x/single.html#_native_websockets). Některá témata zde chybí, ale je to dobrý začátek.

To, co Wicket k WebSocketům přidává a co také využijeme pro náš případ (a co také chybí v dokumentaci), je "broadcastování" WebSocket zpráv. V základě to umožňuje poslat [WebSocket událost](//ci.apache.org/projects/wicket/apidocs/8.x/org/apache/wicket/protocol/ws/api/message/IWebSocketPushMessage.html) všem komponentům, které mají definované [WebSocketBehavior](//ci.apache.org/projects/wicket/apidocs/8.x/org/apache/wicket/protocol/ws/api/WebSocketBehavior.html). Komponent pak může přijatou událost dále filtrovat a rozhodnout se, jestli na ni reagovat.

To, že náše aplikace bude WebSocket-ready, nám zajistí náhrada klasického WicketFiltru za
[JavaxWebSocketFilter](//ci.apache.org/projects/wicket/apidocs/org/apache/wicket/protocol/ws/javax/JavaxWebSocketFilter.html):

{{< gist sw-samuraj 477258ac1ea732f4a95f531614538407 >}}

Tady trochu odbočím od WebSocketů k servletům. Aby WebSockety fungovaly, musí je podporovat servlet kontejner, ve kterém aplikace poběží (všechny moderní kontejnery by to měly umět).

V rámci popisovaných prototypů probíhá deployment jako součást buildu, do embedovaného servlet kontejneru, který nám poskytuje výborný Gradle plugin [Gretty](//github.com/akhikhl/gretty) (k dispozici jsou Jetty a Tomcat). Bohužel, našel jsem tady pravděpodobně bug --- WebSockety nefungují v embedovaném Jetty, takže je potřeba používat embedovaný Tomcat. (Ve standalone Jetty funguje všechno jak má, takže to bude problém Gretty.)

Zpátky k WebSocketům a Wicketu. Nyní, po notifikaci z _observable cache_, chceme z metody `update` broadcastovat WebSocketEvent a opět ji odchytit ve Wicket panelu, který budeme chtít překreslit. Data pro model komponentu si vytáhneme přímo z _cache_.

{{< figure src="/2017/09/Wicket-WebSocket-broadcasting.png" >}}

Pokud se podíváme na tento proces z hlediska kódu, potřebujeme ve Wicket komponentu aktualizovat pár věcí:

1. V konstruktoru přidat na komponentu [WebSocketBehavior](//ci.apache.org/projects/wicket/apidocs/8.x/org/apache/wicket/protocol/ws/api/WebSocketBehavior.html).
1. Z metody `update` broadcastovat [IWebSocketPushMessage](//ci.apache.org/projects/wicket/apidocs/8.x/org/apache/wicket/protocol/ws/api/message/IWebSocketPushMessage.html).
1. V metodě `onEvent` zprávu přefiltrovat.
1. Nechat komponent (nebo jeho část) překreslit.
1. V rámci překreslení dojde ke znovu-načtení modelu.

{{< gist sw-samuraj 43d082488280f4a0130b67ac4f0b75a6 >}}

## Prototype repozitory

Pokud si budete chtít prototyp spustit a trochu si s ním pohrát, naklonujte si následující Bitbucket repository. Součástí prototypu je _SoapUI_ projekt, s připraveným REST requestem, kterým si můžete poslat data do prohlížeče.

* [blog-wicket-spring-rest](//bitbucket.org/sw-samuraj/blog-wicket-spring-rest)

Klíčové třídy:


* [WicketAppFilter](//bitbucket.org/sw-samuraj/blog-wicket-spring-rest/src/tip/src/main/java/cz/swsamuraj/wicketspring/WicketApplication.javahttps://bitbucket.org/sw-samuraj/blog-wicket-spring-rest/src/tip/src/main/java/cz/swsamuraj/wicketspring/WicketAppFilter.java) (JavaxWebSocketFilter)
* [ObservableCache](//bitbucket.org/sw-samuraj/blog-wicket-spring-rest/src/tip/src/main/java/cz/swsamuraj/wicketspring/ObservableCache.java) (observable cache)
* [PersonPanel](//bitbucket.org/sw-samuraj/blog-wicket-spring-rest/src/tip/src/main/java/cz/swsamuraj/wicketspring/PersonPanel.java) (WebSocketBehavior, WebSocket broadcasting)

## Poučení

Občas se ukáže, že řešení, které jsme odkládali jako nejzažší možné, je nakonec to správné. Nebo jediné funkční. Je potřeba zvážit potenciální důsledky --- např. přepisování GUI vrstvy versus pozdější perfomance problémy (kolik WebSocket spojení bude v produkci otevřených? Jaký bude objem broadcastovaných zpráv? apod.)

Zvolili jsme na počátku dostatečně flexibilní technologii, aby uspokojila i nároky v úvodu zmiňovaného Hérakleita z Efesu? (Eh, chtěl jsem říci šíleného UX designera.) Malý, rychlý prototyp může dát na tyto otázky odpověď. Nebo aspoň vyznačit cestu, kudy ne.

## Příště

V pokračování _střípků_ se podíváme na to, čím nahradit _observable cache_. Můžete se těšit na sebevražedný deathmatch [Neo4j](//neo4j.com/) vs. [Infinispan](//infinispan.org/).

## Repository všech prototypů

* [blog-rest-contract-first](//bitbucket.org/sw-samuraj/blog-rest-contract-first)
* [blog-wicket-spring-rest](//bitbucket.org/sw-samuraj/blog-wicket-spring-rest)
* [blog-embedded-neo4j](//bitbucket.org/sw-samuraj/blog-embedded-neo4j)
* [blog-embedded-infinispan](//bitbucket.org/sw-samuraj/blog-embedded-infinispan)

## Související články

* [Střípky z prototypování: Wicket, Spring, REST](/2017/08/stripky-z-prototypovani-wicket-spring.html)
* Střípky z prototypování III: Neo4j, Infinispan (TBD)
* [REST contract-first: Swagger &amp; Gradle](/2017/05/rest-contract-first-swagger-gradle.html)
