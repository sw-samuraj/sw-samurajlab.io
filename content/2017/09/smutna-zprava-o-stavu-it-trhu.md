+++
title = "Smutná zpráva o stavu IT trhu"
date = 2017-09-16T12:19:00Z
updated = 2017-12-18T09:47:46Z
description = """Strávil jsem teď posledních pět měsíců hledáním nové
    práce. Nebylo to radostné období, bylo to tristní. Aneb reflexe
    technických pohovorů z pohledu SoftWarového Samuraje."""
tags = ["interview"]
aliases = [
    "/2017/09/smutna-zprava-o-stavu-it-trhu.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2017/09/Scream-face.png"
    link="/2017/09/Scream-face.png" width="200" >}}

## Aneb jak dělají technické pohovory jinde

Strávil jsem teď posledních pět měsíců hledáním nové práce. Nebylo to radostné období, bylo to tristní. Celkový dojem by se dal shrnout do jedné věty:

**V oblasti IT jsme jen levná montovna aut.**

Zaměstnavatelé (IT firmy) nehledají ani kreativní lidi, ani _problem solvery_, ani autonomní vývojáře. Nehledají lidi, kteří se konstantně učí a zlepšují. Ne. Místo toho hledají někoho, komu jeden bývalý kolega říkal láskyplně _makáči_. Občas jim někdo méně něžně říká _lopaty_. A já to řeknu natvrdo:

**Většina firem u nás hledá programátory, které potřebují posadit rovnou k soustruhu, či k pásové výrobě.**

A když to trochu víc zgeneralizuji, hledají lidi s _fixním mindsetem_. Zapomeňte na to, že byste se ještě někdy mohli něco naučit. Je potřeba, abyste sekali šroubky, jeden jako druhý. Jediným doporučením je, že jste navlas stejné šroubky dělali v minulosti (která skončila včera).

## Silná slova, nebo jak to teda je?

Tak, bulvární úvod už máme za sebou, pojďme se na to podívat trochu objektivněji. V rámci možností. V tom, co jsem napsal v úvodu, se můžu těžce mýlit. Je to jenom odraz toho, jak interpretuji technická kola pohovorů, který jsem absolvoval. Hodně to reflektuje rozpor mezi tím, co firmy (a jejich techničtí lidé) říkají a jak reálně konají na pohovorech.

No a samozřejmě, ať to zazní hned na začátku --- mám spoustu chyb a nejsem rytíř v blyštivé zbroji. Možná že celou situaci špatně čtu a prostě nedosahuji "standardních" nároků, které dnes IT trh vyžaduje. Možná mi jenom ujel vlak. Jako introvert, si to myslím polovinu času.

Moje reflexe je silně poznamenaná zpětnou vazbou, které se mi dostalo. Je po léta smutným faktem českého trhu, že v 80 % dostanete jako jediné vyjádření kultovní větu:

> _"Pro další jednání jsme upřednostnili uchazeče, kteří přesněji odpovídali aktuálním požadavkům pozice."_

Nic proti rozhodnutí. Jen vám to tak nějak nepomůže, že jo? (A jsme zase u toho fixního mindsetu.)

## Rozsah a forma

Dále neuvádím všechny pohovory, které jsem absolvoval, jen ty, kde proběhlo aspoň nějaké technické kolo (v libovolné formě). Uvádím vždy:

* Anonymizovaný typ firmy a název pozice.
* Krátký popis technického kola.
* A pak asi to nejcenější --- reflexi.
* Formální, či neformální zpětná vazba od pohovorující strany.
* Černobílé hodnocení [mindsetu](//www.goodreads.com/book/show/5494321-mindset): fixed/growth.

## Upozornění

Tento článek je převažně negativní. Proto bych zdůraznil, že je primárně o _procesu_, ne o lidech. Pokud se někde vyjadřuju rozporuplně o pohovorujících, jde o názorovou neshodu a nevypovídá to nic o jejich charakteru, nebo schopnostech.

## Start-up inkubátor, System Engineer

### Technický pohovor

Technický pohovor měl formu videocallu, cca 1-1,5 hodiny a povídali jsme si o tématech [Computer Science](//en.wikipedia.org/wiki/Computer_science), [Software Engineering](//en.wikipedia.org/wiki/Software_engineering), [Distributed Systems](//en.wikipedia.org/wiki/Distributed_computing) a [Functional Programming](//en.wikipedia.org/wiki/Functional_programming). Ptali se mě mmj. na věci jako:<br />

* Agoritmy, speciálně [Big O notation](//en.wikipedia.org/wiki/Big_O_notation) a [Tree traversal](//en.wikipedia.org/wiki/Tree_traversal)
* [Monads](//en.wikipedia.org/wiki/Monad_(functional_programming)), [Referential transparency](//en.wikipedia.org/wiki/Referential_transparency), [Covariance/Contravariance](//en.wikipedia.org/wiki/Covariance_and_contravariance_(computer_science))
* [CAP Theorem](//en.wikipedia.org/wiki/CAP_theorem), [Partition tolerance](//en.wikipedia.org/wiki/Network_partition)

Potenciální další kolo bylo vědomostně ve stejném rozsahu, ale on-site, cca na 1/2 dne a "víc do hloubky".

### Reflexe

Byl to můj první "start-up" pohovor v životě a tak mě dost zaskočilo tunelové vidění pohovorujících. Dvě třetiny otázek jsem nebyl schopný zodpovědět, protože:

* jsem je v praxi nikdy nepotkal,
* brali jsme je před 12 lety na škole a od té doby jsem je nikdy ani prakticky, ani teoreticky nepoužil,
* nastudoval jsem si je před časem po večerech čistě z hobby-zájmu a nebyl je schopný z hlavy plynule přednést/vysvětlit (konkrétní příklad: před 5 lety jsem si nastudoval Hadoop, na blogu jsem se dostal jenom k [zápisku o HDFS](/2012/04/hadoop-lehky-uvod-do-hdfs.html)).

Z pohovoru i z následného (podmínečného) pozvání do dalšího kola jsem měl neodbytný dojem semestrální zkoušky --- nastudovat, dostat zápočet, zapomenout. Když jsem se ptal, zda určité věci, na které se ptají, opravdu používají v praxi, otevřeně řekli, že ne, ale "tím si u pohovoru prošli všichni", tak by nebylo fér to v interview nemít i nadále.

Mým silným pocitem bylo, že si se mnou neví rady --- jejich core business a technologie jsem neznal, zároveň "cítili", že "něco" vím (takže to nebylo na přímé zamítnutí), ale nevěděli, jak se k "tomu" dostat, jak se zaptat.

Přímým důsledkem tohoto pohovoru jsou dva články na blogu ([CAP Theorem](/2017/04/cap-theorem.html) a [Covariance &amp; Contravariance](/2017/05/covariance-contravariance.html)) a téměř absolvovaný kurz [Functional Programming Principles in Scala](//www.coursera.org/learn/progfun1). Jaké bylo mé překvapení, když jsem zjistil že pohovorové otázky z funkcionálního programování kopírují témata z kurzu. Nic proti Martinu Oderskému --- jeho kurz je výborný. Ale Scala je jen podmnožinou FP a některé věci nedávají v jiném jazyce smysl (třeba covariance/contravariance v Clojure).

### Zpětná vazba

Musím ocenit následnou, celkem obšírnou zpětnou vazbu, kde byly v mailu rozebrány jednotlivé oblasti, s doporučením, na co bych se měl zaměřit, pokud bych chtěl pokračovat do dalšího kola.

### Mindset

Fixed ⚔

## Machine Learning start-up, Software Developer

### Technický pohovor

Tady to nebyl úplně klasický přijímací proces --- sešli jsme se opakovaně, na několik neformálních rozhovorů, kdy jsme se vzájemně přestavovali a poznávali. Nejvíce technické pak bylo řešení architektonicko-integračního zadání na white-board a firemní představení dvou projektů, hlavně z technického pohledu, kde jsem se mohl ptát spoustu technických otázek (předpokládám, že z toho se dá leccos poznat).

### Reflexe

Velice příjemná forma pohovoru, postavená na vzájemném respektu (to není úplně samozřejmé). Musím ocenit přizpůsobení technické části mé aktuální roli a senioritě (celkem výjimečné).

### Zpětná vazba

Vzhledem k neformálnosti a otevřené formě rozhovorů jsem měl pocit jakési "instantní" zpětné vazby.

### Mindset

Growth ✔

## Marketing eCommerce start-up, Functional Developer

### Technický pohovor

Technický pohovor se skládal z rozhovoru o technických řešení z mé praxe a z implementace [cons funkce](//en.wikipedia.org/wiki/Cons)... na papír.

### Reflexe

Můj pohovorující byl evidentně velmi seniorní funkcionální programátor. Bohužel, jsme nenašli společnou platformu pro diskuzi o technických řešení. Pokud popisujete integraci přes RESTové služby na úrovni API a jste neustále přerušováni dotazy co přesně v ten moment chodí v hlavičkách na úrovni TCP protokolu a ani po 30 minutách se nedoberete bodu, kdy si sladíte terminologii; máte problém --- pokud nejste schopni se domluvit na pohovoru, těžko to budete zvládat později v práci.

V programování na papír jsem extrémně slabý. Re-implementaci nízhoúrovňových funkcí považuji za ztrátu času (pokud zrovna na škole nestudujete předmět Algoritmy).

### Zpětná vazba

Zpětná vazba byla, že pro core business firmy nejsem vhodný, ale "kdybych nebyl tak drahý", tak bych se mohl věnovat vedleší činnosti --- přesýpání dat, klidně v Clojure, kde by to nebyl takový problém. Doufám, že se mi podařilo zachovat poker-face.

### Mindset

Fixed ⚔

## Český internetový hegemon, Developer of Advanced Systems

### Technický pohovor

Prvně jsem online absolvoval Java test na [Codility](//codility.com/). Šlo o dvě úlohy, které bylo potřeba vyřešit a odevzdat během 90 minut. První úloha byla naimplementovat algoritmus na prokládání řetězců, něco jako: 12 + 98 = 1928. V druhé úloze jsem měl vymyslet(!) a naimplementovat algoritmus na počet kroků v geometrické spirále.

Pak jsem byl pozván on-site na pohovor s HR paní, v jehož závěru jsem psal na dedikovaném počítači kombinovaný Java-Python-C++ test, 75 otázek během 45 minut (nevím, jestli si správně pamatuji ten čas, každopádně to byla míň než minuta na otázku). Třetina otázek byla z Javy, třetina z Pythonu, třetina z C++.

### Reflexe

Nemám slov. **Tak špatně udělané technické kolo jsem během své kariéry ještě nezažil.** Jsou to takové ty absurdity života, kdy přemýšlíte, jestli nejde o nějakou skrytou kameru. A když za váma zaklapnou dveře, máte potřebu se hystericky smát, abyste neztratili pojítko s vesmírem, který jste doposud obývali, než jste vsoupili do budovy.

Docela by mě zajímaly myšlenkové pochody člověka, který takovou frašku "designoval". Ještě bych tak akceptoval _Codility_ jako jakýsi filtr pro ne-programátory. Byť vymýšlení algoritmu s tikajícíma hodinama v pozadí patří spíš do ranku "konkurz na [McGyvera](//en.wikipedia.org/wiki/MacGyver#Premise)".

On-site test pak rezignuje na jakoukoliv konvenční racionalitu:

* 2/3 testu nemají relaci jak ke kandidátově minulosti (v Pythonu jsem nikdy komerčně neprogramoval, v C++ ani jako hobby), tak potencionální budoucnosti (šlo o čistou Java pozici). Asi množstevní sleva, nebo snaha ušetřit.
* Cca 40 sekund na otázku je asi z důvodů, aby kandidát nepřemýšlel, případně mechanicky tahal z trouby rozpečené polotovary.
* Ad absurdum dotažené obskurnosti syntaxe daného jazyka ve stylu "kompilátor v hlavě intoxikované LSD".
* Java `array` je "first class citizen", zapomeňte na cokoliv, co přišlo po [Java 1.1](//en.wikipedia.org/wiki/Java_version_history#J2SE_1.1). Třeba znalosti Javy 8 nejlíp otestujete tak, že se tváříte, že kolekce nikdy neexistovaly.

### Zpětná vazba

Nesmím opomenout zdůraznit: nulová zpětná vazba:

> _"Pro další jednání jsme upřednostnili uchazeče, kteří přesněji odpovídali aktuálním požadavkům pozice."_

### Mindset

Extremely Fixed ⚔

## Big Data start-up, Erlang Developer

### Technický pohovor

Prvním krokem bylo programovací zadání, s deklarovanou náročností cca 1/2 dne, které mělo být odevzdáno do 24 hodin. Zadání bylo velmi vágní. K vypracovanému zadání přišly připomínky, které jsem měl zhruba do týdne doimplementovat a znovu odeslat. Odevzdání probíhalo mailem.

Následovalo on-site kolo, kdy jsem postupně mluvil s dvojicí sysadminů/operations, dvojicí architektů a dvojicí vývojářů (Java a Erlang).

Se sysadminy jsme si povídali hlavně o unixu a jak hledat problémy v produkčním prostředí. S architekty jsme si u white-boardu popovídali o jednom z mých projektů.

S Java vývojářem jsme si (tuším) povídali o vypracovaném zadání. Bohužel si víc nepamatuji, protože to všechno přebyl jeden detail (viz reflexe). S Erlang vývojářem to byla klasika --- přinesl si papír a tužku. Už tušíte, že jo? Naimplementovali jsme si funkci na parsování regulárního výrazu.

### Reflexe

Programovací zadání bylo celkem zajímavý --- naimplementovat RESTovou službu s určitou business logikou. V čem jsme se podstatně rozcházeli, byla časová náročnost --- z mého pochopení zadání by toto zvládnul naprogramovat za půl dne jenom superman. Já jsem na zadání, i s dopracováním, strávil 2 MD čistého času a i tak významně prioritizoval, co jen nastínit a co udělat v rozumné kvalitě.

Co mě dost otrávilo, bylo dopracování zadání --- doimplementovat persistenci. Jakože cože?!? To je zadání pro studenty průmyslovky?!? Kdybych to měl řešit na pohovoru já, tak si o tom tváří v tvář popovídáme a za 5 minut se není o čem bavit. Aby to pro mne nebyla ztráta času, aspoň jsem si vyzkoušel verzování databáze přes [Flyway](//flywaydb.org/) (zůstalo nepovšimnuto).

Rozhovory se sysadminy a architekty byly celkem očekávatelné a nemám, co bych vytknul, či vyzvedl. S programátory to byla jiná. Začněme Javou. Napsal jsem v zadání kus takovýhleho kódu:

{{< gist sw-samuraj a4d15b3b37c2d0129e57aed1517a69da >}}

Když pominu design (brilantní kód zkrátka na první dobrou nepíšu), obsahuje tenhle kód jeden podstatný problém --- iteruje se přes mapu, místo aby se k ní přistupovalo přes klíč. Pohovorující kolega mne na tento detail upozornil.

* _"Ano"_, říkám, _"to je chyba"._
* _"Byl jste ve stresu, když jste to psal?"_, ptá se.
* _"To bych ani neřekl, spíš jsem některým věcem dával nižší prioritu --- postatný pro mne byl fungující, buildovatelný a spustitelný projekt."_, vysvětluji.
* _"No dobře, ale z mapy se hodnoty vytahují přes klíče, to je přece základ."_, vrací se k tématu.
* _"Ano"_, souhlasím, _"byla to chyba."_

Takhle to ještě chvilku pokračovalo, až se z toho stal nejsilnější pocit z interview --- já jsem ten pro kterého je normální iterovat přes mapu, místo použití klíče. Je smutné, když se dva seniorní vývojáři baví o takové trivialitě tak dlouho a navíc spolu od počátku souhlasí. Ale možná, že mnou spáchaný hřích přehlušil veškerou další komunikaci.

No budiž, je to pohovor na Erlang programátora, nechme Javu odpočívat v pokoji. Takže: v praxi, stejně jako na pohovoru, je důležité:

* Programovat na papír.
* Re-implementovat esenciální core funkce jazyka (regex match)
* Předpokládat, že funkcionální termíny jsou obecně platné, bez ohledu na kontext (přece když řeknu _list_, tak je jasné, že myslím spojitý seznam z [cons](//en.wikipedia.org/wiki/Cons) buněk --- žádné jiné typy seznamů přece neexistují).

Myslím, že o funkcionálních programátorech možná napíšu satirický článek.

### Zpětná vazba

Hodně zklamaný jsem byl ze zpětné vazby --- do pohovorů jsem investoval 2,5 MD _čistého_ času (zadání + pohovory) a výsledkem byly dvě věty:

> _"Ačkoliv kolegové hodnotili setkání s Vámi jako zajímavé, bohužel do finálního kola budeme zvát jiného kandidáta. Dle kolegů se zcela nepotkávájí Vaše dosavadní znalosti s technickou profilací naší role a nebyli bychom schopni plně využít Vaše zkušenosti."_

Vezměte si denní sazbu seniorního vývojáře za 2,5 MD a na druhou misku vah položte informaci, že jste "zajímavý". Neocenitelné, což?

### Mindset

Fixed ⚔

## Data Management produkt, Java Developer

### Technický pohovor

Byl jsem požádán o zaslání odkazů na své public repository. Poslal jsem link na Bitbucket a GitHub a vypíchnul jsem dva hobby projekty (oba v Groovy).

Následovalo on-site technické kolo, kde dva seniorní vývojáři převáženě zodpovídali mé otázky o své práci. Překvapivě jsem se o jejich technickém řešení dozvěděl méně, než z předešlého rozhovoru s [CTO](//en.wikipedia.org/wiki/Chief_technology_officer), který mi nakreslil a vysvětlil blokové schéma. Vývojáři jen povídali a asi předpokládali, že už všechno vím.

Na prodiskutování mého kódu nezbyl čas, vybavuji si jen jednu povšechnou otázku na Groovy/Gradle. Na závěr byla půl hodina live-programování v Javě, implementace algoritmu procháze bludiště... v _NetBeans_ IDE.

### Reflexe

Že jsme neprobírali můj kód, považuji za chybu. Já chci před pohovorem kandidátův kód nejen vidět, ale hlavně ho s ním prodiskutovat. Jak [říká Linus](//lkml.org/lkml/2000/8/25/132): _"Talk is cheap. Show me your code."_ Ta diskuze je důležitá --- pokud si z něčeho uděláte představu, aniž byste znali kontext, dojdete pravděpodobně k mylným závěrům.

Závěrečné live-programování považuji za druhou nejhorší zkušenost z popisovaných technických kol. Jako jediné pozitivní hodnotím, že jsem požadovaný algoritmus nemusel vymýšlet, ale byl mi vysvětlen.

* Programování probíhalo v _NetBeans_ IDE, které nejen že nepoužívám já, _ale ani nikdo v inkriminované společnosti!_ Na otázku: _"Jak se v NetBeans dělá XYZ?"_, vám odpoví: _"Nevím."_
* Před-připravený "projekt" v IDE, byla jediná Java třída, zabírající vertikálně asi 5 obrazovek.
* Top třída obsahovala několik vnitřních tříd (i doménových). Trochu mi to připomnělo Javu před 10 a více lety, ještě než se rozmohly best-practices a Spring.
* Top třída vesele kombinovala static a non-static membery, kteří se vzájemně provalávali.
* Před-implementovaná část nahodile kombinovala _procedurální_ a _objektový_ přístup.
* Projekt neobsahoval žádnou přípravu pro unit testy, byť mi bylo řečeno, že pokud chci, můžu si testy napsat. Ano, pokud mám na zadání 30 minut, budu v IDE, který neznám rozcházet jak spustit unit testy (co třeba závislosti?).
* Pohovorující seděl celou dobu naproti mně a neviděl, co píšu. Prostě čekal, až skončím.

Přiznám se, na tomhle cvičení jsem pohořel. Ne že bych nic nenapsal, ale byl jsem trochu paralyzován. Asi na mě byl žalostný pohled, protože jsem byl požádán, abych napsal metody `equals` a `toString`. Nevěřícně jsem tak učinil.

Na závěr jsem byl požádán o zhodnocení. Přiznal jsem porážku a snažil se být diplomatický ve vysvětlení. Zakončil jsem tím, že daný kód, ten před-připravený projekt, by chtěl těžce zrefaktorovat.

Teď už být diplomatický být nemusím: Myslím, že je ostuda takový špatný kód ukázat kandidátovi na pohovoru. Je to první kód, který kandidát od firmy vidí. Pokud tato nemá problém ukázat něco takového, jako svou vizitku --- rozumněj, něco čím můžu kandidáta nalákat --- jak pak asi vypadají její produkční kódy?

Možná, že ve skuktečnosti píšou interně nesrovnatelně lepší kód. Potom ale hodně zanedbávají pohovory. Říkám si, jaké kandidáty asi chtějí tímto způsobem nabrat?

A ještě jedna poznámka --- opět jedna z těch firem, kde musíte umět Java `array` jak když bičem mrská. Že od Java 5 používáte výhradně [collections framework](//en.wikipedia.org/wiki/Java_collections_framework) a v polích už nejste tak "fluent" jako kdysi, není argument.

### Zpětná vazba

Zpětná vazba bylo dvojí. Jednak, hned po interview, mne zaskočila informace, že mne pohovorující kolegové _"nepovažují za programátora"_. Budiž, názor je potřeba respektovat.

Následně jsem pak mailem dostal vyrozumění, které považujuza ilustrační příklad fixního mindsetu:<br />

> _"Vaše zkušenosti a schopnosti jsou velice seniorní, bohužel však Vám nemůžeme nabídnout odpovídající pracovní pozici, kde byste tyto schopnosti uplatnil. To co děláte opravdu velmi dobře a na co se ve své práci zaměřujete, bohužel neodpovídá tomu, na co bychom se aktuálně v &lt;jméno firmy&gt; zaměřovali a není to v plánu ani v dohledné době, nebyli bychom tedy schopni Vám nabídnout odpovídající výzvy a začlenit Vás do některého z týmů."_

Holt, s čím se jednou narodíš, s tím si musíš vystačit až do smrti. Proces _učení se_ je iluze.

### Mindset

Fixed ⚔

## Velká (nejen) Java korporace, Big Data Engineer

### Technický pohovor

Prvním kolem bylo zadání, které se skládalo ze tří částí:

* Programování RESTové služby s určitou business logikou.
* Doménový design jednoduché/zjednodušené business domény.
* Návrh solution pro zpracování nekonéčného streamu.

Na vypracování zadání jsem měl zhruba týden (minimálně 1. část), až dva. Řešení jsem komitoval do soukromé repository na GitHubu.

Druhé kolo bylo on-site, kde jsem se po dvojicích sešel s implementačním týmem: senior + standard(?) vývojáři, senior sysadmin + standard(?) vývojář.

S první dvojicí jsme si povídali o programovacích jazycích a pak jsem na white-boardu:

* Vysvětlil řešení, na "které jsem pyšný" (granularita webových služeb v SOA řešení).
* Navrhnul monitorování distribuované platformy.
* Implementoval v Javě `reverse` Stringu, bez použití [StringBuilder.reverse()](//docs.oracle.com/javase/8/docs/api/java/lang/StringBuilder.html#reverse--). 

S druhou dvojicí jsem odpovídal na "linuxové" otázky a pak jsme si hlavně povídali, jak vypadá práce u nich.

### Reflexe

Programovací zadání bylo celkem zajímavý a docela jsem si ho užil. Protože bylo velmi volné (to je rozdíl oproti vágní), tak jsem se rozšoupnul a půlku jsem naimplementoval ve Scale a půlku v Javě (že by potenciální článek?). Designové řešení doménového modelu bylo přímočaré, nic zvláštního.

Poslední část, streamové řešení, je pro mne exotické. Strávil jsem cca týden pročítáním internetu, abych se zorientoval a pak od stolu navrhl řešení v Clojure a jako zálohu [Spark Streaming](//spark.apache.org/streaming/), s poznámkou nutného ověření (o Sparku nic nevím).

On-site práci na white-boardu hodnotím částečně pozitivně (popis mého řešení a monitorování), částečně negativně (programování a re-implementace core funkce). Programovat se má na počítači.

Jelikož většina pohovorujících měla z nějakého důvodu alespoň částečně načtený můj blog, cítil jsem během on-site pohovorů trochu přehnaný respekt, jenž nebyl adekvátní.

### Zpětná vazba

První, krátkou zpětnou vazbu jsem dostal emailem po komitu prvních dvou částí zadání. Zbývající, opět krátkou, na začátku on-site interview.

Týden po on-site pohovoru jsem dostal další telefonický feedback. Řekl bych, že strukturovaný a vyvážený. Minimálně přesně pojmenoval mé slabší stránky.

### Mindset

Growth ✔

## Jak to zlepšit?

Mé hledání nové práce je momentálně u konce a doufám, že to minimálně dalších 5 let nebudu muset řešit. Shodou okolností a preferencí, se v blízké budoucnosti nebudu technickým hiringem zabývat. Jelikož ale pohory byly historicky vždy [silným tématem](/tags/interview) tohoto blogu, měl bych aspoň doporučení (či zbožné přání):

## Patero technického recruitera

### 1. Udělejte technické kolo co nejvíce praktické. 

Nechte kandidáta vypracovat zadání. Udělejte s ním on-site workshop (jde to i přes Skype). Mějte zadání dostatečně volné, abyste kandidáta zbytečně netlačili do své škatulky. **Nenuťte kandidáta programovat na white-board a už vůbec ne na papír!** Neprogramujete děrné štítky.

### 2. Nechte si ukázat a vysvětlit kandidátův kód.

Uvidíte reálný kód, který vzniknul v určitém kontextu. Zadání, či workshop (předchozí bod) je pořád v laboratorních podmínkách, které nemusí každému sednout.

### 3. Testujte kandidáta na praktické věci, které reálně děláte.

Vykašlete se na re-implementaci algoritmů a core funkcí jazyka --- nic podstatného pro váš business tím o kandidátovi nezjistíte. Naopak tím odfiltrujete slušné procento lidí, kteří excelují v něčem jiném. A zapomeňte na idiotské úlohy typu "kolik golfových míčků se vejde do letadla".

### 4. Nezkoumejte současný snapshot, hledejte projekci do budoucna.

Pokud něco neroste, je to pravděpodobně mrtvé. Současné kvality kandidáta jsou jen startovní bod. Snažte se najít jeho budoucí trajektorii.

### 5. Hledejte lidi s historií a schopností učit se.

Cílem nemusí být najít dokonalý technologický "match". Ale najít někoho flexibilního, kdo se potřebné věci rychle naučí, či do dané role doroste.

> _"You don’t hire for skills, you hire for attitude. You can always teach skills."_ ~ Simon Sinek

## Související články

* [Jak dělají Java pohovor jinde](/2013/02/jak-delaji-java-pohovor-jinde.html)
* [Kontrakt místo pohovoru, je to reálné?](/2013/08/kontrakt-misto-pohovoru-je-to-realne.html)
* [Měl by mít vývojář portfolio?](/2013/06/mel-by-mit-vyvojar-portfolio.html)
* [Jak dělám Java pohovor II: proč nedávám testy?](/2014/07/jak-delam-java-pohovor-ii-proc-nedavam.html)

## Související externí články

* [Pohovory](//calavera.info/v3/blog/2016/03/24/pohovory.html) (calavera.info)
* [Zpráva o stavu IT trhu](//blog.zvestov.cz/software%20development/2016/10/17/zprava-o-stavu-it-trhu) (Banter bloguje)
* [Mladý kolektiv](//honzajavorek.cz/blog/mlady-kolektiv) (Javorové lístky)
* [Prečo startupy nemajú šancu získať seniorných vývojárov](//content.fczbkk.com/preco-startupy-nemaju-sancu-ziskat-seniornych-vyvojarov/) (Riki Fridrich)
