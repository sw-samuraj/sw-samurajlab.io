+++
title = "Kniha Clojure Applied"
date = 2017-09-20T20:00:00Z
updated = 2017-09-21T09:04:43Z
description = """Kniha Clojure Applied je občas na internetu doporučovaná jako
    "druhá" kniha o Clojure, kterou byste si měli přečíst. Osobně bych řekl,
    že spíš než "druhou", bych ji doporučil jako "třetí"."""
tags = ["clojure", "knihy"]
blogimport = true 
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://www.blogger.com/profile/12147991495120816879"
+++

{{< figure class="floatleft" src="/2017/09/Clojure-Applied.jpg"
    link="/2017/09/Clojure-Applied.jpg" width="200" >}}

Kniha [Clojure Applied](//pragprog.com/book/vmclojeco/clojure-applied) je občas na internetu doporučovaná jako "druhá" kniha o Clojure, kterou byste si měli přečíst. Možná jsem se tím nechal po svém návratu ke Clojure podvědomě ovlivnit a tak po "refresh" knize od Carin Meier (o které jsem [psal na jaře](/2017/04/kniha-living-clojure)) jsem sáhnul po _Clojure Applied_. A musím říct, že spíš než "druhou", bych ji doporučil jako "třetí".

## Proč by to měla být 2. Clojure kniha?

Důvody, proč je _Clojure Applied_ doporučovaná jako druhá kniha jsou zřejmé. První kniha nového Clojure adepta bude pravděpodobně nějaký úvod do jazyka. Za sebe bych mohl doporučit tituly [Programming Clojure](//pragprog.com/book/shcloj3/programming-clojure-third-edition) (3rd edition), [Living Clojure](//shop.oreilly.com/product/0636920034292.do) a [The Joy of Clojure](//www.manning.com/books/the-joy-of-clojure-second-edition) (2nd edition). A špatná asi nebude jakákoliv jiná, která pokrývá minimálně verzi **1.6+**.

Co v těchto úvodních knihách chybí, je praktický aspekt Clojure:

* Jak v Clojure designovat rozsáhlejší produkční aplikaci.
* Jak strukturovat Clojure projekt.
* Jak vytvářet znovupoužitelné a dobře definované komponenty.
* Jak z těchto komponent stavět komplexnější řešení a jak komponenty provázat.
* Datové formáty pro výměnu dat (obligátní JSON, ale hlavně nativní Clojure formát [EDN](//github.com/edn-format/edn)).
* Konfigurace prostředí.
* Jak aplikaci/knihovnu distribuovat (tohle je obecné, ne Clojure-specific).

Tyhle témata kniha pokrývá dobře a nováčkovi v Clojure může hodně pomoci.

## Proč by to měla být 3. Clojure kniha?

Tohle bude asi dost subjektivní, ale osobně mi některá témata z knihy přišly docela náročná --- necítil jsem se na ně ještě připravený. Clojure nedělám na fulltime, takže víc praxe by určitě pomohlo. Občas mi přišlo, že ten skok začátečník-expert je příliš velký a začátečník (po 1. knize) nedoskočí a zůstane "pod útesem".

Nálož je třeba už hned první kapitola, probírající doménový model, která byla hodně hutná. Zamíchání [multimethod](//clojure.org/reference/multimethods) a [protokolů](//clojure.org/reference/protocols) do modelování entit mi nepřišlo úplně šťastné a téma spíš zamlžilo.

Podobně nevhodné mi přišlo uvedení externí knihovny pro validaci [Prismatic Schema](//github.com/plumatic/schema) hned v úvodní kapitole. To by mi sedělo spíš v kapitole o testování. Navíc bych v úvodu ocenil na validaci něco víc "vanilla-Clojure".

Podobný, jen silnější dojem jsem měl ze sekce o [Transducers](//clojure.org/reference/transducers) a [pipelines](//clojuredocs.org/clojure.core.async/pipeline). Asi je dobře, že to bylo pohromadě v kapitole nazvané _Use Your Cores_. Ale musím se přiznat, že tady mne autoři ztratili a budu se muset k _transducers_ a [core.async](//github.com/clojure/core.async) ještě (opakovaně) vrátit.

## Měla by to být 2½. Clojure kniha

Celkově mi kniha přišla nevyvážená, co se týče seniority témat. Některá témata byla vyloženě začátečnická --- kolekce, sekvence, běžná concurrency ([atoms](//clojure.org/reference/atoms), [refs](//clojure.org/reference/refs), [agents](//clojure.org/reference/agents)), základní testování ([clojure.test](//clojure.github.io/clojure/clojure.test-api.html)), či zakládání projektu na GitHubu. Čekání s těmito tématy až do 3. knihy je tak vlastně činí zbytečnými, jsou příliš esenciální.

Co bych tedy doporučil? Vybrat si jako druhou nějakou Clojure doménovou knihu. Zajímá vás _web_? Zkuste [Web Development with Clojure](//pragprog.com/book/dswdcloj2/web-development-with-clojure-second-edition) (2nd edition). Zajímá vás _machine learning_? Zkuste [Clojure for Machine Learning](//www.packtpub.com/big-data-and-business-intelligence/clojure-machine-learning). Zajímá vás _data science_? Zkuste [Clojure for Data Science](//www.packtpub.com/big-data-and-business-intelligence/clojure-data-science).

A k tomu můžete po částech přikusovat _Clojure Applied_.

## Co by mělo být v druhé edici?

Knihy stárnou. Technické knihy zastarávají ultra-rychle. _Clojure Applied_ není výjimkou. Co by v potenciální druhé edici této knihy nemělo chybět?

Především velké téma posledního roku a nadcházející verze Clojure **1.9**: [clojure.spec](//clojure.org/about/spec). Nevím, jestli se nějaké knihy o _clojure.spec_ dočkáme, ale druhá verze _Clojure Applied_ by byla vhodným místem pro toto téma (se simultánní náhradou kapitoly o [test.check](//github.com/clojure/test.check)).

_clojure.spec_ by mi taky sedlo jako dobrá náhrada za již zmiňovanou knihovnu _Prismatic Schema_ (samozřejmě separátně od úvodní doménové kapitoly).

## Související články

* [Kniha Living Clojure](/2017/04/kniha-living-clojure)
