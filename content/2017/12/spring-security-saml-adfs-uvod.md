+++
title = "Spring Security, SAML & ADFS: Úvod"
date = 2017-12-20T22:34:00Z
updated = 2018-01-29T21:15:51Z
description = """Úvodní díl 3-dílného miniseriálu o Spring Security,
    SAML a ADFS. Podíváme se, co jednotlivé zkratky znamenají a jak to
    celé funguje. Včetně krásných barevných diagramů."""
tags = ["security", "spring", "saml"]
aliases = [
    "/2017/12/spring-security-saml-adfs-uvod.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2017/12/Spring-Security.png" >}}

Posledních pár týdnů jsem se teď mordoval se [Spring Security](//projects.spring.io/spring-security/), abych v grandiózním finále doiteroval k autentikaci pomocí [SAML](//en.wikipedia.org/wiki/Security_Assertion_Markup_Language) vůči [ADFS](//docs.microsoft.com/en-us/windows-server/identity/active-directory-federation-services).

Mimochodem, přijde mi, že poslední dobou, je to se _Springem_, jako s jakoukoliv jinou (velkou) dominantní značkou --- pokud se pohybujete uvnitř dané domény, na vyhrazeném dvorečku, všechno frčí, jak na drátkách. Jakmile to zkusíte zintegrovat s něčím ne-nativním, začnou probémy. A rozlousknout ty problémy chvilku trvá --- zvlášť, když [StackOverflow](//stackoverflow.com/questions/tagged/spring-security+saml) mlčí a oficiální dokumentace se k (technologickým) cizincům moc nezná.

Ale to jsem odbočil, zpátky k tématu. Tentokrát se nebudu věnovat ani aktuálnímu use casu, ani těm problémům, ale zaměřím se jen na to, jak Springovský SAML s ADFS funguje a jak to v aplikaci naimplementovat a celkově nakonfigurovat.

Nicméně, pokud jste se ještě stále nechytli, o čem to mluvím a přemýšlíte, jestli číst dál, tak to bude o: lehce komplikované autentikaci webové aplikace oproti _Active Directory_ (takový Microsoftí [LDAP](//en.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol)).

## SAML slovníček

Neuškodí si trochu prosvištět názvosloví. I když SAML nepřináší nic moc převratného, přeci jenom říká určitým věcem svým vlastním způsobem.

* **SAML** --- Security Assertion Markup Language
* **User Agent (UA)** --- software, skrz který uživatel komunikuje se zabezpečeným zdrojem, typicky browser.
* **Service Provider (SP)** --- systém, který poskytuje zabezpečený zdroj a akceptuje _authentication assertions_ a poskytuje _Single Sign-On_ (viz následující body).
* **Identity Provider (IdP)** --- systém, který vydává _authentication assertions_ a podporuje _Single Sign-On_.
* **SAML Assertions** --- assertions jsou gró SAML zpráv, které lítají mezi _SP_ a _IdP_. Obsahují bezpečnostní informaci/e trojího typu: _authentication statements_, _attribute statements_ a _authorization decision statements_. Nás budou zajímat první dva typy: jestli je uživatel autentikovaný a do jakých _Active Directory_ skupin patří.
* **Single Sign-On (SSO)** --- přihlášení uživatele do více systémů jedinou akcí.
* **Single Logout (SLO)** --- odhlášení uživatele z více systémů jedinou akcí.
* **SAML Metadata** --- XML data formát pro popis specifického SAML deploymentu, tedy _SP_ nebo _IdP_.

## ADFS slovníček

I ADFS má vlastní okruh termínů:

* **ADFS** --- Active Directory Federation Services
* **Claims** --- prohlášení týkající se uživatelů, které se primárně používá pro autorizaci přístupu k aplikacím. _Claims_ jsou to, co potřebujeme dostat do SAML _assertions_ jako _attribute statements_.
* **Claims Provider** --- systém, který poskytuje _claims_.
* **Relying Party Trust** --- partnerský systém, který zpracovává _claims_ a kterému ADFS důvěřuje.
* **Federation Metadata** --- XML data formát pro výměnu konfigurace mezi _Claims Providerem_ a _Relying Party Trust_.

## SAML Binding

Následující popis _SSO_ a _SLO_ je poměrně detailní (musím se pochválit, že podrobnější jsem nevygoogloval) a je zaměřený na jednu konkrétní technologickou integraci --- [Spring Security SAML](//projects.spring.io/spring-security-saml/) a _ADFS_. To znamená, že pro jiné konstelace to může fungovat "trošku" jinak.

Rovněž stojí za zmínku, že SAML specifikace definuje tzv. _binding_, tj. jak jsou SAML requesty a responsy mapovány na standardní message a komunikační protokoly. Pro _SSO_ v browseru se většinou společně používají [HTTP Redirect Binding](//en.wikipedia.org/wiki/SAML_2.0#HTTP_Redirect_Binding) a [HTTP POST Binding](//en.wikipedia.org/wiki/SAML_2.0#HTTP_POST_Binding). V následujících diagramech byste je měli bez problémů identifikovat. Co je podstatné --- u tohoto druhu bindingu teče veškerá komunikace přes _User Agent_ (browser), takže tam např. není žádná komunikace na pozadí mezi _SP_ a _IdP_.

## SAML Single Sign-On

Na následujícím obrázku jsem si dal hodně záležet a věřím, že jeho vysvětlující a informační hodnota je enormní, podaná krystalicky čistým způsobem. Přesto si neodpustím kratičké shrnutí:<br />

1. _Browser (UA)_ přistoupí na chráněný zdroj na _Service Providerovi (SP)_, který přesměruje _UA_ na svoje login URL.
1. _SP_ vygeneruje _SAML request_ a přesměruje UA na _Identity Providera (IdP)_, kterému je předán _SAML request_.
1. _IdP_ vrátí _UA_ přihlašovací formulář (jméno, heslo).
1. _UA_ pošle _IdP_ credentials uživatele, spolu se _SAML requestem_.
1. _IdP_ předá credentials _Claims Providerovi (CP)_, který ověří uživatele a v případě úspěchu vrátí před-konfigurované _claims_.
1. _IdP_ přebalí _claims_ do _assertions_, které obalí do _SAML response_ a tu pak vloží do HTML formuláře, který pošle _UA_. Součástí formuláře je i _JavaScript_, který může formulář automaticky odeslat.
1. _UA_ automaticky odešle formulář se _SAML response_ na _SP_. V případě, že je v _UA_ vypnutý _JavaScript_, musí uživatel odeslat formulář klepnutím na tlačítko.
1. _SP_ ověří přijaté _SAML assertions_ a pokud je všechno v pořádku, přesměruje _UA_ na původně vyžádaný zdroj.

{{< figure caption="SAML Single Sign-On" src="/2017/12/SAML-SSO.png" link="/2017/12/SAML-SSO.png" >}}

Uvedené schéma zobrazuje komunikaci pro ne-autentikovaného uživatele. V případě, že je uživatel již přihlášen --- ať už lokálně na daném _SP_, nebo díky _SSO_ na jakémkoliv jiném _SP_ (který je registrován u daného _IdP_) --- tak se přeskočí posílání a odesílání _ADFS_ login formuláře (a samozřejmě komunikace s _CP_) a _UA_ rovnou obdrží _SAML response_.

## IdP Discovery

Uvedený scénář se dá zpestřit ještě o jednu věc. Vztah mezi _SP_ a _IdP_ je m:n<br />

* jeden _IdP_ může obsluhovat několik _SP_ a stejně tak
* jeden _SP_ si může vybrat z několika _IdP_.

V prvním případě se nic zvláštního neděje, právě od toho tu je _SSO_. V druhém případě je to trochu komplikovanější --- jak si _UA/SP_ vybere, vůči kterému _IdP_ se autentikovat?

Tenhle případ řeší _IdP Discovery_. Místo úvodního přesměrování na login URL aktuálního _SP_ dojde k přesměrování na stránku se seznamem všech zaregistrovaných _IdP_, z nichž si uživatel explicitně vybere.

Nastavit _IdP Discovery_ pomocí _Spring Security SAML_ není nijak složité, nicméně pro tento a následující články s touto možností nepracuji.

## SAML Single Logout

Když jsem se pustil do kreslení předešlého diagramu pro SSO, tak se mi výsledek tak zalíbil, že jsem si střihnul ještě jeden obrázek --- pro _Single Logout (SLO)_.

A aby to nebylo triviální, tak jsem si rozchodil dva _SP_, abych mohl zdokumentovat, jak probíhá odhlášení ze všech zaregistrovaných _SP_. Protože o tom _SLO_ je: když se odhlásím na jednom _SP_, tak mě to automaticky odhlásí i ze všech ostatních _SP_.

{{< figure caption="SAML Single Logout" src="/2017/12/SAML-SLO.png" link="/2017/12/SAML-SLO.png" >}}

## To be continued...

Abych udržel článek v rozumné čitelnosti, rozhodl jsem se téma rozdělit do krátkého mini-seriálku. Příště bych se podíval, jak vyměnit _metadata_ mezi _SP_ a _IdP_, plus jak nakonfigurovat _ADFS_.

V závěrečném díle bych probral konfiguraci _Spring Security SAML_. Můžete se těšit na popis _Java configuration_ (ofiko dokumentace jede furt na XML) a samozřejmě to pofrčí na aktuálním _Spring 5_.

## Související články

* [Spring Security, SAML &amp; ADFS: Konfigurace](//www.sw-samuraj.cz/2018/01/spring-security-saml-adfs-konfigurace.html)
* [Spring Security, SAML &amp; ADFS: Implementace](//www.sw-samuraj.cz/2018/01/spring-security-saml-adfs-implementace.html) 
