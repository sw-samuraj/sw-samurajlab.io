+++
title = "Střípky z prototypování: Wicket, Spring, REST"
date = 2017-08-22T23:09:00Z
updated = 2017-08-22T23:09:38Z
description = """Vytvořil jsem si několik prototypů a o některé z nich
    bych se rád podělil. Začneme trojkombinací Wicket, Spring, REST."""
tags = ["wicket", "rest", "sw-engineering", "java"]
aliases = [
    "/2017/08/stripky-z-prototypovani-wicket-spring.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2017/08/Shards.jpg"
    link="/2017/08/Shards.jpg" width="200" >}}

Měl jsem to štěstí, že jsem se teď mohl několik týdnů věnovat [prototypování](//en.wikipedia.org/wiki/Software_prototyping). Štěstí, protože je to jeden z mých nejoblíbenějších aspektů softwarového inženýrství.

Všechny prototypy vedly k cílovému, zbrusu novému řešení. V kontextu naší firmy, by to normálně dělalo oddělení [R&amp;D](//en.wikipedia.org/wiki/Research_and_development), ale z kapacitních a projektových důvodů to skončilo v našem _delivery_ týmu. Rád jsem se toho ujal.

Vzhledem k tomu, že by popis řešení a implementace zabraly mnoho článků, rozhodl jsem se vytáhnout jen několik zajímavějších fragmentů z následujících témat (a i ty rozdělím do dvou-tří zápisů):

* REST contract-first
* Wicket + Spring
* Wicket + REST via Spring
* WebSockets
* Embedded Neo4j
* Embedded Infinispan

## REST contract-first

O tomhle tématu už jsem psal v článku [REST contract-first: Swagger &amp; Gradle](/2017/05/rest-contract-first-swagger-gradle). Zmiňuju ho pro úplnost, ať je to na jednom místě a také protože vychází ze stejného konceptu prototypu, jako všechny následující.

## Wicket + Spring

### Use case

Na počátku bylo slovo: udělejte novou aplikaci. Technologie nikdo neřešil --- po pravdě, existovalo rozlehlé architektonické vakuum. Tak jsem se do toho vložil a na základě tehdejších informací a kontextu jsem vybral dvě technologie: [Wicket](//wicket.apache.org/) na webové GUI a Spring na "všechno ostatní".

"Všechno ostatní" zde znamená:

* REST služby a klienti
* Business logika
* (Potenciálně) persistence
* (Finálně) security

### Implementace

Wicket má se Springem dobré vztahy už po léta... i když, vo-cuď-pocuď. Zkrátka, bývávalo, že jste si ze Springu vzali jen to co potřebujete. Kdepak, už je to pěkně nenažraný cvalík, který si pozve spoustu bratříčků. A skloubit ho s něčím specifickým... je docela práce.

Nicméně. Způsob, jak propojit Wicket a Spring je dvojí --- jde o to, kdo koho instancuje. První způsob --- kdy Wicket instancuje Springovský kontext, ze kterého je potom možné injektovat do Wicketovských komponent přes anotaci [@SpringBean](//ci.apache.org/projects/wicket/apidocs/8.x/org/apache/wicket/spring/injection/annot/SpringBean.html) --- je dobře popsaný ve Wicketovské Reference Guide: [Integration Wicket with Spring](//ci.apache.org/projects/wicket/guide/8.x/single.html#_integrating_wicket_with_spring).

Druhý způsob je flexibilnější, avšak, není nikde moc zdokumentovaný a když už, tak jen pomocí konfigurace [web.xml](//docs.oracle.com/middleware/1221/wls/WBAPP/web_xml.htm). Funguje to tak, že [WicketFilter](//ci.apache.org/projects/wicket/apidocs/8.x/org/apache/wicket/protocol/http/WicketFilter.html) instancuje [SpringWebApplicationFactory](//ci.apache.org/projects/wicket/apidocs/8.x/org/apache/wicket/spring/SpringWebApplicationFactory.html), která prohledá Springovský kontext a instancuje Wicketovskou [WebApplication](//ci.apache.org/projects/wicket/apidocs/8.x/org/apache/wicket/protocol/http/WebApplication.html). Výhodou je, že se dá injektovat přímo do Wicket aplikace, což u předešlého způsobu nejde.

Pokud chceme oželet `web.xml` (ano, prosím!), vypadá konfigurace filtru takto:

{{< gist sw-samuraj 477258ac1ea732f4a95f531614538407 >}}

Prozatím pomiňme, že filter rozšiřuje [JavaxWebSocketFilter](//ci.apache.org/projects/wicket/apidocs/org/apache/wicket/protocol/ws/javax/JavaxWebSocketFilter.html) (viz sekce _WebSockets_), obyčejný [WicketFilter](//ci.apache.org/projects/wicket/apidocs/8.x/org/apache/wicket/protocol/http/WicketFilter.html) postačí také.

### Prototype repozitory

* [blog-wicket-spring-rest](//bitbucket.org/sw-samuraj/blog-wicket-spring-rest)

Pokud vám to neštymuje dohromady, klíčové jsou 3 třídy:

* [WicketAppFilter](//bitbucket.org/sw-samuraj/blog-wicket-spring-rest/src/tip/src/main/java/cz/swsamuraj/wicketspring/WicketAppFilter.java)
* [SpringContextLoaderListener](//bitbucket.org/sw-samuraj/blog-wicket-spring-rest/src/tip/src/main/java/cz/swsamuraj/wicketspring/SpringContextLoaderListener.java) (klasika)
* [WicketApplication](//bitbucket.org/sw-samuraj/blog-wicket-spring-rest/src/tip/src/main/java/cz/swsamuraj/wicketspring/WicketApplication.java) (anotace [@Component](//docs.spring.io/spring/docs/current/javadoc-api/org/springframework/stereotype/Component.html)).

### Poučení

Tady se žádné velké překvapení nekoná --- funguje to stejně dobře, jako když jsem s Wicketem a Springem pracoval před osmi lety poprvé, jen se nám oba frameworky mocně posunuly ve verzích. Snad jen, že na takovém malém prototypu se jednoduše nacvičí přepnutí z jednoho typu instancování na druhý.

## Wicket + REST via Spring

### Use case

Spring a Wicket nám teď pěkně kohabitují. Ale chceme přidat RESTové služby --- v současnosti všudypřítomná a triviální záležitost. Ne tak docela.

Spring nabízí REST buď v rámci [Spring MVC](//docs.spring.io/spring/docs/current/spring-framework-reference/html/mvc.html), nebo [Spring Boot](//projects.spring.io/spring-boot/). Přiznám se, tady mne dost zaskočilo, že Spring nenabízí RESTové služby samostatně, podobně jako ty SOAPovské ([Spring WS](//projects.spring.io/spring-ws/)). Nejsem expert na Spring, takže mi nejspíš něco schází, ale zatím to vidím, že dostává na frak buď modularita, nebo snadnost použití. Přitom jediný, co vlastně potřebuji je [DispatcherServlet](//docs.spring.io/spring/docs/current/javadoc-api/org/springframework/web/servlet/DispatcherServlet.html).

K tomu use casu --- potřeboval jsem dedikované URL pro REST, které by neobsluhoval Wicket a zpracovávat/produkovat JSON zprávy.

### Implementace

Řešení přišlo ve dvou krocích --- zaregistrovat DispatcherServlet a podstrčit JSON mapování (z nějakého důvodu to Spring nedělá out-of-the-box). Dobral jsem se k řešení, které je sice funkční, ale rozhodně ne elegantní a možná i špatný design. (Zde bych byl vděčný za jakékoliv navedení na "správnou cestu".)

Zaregistrova DispatcherServlet jen tak přes [@WebServlet](//docs.oracle.com/javaee/7/api/javax/servlet/annotation/WebServlet.html) annotaci mi nešlo --- lítaly z toho tuny výjimek, protože ve Springovském kontextu chybělo spousty věcí ze Spring webové aplikace. Všechno to, co vám tam přidá anotace [@EnableWebMvc](//docs.spring.io/spring/docs/current/javadoc-api/org/springframework/web/servlet/config/annotation/EnableWebMvc.html). Tudy cesta nevedla.

Vyřešil jsem to dynamickým zaregistrováním servletu přes [ServletRegistration](//docs.oracle.com/javaee/7/api/javax/servlet/ServletRegistration.html), když jsem si přes Wicket vytáhnul [ServletContext](//docs.oracle.com/javaee/7/api/javax/servlet/ServletContext.html):

{{< gist sw-samuraj da7361df59a692e2162b156bbcbd3cb8 >}}

Druhý problém --- přesvědčit Spring, aby chroustal JSON --- mám vyřešený jen zpola: sice "zázračně" zafungovalo angažování komba [AnnotationMethodHandlerAdapter](//docs.spring.io/autorepo/docs/spring/current/javadoc-api/org/springframework/web/servlet/mvc/annotation/AnnotationMethodHandlerAdapter.html) a [MappingJackson2HttpMessageConverter](//docs.spring.io/spring/docs/current/javadoc-api/org/springframework/http/converter/json/MappingJackson2HttpMessageConverter.html). Bohužel, je první třída _@deprecated_ a to já nesnáším (pokudn není zbytí).

Spring doporučuje migrovat na [RequestMappingHandlerAdapter](//docs.spring.io/spring/docs/current/javadoc-api/org/springframework/web/servlet/mvc/method/annotation/RequestMappingHandlerAdapter.html), jenže k tomu chybí jakákoliv dokumentace a Google mlčí. [Moje otázka na StackOverflow](//stackoverflow.com/questions/43945913/how-to-migrate-from-annotationmethodhandleradapter-to-requestmappinghandleradapt) je také bez odpovědi.

Řešení, na které nejsem hrdý (účel světí prostředky):

{{< gist sw-samuraj e1b0720fa61d8da2387996d86c690b28 >}}

### Prototype repozitory

* [blog-wicket-spring-rest](//bitbucket.org/sw-samuraj/blog-wicket-spring-rest)

Klíčové třídy:

* [WicketApplication](//bitbucket.org/sw-samuraj/blog-wicket-spring-rest/src/tip/src/main/java/cz/swsamuraj/wicketspring/WicketApplication.java) (ServletRegistration)
* [SpringRestConfiguration](//bitbucket.org/sw-samuraj/blog-wicket-spring-rest/src/tip/src/main/java/cz/swsamuraj/wicketspring/SpringRestConfiguration.java) (AnnotationMethodHandlerAdapter)
* [PersonApiController](//bitbucket.org/sw-samuraj/blog-wicket-spring-rest/src/tip/src/main/java/cz/swsamuraj/wicketspring/ws/api/PersonApiController.java) (klasika)

### Poučení

Spring je fajn. Ale přijde mi po těch letech, že je míň flexibilní a narostl do stejného molocha jako _Java EE_. Pokud potřebujete něco specifického, budete se brodit Springovskými výjimkami a zoufale prohledávat StackOverflow. Mimochodem, [další moje StackOverflow otázka](//stackoverflow.com/questions/43762364/spring-contract-first-rest) o Springu je také nezodpovězena.

Říkám si, jestli to za to stojí, se přehrabovat tak hluboko ve vnitřnostech Springu, aby člověk implementoval relativně jednoduché věci. Trochu mi to zavání [vendor lock-in](//en.wikipedia.org/wiki/Vendor_lock-in): dobře si rozmyslet, jestli stavět heterogenní aplikaci, nebo použít homogenní Spring.

## Příště

V pokračování _střípků_ se podíváme na [WebSockety](//en.wikipedia.org/wiki/WebSocket) a jak do "standardní" webové aplikace přidat "reactive-like" chování.

## Repository všech prototypů

* [blog-rest-contract-first](//bitbucket.org/sw-samuraj/blog-rest-contract-first)
* [blog-wicket-spring-rest](//bitbucket.org/sw-samuraj/blog-wicket-spring-rest)
* [blog-embedded-neo4j](//bitbucket.org/sw-samuraj/blog-embedded-neo4j)
* [blog-embedded-infinispan](//bitbucket.org/sw-samuraj/blog-embedded-infinispan)

## Související články

* Střípky z prototypování II: WebSockets (TBD)
* Střípky z prototypování III: Neo4j, Infinispan (TBD)
* [REST contract-first: Swagger &amp; Gradle](/2017/05/rest-contract-first-swagger-gradle)
