+++
title = "Šest ctností softwarového inženýra"
date = 2017-08-01T12:17:00Z
updated = 2017-08-01T12:17:48Z
description = """Rád hledám paralely k softwarovému inženýrství. Jedna
    taková mne hned napadla při čtení budhistické knížky pro děti.
    Může se programátor vydat na dlouhou cestu bódhisattvy?"""
tags = ["sw-engineering"]
aliases = [
    "/2017/08/sest-ctnosti-softwaroveho-inzenyra.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2017/08/Lotus.jpg"
    link="/2017/08/Lotus.jpg" width="200" >}}

Čteme teď s dětmi výbornou knížku [Buddhovy pohádky na dobrou noc](//www.kosmas.cz/knihy/155507/buddhovy-pohadky-na-dobrou-noc/). Kromě pohádek samotných je v úvodu knihy několik krátkých kapitol o tom, co je to Buddhismus, kdo byl Buddha apod. Tam mi padla do oka kapitola _Šest ctností_ ([pāramitā](//en.wikipedia.org/wiki/P%C4%81ramit%C4%81#Mah.C4.81y.C4.81na_Buddhism)) a okamžitě mi začaly naskakovat paralely ze softwarového inženýrství.

## 1. Dána

> _Touha dávat každému, aniž bych očekával odměnu._

Moje subjektivní zkušenost bloggera je, že máte jen minimální zpětnou vazbu na své psaní a když už nějaká je, tak je neobjektivní --- pokud nepíšete úplné blbosti, tak vás (zlomek) lidí buď pochválí, sem tam nějaký ten troll, nebo spam a občas vás někdo upozorní na chyby a nesrovnalosti.

Zkrátka, obsah blogu si žije svým životem a vy to nemůžete nijak ovlivnit. Pořád ještě žiju v iluzi, že občas mé psaní někomu pomůže, nebo ho aspoň pobaví. Není to jediný aspekt mého psaní, ale právě potřeba unilaterálního sdílení je silně přítomna.

Ze stejného ranku je také obecně [knowledge sharing](//en.wikipedia.org/wiki/Knowledge_sharing). Krystalickým příkladem je potom _open source_. Možná, že někdo publikuje svůj kód, protože očekává nějaké sociální benefity, třeba uznání. Ale řekl bych, že většinou je to záležitost kultury a osobního přístupu.

## 2. Šíla

> _Rozvíjení mravného chování._

Možná už jste to zažili --- přišel za váma projekťák, či jiný manažer a (v lepším případě) vás požádal, nebo vám přímo přikázal něco, co je nemorální. Možná jen trošku, možná za hranicí slušnosti a možná i něco, co je polo/ne-legální.

Možná jste byli v týmu, který nebyl přátelský k ostatním stakeholderům. Možná ani ke členům vlastního týmu.

Nevytáhnu žádné eso z rukávu, jak tyhle věci řešit. Můžu potvrdit, že je to těžké, obstát se ctí. Abyste se za to nemuseli další léta stydět.

## 3. Kšánti

> _Trpělivost a schopnost zůstat klidný, zejména v problematických situacích._

V IT se točí hodně peněz. Možná jste dělali něco pro nějakou banku, nebo evropský/mezinárodní/globální projekt, kde šlo o miliony $/€. Když pak dojde na lámání chleba, lidi dokážou pěkně zdivočet.

A nemusí jít jen o velké peníze. Někdy stačí jen nevraživost v kanceláři, či spor o to, jestli se bude větrat, nebo pouštět klimatizace.

Tváří v tvář takovým malým a velkým problémům se pak poznají profesionálové a leadeři. A vlastně... zralí lidé. Kteří neztrácejí ze zřetele, proč jsou tady a co je cílem (projektu, schůzky apod.) a situaci věcně řeší. Nenechají se strhnout emocemi.

A ví, že některé věci potřebují čas. Však to znáte --- žádný projekt/produkt nevyrostl přes noc.

## 4. Virjá

> _Nadšené úsilí, které podněcuje sílu a vytrvalost, jež jsou nezbytné pro pokračování na cestě bódhisattvy._

Projekty můžou být krátké a dlouhé. Produkty mají životnost ještě delší. Být smysluplným přispěvatelem v takové práci je běh na dlouhou trať. Ne náhodou beru maraton (a trénink na něj), jako dobrou paralelu softwarového projektu. Přijdou těžké chvíle, to je realita. Co vás z toho vytáhne, je motivace. Proč to děláte?

> _Winners never quit and quitters never win._ ~ Vince Lombardi

## 5. Dhjána

> _Koncentrace nebo meditace, která rozvíjí duševní schopnost pevného záměru, aby naše činění přinášelo lepší výsledky._

Tak pravda, tady to asi úplně nesedí. Ale máme třeba [coding kata](//en.wikipedia.org/wiki/Kata_(programming)). A určitě spoustu z vás kóduje po večerech, či po nocích, zkrátka ve svém volném čase. A pokud se u toho dostanete do _flow_, je to vlastně [práce jako meditace](//www.osho.com/learn/work-as-meditation/osho-on-work-as-meditation).

> _Work is valuable. It will bring humbleness and silence._ ~ Osho

## 6. Pradžňá

> _Moudrost, nejen coby intelektuální porozumění, ale také schopnost přímého vhledu do pravé podstaty skutečnosti._

Ve svém jádru je softwarové inženýrství velice racionální a opřené o data. Pokud opravdu dobře rozumíte nějakému jazyku, knihovně, frameworku, víte, jak to uvnitř pracuje. Ale svět projektů je daleko rozsáhlejší a nikdo ho není schopen obsáhnout v jeho celistvosti --- znáte to podobenství [o slepcích a slonovi](//en.wikipedia.org/wiki/Blind_men_and_an_elephant).

Čím více je člověk expert v nějaké oblasti, tím více má vyvinutou intuici. Tedy --- přímý vhled do problému. Vede k tomu dlouhá, trnitá a klikatá cesta.

## Cesta bódhisattvy

Ne nadarmo se ultimátnímu odborníkovi v IT říká _guru_. Guruem se člověk nerodí, ale postupně stává. Cesta [bódhisattvy](//en.wikipedia.org/wiki/Bodhisattva) (někdo, kdo odložil vlastní osvícení, aby pomohl druhým dosáhnout stejného stavu) může být jední ze způsobů, jak se na tuto úroveň dostat.

## Související články

* [Software Engineering, má rozumné paralely?](/2016/10/software-engineering-ma-rozumne-paralely)
* [Programátor -&gt; Vývojář -&gt; Software Engineer](/2017/02/programator-vyvojar-software-engineer)
