+++
title = "Kniha Living Clojure"
date = 2017-04-05T22:08:00Z
updated = 2017-04-05T22:08:33Z
description = """Vrátil jsem se po čase ke Clojure a věrný svému přístupu
    jsem sáhnul po knize. Living Clojure od Carin Meier je dobrá kniha
    pro (věčné) Clojure začátečníky."""
tags = ["clojure", "knihy"]
blogimport = true 
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://www.blogger.com/profile/12147991495120816879"
+++

{{< figure class="floatleft" src="/2017/04/Living-Clojure.png"
    link="/2017/04/Living-Clojure.png" width="200" >}}

Jsem beznadějný čtenář knih. Musím denně číst a ačkoliv jsou dnes k dispozici tuny textů v různých formách, mě nejvíc vyhovují knihy.

Jedinou úlitbou modernosti je, že pro odbornou literaturu mi více vyhovují ty digitální. Přece jenom --- do Kindlu se toho vejde kupa a zastaralé vydání vám nepřekáží v reálném světě.

## Back to the Future

Ani nevím, co mě to cvrnklo přes nos, ale po pětileté přestávce se vracím ke _Clojure_. Pět let je v softwarovém inženýrství dlouhá doba. Technologie můžou zaniknout, či se nečekaně rozvinout, nebo nabrat úplně jiný směr. A těší mě, že _Clojure_ stále rozkvétá.

Říkal jsem si --- jak se do toho zase dostat? No dobře, je to řečnická otázka. Spíš jsem si říkal, kterou knihu si vybrat? Když jsem s _Clojure_ začínal, byla na trhu jediná kniha --- [Programming Clojure](//pragprog.com/book/shcloj/programming-clojure) od [@StuartHalloway](//twitter.com/stuarthalloway). Byla to tehdy bible _Clojure_. A možná ještě je --- letos vyjde [třetí edice](//pragprog.com/book/shcloj3/programming-clojure-3rd-edition)!

Udělal jsem si seznam a jako první volbu jsem hodně zvažoval oceňované [Joy of Clojure](//www.manning.com/books/the-joy-of-clojure-second-edition), jejíž první edici jsem kdysi nedočetl. Nakonec ale převážila náhoda. (Mimochodem, kdyby vás ten seznam zajímal, najdete ho [u mne na Goodreads](//www.goodreads.com/review/list/25024972-v-t-kota-ka?shelf=clojure). Nebo ve webové verzi zaskrolujte po pravé straně dolů.)

## Gigasquid!

Že jsem zase vlítnul do _Clojure_ je více méně náhoda. Nebo osud. Minulý podzim jsem se byl po čase podívat na Java konferenci --- [český GeeCON](//geecon.cz/) --- a tam jsem potkal Carin Meier, aka [@Gigasquid](//twitter.com/gigasquid). (Už jsem o tom psal na svém druhém blogu --- [GeeCON Prague 2016, den 2](/2016/11/geecon-prague-2016-den-2.html).)

Carin stojí za to sledovat. Kromě výše a níže zmíněné knihy, pracuje v [Cognitectu](//cognitect.com/). Ano, v té firmě, kde se koncentruje _Clojure_ smetánka ([posuďte sami](//cognitect.com/team)), včetně polo-boha Riche Hickeyho.

Pokud budete mít možnost, určitě si s Carin popovídejte. Zvládl jsem to i já, zavilý introvert. Zakecal jsem se s ní tak moc, až jsem v podstatě prošvihnul následnou přednášku.

## Alice in Wanderland

No dobře, to bylo keců... co ta kniha? Je dobrá, dal jsem jí na Goodreads [5 hvězd](//www.goodreads.com/book/show/25414665-living-clojure). Samozřejmě, v daném kontextu --- je to dobrá kniha pro začátečníky, nebo pro někoho, kdo si potřebuje po čase _Clojure_ oživit.

{{< figure class="floatright" src="/2017/04/Alice-in-Wonderland.gif"
    link="/2017/04/Alice-in-Wonderland.gif" width="320" >}}

Kniha prochází všechny základní témata, která byste v učebnici čekali a skládá je v rozumném pořadí:

* Kolekce a sekvence
* Funkcionální programování
* Stav a konkurence
* Java interoperabilita
* [Leiningen](//github.com/technomancy/leiningen) a _Clojure_ projekt
* Asynchronni komunikace s [core.async](//github.com/clojure/core.async)
* _Clojure_ web applikace
* Makra

Napsáno pěkně, stručně, přehledně. Kromě toho, ale přináší druhá část knihy tréninkový program: _Clojure_ v 7 týdnech.

Jak píše Carin v úvodu, učení se nového jazyka má silné paralely, jako když chcete zčista jasna začít běhat: důležitá je motivace, pravidelnost a... nepřetrénovat se.

Ohledně běhání, na webu je dnes spousta tréninkových plánů od 5 kilometrů až po maraton. Carin k tomu přistupuje obdobně --- začít zvolna a postupně navyšovat objemy a obtížnost.

Prvních pár týdnů jde o řešení problémů na [4clojure](//www.4clojure.com/). Pak se přesuneme na GitHub, kde má Carin několik kata, inspirovaných příběhem _Alenky v říši divů_: [wonderland-clojure-katas](//github.com/gigasquid/wonderland-clojure-katas). Poslední týden je pak věnovaný deploymentu _Clojure_ aplikace na [Heroku](//www.heroku.com/).

A je to! Kniha je přečtená a vy jste fluent v _Clojure_ :-)  No, tak jednoduché to není. Ale je to dobrý začátek. Jeden z možných.
