+++
title = "Programátor -> Vývojář -> Software Engineer"
date = 2017-02-27T22:01:00Z
updated = 2017-02-27T22:01:43Z
description = """Pokud člověk zůstane pár let v jedné profesi a nezaseknul
    se někde na začátku, prochází určitým vývojem. Jak může vypadat jedna
    z možných evolucí adepta softwarového inženýrství?"""
tags = ["sw-engineering"]
aliases = [
    "/2017/02/programator-vyvojar-software-engineer.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2017/02/Programmer-writing-code-with-Unit-Tests.jpg" >}}

Jak léta jdou, přemýšlím kontinuálně o cestě, kterou jsem urazil a kam směřuju. Jeden takový pohled na _část_ takové cesty naznačuje titulek.

Dneska už jsem na té cestě dál --- jsem v bodě, kdy už pár let "stavím" týmy. A byť nehledám lidi, kteří jsou mým odrazem --- naopak, mám rád diverzitu a plánovaně, podvratně :-) ji aplikuju --- podvědomě vybírám lidi, kteří na podobnou cestu aspirují a berou ji jako součást týmové kultury.

Téma je pro mě zajímavé --- jak čistě myšlenkově, tak i prakticky. Pokud pracujete s týmy, tak kromě těch technických věcí, řešíte taky záležitosti jako rozvoj lidí, jejich vzrůstající finanční očekávání, ohodnocení jejich technické a jiné seniority atd.

Zároveň nebudu skrývat, že ne vždy se to podaří --- přesvědčit, inspirovat lidi, že tohle je ta správná cesta. Občas vidíte potenciál, ale nepovede se vám dostat daného člověka z jeho bubliny. A někdy se prostě jen mýlíte a chybujete.

## Korintským

Asociace, která mi naskočila, když jsem začal o tématu přemýšlet, pochází, možná překvapivě, z Bible --- Pavlova listu Korintským. Je to taková ta pasáž, co se čte vždycky na svatbách... ale nebojte, nebudu vás zahrnovat láskou ;-) mám na mysli jinou větu:

> _Dokud jsem byl dítě, mluvil jsem jako dítě, myslel jsem jako dítě, měl jsem dětské názory; když jsem však dospěl, s dětinskými věcmi jsem se rozloučil._ — [1. Korintským 13:11](//www.bible.com/cs/bible/15/1co.13.11)

Co mně přijde podstatné na tomhle citátu, je ten přechod --- opuštění určité formativní fáze a přechod do další. A i když s tím občas bývají problémy, nejde o odvrhnutí předešlé fáze, naopak, ta by měla být integrována.

(A jen pro jistotu, protože vím, jak jsou lidi chytlavý --- výběrem citátu jsem nechtěl říct, že – obecně – třeba programátoři jsou dětinští. Jasně, že občas jsou, ale není to generalizace. Ale což, ať si každý odnese, co potřebuje.)

## Model

Jednoduchý model, který popisuje naše téma, vypadá jako tři soustředné kruhy. A tak jako cibule, krystal, či perla, rostou postupně schopnosti našeho hypotetického programátora.

{{< figure src="/2017/02/SW-Engineer.png" caption="Skill model" >}}

## Programátor

Znáte termín _code monkey_? Je to většinou nelichotivý termín, i když občas se k němu někdo hrdě hlásí (většinou nápisem na tričku). Mě se líbí definice z [Urban Dictionary](//www.urbandictionary.com/define.php?term=code+monkey&amp;defid=746354), která dobře vystihuje, co si pod termínem _programátor_ představuju já:

_"A programmer who isn't actually involved in any aspect of conceptual or design work, but simply writes code to specifications given."_

Programátor je pro mne člověk, který --- s ohledem na svou senioritu --- dobře rozumí úzce vymezené technické oblasti. Hranice je zpravidla definována daným programovacím jazykem a v dnešní době také stále více nějakým přidruženým frameworkem. Cokoliv za algoritmy, technickými aspekty jazyka a lokální kompilací (je-li potřeba), je mimo oblast zájmu a zodpovědnosti programátora --- jeho práce končí komitem do verzovacího systému.

Taková úzce vymezená specializace nemusí být na škodu --- záleží, jaká je vaše doména, role v týmu, rozsah projektu ad. Na druhou stranu, moderní softwarový vývoj vyžaduje daleko komplexnější spektrum schopností a mít v týmu "pouhého" programátora pak často znamená více zatížit ostatní členy týmu a  přenést na ně část činností, jež u programátora "padají pod stůl".

## Vývojář

Vývojář je už o úroveň dál. Že dobře rozumí svému programovacímu jazyku, je samozřejmost. Stejně tak dobře ovladá sadu souvisejících knihoven a frameworků, jež tvoří jeho technickou doménu.

Jako velmi volný příklad si představme mlhavý termín _Backend Developer_. Umí asi nějakou "business logiku", řekněme v případě Javy někdo s [CDI](//docs.oracle.com/javaee/7/tutorial/cdi-basic002.htm)/[EJB](//docs.oracle.com/javaee/7/tutorial/ejb-intro001.htm), nebo [Spring](//projects.spring.io/spring-framework/)+[AOP](//docs.spring.io/spring/docs/current/spring-framework-reference/html/aop.html)+[Transakce](//docs.spring.io/spring-framework/docs/4.3.x/spring-framework-reference/html/transaction.html), plus nějakou tu persistenci, tudíž [JPA](//docs.oracle.com/javaee/7/tutorial/persistence-intro.htm)/[Hibernate](//hibernate.org/orm/)/[MyBatis](//blog.mybatis.org/)/[JDBC](//www.oracle.com/technetwork/java/overview-141217.html). A trošku té security.

Takže to je samozřejmost. Ale je to ještě něco navíc --- větší, či menší povědomí, jak to spolu souvisí, nejen interně, ale i za hranice toho "backendu". Plus troška "toho designu" a zase --- jak na úrovni kódu, tak na úrovni komponent.

No a pak... pozvolné přibližování se k oblasti SW inženýrství: vědět něco o buildování, možná i nějaký ten release management, něco praxe a teorie testování (aspoň unit a functional), source code management, issue tracking, atd.

To hlavní, co odděluje vývojáře od programátora? Schopnost vidět v technické oblasti "big picture".

## Software Engineer

A co SW inženýr? Ovšem, je to dobrý vývojář. Ale ovládá také většinu souvisejících oblastí, potřebných pro kvalitní a moderní vývoj softwaru:

* konzistentní [release management](//en.wikipedia.org/wiki/Release_management)
* efektivní, ale "čitelný" [source code management](//en.wikipedia.org/wiki/Version_control)
* [issue tracking](//en.wikipedia.org/wiki/Issue_tracking_system), který podporuje daný vývojový proces
* [code quality](//en.wikipedia.org/wiki/Software_quality#Code-based_analysis) management
* efektivní dokumentace (kódu, komponent, releasů, konfigurací atd.)
* [continuous integration](//en.wikipedia.org/wiki/Continuous_integration)/[deployment](//en.wikipedia.org/wiki/Continuous_delivery)
* smysluplná a efektivní automatizace

Ale v první řadě --- a to už je moje velmi subjektivní spekulace --- rozumí byznisově tomu, proč daný úkol dělá a proč ho dělá daným způsobem (což je, vzhledem k mé zkušenosti, velmi, velmi vzácné). Přece jenom, je to inženýr a jeho prvotním úkolem je řešit problémy. Reálné problémy.

## Jediná cesta?

Na internetu a v knihách se vedou dlouhé diskuze, co je a co není SW inženýrství, ba, jestli vůbec něco jako SW inženýrství existuje. Moje zkušenost je, že na každém projektu je potřeba vždy a znova vybudovat terminologii. Význam a kontext konkrétního termínu se neustále proměňuje a podstatné je, abyste si s kolegy v týmu rozuměli.

Pro mne je software inženýr období a stav, kam jsem se dostal po téměř 20 letech programování. Šel jsem po výše naznačené cestě. Ta vaše bude zcela určitě jiná. O cestě, co jste urazili můžete vyprávět, ale zkušenosti jsou jenom vaše.

## Je to realistické?

Samozřejmě, život není o zjednodušujících modelech a ideálních okolnostech. Skutečný svět je špinavý, divoký a ne-soustředný. V realitě tak vidíme spoustu prolínání vyše řečených rolí, jež se častokrát mění v čase i v rámci krátkého období, jako je třeba iterace, nebo projekt. A pak, existuje spoustu odlišností, jež nejsou výjimkami, ale spíše úplně jinými koncepty a přístupy. Diverzita života se mi vždycky líbila.
