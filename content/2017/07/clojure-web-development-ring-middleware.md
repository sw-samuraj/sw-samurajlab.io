+++
title = "Clojure web development: Ring Middleware"
date = 2017-07-11T13:51:00Z
updated = 2017-09-19T16:01:41Z
description = """Poté, co jsme se podívali jak v Clojure zpracovat HTTP request
    a response, budeme pokračovat dalším obohacením tohoto základního webového
    paradigmatu a sice pomocí konceptu Middleware."""
tags = ["clojure"]
blogimport = true 
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://www.blogger.com/profile/12147991495120816879"
+++

{{< figure class="floatleft" src="/2017/07/Ouroboros.jpg" >}}

V [minulém článku](/2017/05/clojure-web-development-ring) jsme se podívali na úplně nejzákladnější základy webového vývoje v Clojure --- jak zpracovat HTTP request a response pomocí knihovny [Ring](//github.com/ring-clojure/ring). Tu nejzajímavější část Ringu --- Middleware --- jsme ale zmínili jen letmo a byla by škoda se do tohoto zajímavého konceptu trochu více neponořit.

## Připomenutí základních konceptů

Než zrychlíme z 0 na 100, připomenu čtyři základní komponenty Ringu:

* **Handler** --- funkce, která přijímá mapu reprezentující HTTP request a vrací mapu představující HTTP response.
* **Request** --- mapa reprezentující HTTP request, která obsahuje sadu "standardních" klíčů, které nám budou povědomé ze Servletu: `:server-port`, `:server-name`, `:remote-address`, `:request-method` ad.
* **Response** --- mapa, představující HTTP response, která obsahuje tři klíče: `:status`, `:header`, `:body`.
* **Middleware** --- funkce, která handleru přidá dodatečné funkcionality. Věci jako session, cookies, či parametry jsou řešené middlewarem.

## Anatomie Middlewaru

_Middleware_ je [funkce vyššího řádu](//en.wikipedia.org/wiki/Higher-order_function), která přijímá jako parameter _handler_ a vrací jiný _handler_ (jako anonymní funkci), který nějakým způsobem obohatil buď _request_, nebo _response_. V posdtatě se dá na _middleware_ nahlížet jako na takový funkcionální [decorator](//en.wikipedia.org/wiki/Decorator_pattern).

Pokud se podíváme na obecnou podobu _middlewaru_, má často následující strukturu (kudos to [StackOverflow](//stackoverflow.com/a/19459508/7864001)):

{{< gist sw-samuraj 88f981daab3191c4a29112891ae2fba3 >}}

## Balení middlewaru

Middleware většinou bývá jenom velmi jednoduchá funkce, takže komplexnější chování dostaneme zřetězením jednotlivých middlewarů a to tak, že je postupně zabalujeme do sebe:

{{< figure src="/2017/07/Ring-middleware-wrapping.png" >}}

Způsob, jak middlewary do sebe zabalit je dvojí --- buď klasické zanořené funkce, nebo častější způsob je pomocí [thread-first makra](//clojure.org/guides/threading_macros) (`->`):

{{< gist sw-samuraj 99be823eda3254e5647e04bc855c055c >}}

A jen pro úplnost --- zabalit se anglicky řekne "wrap", proto se používá konvence, že middlewary začínají prefixem `wrap-`.

## Ilustrační příklad

Řekněme, že bysme psali RESTovou službu, která přijímá data pomocí PUT. Pomineme funkční logiku, zpracování dat i routování a budeme se soustředit jen na middleware, který se bude chovat následovně:

* Pokud request metoda není PUT, vrátí middleware status _405_ a text _"Method Not Allowed"_.
* Pokud je request metoda PUT, vrátí status _204_ (No Content) a prázdné body.

Aby middlewary byly malé a znovupoužitelné, rozdělíme si každou funkčnost, popsanou v předešlých odrážkách, do samostatné funkce:

* `wrap-no-content` bude vracet status _204_ a prázdné body.
* `wrap-put-allowed` vrátí buď _405_ (a popis v body), pokud metoda není PUT, nebo jenom zavolá původní handler.

{{< gist sw-samuraj 1ccf709e110b1c9d3c4355ee62b422fe >}}

Teď už stačí jen dát tyto dva middlewary dohromady, abychom dostali požadované chování:

{{< gist sw-samuraj 8ed8699175a48d7f097b41d5bfa0ccd6 >}}

## GitHub projekt

Pokud vám výše popsané principy neštymují dohromady, mrkněte na GitHub, kde je malý spustitelný projektík, plus pár middleware unit testů:<br />

* [sw-samuraj/blog-ring](//github.com/sw-samuraj/blog-ring)

## Co příště?

O Ringu a jeho middlewarech by se dalo psát ještě dlouho, ale je čas pokročit. Logicky následným tématem je [Compojure](//github.com/weavejester/compojure) --- routovací knihovna pro Ring webové aplikace.

## Související články

* [Clojure web development: Ring](/2017/05/clojure-web-development-ring)
* Clojure web development: Compojure (TBD)
* Clojure web development: Hiccup (TBD)
