+++
title = "Technical Leader, mytické stvoření"
date = 2017-10-03T20:44:00Z
updated = 2017-10-03T20:52:21Z
description = """Technical leader je jedna z těch rolí, o které se občas mluví,
    ale nikdo pořádně neví, co to znamená. Je to takové, trochu mytické
    stvoření. A co když, nedejbože, někdo takovou roli vyfasuje? Co má
    taková role na starosti? SoftWare Samuraj poodkrývá roušku tajemství
    a nechá vás spatřit nefalšovaného jednorožce."""
tags = ["teamleading"]
aliases = [
    "/2017/10/technical-leader-myticke-strvoreni.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2017/10/Unicorn.png" >}}

**tl;dr** _Pokud vás téma zajímá, ale nechce se vám číst dlouhý článek, skočte na sekci **Model**. Nic jiného číst nepotřebujete_ 😉

## Into the woods

Jak už jsem letos párkrát zmiňoval, procházím určitým přelomovým obdobím, jehož vedlejším, či hlavním(?) důsledkem je, že opouštím vyšlapané cesty. Prostě jsem si koupil bare-foot boty a sešel z dlážděných cest na trávu a lesní pěšiny.

Není to ale žádné pálení mostů. Jsem za tu cestu, kterou jsem urazil, vděčný a hodně jsem se na ní naučil. A abych mohl tenhle úsek putování uzavřít, rád bych se podělil ještě o několik témat, která do budoucna opouštím. Můj náhled, co to je _technical leader_, je jedním z nich.

## Obiter dictum

Jako formální i neformální _technical leader_ jsem pracoval řadu posledních let --- v různém prostředí, pro různé zaměstnavatele, s různými týmy. Také jsem o této doméně hodně četl. Byla to role, která mne neskutečně bavila a ve které jsem se chtěl kontinuálně zlepšovat. Snad se mi to i dařilo.

Zároveň jsem viděl, jak lidé v mém blízkém a občas i (velmi) vzdáleném okolí tápou. Co by mělo být obsahem té role? Jak se mám naučit potřebné věci?

A občas jsem také viděl, jak měli lidé ambici stát se formálním _technical leaderem_, aniž by tušili co to obnáší a hrnuli se do toho s pouhými romantickými představami a vidinou nově deklarovaného statusu a finančních benefitů.

Kromě toho, že jsem sám v této roli pracoval, jsem i pár _technical leaderů_ vychoval. No, vychoval --- řekněme, že jsem jim nestál v cestě (ale to už je jiný příběh).

Takže i když vím, že "tam venku" existují lepší _technical leadeři_, než jsem já, cítím se trochu povolaný sdílet pár informací a osobních názorů. Už proto, že o tom nikdo moc nepíše. (A pár jednorožců už jsem viděl.)

## Model

K roli _technical leadera_ můžete mířit z jakékoli jiné _technické_ role. Pro potřebu tohoto článku budeme vycházet z pozice _vývojáře_, ale můžete si za něj dosadit "obecného" (či specifického) _SW Engineera_.

Výchozím bodem by měl být vývojář, který je "rozumně" seniorní. Co znamená ono rozumně, nechám na vašem zdravém rozumnu. 😉 Povýšení juniorního člověka na _technical leadera_ nedává moc smysl.

K tomu, aby se člověk stal _technical leaderem_, musí mít, kromě svého ústředního skillu, ještě nějaký **přesah**. Co jsem za ty roky vypozoroval, je to většinou přesah do jedné ze tří oblastí: _architektura_, _project management_ a _team leading_. A podobně jako u _technical leadingu_, mohou být tyto oblasti formální i neformální.

{{< figure src="/2017/10/Technical-leader.png" >}}

**tl;dr** _Pokud jste rychločtenáři, můžete nyní přeskočit na sekci **Gotický svorník (model re-visited)**. Pak už je to opravdu všechno._ 😜

## Developer

Jedním z méně častých, i když také možných typů _technical leadedra_, je vývojář, který sice nemá přesah do výše naznačených oblastí, ale který má velmi hluboké znalosti své technické domény. Je to ten tzv. guru (trošku devalvované slovo, což?). Ale nejenom to --- je také problem-solver a mentor. Umí vyřešit nejen své technické problémy, ale i svých kolegů a zároveň jim dát do "své" technologie potřebný vhled.

Uvádím tenhle typ jen pro úplnost. Většinou je zde někdy tenká, někdy hodně široká a rozmazaná hranice mezi velmi seniorním člověkem a "opravdovým" _technical leaderem_. Osobně jsem takových moc nepotkal. Ale je to validní cesta, pokud se necítíte povoláni k ostatním typům přesahů.

## Architekt

Přesah do architektury je s přibývající senioritou běžný. Jako seniorní vývojář chcete vystrčit hlavu z písku své technické domény a podívat se, co se děje u souseda na dvoře. Časem si možná uvědomíte že mezi vaším (vývojářským) a sousedovým dvorem (architektura) není žádný plot. Jsou to dvě plochy téhož tělesa.

Jako _technical leader_ s přesahem do _architektury_ je vhodné umět navrhnout architekturu a zároveň ji zvládnout kompletně naimplementovat. Nikdy byste neměli z pusy vypustit ohavnou větičku, že něco je "implementační detail". To nechte ivory-tower architektům.

Je vhodné vidět daný software v celosti jeho life-cyclu. Nemít klapky na očích a nesoustředit se jen na aktuální (a nejzajímavější) vývojovou fázi. Jak například vypadají vaše logy? Budou sloužit jen pár měsíců vývojářům, nebo budou _supportu_/_operations_ pomáhat roky na produkci?

Ovšem, není to jen o vás --- je potřeba umět vysvětlit vaše návrhy svým méně seniorním kolegům a také jim pomoci s úseky, které jsou nad jejich síly (technicky, designově, myšlenkově).

## Projekt Manager

Často se setkávám s tím, že vývojáři vlastně neví, za co je platí. Že dostávají peníze za to, že (efektivně) vyřeší nějaký business problém. To je věc, kterou si velmi dobře uvědomuje _project manager_ --- zná finanční rozměr projektu a měl by mít plán, jak to zvládnout.

Jako _technical leader_ s přesahem do _project managementu_ je vhodné umět sestavit implementační plán (v libovolné vyhovující formě). Naplánovat iteraci. Prioritizovat. Zvážit smysluplnost investovaného úsilí. Seřadit a spravovat časové slouslednosti --- aby si vývojáři s tunelovým viděním nestěžovali, že je "něco" blokuje.

A také umět předvídat, odhalovat a mitigovat rizika. Umět řešit a případně eskalovat problémy: chybějící lidi, externí závislosti, nečekané události. Navrhnout a řídit vývojovou metodologii a umět ji podle kontextu modifikovat a přizpůsobit.

## Team Leader

_Team leader_ a _technical leader_ jsou často překrývající se, nebo zaměňované pojmy. Temínem _team leader_ zde myslím toho, kdo pracuje s lidmi, vede tým a má za něj zodpovědnost.

Jako _technical leader_ s přesahem do _team leadingu_ je vhodné... vést lidi. Komunikovat se svým týmem. Řešit jeho problémy a objevovat příležitosti. Mít individuální přístup k jeho členům --- každý z nich je originál. Vystupovat jako [facilitátor](//en.wikipedia.org/wiki/Facilitator).

A nejpodstatnější věc --- mít vizi a udržovat týmovou kulturu. Působit preventivně proti [bad apples](//www.urbandictionary.com/define.php?term=bad%20apple). Vytvořit dobře promazaný stroj, který vyhraje softwarové [play-off](//en.wikipedia.org/wiki/Playoffs).

Budu k vám upřímný. Tenhle přesah je ze všech nejtěžší a nejvíc adeptů na něm ztroskotá. Jde o to, se nepoložit a pokračovat. Zkusit to znova, až to půjde. Ono to půjde.

## Gotický svorník (model re-visited)

V předšlých sekcích jsme naznačili možné přesahy, které může _technical leader_ mít, kromě svého core-skillu. Někdy má přesah jen do jedné z těchto oblastí, někdy do dvou. Nejcennější ale je, pokud má přesah do všech tří domén a působí tak jako svorník klenby, která z těchto pilířů vyrůstá.

{{< figure src="/2017/10/Technical-leader-keystone.png" >}}

V reálním světě měnících se potřeb a požadavků, se ty přesahy budou organicky měnit a přelévat, někdy i během krátké doby (jednoho projektu). I když ale daný skill není zrovna potřeba, pořád obohacuje všechny zbývající aspekty.

## Technical... Leader

Většina adeptů na _technical leadera_ se soustředí na to první slovo v názvu role. Ale to druhé slovo --- _leader_ --- tam není náhodně. Osobně bych řekl, že tenhle aspekt je v celkovém přínosu a smyslu ten důležitější. Protože _technical leader_, který není _leader_ je jenom seniorní vývojář.

Důvod, proč je tato druhá složka opomíjená, je celkem prostý --- techničtí lidé z ní mají obavy. Jak se stanu _leaderem_? Všichni uvidí, že to neumím! A tak se soustředí na to, co už dobře znají... technologie. Komfortní bublina. A možná tak trochu čekají, že ty věci kolem _leadershipu_, tak nějak přijdou samy. Snesou se z modrého nebe jako vavřínový věnec nesený holubicemi...

Ale něco vám řeknu: vy už jste se jako seniorn vývojáři narodili? Že ne? Že to trvalo léta praxe a učení se? Jasně, jasně. Prozradím vám tajemství: nic jako _"natural born leader"_ neexistuje! Vede k němu úplně ta samá cesta, jakou jste už sami ušli. Zadejte si do Googlu heslo [natural born leader](//www.google.com/search?q=natural+born+leader) a budete vědět, na čem ještě potřebujete pracovat. Většinou je to práce na celý život. Ale někdy se začít musí.

## Jediná cesta?

Samozřejmě, že role _technical leadera_ má mnoho podob. Kdybychom chtěli pokračovat v naznačeném modelu, mohli bychom přidat jako další oblast přesahu například _business analýzu_. Nebo _QA_. Nebo... Anebo modelovat/definovat tuhle měňavou roli úplně jinak.

Podstatné je dát vaší roli _technical leadera_ to, co váš kontext potřebuje. Cesty páně jsou nevyzpytatelné a matka příroda je flexibilní a přizpůsobivá. Nechť vás _síla_ na vaší cestě _technical leadera_ provází.

## Doporučená literatura

{{< figure class="floatright" width="150"
    src="/2017/10/Becoming-a-Technical-Leader.png"
    link="/2017/10/Becoming-a-Technical-Leader.png"
>}}

Pokud jste dočetli až sem, tak jste možná opravdoví čtenáři. A ti čtou opravdové knihy. O _technical leadingu_ už pár titulů napsáno bylo. Jestli můžu doporučit jeden z nich, je to kniha [Becoming a Technical Leader](//leanpub.com/becomingatechnicalleader) od Geralda Weinberga ([@JerryWeinberg](//twitter.com/JerryWeinberg)), která je jako truhlice narvaná drahokamy. Některé nedoceníte hned. Jak říká Jerry:

> _"Becoming a technical leader is not something that happens to you, but something that you do."_

## Související články

* [Programátor -> Vývojář -> Software Engineer](/2017/02/programator-vyvojar-software-engineer/)

## Související externí články

* [Technický lídr](//blog.zvestov.cz/software%20development/2015/11/24/technicky-lidr.html) (Banter bloguje)
* [Mýtus nekódujícího architekta](//dagblog.cz/my%CC%81tus-neko%CC%81duji%CC%81ci%CC%81ho-architekta-f2d4c3273382) (Dagblog, TL tématu se dotýká jen okrajově)
