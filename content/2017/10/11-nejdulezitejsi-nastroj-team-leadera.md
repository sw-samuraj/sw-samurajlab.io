+++
title = "1:1, nejdůležitější nástroj team leadera"
date = 2017-10-16T08:30:00Z
updated = 2020-10-09T10:58:36+02:00
description = """Říká se tomu one-on-one. V psané podobě můžete narazit
    na zápis OoO, O-o-O, 1on1 a různé další. Já používám 1:1. Setkal jsem
    se s širokou paletou lidí a jejich zkušeností s 1:1. Jsou tací, kteří
    1:1 nikdy neměli a někdy o něm dokonce ani neslyšeli. Jsou lidi,
    pro které je to jenom takový "manažment folklór". A pak je menšina
    těch, kteří 1:1 očekávají a vyžadují."""
tags = ["teamleading"]
aliases = [
    "/2017/10/11-nejdulezitejsi-nastroj-team-leadera.html"
]
blogimport = true
hero_image = "hero.jpg"
thumbnail = "2017/10/Ladybirds.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure src="/2017/10/Ladybirds.jpg" link="/2017/10/Ladybirds.jpg" >}}

Říká se tomu **one-on-one**. V psané podobě můžete narazit na zápis OoO, O-o-O, 1on1 a různé další. Já používám **1:1**, ale kreativitě se meze nekladou.

Za ta léta, co dělám team leadera, jsem se setkal s širokou paletou lidí a jejich zkušeností s 1:1. Jsou tací, kteří 1:1 nikdy neměli a někdy o něm dokonce ani neslyšeli. Jsou lidi, pro které je to jenom takový "manažment folklór". A pak je menšina těch, kteří 1:1 očekávají a vyžadují.

Jak už jsem psal v minulém článku ([Technical Leader, mytické stvoření](/2017/10/technical-leader-myticke-stvoreni/)), potřebuju uzavřít určité období a shrnout, co jsem se naučil. Tady je moje moudro:

> _"Hlavním nástrojem_ vývojáře _je_ IDE. _Hlavním nástrojem_ Technical Leadera _je_ Issue Tracking System. _A hlavním nástrojem_ Team Leadera _je_ 1:1._"_ ~ SoftWare Samuraj

## Můj příběh

{{< figure class="floatleft" src="/2017/10/One-on-One.png" >}}

Když jsem před nějakými osmi lety začal zvolna aspirovat na roli team leadera (ano, bylo to ještě předtím, než jsem začal psát tenhle blog), šel jsem na to svou obvyklou cestou, která se dá shrnout do tří slov: internet, knihy, praxe.

Hodně mi pomohl Michael Lopp, píšící blog [Rands in Repose](//randsinrepose.com/). Jeho nejlepší články tehdy vyšly v inspirativní knížce [Managing Humans](//www.goodreads.com/book/show/31433313-managing-humans), na kterou [mě upozornil](//blog.krecan.net/2009/06/21/vedeme-lidske-bytosti/) na svém blogu Lukáš Křečan (jak to ten chlap dělá, že je vždycky o pár kroků přede mnou?).

Já sám jsem do té doby nikdy žádné pravidelné 1:1 neměl, jen takové to klasické [performance review](//en.wikipedia.org/wiki/Performance_appraisal). Proto mě šokovalo, že bych měl organizovat 1:1 na týdenní bázi --- neuměl jsem si to představit.

V té době jsem v někdejší firmě převzal/zdědil _Java kompetenci_. V reálu to znamenalo, že všichni seniornější Javisti už z firmy odešli. Nebylo od koho se učit, tak jsem si roli definoval sám. Naplánoval jsem si s klukama semestrální 1:1 a protože byli rozeseti na projektech po celé Praze, chodil jsem tam za nima.

Dalším milníkem bylo angažmá, kde došlo k nepřátelskému převzetí projektu (lehce jsem se tématu dotknul v článku [(Ne)funkční tým](/2011/11/nefunkcni-tym/)). Pořád ještě jako juniorní team leader jsem bojoval, jak to šlo, podle svých tehdejších schopností. Jedna z věcí, které jsem zkusil a která byla dobrým krokem, bylo zavedení 1:1. Scházeli jsme se jednou za 1/4 roku.

A pak přišel další projekt --- dostal jsem v kulminaci nálož 16 lidí (naštěstí to rostlo postupně). Opět jsem sáhl po knize (článek [Management za zavřenými dveřmi](/2012/09/management-za-zavrenymi-dvermi/)) a opět jsem zredukoval periodu --- scházeli jsme se na 1:1 jednou za měsíc.

{{< figure class="floatleft" src="/2017/10/Managing-Humans.jpg"
    link="/2017/10/Managing-Humans.jpg" width="140" >}}
{{< figure class="floatleft" src="/2017/10/Five-Dysfunctions-of-a-Team.jpg"
    link="/2017/10/Five-Dysfunctions-of-a-Team.jpg" width="140" >}}
{{< figure class="floatleft" src="/2017/10/Behind-the-Closed-Doors.jpg"
    link="/2017/10/Behind-the-Closed-Doors.jpg" width="140" >}}

Ano, už to přijde. Pointa. Ale tentokrát bez knihy. Když jsem nastupoval do nové práce, měl jsem neobvyklý luxus --- mohl jsem si postavit svůj vlastní tým, úplně od začátku. S tímhle novým týmem jsem začal dělat to, co se všude radí bez výjimky --- týdenní 1:1. Dělám to tak už téměř pět let a můžu jen konstatovat, že **1:1 je ten nejdůležitější nástroj, který team leader má**.

## Vize, benefit, smysl

1:1 je nástroj. Je to nástroj používaný mezi dvěma entitatmi --- mezi team leaderem a členem týmu, ev. mezi manažerem a reportujícím. Aspektů, které tento nástroj může přinést je mnoho, ale já bych vypíchnul čtyři, které považuji za nejhodnotnější.

* 1:1 je silný nástroj pro _vytváření vztahu_ mezi team leaderem a členem týmu.
* 1:1 kritický nástroj pro _budování důvěry_.
* 1:1 je také nástroj na vytváření a ovlivňování _týmové kultury_.
* A 1:1 je prostor a čas, který team leader dedikuje svým lidem.

Nejkritičtějším elementem je _budování důvěry_ --- pokud ve vztahu chybí důvěra, je těch benefitů jenom polovina (a míň).

## Pravidla

Každý si umí představit analogii s autem, které potřebuje pravidelný servis, aby dobře fungovalo. Pokud ho zanedbáte (nebo už se to nevyplatí), tak se to nějak promítne, nastřádá. O 1:1 to platí také. A možná i víc, protože lidi nejsou stroje. Tady je pár pravidel, která vám můžou pomoct udržet 1:1 v dobrém stavu.

### Pravidelnost

Běháte? Chcete uměhnout maraton (5k, 10k, 1/2 maraton)? Je potřeba, abyste pravidelně trénovali. Učíte se cizí jazyk? Je potřeba, abyste ho pravidelně procvičovali. Učíte se nový programovací jazyk/framework/technologii? Je potřeba je pravidelně používat. Jinak to nemá moc smysl a přínos je malý.

S 1:1 je to stejný. Jak říkal soudruh Lenin: pravidelnost, pravidelnost, pravidelnost. Rytmus je benefitem i podpůrnou konstrukcí.

### Nezrušitelnost

Nikdy 1:1 nerušte. Nastavte si to jako železné pravidlo --- jestli jste team leader, 1:1 je důležitejší než jakýkoli projekt. Projekt skončí, lidi vám zůstanou.

Když říkám _nikdy_, jsem si vědom absolutnosti toho slova. Já sám se snažím pít vodu, když kážu víno. Snažím se tomu ideálu aspoň přiblížit. Osobně to mám nastavené tak, že 1:1 ruším jenom když jsem (a) nemocný, (b) na dovolený, \(c) na služební cestě. Pokud se něco kritického vyskytne v práci, snažím se 1:1 místo zrušení aspoň přeplánovat.

Rušení 1:1 má ještě jeden důležitý aspekt, ke kterému se vrátím v sekci _Doporučení_.

### Respekt a vychování

Ono to přímo vyplývá ze zmíněného axiomu, že 1:1 je nástroj na vytváření vztahu --- chovejte se ke členům svého týmu s respektem. Kromě běžné sociální slušnosti, bych vypíchnul reakci na vyrušení.

Na 1:1 neberte _nikomu_ telefon. Nekontrolujte zprávy. Zdvořile odražte osobní vyrušení. Pokud vaše žena nejede do porodnice, tak vás nic neomlouvá, že se nevěnujete na 100 % člověku, kterého máte před sebou.

Mě když volá žena během 1:1 schůzky... típnu jí telefon. A zavolám potom. Když mi během 1:1 volá šéf, nezvednu mu to. Když šéf během schůzky vpadne do místnosti, řeknu že mám 1:1 a zastavím se potom. Za prvý jsem v právu a za druhý, je to slušnost.

### Naslouchejte

Lidé ve vedoucích rolích mají často nepříjemný zlozvyk: moc mluví. I já --- jako introvert --- tím dost trpím a musím si to neustále (už po léta) připomínat. Někdy se mi to i daří. **Naslouchejte**. Poslouchejte, co vám kolegové chtějí říct. Když už máte jejich důvěru, tak ji nezahoďte.

### Technology free

Předpokládám, že tohle hodně lidí šmahem odmítne, ale stejně to řeknu. Na 1:1 choďte _bez technologií_. Poznámky si pište na papír. Psát si během 1:1 na počítači --- s libovonými bohulibými úmysly --- je nevychované a neosobní. Pamatujete? Respekt a budování vztahu.

Pokud opravdu potřebujete mít vaše poznámky archivované na počítači, je to jenom vaše pohodlnost, co snižuje kvalitu komunikace.

Samozřejmě, pokud potřebujete svému kolegovi něco na počítači ukázat, je to v pořádku. Pokud ale před ním frontálně sedíte s počítačem před sebou, je to chucpe.

## Obsah

Lidé, kteří se setkávají s 1:1 poprvé, můžou váhat, co je obsahem. Samozřejmě, je to primárně pracovní záležitost. Ale je v pořádku probírat i osobní a rodiné záležitosti. Jak tedy obsah 1:1 definovat?

Ještě než se přesunu k lehce konkrétním bodům, zmíním dva stěžejní principy:

1. **Nechte své kolegy spolu-vytvářet obsah 1:1.** Ono i když se podíváte na ten zápis --- 1:1 --- mělo by to evokovat fifty-fifty, či win-win. Jsou věci, o kterých nemáte ani tušení, že by je vaši členové týmu chtěli řešit.
1. **Přizpůsobte 1:1 každemu člověku.** Lidé jsou různí --- mají různé povahy, senioritu, externí a životní zkušenosti, sociální a rodinný status. Můžou pocházet z jiných kultur a světadílů. Respektujte to a reflektujte to.


### Pracovní témata

Pracovním tématem číslo jedna jsou problémy. Technologické problémy, projektové problémy, problémy ve vztazích v práci. To je to, co vaše lidi nejvíc trápí. Externí záležitosti vyřešit nemůžete, ale můžete aspoň svého kolegu vyslechnout a nabídnout empatii.

Interní problémy jsou dvojího druhu --- ty, které daný člověk může vyřešit sám. Možná mu k tomu chybí odvaha, možná neví jak, možná jen potřebuje cítit vaši podporu. A pak interní problémy, které on vyřešit nemůže, ale můžete je vyřešit vy... protože jste (jeho) team leader. Tohle výživné téma by vydalo nejméně na článek --- klíčové slovo: coaching.

Tématem číslo dva jsou úšpěchy. I drobné. Co se vašemu kolegovi povedlo? V čem se zlepšil? Co se naučil? Není u toho potřeba používat vyprázdněné superlativy, méně je více. Ale nemělo by to zapadnout.

Téma číslo tři: rozvoj. Co dělá váš kolega navíc, nad rámec "daily job"? Čte knihy? Zajímavý článek na internetu? Osobní projekt? Samozřejmě sem spadají i školení a podobné věci, i když ty se neplánují každý týden.

Téma číslo čtyři (záměrně není v TOP 3): něco jako "mikro-status". Záleží na kontextu. Pokud kolegu potkáváte na denním standupu, asi to nebude tak podstatný, možná i zbytečný. Pokud dělá na jiném projektu, hodí se vědět, jak vypadá aktuální projektový/technologický "snapshot". Bacha na micro-management!

### Osobní témata

Asi je jasný, co se tím míní. Rozsah a hloubka se budou lišit od kvality vztrahu a míry důvěry. Já si běžně se členy týmu povídám o sportu, dovolený, rodině, koníčkách apod. Jde hlavně o to, co jste ochotni prozradit vy. (Jenom to nepřežeňte.)

> _"The most important action that a leader must take to encourage the building of trust on a team is to demonstrate vulnerability first."_ ~ Patrick Lencioni

## Doporučení

Na závěr ještě pár doporučení, která rozvíjejí témata, která už zazněla.

Pokud vše dobře půjde, budete mít 1:1 s daným člověkem léta. Jak váš vzájemný vztah, tak vaše 1:1 by se mělo vyvíjet. Neupadněte do rutiny. Adaptujte (se). Změňte vaše individualizované 1:1 tak, aby to stále byl oboustranně prospěšný nástroj.

Jak už jsem naťuknul --- neřešte problémy vašich lidí za každou cenu. Buďte umírnění a nechte je vyřešit maximum problémů, které zvládnou sami. Podpořte je, když selžou. Bez chyb není učení.

Vyhraďte si na 1:1 poznámky dedikovaný sešit. Budete mít historii na jednom místě.

A zlaté doporučení. **Pokud vaši lidé odcházejí (z firmy)** --- stává se to --- **nerušte jim 1:1, ale pokračujte s nimi až do konce.** Ukážete jim, že vám na nich i tak pořád záleží. V opačném případě budou mít pocit, že jste je hodili přes palubu.

## A ještě jedno doporučení

_[Update 9. 10. 2020]_

Když jsem si tenhle článek po 3 letech znovu přečetl, přišel mi tak dobrý,
že jsem ho znovu za-anoncoval na Twitteru. Dobře jsem udělal, protože mi
přišla reakce, kterou trochu rozvedu, protože za to stojí a do tohohle
článku patří.

{{< tweet 1314233804136087552 >}}

To doporučení zní --- _pokud můžete, dělejte 1:1 mimo pracovní prostředí_.

Pokud se tak zařídíte, má to minimálně tři benefity. Ten první zmiňuje
Jakub --- setkání bude více neformální, což 1:1 jednoznačně prospívá.

Druhý benfit je více fundamentální --- tím že změníte prostředí (např.
vyměníte klasickou zasedačku za lavičku v parku), změníte kontext,
ve kterém se rozhovor odehrává. Lidi začnou přemýšlet a chovat se jinak.
To je také ku prospěchu. Najednou se začnete dívat na témata a problémy
z jiného úhlu. Prostředí velmi ovlivňuje, jak lidé řeší problémy.

A za třetí, narušíte tím rutinu. Pokud se scházíte po celý rok ve stejný
čas na stejném místě, budete mít tendenci přenášet rutinu i do agendy 1:1.
Vyhněte se tomu.

Samozřejmě, čekám klasickou námitku: "ale to nejde...". Ale jde. Možná
nebudete mít to štěstí jako já, kdy jsem v munulé práci bral členy svého
týmu do přilehlého parku s fontánou. Anebo v současnosti --- když se role
obrátily --- mám 1:1 se svým šéfem tak, že si jdeme zaběhat do Prokopského
údolí.

Běžte třeba do blízké kavárny. Obejděte svůj office-building. Běžte
na terasu své kanceláře. Však vy na něco přijdete. 👍

_[/Update]_

## Mind Map

{{< figure src="/2017/10/One-on-One-mind-map.jpg"
    link="/2017/10/One-on-One-mind-map.jpg" >}}

## Související články

* [Manažerem humorně a kousavě](/2011/07/manazerem-humorne-a-kousave/)
* [(Ne)funkční tým](/2011/11/nefunkcni-tym/)
* [Management za zavřenými dveřmi](/2012/09/management-za-zavrenymi-dvermi/)
