+++
title = "Trampoty s JUnit 5"
date = 2017-11-01T23:02:00Z
updated = 2018-07-21T12:58:01Z
description = """Poslední dobou jsem moc nepsal unit testy v Javě. Jednak
    jsem posledního půl roku hodně prototypoval a když už jsem testy psal,
    tak to bylo převážně ve Scale, nebo v Clojure. Ale teď se naskytla
    příležitost ošahat si nové JUnit 5."""
tags = ["gradle", "testing", "build", "maven", "java"]
aliases = [
    "/2017/11/trampoty-s-junit-5.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/img/logos/JUnit-5.png" width="100" >}}

Poslední dobou jsem nepsal moc unit testy... v Javě. Jednak jsem posledního půl roku hodně prototypoval --- a tam moc testů nenapíšete --- a když už jsem testy psal, tak to bylo převážně ve _Scale_, nebo v _Clojure_.

Teď ale naše firma projevila sklony k evoluci, se snahou trochu více zautomatizovat vytváření prostředí a zakládání projektů. Sice to jde mimo mě, ale když jsem byl požádán, ať napíšu testovací projekty v Javě pro _Gradle_ a _Maven_, chopil jsem se příležitosti a ponořil se do (povrchního) studia [JUnit 5](//junit.org/junit5/).

## Vyznání

Obecně musím říct, že pro _JUnit_ mám slabost --- začal jsem ho používat na začátku své Java kariéry ve verzi 4.2 (pro pamětníky únor 2007) a tak vlastně celý můj Java-produktivní věk jsem strávil se čtyřkovou verzí. Naučilo mě to hodně --- za to, že jsem dnes takový skvělý programátor (ha, ha, ha) vděčím tomu, že mě unit testy naučily psát dobrý design.

Samozřejmě jsem si k tomu občas něco přibral. Už v roce 2008 jsem si mistrně osvojil (tehdy progresivní) [jMock 2](//www.jmock.org/) a naplno se oddával neřesti BDD. Taktéž [TestNG](//testng.org/) jsem si na pár projektech zkusil. Ale gravitační síla tradičního JUnit a TDD mě vždy přivedla zpátky.

A teď zažívám něco jako [déjà vu](//en.wikipedia.org/wiki/D%C3%A9j%C3%A0_vu). Je to podobný, jako když přišla _Java 5_ --- skoro všechny nástroje s tím mají menší nebo větší problém. Java komunita to ještě moc neadaptovala. Když narazíte na problém, [StackOverflow](//stackoverflow.com/) často nepomůže. Atd.

{{< tweet 923451977085997056 >}}

Pár aktuálních problémů JUnit 5 se mi podařilo vyřešit ke své spokojenosti. Tady je máte na stříbrném podnose.

## Zadání

Zadání, které jsem dostal, bylo triviální --- napsat miniaturní Java projekt, buildovatelný [Gradlem](//gradle.org/) a [Mavenem](//maven.apache.org/), který bude mít unit testy. Projekt se bude buildovat na [Jenkinsu](//jenkins.io/), potřebuje změřit pokrytí testy pomocí [JaCoCo](//www.jacoco.org/jacoco/) a projít statickou analýzou kódu na [SonarQube](//www.sonarqube.org/).

Jak říkám, bylo by to triviální, kdybych si pro testy nevybral [JUnit 5](//junit.org/junit5/).

## Gradle

{{< figure class="floatright" src="/img/logos/Gradle.png" width="100" >}}

Odpírači pokroku a milovníci XML se mnou nebudou souhlasit, ale já považuju _Gradle_ za základ moderní automatizace na JVM. Včetně (a primárně) buildů. Jak tedy zkrotit _Gradle_, aby se kamarádil s _JUnit 5_?

Zatím jsem se v tom nějak moc nevrtal, pač nemám ambice se stát _JUnit 5_ guru, jen potřebuju běžící testy. Ale je dobré vědět, že:

> **JUnit 5 = JUnit Platform + JUnit Jupiter + JUnit Vintage** ([JUnit 5 User Guide](//junit.org/junit5/docs/current/user-guide/#overview-what-is-junit-5))

_JUnit Vintage_ je pro _JUnit 4_, což nás dnes nezajímá. Zbývá tedy _JUnit Platform_ pro spouštění unit testů a _JUnit Jupiter_ pro samotné psaní testů.

Protože _JUnit 5_ změnilo pravidla hry, nestačí do _Gradlu_ jenom přidat závislosti --- současný _Gradle_ novým unit testům nerozumí a zůstaly by nepovšimnuty. Naštěstí je k dispozici je nový plugin, který přidá do build life-cyclu nový task `junitPlatformTest` a který umí testy spustit.

Bohužel, plugin ještě pořád není dostupný na [Gradle Plugins Portal](//plugins.gradle.org/), ale jen v [Maven Central](//search.maven.org/). Tím pádem se zatím nedá použít [Plugins DSL](//docs.gradle.org/current/userguide/plugins.html#sec:plugins_block) :-(

V následujícím minimalistickém _Gradle_ skriptu si povšimněte různých konfigurací pro jednotlivé závislosti:

* `testCompile` pro `api`
* `testRuntime` pro `engine`.

{{< gist sw-samuraj d05971ed357d74ec5b33b607df33780d >}}

Závislost `apigurdian-api` je optional a je tam jenom proto, aby se ve výstupu nevypisovalo varování:

    warning: unknown enum constant Status.STABLE
        reason: class file for org.apiguardian.api.API$Status not found

Task `juPlTe` je "zahákovaný" na standardní `test` task, který se dá použít také.

{{< figure src="/2017/11/JUnit-5-Gradle-result.jpg" caption="Spuštění JUnit 5 testů Gradlem" >}}

Jedna z [killer feature](//en.wikipedia.org/wiki/Killer_feature) _Gradlu_ je [incremental build](//docs.gradle.org/current/userguide/more_about_tasks.html#sec:up_to_date_checks) --- pokud nešáhnete na produkční kód, nebo na testy, _Gradle_ testy nespouští. Je prostě chytrej ;-)

{{< figure src="/2017/11/JUnit-5-Gradle-incremental-build.jpg"
    caption="Gradle incremental build přeskočí testy, pokukd se kód nezměnil" >}}

## Maven

{{< figure class="floatright" src="/img/logos/Maven.png" width="200" >}}

Tradicionalisti milují _Maven_ a protože jsem shovívavý lidumil, podělím se i o toto nastavení. Pro _Maven_ platí totéž, co pro _Gradle_:

* nový plugin (přesněji [Surfire provider](//maven.apache.org/components/surefire/maven-surefire-plugin/examples/providers.html))
* závislost na `api` a `engine` v různém scopu

{{< gist sw-samuraj 41d2f6e4779bc712e240f42c96c79989 >}}

Velký rozdíl mezi _Mavenem_ a _Gradlem_ je, že _Maven_ [incremental build](//docs.gradle.org/current/userguide/more_about_tasks.html#sec:up_to_date_checks) (moc dobře) neumí --- tupě spouští testy, kdykoliv mu řeknete.

{{< figure src="/2017/11/JUnit-5-Maven-result.jpg" caption="Spuštění JUnit 5 testů Mavenem" >}}

## JaCoCo pokrytí testy

Zbuildovat a spustit _JUnit 5_ testy byla ta jednodušší část. S čím jsem se trochu potrápil a chvilku jsem to ladil, bylo pokrytí testy. Vybral jsem [JaCoCo](//www.eclemma.org/jacoco/), protože mi vždycky přišlo progresivnější, než [Cobertura](//cobertura.github.io/cobertura/) (jen takový pocit, či preference).

Dále budu uvádět jen nastavení pro _Gradle_, protože _Maven_ je hrozně ukecaný. Pokud vás ale _Maven_ (ještě pořád) zajímá, podívejte se do [pom.xml](//bitbucket.org/sw-samuraj/blog-junit5/src/HEAD/pom.xml?fileviewer=file-view-default) v projektové repository.

Zkrácená _JaCoCo_ konfigurace vypadá takto:

{{< gist sw-samuraj f245e6c8a05ea07d8d2bf513ff145bd1 >}}

V předešlém výpisu jsou podstatné tři věci: (1) generování _JaCoCo_ [destination file](//www.jacoco.org/jacoco/trunk/doc/agent.html) je svázáno s taskem `junitPlatformTest`. (2) Definujeme název _destination file_. Název může být libovolný, ale aby fungovalo generování _JaCoCo_ reportů, je potřeba, aby se soubor jmenoval `test.exec`. A za (3), pokud chceme některé soubory z reportu exkludovat, dá se to udělat trochu obskurně přes life-cycle metodu `afterEvaluate`. (Tohle by chtělo ještě doladit.)

{{< figure src="/2017/11/JUnit-5-JaCoCo.jpg" caption="JaCoCo pokrytí testy" >}}

## SonarQube statická analýza

_Sonar_ vlastně s _JUnit_ nesouvisí. Pokrytí testy už máme přece vyřešeno. No, uvádím to proto, že opět je potřeba jít tomu štěstíčku trochu naproti.

Zkrácená verze _Sonar_ konfigurace je následující (_Maven_ opět hledejte v [repo](//bitbucket.org/sw-samuraj/blog-junit5/src/HEAD/pom.xml?fileviewer=file-view-default)):

{{< gist sw-samuraj b005831405c83e2903d452d784ce11e6 >}}

Tady jsou důležité dvě věci: (1) říct _Sonaru_, kde má hledat coverage report (klíč `sonar.jacoco.reportPath`) a za (2) naznačit, co má _Sonar_ z coverage ignorovat (`sonar.coverage.exclusions`) --- bohužel, _JaCoCo _exkluduje jenom z reportu, v _destination file_ je všechno a tak to _Sonaru_ musíte říct ještě jednou.

{{< figure src="/2017/11/JUnit-5-SonarQube.jpg" caption="SonarQube statická analýza" >}}

## Má smysl migrovat?

Jak je vidět, popsal jsem spoustu papíru, není to úplně [easy peasy](//www.urbandictionary.com/define.php?term=Easy%20Peasy). A tak se nabízí hamletovská otázka: má smysl upgradovat z _JUnit 4_ na verzi _5_?

{{< twitter 925023042908098560 >}}

Výše už jsem zmínil velmi přesnou analogii s _Javou 5_. Tehdy šlo hlavně o _anotace_ a _generické kolekce_. Můj povrchní dojem je, že u _JUnit 5_ může být tahákem _Java 8_ (na nižších verzích Javy to neběží), takže primárně [lambdy](//docs.oracle.com/javase/tutorial/java/javaOO/lambdaexpressions.html) a [streamy](//www.oracle.com/technetwork/articles/java/ma14-java-se-8-streams-2177646.html).

Pokud máte stávající code base slušně pokrytou pomocí _JUnit 4_, tak se migrace nevyplatí. Protože ale _JUnit 5_ umí (pomocí [JUnitPlatform runneru](//junit.org/junit5/docs/current/user-guide/#running-tests-junit-platform-runner)) spouštět obě verze simultánně, je možné na verzi _5_ přecházet inkremenálně.

## Projekt repository

Na Bitbucket jsem nahrál repozitory jednoduchého projektu, kde si můžete v _Gradlu_ a v _Mavenu_ spustit _JUnit 5_ testy, vygenerovat _JaCoCo_ report a publikovat výsledek do _SonarQube_.

* [sw-samuraj/blog-junit5](//bitbucket.org/sw-samuraj/blog-junit5)
