+++
title = "Covariance & Contravariance"
date = 2017-05-29T22:36:00Z
updated = 2017-05-30T11:12:35Z
description = """Variance je obecný pojem, který říká, jakým způsobem funguje
    subtyping u komplexních typů a může být trojího druhu: Invariance, Kovariance
    a Kontravariance. Jak to funguje pro pole a kolekce v Javě a ve Scale?"""
tags = ["scala", "java"]
aliases = [
    "/2017/05/covariance-contravariance.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2017/05/Computer-Science.jpg"
    src="/2017/05/Computer-Science.jpg" width="180" >}}

Myslím, že je dost pravděpodobné, že na univerzitě jste měli nějaký ten semestr statistiky (já jsem měl 4 😱) a tak by vám měl být aspoň lehce povědomý termín [kovariance](//en.wikipedia.org/wiki/Covariance), který je vyjádřený vztahem:

> `cov(X, Y) = E[(X - E[X])(Y - E[Y])]`

**Tak o tom si dnes povídat nebudeme.**

Místo toho bych si chtěl rozebrat téma, které jsem dostal jako otázku na nedávném pohovoru. (Jo, to byl ten pohovor, kvůli kterému jsem napsal článek [CAP Theorem](//www.sw-samuraj.cz/2017/04/cap-theorem.html), takže to už je druhý zářez na pažbě.)

Když jsem tedy dostal otázku, jestli vím, co je [kovariance a kontravariance](//en.wikipedia.org/wiki/Covariance_and_contravariance_(computer_science)), tak u prvního termínu mi zablikala kontrolka "mlhavé vzpomínky" (léta nepoužívaná statistika se projeví "určitým povědomím neurčitosti") a pak jsem na férovku přiznal, že termín _kontravariance_ jsem nikdy neslyšel a nevím, co to je.

**Poslední varování!** Pokud vás neodradila rovnice v úvodu ani vás neodežene tento odstavec, vězte, že se dozvíte o temném zákoutí _computer science_, které vám asi bude prakticky k ničemu --- já jsem tuto "záležitost" za 12 let v Javě nepoužil ani jednou. Na druhou stranu, je to aspekt, který je ve vašem jazyce trvale přítomen a dost možná jsou vám jeho praktické konsekvence známé.

## Podivný případ s Array

Než se do toho pořádně pustíme, zkuste se podívat na následující kód v Javě, který se bez problémů zkompiluje. Co je na tom špatně a co se stane, když ho spustíme?

{{< gist sw-samuraj e813bda5346946c2615660efc3153fe8 >}}

Asi jste správně odhalili, že na 9. řádku micháme jablka s hruškama. Ehm, tedy Jedie se Sithama. Možná už ale nevíte, že zmíněný řádek vyhodí runtime výjimku [ArrayStoreException](//docs.oracle.com/javase/8/docs/api/java/lang/ArrayStoreException.html).

Pokud bychom se podívali na ekvivalentní příklad ve Scale, tak se nám následující kód ani nezkompiluje, protože vyhodí kompilační chybu už na řádku 8.

{{< gist sw-samuraj de7bf21798ebb051ad80ce03145e9b4f >}}

To jsou věci! Vítejte ve světě _variance_.

## 3 sestry: Kovariance, Kontravariance a Invariance

Výše uvedené příklady demonstrují, že pole jsou v Javě _kovariantní_ a ve Scale _invariantní_. Pojďme si na to posvítit. Všechno se to motá kolem pojmů [subtyping](//en.wikipedia.org/wiki/Subtyping) (pozor, neplést s [dědičností](//en.wikipedia.org/wiki/Inheritance_(object-oriented_programming))) a _variance_. Takže. 

**Variance** je obecný pojem, který říká, jakým způsobem funguje subtyping u komplexních typů. Komplexním type je třeba generická kolekce, nebo funkce. Variance může být trojího druhu:

* **Invariance** říká, že mezi komplexními typy není žádný vztah.
* **Kovariance** umožňuje nahradit komplexní typ jeho podtypem.
* **Kontravariance** umožňuje nahradit komplexní typ jeho nadtypem.

Pro úplnost je potřeba říct, že je tady ještě čtvrtá sestra: _bivariance_. Ale protože ji macecha ráda neměla, tak se s ní nebudeme zabývat. 

## Variance v kolekci

Pokud se vrátím k výše uvedenému příkladu v Javě --- proč je pole kovariantní, když to pak působí problémy? Důvod je historický. Pole byla jedna z prvních kolekcí, které Java měla a jak ti starší z nás zažili, až do verze 5, neměla Java [generika](//en.wikipedia.org/wiki/Generics_in_Java). (Mimochodem, jeden z lidí, kteří generiky do Javy přidávali, byl [Martin Odersky](//en.wikipedia.org/wiki/Martin_Odersky), autor Scaly.)

Pole v Javě byly navrženy, aby podporovaly pole objektů a protože každá třída automaticky dědí z [Object](//docs.oracle.com/javase/8/docs/api/java/lang/Object.html), tak jsou pole kovariantní. S příchodem generik to už nedává smysl a tak máme v Javě takovou dichotomii --- pole jsou kovariantní (viz příklad nahoře), kdežto generické kolekce jsou invariantní.

Jak taková invariance v kolekci vypadá? Podívejme se na následující příklad seznamu v Javě:

{{< gist sw-samuraj 7f13a08bc83eb20bbe655b0fb738b4dd >}}

Teď už by mělo být zřejmé, že ačkoliv typy v kolekci podporují subtyping (např. `Jedi` &rarr; `Powerful`), tak kolekce samotné jsou invariantní: seznam `List<Jedi>` není podtypem `List<Powerful>`.

## Immutable kolekce

Jak zhruba říká Martin Odersky (tady hodně zjednodušuji), _mutable_ kolekce by měly být invariantní, zatímco _immutable_ kolekce můžou být kovariantní.

Kovariantní seznam si můžeme hezky ukázat ve Scale. Jelikož [List](//www.scala-lang.org/api/current/scala/collection/immutable/List.html) je ve Scale _immutable_ (ve skutečnosti je to klasická funkcionální struktura [cons](//en.wikipedia.org/wiki/Cons#Lists) buněk). Právě immutabilita nám zajistí, že nás nepotká podobné runtime překvapení, jako u Java polí.

{{< gist sw-samuraj d4e82eebfb08e8df43d7a5604918c290 >}}

Tam, kde se nám Scala u polí bouřila (řádek 8), tak u seznamu ani necekne. Prostě kovariantní pohodička.

## Jakou varianci má immutable Java?

Když se člověk dívá, jakou pěknou implementaci immutable kolekcí má Scala, tak ho napadne, jestli jsou také kovariantní immutable kolekce v Javě. Zklamu vás... nejsou.

V první řadě, immutable kolekce v Javě vůbec nejsou. To nejbližší, co se dá v Java kolekcích najít je [Collections.unmodifiableList](//docs.oracle.com/javase/8/docs/api/java/util/Collections.html#unmodifiableList-java.util.List-) (ev. `Map`, `Set`, `Collection` atd.), což je ale jenom read-only pohled na interní, mutable list. Který je invariantní.

Pokud se podíváme na externí frameworky, immutable kolekce nabízí Google [Guava](//github.com/google/guava). Když se na ně ale zaměříme z hlediska variance, tak jsou opět invariantní. Navíc, ošklivost builderu pro přidání elementu do kolekce se s funkcionální krásou [cons](//www.scala-lang.org/api/current/scala/collection/immutable/$colon$colon.html) nedá srovnávat:

{{< gist sw-samuraj d451f8402e48d9f7a8e744602e5099e7 >}}

## To je všechno? Co funkce?

Povídání o varianci v kolekcích se nám trochu protáhlo, takže se nedostalo na to nejzajímavější --- variance ve funkcích. Jako opravdu ve funkcích --- Java se nám vrabčími krůčky přibližuje funkcionálnímu programování, takže bychom toto téma neměli minout.

## Související články

* Covariance & Contravariance II: Funkce (TBD)
