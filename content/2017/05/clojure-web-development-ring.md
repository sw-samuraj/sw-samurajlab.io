+++
title = "Clojure web development: Ring"
date = 2017-05-01T21:38:00Z
updated = 2017-07-11T13:53:30Z
description = """Webový vývoj v Clojure je dobře etablovaný. Nebylo by to
    ale Clojure, kdyby si věci nedělalo trochu po svém. Dnes se podíváme
    jak pořešit esenci webového vývoje - HTTP request a response."""
tags = ["clojure"]
blogimport = true 
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://www.blogger.com/profile/12147991495120816879"
+++

{{< figure class="floatleft" src="/2017/05/Ring.png" width="200" >}}

Webový vývoj v [Clojure](//clojure.org/) je dobře etablovaný. Nebylo by to ale Clojure, kdyby si věci nedělalo trochu po svém. A tak nabízí, místo rozsáhlých aplikačních frameworků, množinu knihoven, které se dají pospojovat dohromady. Trochu to připomíná unixovou filozofii --- malé, jednoúčelové prográmky, které lze propojovat do komplexnějších řešení.

Když jde o web, tak jde v první řadě o [HTTP](//en.wikipedia.org/wiki/Hypertext_Transfer_Protocol). Clojure na to jde od podlahy a jeho odpovědí je [Ring](//github.com/ring-clojure/ring) --- "Clojure HTTP server abstraction". ~~Možná~~ teď nebudu úplně přesný: Ring je Clojure ~~implementací~~ abstrakcí HTTP protokolu a zároveň je částečně kompatibilní s Java [Servlety](//docs.oracle.com/javaee/7/tutorial/servlets.htm).

## Ring vládne čtyřem komponentám

Každá Ring aplikace se skládá ze čtyř základních částí:

* **Handler** --- funkce, která přijímá mapu reprezentující HTTP request a vrací mapu představující HTTP response.
* **Request** --- mapa reprezentující HTTP request, která obsahuje sadu "standardních" klíčů, které nám budou povědomé ze Servletu: `:server-port`, `:server-name`, `:remote-address`, `:request-method` ad.
* **Response** --- mapa, představující HTTP response, která obsahuje tři klíče: `:status`, `:header`, `:body`.
* **Middleware** --- funkce, která handleru přidá dodatečné funkcionality. Věci jako session, cookies, či parametry jsou řešené middlewarem.

## Ring Hello, world!

Začneme tradičně, vytvořením [Leiningen](//github.com/technomancy/leiningen) projektu:

    $ lein new blog-ring

a v souboru `project.clj` doplníme dependency na Ring:

{{< gist sw-samuraj 01b42675eff0239a7eade0dec6348126 >}}

Dále upravíme soubor `src/blog_ring.core.clj`, aby obsahoval náš _Hello, world_ handler. Přidáme také funkci `-main`, abychom mohli aplikaci rovnou spustit:

{{< gist sw-samuraj 4b6a464de41a79d1fd3cd833bfe129c4 >}}

Aplikaci spustíme příkazem:

    $ lein run

a můžeme si ji prohlédnout v browseru na URL [http://localhost:3000](//localhost:3000/).

{{< figure src="/2017/05/Ring-hello-world.png" >}}

## Request

Práce s requestem je přímočará --- request je v Ringu prezentován mapou, takže stačí pomocí klíče vytáhnout požadovanou hodnotu a nějak ji zpracovat.

{{< gist sw-samuraj 329ff44bd6332cc47ba41da36d9772f1 >}}

Předešlý handler nám vrátí následující stránku:

{{< figure src="/2017/05/Ring-request.png" >}}

## Response

V _hello world_ příkladu jsme si celou response mapu sestavili sami. Ring nabízí řadu funkcí, soustředěných v namespace [ring.util.response](//ring-clojure.github.io/ring/ring.util.response.html), které práci s response usnadňují:

{{< gist sw-samuraj c9a00526e7af776c6c4e0596cedbb96c >}}

## Middleware

Tak, to bylo triviální. Co si teď střihnout nějaký middlewérek? Middleware je, podle definice, _"[funkce vyššího řádu](//en.wikipedia.org/wiki/Higher-order_function), která přidává handleru dodatečné funkcionality. Prvním argumentem midddleware funkce je handler a její návratovou hodnotou je nový handler, který bude volat handler původní."_

Seznam [standard middleware](//github.com/ring-clojure/ring/wiki/Standard-middleware) funkcí se dá najít na wiki stránce Ringu, nebo na [stránce API](//ring-clojure.github.io/ring/index.html) (všechno co je `ring.middleware`).

Pro potřebu článku jsem si vybral dva middlewary:<br />

* [wrap-params](//ring-clojure.github.io/ring/ring.middleware.params.html#var-wrap-params), který rozparsuje parametry z formuláře a query stringu a přidá do requestu klíče `:query-params`, `:form-params` a `:params`
* [wrap-keyword-params](//ring-clojure.github.io/ring/ring.middleware.keyword-params.html#var-wrap-keyword-params), který zkonvertuje stringové klíče v mapě `:params` na keyword klíče (protože všichni přece máme rádi keyword klíče v Clojure mapách).

Middleware funkce se dají dobře testovat pomocí funkce [identity](//clojuredocs.org/clojure.core/identity), kdy není potřeba funkci podstrčit celý handler a následně request, ale jen část (request) mapy, která nás z hlediska middlewaru zajímá.

Tady je ukázka, co s requestem dělají uvedené funkce [wrap-params](//ring-clojure.github.io/ring/ring.middleware.params.html#var-wrap-params) a [wrap-keyword-params](//ring-clojure.github.io/ring/ring.middleware.keyword-params.html#var-wrap-keyword-params):

{{< gist sw-samuraj af6ee199af9b0b12477595f8a54f94a5 >}}

Jak naznačuje definice, midddlewary jsou navržený tak, aby šly pospojovat za sebe. Je to v podstatě analogie [Java (servlet) filtrů](//docs.oracle.com/javaee/7/api/javax/servlet/Filter.html). Jak middlewary zřetězíme? Můžeme funkce jednoduše zanořit do sebe. Ale čitelnější, i používanější, je použití [thread-first](//clojure.org/guides/threading_macros) makra:

{{< gist sw-samuraj 610df90293dae30939f0b04103a9358d >}}

## Kompletní kruh

Tak, a jsme na konci. Probrali jsme všechny čtyři komponenty, ze kterých se Ring skládá --- handler, request, response a middleware. Pokdu je všechny spojíme do jednoduché, spustitelné aplikace, může to vypadat takto:

{{< gist sw-samuraj e48d666745cd35382b02e9b12e946366 >}}

A výsledek v browseru:

{{< figure src="/2017/05/Ring-app.png" >}}

## Co příště?

Můj původní záměr o příštím článku bylo napsat o routování pomocí [Compojure](//github.com/weavejester/compojure). Když jsem se ale ponořil do psaní sekce o middleware a připravoval si příklady, začalo se tohle pod-téma dost rozrůstat. Middleware je hodně zajímavý koncept --- jednoduchý a velmi silný --- a proto by bylo škoda se mu nevěnovat samostatně.

## GitHub projekt

Pokud vám výše popsané principy neštymují dohromady, mrkněte na GitHub, kde je malý spustitelný projektík:

* [sw-samuraj/blog-ring](//github.com/sw-samuraj/blog-ring)

## Související články

* [Clojure web development: Ring Middleware](/2017/07/clojure-web-development-ring-middleware)
* Clojure web development: Compojure (TBD)
* Clojure web development: Hiccup (TBD)
