+++
title = "REST contract-first: Swagger & Gradle"
date = 2017-05-09T14:42:00Z
updated = 2017-06-17T11:53:41Z
description = """Mám rád přístup contract-first. U webových služeb založených
    na SOAP je to poměrně etablovaný přístup. U těch REST-ových to ještě není
    tak jednoznačné, ale také už se to vyvíjí správným směrem. A tak SoftWare
    Samuraj přináší krátký tutorial, jak tento problém řešit pomocí Swagger
    a Gradle."""
tags = ["gradle", "swagger", "web-services", "rest", "build"]
aliases = [
    "/2017/05/rest-contract-first-swagger-gradle.html"
]
blogimport = true
hero_image = "hero.jpg"
[author]
	name = "Vít Kotačka"
	uri = "https://plus.google.com/107194067086704550761"
+++

{{< figure class="floatleft" src="/2017/05/Swagger-Gradle.png" >}}

U webových služeb mám rád přístup [contract-first](//docs.spring.io/spring-ws/site/reference/html/why-contract-first.html). Jsem 100% přesvědčen, že tak vzniká lepší design i lepší API.

V případě SOAP webových služeb je to celkem běžné. (Teda pokud webové služby "nedesignují" programátoři --- to pak většinou skončí "vyzvracením" interního kódu na veřejnost.)

Ohledně REST-ových služeb mi to přijde jako minoritní způsob. To je jednak škoda a jednak problém. Ono to nakonec vždycky nějak funguje. Ale jen výjimečně pak vznikají [API, které vývojáři milují](//pages.apigee.com/rs/apigee/images/api-design-ebook-2012-03.pdf).

Jak tedy na REST contract-first službu? Následuje popis, který jsem použil na stávajícím projektu a se kterým jsem --- po vychytání (Swagger) much --- spokojen.

## Jak by to mělo fungovat?

Mám dost jasnou představu, jak by celistvý přístup contract-first měl fungovat. Pokud máte jiný postup, nebo se mnou nesouhlasíte, budu rád, když se podělíte v komentářích. Můj zobecněný přístup vypadá takto:

1. Napsat kontrakt v nějaké rozumně standardizované a obecně přijímané specifikaci.
1. Vygenerovat potřebný kód, zejména model a rozhraní (API).
1. Vygenerovaný kód by měl být v adresáři, kde build tool očekává zdrojové kódy.
1. Generování a kompilace vygenerovaného kódu je součástí standardního build lifecycle (není potřeba je spouštět samostatně).
1. Ideálně, generování a kompilace probíhá jen tehdy, pokud došlo ke změně specifikace.
1. Implementaci rozhraní si píšu sám, ručně.

## Swagger


[Swagger](//swagger.io/) je soubor nástrojů, které se točí kolem [OpenAPI specifikace](//github.com/OAI/OpenAPI-Specification). Za OpenAPI si můžete představit "něco jako [WSDL](//en.wikipedia.org/wiki/Web_Services_Description_Language) pro [REST](//en.wikipedia.org/wiki/Representational_state_transfer)". Pro naše potřeby nás budou zajímat dva nástroje: [Swagger Editor](//swagger.io/swagger-editor/) pro napsání specifikace a [Swagger Codegen](//swagger.io/swagger-codegen/) pro generování kódu ze specifikace.

### Swagger Editor

[Swagger Editor](//swagger.io/swagger-editor/) je webový editor, který si můžete [vyzkoušet on-line](//editor.swagger.io/), ale pro reálnou práci bude lepší ho mít [lokálně](//swagger.io/docs/swagger-tools/#swagger-editor-documentation-0). Je trochu otravné, že kvůli tomu musíte nainstalovat [Node.js](//nodejs.org/en/), ale jinak má lokální verze víc šikovných funkcionalit.

{{< figure src="/2017/05/Swagger-editor.png" caption="Swagger Editor" >}}

Mimochodem, za celým Swaggerem stojí společnost [SmartBear](//smartbear.com/), která dělá [SoapUI](//www.soapui.org/), takže očekávejte něco podobného --- je to proklatě použitelné a v detailech mrzce nedotažené. A mizerná dokumentace.

### Swagger specifikace

Swagger specifikace může mít dva formáty: [JSON](//www.json.org/), nebo [YAML](//www.yaml.org/spec/1.2/spec.html). Jak na zmíněném projektu, tak v článku jsem zvolil YAML --- jednak je to čitelnější pro lidi a pak, aspoň na projektu, to nebudou číst jenom programátoři.

Takže, contract-first. Začneme jednoduchou Swagger specifikací, která nám odpoví na [otázku Života, Vesmíru a vůbec](//en.wikipedia.org/wiki/Phrases_from_The_Hitchhiker%27s_Guide_to_the_Galaxy#Answer_to_the_Ultimate_Question_of_Life.2C_the_Universe.2C_and_Everything_.2842.29):

{{< gist sw-samuraj 6e85b400a64d8e27ddbb8678bdf3a7ae >}}

### Swagger Codegen

[Swagger Codegen](//swagger.io/swagger-codegen/) je Java knihovna s [CLI](//en.wikipedia.org/wiki/Command-line_interface) rozhraním. Swagger se chlubí, že pro generování kódu podporuje 20 serverových a 40 klientských řešení. Moje ukázka je ve [Springu](//spring.io/), ale klidně si vyberte svoji oblíbenou platformu.

{{< figure src="/2017/05/Swagger-codegen-CLI.png" caption="Swagger Codegen CLI" >}}

Jak už jsem to naťuknul výše, ne všechno je ve Swaggeru perfektní --- kolegové si dost stěžovali na kvalitu a použitelnost generovaných artefaktů pro [JAX-RS](//jax-rs-spec.java.net/) a pro C#.

Já jsem sice s výsledkem spokojený a dělá to přesně, co jsem chtěl, ale bylo potřeba to poladit --- strávil jsem cca 2 dny experimentováním, než jsem se dostal do stavu "akceptováno". Dva dny se mohou zdát hodně, ale jednak to byla zábava a jednak se to v blízké budoucnosti bohatě vrátí.

No, komand-lajna je sice boží, ale my se s ní patlat nebudeme, jsme přece profíci --- použijeme [Gradle](//gradle.org/).

## Gradle

Pokud na [Gradle Plugin Portálu](//plugins.gradle.org/search?term=swagger) zadáte heslo Swagger, vyjede vám 7 pluginů. Trochu jsem se bál, abych si nemusel napsat [vlastní Gradle plugin](//plugins.gradle.org/plugin/cz.swsamuraj.jaxws), jako se mi to stalo u JAX-WS, protože žádný plugin nedělal, co jsem očekával. Ale nakonec po pročtení GitHubu a vyzkoušení jsem vybral plugin [org.hidetake.swagger.generator](//plugins.gradle.org/plugin/org.hidetake.swagger.generator), který šel rozumně ohnout pro moje potřeby.

Konfigurace zmíněného pluginu v build skriptu `build.gradle` může vypadat následovně (po ořezání ostatních věcí, které nás z hlediska Swaggeru nezajímají).

{{< gist sw-samuraj 82b76331964685c47b0076bed18e6681 >}}

Podstatné věci na uvedeném skriptu:

* V závislostech nám přibyla [konfigurace](//docs.gradle.org/current/dsl/org.gradle.api.artifacts.Configuration.html) `swaggerCodegen`.
* Kvůli Swagger anotacím generovaným do API tříd je potřeba přidat závislost na `swagger-annotations`. To je sice otravný, ale asi nutná daň za použití Swaggeru. Naštěstí má ta knihovna jen 20 kB.
* Cílová platforma, pro kterou generujeme, je definovaná atributem `language`.
* Aby se nám negeneroval veškerý Swagger čurbes, omezíme generované artefakty atributem `components`, kdy říkáme, že chceme jenom `model` a `api`.
* Adresář s vygenerovanými artefakty je potřeba přidat ke zdrojovým souborům pomocí 
[sourceSets](//docs.gradle.org/current/dsl/org.gradle.api.tasks.SourceSet.html). (To už není plugin, ale čistý Gradle.)
* A poslední řádek předsadí v lifecyclu task `generateSwaggerCode` před kompilaci Java kódu.

Bohužel, tím jsme s konfigurací ještě neskončili --- tady to plugin mohl dotáhnout ještě dál. Sofistikovanější nastavení je potřeba dotáhnout v souboru `config.json`:

{{< gist sw-samuraj f296a2d1648bfef218d67e6021e00c75 >}}

Tady stojí za zmínku dvě věci:

* Jednak prázdný atribut `sourceFolder`, to aby nám Swagger nevygeneroval duplicitně zanořené adresáře.
* A potom říkáme, že chceme vygenerovat jenom rozhraní (atribut `interfaceOnly`), aby nám Swagger rovnou negeneroval prázdné dummy kontrolery. (Možná použitelné jako stuby, pokud generujeme jenom jednorázově.)

A je to. Pokud spustíme build příkazem `gradle build`, dostaneme následující strukturu, kdy Swagger specifikace je v adresáři `src/main/swagger` a generovaný kód v adresáři `src/generated/swagger`:

{{< figure src="/2017/05/Swagger-directory-tree.png" caption="Adresářová struktura projektu se Swagger definicí a generovaným kódem" >}}

## Ukázkový projekt

Pro potřebu článku jsem vytvořil malý projekt na [Bitbucketu](//bitbucket.org/sw-samuraj/), který vygeneruje potřebný kód a jde spustit v embeddovaném [Jetty](//www.eclipse.org/jetty/). Stačí naklonovan a spustit `gradle jettyRun`.

* [sw-samuraj/blog-rest-contract-first](//bitbucket.org/sw-samuraj/blog-rest-contract-first)

Měli byste ho vyzkoušet --- minimálně vám odpoví na nejzákladnější otázku života. A vesmíru. A vůbec... 

## Související externí články

* [Generování modelu z XSD](//calavera.info/v3/blog/2016/02/01/generovani-modelu-z-xsd.html) (calavera.info)
