" -------------------
" Generic markdownify
" -------------------

" Put every tag to new line
%s/></>\r</g
" Separate paragraphs by one line
%s/<br \/>\n*<br \/>/\r\r/
%s/\n\n\{2,}/\r\r/
" Markdownify links
%s/<a href="https\?:\(.\{-}\)">\(.\{-}\)<\/a>/[\2](\1)/g
" Markdownify bold, cursiva & strikethrough
%s/<i>\(.\{-}\)<\/i>/_\1_/g
%s/<b>\(.\{-}\)<\/b>/**\1**/g
%s/<strike>\(.\{-}\)<\/strike>/\~\~\1\~\~/g
" Markdownify headers
%s/<h2.\{-}>\(.\{-}\)<\/h2>/## \1\r\r/g
%s/<h3.\{-}>\(.\{-}\)<\/h3>/### \1\r\r/g
%s/<h4.\{-}>\(.\{-}\)<\/h4>/#### \1\r\r/g
" Markdownify monospace
%s/<span.\{-}monospace.\{-}>\(.\{-}\)<\/span>/`\1`/g
" Markdownify items
%s/<li>\n\?\(.\{-}\)\n\?<\/li>/* \1/
" Replace a non-breaking-space with a space
%s/&nbsp;/ /g
" Markdownify dashes
%s/ - / --- /g
" Markdownify blockquotes
%s/<blockquote.\{-}>\n\?\(.\+\)\n\?<\/blockquote>/> \1\r\r/g

" -------------------------
" External sites shortcodes
" -------------------------

" Markdownify gists
%s/<script .\{-}\(gist\)\.github\.com\/\(.\{-}\)\/\(.\{-}\)\.js">\n\?<\/script>\s*\(<br \/>\)\?/{{< \1 \2 \3 >}}\r\r/g

" ---------------------------
" sw-samuraj.cz & clojure.cz specific changes
" ---------------------------

" Remove following prefix from links, plus .html suffix:
" * //sw-samuraj.cz
" * //www.sw-samuraj.cz
" * http://sw-samuraj.cz
" * https://sw-samuraj.cz
" * http://www.sw-samuraj.cz
" * https://www.sw-samuraj.cz
%s/(\(https\?:\)\?\/\/\(w\{3}\.\)\?sw-samuraj\.cz\(\/.\{-}\)\.html)/(\3\/)/cg
" Remove following prefix from links, plus .html suffix:
" * //clojure.cz
" * //www.clojure.cz
" * http://clojure.cz
" * https://clojure.cz
" * http://www.clojure.cz
" * https://www.clojure.cz
%s/(\(https\?:\)\?\/\/\(w\{3}\.\)\?clojure\.cz\(\/.\{-}\)\.html)/(\3\/)/cg
